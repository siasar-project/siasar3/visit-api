# Project dev branch status

[![pipeline status](https://gitlab.com/siasar3/visit-api/badges/dev/pipeline.svg)](https://gitlab.com/siasar3/visit-api/-/commits/dev)

# Install project

By GitHub issue we require composer 2.1.8.

The project was configured with Dockerizer. We need to start containers first, to work with the code.

First install dependencies

`composer install`

Then create and update the database.

First time.
```bash
console doctrine:database:create
console lexik:jwt:generate-keypair
```

Both commands, symfony and console, are integrated in Dockerizer. When you see in documentation anything like:
```bash
php bin/console ...
```

You can replace it with:
```bash
console ...
```

The same with the symfony command.

With each code update:

- See database status: `console doctrine:migrations:status`
- Update the database if required: `console doctrine:migrations:migrate`
  This will erase the database content if executed.
- Load default test data: `console doctrine:fixtures:load`
- Import database configurations: `console configuration:import`
- Import translations files: `console gettext:import-all-po`
- Create default parametric: `console doctrine:parametric:default <country code>`

By default, translations must live in `Translations` folder.

With `launch` command we can see the site. We have the `launchpma` command to open the phpMyAdmin tool.

Complete database reset:
`console doctrine:database:drop --force && console doctrine:database:create && console doctrine:migrations:migrate --no-interaction && console doctrine:fixtures:load -n && console configuration:import -n && console gettext:import-all-po`

The dockerizer domain is `siasar3.localhost`

To load demo data (use -c option to clean before generate):
The order is important.
- Generate entities: `console doctrine:generate:dummies`
- Generate users: `console user:generate:dummies`
  - You can see generated users with: `console user:list ROLE_USER`
- Generate form records: `console forms:generate:dummies -r 10 <username>`
  - Use `console forms:generate:dummies -r 0 -c <username>` to clear forms.

To change the server list into the Swagger UI we have the environment variable `APP_CONFIG_OTHER_SERVERS`, in .env for example. 

First one line deploy, with example data:

`console doctrine:database:drop --force && console doctrine:database:create && console doctrine:migrations:migrate --no-interaction && console doctrine:fixtures:load -n && console configuration:import -n && console gettext:import-all-po && console doctrine:generate:dummies -c hn && console doctrine:parametric:default hn && console doctrine:polygon:update hn tests/assets/geometry/hn.geojson && console user:generate:dummies -c hn && console forms:generate:dummies -r 0 -c admin hn && console user:list ROLE_USER`

## Production

This commands must be executed in `development` environment, after that we must change to `production` environment and clear cache, `console cache:clear`.

- `console lexik:jwt:generate-keypair`
  - First time only.
- `console security:regenerate-app-secret .env.local`
  - First time only.
- `console doctrine:database:create`
  - First time only.
- `console doctrine:migrations:migrate --no-interaction`
- `console doctrine:fixtures:load -n`
- `console configuration:import -n`
- `console gettext:import-all-po`
- `console doctrine:parametric:default COUNTRY_ID`
  - `COUNTRY_ID` is the instance country ID.
- `console user:add admin MAIL COUNTRY_ID ROLE_ADMIN`
  - First time only.
  - `MAIL` is the new user mail.
  - `COUNTRY_ID` is the instance country ID.
  - `ROLE_ADMIN` is the real role ID, NOT modify to get un admin user.
  - Remember annotate the generated password.
- `console user:list ROLE_USER`
- `console doctrine:import:parametric COUNTRY_ID ENTITY_CLASS FILE_PATH`
  - `COUNTRY_ID` is the instance country ID.
  - `ENTITY_CLASS` is the entity type, parametric, to import.
  - `FILE_PATH` is the file path to import.
- `console siasar2:query -c divisions -i COUNTRY_ID`
  - `COUNTRY_ID` is the instance country ID.
- `console configuration:maintenance:on`
- `console user:add SIASAR mail@ COUNTRY_ID ROLE_ADMIN`
  - The password will be auto-generated.
  - Ej. `console user:add siasar mfernandez3@worldbank.org ni ROLE_ADMIN`

# Coding

All code must pass code styling with:
```
phpcs -p --colors --standard=Symfony src/
phpcs -p --colors --standard=Symfony tests/
```

We can run tests with `bin/phpunit`. The data test fixtures are in `/src/TestFixtures`, in YAML format.

By default .env send mails to a local Mailhog instance, we can inspect mails in http://siasar3.mailhog.localhost/

Registered PHP Streams:
- **files://**: `File` entities storage folder.
- **assets://**: Static assets' folder.

**IMPORTANT** Doctrine transactions are disabled by code. To use transactions we must use custom methods:

```php
        $this->connection->superBeginTransaction();
        try {
            $pointManager->drop($id);
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();
            throw $e;
        }
```

A transaction must be used in the last layer only, for example in end-point responses.

Generate new migration after update an entity:
`console doctrine:migrations:diff`

Generated migrations must extend `AbstractNotTransactionalMigration` class to avoid fixture load error `There is no active transaction`.

App\Kernel::__constructor define UTC how the system time zone. The database must be setting in this time zone too.

Doctrine entities and Forms must have names in singular, this way plural generated names will be betters.

## System configuration model

This project implements a new configuration model based in the Drupal 8 configurations. It's built over the
`configuration` entity, but the repository doesn't return configuration entities. Instead, return `ConfigurationRead`
instances to read-only configurations or `ConfigurationWrite` instances to writable configuration. By default, we read
configurations how read-only, and if we need to update or create it we do a special call to the repository.

A configuration is a key name with an array associated. The array value must be serializable, and it can have any
elements amount. It has two special values:
- **requires**: Must contain a list of configuration keys. This property is used to select other configurations that must
  exist before creating this one.
- **local_only**: Optional boolean value. If present and true the configuration can't be exported, and it's ignored while importing.

## SIASAR Point (SP)
It's used to manage inquiry forms groups.

- The collection end-point must list Administrative Divisions.
- Each listed SP must list the actions allowed to the current user.
- Each Administrative Division must refer to an SP, if SP does not exist must simulate one with status `without planning`.
- Each SP must use `pointing` workflow.
- Each SP must have a version code, the creation date.
- Each SP must build a tree with the referred inquiries.
- Each SP must have a list of warnings and errors, before entering to the `validating` state must delete all warnings & errors.
- An SP in state `reviewing` & all inquiries in `validated` state, must show with state `reviewed`.
- An SP in state `calculated` can't be modified, we only can copy it and set `planning` state.
- Each new SP, empty or copied, must have a user list.

## SP database map
The picture with tables and fields required to query about SP by Administrative divisions.
![](assets/SP database map.png)

## Adding a new measurement unit configuration and form field

We can see `system.units.flow` or `system.units.volume` how examples, there are two system configuration keys.

- Create a new YML file in the configuration export folder, the file name is the configuration key with extension .yml.
- Add the new configuration key to `src/Security/Voter/ConfigurationVoter.php`, in `CONFIGURATION_BY_ATTRIBUTE` constant.
- Create a new `Measurement` tool class in `src/Tools/Measurements`.
- Create the field type class into `src/Forms/FieldTypes/Types` folder, see other fields types how example.
- Add to country a list property, prefixed with `unit` and in plural, for example `unitFlows`.
- Add to country a main unit property, prefixed with `mainUnit` string. For example: `mainUnitFlow`.
- The new country properties must limit the new values to units that exist into the system configuration.
- Update `serializeToLog` Country method.
- Update `src/Command/DoctrineGenerateDummyCommandAbstract.php` command.
- Expose the new configuration in Open API.
- Write tests to validate the end-points.

## How to expose a system configuration in Open API

Each listed file have valid examples inside.

- _src/Entity/Configuration.php_: You must add a new end-point using annotations.
- _src/Api/Action/ConfigurationAction.php_: Add the new route. It must be in `/configuration` route space.
- _src/Security/Voter/ConfigurationVoter.php_: Add the new public configuration key in `ConfigurationVoter::CONFIGURATION_BY_ATTRIBUTE` array.

## How to secure an Entity in Open API

Customize ApiResource annotation to add security:
```php
 * @SecuredRepository(class=<Entity name>RepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"<Camel case entity name>:write"}},
 *     normalizationContext={"groups":{"<Camel case entity name>:read"}},
 *     itemOperations={
 *          "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *          "put" = { "method" = "PUT", "security" = "is_granted('UPDATE', object)" },
 *          "patch" = { "method" = "PATCH", "security" = "is_granted('UPDATE', object)" },
 *          "delete" = { "method" = "DELETE", "security" = "is_granted('DELETE', object)" }
 *     },
 *     collectionOperations={
 *      "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)"
 *      },
 *      "post" = { "method" = "POST", "security_post_denormalize" = "is_granted('CREATE', object)" },
 *     }
 * )
```

Add management classes:
```
App\RepositorySecured\<Entity name>RepositorySecured
App\DataPersister\<Entity name>DataPersister
App\DataProvider\Extension\<Entity name>CollectionExtensionInterface
App\DataProvider\Extension\<Entity name>PaginationExtension
App\DataProvider\<Entity name>CollectionDataProvider
App\DataProvider\<Entity name>DataProvider
App\Security\Voter\<Entity name>Voter
App\Serializer\Normalizer\<Entity name>Normalizer
```

We can use AdministrativeDivision implementation how example.

## How to add a new parametric to country.

Steps:

- Create new entity with country relations. (If you have default values, add lock control)
- Add `App\Annotations\IsCountryParametric` annotation to the new parametric class.
- Generate doctrine migration.
- Add new permissions to system.permissions.
- Set role permissions.
- Update permissions test assets
- Create default country instances into src/EventListener/CountryToConfigurationListener.php
- Create new reference form field.
- Add new entity repository. (If you have default values, add lock control)
- Add new entity securized repository.
- Create the new App\Security\Voter\<Entity name>Voter. (If you have default values, add lock control)
- Add App\DataPersister\<Entity name>DataPersister (Optional)
- Add App\DataProvider\Extension\<Entity name>CollectionExtensionInterface
- Add App\DataProvider\Extension\<Entity name>PaginationExtension
- Add App\DataProvider\<Entity name>CollectionDataProvider
- Add App\Serializer\Normalizer\<Entity name>Normalizer (Optional)
- Add new src/TestFixtures/ (To two countries, then test that permissions are working) `console debug:ulid:get32`
- Update tests/Functional/CountryParameters/CountryResourceTest.php
- Update tests/Functional/OpenApi/GraphQlTest.php
- Update tests/Functional/OpenApi/OpenApiQueryParamsTest.php
- Update tests/Functional/OpenApi/OpenApiTest.php
- Create new field type tests (tests/Unit/Forms)
- Create new country parametric tests
- Update tests/Unit/Forms/FormManagerTest.php
- Update tests/Unit/Service/User/AccessManagerTest.php
- Generate new upload templates with `console doctrine:generate:upload:templates`
- Update README.md with the new field type using `console forms:fieldtype:list > fieldtypes.md`

See "How to secure an Entity in Open API"

## Updating administrative division types in a Country

The countries have a dynamic administrative divisions levels. Each administrative division can have multiple sub
administrative divisions, how children. To optimize queries, each administrative division has a level value that
is the deepest into the administrative division types tree.

In formularies, the administrative division field type only allows reference to a leaf administrative division in
the last deep level, these administrative divisions are called community.

By that, we can't reduce the administrative division types in a Country. To do it we need the next steps into a
database transaction:
- Remove the administrative division type in the database configuration entity.
- Remove it in the configuration file, YML, too.
- Set all administrative divisions levels to 0 in this country.
- Update each administrative division level in this country.

If we need to add new administrative division types in the Country we need only add them in the configuration file, YML,
and import.

## Cron jobs

Defined jobs:
 --------------------------------------- ------------- -------------------------------- 
  Job                                     Format        Worker                           
 --------------------------------------- ------------- -------------------------------- 
  Queue runner                            */1 * * * *   App\Cron\QueueRunnerCron            
  Queue pruner                            0 4 * * *     App\Cron\QueuePrunerCron            
  Queue point calculating update          * * * * *     App\Cron\CreatePointCalcJobsCron            
  Remove orphan files                     0 4 * * *     App\Cron\RemoveOrphanFilesCron   
 --------------------------------------- ------------- --------------------------------

Jobs are implemented using [PSF1/CronBundle](https://github.com/PSF1/CronBundle) and the tasks queue with
[mmucklo/DtcQueueBundle](https://github.com/mmucklo/DtcQueueBundle).

The application need add a crontab to execute each minute:

`php <full path>/bin/console cron:job:run -e prod`

Staging example cron job:

`* * * * * cd /home/stg.visitapi.siasar3.front.id/public_html/ && /usr/bin/php /home/stg.visitapi.siasar3.front.id/public_html/bin/console cron:job:run -e prod >/dev/null 2>&1`

Jobs will be executed in their own timing.

Note: Some jobs need to execute console commands, but it call the console with a relative file system path.

### Adding queue cron jobs.

Queue workers can't use Doctrine entity manager, to do that we need to use an external queue manager.

This implementation use console commands to implement queue jobs.

Example of adding a new queue job.
- `console dtc:queue:create_job Console execute forms:point:queue:calculating siasar`

Useful console commands:
- `console cron:job:run` to execute queue jobs.
- `console dtc:queue:count` to see current queue status.
- `console dtc:queue:reset` restart stopped jobs.

## Post configuration event actions

When we update the code in servers maybe we need to do changes in the database or files to do compatible with
the system with the new code, but the code can't detect this type of change and it can't auto-update it.

To manage this type of low-level change we have the after configuration import methods.

To add a new update action add a new method named updateN in class App\Update\SystemUpdateMethods, where N is the updated version.
The first one is 0, the next 1, etc...
System will remember last executed method and the next time will execute last + 1.
Each update must verify that the changes are required, and it's not been applied before in the system.

## Starting parametric values

Some field types need an "Other" option, or other not parametric values, but other valid values are parametric.
To add this default values we must add a "constant" values to the collection. We can do it this way:

Default parametric values could be defined in `src/ParametricFixtures` folder with a YML file. Default values will be
created for each country with the same properties.

To install the default parametric we can execute this console command:

(this command doesn't duplicate values)

`console doctrine:parametric:default <country code>`

A parametric is a Doctrine entity that extend `FormReferenceEntityInterface`. That interface requires implement `isStarting`
and `setStarting` to allow creates protected parametric instances. we need implement an ignored property to set it:

```php
    /**
     * @var bool
     *
     * @Ignore
     */
    protected bool $starting = false;
```

That it's accomplished using `FormReferenceStartingTrait`. Then we need to lock the edition of these instances, like this:

```php
# In the doctrine entity.
    /**
     * Set the name
     *
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        if ((!$this->starting) && ('1' === $this->keyIndex || '99' === $this->keyIndex)) {
            throw new \Exception(self::t("You can't update keys 1 or 99."));
        }

        $this->name = $name;

        return $this;
    }
```

lock the remove in the Repository, like this:

```php
    /**
     * @inheritDoc
     */
    public function remove(Object $entity): void
    {
        /** @var GeographicalScope $entity */
        if ('1' === $entity->getKeyIndex() || '99' === $entity->getKeyIndex()) {
            throw new \Exception(self::t("You can't remove keys 1 or 99."));
        }
        $this->_em->remove($entity);
    }
```

And in the voter, `src/Security/Voter/`, like this:

```php
    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if ('other' === strtolower(trim($subject->getType()))) {
            return false;
        }

        return parent::voteOnAttribute($attribute, $subject,  $token);
    }
```

# Updating project

- See `composer recipes`
- See `composer show -l --direct`

# Exposed API

API Generated with https://api-platform.com/docs/core/#features

Live in `/api/v1`, this URL has a default API client interface. In `/api/v1/graphql/graphql_playground`, we have a GraphQL interface.

The system uses JWT tokens, this token contains session user information in it.
For example, you can see it with: [jwt.io viewer](https://jwt.io/#debugger-io?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MjQ0NjE1OTgsImV4cCI6MTYyNDQ2NTE5OCwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6IkFkbWluIiwiaXAiOiIxNzIuMTguMC4zIiwiaWQiOiIwMUY4VlgzSDhNUUNZSkJCQ1NTMjBIS0hRNiJ9.i9wzv1DyOURwuyhERJ7uixKjYXUZuR7T4qW10fInqSuV0f-ZWxms_AI-MXfeTU95uzWJTu5tAI4IlCtGG65DpuXUNEVxVXyBJX8kkjqWsTepB7ddUZ_Kb89g3DRCQuzYn86M34TTi4WujZFyh61BdvfKskzwjczbEB2G2xmD44ogHQxucSoIIDwuP7sA0l2wCWjyhKIf9HcQRLJ38fMA_3Ioli7CRhkAfqAjoyLKbQUmzd2Ys_gW32WQJ-9qv2cUpZAy-e7AnksmyDqLX7ftTsGLw6T-DjsBv2e8xWCb5YCsbMgGuWbbxowVcH69BXITZgqVvBVHfZKKq216E1YFTg)

**Warning**: JWTs are credentials, which can grant access to resources. Be careful where you paste them!

Updating Country form levels example:
```bash
curl -X 'PUT' \
  '/api/v1/countries/hn' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer ...' \
  -H 'Content-Type: application/json' \
  -d '{
	"formLevel": {
		"form.community": {
			"level": 2,
			"sdg": true
		},
		"form.health.care": {
			"level": 2,
			"sdg": true
		}
	}
}'
```

# GraphQl

Included UI path: `/api/v1/graphql/graphql_playground`

Example User query:
```graphql
{
  user(id: "/api/v1/users/01F8WBP0JMPC9TDWTKT36KV8AR") {
    id
    _id
    username
    email
  }
}
```

Example response:
```graphql
{
  "data": {
    "user": {
      "id": "/api/v1/users/01F8WBP0JMPC9TDWTKT36KV8AR",
      "_id": "01F8WBP0JMPC9TDWTKT36KV8AR",
      "username": "front.id",
      "email": "pedro+ejemplo@front.id"
    }
  }
}
```

Fields of type `select` defined in Forms, using own Form Factory, will return the key with `v` prefixed. Use the 
GraphQL generated documentation for more information.

You can see more examples in unit tests.

# Workflows

We can generate PNG files with: `console workflow:dump <workflow name> | dot -Tpng > <workflow name>-workflow.png`

## publishing
Basic workflow to assist in development.

![](assets/publishing-workflow.png)

## inquiring
Inquiries workflow.

- We can't delete Inquiries in states `validated`, `removed` or `locked`.
- If Inquiry never get `validated` state, it can be deleted from the system.
- A `validated` Inquiry can be updated, this action create a clone in `draft` state and get `locked` state.

![](assets/inquiring-workflow.png)

## pointing
SIASAR Points workflow, these Points it's used to manage inquiry forms groups.

![](assets/pointing-workflow.png)

# Console commands

## configuration
- **configuration:export** Export configuration into files. See "APP_CONFIG_EXPORT_DIR" in ".env".
- **configuration:import** Import configuration into database. See "APP_CONFIG_EXPORT_DIR" in ".env".
- **configuration:maintenance:on** Activate the maintenance mode.
- **configuration:maintenance:off** Deactivate the maintenance mode.
- **configuration:maintenance:status** Display maintenance mode status.
- **configuration:update:next** Set the next configuration update. This command allow go back updates.

## gettext
- **gettext:export-all-po** Export all languages to PO files.
- **gettext:export-to-po** Export translation literals to an PO file.
- **gettext:import-all-po** Import all languages from PO files.
- **gettext:import-from-po** Import translation literals from an PO file.
- **gettext:export-languages** Export the list of defined languages.
- **gettext:scan** Scan PHP files to find translatable strings and create it in database.
- **gettext:clean** Remove all translation literals.
- **gettext:status** Translation literals status.

## language
- **language:plural-rules** Display language plural rules how YAML.

## user
- **user:list** List users by role.
- **user:password** Set user password.
- **user:token** Get authentication token.
- **user:add** Add a new user.
- **user:show:role** Display role information.
- **user:show:permission** Display permission information.
- **user:generate:dummies** Generate dummy users.

## file
- **file:remove:orphan** Remove file system orphan files.
- **file:show** Display file information.

## Forms
- **forms:fieldtype:list** Display forms field type list.
- **forms:generate:dummies** Generate dummy form records.
- **forms:lint** Validate forms meta.
- **forms:configuration:update** Update forms default configuration structure.
- **forms:list** List defined forms with manager type.
- **forms:find:field** Find field ID by catalog ID.
- **forms:indicator:update** Update a form record indicator value.
- **forms:dump:record** Json form record dump.
- **forms:point:queue:calculating** Search calculating SIASAR points to create queue jobs.
- **forms:point:indicator:update** Update a SIASAR point indicator values.
- **forms:recalculate:indicators** Delete all calculated indicators and recalculate all those with "calculated" status
- **forms:reopen:households** Re-open household process by community inquiry.
- **forms:clean:subforms** Find and clean orphan sub-forms.

## Debug
- **debug:ulid:get32** Display a new Base 32 ULID.

## Doctrine
- **doctrine:generate:dummies** Generate example Doctrine entities.
- **doctrine:entity:clear** Clean Doctrine entities.
- **doctrine:generate:upload:templates** Generate parametric upload template files.
- **doctrine:import:parametric** Massive parametric import.
- **doctrine:parametric:list** List all existing parametrics.
- **doctrine:polygon:update** Update country polygon.
- **doctrine:polygon:test** Validate country polygon.

## MySQL
- **mysql:status** Display database status and settings.
- **mysql:dump** Dump database.

## Remote
- **remote:exec** Execute a command in a remote server.
- **remote:console** Execute console in a remote server.
- **remote:import:database** Import remote database locally.
- **remote:import:files** Import remote content files locally.
- **remote:import** Import remote content locally.

## SIASAR 2
- **siasar2:query** Query SIASAR 2 server.

## Security
- **security:regenerate-app-secret** Generate and update app secret value.

# Forms documentation

Note: While development, if you see a record memory status with xDebug, or others, the 
multivalued fields maybe empty if not used any time.

Each form can disable any end-point with the next properties in the configuration:
- **endpoint**: End-point settings.
  - **collection**: Collection settings.
    - **get**: true
    - **post**: true
  - **item**: Item settings.
    - **get**: true
    - **put**: true
    - **delete**: true
    - **patch**: true
    - **clone**: false

If any of these settings it's not defined take the default value of true, except clone that have a default value of false.
Instead of true, we can set a method name from the class `src/Api/Action/FormCollectionAction.php`,
or inherited class.

For example:
- **endpoint**: End-point settings.
  - **<space>**: Collection settings.
    - **<method>**: App\Api\Action\PointAction::getPointCollection

This replaces the default form controller with `getPointCollection`, the `App\Api\Action\PointAction` class must
inherits from `src/Api/Action/FormCollectionAction.php`.

Other option is define a new controller and a new definition, this way:
- **endpoint**: End-point settings.
  - **<space>**: Collection settings.
    - **<method>**:
        controller: App\Api\Action\PointAction::getPointCollection
        openapi_operation: App\Api\Action\PointAction::getPointCollectionOperation

## Updating form manager
If we require update a form manager we must do it updating the Yaml file in
the configuration folder, we must edit the `type` key.

Then we must compare default fields in the new form manager and add/update
field list in the Yaml file. To remove fields without data loosing we must
mark the field how `deprecated`.

## Mutateble field type

A `mutateble` field type can be drawn how another field type, but always save the value how `long_text`.
The responsibility to set the final aspect is from the application, saving settings in the Country.

Valid field types to mutate are:

- boolean
- radio_boolean
- decimal
- integer
- long_text
- short_text
- select
- radio_select

We can know each mutateble field, in any form, calling the end-point `/api/v1/configuration/mutatebles`. The end-point list each
form with the mutable field list inside.

We have the `mutatebleFields` property in the' Country' entity with the fields settings.

In the `Country` entity we have the `mutableFields` property with the fields settings, in this structure:

```json
{
  "form.id": {
    "field_id": {
      "options": {
        "key": "label",
        "key1": "label 1"
      },
      "mutate_to": "long_text",
      "true_label": "Yes",
      "false_label": "No",
      "show_labels": true
    }
  }
}
```

## Meta special properties

### Form meta
- **title**: Form title. 
- **version**: Form version string, it's a free text.
- **allow_sdg**: Boolean, use or not SDG fields.
- **icon_field**: Optional. Field ID to use how records image, it must be an image_reference field.
- **title_field**: Optional. Field ID to use how records title, it must be a short_text field.
- **interviewer_field**: Optional. Field ID to use how records interviewer, it must be a user reference field.
- **table_fields**: Optional. Field ID list to use how subforms columns, it must be 'integer', 'decimal', 'long_text', 'short_text', 'date', 'phone', 'boolean', 'radio_boolean', 'ulid' field types.
- **parent_form**: In a `App\Forms\SubformFormManager` form type, reference to the parent form.
- **point_associated**: This form type must be inside a SIASAR Point. Required to `App\Forms\InquiryFormManager` form types.
- **indicator_class**: If this form has an indicator, use this class to calculate it. Optional to `App\Forms\InquiryFormManager` form types.
- **isSubForm**: This form aspect must be how a subform.
- **field_groups** Array of field groups.
  - **id**: Group ID, the fields have the meta property catalog_id to insert inside this group.
  - **title**: Group title
  - **help**: Group notes
  - **children**: Subgroups with same structure that sub-groups, indexed by ID
  - **field_id** If it's a field, the field id
  - **weight** Group/field weight
  - **level**: Field detail level. Each Country have a formLevel property to match with this one.
  - **sdg**: SDG field, allow this field if form.allow_sdg is true.
  - **conditional**: Condition applied to the property used how index.
    - **option**: Option visible if...
      - <option id>: `Conditional block`.
    - **visible**: Field visible if...
      - `Conditional block`

Conditional block:
Is an array of conditions.
A condition is a field with the required value or an operator, `or` or `and`, with a conditional block how value.
Fields can be one of three representation types:
- **parent.<field_id>**: In a sub-form, form type `App\Forms\SubformFormManager`, is a reference to a field in the parent form. 
- **<field_id_1>.<field_id>**: Reference a field into a sub-form. The sub-form is referred in the field_id_1 field.
- **<field_id>**: Field ID on tha current form

### Field meta
- **catalog_id**: ID to nest in field groups
- **level**: Field detail level. Each Country have a formLevel property to match with this one.
- **sdg**: SDG field, allow this field if form.allow_sdg is true.
- **conditional**: How in form meta.
- **field_other**: In a select field type, it's the field used for the "other" option.
- **disabled**: Not allow to add data in this field.
- **displayId**: False to disabled catalog_id property in the form draw.
- **filtered**: Allowed responses must be filtered by user access. (administrative_division_reference field type is a example of this requirement)
- **hidden**: If true a frontend app never must render this field.

## Field types
### Water system form record reference field type
 -------------------------------------- ------ ------------------------------------------------------ 
id                                      =>    wsystem_reference                                     
description                             =>                                                          
class                                   =>    App\Forms\FieldTypes\Types\WSystemReferenceFieldType
 -------------------------------------- ------ ------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------ 
required                                =>    Is this field require?                                
multivalued                             =>    Is this field multivalued?                            
weight                                  =>    Field weight                                          
meta                                    =>    Field metadata properties.                            
country_field                           =>    The country reference field                           
sort                                    =>    Is this field sortable?                               
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------ 
**· value**                             =>    int(0)                                                
**form**                                =>    string(13) "form.wssystem"
 -------------------------------------- ------ ------------------------------------------------------ 

### Type of health care facility type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------ 
id                                      =>    type_health_facility_reference                                    
description                             =>    Requires a country field in same form                             
class                                   =>    App\Forms\FieldTypes\Types\TypeHealthcareFacilityReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------ 
required                                =>    Is this field require?                                            
multivalued                             =>    Is this field multivalued?                                        
weight                                  =>    Field weight                                                      
meta                                    =>    Field metadata properties.                                        
sort                                    =>    Is this field sortable?                                           
filter                                  =>    Is this field filterable?                                         
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                      
**class**                               =>    string(33) "App\Entity\TypeHealthcareFacility"
 -------------------------------------- ------ ------------------------------------------------------------------ 

### Water service provider form record reference field type
 -------------------------------------- ------ --------------------------------------------------------- 
id                                      =>    wsprovider_reference                                     
description                             =>                                                             
class                                   =>    App\Forms\FieldTypes\Types\WSProviderReferenceFieldType
 -------------------------------------- ------ --------------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------------- 
required                                =>    Is this field require?                                   
multivalued                             =>    Is this field multivalued?                               
weight                                  =>    Field weight                                             
meta                                    =>    Field metadata properties.                               
country_field                           =>    The country reference field                              
sort                                    =>    Is this field sortable?                                  
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ --------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------------- 
**· value**                             =>    int(0)                                                   
**form**                                =>    string(15) "form.wsprovider"
 -------------------------------------- ------ --------------------------------------------------------- 

### Functions Carried Out W.S.P. reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------ 
id                                      =>    functions_carried_out_wsp_reference                               
description                             =>    Requires a country field in same form                             
class                                   =>    App\Forms\FieldTypes\Types\FunctionsCarriedOutWspReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------ 
required                                =>    Is this field require?                                            
multivalued                             =>    Is this field multivalued?                                        
weight                                  =>    Field weight                                                      
meta                                    =>    Field metadata properties.                                        
sort                                    =>    Is this field sortable?                                           
filter                                  =>    Is this field filterable?                                         
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                      
**class**                               =>    string(33) "App\Entity\FunctionsCarriedOutWsp"
 -------------------------------------- ------ ------------------------------------------------------------------ 

### Treatment Technology Coag-Floccu type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------- 
id                                      =>    treat_tech_coag_floccu_reference                                         
description                             =>    Requires a country field in same form                                    
class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyCoagFloccuReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                   
multivalued                             =>    Is this field multivalued?                                               
weight                                  =>    Field weight                                                             
meta                                    =>    Field metadata properties.                                               
sort                                    =>    Is this field sortable?                                                  
filter                                  =>    Is this field filterable?                                                
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                             
**class**                               =>    string(40) "App\Entity\TreatmentTechnologyCoagFloccu"
 -------------------------------------- ------ ------------------------------------------------------------------------- 

### Household Process type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------ 
id                                      =>    household_process_reference                                 
description                             =>    Requires a country field in same form                       
class                                   =>    App\Forms\FieldTypes\Types\HouseholdProcessReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------ 
required                                =>    Is this field require?                                      
multivalued                             =>    Is this field multivalued?                                  
weight                                  =>    Field weight                                                
meta                                    =>    Field metadata properties.                                  
sort                                    =>    Is this field sortable?                                     
filter                                  =>    Is this field filterable?                                   
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                
**class**                               =>    string(27) "App\Entity\HouseholdProcess"
 -------------------------------------- ------ ------------------------------------------------------------ 

### Form record reference field type
 -------------------------------------- ------ --------------------------------------------------------- 
id                                      =>    form_record_reference                                    
description                             =>                                                             
class                                   =>    App\Forms\FieldTypes\Types\FormRecordReferenceFieldType
 -------------------------------------- ------ --------------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------------- 
required                                =>    Is this field require?                                   
multivalued                             =>    Is this field multivalued?                               
weight                                  =>    Field weight                                             
meta                                    =>    Field metadata properties.
 -------------------------------------- ------ --------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------------- 
**· value**                             =>    int(0)                                                   
**form**                                =>    string(0) ""
 -------------------------------------- ------ --------------------------------------------------------- 

### Diameter field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    diameter                                                                
description                             =>                                                                            
class                                   =>    App\Forms\FieldTypes\Types\DiameterFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    float(0)                                                                
**unit**                                =>    string(5) "metre"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Volume field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    volume                                                                  
description                             =>                                                                            
class                                   =>    App\Forms\FieldTypes\Types\VolumeFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    float(0)                                                                
**unit**                                =>    string(11) "cubic metre"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Program reference field type.
 -------------------------------------- ------ --------------------------------------------------- 
id                                      =>    program_reference                                  
description                             =>    Requires a country field in same form              
class                                   =>    App\Forms\FieldTypes\Types\ProgramReferenceEntity
 -------------------------------------- ------ --------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------- 
required                                =>    Is this field require?                             
multivalued                             =>    Is this field multivalued?                         
weight                                  =>    Field weight                                       
meta                                    =>    Field metadata properties.                         
sort                                    =>    Is this field sortable?                            
filter                                  =>    Is this field filterable?                          
country_field                           =>    The country reference field
 -------------------------------------- ------ --------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------- 
**· value**                             =>    string(0) ""                                       
**class**                               =>    string(30) "App\Entity\ProgramIntervention"
 -------------------------------------- ------ --------------------------------------------------- 

### Functions Carried Out T.A.P. reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------ 
id                                      =>    functions_carried_out_tap_reference                               
description                             =>    Requires a country field in same form                             
class                                   =>    App\Forms\FieldTypes\Types\FunctionsCarriedOutTapReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------ 
required                                =>    Is this field require?                                            
multivalued                             =>    Is this field multivalued?                                        
weight                                  =>    Field weight                                                      
meta                                    =>    Field metadata properties.                                        
sort                                    =>    Is this field sortable?                                           
filter                                  =>    Is this field filterable?                                         
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                      
**class**                               =>    string(33) "App\Entity\FunctionsCarriedOutTap"
 -------------------------------------- ------ ------------------------------------------------------------------ 

### Integer field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    integer                                                                 
description                             =>                                                                            
class                                   =>    App\Forms\FieldTypes\Types\IntegerFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
min                                     =>    Minimum value                                                           
max                                     =>    Maximun value                                                           
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
sort                                    =>    Is this field sortable?                                                 
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    int(0)
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Treatment Technology Sedimentation type reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
id                                      =>    treat_tech_sedimen_reference                                                
description                             =>    Requires a country field in same form                                       
class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologySedimentationReferenceEntity
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                      
multivalued                             =>    Is this field multivalued?                                                  
weight                                  =>    Field weight                                                                
meta                                    =>    Field metadata properties.                                                  
sort                                    =>    Is this field sortable?                                                     
filter                                  =>    Is this field filterable?                                                   
country_field                           =>    The country reference field
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                                
**class**                               =>    string(43) "App\Entity\TreatmentTechnologySedimentation"
 -------------------------------------- ------ ---------------------------------------------------------------------------- 

### Community form record reference field type
 -------------------------------------- ------ -------------------------------------------------------- 
id                                      =>    community_reference                                     
description                             =>                                                            
class                                   =>    App\Forms\FieldTypes\Types\CommunityReferenceFieldType
 -------------------------------------- ------ -------------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------------- 
required                                =>    Is this field require?                                  
multivalued                             =>    Is this field multivalued?                              
weight                                  =>    Field weight                                            
meta                                    =>    Field metadata properties.                              
country_field                           =>    The country reference field                             
sort                                    =>    Is this field sortable?                                 
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ -------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------------- 
**· value**                             =>    int(0)                                                  
**form**                                =>    string(14) "form.community"
 -------------------------------------- ------ -------------------------------------------------------- 

### Geographical scope type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------- 
id                                      =>    geographical_scope_reference                                 
description                             =>    Requires a country field in same form                        
class                                   =>    App\Forms\FieldTypes\Types\GeographicalScopeReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------------------------- 
required                                =>    Is this field require?                                       
multivalued                             =>    Is this field multivalued?                                   
weight                                  =>    Field weight                                                 
meta                                    =>    Field metadata properties.                                   
sort                                    =>    Is this field sortable?                                      
filter                                  =>    Is this field filterable?                                    
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                 
**class**                               =>    string(28) "App\Entity\GeographicalScope"
 -------------------------------------- ------ ------------------------------------------------------------- 

### Publishing status field type
 -------------------------------------- ------ ------------------------------------------------------ 
id                                      =>    publishing_status                                     
description                             =>                                                          
class                                   =>    App\Forms\FieldTypes\Types\PublishingStatusFieldType
 -------------------------------------- ------ ------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------ 
required                                =>    Is this field require?                                
multivalued                             =>    This field type can't be multivalued.                 
weight                                  =>    Field weight                                          
meta                                    =>    Field metadata properties.                            
sort                                    =>    Is this field sortable?                               
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------ 
**· value**                             =>    string(5) "draft"
 -------------------------------------- ------ ------------------------------------------------------ 

### Entity reference field type
 -------------------------------------- ------ ----------------------------------------------------- 
id                                      =>    entity_reference                                     
description                             =>                                                         
class                                   =>    App\Forms\FieldTypes\Types\EntityReferenceFieldType
 -------------------------------------- ------ ----------------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------------- 
required                                =>    Is this field require?                               
multivalued                             =>    Is this field multivalued?                           
weight                                  =>    Field weight                                         
sort                                    =>    Is this field sortable?                              
filter                                  =>    Is this field filterable?                            
meta                                    =>    Field metadata properties.
 -------------------------------------- ------ ----------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------------- 
**· value**                             =>    string(0) ""                                         
**class**                               =>    string(0) ""
 -------------------------------------- ------ ----------------------------------------------------- 

### Administrative division reference field type.
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
id                                      =>    administrative_division_reference                                                                    
description                             =>    Requires a country field in same form. The reference must be a community's administrative division.  
class                                   =>    App\Forms\FieldTypes\Types\AdministrativeDivisionReferenceEntity
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                                               
multivalued                             =>    Is this field multivalued?                                                                           
weight                                  =>    Field weight                                                                                         
meta                                    =>    Field metadata properties.                                                                           
sort                                    =>    Is this field sortable?                                                                              
filter                                  =>    Is this field filterable?                                                                            
country_field                           =>    The country reference field
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                                                         
**class**                               =>    string(33) "App\Entity\AdministrativeDivision"
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 

### Intervention status type reference field type.
 -------------------------------------- ------ -------------------------------------------------------------- 
id                                      =>    intervention_status_reference                                 
description                             =>    Requires a country field in same form                         
class                                   =>    App\Forms\FieldTypes\Types\InterventionStatusReferenceEntity
 -------------------------------------- ------ -------------------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------------------- 
required                                =>    Is this field require?                                        
multivalued                             =>    Is this field multivalued?                                    
weight                                  =>    Field weight                                                  
meta                                    =>    Field metadata properties.                                    
sort                                    =>    Is this field sortable?                                       
filter                                  =>    Is this field filterable?                                     
country_field                           =>    The country reference field
 -------------------------------------- ------ -------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                  
**class**                               =>    string(29) "App\Entity\InterventionStatus"
 -------------------------------------- ------ -------------------------------------------------------------- 

### Special component reference field type.
 -------------------------------------- ------ --------------------------------------------------------------- 
id                                      =>    special_component_reference                                    
description                             =>    Requires a country field in same form                          
class                                   =>    App\Forms\FieldTypes\Types\WsSpecialComponentsReferenceEntity
 -------------------------------------- ------ --------------------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------------------- 
required                                =>    Is this field require?                                         
multivalued                             =>    Is this field multivalued?                                     
weight                                  =>    Field weight                                                   
meta                                    =>    Field metadata properties.                                     
sort                                    =>    Is this field sortable?                                        
filter                                  =>    Is this field filterable?                                      
country_field                           =>    The country reference field
 -------------------------------------- ------ --------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                   
**class**                               =>    string(27) "App\Entity\SpecialComponent"
 -------------------------------------- ------ --------------------------------------------------------------- 

### Pump type reference field type.
 -------------------------------------- ------ ---------------------------------------------------- 
id                                      =>    pump_type_reference                                 
description                             =>    Requires a country field in same form               
class                                   =>    App\Forms\FieldTypes\Types\PumpTypeReferenceEntity
 -------------------------------------- ------ ---------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------- 
required                                =>    Is this field require?                              
multivalued                             =>    Is this field multivalued?                          
weight                                  =>    Field weight                                        
meta                                    =>    Field metadata properties.                          
sort                                    =>    Is this field sortable?                             
filter                                  =>    Is this field filterable?                           
country_field                           =>    The country reference field
 -------------------------------------- ------ ---------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------- 
**· value**                             =>    string(0) ""                                        
**class**                               =>    string(19) "App\Entity\PumpType"
 -------------------------------------- ------ ---------------------------------------------------- 

### Predominant diameter reference field type.
 -------------------------------------- ------ --------------------------------------------------------- 
id                                      =>    default_diameter_reference                               
description                             =>    Requires a country field in same form                    
class                                   =>    App\Forms\FieldTypes\Types\PredominantDiameterFieldType
 -------------------------------------- ------ --------------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------------- 
required                                =>    Is this field require?                                   
multivalued                             =>    Is this field multivalued?                               
weight                                  =>    Field weight                                             
meta                                    =>    Field metadata properties.                               
sort                                    =>    Is this field sortable?                                  
filter                                  =>    Is this field filterable?                                
country_field                           =>    The country reference field
 -------------------------------------- ------ --------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------------- 
**· value**                             =>    string(0) ""                                             
**class**                               =>    string(26) "App\Entity\DefaultDiameter"
 -------------------------------------- ------ --------------------------------------------------------- 

### Ethnicity type reference field type.
 -------------------------------------- ------ ----------------------------------------------------- 
id                                      =>    ethnicity_reference                                  
description                             =>    Requires a country field in same form                
class                                   =>    App\Forms\FieldTypes\Types\EthnicityReferenceEntity
 -------------------------------------- ------ ----------------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------------- 
required                                =>    Is this field require?                               
multivalued                             =>    Is this field multivalued?                           
weight                                  =>    Field weight                                         
meta                                    =>    Field metadata properties.                           
sort                                    =>    Is this field sortable?                              
filter                                  =>    Is this field filterable?                            
country_field                           =>    The country reference field
 -------------------------------------- ------ ----------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------------- 
**· value**                             =>    string(0) ""                                         
**class**                               =>    string(20) "App\Entity\Ethnicity"
 -------------------------------------- ------ ----------------------------------------------------- 

### Select field type
 -------------------------------------- ------ ---------------------------------------------------------- 
id                                      =>    select                                                    
description                             =>    We can add option groups using keys that start with '_'.  
class                                   =>    App\Forms\FieldTypes\Types\SelectFieldType
 -------------------------------------- ------ ---------------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------------- 
options                                 =>    Allowed options                                           
required                                =>    Is this field require?                                    
multivalued                             =>    Is this field multivalued?                                
weight                                  =>    Field weight                                              
meta                                    =>    Field metadata properties.                                
sort                                    =>    Is this field sortable?                                   
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ---------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------------- 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ---------------------------------------------------------- 

### File reference field type
 -------------------------------------- ------ -------------------------------------------------------- 
id                                      =>    file_reference                                          
description                             =>                                                            
class                                   =>    App\Forms\FieldTypes\Types\FileReferenceEntity
 -------------------------------------- ------ -------------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------------- 
required                                =>    Is this field require?                                  
multivalued                             =>    Is this field multivalued?                              
weight                                  =>    Field weight                                            
meta                                    =>    Field metadata properties.                              
sort                                    =>    Is this field sortable?                                 
filter                                  =>    Is this field filterable?                               
allow_extension                         =>    Comma separated list of valid extensions                
maximum_file_size                       =>    Maximum file size, by default, use the server settings
 -------------------------------------- ------ -------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------------- 
**· value**                             =>    string(0) ""                                            
**class**                               =>    string(15) "App\Entity\File"                            
**filename**                            =>    string(0) ""
 -------------------------------------- ------ -------------------------------------------------------- 

### Treatment Technology Desalination type reference field type.
 -------------------------------------- ------ --------------------------------------------------------------------------- 
id                                      =>    treat_tech_desali_reference                                                
description                             =>    Requires a country field in same form                                      
class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyDesalinationReferenceEntity
 -------------------------------------- ------ --------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                     
multivalued                             =>    Is this field multivalued?                                                 
weight                                  =>    Field weight                                                               
meta                                    =>    Field metadata properties.                                                 
sort                                    =>    Is this field sortable?                                                    
filter                                  =>    Is this field filterable?                                                  
country_field                           =>    The country reference field
 -------------------------------------- ------ --------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                               
**class**                               =>    string(42) "App\Entity\TreatmentTechnologyDesalination"
 -------------------------------------- ------ --------------------------------------------------------------------------- 

### Long text field type
 -------------------------------------- ------ ---------------------------------------------- 
id                                      =>    long_text                                     
description                             =>                                                  
class                                   =>    App\Forms\FieldTypes\Types\LongTextFieldType
 -------------------------------------- ------ ---------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------- 
required                                =>    Is this field require?                        
multivalued                             =>    Is this field multivalued?                    
weight                                  =>    Field weight                                  
meta                                    =>    Field metadata properties.                    
sort                                    =>    Is this field sortable?                       
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ---------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------- 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ---------------------------------------------- 

### Short text field type
 -------------------------------------- ------ ----------------------------------------------- 
id                                      =>    short_text                                     
description                             =>                                                   
class                                   =>    App\Forms\FieldTypes\Types\ShortTextFieldType
 -------------------------------------- ------ ----------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------- 
max-length                              =>    Maximun data length                            
required                                =>    Is this field require?                         
multivalued                             =>    Is this field multivalued?                     
weight                                  =>    Field weight                                   
meta                                    =>    Field metadata properties.                     
sort                                    =>    Is this field sortable?                        
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ----------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------- 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ----------------------------------------------- 

### Community Service reference field type.
 -------------------------------------- ------ ------------------------------------------------------------ 
id                                      =>    community_service_reference                                 
description                             =>    Requires a country field in same form                       
class                                   =>    App\Forms\FieldTypes\Types\CommunityServiceReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------ 
required                                =>    Is this field require?                                      
multivalued                             =>    Is this field multivalued?                                  
weight                                  =>    Field weight                                                
meta                                    =>    Field metadata properties.                                  
sort                                    =>    Is this field sortable?                                     
filter                                  =>    Is this field filterable?                                   
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                
**class**                               =>    string(27) "App\Entity\CommunityService"
 -------------------------------------- ------ ------------------------------------------------------------ 

### Currency reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    currency                                                                
description                             =>    Requires a country field in same form                                   
class                                   =>    App\Forms\FieldTypes\Types\CurrencyFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
sort                                    =>    Is this field sortable?                                                 
filter                                  =>    Is this field filterable?                                               
country_field                           =>    The country reference field                                             
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                            
**class**                               =>    string(19) "App\Entity\Currency"                                        
**amount**                              =>    int(0)
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Month and year field type
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
id                                      =>    month_year                                                                                    
description                             =>    Month are from 1, that is January, to 12, that is December. The years are from 1900 to 9999.  
class                                   =>    App\Forms\FieldTypes\Types\MonthYearFieldType
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                                        
multivalued                             =>    Is this field multivalued?                                                                    
weight                                  =>    Field weight                                                                                  
meta                                    =>    Field metadata properties.
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
**month**                               =>    string(0) ""                                                                                  
**year**                                =>    string(0) ""
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 

### Treatment Technology Filtration type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------- 
id                                      =>    treat_tech_filtra_reference                                              
description                             =>    Requires a country field in same form                                    
class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyFiltrationReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                   
multivalued                             =>    Is this field multivalue?                                                
weight                                  =>    Field weight                                                             
meta                                    =>    Field metadata properties.                                               
sort                                    =>    Is this field sortable?                                                  
filter                                  =>    Is this field filterable?                                                
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                             
**class**                               =>    string(40) "App\Entity\TreatmentTechnologyFiltration"
 -------------------------------------- ------ ------------------------------------------------------------------------- 

### Funder intervention reference field type.
 -------------------------------------- ------ -------------------------------------------------------------- 
id                                      =>    funder_intervention_reference                                 
description                             =>    Requires a country field in same form                         
class                                   =>    App\Forms\FieldTypes\Types\FunderInterventionReferenceEntity
 -------------------------------------- ------ -------------------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------------------- 
required                                =>    Is this field require?                                        
multivalued                             =>    Is this field multivalued?                                    
weight                                  =>    Field weight                                                  
meta                                    =>    Field metadata properties.                                    
sort                                    =>    Is this field sortable?                                       
filter                                  =>    Is this field filterable?                                     
country_field                           =>    The country reference field
 -------------------------------------- ------ -------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                  
**class**                               =>    string(29) "App\Entity\FunderIntervention"
 -------------------------------------- ------ -------------------------------------------------------------- 

### Treatment Technology Aera-Oxida type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    treat_tech_aera_oxida_reference                                         
description                             =>    Requires a country field in same form                                   
class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyAeraOxidaReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
sort                                    =>    Is this field sortable?                                                 
filter                                  =>    Is this field filterable?                                               
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                            
**class**                               =>    string(39) "App\Entity\TreatmentTechnologyAeraOxida"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Language reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
id                                      =>    language_reference                                                                
description                             =>    Requires a country field in same form. Reference to App\Entity\OfficialLanguage.  
class                                   =>    App\Forms\FieldTypes\Types\LanguageReferenceEntity
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                            
multivalued                             =>    Is this field multivalued?                                                        
weight                                  =>    Field weight                                                                      
meta                                    =>    Field metadata properties.                                                        
sort                                    =>    Is this field sortable?                                                           
filter                                  =>    Is this field filterable?                                                         
country_field                           =>    The country reference field
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                                      
**class**                               =>    string(27) "App\Entity\OfficialLanguage"
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 

### Map point field type
 -------------------------------------- ------ ---------------------------------------------- 
id                                      =>    map_point                                     
description                             =>                                                  
class                                   =>    App\Forms\FieldTypes\Types\MapPointFieldType
 -------------------------------------- ------ ---------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------- 
required                                =>    Is this field require?                        
multivalued                             =>    This field type can't be multivalued.         
weight                                  =>    Field weight                                  
meta                                    =>    Field metadata properties.                    
sort                                    =>    Is this field sortable?                       
filter                                  =>    Is this field filterable?                     
lat_field                               =>    The Latitude field                            
lon_field                               =>    The Longitude field
 -------------------------------------- ------ ---------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------- 
**· lat**                               =>    int(0)                                        
**lon**                                 =>    int(0)
 -------------------------------------- ------ ---------------------------------------------- 

### Type of T.A.P. type reference field type.
 -------------------------------------- ------ --------------------------------------------------- 
id                                      =>    type_tap_reference                                 
description                             =>    Requires a country field in same form              
class                                   =>    App\Forms\FieldTypes\Types\TypeTapReferenceEntity
 -------------------------------------- ------ --------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------- 
required                                =>    Is this field require?                             
multivalued                             =>    Is this field multivalued?                         
weight                                  =>    Field weight                                       
meta                                    =>    Field metadata properties.                         
sort                                    =>    Is this field sortable?                            
filter                                  =>    Is this field filterable?                          
country_field                           =>    The country reference field
 -------------------------------------- ------ --------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------- 
**· value**                             =>    string(0) ""                                       
**class**                               =>    string(18) "App\Entity\TypeTap"
 -------------------------------------- ------ --------------------------------------------------- 

### Pointing status field type
 -------------------------------------- ------ ---------------------------------------------------- 
id                                      =>    pointing_status                                     
description                             =>                                                        
class                                   =>    App\Forms\FieldTypes\Types\PointingStatusFieldType
 -------------------------------------- ------ ---------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------- 
required                                =>    Is this field require?                              
multivalued                             =>    This field type can't be multivalued.               
weight                                  =>    Field weight                                        
meta                                    =>    Field metadata properties.                          
sort                                    =>    Is this field sortable?                             
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ---------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------- 
**· value**                             =>    string(8) "planning"
 -------------------------------------- ------ ---------------------------------------------------- 

### Date field type
 -------------------------------------- ------ ------------------------------------------- 
id                                      =>    date                                       
description                             =>                                               
class                                   =>    App\Forms\FieldTypes\Types\DateFieldType
 -------------------------------------- ------ ------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------- 
required                                =>    Is this field require?                     
multivalued                             =>    Is this field multivalued?                 
weight                                  =>    Field weight                               
meta                                    =>    Field metadata properties.                 
sort                                    =>    Is this field sortable?                    
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------- 
**· value**                             =>    object(DateTime)#1585 (3) {                
["date"]=>                               
string(26) "2023-03-15 13:40:55.137870"  
["timezone_type"]=>                      
int(3)                                   
["timezone"]=>                           
string(3) "UTC"                          
}                                          
**timezone**                            =>    object(DateTimeZone)#1557 (2) {            
["timezone_type"]=>                      
int(3)                                   
["timezone"]=>                           
string(3) "UTC"                          
}
 -------------------------------------- ------ ------------------------------------------- 

### Water Quality Intervention Type type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    water_quality_inter_tp_reference                                        
description                             =>    Requires a country field in same form                                   
class                                   =>    App\Forms\FieldTypes\Types\WaterQualityInterventionTypeReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
sort                                    =>    Is this field sortable?                                                 
filter                                  =>    Is this field filterable?                                               
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                            
**class**                               =>    string(39) "App\Entity\WaterQualityInterventionType"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Boolean field type with radio buttons
 -------------------------------------- ------ -------------------------------------------------- 
id                                      =>    radio_boolean                                     
description                             =>                                                      
class                                   =>    App\Forms\FieldTypes\Types\RadioBooleanFieldType
 -------------------------------------- ------ -------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------- 
required                                =>    Is this field require?                            
multivalued                             =>    Is this field multivalued?                        
weight                                  =>    Field weight                                      
meta                                    =>    Field metadata properties.                        
sort                                    =>    Is this field sortable?                           
filter                                  =>    Is this field filterable?                         
true_label                              =>    Label to show TRUE value                          
false_label                             =>    Label to show FALSE value                         
show_labels                             =>    Must this field show value labels?
 -------------------------------------- ------ -------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------- 
**· value**                             =>    bool(false)
 -------------------------------------- ------ -------------------------------------------------- 

### Mail response record reference field type
 -------------------------------------- ------ ------------------------------------------------ 
id                                      =>    thread_mail_reference                           
description                             =>                                                    
class                                   =>    App\Forms\FieldTypes\Types\ThreadMailFieldType
 -------------------------------------- ------ ------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------ 
required                                =>    Is this field require?                          
multivalued                             =>    Is this field multivalued?                      
weight                                  =>    Field weight                                    
meta                                    =>    Field metadata properties.                      
country_field                           =>    The country reference field                     
subform                                 =>    The subform id
 -------------------------------------- ------ ------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------ 
**· value**                             =>    int(0)                                          
**form**                                =>    string(0) ""
 -------------------------------------- ------ ------------------------------------------------ 

### Water Quality Entity type reference field type.
 -------------------------------------- ------ -------------------------------------------------------------- 
id                                      =>    water_quality_entity_reference                                
description                             =>    Requires a country field in same form                         
class                                   =>    App\Forms\FieldTypes\Types\WaterQualityEntityReferenceEntity
 -------------------------------------- ------ -------------------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------------------- 
required                                =>    Is this field require?                                        
multivalued                             =>    Is this field multivalued?                                    
weight                                  =>    Field weight                                                  
meta                                    =>    Field metadata properties.                                    
sort                                    =>    Is this field sortable?                                       
filter                                  =>    Is this field filterable?                                     
country_field                           =>    The country reference field
 -------------------------------------- ------ -------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                  
**class**                               =>    string(29) "App\Entity\WaterQualityEntity"
 -------------------------------------- ------ -------------------------------------------------------------- 

### Mutateble field type
 -------------------------------------- ------ ----------------------------------------------- 
id                                      =>    mutateble                                      
description                             =>                                                   
class                                   =>    App\Forms\FieldTypes\Types\MutatebleFieldType
 -------------------------------------- ------ ----------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------- 
required                                =>    Is this field require?                         
multivalued                             =>    Is this field multivalued?                     
weight                                  =>    Field weight                                   
meta                                    =>    Field metadata properties.                     
sort                                    =>    Is this field sortable?                        
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ----------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------- 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ----------------------------------------------- 

### Mail field type
 -------------------------------------- ------ ------------------------------------------ 
id                                      =>    mail                                      
description                             =>                                              
class                                   =>    App\Forms\FieldTypes\Types\MailFieldType
 -------------------------------------- ------ ------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------ 
required                                =>    Is this field require?                    
multivalued                             =>    Is this field multivalued?                
weight                                  =>    Field weight                              
meta                                    =>    Field metadata properties.                
sort                                    =>    Is this field sortable?                   
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------ 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ------------------------------------------ 

### Flow field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    flow                                                                    
description                             =>                                                                            
class                                   =>    App\Forms\FieldTypes\Types\FlowFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    float(0)                                                                
**unit**                                =>    string(12) "liter/second"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Sub form record reference field type
 -------------------------------------- ------ --------------------------------------------- 
id                                      =>    subform_reference                            
description                             =>                                                 
class                                   =>    App\Forms\FieldTypes\Types\SubformFieldType
 -------------------------------------- ------ --------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------- 
required                                =>    Is this field require?                       
multivalued                             =>    Is this field multivalued?                   
weight                                  =>    Field weight                                 
meta                                    =>    Field metadata properties.                   
country_field                           =>    The country reference field                  
subform                                 =>    The subform id
 -------------------------------------- ------ --------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------- 
**· value**                             =>    int(0)                                       
**form**                                =>    string(0) ""
 -------------------------------------- ------ --------------------------------------------- 

### Radio/Checkbox field type
 -------------------------------------- ------ ------------------------------------------------- 
id                                      =>    radio_select                                     
description                             =>                                                     
class                                   =>    App\Forms\FieldTypes\Types\RadioSelectFieldType
 -------------------------------------- ------ ------------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------------- 
options                                 =>    Allowed options                                  
required                                =>    Is this field require?                           
multivalued                             =>    Is this field multivalued?                       
weight                                  =>    Field weight                                     
meta                                    =>    Field metadata properties.                       
sort                                    =>    Is this field sortable?                          
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------- 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ------------------------------------------------- 

### Decimal field type
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
id                                      =>    decimal                                                                                                                             
description                             =>                                                                                                                                        
class                                   =>    App\Forms\FieldTypes\Types\DecimalFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
min                                     =>    Minimum value                                                                                                                       
max                                     =>    Maximun value                                                                                                                       
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.                                                              
precision                               =>    The maximum number of digits (the precision). It has a range of 1 to 65.                                                            
scale                                   =>    The number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than precision  
required                                =>    Is this field require?                                                                                                              
multivalued                             =>    Is this field multivalued?                                                                                                          
weight                                  =>    Field weight                                                                                                                        
meta                                    =>    Field metadata properties.                                                                                                          
sort                                    =>    Is this field sortable?                                                                                                             
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
**· value**                             =>    int(0)
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 

### Telephone field type
 -------------------------------------- ------ ------------------------------------------- 
id                                      =>    phone                                      
description                             =>                                               
class                                   =>    App\Forms\FieldTypes\Types\PhoneFieldType
 -------------------------------------- ------ ------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------- 
required                                =>    Is this field require?                     
multivalued                             =>    Is this field multivalued?                 
weight                                  =>    Field weight                               
meta                                    =>    Field metadata properties.                 
sort                                    =>    Is this field sortable?                    
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------- 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ------------------------------------------- 

### Type intervention reference field type.
 -------------------------------------- ------ ------------------------------------------------------------ 
id                                      =>    type_intervention_reference                                 
description                             =>    Requires a country field in same form                       
class                                   =>    App\Forms\FieldTypes\Types\TypeInterventionReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------ 
required                                =>    Is this field require?                                      
multivalued                             =>    Is this field multivalued?                                  
weight                                  =>    Field weight                                                
meta                                    =>    Field metadata properties.                                  
sort                                    =>    Is this field sortable?                                     
filter                                  =>    Is this field filterable?                                   
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------ 
**· value**                             =>    string(0) ""                                                
**class**                               =>    string(27) "App\Entity\TypeIntervention"
 -------------------------------------- ------ ------------------------------------------------------------ 

### Material to distribution infrastructure reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------- 
id                                      =>    distribution_material_reference                                 
description                             =>    Requires a country field in same form                           
class                                   =>    App\Forms\FieldTypes\Types\DistributionMaterialReferenceEntity
 -------------------------------------- ------ ---------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------------------- 
required                                =>    Is this field require?                                          
multivalued                             =>    Is this field multivalued?                                      
weight                                  =>    Field weight                                                    
meta                                    =>    Field metadata properties.                                      
sort                                    =>    Is this field sortable?                                         
filter                                  =>    Is this field filterable?                                       
country_field                           =>    The country reference field
 -------------------------------------- ------ ---------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                    
**class**                               =>    string(31) "App\Entity\DistributionMaterial"
 -------------------------------------- ------ ---------------------------------------------------------------- 

### Institution intervention reference field type.
 -------------------------------------- ------ ------------------------------------------------------- 
id                                      =>    institution_reference                                  
description                             =>    Requires a country field in same form                  
class                                   =>    App\Forms\FieldTypes\Types\InstitutionReferenceEntity
 -------------------------------------- ------ ------------------------------------------------------- 
Settings
 -------------------------------------- ------ ------------------------------------------------------- 
required                                =>    Is this field require?                                 
multivalued                             =>    Is this field multivalued?                             
weight                                  =>    Field weight                                           
meta                                    =>    Field metadata properties.                             
sort                                    =>    Is this field sortable?                                
filter                                  =>    Is this field filterable?                              
country_field                           =>    The country reference field
 -------------------------------------- ------ ------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------- 
**· value**                             =>    string(0) ""                                           
**class**                               =>    string(34) "App\Entity\InstitutionIntervention"
 -------------------------------------- ------ ------------------------------------------------------- 

### Boolean field type
 -------------------------------------- ------ --------------------------------------------- 
id                                      =>    boolean                                      
description                             =>                                                 
class                                   =>    App\Forms\FieldTypes\Types\BooleanFieldType
 -------------------------------------- ------ --------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------- 
required                                =>    Is this field require?                       
multivalued                             =>    Is this field multivalued?                   
weight                                  =>    Field weight                                 
meta                                    =>    Field metadata properties.                   
sort                                    =>    Is this field sortable?                      
filter                                  =>    Is this field filterable?                    
true_label                              =>    Label to show TRUE value                     
false_label                             =>    Label to show FALSE value                    
show_labels                             =>    Must this field show value labels?
 -------------------------------------- ------ --------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------- 
**· value**                             =>    bool(false)
 -------------------------------------- ------ --------------------------------------------- 

### Inquiring status field type
 -------------------------------------- ------ ----------------------------------------------------- 
id                                      =>    inquiring_status                                     
description                             =>                                                         
class                                   =>    App\Forms\FieldTypes\Types\InquiringStatusFieldType
 -------------------------------------- ------ ----------------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------------- 
required                                =>    Is this field require?                               
multivalued                             =>    This field type can't be multivalued.                
weight                                  =>    Field weight                                         
meta                                    =>    Field metadata properties.                           
sort                                    =>    Is this field sortable?                              
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ----------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------------- 
**· value**                             =>    string(5) "draft"
 -------------------------------------- ------ ----------------------------------------------------- 

### Point form record reference field type
 -------------------------------------- ------ ---------------------------------------------------- 
id                                      =>    point_reference                                     
description                             =>                                                        
class                                   =>    App\Forms\FieldTypes\Types\PointReferenceFieldType
 -------------------------------------- ------ ---------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------- 
required                                =>    Is this field require?                              
multivalued                             =>    Is this field multivalued?                          
weight                                  =>    Field weight                                        
meta                                    =>    Field metadata properties.                          
country_field                           =>    The country reference field                         
sort                                    =>    Is this field sortable?                             
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ---------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------- 
**· value**                             =>    int(0)                                              
**form**                                =>    string(10) "form.point"
 -------------------------------------- ------ ---------------------------------------------------- 

### Technical assistance provider reference field type.
 -------------------------------------- ------ ----------------------------------------------------- 
id                                      =>    tap_reference                                        
description                             =>    Requires a country field in same form                
class                                   =>    App\Forms\FieldTypes\Types\TapReferenceEntity
 -------------------------------------- ------ ----------------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------------- 
required                                =>    Is this field require?                               
multivalued                             =>    Is this field multivalued?                           
weight                                  =>    Field weight                                         
meta                                    =>    Field metadata properties.                           
sort                                    =>    Is this field sortable?                              
filter                                  =>    Is this field filterable?                            
country_field                           =>    The country reference field
 -------------------------------------- ------ ----------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------------- 
**· value**                             =>    string(0) ""                                         
**class**                               =>    string(38) "App\Entity\TechnicalAssistanceProvider"
 -------------------------------------- ------ ----------------------------------------------------- 

### Material to storage infrastructure reference field type.
 -------------------------------------- ------ ---------------------------------------------------- 
id                                      =>    material_reference                                  
description                             =>    Requires a country field in same form               
class                                   =>    App\Forms\FieldTypes\Types\MaterialReferenceEntity
 -------------------------------------- ------ ---------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------- 
required                                =>    Is this field require?                              
multivalued                             =>    Is this field multivalued?                          
weight                                  =>    Field weight                                        
meta                                    =>    Field metadata properties.                          
sort                                    =>    Is this field sortable?                             
filter                                  =>    Is this field filterable?                           
country_field                           =>    The country reference field
 -------------------------------------- ------ ---------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------- 
**· value**                             =>    string(0) ""                                        
**class**                               =>    string(19) "App\Entity\Material"
 -------------------------------------- ------ ---------------------------------------------------- 

### Editor + Update field type
 -------------------------------------- ------ -------------------------------------------------- 
id                                      =>    editor_update                                     
description                             =>                                                      
class                                   =>    App\Forms\FieldTypes\Types\EditorUpdateFieldType
 -------------------------------------- ------ -------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------- 
required                                =>    Is this field require?                            
multivalued                             =>    This field type can't be multivalued.             
weight                                  =>    Field weight                                      
meta                                    =>    Field metadata properties.                        
sort                                    =>    Is this field sortable?                           
filter                                  =>    Is this field filterable?                         
editor_field                            =>    The editor reference field                        
update_field                            =>    The update reference field
 -------------------------------------- ------ -------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------- 
**· value**                             =>    string(0) ""                                      
**value1**                              =>    string(0) ""
 -------------------------------------- ------ -------------------------------------------------- 

### Length field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    length                                                                  
description                             =>                                                                            
class                                   =>    App\Forms\FieldTypes\Types\LengthFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    float(0)                                                                
**unit**                                =>    string(5) "metre"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### User reference field type
 -------------------------------------- ------ ------------------------------------------------ 
id                                      =>    user_reference                                  
description                             =>                                                    
class                                   =>    App\Forms\FieldTypes\Types\UserReferenceEntity
 -------------------------------------- ------ ------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------ 
required                                =>    Is this field require?                          
multivalued                             =>    Is this field multivalued?                      
weight                                  =>    Field weight                                    
meta                                    =>    Field metadata properties.                      
sort                                    =>    Is this field sortable?                         
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------ 
**· value**                             =>    string(0) ""                                    
**class**                               =>    string(15) "App\Entity\User"
 -------------------------------------- ------ ------------------------------------------------ 

### Country reference field type
 -------------------------------------- ------ --------------------------------------------------- 
id                                      =>    country_reference                                  
description                             =>                                                       
class                                   =>    App\Forms\FieldTypes\Types\CountryReferenceEntity
 -------------------------------------- ------ --------------------------------------------------- 
Settings
 -------------------------------------- ------ --------------------------------------------------- 
required                                =>    Is this field require?                             
multivalued                             =>    Is this field multivalued?                         
weight                                  =>    Field weight                                       
meta                                    =>    Field metadata properties.                         
sort                                    =>    Is this field sortable?                            
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ --------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ --------------------------------------------------- 
**· value**                             =>    string(0) ""                                       
**class**                               =>    string(18) "App\Entity\Country"
 -------------------------------------- ------ --------------------------------------------------- 

### ULID field type. If required and empty generate a new ULID.
 -------------------------------------- ------ ------------------------------------------ 
id                                      =>    ulid                                      
description                             =>                                              
class                                   =>    App\Forms\FieldTypes\Types\UlidFieldType
 -------------------------------------- ------ ------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------ 
required                                =>    Is this field require?                    
multivalued                             =>    Is this field multivalued?                
weight                                  =>    Field weight                              
meta                                    =>    Field metadata properties.                
sort                                    =>    Is this field sortable?                   
filter                                  =>    Is this field filterable?
 -------------------------------------- ------ ------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------ 
**· value**                             =>    string(0) ""
 -------------------------------------- ------ ------------------------------------------ 

### Concentration field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
id                                      =>    concentration                                                           
description                             =>                                                                            
class                                   =>    App\Forms\FieldTypes\Types\ConcentrationFieldType
 -------------------------------------- ------ ------------------------------------------------------------------------ 
Settings
 -------------------------------------- ------ ------------------------------------------------------------------------ 
required                                =>    Is this field require?                                                  
multivalued                             =>    Is this field multivalued?                                              
weight                                  =>    Field weight                                                            
meta                                    =>    Field metadata properties.                                              
unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
[· = Main] Property => default value
 -------------------------------------- ------ ------------------------------------------------------------------------ 
**· value**                             =>    float(0)                                                                
**unit**                                =>    string(21) "particles per million"
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Typology Chlorination Installation type reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
id                                      =>    typo_chlori_install_reference                                               
description                             =>    Requires a country field in same form                                       
class                                   =>    App\Forms\FieldTypes\Types\TypologyChlorinationInstallationReferenceEntity
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
required                                =>    Is this field require?                                                      
multivalued                             =>    Is this field multivalued?                                                  
weight                                  =>    Field weight                                                                
meta                                    =>    Field metadata properties.                                                  
sort                                    =>    Is this field sortable?                                                     
filter                                  =>    Is this field filterable?                                                   
country_field                           =>    The country reference field
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                                
**class**                               =>    string(43) "App\Entity\TypologyChlorinationInstallation"
 -------------------------------------- ------ ---------------------------------------------------------------------------- 

### File image reference field type
 -------------------------------------- ------ -------------------------------------------------------- 
id                                      =>    image_reference                                         
description                             =>                                                            
class                                   =>    App\Forms\FieldTypes\Types\ImageFileReferenceEntity
 -------------------------------------- ------ -------------------------------------------------------- 
Settings
 -------------------------------------- ------ -------------------------------------------------------- 
required                                =>    Is this field require?                                  
multivalued                             =>    Is this field multivalued?                              
weight                                  =>    Field weight                                            
meta                                    =>    Field metadata properties.                              
sort                                    =>    Is this field sortable?                                 
filter                                  =>    Is this field filterable?                               
allow_extension                         =>    Comma separated list of valid extensions                
maximum_file_size                       =>    Maximum file size, by default, use the server settings
 -------------------------------------- ------ -------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ -------------------------------------------------------- 
**· value**                             =>    string(0) ""                                            
**class**                               =>    string(15) "App\Entity\File"                            
**filename**                            =>    string(0) ""
 -------------------------------------- ------ -------------------------------------------------------- 

### Disinfecting Substance type reference field type.
 -------------------------------------- ------ ----------------------------------------------------------------- 
id                                      =>    disinfect_substance_reference                                    
description                             =>    Requires a country field in same form                            
class                                   =>    App\Forms\FieldTypes\Types\DisinfectingSubstanceReferenceEntity
 -------------------------------------- ------ ----------------------------------------------------------------- 
Settings
 -------------------------------------- ------ ----------------------------------------------------------------- 
required                                =>    Is this field require?                                           
multivalued                             =>    Is this field multivalued?                                       
weight                                  =>    Field weight                                                     
meta                                    =>    Field metadata properties.                                       
sort                                    =>    Is this field sortable?                                          
filter                                  =>    Is this field filterable?                                        
country_field                           =>    The country reference field
 -------------------------------------- ------ ----------------------------------------------------------------- 
[· = Main] Property => default value
 -------------------------------------- ------ ----------------------------------------------------------------- 
**· value**                             =>    string(0) ""                                                     
**class**                               =>    string(32) "App\Entity\DisinfectingSubstance"
 -------------------------------------- ------ -----------------------------------------------------------------

# Development and Staging environments

https://visitapi-dev.siasar.org

https://visitapi-stg.siasar.org