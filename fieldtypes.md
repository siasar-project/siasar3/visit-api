### Water system form record reference field type
 -------------------------------------- ------ ------------------------------------------------------ 
  id                                      =>    wsystem_reference                                     
  description                             =>                                                          
  class                                   =>    App\Forms\FieldTypes\Types\WSystemReferenceFieldType  
 -------------------------------------- ------ ------------------------------------------------------ 
  Settings                                                                                            
 -------------------------------------- ------ ------------------------------------------------------ 
  required                                =>    Is this field require?                                
  multivalued                             =>    Is this field multivalued?                            
  weight                                  =>    Field weight                                          
  meta                                    =>    Field metadata properties.                            
  country_field                           =>    The country reference field                           
  sort                                    =>    Is this field sortable?                               
  filter                                  =>    Is this field filterable?                             
 -------------------------------------- ------ ------------------------------------------------------ 
  [· = Main] Property => default value                                                                
 -------------------------------------- ------ ------------------------------------------------------ 
  **· value**                             =>    int(0)                                                
  **form**                                =>    string(13) "form.wssystem"                            
 -------------------------------------- ------ ------------------------------------------------------ 

### Type of health care facility type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------ 
  id                                      =>    type_health_facility_reference                                    
  description                             =>    Requires a country field in same form                             
  class                                   =>    App\Forms\FieldTypes\Types\TypeHealthcareFacilityReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------ 
  Settings                                                                                                        
 -------------------------------------- ------ ------------------------------------------------------------------ 
  required                                =>    Is this field require?                                            
  multivalued                             =>    Is this field multivalued?                                        
  weight                                  =>    Field weight                                                      
  meta                                    =>    Field metadata properties.                                        
  sort                                    =>    Is this field sortable?                                           
  filter                                  =>    Is this field filterable?                                         
  country_field                           =>    The country reference field                                       
 -------------------------------------- ------ ------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                            
 -------------------------------------- ------ ------------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                      
  **class**                               =>    string(33) "App\Entity\TypeHealthcareFacility"                    
 -------------------------------------- ------ ------------------------------------------------------------------ 

### Water service provider form record reference field type
 -------------------------------------- ------ --------------------------------------------------------- 
  id                                      =>    wsprovider_reference                                     
  description                             =>                                                             
  class                                   =>    App\Forms\FieldTypes\Types\WSProviderReferenceFieldType  
 -------------------------------------- ------ --------------------------------------------------------- 
  Settings                                                                                               
 -------------------------------------- ------ --------------------------------------------------------- 
  required                                =>    Is this field require?                                   
  multivalued                             =>    Is this field multivalued?                               
  weight                                  =>    Field weight                                             
  meta                                    =>    Field metadata properties.                               
  country_field                           =>    The country reference field                              
  sort                                    =>    Is this field sortable?                                  
  filter                                  =>    Is this field filterable?                                
 -------------------------------------- ------ --------------------------------------------------------- 
  [· = Main] Property => default value                                                                   
 -------------------------------------- ------ --------------------------------------------------------- 
  **· value**                             =>    int(0)                                                   
  **form**                                =>    string(15) "form.wsprovider"                             
 -------------------------------------- ------ --------------------------------------------------------- 

### Functions Carried Out W.S.P. reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------ 
  id                                      =>    functions_carried_out_wsp_reference                               
  description                             =>    Requires a country field in same form                             
  class                                   =>    App\Forms\FieldTypes\Types\FunctionsCarriedOutWspReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------ 
  Settings                                                                                                        
 -------------------------------------- ------ ------------------------------------------------------------------ 
  required                                =>    Is this field require?                                            
  multivalued                             =>    Is this field multivalued?                                        
  weight                                  =>    Field weight                                                      
  meta                                    =>    Field metadata properties.                                        
  sort                                    =>    Is this field sortable?                                           
  filter                                  =>    Is this field filterable?                                         
  country_field                           =>    The country reference field                                       
 -------------------------------------- ------ ------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                            
 -------------------------------------- ------ ------------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                      
  **class**                               =>    string(33) "App\Entity\FunctionsCarriedOutWsp"                    
 -------------------------------------- ------ ------------------------------------------------------------------ 

### Treatment Technology Coag-Floccu type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  id                                      =>    treat_tech_coag_floccu_reference                                         
  description                             =>    Requires a country field in same form                                    
  class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyCoagFloccuReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  Settings                                                                                                               
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                   
  multivalued                             =>    Is this field multivalued?                                               
  weight                                  =>    Field weight                                                             
  meta                                    =>    Field metadata properties.                                               
  sort                                    =>    Is this field sortable?                                                  
  filter                                  =>    Is this field filterable?                                                
  country_field                           =>    The country reference field                                              
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                   
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                             
  **class**                               =>    string(40) "App\Entity\TreatmentTechnologyCoagFloccu"                    
 -------------------------------------- ------ ------------------------------------------------------------------------- 

### Household Process type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------ 
  id                                      =>    household_process_reference                                 
  description                             =>    Requires a country field in same form                       
  class                                   =>    App\Forms\FieldTypes\Types\HouseholdProcessReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------ 
  Settings                                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------ 
  required                                =>    Is this field require?                                      
  multivalued                             =>    Is this field multivalued?                                  
  weight                                  =>    Field weight                                                
  meta                                    =>    Field metadata properties.                                  
  sort                                    =>    Is this field sortable?                                     
  filter                                  =>    Is this field filterable?                                   
  country_field                           =>    The country reference field                                 
 -------------------------------------- ------ ------------------------------------------------------------ 
  [· = Main] Property => default value                                                                      
 -------------------------------------- ------ ------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                
  **class**                               =>    string(27) "App\Entity\HouseholdProcess"                    
 -------------------------------------- ------ ------------------------------------------------------------ 

### Form record reference field type
 -------------------------------------- ------ --------------------------------------------------------- 
  id                                      =>    form_record_reference                                    
  description                             =>                                                             
  class                                   =>    App\Forms\FieldTypes\Types\FormRecordReferenceFieldType  
 -------------------------------------- ------ --------------------------------------------------------- 
  Settings                                                                                               
 -------------------------------------- ------ --------------------------------------------------------- 
  required                                =>    Is this field require?                                   
  multivalued                             =>    Is this field multivalued?                               
  weight                                  =>    Field weight                                             
  meta                                    =>    Field metadata properties.                               
 -------------------------------------- ------ --------------------------------------------------------- 
  [· = Main] Property => default value                                                                   
 -------------------------------------- ------ --------------------------------------------------------- 
  **· value**                             =>    int(0)                                                   
  **form**                                =>    string(0) ""                                             
 -------------------------------------- ------ --------------------------------------------------------- 

### Diameter field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    diameter                                                                
  description                             =>                                                                            
  class                                   =>    App\Forms\FieldTypes\Types\DiameterFieldType                            
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    float(0)                                                                
  **unit**                                =>    string(5) "metre"                                                       
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Volume field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    volume                                                                  
  description                             =>                                                                            
  class                                   =>    App\Forms\FieldTypes\Types\VolumeFieldType                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    float(0)                                                                
  **unit**                                =>    string(11) "cubic metre"                                                
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Program reference field type.
 -------------------------------------- ------ --------------------------------------------------- 
  id                                      =>    program_reference                                  
  description                             =>    Requires a country field in same form              
  class                                   =>    App\Forms\FieldTypes\Types\ProgramReferenceEntity  
 -------------------------------------- ------ --------------------------------------------------- 
  Settings                                                                                         
 -------------------------------------- ------ --------------------------------------------------- 
  required                                =>    Is this field require?                             
  multivalued                             =>    Is this field multivalued?                         
  weight                                  =>    Field weight                                       
  meta                                    =>    Field metadata properties.                         
  sort                                    =>    Is this field sortable?                            
  filter                                  =>    Is this field filterable?                          
  country_field                           =>    The country reference field                        
 -------------------------------------- ------ --------------------------------------------------- 
  [· = Main] Property => default value                                                             
 -------------------------------------- ------ --------------------------------------------------- 
  **· value**                             =>    string(0) ""                                       
  **class**                               =>    string(30) "App\Entity\ProgramIntervention"        
 -------------------------------------- ------ --------------------------------------------------- 

### Functions Carried Out T.A.P. reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------ 
  id                                      =>    functions_carried_out_tap_reference                               
  description                             =>    Requires a country field in same form                             
  class                                   =>    App\Forms\FieldTypes\Types\FunctionsCarriedOutTapReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------ 
  Settings                                                                                                        
 -------------------------------------- ------ ------------------------------------------------------------------ 
  required                                =>    Is this field require?                                            
  multivalued                             =>    Is this field multivalued?                                        
  weight                                  =>    Field weight                                                      
  meta                                    =>    Field metadata properties.                                        
  sort                                    =>    Is this field sortable?                                           
  filter                                  =>    Is this field filterable?                                         
  country_field                           =>    The country reference field                                       
 -------------------------------------- ------ ------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                            
 -------------------------------------- ------ ------------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                      
  **class**                               =>    string(33) "App\Entity\FunctionsCarriedOutTap"                    
 -------------------------------------- ------ ------------------------------------------------------------------ 

### Integer field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    integer                                                                 
  description                             =>                                                                            
  class                                   =>    App\Forms\FieldTypes\Types\IntegerFieldType                             
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  min                                     =>    Minimum value                                                           
  max                                     =>    Maximun value                                                           
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  sort                                    =>    Is this field sortable?                                                 
  filter                                  =>    Is this field filterable?                                               
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    int(0)                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Treatment Technology Sedimentation type reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  id                                      =>    treat_tech_sedimen_reference                                                
  description                             =>    Requires a country field in same form                                       
  class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologySedimentationReferenceEntity  
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  Settings                                                                                                                  
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                      
  multivalued                             =>    Is this field multivalued?                                                  
  weight                                  =>    Field weight                                                                
  meta                                    =>    Field metadata properties.                                                  
  sort                                    =>    Is this field sortable?                                                     
  filter                                  =>    Is this field filterable?                                                   
  country_field                           =>    The country reference field                                                 
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                      
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                                
  **class**                               =>    string(43) "App\Entity\TreatmentTechnologySedimentation"                    
 -------------------------------------- ------ ---------------------------------------------------------------------------- 

### Community form record reference field type
 -------------------------------------- ------ -------------------------------------------------------- 
  id                                      =>    community_reference                                     
  description                             =>                                                            
  class                                   =>    App\Forms\FieldTypes\Types\CommunityReferenceFieldType  
 -------------------------------------- ------ -------------------------------------------------------- 
  Settings                                                                                              
 -------------------------------------- ------ -------------------------------------------------------- 
  required                                =>    Is this field require?                                  
  multivalued                             =>    Is this field multivalued?                              
  weight                                  =>    Field weight                                            
  meta                                    =>    Field metadata properties.                              
  country_field                           =>    The country reference field                             
  sort                                    =>    Is this field sortable?                                 
  filter                                  =>    Is this field filterable?                               
 -------------------------------------- ------ -------------------------------------------------------- 
  [· = Main] Property => default value                                                                  
 -------------------------------------- ------ -------------------------------------------------------- 
  **· value**                             =>    int(0)                                                  
  **form**                                =>    string(14) "form.community"                             
 -------------------------------------- ------ -------------------------------------------------------- 

### Geographical scope type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------- 
  id                                      =>    geographical_scope_reference                                 
  description                             =>    Requires a country field in same form                        
  class                                   =>    App\Forms\FieldTypes\Types\GeographicalScopeReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------- 
  Settings                                                                                                   
 -------------------------------------- ------ ------------------------------------------------------------- 
  required                                =>    Is this field require?                                       
  multivalued                             =>    Is this field multivalued?                                   
  weight                                  =>    Field weight                                                 
  meta                                    =>    Field metadata properties.                                   
  sort                                    =>    Is this field sortable?                                      
  filter                                  =>    Is this field filterable?                                    
  country_field                           =>    The country reference field                                  
 -------------------------------------- ------ ------------------------------------------------------------- 
  [· = Main] Property => default value                                                                       
 -------------------------------------- ------ ------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                 
  **class**                               =>    string(28) "App\Entity\GeographicalScope"                    
 -------------------------------------- ------ ------------------------------------------------------------- 

### Publishing status field type
 -------------------------------------- ------ ------------------------------------------------------ 
  id                                      =>    publishing_status                                     
  description                             =>                                                          
  class                                   =>    App\Forms\FieldTypes\Types\PublishingStatusFieldType  
 -------------------------------------- ------ ------------------------------------------------------ 
  Settings                                                                                            
 -------------------------------------- ------ ------------------------------------------------------ 
  required                                =>    Is this field require?                                
  multivalued                             =>    This field type can't be multivalued.                 
  weight                                  =>    Field weight                                          
  meta                                    =>    Field metadata properties.                            
  sort                                    =>    Is this field sortable?                               
  filter                                  =>    Is this field filterable?                             
 -------------------------------------- ------ ------------------------------------------------------ 
  [· = Main] Property => default value                                                                
 -------------------------------------- ------ ------------------------------------------------------ 
  **· value**                             =>    string(5) "draft"                                     
 -------------------------------------- ------ ------------------------------------------------------ 

### Entity reference field type
 -------------------------------------- ------ ----------------------------------------------------- 
  id                                      =>    entity_reference                                     
  description                             =>                                                         
  class                                   =>    App\Forms\FieldTypes\Types\EntityReferenceFieldType  
 -------------------------------------- ------ ----------------------------------------------------- 
  Settings                                                                                           
 -------------------------------------- ------ ----------------------------------------------------- 
  required                                =>    Is this field require?                               
  multivalued                             =>    Is this field multivalued?                           
  weight                                  =>    Field weight                                         
  sort                                    =>    Is this field sortable?                              
  filter                                  =>    Is this field filterable?                            
  meta                                    =>    Field metadata properties.                           
 -------------------------------------- ------ ----------------------------------------------------- 
  [· = Main] Property => default value                                                               
 -------------------------------------- ------ ----------------------------------------------------- 
  **· value**                             =>    string(0) ""                                         
  **class**                               =>    string(0) ""                                         
 -------------------------------------- ------ ----------------------------------------------------- 

### Administrative division reference field type.
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
  id                                      =>    administrative_division_reference                                                                    
  description                             =>    Requires a country field in same form. The reference must be a community's administrative division.  
  class                                   =>    App\Forms\FieldTypes\Types\AdministrativeDivisionReferenceEntity                                     
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
  Settings                                                                                                                                           
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                                               
  multivalued                             =>    Is this field multivalued?                                                                           
  weight                                  =>    Field weight                                                                                         
  meta                                    =>    Field metadata properties.                                                                           
  sort                                    =>    Is this field sortable?                                                                              
  filter                                  =>    Is this field filterable?                                                                            
  country_field                           =>    The country reference field                                                                          
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                                               
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                                                         
  **class**                               =>    string(33) "App\Entity\AdministrativeDivision"                                                       
 -------------------------------------- ------ ----------------------------------------------------------------------------------------------------- 

### Intervention status type reference field type.
 -------------------------------------- ------ -------------------------------------------------------------- 
  id                                      =>    intervention_status_reference                                 
  description                             =>    Requires a country field in same form                         
  class                                   =>    App\Forms\FieldTypes\Types\InterventionStatusReferenceEntity  
 -------------------------------------- ------ -------------------------------------------------------------- 
  Settings                                                                                                    
 -------------------------------------- ------ -------------------------------------------------------------- 
  required                                =>    Is this field require?                                        
  multivalued                             =>    Is this field multivalued?                                    
  weight                                  =>    Field weight                                                  
  meta                                    =>    Field metadata properties.                                    
  sort                                    =>    Is this field sortable?                                       
  filter                                  =>    Is this field filterable?                                     
  country_field                           =>    The country reference field                                   
 -------------------------------------- ------ -------------------------------------------------------------- 
  [· = Main] Property => default value                                                                        
 -------------------------------------- ------ -------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                  
  **class**                               =>    string(29) "App\Entity\InterventionStatus"                    
 -------------------------------------- ------ -------------------------------------------------------------- 

### Special component reference field type.
 -------------------------------------- ------ --------------------------------------------------------------- 
  id                                      =>    special_component_reference                                    
  description                             =>    Requires a country field in same form                          
  class                                   =>    App\Forms\FieldTypes\Types\WsSpecialComponentsReferenceEntity  
 -------------------------------------- ------ --------------------------------------------------------------- 
  Settings                                                                                                     
 -------------------------------------- ------ --------------------------------------------------------------- 
  required                                =>    Is this field require?                                         
  multivalued                             =>    Is this field multivalued?                                     
  weight                                  =>    Field weight                                                   
  meta                                    =>    Field metadata properties.                                     
  sort                                    =>    Is this field sortable?                                        
  filter                                  =>    Is this field filterable?                                      
  country_field                           =>    The country reference field                                    
 -------------------------------------- ------ --------------------------------------------------------------- 
  [· = Main] Property => default value                                                                         
 -------------------------------------- ------ --------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                   
  **class**                               =>    string(27) "App\Entity\SpecialComponent"                       
 -------------------------------------- ------ --------------------------------------------------------------- 

### Pump type reference field type.
 -------------------------------------- ------ ---------------------------------------------------- 
  id                                      =>    pump_type_reference                                 
  description                             =>    Requires a country field in same form               
  class                                   =>    App\Forms\FieldTypes\Types\PumpTypeReferenceEntity  
 -------------------------------------- ------ ---------------------------------------------------- 
  Settings                                                                                          
 -------------------------------------- ------ ---------------------------------------------------- 
  required                                =>    Is this field require?                              
  multivalued                             =>    Is this field multivalued?                          
  weight                                  =>    Field weight                                        
  meta                                    =>    Field metadata properties.                          
  sort                                    =>    Is this field sortable?                             
  filter                                  =>    Is this field filterable?                           
  country_field                           =>    The country reference field                         
 -------------------------------------- ------ ---------------------------------------------------- 
  [· = Main] Property => default value                                                              
 -------------------------------------- ------ ---------------------------------------------------- 
  **· value**                             =>    string(0) ""                                        
  **class**                               =>    string(19) "App\Entity\PumpType"                    
 -------------------------------------- ------ ---------------------------------------------------- 

### Predominant diameter reference field type.
 -------------------------------------- ------ --------------------------------------------------------- 
  id                                      =>    default_diameter_reference                               
  description                             =>    Requires a country field in same form                    
  class                                   =>    App\Forms\FieldTypes\Types\PredominantDiameterFieldType  
 -------------------------------------- ------ --------------------------------------------------------- 
  Settings                                                                                               
 -------------------------------------- ------ --------------------------------------------------------- 
  required                                =>    Is this field require?                                   
  multivalued                             =>    Is this field multivalued?                               
  weight                                  =>    Field weight                                             
  meta                                    =>    Field metadata properties.                               
  sort                                    =>    Is this field sortable?                                  
  filter                                  =>    Is this field filterable?                                
  country_field                           =>    The country reference field                              
 -------------------------------------- ------ --------------------------------------------------------- 
  [· = Main] Property => default value                                                                   
 -------------------------------------- ------ --------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                             
  **class**                               =>    string(26) "App\Entity\DefaultDiameter"                  
 -------------------------------------- ------ --------------------------------------------------------- 

### Ethnicity type reference field type.
 -------------------------------------- ------ ----------------------------------------------------- 
  id                                      =>    ethnicity_reference                                  
  description                             =>    Requires a country field in same form                
  class                                   =>    App\Forms\FieldTypes\Types\EthnicityReferenceEntity  
 -------------------------------------- ------ ----------------------------------------------------- 
  Settings                                                                                           
 -------------------------------------- ------ ----------------------------------------------------- 
  required                                =>    Is this field require?                               
  multivalued                             =>    Is this field multivalued?                           
  weight                                  =>    Field weight                                         
  meta                                    =>    Field metadata properties.                           
  sort                                    =>    Is this field sortable?                              
  filter                                  =>    Is this field filterable?                            
  country_field                           =>    The country reference field                          
 -------------------------------------- ------ ----------------------------------------------------- 
  [· = Main] Property => default value                                                               
 -------------------------------------- ------ ----------------------------------------------------- 
  **· value**                             =>    string(0) ""                                         
  **class**                               =>    string(20) "App\Entity\Ethnicity"                    
 -------------------------------------- ------ ----------------------------------------------------- 

### Select field type
 -------------------------------------- ------ ---------------------------------------------------------- 
  id                                      =>    select                                                    
  description                             =>    We can add option groups using keys that start with '_'.  
  class                                   =>    App\Forms\FieldTypes\Types\SelectFieldType                
 -------------------------------------- ------ ---------------------------------------------------------- 
  Settings                                                                                                
 -------------------------------------- ------ ---------------------------------------------------------- 
  options                                 =>    Allowed options                                           
  required                                =>    Is this field require?                                    
  multivalued                             =>    Is this field multivalued?                                
  weight                                  =>    Field weight                                              
  meta                                    =>    Field metadata properties.                                
  sort                                    =>    Is this field sortable?                                   
  filter                                  =>    Is this field filterable?                                 
 -------------------------------------- ------ ---------------------------------------------------------- 
  [· = Main] Property => default value                                                                    
 -------------------------------------- ------ ---------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                              
 -------------------------------------- ------ ---------------------------------------------------------- 

### File reference field type
 -------------------------------------- ------ -------------------------------------------------------- 
  id                                      =>    file_reference                                          
  description                             =>                                                            
  class                                   =>    App\Forms\FieldTypes\Types\FileReferenceEntity          
 -------------------------------------- ------ -------------------------------------------------------- 
  Settings                                                                                              
 -------------------------------------- ------ -------------------------------------------------------- 
  required                                =>    Is this field require?                                  
  multivalued                             =>    Is this field multivalued?                              
  weight                                  =>    Field weight                                            
  meta                                    =>    Field metadata properties.                              
  sort                                    =>    Is this field sortable?                                 
  filter                                  =>    Is this field filterable?                               
  allow_extension                         =>    Comma separated list of valid extensions                
  maximum_file_size                       =>    Maximum file size, by default, use the server settings  
 -------------------------------------- ------ -------------------------------------------------------- 
  [· = Main] Property => default value                                                                  
 -------------------------------------- ------ -------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                            
  **class**                               =>    string(15) "App\Entity\File"                            
  **filename**                            =>    string(0) ""                                            
 -------------------------------------- ------ -------------------------------------------------------- 

### Treatment Technology Desalination type reference field type.
 -------------------------------------- ------ --------------------------------------------------------------------------- 
  id                                      =>    treat_tech_desali_reference                                                
  description                             =>    Requires a country field in same form                                      
  class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyDesalinationReferenceEntity  
 -------------------------------------- ------ --------------------------------------------------------------------------- 
  Settings                                                                                                                 
 -------------------------------------- ------ --------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                     
  multivalued                             =>    Is this field multivalued?                                                 
  weight                                  =>    Field weight                                                               
  meta                                    =>    Field metadata properties.                                                 
  sort                                    =>    Is this field sortable?                                                    
  filter                                  =>    Is this field filterable?                                                  
  country_field                           =>    The country reference field                                                
 -------------------------------------- ------ --------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                     
 -------------------------------------- ------ --------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                               
  **class**                               =>    string(42) "App\Entity\TreatmentTechnologyDesalination"                    
 -------------------------------------- ------ --------------------------------------------------------------------------- 

### Long text field type
 -------------------------------------- ------ ---------------------------------------------- 
  id                                      =>    long_text                                     
  description                             =>                                                  
  class                                   =>    App\Forms\FieldTypes\Types\LongTextFieldType  
 -------------------------------------- ------ ---------------------------------------------- 
  Settings                                                                                    
 -------------------------------------- ------ ---------------------------------------------- 
  required                                =>    Is this field require?                        
  multivalued                             =>    Is this field multivalued?                    
  weight                                  =>    Field weight                                  
  meta                                    =>    Field metadata properties.                    
  sort                                    =>    Is this field sortable?                       
  filter                                  =>    Is this field filterable?                     
 -------------------------------------- ------ ---------------------------------------------- 
  [· = Main] Property => default value                                                        
 -------------------------------------- ------ ---------------------------------------------- 
  **· value**                             =>    string(0) ""                                  
 -------------------------------------- ------ ---------------------------------------------- 

### Short text field type
 -------------------------------------- ------ ----------------------------------------------- 
  id                                      =>    short_text                                     
  description                             =>                                                   
  class                                   =>    App\Forms\FieldTypes\Types\ShortTextFieldType  
 -------------------------------------- ------ ----------------------------------------------- 
  Settings                                                                                     
 -------------------------------------- ------ ----------------------------------------------- 
  max-length                              =>    Maximun data length                            
  required                                =>    Is this field require?                         
  multivalued                             =>    Is this field multivalued?                     
  weight                                  =>    Field weight                                   
  meta                                    =>    Field metadata properties.                     
  sort                                    =>    Is this field sortable?                        
  filter                                  =>    Is this field filterable?                      
 -------------------------------------- ------ ----------------------------------------------- 
  [· = Main] Property => default value                                                         
 -------------------------------------- ------ ----------------------------------------------- 
  **· value**                             =>    string(0) ""                                   
 -------------------------------------- ------ ----------------------------------------------- 

### Community Service reference field type.
 -------------------------------------- ------ ------------------------------------------------------------ 
  id                                      =>    community_service_reference                                 
  description                             =>    Requires a country field in same form                       
  class                                   =>    App\Forms\FieldTypes\Types\CommunityServiceReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------ 
  Settings                                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------ 
  required                                =>    Is this field require?                                      
  multivalued                             =>    Is this field multivalued?                                  
  weight                                  =>    Field weight                                                
  meta                                    =>    Field metadata properties.                                  
  sort                                    =>    Is this field sortable?                                     
  filter                                  =>    Is this field filterable?                                   
  country_field                           =>    The country reference field                                 
 -------------------------------------- ------ ------------------------------------------------------------ 
  [· = Main] Property => default value                                                                      
 -------------------------------------- ------ ------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                
  **class**                               =>    string(27) "App\Entity\CommunityService"                    
 -------------------------------------- ------ ------------------------------------------------------------ 

### Currency reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    currency                                                                
  description                             =>    Requires a country field in same form                                   
  class                                   =>    App\Forms\FieldTypes\Types\CurrencyFieldType                            
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  sort                                    =>    Is this field sortable?                                                 
  filter                                  =>    Is this field filterable?                                               
  country_field                           =>    The country reference field                                             
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                            
  **class**                               =>    string(19) "App\Entity\Currency"                                        
  **amount**                              =>    int(0)                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Month and year field type
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
  id                                      =>    month_year                                                                                    
  description                             =>    Month are from 1, that is January, to 12, that is December. The years are from 1900 to 9999.  
  class                                   =>    App\Forms\FieldTypes\Types\MonthYearFieldType                                                 
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
  Settings                                                                                                                                    
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                                        
  multivalued                             =>    Is this field multivalued?                                                                    
  weight                                  =>    Field weight                                                                                  
  meta                                    =>    Field metadata properties.                                                                    
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                                        
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 
  **month**                               =>    string(0) ""                                                                                  
  **year**                                =>    string(0) ""                                                                                  
 -------------------------------------- ------ ---------------------------------------------------------------------------------------------- 

### Treatment Technology Filtration type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  id                                      =>    treat_tech_filtra_reference                                              
  description                             =>    Requires a country field in same form                                    
  class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyFiltrationReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  Settings                                                                                                               
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                   
  multivalued                             =>    Is this field multivalue?                                                
  weight                                  =>    Field weight                                                             
  meta                                    =>    Field metadata properties.                                               
  sort                                    =>    Is this field sortable?                                                  
  filter                                  =>    Is this field filterable?                                                
  country_field                           =>    The country reference field                                              
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                   
 -------------------------------------- ------ ------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                             
  **class**                               =>    string(40) "App\Entity\TreatmentTechnologyFiltration"                    
 -------------------------------------- ------ ------------------------------------------------------------------------- 

### Funder intervention reference field type.
 -------------------------------------- ------ -------------------------------------------------------------- 
  id                                      =>    funder_intervention_reference                                 
  description                             =>    Requires a country field in same form                         
  class                                   =>    App\Forms\FieldTypes\Types\FunderInterventionReferenceEntity  
 -------------------------------------- ------ -------------------------------------------------------------- 
  Settings                                                                                                    
 -------------------------------------- ------ -------------------------------------------------------------- 
  required                                =>    Is this field require?                                        
  multivalued                             =>    Is this field multivalued?                                    
  weight                                  =>    Field weight                                                  
  meta                                    =>    Field metadata properties.                                    
  sort                                    =>    Is this field sortable?                                       
  filter                                  =>    Is this field filterable?                                     
  country_field                           =>    The country reference field                                   
 -------------------------------------- ------ -------------------------------------------------------------- 
  [· = Main] Property => default value                                                                        
 -------------------------------------- ------ -------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                  
  **class**                               =>    string(29) "App\Entity\FunderIntervention"                    
 -------------------------------------- ------ -------------------------------------------------------------- 

### Treatment Technology Aera-Oxida type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    treat_tech_aera_oxida_reference                                         
  description                             =>    Requires a country field in same form                                   
  class                                   =>    App\Forms\FieldTypes\Types\TreatmentTechnologyAeraOxidaReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  sort                                    =>    Is this field sortable?                                                 
  filter                                  =>    Is this field filterable?                                               
  country_field                           =>    The country reference field                                             
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                            
  **class**                               =>    string(39) "App\Entity\TreatmentTechnologyAeraOxida"                    
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Language reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
  id                                      =>    language_reference                                                                
  description                             =>    Requires a country field in same form. Reference to App\Entity\OfficialLanguage.  
  class                                   =>    App\Forms\FieldTypes\Types\LanguageReferenceEntity                                
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
  Settings                                                                                                                        
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                            
  multivalued                             =>    Is this field multivalued?                                                        
  weight                                  =>    Field weight                                                                      
  meta                                    =>    Field metadata properties.                                                        
  sort                                    =>    Is this field sortable?                                                           
  filter                                  =>    Is this field filterable?                                                         
  country_field                           =>    The country reference field                                                       
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                            
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                                      
  **class**                               =>    string(27) "App\Entity\OfficialLanguage"                                          
 -------------------------------------- ------ ---------------------------------------------------------------------------------- 

### Map point field type
 -------------------------------------- ------ ---------------------------------------------- 
  id                                      =>    map_point                                     
  description                             =>                                                  
  class                                   =>    App\Forms\FieldTypes\Types\MapPointFieldType  
 -------------------------------------- ------ ---------------------------------------------- 
  Settings                                                                                    
 -------------------------------------- ------ ---------------------------------------------- 
  required                                =>    Is this field require?                        
  multivalued                             =>    This field type can't be multivalued.         
  weight                                  =>    Field weight                                  
  meta                                    =>    Field metadata properties.                    
  sort                                    =>    Is this field sortable?                       
  filter                                  =>    Is this field filterable?                     
  lat_field                               =>    The Latitude field                            
  lon_field                               =>    The Longitude field                           
 -------------------------------------- ------ ---------------------------------------------- 
  [· = Main] Property => default value                                                        
 -------------------------------------- ------ ---------------------------------------------- 
  **· lat**                               =>    int(0)                                        
  **lon**                                 =>    int(0)                                        
 -------------------------------------- ------ ---------------------------------------------- 

### Type of T.A.P. type reference field type.
 -------------------------------------- ------ --------------------------------------------------- 
  id                                      =>    type_tap_reference                                 
  description                             =>    Requires a country field in same form              
  class                                   =>    App\Forms\FieldTypes\Types\TypeTapReferenceEntity  
 -------------------------------------- ------ --------------------------------------------------- 
  Settings                                                                                         
 -------------------------------------- ------ --------------------------------------------------- 
  required                                =>    Is this field require?                             
  multivalued                             =>    Is this field multivalued?                         
  weight                                  =>    Field weight                                       
  meta                                    =>    Field metadata properties.                         
  sort                                    =>    Is this field sortable?                            
  filter                                  =>    Is this field filterable?                          
  country_field                           =>    The country reference field                        
 -------------------------------------- ------ --------------------------------------------------- 
  [· = Main] Property => default value                                                             
 -------------------------------------- ------ --------------------------------------------------- 
  **· value**                             =>    string(0) ""                                       
  **class**                               =>    string(18) "App\Entity\TypeTap"                    
 -------------------------------------- ------ --------------------------------------------------- 

### Pointing status field type
 -------------------------------------- ------ ---------------------------------------------------- 
  id                                      =>    pointing_status                                     
  description                             =>                                                        
  class                                   =>    App\Forms\FieldTypes\Types\PointingStatusFieldType  
 -------------------------------------- ------ ---------------------------------------------------- 
  Settings                                                                                          
 -------------------------------------- ------ ---------------------------------------------------- 
  required                                =>    Is this field require?                              
  multivalued                             =>    This field type can't be multivalued.               
  weight                                  =>    Field weight                                        
  meta                                    =>    Field metadata properties.                          
  sort                                    =>    Is this field sortable?                             
  filter                                  =>    Is this field filterable?                           
 -------------------------------------- ------ ---------------------------------------------------- 
  [· = Main] Property => default value                                                              
 -------------------------------------- ------ ---------------------------------------------------- 
  **· value**                             =>    string(8) "planning"                                
 -------------------------------------- ------ ---------------------------------------------------- 

### Date field type
 -------------------------------------- ------ ------------------------------------------- 
  id                                      =>    date                                       
  description                             =>                                               
  class                                   =>    App\Forms\FieldTypes\Types\DateFieldType   
 -------------------------------------- ------ ------------------------------------------- 
  Settings                                                                                 
 -------------------------------------- ------ ------------------------------------------- 
  required                                =>    Is this field require?                     
  multivalued                             =>    Is this field multivalued?                 
  weight                                  =>    Field weight                               
  meta                                    =>    Field metadata properties.                 
  sort                                    =>    Is this field sortable?                    
  filter                                  =>    Is this field filterable?                  
 -------------------------------------- ------ ------------------------------------------- 
  [· = Main] Property => default value                                                     
 -------------------------------------- ------ ------------------------------------------- 
  **· value**                             =>    object(DateTime)#1585 (3) {                
                                                  ["date"]=>                               
                                                  string(26) "2023-03-15 13:40:55.137870"  
                                                  ["timezone_type"]=>                      
                                                  int(3)                                   
                                                  ["timezone"]=>                           
                                                  string(3) "UTC"                          
                                                }                                          
  **timezone**                            =>    object(DateTimeZone)#1557 (2) {            
                                                  ["timezone_type"]=>                      
                                                  int(3)                                   
                                                  ["timezone"]=>                           
                                                  string(3) "UTC"                          
                                                }                                          
 -------------------------------------- ------ ------------------------------------------- 

### Water Quality Intervention Type type reference field type.
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    water_quality_inter_tp_reference                                        
  description                             =>    Requires a country field in same form                                   
  class                                   =>    App\Forms\FieldTypes\Types\WaterQualityInterventionTypeReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  sort                                    =>    Is this field sortable?                                                 
  filter                                  =>    Is this field filterable?                                               
  country_field                           =>    The country reference field                                             
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                            
  **class**                               =>    string(39) "App\Entity\WaterQualityInterventionType"                    
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Boolean field type with radio buttons
 -------------------------------------- ------ -------------------------------------------------- 
  id                                      =>    radio_boolean                                     
  description                             =>                                                      
  class                                   =>    App\Forms\FieldTypes\Types\RadioBooleanFieldType  
 -------------------------------------- ------ -------------------------------------------------- 
  Settings                                                                                        
 -------------------------------------- ------ -------------------------------------------------- 
  required                                =>    Is this field require?                            
  multivalued                             =>    Is this field multivalued?                        
  weight                                  =>    Field weight                                      
  meta                                    =>    Field metadata properties.                        
  sort                                    =>    Is this field sortable?                           
  filter                                  =>    Is this field filterable?                         
  true_label                              =>    Label to show TRUE value                          
  false_label                             =>    Label to show FALSE value                         
  show_labels                             =>    Must this field show value labels?                
 -------------------------------------- ------ -------------------------------------------------- 
  [· = Main] Property => default value                                                            
 -------------------------------------- ------ -------------------------------------------------- 
  **· value**                             =>    bool(false)                                       
 -------------------------------------- ------ -------------------------------------------------- 

### Mail response record reference field type
 -------------------------------------- ------ ------------------------------------------------ 
  id                                      =>    thread_mail_reference                           
  description                             =>                                                    
  class                                   =>    App\Forms\FieldTypes\Types\ThreadMailFieldType  
 -------------------------------------- ------ ------------------------------------------------ 
  Settings                                                                                      
 -------------------------------------- ------ ------------------------------------------------ 
  required                                =>    Is this field require?                          
  multivalued                             =>    Is this field multivalued?                      
  weight                                  =>    Field weight                                    
  meta                                    =>    Field metadata properties.                      
  country_field                           =>    The country reference field                     
  subform                                 =>    The subform id                                  
 -------------------------------------- ------ ------------------------------------------------ 
  [· = Main] Property => default value                                                          
 -------------------------------------- ------ ------------------------------------------------ 
  **· value**                             =>    int(0)                                          
  **form**                                =>    string(0) ""                                    
 -------------------------------------- ------ ------------------------------------------------ 

### Water Quality Entity type reference field type.
 -------------------------------------- ------ -------------------------------------------------------------- 
  id                                      =>    water_quality_entity_reference                                
  description                             =>    Requires a country field in same form                         
  class                                   =>    App\Forms\FieldTypes\Types\WaterQualityEntityReferenceEntity  
 -------------------------------------- ------ -------------------------------------------------------------- 
  Settings                                                                                                    
 -------------------------------------- ------ -------------------------------------------------------------- 
  required                                =>    Is this field require?                                        
  multivalued                             =>    Is this field multivalued?                                    
  weight                                  =>    Field weight                                                  
  meta                                    =>    Field metadata properties.                                    
  sort                                    =>    Is this field sortable?                                       
  filter                                  =>    Is this field filterable?                                     
  country_field                           =>    The country reference field                                   
 -------------------------------------- ------ -------------------------------------------------------------- 
  [· = Main] Property => default value                                                                        
 -------------------------------------- ------ -------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                  
  **class**                               =>    string(29) "App\Entity\WaterQualityEntity"                    
 -------------------------------------- ------ -------------------------------------------------------------- 

### Mutateble field type
 -------------------------------------- ------ ----------------------------------------------- 
  id                                      =>    mutateble                                      
  description                             =>                                                   
  class                                   =>    App\Forms\FieldTypes\Types\MutatebleFieldType  
 -------------------------------------- ------ ----------------------------------------------- 
  Settings                                                                                     
 -------------------------------------- ------ ----------------------------------------------- 
  required                                =>    Is this field require?                         
  multivalued                             =>    Is this field multivalued?                     
  weight                                  =>    Field weight                                   
  meta                                    =>    Field metadata properties.                     
  sort                                    =>    Is this field sortable?                        
  filter                                  =>    Is this field filterable?                      
 -------------------------------------- ------ ----------------------------------------------- 
  [· = Main] Property => default value                                                         
 -------------------------------------- ------ ----------------------------------------------- 
  **· value**                             =>    string(0) ""                                   
 -------------------------------------- ------ ----------------------------------------------- 

### Mail field type
 -------------------------------------- ------ ------------------------------------------ 
  id                                      =>    mail                                      
  description                             =>                                              
  class                                   =>    App\Forms\FieldTypes\Types\MailFieldType  
 -------------------------------------- ------ ------------------------------------------ 
  Settings                                                                                
 -------------------------------------- ------ ------------------------------------------ 
  required                                =>    Is this field require?                    
  multivalued                             =>    Is this field multivalued?                
  weight                                  =>    Field weight                              
  meta                                    =>    Field metadata properties.                
  sort                                    =>    Is this field sortable?                   
  filter                                  =>    Is this field filterable?                 
 -------------------------------------- ------ ------------------------------------------ 
  [· = Main] Property => default value                                                    
 -------------------------------------- ------ ------------------------------------------ 
  **· value**                             =>    string(0) ""                              
 -------------------------------------- ------ ------------------------------------------ 

### Flow field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    flow                                                                    
  description                             =>                                                                            
  class                                   =>    App\Forms\FieldTypes\Types\FlowFieldType                                
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    float(0)                                                                
  **unit**                                =>    string(12) "liter/second"                                               
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Sub form record reference field type
 -------------------------------------- ------ --------------------------------------------- 
  id                                      =>    subform_reference                            
  description                             =>                                                 
  class                                   =>    App\Forms\FieldTypes\Types\SubformFieldType  
 -------------------------------------- ------ --------------------------------------------- 
  Settings                                                                                   
 -------------------------------------- ------ --------------------------------------------- 
  required                                =>    Is this field require?                       
  multivalued                             =>    Is this field multivalued?                   
  weight                                  =>    Field weight                                 
  meta                                    =>    Field metadata properties.                   
  country_field                           =>    The country reference field                  
  subform                                 =>    The subform id                               
 -------------------------------------- ------ --------------------------------------------- 
  [· = Main] Property => default value                                                       
 -------------------------------------- ------ --------------------------------------------- 
  **· value**                             =>    int(0)                                       
  **form**                                =>    string(0) ""                                 
 -------------------------------------- ------ --------------------------------------------- 

### Radio/Checkbox field type
 -------------------------------------- ------ ------------------------------------------------- 
  id                                      =>    radio_select                                     
  description                             =>                                                     
  class                                   =>    App\Forms\FieldTypes\Types\RadioSelectFieldType  
 -------------------------------------- ------ ------------------------------------------------- 
  Settings                                                                                       
 -------------------------------------- ------ ------------------------------------------------- 
  options                                 =>    Allowed options                                  
  required                                =>    Is this field require?                           
  multivalued                             =>    Is this field multivalued?                       
  weight                                  =>    Field weight                                     
  meta                                    =>    Field metadata properties.                       
  sort                                    =>    Is this field sortable?                          
  filter                                  =>    Is this field filterable?                        
 -------------------------------------- ------ ------------------------------------------------- 
  [· = Main] Property => default value                                                           
 -------------------------------------- ------ ------------------------------------------------- 
  **· value**                             =>    string(0) ""                                     
 -------------------------------------- ------ ------------------------------------------------- 

### Decimal field type
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
  id                                      =>    decimal                                                                                                                             
  description                             =>                                                                                                                                        
  class                                   =>    App\Forms\FieldTypes\Types\DecimalFieldType                                                                                         
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
  Settings                                                                                                                                                                          
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
  min                                     =>    Minimum value                                                                                                                       
  max                                     =>    Maximun value                                                                                                                       
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.                                                              
  precision                               =>    The maximum number of digits (the precision). It has a range of 1 to 65.                                                            
  scale                                   =>    The number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than precision  
  required                                =>    Is this field require?                                                                                                              
  multivalued                             =>    Is this field multivalued?                                                                                                          
  weight                                  =>    Field weight                                                                                                                        
  meta                                    =>    Field metadata properties.                                                                                                          
  sort                                    =>    Is this field sortable?                                                                                                             
  filter                                  =>    Is this field filterable?                                                                                                           
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 
  **· value**                             =>    int(0)                                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------------------------------------------------------------------ 

### Telephone field type
 -------------------------------------- ------ ------------------------------------------- 
  id                                      =>    phone                                      
  description                             =>                                               
  class                                   =>    App\Forms\FieldTypes\Types\PhoneFieldType  
 -------------------------------------- ------ ------------------------------------------- 
  Settings                                                                                 
 -------------------------------------- ------ ------------------------------------------- 
  required                                =>    Is this field require?                     
  multivalued                             =>    Is this field multivalued?                 
  weight                                  =>    Field weight                               
  meta                                    =>    Field metadata properties.                 
  sort                                    =>    Is this field sortable?                    
  filter                                  =>    Is this field filterable?                  
 -------------------------------------- ------ ------------------------------------------- 
  [· = Main] Property => default value                                                     
 -------------------------------------- ------ ------------------------------------------- 
  **· value**                             =>    string(0) ""                               
 -------------------------------------- ------ ------------------------------------------- 

### Type intervention reference field type.
 -------------------------------------- ------ ------------------------------------------------------------ 
  id                                      =>    type_intervention_reference                                 
  description                             =>    Requires a country field in same form                       
  class                                   =>    App\Forms\FieldTypes\Types\TypeInterventionReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------------ 
  Settings                                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------ 
  required                                =>    Is this field require?                                      
  multivalued                             =>    Is this field multivalued?                                  
  weight                                  =>    Field weight                                                
  meta                                    =>    Field metadata properties.                                  
  sort                                    =>    Is this field sortable?                                     
  filter                                  =>    Is this field filterable?                                   
  country_field                           =>    The country reference field                                 
 -------------------------------------- ------ ------------------------------------------------------------ 
  [· = Main] Property => default value                                                                      
 -------------------------------------- ------ ------------------------------------------------------------ 
  **· value**                             =>    string(0) ""                                                
  **class**                               =>    string(27) "App\Entity\TypeIntervention"                    
 -------------------------------------- ------ ------------------------------------------------------------ 

### Material to distribution infrastructure reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------- 
  id                                      =>    distribution_material_reference                                 
  description                             =>    Requires a country field in same form                           
  class                                   =>    App\Forms\FieldTypes\Types\DistributionMaterialReferenceEntity  
 -------------------------------------- ------ ---------------------------------------------------------------- 
  Settings                                                                                                      
 -------------------------------------- ------ ---------------------------------------------------------------- 
  required                                =>    Is this field require?                                          
  multivalued                             =>    Is this field multivalued?                                      
  weight                                  =>    Field weight                                                    
  meta                                    =>    Field metadata properties.                                      
  sort                                    =>    Is this field sortable?                                         
  filter                                  =>    Is this field filterable?                                       
  country_field                           =>    The country reference field                                     
 -------------------------------------- ------ ---------------------------------------------------------------- 
  [· = Main] Property => default value                                                                          
 -------------------------------------- ------ ---------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                    
  **class**                               =>    string(31) "App\Entity\DistributionMaterial"                    
 -------------------------------------- ------ ---------------------------------------------------------------- 

### Institution intervention reference field type.
 -------------------------------------- ------ ------------------------------------------------------- 
  id                                      =>    institution_reference                                  
  description                             =>    Requires a country field in same form                  
  class                                   =>    App\Forms\FieldTypes\Types\InstitutionReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------------- 
  Settings                                                                                             
 -------------------------------------- ------ ------------------------------------------------------- 
  required                                =>    Is this field require?                                 
  multivalued                             =>    Is this field multivalued?                             
  weight                                  =>    Field weight                                           
  meta                                    =>    Field metadata properties.                             
  sort                                    =>    Is this field sortable?                                
  filter                                  =>    Is this field filterable?                              
  country_field                           =>    The country reference field                            
 -------------------------------------- ------ ------------------------------------------------------- 
  [· = Main] Property => default value                                                                 
 -------------------------------------- ------ ------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                           
  **class**                               =>    string(34) "App\Entity\InstitutionIntervention"        
 -------------------------------------- ------ ------------------------------------------------------- 

### Boolean field type
 -------------------------------------- ------ --------------------------------------------- 
  id                                      =>    boolean                                      
  description                             =>                                                 
  class                                   =>    App\Forms\FieldTypes\Types\BooleanFieldType  
 -------------------------------------- ------ --------------------------------------------- 
  Settings                                                                                   
 -------------------------------------- ------ --------------------------------------------- 
  required                                =>    Is this field require?                       
  multivalued                             =>    Is this field multivalued?                   
  weight                                  =>    Field weight                                 
  meta                                    =>    Field metadata properties.                   
  sort                                    =>    Is this field sortable?                      
  filter                                  =>    Is this field filterable?                    
  true_label                              =>    Label to show TRUE value                     
  false_label                             =>    Label to show FALSE value                    
  show_labels                             =>    Must this field show value labels?           
 -------------------------------------- ------ --------------------------------------------- 
  [· = Main] Property => default value                                                       
 -------------------------------------- ------ --------------------------------------------- 
  **· value**                             =>    bool(false)                                  
 -------------------------------------- ------ --------------------------------------------- 

### Inquiring status field type
 -------------------------------------- ------ ----------------------------------------------------- 
  id                                      =>    inquiring_status                                     
  description                             =>                                                         
  class                                   =>    App\Forms\FieldTypes\Types\InquiringStatusFieldType  
 -------------------------------------- ------ ----------------------------------------------------- 
  Settings                                                                                           
 -------------------------------------- ------ ----------------------------------------------------- 
  required                                =>    Is this field require?                               
  multivalued                             =>    This field type can't be multivalued.                
  weight                                  =>    Field weight                                         
  meta                                    =>    Field metadata properties.                           
  sort                                    =>    Is this field sortable?                              
  filter                                  =>    Is this field filterable?                            
 -------------------------------------- ------ ----------------------------------------------------- 
  [· = Main] Property => default value                                                               
 -------------------------------------- ------ ----------------------------------------------------- 
  **· value**                             =>    string(5) "draft"                                    
 -------------------------------------- ------ ----------------------------------------------------- 

### Point form record reference field type
 -------------------------------------- ------ ---------------------------------------------------- 
  id                                      =>    point_reference                                     
  description                             =>                                                        
  class                                   =>    App\Forms\FieldTypes\Types\PointReferenceFieldType  
 -------------------------------------- ------ ---------------------------------------------------- 
  Settings                                                                                          
 -------------------------------------- ------ ---------------------------------------------------- 
  required                                =>    Is this field require?                              
  multivalued                             =>    Is this field multivalued?                          
  weight                                  =>    Field weight                                        
  meta                                    =>    Field metadata properties.                          
  country_field                           =>    The country reference field                         
  sort                                    =>    Is this field sortable?                             
  filter                                  =>    Is this field filterable?                           
 -------------------------------------- ------ ---------------------------------------------------- 
  [· = Main] Property => default value                                                              
 -------------------------------------- ------ ---------------------------------------------------- 
  **· value**                             =>    int(0)                                              
  **form**                                =>    string(10) "form.point"                             
 -------------------------------------- ------ ---------------------------------------------------- 

### Technical assistance provider reference field type.
 -------------------------------------- ------ ----------------------------------------------------- 
  id                                      =>    tap_reference                                        
  description                             =>    Requires a country field in same form                
  class                                   =>    App\Forms\FieldTypes\Types\TapReferenceEntity        
 -------------------------------------- ------ ----------------------------------------------------- 
  Settings                                                                                           
 -------------------------------------- ------ ----------------------------------------------------- 
  required                                =>    Is this field require?                               
  multivalued                             =>    Is this field multivalued?                           
  weight                                  =>    Field weight                                         
  meta                                    =>    Field metadata properties.                           
  sort                                    =>    Is this field sortable?                              
  filter                                  =>    Is this field filterable?                            
  country_field                           =>    The country reference field                          
 -------------------------------------- ------ ----------------------------------------------------- 
  [· = Main] Property => default value                                                               
 -------------------------------------- ------ ----------------------------------------------------- 
  **· value**                             =>    string(0) ""                                         
  **class**                               =>    string(38) "App\Entity\TechnicalAssistanceProvider"  
 -------------------------------------- ------ ----------------------------------------------------- 

### Material to storage infrastructure reference field type.
 -------------------------------------- ------ ---------------------------------------------------- 
  id                                      =>    material_reference                                  
  description                             =>    Requires a country field in same form               
  class                                   =>    App\Forms\FieldTypes\Types\MaterialReferenceEntity  
 -------------------------------------- ------ ---------------------------------------------------- 
  Settings                                                                                          
 -------------------------------------- ------ ---------------------------------------------------- 
  required                                =>    Is this field require?                              
  multivalued                             =>    Is this field multivalued?                          
  weight                                  =>    Field weight                                        
  meta                                    =>    Field metadata properties.                          
  sort                                    =>    Is this field sortable?                             
  filter                                  =>    Is this field filterable?                           
  country_field                           =>    The country reference field                         
 -------------------------------------- ------ ---------------------------------------------------- 
  [· = Main] Property => default value                                                              
 -------------------------------------- ------ ---------------------------------------------------- 
  **· value**                             =>    string(0) ""                                        
  **class**                               =>    string(19) "App\Entity\Material"                    
 -------------------------------------- ------ ---------------------------------------------------- 

### Editor + Update field type
 -------------------------------------- ------ -------------------------------------------------- 
  id                                      =>    editor_update                                     
  description                             =>                                                      
  class                                   =>    App\Forms\FieldTypes\Types\EditorUpdateFieldType  
 -------------------------------------- ------ -------------------------------------------------- 
  Settings                                                                                        
 -------------------------------------- ------ -------------------------------------------------- 
  required                                =>    Is this field require?                            
  multivalued                             =>    This field type can't be multivalued.             
  weight                                  =>    Field weight                                      
  meta                                    =>    Field metadata properties.                        
  sort                                    =>    Is this field sortable?                           
  filter                                  =>    Is this field filterable?                         
  editor_field                            =>    The editor reference field                        
  update_field                            =>    The update reference field                        
 -------------------------------------- ------ -------------------------------------------------- 
  [· = Main] Property => default value                                                            
 -------------------------------------- ------ -------------------------------------------------- 
  **· value**                             =>    string(0) ""                                      
  **value1**                              =>    string(0) ""                                      
 -------------------------------------- ------ -------------------------------------------------- 

### Length field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    length                                                                  
  description                             =>                                                                            
  class                                   =>    App\Forms\FieldTypes\Types\LengthFieldType                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    float(0)                                                                
  **unit**                                =>    string(5) "metre"                                                       
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### User reference field type
 -------------------------------------- ------ ------------------------------------------------ 
  id                                      =>    user_reference                                  
  description                             =>                                                    
  class                                   =>    App\Forms\FieldTypes\Types\UserReferenceEntity  
 -------------------------------------- ------ ------------------------------------------------ 
  Settings                                                                                      
 -------------------------------------- ------ ------------------------------------------------ 
  required                                =>    Is this field require?                          
  multivalued                             =>    Is this field multivalued?                      
  weight                                  =>    Field weight                                    
  meta                                    =>    Field metadata properties.                      
  sort                                    =>    Is this field sortable?                         
  filter                                  =>    Is this field filterable?                       
 -------------------------------------- ------ ------------------------------------------------ 
  [· = Main] Property => default value                                                          
 -------------------------------------- ------ ------------------------------------------------ 
  **· value**                             =>    string(0) ""                                    
  **class**                               =>    string(15) "App\Entity\User"                    
 -------------------------------------- ------ ------------------------------------------------ 

### Country reference field type
 -------------------------------------- ------ --------------------------------------------------- 
  id                                      =>    country_reference                                  
  description                             =>                                                       
  class                                   =>    App\Forms\FieldTypes\Types\CountryReferenceEntity  
 -------------------------------------- ------ --------------------------------------------------- 
  Settings                                                                                         
 -------------------------------------- ------ --------------------------------------------------- 
  required                                =>    Is this field require?                             
  multivalued                             =>    Is this field multivalued?                         
  weight                                  =>    Field weight                                       
  meta                                    =>    Field metadata properties.                         
  sort                                    =>    Is this field sortable?                            
  filter                                  =>    Is this field filterable?                          
 -------------------------------------- ------ --------------------------------------------------- 
  [· = Main] Property => default value                                                             
 -------------------------------------- ------ --------------------------------------------------- 
  **· value**                             =>    string(0) ""                                       
  **class**                               =>    string(18) "App\Entity\Country"                    
 -------------------------------------- ------ --------------------------------------------------- 

### ULID field type. If required and empty generate a new ULID.
 -------------------------------------- ------ ------------------------------------------ 
  id                                      =>    ulid                                      
  description                             =>                                              
  class                                   =>    App\Forms\FieldTypes\Types\UlidFieldType  
 -------------------------------------- ------ ------------------------------------------ 
  Settings                                                                                
 -------------------------------------- ------ ------------------------------------------ 
  required                                =>    Is this field require?                    
  multivalued                             =>    Is this field multivalued?                
  weight                                  =>    Field weight                              
  meta                                    =>    Field metadata properties.                
  sort                                    =>    Is this field sortable?                   
  filter                                  =>    Is this field filterable?                 
 -------------------------------------- ------ ------------------------------------------ 
  [· = Main] Property => default value                                                    
 -------------------------------------- ------ ------------------------------------------ 
  **· value**                             =>    string(0) ""                              
 -------------------------------------- ------ ------------------------------------------ 

### Concentration field type
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  id                                      =>    concentration                                                           
  description                             =>                                                                            
  class                                   =>    App\Forms\FieldTypes\Types\ConcentrationFieldType                       
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  Settings                                                                                                              
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  required                                =>    Is this field require?                                                  
  multivalued                             =>    Is this field multivalued?                                              
  weight                                  =>    Field weight                                                            
  meta                                    =>    Field metadata properties.                                              
  unknowable                              =>    Can this field be unknow? If TRUE will be unknow if value is -9999999.  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  [· = Main] Property => default value                                                                                  
 -------------------------------------- ------ ------------------------------------------------------------------------ 
  **· value**                             =>    float(0)                                                                
  **unit**                                =>    string(21) "particles per million"                                      
 -------------------------------------- ------ ------------------------------------------------------------------------ 

### Typology Chlorination Installation type reference field type.
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  id                                      =>    typo_chlori_install_reference                                               
  description                             =>    Requires a country field in same form                                       
  class                                   =>    App\Forms\FieldTypes\Types\TypologyChlorinationInstallationReferenceEntity  
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  Settings                                                                                                                  
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  required                                =>    Is this field require?                                                      
  multivalued                             =>    Is this field multivalued?                                                  
  weight                                  =>    Field weight                                                                
  meta                                    =>    Field metadata properties.                                                  
  sort                                    =>    Is this field sortable?                                                     
  filter                                  =>    Is this field filterable?                                                   
  country_field                           =>    The country reference field                                                 
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  [· = Main] Property => default value                                                                                      
 -------------------------------------- ------ ---------------------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                                
  **class**                               =>    string(43) "App\Entity\TypologyChlorinationInstallation"                    
 -------------------------------------- ------ ---------------------------------------------------------------------------- 

### File image reference field type
 -------------------------------------- ------ -------------------------------------------------------- 
  id                                      =>    image_reference                                         
  description                             =>                                                            
  class                                   =>    App\Forms\FieldTypes\Types\ImageFileReferenceEntity     
 -------------------------------------- ------ -------------------------------------------------------- 
  Settings                                                                                              
 -------------------------------------- ------ -------------------------------------------------------- 
  required                                =>    Is this field require?                                  
  multivalued                             =>    Is this field multivalued?                              
  weight                                  =>    Field weight                                            
  meta                                    =>    Field metadata properties.                              
  sort                                    =>    Is this field sortable?                                 
  filter                                  =>    Is this field filterable?                               
  allow_extension                         =>    Comma separated list of valid extensions                
  maximum_file_size                       =>    Maximum file size, by default, use the server settings  
 -------------------------------------- ------ -------------------------------------------------------- 
  [· = Main] Property => default value                                                                  
 -------------------------------------- ------ -------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                            
  **class**                               =>    string(15) "App\Entity\File"                            
  **filename**                            =>    string(0) ""                                            
 -------------------------------------- ------ -------------------------------------------------------- 

### Disinfecting Substance type reference field type.
 -------------------------------------- ------ ----------------------------------------------------------------- 
  id                                      =>    disinfect_substance_reference                                    
  description                             =>    Requires a country field in same form                            
  class                                   =>    App\Forms\FieldTypes\Types\DisinfectingSubstanceReferenceEntity  
 -------------------------------------- ------ ----------------------------------------------------------------- 
  Settings                                                                                                       
 -------------------------------------- ------ ----------------------------------------------------------------- 
  required                                =>    Is this field require?                                           
  multivalued                             =>    Is this field multivalued?                                       
  weight                                  =>    Field weight                                                     
  meta                                    =>    Field metadata properties.                                       
  sort                                    =>    Is this field sortable?                                          
  filter                                  =>    Is this field filterable?                                        
  country_field                           =>    The country reference field                                      
 -------------------------------------- ------ ----------------------------------------------------------------- 
  [· = Main] Property => default value                                                                           
 -------------------------------------- ------ ----------------------------------------------------------------- 
  **· value**                             =>    string(0) ""                                                     
  **class**                               =>    string(32) "App\Entity\DisinfectingSubstance"                    
 -------------------------------------- ------ ----------------------------------------------------------------- 

