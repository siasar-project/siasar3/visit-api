<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531071109 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE language DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE language CHANGE language_id id VARCHAR(5) NOT NULL');
        $this->addSql('ALTER TABLE language ADD PRIMARY KEY (id)');
        $this->addSql('CREATE TABLE locale_target (language_id VARCHAR(5) NOT NULL, locale_source_id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', translation LONGBLOB NOT NULL, context VARCHAR(255) DEFAULT NULL, INDEX IDX_368482471C9A06 (language_id), INDEX IDX_368482474B795390 (locale_source_id), PRIMARY KEY(language_id, locale_source_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482471C9A06 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482474B795390 FOREIGN KEY (locale_source_id) REFERENCES locale_source (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE locale_target');
        $this->addSql('ALTER TABLE language DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE language CHANGE id language_id VARCHAR(5) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE language ADD PRIMARY KEY (language_id)');
    }
}
