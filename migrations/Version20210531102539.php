<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210531102539 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE locale_target DROP FOREIGN KEY FK_368482471C9A06');
        $this->addSql('ALTER TABLE locale_target DROP FOREIGN KEY FK_368482474B795390');
        $this->addSql('DROP INDEX idx_368482471c9a06 ON locale_target');
        $this->addSql('CREATE INDEX IDX_3684824782F1BAF4 ON locale_target (language_id)');
        $this->addSql('DROP INDEX idx_368482474b795390 ON locale_target');
        $this->addSql('CREATE INDEX IDX_368482477046F4B4 ON locale_target (locale_source_id)');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482471C9A06 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482474B795390 FOREIGN KEY (locale_source_id) REFERENCES locale_source (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE locale_target DROP FOREIGN KEY FK_3684824782F1BAF4');
        $this->addSql('ALTER TABLE locale_target DROP FOREIGN KEY FK_368482477046F4B4');
        $this->addSql('DROP INDEX idx_3684824782f1baf4 ON locale_target');
        $this->addSql('CREATE INDEX IDX_368482471C9A06 ON locale_target (language_id)');
        $this->addSql('DROP INDEX idx_368482477046f4b4 ON locale_target');
        $this->addSql('CREATE INDEX IDX_368482474B795390 ON locale_target (locale_source_id)');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_3684824782F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482477046F4B4 FOREIGN KEY (locale_source_id) REFERENCES locale_source (id)');
    }
}
