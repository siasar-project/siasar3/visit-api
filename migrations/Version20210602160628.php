<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602160628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE locale_target DROP FOREIGN KEY FK_368482474B795390');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482477046F4B4 FOREIGN KEY (locale_source_id) REFERENCES locale_source (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE locale_target DROP FOREIGN KEY FK_368482477046F4B4');
        $this->addSql('ALTER TABLE locale_target ADD CONSTRAINT FK_368482474B795390 FOREIGN KEY (locale_source_id) REFERENCES locale_source (id)');
    }
}
