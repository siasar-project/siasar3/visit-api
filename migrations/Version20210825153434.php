<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210825153434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country ADD flag_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:ulid)\'');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C966919FE4E5 FOREIGN KEY (flag_id) REFERENCES file (id)');
        $this->addSql('CREATE INDEX IDX_5373C966919FE4E5 ON country (flag_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C966919FE4E5');
        $this->addSql('DROP INDEX IDX_5373C966919FE4E5 ON country');
        $this->addSql('ALTER TABLE country DROP flag_id');
    }
}
