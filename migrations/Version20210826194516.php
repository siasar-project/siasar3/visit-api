<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210826194516 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE funder_intervention (id INT AUTO_INCREMENT NOT NULL, country_code VARCHAR(50) NOT NULL, name VARCHAR(100) NOT NULL, address VARCHAR(255) DEFAULT NULL, phone VARCHAR(25) DEFAULT NULL, contact VARCHAR(255) DEFAULT NULL, INDEX IDX_20F47F77F026BB7C (country_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE funder_intervention ADD CONSTRAINT FK_20F47F77F026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE funder_intervention');
    }
}
