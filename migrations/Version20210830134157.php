<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210830134157 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE type_intervention (id INT AUTO_INCREMENT NOT NULL, country_code VARCHAR(50) NOT NULL, type VARCHAR(100) NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_565BAEAEF026BB7C (country_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE type_intervention ADD CONSTRAINT FK_565BAEAEF026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
        $this->addSql('ALTER TABLE institution_intervention DROP FOREIGN KEY FK_7938EF9CF026BB7C');
        $this->addSql('DROP INDEX idx_7938ef9cf026bb7c ON institution_intervention');
        $this->addSql('CREATE INDEX IDX_6718D9D9F026BB7C ON institution_intervention (country_code)');
        $this->addSql('ALTER TABLE institution_intervention ADD CONSTRAINT FK_7938EF9CF026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE type_intervention');
        $this->addSql('ALTER TABLE institution_intervention DROP FOREIGN KEY FK_6718D9D9F026BB7C');
        $this->addSql('DROP INDEX idx_6718d9d9f026bb7c ON institution_intervention');
        $this->addSql('CREATE INDEX IDX_7938EF9CF026BB7C ON institution_intervention (country_code)');
        $this->addSql('ALTER TABLE institution_intervention ADD CONSTRAINT FK_6718D9D9F026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
    }
}
