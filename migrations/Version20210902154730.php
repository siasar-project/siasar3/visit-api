<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210902154730 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE funder_intervention CHANGE id id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE institution_intervention CHANGE id id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE material CHANGE id id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE type_intervention CHANGE id id VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE funder_intervention CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE institution_intervention CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE material CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE type_intervention CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }
}
