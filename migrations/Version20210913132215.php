<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210913132215 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_administrative_division (user_id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', administrative_division_id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', INDEX IDX_649937A76ED395 (user_id), INDEX IDX_6499374E5603EB (administrative_division_id), PRIMARY KEY(user_id, administrative_division_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_administrative_division ADD CONSTRAINT FK_649937A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_administrative_division ADD CONSTRAINT FK_6499374E5603EB FOREIGN KEY (administrative_division_id) REFERENCES administrative_division (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_administrative_division');
    }
}
