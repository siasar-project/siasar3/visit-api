<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915145548 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE official_language DROP FOREIGN KEY FK_1446FD0982F1BAF4');
        $this->addSql('DROP INDEX IDX_1446FD0982F1BAF4 ON official_language');
        $this->addSql('ALTER TABLE official_language ADD language VARCHAR(50) DEFAULT NULL, DROP language_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE official_language ADD language_id VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP language');
        $this->addSql('ALTER TABLE official_language ADD CONSTRAINT FK_1446FD0982F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id)');
        $this->addSql('CREATE INDEX IDX_1446FD0982F1BAF4 ON official_language (language_id)');
    }
}
