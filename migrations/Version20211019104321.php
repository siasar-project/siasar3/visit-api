<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211019104321 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrative_division CHANGE name name VARCHAR(255) NOT NULL, CHANGE code code VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE administrative_division_type CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE country CHANGE predominant predominant VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE currency CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ethnicity CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE funder_intervention CHANGE name name VARCHAR(255) NOT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE institution_intervention CHANGE name name VARCHAR(255) NOT NULL, CHANGE phone phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE material CHANGE type type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE official_language CHANGE language language VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE program_intervention CHANGE name name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE technical_assistance_provider CHANGE name name VARCHAR(255) NOT NULL, CHANGE national_id national_id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE type_intervention CHANGE type type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user_administrative_division DROP FOREIGN KEY FK_6499374E5603EB');
        $this->addSql('ALTER TABLE user_administrative_division DROP FOREIGN KEY FK_649937A76ED395');
        $this->addSql('ALTER TABLE user_administrative_division ADD CONSTRAINT FK_6499374E5603EB FOREIGN KEY (administrative_division_id) REFERENCES administrative_division (id)');
        $this->addSql('ALTER TABLE user_administrative_division ADD CONSTRAINT FK_649937A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE administrative_division CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE code code VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE administrative_division_type CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE country CHANGE predominant predominant VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE currency CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE ethnicity CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE funder_intervention CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(25) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE institution_intervention CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone phone VARCHAR(20) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE material CHANGE type type VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE official_language CHANGE language language VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE program_intervention CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE technical_assistance_provider CHANGE name name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE national_id national_id VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE type_intervention CHANGE type type VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user_administrative_division DROP FOREIGN KEY FK_649937A76ED395');
        $this->addSql('ALTER TABLE user_administrative_division DROP FOREIGN KEY FK_6499374E5603EB');
        $this->addSql('ALTER TABLE user_administrative_division ADD CONSTRAINT FK_649937A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_administrative_division ADD CONSTRAINT FK_6499374E5603EB FOREIGN KEY (administrative_division_id) REFERENCES administrative_division (id) ON DELETE CASCADE');
    }
}
