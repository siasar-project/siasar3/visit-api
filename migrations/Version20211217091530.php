<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211217091530 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country ADD main_unit_concentration VARCHAR(255) DEFAULT NULL, ADD unit_concentrations LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', ADD main_unit_flow VARCHAR(255) DEFAULT NULL, ADD unit_flows LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', ADD main_unit_length VARCHAR(255) DEFAULT NULL, ADD unit_lengths LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', ADD main_unit_volume VARCHAR(255) DEFAULT NULL, ADD unit_volumes LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country DROP main_unit_concentration, DROP unit_concentrations, DROP main_unit_flow, DROP unit_flows, DROP main_unit_length, DROP unit_lengths, DROP main_unit_volume, DROP unit_volumes');
    }
}
