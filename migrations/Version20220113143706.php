<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113143706 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE point_log ADD country_code VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE point_log ADD CONSTRAINT FK_DCF3EA5CF026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
        $this->addSql('CREATE INDEX IDX_DCF3EA5CF026BB7C ON point_log (country_code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE point_log DROP FOREIGN KEY FK_DCF3EA5CF026BB7C');
        $this->addSql('DROP INDEX IDX_DCF3EA5CF026BB7C ON point_log');
        $this->addSql('ALTER TABLE point_log DROP country_code');
    }
}
