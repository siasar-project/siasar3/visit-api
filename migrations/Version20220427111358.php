<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427111358 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country ADD main_official_language_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:ulid)\', DROP predominant');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C96677A04C9E FOREIGN KEY (main_official_language_id) REFERENCES official_language (id)');
        $this->addSql('CREATE INDEX IDX_5373C96677A04C9E ON country (main_official_language_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C96677A04C9E');
        $this->addSql('DROP INDEX IDX_5373C96677A04C9E ON country');
        $this->addSql('ALTER TABLE country ADD predominant VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP main_official_language_id');
    }
}
