<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220512092002 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE treatment_technology_sedimentation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', country_code VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_EA6D70F2F026BB7C (country_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE treatment_technology_sedimentation ADD CONSTRAINT FK_EA6D70F2F026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE treatment_technology_sedimentation');
    }
}
