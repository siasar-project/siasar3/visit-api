<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220516082908 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE typology_chlorination_installation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', country_code VARCHAR(50) NOT NULL, key_index VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_22D378EFF026BB7C (country_code), UNIQUE INDEX unique_key (key_index, country_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE typology_chlorination_installation ADD CONSTRAINT FK_22D378EFF026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE typology_chlorination_installation');
    }
}
