<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220622072548 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inquiry_log ADD country_code VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE inquiry_log ADD CONSTRAINT FK_C34EEC91F026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
        $this->addSql('CREATE INDEX IDX_C34EEC91F026BB7C ON inquiry_log (country_code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inquiry_log DROP FOREIGN KEY FK_C34EEC91F026BB7C');
        $this->addSql('DROP INDEX IDX_C34EEC91F026BB7C ON inquiry_log');
        $this->addSql('ALTER TABLE inquiry_log DROP country_code');
    }
}
