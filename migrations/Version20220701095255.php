<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220701095255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE household_process (id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', administrative_division_id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', country_code VARCHAR(50) NOT NULL, community_reference BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\', finished_household INT NOT NULL, open TINYINT(1) NOT NULL, INDEX IDX_EBE9F2B84E5603EB (administrative_division_id), INDEX IDX_EBE9F2B8F026BB7C (country_code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE household_process ADD CONSTRAINT FK_EBE9F2B84E5603EB FOREIGN KEY (administrative_division_id) REFERENCES administrative_division (id)');
        $this->addSql('ALTER TABLE household_process ADD CONSTRAINT FK_EBE9F2B8F026BB7C FOREIGN KEY (country_code) REFERENCES country (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE household_process');
    }
}
