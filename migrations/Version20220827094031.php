<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220827094031 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX level_idx ON inquiry_log (level)');
        $this->addSql('CREATE INDEX level_name_idx ON inquiry_log (level_name)');
        $this->addSql('CREATE INDEX created_at_idx ON inquiry_log (created_at)');
        $this->addSql('CREATE INDEX form_id_idx ON inquiry_log (form_id)');
        $this->addSql('CREATE INDEX record_id_idx ON inquiry_log (record_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX level_idx ON inquiry_log');
        $this->addSql('DROP INDEX level_name_idx ON inquiry_log');
        $this->addSql('DROP INDEX created_at_idx ON inquiry_log');
        $this->addSql('DROP INDEX form_id_idx ON inquiry_log');
        $this->addSql('DROP INDEX record_id_idx ON inquiry_log');
    }
}
