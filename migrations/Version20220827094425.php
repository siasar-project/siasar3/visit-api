<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220827094425 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX level_idx ON point_log (level)');
        $this->addSql('CREATE INDEX level_name_idx ON point_log (level_name)');
        $this->addSql('CREATE INDEX created_at_idx ON point_log (created_at)');
        $this->addSql('CREATE INDEX point_idx ON point_log (point)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX level_idx ON point_log');
        $this->addSql('DROP INDEX level_name_idx ON point_log');
        $this->addSql('DROP INDEX created_at_idx ON point_log');
        $this->addSql('DROP INDEX point_idx ON point_log');
    }
}
