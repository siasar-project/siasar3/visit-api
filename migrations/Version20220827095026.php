<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220827095026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX username_idx ON refresh_token (username)');
        $this->addSql('CREATE INDEX identifier_idx ON refresh_token (identifier)');
        $this->addSql('CREATE INDEX verifier_idx ON refresh_token (verifier)');
        $this->addSql('CREATE INDEX valid_until_idx ON refresh_token (valid_until)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX username_idx ON refresh_token');
        $this->addSql('DROP INDEX identifier_idx ON refresh_token');
        $this->addSql('DROP INDEX verifier_idx ON refresh_token');
        $this->addSql('DROP INDEX valid_until_idx ON refresh_token');
    }
}
