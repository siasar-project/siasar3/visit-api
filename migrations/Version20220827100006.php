<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220827100006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE INDEX iso_code_idx ON language (iso_code)');
        $this->addSql('CREATE INDEX message_idx ON locale_source (message)');
        $this->addSql('CREATE INDEX context_idx ON locale_source (context)');
        $this->addSql('CREATE INDEX context_idx ON locale_target (context)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX iso_code_idx ON language');
        $this->addSql('DROP INDEX message_idx ON locale_source');
        $this->addSql('DROP INDEX context_idx ON locale_source');
        $this->addSql('DROP INDEX context_idx ON locale_target');
    }
}
