<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221021094840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE language ADD translatable TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('DROP INDEX message_idx ON locale_source');
        $this->addSql('CREATE INDEX message_idx ON locale_source (message)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE language DROP translatable');
        $this->addSql('DROP INDEX message_idx ON locale_source');
        $this->addSql('CREATE INDEX message_idx ON locale_source (message(768))');
    }
}
