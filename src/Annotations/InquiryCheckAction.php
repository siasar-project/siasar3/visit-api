<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Inquiry checking operation annotation.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @Annotation
 *
 * @Target("CLASS")
 */
class InquiryCheckAction
{
    /**
     * Check type unique ID.
     *
     * @Required
     */
    public string $id;

    /**
     * Level type: Error | Warning.
     *
     * @Required
     */
    public string $level;

    /**
     * Inquiry type ID. Use "*" to apply to all form types.
     */
    public string $form = '';

    /**
     * Observations.
     *
     * @var string
     */
    public string $observation;

    /**
     * Throw message.
     *
     * @var string
     */
    public string $message;

    /**
     * Is this check active? Must it be applied?
     *
     * @var bool
     */
    public bool $active;

    /**
     * Is this check multi message?
     *
     * Used to tests.
     *
     * @var bool
     */
    public bool $multiMessage = false;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level): void
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getForm(): string
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm(string $form): void
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getObservation(): string
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation(string $observation): void
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isMultiMessage(): bool
    {
        return $this->multiMessage;
    }

    /**
     * @param bool $multiMessage
     */
    public function setMultiMessage(bool $multiMessage): void
    {
        $this->multiMessage = $multiMessage;
    }
}
