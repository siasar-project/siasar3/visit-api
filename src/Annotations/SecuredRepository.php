<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Annotations;

use Doctrine\Common\Annotations\Annotation\Required;

/**
 * Mark to use the relation property how a counter, instead a list, in custom
 * OpenAPI controllers.
 *
 * @Annotation
 *
 * @Target({"CLASS"})
 */
class SecuredRepository
{
    /**
     * @Required
     */
    public $class;
}
