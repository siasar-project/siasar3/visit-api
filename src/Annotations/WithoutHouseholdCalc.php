<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Without Household calc annotation.
 *
 * If the community don't have households inquiries.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @Annotation
 *
 * @Target("CLASS")
 */
class WithoutHouseholdCalc
{
    /**
     * Check type unique ID.
     *
     * @Required
     */
    public string $id;

    /**
     * Is this check active? Must it be applied?
     *
     * @var bool
     */
    public bool $active;

    /**
     * The field where to save the results.
     *
     * Used here to reset value before do calcs.
     *
     * @var string
     */
    public string $communityField;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getCommunityField(): string
    {
        return $this->communityField;
    }

    /**
     * @param string $communityField
     */
    public function setCommunityField(string $communityField): void
    {
        $this->communityField = $communityField;
    }
}
