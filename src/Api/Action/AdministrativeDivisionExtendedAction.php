<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Serializer\JsonEncoder;
use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\User;
use App\Repository\AdministrativeDivisionRepository;
use App\Security\Handlers\DoctrineAccessHandler;
use App\Service\AccessManager;
use App\Service\Mailer\MailerService;
use App\Service\Password\EncoderService;
use App\Service\Request\RequestService;
use App\Service\SessionService;
use App\Templating\TwigTemplate;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Uid\Ulid;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * Entities extended controllers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AdministrativeDivisionExtendedAction
{
    protected AdministrativeDivisionRepository $divisionRepository;
    protected MailerService $mailer;
    protected EncoderService $encoderService;
    protected IriConverterInterface $iriConverter;
    protected EntityManagerInterface $em;
    private DoctrineAccessHandler $accessHandler;

    /**
     * AdministrativeDivisionExtendedAction constructor.
     *
     * @param AdministrativeDivisionRepository $divisionRepository Divisions repository.
     * @param MailerService                    $mailer             Mailer service.
     * @param EncoderService                   $encoderService     Encoder service.
     * @param IriConverterInterface            $iriConverter
     * @param EntityManagerInterface           $em
     * @param SessionService                   $sessionService
     * @param AccessManager                    $accessManager
     */
    public function __construct(AdministrativeDivisionRepository $divisionRepository, MailerService $mailer, EncoderService $encoderService, IriConverterInterface $iriConverter, EntityManagerInterface $em, SessionService $sessionService, AccessManager $accessManager)
    {
        $this->divisionRepository = $divisionRepository;
        $this->mailer = $mailer;
        $this->encoderService = $encoderService;
        $this->iriConverter = $iriConverter;
        $this->em = $em;
        $this->accessHandler = new DoctrineAccessHandler(AdministrativeDivision::class, $accessManager, $sessionService, $iriConverter);
    }

    /**
     * Get divisions by country.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="ad_by_country",
     *     path="/administrative_divisions/country/{country_code}",
     *     methods={"GET"},
     * )
     */
    public function divisionsByCountry(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->accessHandler->getCurrentUser();
        $requestCountryCode = $request->get('country_code');
        $page = $request->get('page');
        if ($page < 1) {
            $page = 1;
        }
        $page -= 1;
        // See the ad_by_country action definition src/Entity/AdministrativeDivision.php.
        // It exposes this value to users.
        $pageSize = 500;

        // Validate user access in the requested country.
        $this->accessHandler->checkAccess(
            $user,
            'read',
            'doctrine administrativedivision',
            [
                'country' => $this->em->getRepository(Country::class)->find($requestCountryCode),
            ]
        );
        $resp = $this->em
            //
            ->createQuery(
                'SELECT ad.id, ad.name, ad.code, ad.level, IDENTITY(ad.parent) as parent FROM App\Entity\AdministrativeDivision ad WHERE ad.country = :country_code'
            )
            ->setParameters([
                'country_code' => $request->get('country_code'),
            ])
            ->setMaxResults($pageSize)
            ->setFirstResult($page * $pageSize)
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        // Fix ULIDs.
        foreach ($resp as &$item) {
            /** @var Ulid $itemId */
            $itemId = $item['id'];
            $item['id'] = $itemId->toBase32();
            if ($item['parent']) {
                $itemId = Ulid::fromBinary($item['parent']);
                $item['parent'] = $itemId->toBase32();
            }
        }

        return new JsonResponse($resp, 200);
    }
}
