<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use App\Entity\HouseholdProcess;
use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Entity\User;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Repository\ConfigurationRepository;
use App\Security\Voter\ConfigurationVoter;
use App\Security\Voter\VoterBase;
use App\Service\AccessManager;
use App\Service\ConfigurationService;
use App\Service\Mailer\MailerService;
use App\Service\Password\EncoderService;
use App\Service\Request\RequestService;
use App\Tools\Json;
use App\Tools\TranslatableMarkup;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Util\Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Ulid;

/**
 * User account controllers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationAction
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected ConfigurationRepository $configurationRepository;
    protected MailerService $mailer;
    protected EncoderService $encoderService;
    protected AccessManager $accessManager;
    protected ConfigurationService $configurationService;
    protected EntityManagerInterface $em;
    protected FormFactory $formFactory;
    protected ParameterBagInterface $systemSettings;

    /**
     * UserAccountAction constructor.
     *
     * @param ConfigurationService   $configurationService Configuration service.
     * @param MailerService          $mailer               Mailer service.
     * @param EncoderService         $encoderService       Encoder service.
     * @param AccessManager          $accessManager        Access manager.
     * @param EntityManagerInterface $em                   Entity manager.
     * @param FormFactory            $formFactory          Form factory.
     */
    public function __construct(ConfigurationService $configurationService, MailerService $mailer, EncoderService $encoderService, AccessManager $accessManager, EntityManagerInterface $em, FormFactory $formFactory)
    {
        $this->configurationService = $configurationService;
        $this->configurationRepository = $this->configurationService->getRepository();
        $this->mailer = $mailer;
        $this->encoderService = $encoderService;
        $this->accessManager = $accessManager;
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
    }

    /**
     * Get system.units.length values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_units_length",
     *     path="/configuration/system.units.length",
     *     methods={"GET"},
     * )
     */
    public function lengthUnits(): JsonResponse
    {
        $allowed = $this->isAllowed('system.units.length');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $cfg = $this->configurationRepository->find('system.units.length');

        return new JsonResponse([$this->translateUnits($cfg->getValue())], 200);
    }

    /**
     * Get system.units.flow values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_units_flow",
     *     path="/configuration/system.units.flow",
     *     methods={"GET"},
     * )
     */
    public function flowUnits(): JsonResponse
    {
        $allowed = $this->isAllowed('system.units.flow');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $cfg = $this->configurationRepository->find('system.units.flow');

        return new JsonResponse([$this->translateUnits($cfg->getValue())], 200);
    }

    /**
     * Get system.units.volume values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_units_volume",
     *     path="/configuration/system.units.volume",
     *     methods={"GET"},
     * )
     */
    public function volumeUnits(): JsonResponse
    {
        $allowed = $this->isAllowed('system.units.volume');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $cfg = $this->configurationRepository->find('system.units.volume');

        return new JsonResponse([$this->translateUnits($cfg->getValue())], 200);
    }

    /**
     * Get system.units.concentration values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_units_concentration",
     *     path="/configuration/system.units.concentration",
     *     methods={"GET"},
     * )
     */
    public function concentrationUnits(): JsonResponse
    {
        $allowed = $this->isAllowed('system.units.concentration');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $cfg = $this->configurationRepository->find('system.units.concentration');

        return new JsonResponse([$this->translateUnits($cfg->getValue())], 200);
    }

    /**
     * Get system.role values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_role",
     *     path="/configuration/system.role",
     *     methods={"GET"},
     * )
     */
    public function systemRole(): JsonResponse
    {
        $allowed = $this->isAllowed('system.role.*');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $roles = $this->accessManager->getSystemRoles();
        foreach ($roles as &$role) {
            $role['label'] = self::t($role['label']);
            $role['description'] = self::t($role['description']);
        }

        return new JsonResponse($roles, 200);
    }

    /**
     * Get system.permissions values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_permissions",
     *     path="/configuration/system.permissions",
     *     methods={"GET"},
     * )
     */
    public function systemPermissions(): JsonResponse
    {
        $allowed = $this->isAllowed('system.permissions');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $permissions = $this->accessManager->getSystemPermissions();

        return new JsonResponse($permissions, 200);
    }

    /**
     * Set maintenance mode..
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @Route(
     *     name="maintenance_mode",
     *     path="/configuration/maintenance-mode",
     *     methods={"POST"},
     * )
     */
    public function maintenanceMode(Request $request): JsonResponse
    {
        $allow = false;
        $user = $this->accessManager->getSecurity()->getUser();
        if ($user) {
            $allow = $this->accessManager->hasPermission($user, 'use the site in maintenance mode');
        }
        if (!$allow) {
            return new JsonResponse(
                $this->t(
                    'The permission "@permission" is required.',
                    ['@permission' => 'use the site in maintenance mode']
                ),
                403
            );
        }

        $active = RequestService::getField($request, 'active');
        $this->configurationService->setMaintenanceMode($active);

        return new JsonResponse(['active' => $this->configurationService->inMaintenanceMode()], 200);
    }

    /**
     * Get server status.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_ping",
     *     path="/configuration/ping",
     *     methods={"GET"},
     * )
     */
    public function systemPing(): JsonResponse
    {
        return new JsonResponse(['maintenance_mode' => $this->configurationService->inMaintenanceMode()], 200);
    }

    /**
     * Get mutateble fields.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="form_mutatebles",
     *     path="/configuration/mutatebles",
     *     methods={"GET"},
     * )
     */
    public function mutatebles(): JsonResponse
    {
        $mutatebles = $this->formFactory->getMutatebleFields(InquiryFormManager::class);

        return new JsonResponse($mutatebles, 200);
    }

    /**
     * Get maintenance mode..
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="maintenance_mode_status",
     *     path="/configuration/maintenance-mode",
     *     methods={"GET"},
     * )
     */
    public function maintenanceModeStatus(): JsonResponse
    {
        return new JsonResponse(['active' => $this->configurationService->inMaintenanceMode()], 200);
    }

    /**
     * Get system stats..
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_stats",
     *     path="/configuration/stats",
     *     methods={"GET"},
     * )
     */
    public function systemStats(): JsonResponse
    {
        $currentUser = $this->accessManager->getSecurity()->getUser();
        $stats = [];
        $forms = $this->formFactory->findByType(InquiryFormManager::class);

        if ($this->accessManager->hasPermission($currentUser, 'can see user stats')) {
            $stats['by_user'] = [];
            // Roles to look.
            $roles = ['ROLE_DIGITIZER', 'ROLE_HOUSEHOLD_DIGITIZER', 'ROLE_SECTORIAL_VALIDATOR'];
            // Get list of users.
            $userRepo = $this->em->getRepository(User::class);
            $systemUsers = $userRepo->findBy(['country' => $currentUser->getCountry()]);
            $users = [];
            foreach ($systemUsers as $systemUser) {
                foreach ($systemUser->getRoles() as $role) {
                    if (in_array($role, $roles)) {
                        $users[] = $systemUser;
                        break;
                    }
                }
            }
            // Get all inquiry types.
            foreach ($users as $user) {
                $stats['by_user'][$user->getUsername()] = [
                    'username' => $user->getUsername(),
                    'gravatar' => $user->getGravatar(),
                    'stats' => [],
                ];
                /** @var InquiryFormManager $form */
                foreach ($forms as $form) {
                    $meta = $form->getMeta();
                    if (isset($meta['interviewer_field'])) {
                        $interviwerFieldName = $meta['interviewer_field'];
                        $userUlid = Ulid::fromString($user->getId());
                        // Count total.
                        $total = $form->countBy([
                            $interviwerFieldName => $userUlid->toBinary(),
                        ]);
                        // Count draft.
                        $draft = $form->countBy([
                            $interviwerFieldName => $userUlid->toBinary(),
                            'field_status' => 'draft',
                        ]);
                        // Count finished.
                        $finished = $form->countBy([
                            $interviwerFieldName => $userUlid->toBinary(),
                            'field_status' => 'finished',
                        ]);
                        // validated
                        $validated = $form->countBy([
                            $interviwerFieldName => $userUlid->toBinary(),
                            'field_status' => 'validated',
                        ]);
                        // locked
                        $locked = $form->countBy([
                            $interviwerFieldName => $userUlid->toBinary(),
                            'field_status' => 'locked',
                        ]);
                        // removed
                        $removed = $form->countBy([
                            $interviwerFieldName => $userUlid->toBinary(),
                            'field_status' => 'removed',
                        ]);

                        // Calculate others.
                        $stats['by_user'][$user->getUsername()]['stats'][$form->getId()] = [
                            'label' => self::t($form->getTitle()),
                            'total' => $total,
                            'draft' => $draft,
                            'finished' => $finished,
                            'validated' => $validated,
                            'locked' => $locked,
                            'removed' => $removed,
                            'zero_test' => ($total
                                - $draft
                                - $finished
                                - $validated
                                - $locked
                                - $removed
                            ),
                        ];
                    }
                }
            }
        }
        if ($this->accessManager->hasPermission($currentUser, 'can see inquiries stats')) {
            // Points stats.
            $pointForm = $this->formFactory->find('form.point');
            $total = $pointForm->countBy([]);
            // planning
            $planning = $pointForm->countBy(['field_status' => 'planning']);
            // digitizing
            $digitizing = $pointForm->countBy(['field_status' => 'digitizing']);
            // complete
            $complete = $pointForm->countBy(['field_status' => 'complete']);
            // checking
            $checking = $pointForm->countBy(['field_status' => 'checking']);
            // reviewing
            $reviewing = $pointForm->countBy(['field_status' => 'reviewing']);
            // calculating
            $calculating = $pointForm->countBy(['field_status' => 'calculating']);
            // calculated
            $calculated = $pointForm->countBy(['field_status' => 'calculated']);

            $stats['points'] = [
                'total' => $total,
                'planning' => $planning,
                'digitizing' => $digitizing,
                'complete' => $complete,
                'checking' => $checking,
                'reviewing' => $reviewing,
                'calculating' => $calculating,
                'calculated' => $calculated,
                'zero_test' => ($total
                    - $planning
                    - $digitizing
                    - $complete
                    - $checking
                    - $reviewing
                    - $calculating
                    - $calculated
                ),
                //'other' => ($total - $digitizing - $reviewing),
            ];

            // Inquiries stats.
            /** @var InquiryFormManager $form */
            foreach ($forms as $form) {
                // Count total.
                $total = $form->countBy([]);
                // Count draft.
                $draft = $form->countBy(['field_status' => 'draft']);
                // Count finished.
                $finished = $form->countBy(['field_status' => 'finished']);
                // validated
                $validated = $form->countBy(['field_status' => 'validated']);
                // locked
                $locked = $form->countBy(['field_status' => 'locked']);
                // removed
                $removed = $form->countBy(['field_status' => 'removed']);
                // Calculate others.
                $stats['inquiries'][$form->getId()] = [
                    'label' => self::t($form->getTitle()),
                    'total' => $total,
                    'draft' => $draft,
                    'finished' => $finished,
                    'validated' => $validated,
                    'locked' => $locked,
                    'removed' => $removed,
                    'zero_test' => ($total
                        - $draft
                        - $finished
                        - $validated
                        - $locked
                        - $removed
                    ),
                ];
            }

            // Household process stats.
            $hhProcessRepo = $this->em->getRepository(HouseholdProcess::class);
            $hhTotal = $hhProcessRepo->count([]);
            $hhOpen = $hhProcessRepo->count(['open' => true]);
            $hhClose = $hhProcessRepo->count(['open' => false]);
            $hhZeroTest = $hhTotal - $hhOpen - $hhClose;
            $stats['hhprocess'] = [
                'total' => $hhTotal,
                'open' => $hhOpen,
                'close' => $hhClose,
                'zero_test' => $hhZeroTest,
            ];
        }

        return new JsonResponse($stats, 200);
    }

    /**
     * Get API version.
     *
     * @return JsonResponse
     */
    public function getVersion(): JsonResponse
    {
        return new JsonResponse(
            [
                "title" => $this->systemSettings->get('api_platform.title'),
                "description" => $this->systemSettings->get('api_platform.description'),
                "version" => $this->systemSettings->get('api_platform.version'),
                "terms_of_service" => $this->systemSettings->get('api_platform.openapi.termsOfService'),
                "licence" => sprintf('%s <%s>', $this->systemSettings->get('api_platform.openapi.license.name'), $this->systemSettings->get('api_platform.openapi.license.url')),
            ],
            200
        );
    }

    /**
     * Get system.languages values.
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="system_languages",
     *     path="/configuration/system.languages",
     *     methods={"GET"},
     * )
     */
    public function systemLanguages(): JsonResponse
    {
        $allowed = $this->isAllowed('system.languages');
        if ($allowed instanceof JsonResponse) {
            return $allowed;
        }

        $cfg = $this->configurationRepository->find('system.languages');

        return new JsonResponse([$cfg->getValue()], 200);
    }

    /**
     * Add label property with the key translated to the user language.
     *
     * @param array $units Units list.
     *
     * @return array
     */
    protected function translateUnits(array $units): array
    {
        $resp = [];
        foreach ($units as $key => $unit) {
            $resp[$key] = $unit + ['label' => $this->t($key, [], ['context' => 'measurement unit'])];
        }

        return $resp;
    }

    /**
     * Can user see this configuration key?
     *
     * @param string $key
     *
     * @return ?JsonResponse
     */
    protected function isAllowed(string $key): ?JsonResponse
    {
        $allowed = $this->accessManager->getSecurity()->isGranted(
            ConfigurationVoter::READ,
            [ConfigurationVoter::SUBJECT_ARRAY_KEY => $key]
        );
        if (!$allowed) {
            return new JsonResponse(
                $this->t(
                    'To see "@key" configuration, the permission "@perm" is required.',
                    [
                        '@key' => $key,
                        '@perm' => 'read public configuration',
                    ]
                ),
                403
            );
        }

        return null;
    }
}
