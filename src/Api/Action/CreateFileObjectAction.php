<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Country;
use App\Entity\File;
use App\RepositorySecured\FileRepositorySecured;
use App\Traits\StringTranslationTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Create File Object Action.
 */
class CreateFileObjectAction extends AbstractController
{
    use StringTranslationTrait;

    protected IriConverterInterface $iriConverter;
    protected FileRepositorySecured $fileRepositorySecured;

    /**
     * CreateFileObjectAction constructor.
     *
     * @param IriConverterInterface $iriConverter
     * @param FileRepositorySecured $fileRepositorySecured
     */
    public function __construct(IriConverterInterface $iriConverter, FileRepositorySecured $fileRepositorySecured)
    {
        $this->iriConverter = $iriConverter;
        $this->fileRepositorySecured = $fileRepositorySecured;
    }

    /**
     * Controller callable.
     *
     * @Route(
     *     name="file_upload",
     *     path="/files",
     *     methods={"POST"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function fileUpload(Request $request): JsonResponse
    {
        try {
            // Default error message.
            $message = $this->t('Error while creating file.');

            if (!$request->get('country')) {
                $message = $this->t('Country value is required.');
            } else {
                $country = $this->iriConverter->getItemFromIri($request->get('country'));

                /** @var UploadedFile $uploadedFile */
                $uploadedFile = $request->files->get('file');
                if (!$uploadedFile) {
                    throw new BadRequestHttpException('"file" is required');
                }

                $file =  $this->fileRepositorySecured->create($uploadedFile, $country);
                if ($file) {
                    return new JsonResponse(['id' => $file->getId()], 201);
                }
            }
        } catch (\Exception $e) {
            $message = $this->t('Error while creating file: @msg.', ['@msg' => $e->getMessage()]);
        }

        return new JsonResponse([$message], 422);
    }
}
