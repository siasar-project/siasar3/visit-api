<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

/**
 * Pharinix File Manager Copyright (C) 2016 Pedro Pelaez <aaaaa976@gmail.com>
 * Sources https://github.com/PSF1/pharinix_mod_file_manager
 *
 * GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007
 */

namespace App\Api\Action;

use App\Entity\Configuration\ConfigurationRead;
use App\Entity\File;
use App\Entity\User;
use App\Repository\ConfigurationRepository;
use App\RepositorySecured\FileRepositorySecured;
use App\Service\FileSystem;
use App\Service\Password\EncoderService;
use App\Service\SessionService;
use App\Tools\ExceptionNormalizer;
use App\Tools\ImageOptimizer;
use App\Tools\ImportOfflinePack;
use App\Tools\Json;
use App\Tools\ParametricImporter;
use App\Tools\TemplateGenerator;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use App\Worker\Console;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Create File Object Action.
 */
class DownloadFileObjectAction extends AbstractController
{
    use getContainerTrait;
    use StringTranslationTrait;

    protected FileSystem $fileSystem;
    protected TemplateGenerator $templates;
    protected ConfigurationRepository $config;
    protected ImageOptimizer $imageOptimizer;
    protected SessionService $sessionService;
    protected FileRepositorySecured $fileRepositorySecured;
    protected EncoderService $encoderService;
    protected Connection $connection;
    protected EntityManagerInterface|null $entityManager;

    /**
     * Text collected by callbacks.
     *
     * @var string
     */
    protected string $callbackOutput = "";

    /**
     * Download file action.
     *
     * @param FileSystem              $fileSystem
     * @param TemplateGenerator       $templates
     * @param ConfigurationRepository $config
     * @param ImageOptimizer          $imageOptimizer
     * @param SessionService          $sessionService
     * @param FileRepositorySecured   $fileRepositorySecured
     * @param EntityManagerInterface  $entityManager
     * @param EncoderService          $encoderService        Encoder service.
     * @param Connection              $connection
     */
    public function __construct(FileSystem $fileSystem, TemplateGenerator $templates, ConfigurationRepository $config, ImageOptimizer $imageOptimizer, SessionService $sessionService, FileRepositorySecured $fileRepositorySecured, EntityManagerInterface $entityManager, EncoderService $encoderService, Connection $connection)
    {
        $this->fileSystem = $fileSystem;
        $this->templates = $templates;
        $this->config = $config;
        $this->imageOptimizer = $imageOptimizer;
        $this->sessionService = $sessionService;
        $this->fileRepositorySecured = $fileRepositorySecured;
        $this->entityManager = $entityManager;
        $this->encoderService = $encoderService;
        $this->connection = $connection;
    }

    /**
     * Download selected file.
     *
     * @param Request $request
     * @param File    $file
     *
     * @return Response
     *
     * @throws \Exception
     *
     * @Route(
     *     name="file_download",
     *     path="/files/{id}/download",
     *     methods={"GET"},
     * )
     */
    public function fileDownload(Request $request, File $file): Response
    {
        if ($file) {
            $width = $request->query->get('width');

            return $this->downloadFile($file, 32 * 1024, true, $width);
        }

        return new Response('', 404);
    }

    /**
     * Download a parametric template.
     *
     * @param string $entityName
     *
     * @return Response
     *
     * @Route(
     *     name="template_download",
     *     path="/configuration/templates/{entityName}",
     *     methods={"GET"},
     * )
     */
    public function templateDownload(string $entityName): Response
    {
        if ($entityName) {
            /** @var ConfigurationRead $cfg */
            $cfg = $this->config->find('system.upload.templates');
            if ($cfg) {
                $cfgValue = $cfg->getValue();
                if (isset($cfgValue['App\\Entity\\'.$entityName])) {
                    $conf = $cfgValue['App\\Entity\\'.$entityName];
                    $filePath = $conf['file'];

                    return $this->downloadFileByPath($this->fileSystem->getPublicFolder().'/../'.$filePath, 32 * 1024, true);
                }
            }
        }

        return new Response('', 404);
    }

    /**
     * Import a parametric CSV.
     *
     * @param Request $request
     * @param string  $entityName
     *
     * @return Response
     *
     * @Route(
     *     name="import_parametrics",
     *     path="/configuration/import/{entityName}",
     *     methods={"POST"},
     * )
     */
    public function parametricImport(Request $request, string $entityName): Response
    {
        $errorMessage = self::t('Entity @name not found', ['@name' => $entityName]);
        if ($entityName) {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');
            $fileMime = $this->fileSystem->getMimeByExt($file->getClientOriginalName());
            if (!$file) {
                $errorMessage = self::t('File not uploaded');
            } else {
                if ($file->getError() !== UPLOAD_ERR_OK) {
                    $errorMessage = $file->getErrorMessage();
                } elseif ('application/csv' !== $fileMime && 'text/csv' !== $fileMime) {
                    $errorMessage = self::t('File must be an "application/csv", but is "@mime".', ['@mime' => $fileMime]);
                } else {
                    $this->callbackOutput = "";
                    /** @var User $user */
                    $user = $this->sessionService->getUser();
                    $importer = new ParametricImporter($user->getCountryId(), $entityName, $file->getRealPath(), false);
                    $importer->setErrorCallback([$this, 'errorCallback']);
                    $importer->setInfoCallback([$this, 'infoCallback']);
                    $importer->setWarningCallback([$this, 'warningCallback']);
                    $importer->setTextCallback([$this, 'textCallback']);

                    // Validate file and if ok program a queue job.
                    if ($importer->import(false, true)) {
                        /** @var File $file */
                        $file =  $this->fileRepositorySecured->create($file->getRealPath(), $user->getCountry());
                        // Create queue job.
                        // console doctrine:import:parametric hn AdministrativeDivision "imports/Panama/limpio/01_AdministrativeDivision_template.csv"
                        /** @var Console $job */
                        $job = self::getContainerInstance()->get('App\Worker\Console');
                        $job->later()->execute(
                            'doctrine:import:parametric',
                            sprintf(
                                '%s %s %s %s',
                                '--no-validate',
                                $user->getCountryId(),
                                $entityName,
                                $this->fileSystem->getPublicFolder().'/'.$file->getPath()
                            )
                        );

                        return new Response(self::t($this->callbackOutput."\r\n".'Import scheduled.'), 201);
                    }

                    $errorMessage = $this->callbackOutput;

                    return new Response($errorMessage, 422);
                }
            }
        }

        return new Response($errorMessage, 404);
    }

    /**
     * Import an offline back ZIP package.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @Route(
     *     name="import_inquiries",
     *     path="/form/data/inquiries",
     *     methods={"POST"},
     * )
     */
    public function inquiriesImport(Request $request): Response
    {
        // User identity.
        $username = $request->get('username');
        $password = $request->get('password');
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneByUserName($username);
        if (!$user) {
            return new Response(self::t('User or password not valid'), 401);
        }
        if (!password_verify($password, $user->getPassword())) {
            return new Response(self::t('User or password not valid'), 401);
        }
        // Start user session.
        $this->sessionService->loginFakeUser($username);
        // FIle mime type application/zip
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        $this->connection->superBeginTransaction();
        try {
            $importer = new ImportOfflinePack($file, $this->entityManager);
            $importer->import();
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();
            $errObj = ExceptionNormalizer::normalize($e);
            $json = Json::encode($errObj);
            $code = $e->getCode();
            if (400 !== $code) {
                $code = 422;
            }

            return new JsonResponse($json, $code, [], true);
        }

        return new JsonResponse(['message' => self::t('Offline pack imported.')], 201, [], false);
    }

    /**
     * Add error text to callback output.
     *
     * @param string $msg
     *
     * @return void
     */
    public function errorCallback(string $msg)
    {
        $this->callbackOutput .= '[error] '.$msg."\r\n";
    }

    /**
     * Add info text to callback output.
     *
     * @param string $msg
     *
     * @return void
     */
    public function infoCallback(string $msg)
    {
        $this->callbackOutput .= '[info] '.$msg."\r\n";
    }

    /**
     * Add warning text to callback output.
     *
     * @param string $msg
     *
     * @return void
     */
    public function warningCallback(string $msg)
    {
        $this->callbackOutput .= '[warning] '.$msg."\r\n";
    }

    /**
     * Add raw text to callback output.
     *
     * @param string $msg
     * @param bool   $newline
     *
     * @return void
     */
    public function textCallback(string $msg, bool $newline = false)
    {
        $this->callbackOutput .= $msg.($newline ? "\r\n" : "");
    }

    /**
     * Send file content to client.
     *
     * http://stackoverflow.com/a/13821992
     *
     * Parameters: downloadFile(File Location, File Name, max speed, is
     * streaming If streaming - videos will show as videos, images as images
     * instead of download prompt.
     *
     * @param File     $file     File instance.
     * @param int      $maxSpeed Maximum download speed.
     * @param bool     $doStream Work how stream.
     * @param int|null $width    Width to resize if the file is an image.
     *
     * @return ?StreamedResponse
     *
     * @throws \Exception
     */
    protected function downloadFile(File &$file, int $maxSpeed = 100, bool $doStream = false, int $width = null): ?StreamedResponse
    {
        if (connection_status() !== 0) {
            return null;
        }

        $response = new StreamedResponse();

        $contentType = $this->fileSystem->getMimeByExt($file->getFileName());
        $isImage = (stripos($contentType, 'image') !== false) && !is_null($width);

        $response->headers->set('Cache-Control', 'public');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Type', $contentType);

        $contentDisposition = 'attachment';

        if ($doStream) {
            /* extensions to stream */
            // $array_listen = array('mp3', 'm3u', 'm4a', 'mid', 'ogg', 'ra', 'ram', 'wm',
            // 'wav', 'wma', 'aac', '3gp', 'avi', 'mov', 'mp4', 'mpeg', 'mpg', 'swf', 'wmv', 'divx', 'asf');
            // if (in_array($extension, $array_listen)) {
            $contentDisposition = 'inline';
            // }
        }
        $response->headers->set('Content-Disposition', "$contentDisposition;filename=\"{$file->getFileName()}\"");
        $response->headers->set('Accept-Ranges', 'bytes');
        $range = 0;
        $filePath = $file->getPath();
        $size = $file->getSize();
        if ($isImage) {
            $filePath = $this->imageOptimizer->resize($file->getPath(), $width, -100);
            $size = filesize($filePath);
        }

        if (isset($_SERVER['HTTP_RANGE'])) {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            str_replace($range, "-", $range);
            $size2 = $size - 1;
            $newLength = $size - $range;
            $response->setStatusCode(206, 'Partial Content');
            $response->headers->set('Content-Length', $newLength);
            $response->headers->set('Content-Range', "bytes $range$size2/$size");
        } else {
            $size2 = $size - 1;
            $response->headers->set('Content-Range', 'bytes 0-'.$size2.'/'.$size);
            $response->headers->set('Content-Length', $size);
        }

        if (0 === $size) {
            throw new \Exception($this->t('Zero byte file! Aborting download'));
        }

        $response->setCallback(function () use ($file, $range, $maxSpeed, $isImage, $filePath) {
            if ($isImage) {
                $fp = fopen($filePath, "rb");
            } else {
                $fp = fopen('files://'.$file->getPath(), "rb");
            }

            fseek($fp, str_replace('-', '', $range));

            while (!feof($fp) and (connection_status() === 0)) {
                set_time_limit(0);
                print(fread($fp, 1024 * $maxSpeed));
                flush();
                // Allow run tests in process isolation mode.
                if ('test' !== $_ENV['APP_ENV']) {
                    ob_flush();
                }
            }
            fclose($fp);
        });

        return $response;
    }

    /**
     * Send file content to client.
     *
     * http://stackoverflow.com/a/13821992
     *
     * Parameters: downloadFile(File Location, File Name, max speed, is
     * streaming If streaming - videos will show as videos, images as images
     * instead of download prompt.
     *
     * @param string $file     File path.
     * @param int    $maxSpeed Maximum download speed.
     * @param bool   $doStream Work how stream.
     *
     * @return ?StreamedResponse
     */
    protected function downloadFileByPath(string $file, int $maxSpeed = 100, bool $doStream = false): ?StreamedResponse
    {
        if (connection_status() !== 0) {
            return null;
        }

        $response = new StreamedResponse();

        $contentType = $this->fileSystem->getMimeByExt($file);

        $response->headers->set('Cache-Control', 'public');
        $response->headers->set('Content-Transfer-Encoding', 'binary');
        $response->headers->set('Content-Type', $contentType);

        $contentDisposition = 'attachment';

        if ($doStream) {
            /* extensions to stream */
            // $array_listen = array('mp3', 'm3u', 'm4a', 'mid', 'ogg', 'ra', 'ram', 'wm',
            // 'wav', 'wma', 'aac', '3gp', 'avi', 'mov', 'mp4', 'mpeg', 'mpg', 'swf', 'wmv', 'divx', 'asf');
            // if (in_array($extension, $array_listen)) {
            $contentDisposition = 'inline';
            // }
        }
        $response->headers->set('Content-Disposition', "$contentDisposition;filename=\"{$this->fileSystem->getFileNameWithExtension($file)}\"");
        $response->headers->set('Accept-Ranges', 'bytes');
        $range = 0;
        $size = filesize($file);

        if (isset($_SERVER['HTTP_RANGE'])) {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            str_replace($range, "-", $range);
            $size2 = $size - 1;
            $newLength = $size - $range;
            $response->setStatusCode(206, 'Partial Content');
            $response->headers->set('Content-Length', $newLength);
            $response->headers->set('Content-Range', "bytes $range$size2/$size");
        } else {
            $size2 = $size - 1;
            $response->headers->set('Content-Range', 'bytes 0-'.$size2.'/'.$size);
            $response->headers->set('Content-Length', $size);
        }

        if (0 === $size) {
            throw new \Exception($this->t('Zero byte file! Aborting download'));
        }

        $response->setCallback(function () use ($file, $range, $maxSpeed) {
            $fp = fopen($file, "rb");

            fseek($fp, str_replace('-', '', $range));

            while (!feof($fp) and (connection_status() === 0)) {
                set_time_limit(0);
                print(fread($fp, 1024 * $maxSpeed));
                flush();
                // Allow run tests in process isolation mode.
                if ('test' !== $_ENV['APP_ENV']) {
                    ob_flush();
                }
            }
            fclose($fp);
        });

        return $response;
    }
}
