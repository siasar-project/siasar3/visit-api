<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Entity\InquiryFormLog;
use App\Entity\TechnicalAssistanceProvider;
use App\Entity\User;
use App\Entity\UserReadMail;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\PointFormManager;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;
use App\OpenApi\OpenApiFactory;
use App\RepositorySecured\InquiryFormLogRepositorySecured;
use App\Service\SessionService;
use App\Tools\ExceptionNormalizer;
use App\Tools\Json;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception as DoctrineException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Ulid;

/**
 * Forms collections controllers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormCollectionAction
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected array $formByIdCache = [];

    protected FormFactory $formFactory;
    protected ParameterBagInterface $systemSettings;
    protected LoggerInterface $inquiryFormLogger;
    protected SessionService $sessionService;
    protected Connection $connection;
    protected IriConverterInterface $iriConverter;
    protected EntityManagerInterface|null $entityManager;

    /**
     * Form collection action constructor.
     *
     * @param FormFactory            $formFactory       Form factory.
     * @param LoggerInterface        $inquiryFormLogger
     * @param SessionService         $sessionService
     * @param Connection             $connection
     * @param IriConverterInterface  $iriConverter
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(FormFactory $formFactory, LoggerInterface $inquiryFormLogger, SessionService $sessionService, Connection $connection, IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        $this->formFactory = $formFactory;
        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
        $this->inquiryFormLogger = $inquiryFormLogger;
        $this->sessionService = $sessionService;
        $this->connection = $connection;
        $this->iriConverter = $iriConverter;
        $this->entityManager = $entityManager;
    }

    /**
     * Get inquiries collection.
     *
     * Used by function self::get(Request $request, string $formId).
     * @see FormCollectionAction::get()
     *
     * This method don't use forms data control logic, it only mimics it.
     *
     * WARNING: This method use direct MariaDB SQL query.
     *
     * OpenApi definition created at OpenApiFactory decorator.
     * @see OpenApiFactory::__invoke(), line ~174. (Search '// Inquiries form schema.')
     * @see OpenApiFactory::__invoke(), line ~498. (Search '// Inquiries field orders.')
     * @see OpenApiFactory::__invoke(), line ~528. (Search '// Inquiries field filters.')
     *
     * @param Request $request Petition request.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     */
    public function getInquiries(Request $request): Response
    {
        // Load forms.
        $inquiryForms = $this->formFactory->findByType(InquiryFormManager::class);
        // Process parameters.
        $parameters = $this->getRequestParameters($request);
        if (isset($parameters["filter_by"]["field_creator"])) {
            $ulid = Ulid::fromString($parameters["filter_by"]["field_creator"]);
            $parameters["filter_by"]['grp1']['or'] = [
                'field_creator' => $ulid->toBinary(),
            ];
            unset($parameters["filter_by"]["field_creator"]);
        }
        $offset = $parameters['offset'];
        $itemsPerPage = $parameters['limit'];
        $orderBy = $parameters['order_by'];
        $filterBy = $parameters['filter_by'];
        $filterRecordType = [];
        // Remember filter record_type and remove it.
        // This filter it's not applicable in database.
        if (isset($filterBy["record_type"])) {
            $filterRecordType = explode(',', $filterBy["record_type"]);
            unset($filterBy["record_type"]);
        }
        if (isset($filterBy["field_status"])) {
            $filterRecordStatus = explode(',', $filterBy["field_status"]);
            unset($filterBy["field_status"]);
            foreach ($filterRecordStatus as &$st) {
                $st = "'{$st}'";
            }
            $filterBy["field_status"] = implode(',', $filterRecordStatus);
        }
        $filterBy["field_deleted"] = '0';
//        // Fix field_region filter, in database have another name.
//        if (isset($filterBy["field_region"])) {
//            $filterBy["field_region"] = ['IN' => [$filterBy["field_region"]]];
////            unset($filterBy["field_region"]);
//        }
        // Log action.
        // $this->logAction(
        //     'get inquiry forms collection',
        //     [
        //         'form_id' => 'Inquiries',
        //         'filter_by' => $filterBy,
        //         'order_by' => $orderBy,
        //         'limit' => $itemsPerPage,
        //         'offset' => $offset,
        //         'record_id' => '',
        //     ]
        // );
        // Query.
        $selects = [];
        $fields = implode(
            ', ',
            [
                '[table].id',
                '[table].field_reference',
                '[table].field_ulid_reference',
                '[table].field_validated',
                '[table].field_deleted',
                '[table].field_status',
                '[table].field_country_value',
                '[table].field_region_value',
                '[table].field_created_value',
                '[table].field_changed_value',
            ]
        );
        $adminDivRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var InquiryFormManager $inquiryForm */
        foreach ($inquiryForms as $inquiryForm) {
            // Add extra SQL if the form is in the filter or (not filter and form have collection get).
            if ((!$filterRecordType && $inquiryForm->haveEndpoint(FormManager::ENDPOINT_COLLECTION_GET)) || in_array($inquiryForm->getId(), $filterRecordType)) {
                // Honor user access conditions.
                $criteria = $filterBy;
                if (isset($criteria['grp1']['or']['field_creator'])) {
                    // Add interviewer how other user option to include in filter.
                    $meta = $inquiryForm->getMeta();
                    if (isset($meta['interviewer_field'])) {
                        $interviewerField = $meta['interviewer_field'];
                        $criteria['grp1']['or'][$interviewerField] = $criteria['grp1']['or']['field_creator'];
                    }
                }
                if (isset($criteria["field_region"])) {
                    // Find region.
                    $region = $adminDivRepo->find($criteria["field_region"]);
                    //$filter['alike_start']['field_region'] = ['branch value', 'branch value']
                    $criteria['branch']['alike_start']['field_region'] = [$region->getBranch()];
                    unset($criteria["field_region"]);
                }
                $criteria = $inquiryForm->updateReadCriteria($criteria);

                // Build sub-query.
                $subQuery = sprintf(
                    'select %s, \'%s\' as form_id, \'%s\' as form_type from %s '.
                          'left join administrative_division ON %s.field_region_value = administrative_division.id',
                    str_replace('[table]', $inquiryForm->getTableName(), $fields),
                    $inquiryForm->getId(),
                    str_replace('\\', '\\\\', $inquiryForm->getType()),
                    $inquiryForm->getTableName(),
                    $inquiryForm->getTableName()
                );
                $selects[] = $subQuery.' where'.$this->addFilters($criteria, $inquiryForm).' group by field_ulid_reference';
            }
        }
        // Joint selects.
        $sql = 'select * from ('.implode(' union ', $selects).') as t';
        // Apply order by.
        $orderLine = [];
        foreach ($orderBy as $field => $order) {
            switch ($field) {
                case 'field_region':
                case 'field_country':
                case 'field_created':
                case 'field_changed':
                    $aux = '_value';
                    $orderLine[] = " $field$aux $order";
                    break;
                default:
                    $orderLine[] = " $field $order";
                    break;
            }
        }
        if (count($orderLine) > 0) {
            $sql .= ' order by '.implode(', ', $orderLine);
        } else {
            $sql .= ' order by field_changed_value DESC';
        }
        // Apply pagination.
        $sql .= ' limit '.$offset.', '.$itemsPerPage;

        try {
            $stmt = $this->connection->prepare($sql);
            $result = $stmt->executeQuery();
            $results = $result->fetchAllAssociative();
        } catch (DBALException $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, false)), 500, [], true);
        }
        $resp = [];
        /** @var PointFormManager $pointManager */
        $pointManager = $this->formFactory->find('form.point');
        /** @var InquiryFormLogRepositorySecured $formLogRepo */
        $formLogRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        foreach ($results as $item) {
            $form = $this->getFormByIdCached($item['form_id']);
            $formMeta = $form->getMeta();
            // Find the most recent version.
            /** @var FormRecord $record */
            $record = current($form->findBy(
                [
                    'field_ulid_reference' => $item["field_ulid_reference"],
                    'field_deleted' => '0',
                ],
                [
                    'field_created' => 'desc',
                ],
                1
            ));
            $aRecord = $record->toArray();

            $item = new \stdClass();
            $item->id = $record->getId();
            $item->type = $form->getId();
            $item->{'type_title'} = self::t($form->getTitle());
            $item->{'field_reference'} = $aRecord["field_reference"];
            $item->{'field_ulid_reference'} = $aRecord["field_ulid_reference"];
            $item->{'field_validated'} = $aRecord["field_validated"];
            $item->{'field_deleted'} = $aRecord["field_deleted"];
            $item->{'field_status'} = $aRecord["field_status"];
            $item->{'field_country'} = $aRecord["field_country"];
            $item->{'field_region'} = $aRecord["field_region"];
            $item->{'field_created'} = $aRecord["field_changed"];
            $item->{'field_changed'} = $aRecord["field_changed"];
            $item->{'field_creator'} = $aRecord["field_creator"];
            $item->{'field_editors'} = $aRecord["field_editors"];
            $item->{'field_editors_update'} = $aRecord["field_editors_update"];
            if (isset($formMeta['icon_field']) && !empty($formMeta['icon_field'])) {
                $item->{'field_image'} = $aRecord[$formMeta['icon_field']];
            } else {
                $item->{'field_image'} = '';
            }
            if (isset($formMeta['title_field']) && !empty($formMeta['title_field'])) {
                $item->{'field_title'} = $aRecord[$formMeta['title_field']];
            } else {
                // TAP form type is an exception.
                if ('form.tap' === $record->getForm()->getId()) {
                    // todo 'field_provider_name' is hardcoded, it must be `$formMeta['title_field']`
                    /** @var TechnicalAssistanceProvider $tap */
                    $tap = $record->{'field_provider_name'};
                    $item->{'field_title'} = $tap->getName();
                } else {
                    $item->{'field_title'} = '';
                }
            }
            // Add point ID if exists.
            /** @var FormRecord $point */
            $point = null;
            $item->{'point'} = '';
            $item->{'point_status'} = '';
            switch ($item->type) {
                case 'form.community':
                    $point = current($pointManager->findByCommunity($item->id));
                    break;
                case 'form.wsprovider':
                    $point = current($pointManager->findByWsp($item->id));
                    break;
                case 'form.wssystem':
                    $point = current($pointManager->findByWSystem($item->id));
                    break;
            }
            if ($point) {
                $item->{'point'} = $point->getId();
                $item->{'point_status'} = $point->{'field_status'};
            }
            // Add indicator information if exists.
            $item->{'indicator'} = '';
            $item->{'indicator_value'} = AbstractIndicator::VALUE_NOT_CALCULATED;
            if (!$form->isPointAssociated()) {
                $class = $form->getIndicatorClass();
                if (!empty($class)) {
                    $context = new SimpleIndicatorContext($record);
                    /** @var AbstractIndicator $indicator */
                    $indicator = new $class($context);
                    $item->{'indicator'} = $indicator->getLabel(true);
                    $item->{'indicator_value'} = $indicator->getValue(true);
                }
            } else {
                $class = $form->getIndicatorClass();
                if (!empty($class) && $point) {
                    $context = new PointIndicatorContext($record, $point);
                    /** @var AbstractIndicator $indicator */
                    $indicator = new $class($context);
                    $item->{'indicator'} = $indicator->getLabel(true);
                    $item->{'indicator_value'} = $indicator->getValue(true);
                }
            }
            // Add errors and warnings.
            $recordErrors = $formLogRepo->count([
                'formId' => $record->getForm()->getId(),
                'recordId' => $record->getId(),
                'level' => 400,
            ]);
            $recordWarnings = $formLogRepo->count([
                'formId' => $record->getForm()->getId(),
                'recordId' => $record->getId(),
                'level' => 300,
            ]);
            $item->{'logs'} = [
                'errors' => $recordErrors,
                'warnings' => $recordWarnings,
            ];

            $resp[] = Json::encode($item);
        }

        return new JsonResponse('['.implode(', ', $resp).']', 200, [], true);
    }

    /**
     * Get collection.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_collections",
     *     path="/form/data/{formId}",
     *     methods={"GET"},
     * )
     */
    public function get(Request $request, string $formId): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);
        if ('inquirie' === $formId) {
            // If query Inquiries, use the dedicated method.
            return $this->getInquiries($request);
        }
        // Load form.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }
        // If set in the form configuration, use another controller to process this request.
        if ($alternative = $form->getEndpointRawValue(FormManager::ENDPOINT_COLLECTION_GET)) {
            if (is_array($alternative)) {
                $alternative = $alternative['controller'];
            }
            if (is_string($alternative)) {
                return call_user_func([$this, $alternative], $request, $formId);
            }
        }
        // Process parameters.
        $parameters = $this->getRequestParameters($request);
        $offset = $parameters['offset'];
        $itemsPerPage = $parameters['limit'];
        $orderBy = $parameters['order_by'];
        $filterBy = $parameters['filter_by'];
        $groupBy = $parameters['group_by'];
        // Log action.
        // $this->logAction(
        //     'get form collection',
        //     [
        //         'form_id' => $formId,
        //         'filter_by' => $filterBy,
        //         'order_by' => $orderBy,
        //         'group_by' => $groupBy,
        //         'limit' => $itemsPerPage,
        //         'offset' => $offset,
        //         'record_id' => '',
        //     ]
        // );
        // Query.
        $resp = $form->findBy($filterBy, $orderBy, $itemsPerPage, $offset, $groupBy);
        /** @var FormRecord $record */
        foreach ($resp as &$record) {
            $record = $record->toJson();
        }

        return new JsonResponse('['.implode(', ', $resp).']', 200, [], true);
    }

    /**
     * Get collection.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_coun_unread_mails",
     *     path="/form/data/{formId}/count_unread",
     *     methods={"GET"},
     * )
     */
    public function getCountUnreadMails(Request $request, string $formId): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);
        // Load form.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }
        // Process parameters.
        /** @var User $user */
        $user = $this->sessionService->getUser();
        $userRepo = $this->entityManager->getRepository(User::class);
        $readMailsRepo = $this->entityManager->getRepository(UserReadMail::class);
        /** @var User $user */
        $user = $userRepo->find($user->getId());
        $filterBy = [
            'field_is_child' => '0',
            'or' => [
                'field_to' => $user->getId(),
                'field_from' => $user->getId(),
                'field_global' => '1',
            ],
        ];
        // Query.
        $mails = $form->findBy($filterBy);
        $totalMails = count($mails);
        $totalRead = $readMailsRepo->count([
            'user' => $user,
            'isChild' => false,
        ]);
        $totalReadAA = $readMailsRepo->findBy([
            'user' => $user,
            'isChild' => false,
        ]);
        $resp = new \stdClass();
        $resp->count = $totalMails - $totalRead;

        return new JsonResponse($resp, 200, [], false);
    }

    /**
     * Get mail collection.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * path="/form/data/{formId}",
     * methods={"GET"},
     */
    public function getMails(Request $request, string $formId): Response
    {
        // Load form.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }
        // Process parameters.
        $parameters = $this->getRequestParameters($request);
        $offset = $parameters['offset'];
        $itemsPerPage = $parameters['limit'];
        $orderBy = $parameters['order_by'];
        $filterBy = $parameters['filter_by'];
        $groupBy = $parameters['group_by'];
        // Query with read marks.
        $user = $this->sessionService->getUser();
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->find($user->getId());
        $resp = $form->findBy($filterBy, $orderBy, $itemsPerPage, $offset, $groupBy);
        $readMarkRepo = $this->entityManager->getRepository(UserReadMail::class);
        /** @var FormRecord $record */
        foreach ($resp as &$record) {
            $marks = $readMarkRepo->findBy([
                'user' => $user,
                'mail' => $record->getId(),
            ]);
            $respRecord = $record->toArray();
            if (count($marks) > 0) {
                $respRecord["field_is_read"] = '1';
            }
            $nThreads = $record->getCurrentRaw();
            $respRecord["responses"] = !$nThreads["field_thread"] ? 0 : count($nThreads["field_thread"]);
            $record = json_encode($respRecord, JSON_THROW_ON_ERROR);
        }

        return new JsonResponse('['.implode(', ', $resp).']', 200, [], true);
    }

    /**
     * Insert a new form record.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     *
     * @return Response
     *
     * @throws \Exception
     *
     * @Route(
     *     name="form_collections_post",
     *     path="/form/data/{formId}",
     *     methods={"POST"},
     * )
     */
    public function post(Request $request, string $formId): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);
        $requestBody = $request->getContent();
        try {
            $newRecord = (new Json())->decode($requestBody);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 400, [], true);
        }

        // Get form and insert the new record.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }
        // If set in the form configuration, use another controller to process this request.
        if ($alternative = $form->getEndpointRawValue(FormManager::ENDPOINT_COLLECTION_POST)) {
            if (is_array($alternative)) {
                $alternative = $alternative['controller'];
            }
            if (is_string($alternative)) {
                return call_user_func([$this, $alternative], $request, $formId);
            }
        }
        $this->connection->superBeginTransaction();
        try {
            // Log action.
            // $this->logAction('insert new record', ['form_id' => $formId, 'record_id' => '', 'post_data' => $requestBody]);
            $newId = $form->insert($newRecord);
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();
            $errObj = ExceptionNormalizer::normalize($e);
            $json = Json::encode($errObj);

            return new JsonResponse($json, 422, [], true);
        }

        // Load & send response.
        $record = $form->find($newId);

        return new JsonResponse($record->toJson(), 201, [], true);
    }

    /**
     * Get item.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     * @param string  $id      Record ID.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_get_item",
     *     path="/form/data/{formId}/{id}",
     *     methods={"GET"},
     * )
     */
    public function getItem(Request $request, string $formId, string $id): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);

        // Get form and find the record.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }
        // If set in the form configuration, use another controller to process this request.
        if ($alternative = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_GET)) {
            if (is_array($alternative)) {
                $alternative = $alternative['controller'];
            }
            if (is_string($alternative)) {
                return call_user_func([$this, $alternative], $request, $formId, $id);
            }
        }
        // Log action.
        // $this->logAction('get record', ['form_id' => $formId, 'record_id' => $id]);
        try {
            $record = $form->find($id);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }

        // Send response.
        if (!$record) {
            return new Response('', 404);
        }

        return new JsonResponse($record->toJson(), 200, [], true);
    }

    /**
     * Get mail.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     * @param string  $id      Record ID.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * path="/form/data/{formId}/{id}"
     * methods={"GET"}
     */
    public function getMail(Request $request, string $formId, string $id): Response
    {
        // Get form and find the record.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }

        try {
            $record = $form->find($id);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }

        // Send response.
        if (!$record) {
            return new Response('', 404);
        }
        // Add read mark.
        $respRecord = $record->toArray();
        $this->markReadMail($respRecord, $record);

        return new JsonResponse(json_encode($respRecord, JSON_THROW_ON_ERROR), 200, [], true);
    }

    /**
     * Put item.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     * @param string  $id      Record ID.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_put_item",
     *     path="/form/data/{formId}/{id}",
     *     methods={"PUT", "PATCH"},
     * )
     */
    public function putItem(Request $request, string $formId, string $id): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);
        $requestBody = $request->getContent();
        try {
            $updatedRecord = (new Json())->decode($requestBody);
            // Get form and find the record.
            $form = $this->formFactory->find($formId);
            // If set in the form configuration, use another controller to process this request.
            $endpointType = FormManager::ENDPOINT_ITEM_PUT;
            if ('PATCH' === $request->getMethod()) {
                $endpointType = FormManager::ENDPOINT_ITEM_PATCH;
            }
            if ($alternative = $form->getEndpointRawValue($endpointType)) {
                if (is_array($alternative)) {
                    $alternative = $alternative['controller'];
                }
                if (is_string($alternative)) {
                    return call_user_func([$this, $alternative], $request, $formId, $id);
                }
            }
            $record = $form->find($id);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 400, [], true);
        }

        // Log action.
        // $this->logAction('update record', ['form_id' => $formId, 'record_id' => $id, 'updates' => $updatedRecord]);

        if (!$record) {
            return new JsonResponse(["message" => self::t('Record not found.')], 404, []);
        }

        $this->connection->superBeginTransaction();
        try {
            foreach ($updatedRecord as $fieldName => $value) {
                if ('id' !== $fieldName && 'form' !== $fieldName) {
                    $record->set($fieldName, $value);
                }
            }
            $record->save();
            $this->connection->superCommit();

            $record = $form->find($id);
            $originalData = $record->getOriginal();
            if (isset($updatedRecord['field_status'])) {
                if ('finished' === $updatedRecord['field_status'] && 'draft' === $originalData['field_status']['value']) {
                    return new JsonResponse(Json::encode($this->t("Validation at finish failed.")), 422, [], true);
                }
            }
        } catch (\Exception $e) {
            $this->connection->superRollBack();

            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, false)), 422, [], true);
        }

        return new JsonResponse($record->toJson(), 200, [], true);
    }

    /**
     * Clone item.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     * @param string  $id      Record ID.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_clone_item",
     *     path="/form/data/{formId}/{id}/clone",
     *     methods={"PUT", "PATCH"},
     * )
     */
    public function cloneItem(Request $request, string $formId, string $id): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);
        try {
            // Get form and find the record.
            $form = $this->formFactory->find($formId);
            // If set in the form configuration, use another controller to process this request.
            $endpointType = FormManager::ENDPOINT_ITEM_CLONE;
            if ($alternative = $form->getEndpointRawValue($endpointType)) {
                if (is_array($alternative)) {
                    $alternative = $alternative['controller'];
                }
                if (is_string($alternative)) {
                    return call_user_func([$this, $alternative], $request, $formId, $id);
                }
            }
            $record = $form->find($id);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 400, [], true);
        }

        // Log action.
        // $this->logAction('clone record', ['form_id' => $formId, 'record_id' => $id]);

        if (!$record) {
            return new JsonResponse(["message" => self::t('Record not found.')], 404, []);
        }

        if (!in_array($record->{'field_status'}, ['validated', 'locked']) || $record->{'field_deleted'}) {
            return new JsonResponse(["message" => self::t("This record can't be cloned.")], 400, []);
        }

        if ('locked' === $record->{'field_status'}) {
            // A record with a clone in draft status can't have a new clone.
            $ulid = Ulid::fromString($record->{'field_ulid_reference'});
            $cnt = $form->countBy(
                [
                    'field_ulid_reference' => $ulid->toBinary(),
                    'field_status' => ['IN' => ['draft', 'finished']],
                    'field_deleted' => '0',
                ]
            );
            if ($cnt > 0) {
                return new JsonResponse(["message" => self::t("This record can't be cloned.")], 400, []);
            }
        }

        $this->connection->superBeginTransaction();
        try {
            $clonedRecord = $record->clone();
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();

            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, false)), 422, [], true);
        }

        return new JsonResponse($clonedRecord->toJson(), 200, [], true);
    }

    /**
     * History item.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     * @param string  $id      Record ID.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_history_item",
     *     path="/form/data/{formId}/{id}/history",
     *     methods={"GET"},
     * )
     */
    public function historyItem(Request $request, string $formId, string $id): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);
        try {
            // Get form and find the record.
            $form = $this->formFactory->find($formId);
            // If set in the form configuration, use another controller to process this request.
            $endpointType = FormManager::ENDPOINT_ITEM_HISTORY;
            if ($alternative = $form->getEndpointRawValue($endpointType)) {
                if (is_array($alternative)) {
                    $alternative = $alternative['controller'];
                }
                if (is_string($alternative)) {
                    return call_user_func([$this, $alternative], $request, $formId, $id);
                }
            }
            $record = $form->find($id);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 400, [], true);
        }

        if (!$record) {
            return new JsonResponse(["message" => self::t('Record not found.')], 404, []);
        }

        $ulid = Ulid::fromString($record->{'field_ulid_reference'});
        $result = $form->findBy([
            'field_ulid_reference' => $ulid->toBinary(),
            ], [
            'field_created' => 'DESC',
        ]);

        $resp = [];
        /** @var FormRecord $item */
        foreach ($result as $item) {
            $resp[] = $item->toJson();
        }

        return new JsonResponse('['.implode(',', $resp).']', 200, [], true);
    }

    /**
     * Delete item.
     *
     * @param Request $request Petition request.
     * @param string  $formId  Form ID to query, with extra "s" at end.
     * @param string  $id      Record ID.
     *
     * @return Response
     *
     * @throws Exception
     * @throws DoctrineException
     *
     * @Route(
     *     name="form_delete_item",
     *     path="/form/data/{formId}/{id}",
     *     methods={"DELETE"},
     * )
     */
    public function deleteItem(Request $request, string $formId, string $id): Response
    {
        // Prepare data.
        $formId = $this->cleanFormId($formId);

        // Get form and find the record.
        try {
            $form = $this->formFactory->find($formId);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }
        // If set in the form configuration, use another controller to process this request.
        if ($alternative = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_DELETE)) {
            if (is_array($alternative)) {
                $alternative = $alternative['controller'];
            }
            if (is_string($alternative)) {
                return call_user_func([$this, $alternative], $request, $formId, $id);
            }
        }
        try {
            $record = $form->find($id);
        } catch (\Exception $e) {
            $record = null;
        }

        if (!$record) {
            return new JsonResponse(json_encode(['message' => $this->t('Resource not found')]), 404, [], true);
        }

        // Log action.
        // $this->logAction('delete record', ['form_id' => $formId, 'record_id' => $id]);

        $this->connection->superBeginTransaction();
        try {
            $record->delete();
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();

            return new JsonResponse(json_encode(['message' => $e->getMessage()]), 400, [], true);
        }

        return new Response('', 204);
    }

    /**
     * Clean the form ID that come from path.
     *
     * @param string $formId Api form ID from path.
     *
     * @return string
     */
    protected function cleanFormId(string $formId): string
    {
        return substr($formId, 0, -1);
    }

    /**
     * Add action to activity log.
     *
     * @param string $msg
     * @param array  $context
     */
    protected function logAction(string $msg, array $context = []): void
    {
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        $this->inquiryFormLogger->info('Try to '.$msg.' by API.', $context);
    }

    /**
     * Got form manager by form ID.
     *
     * @param string $formId
     *
     * @return FormManagerInterface
     *
     * @throws \Exception
     */
    protected function getFormByIdCached(string $formId): FormManagerInterface
    {
        if (!isset($this->formByIdCache[$formId]) || empty($this->formByIdCache[$formId])) {
            $this->formByIdCache[$formId] = $this->formFactory->find($formId);
        }

        return $this->formByIdCache[$formId];
    }

    /**
     * Add filter to a sub-query.
     *
     * This method format ID filters.
     *
     * @param array                $filterBy
     * @param FormManagerInterface $form
     * @param string               $operator
     *
     * @return string
     */
    protected function addFilters(array $filterBy, FormManagerInterface $form, string $operator = 'and'): string
    {
        $conditions = [];
        foreach ($filterBy as $field => $filter) {
            switch ($field) {
                case 'and':
                case '&&':
                    $conditions[] = sprintf(' (%s)', $this->addFilters($filter, $form));
                    break;

                case 'or':
                case '||':
                    $conditions[] = sprintf(' (%s)', $this->addFilters($filter, $form, 'or'));
                    break;

                case 'id':
                    $ulid = new Ulid($filter);
                    $conditions[] = ' HEX(id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'';
                    break;
                case 'field_reference':
                    $conditions[] = ' field_reference like \'%'.$filter.'%\'';
                    break;
                case 'field_ulid_reference':
                    $ulid = new Ulid($filter);
                    $conditions[] = ' HEX(field_ulid_reference) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'';
                    break;
                case 'field_validated':
                    $conditions[] = ' field_validated = \''.($this->str2Bool($filter) ? '1' : '0').'\'';
                    break;
                case 'field_deleted':
                    $conditions[] = ' field_deleted = \''.($this->str2Bool($filter) ? '1' : '0').'\'';
                    break;
                case 'field_status':
                    $conditions[] = ' field_status in ('.$filter.')';
                    break;
                // Security filters.
                // Because these criteria is a security criteria, it only has a max of two filters.
                // We can implement here simple criteria to SQL parser.
                case 'field_country':
                    $conditions[] = ' field_country_value = \''.$filter.'\'';
                    break;
                case 'branch':
                    // IMPORTANT!!! This code fragment is waiting a specific array structure:
                    // $filter['alike_start']['field_region'] = ['branch value', 'branch value']
//                    $aux = current($filter);
//                    $auxKey = key($filter);
                    // HARCODED array element = alike_start
//                    $divsRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
                    $key = key($filter);
                    $value = current($filter);
                    foreach (current($filter) as $filterItem) {
                        // HARCODED array element = field_region
                        foreach ($filterItem as $fItem) {
                            $aDql[] = ' branch like \''.$fItem.'%\'';
//                            $div = $divsRepo->find($fItem);
//                            if ($div) {
//                                $aDql[] = ' branch like \''.$div->getBranch().'%\'';
//                            }
                        }
                    }
                    if (count($value) === 0 && $key) {
                        switch ($key) {
                            case 'not_exists':
                                $aDql[] = ' branch is null';
                                break;
                        }
                    }
//                    $conditions[] = '('.implode(' || ', $aDql).' || ISNULL(branch))';
                    $conditions[] = '('.implode(' || ', $aDql).')';
                    break;
                case 'field_region':
                    // Fix field_region criteria if the query have access filters.
                    if (isset($filter['IN'])) {
                        $in = [];
                        foreach ($filter['IN'] as $id) {
                            $in[] = $this->connection->quote($id);
                        }
                        $conditions[] = ' field_region_value in ('.implode(',', $in).')';
                        unset($in);
                        if (isset($filterBy["and"]["field_region"])) {
                            $conditions[] = ' field_region_value = '.$this->connection->quote($filterBy["and"]["field_region"]);
                        }
                    } else {
                        // Fix field_region criteria if the query doesn't have access filters.
                        if (is_array($filter) && isset($filter['not_exists'])) {
                            $conditions[] = ' field_region_value IS NULL';
                        } else {
                            $conditions[] = ' field_region_value = '.$this->connection->quote($filter);
                        }
                    }
                    break;
                case 'field_creator':
                    $ulid = Ulid::fromString($filter);
                    $conditions[] = ' HEX(field_creator_value) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'';
                    break;
                case 'field_created':
                case 'field_changed':
                    $user = $this->sessionService->getUser();
                    $timezone = new \DateTimeZone($user->getTimezone());
                    $date = new \DateTime($filter, $timezone);
                    $date->setTimezone(new \DateTimeZone('UTC'));
                    $fieldName = $field.'_value';
                    // $conditions[] = ' field_changed_value >= \''. $date->format('Y-m-d H:i:s') .'\'';
                    $conditions[] = ' '.$fieldName.' >= \''.$date->format('Y-m-d H:i:s').'\'';
                    break;
                default:
                    $fieldDef = $form->getFieldDefinition($field);
                    if ($fieldDef) {
                        switch ($fieldDef->getFieldType()) {
                            case 'user_reference':
                                $ulid = Ulid::fromString($filter);
                                $conditions[] = ' HEX('.$field.'_value) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'';
                                break;
                            // todo Process more field types or generalize this process.
                        }
                    } else {
                        // This is a conditional group.
                        $conditions[] = $this->addFilters($filter, $form, $operator);
                    }
                    break;
            }
        }

        return implode(' '.$operator.' ', $conditions);
    }

    /**
     * Convert string to boolean.
     *
     * @param string $str
     *
     * @return bool
     */
    protected function str2Bool(string $str): bool
    {
        return ('TRUE' === strtoupper($str) || '1' === $str);
    }

    /**
     * Process request parameters.
     *
     * @param Request $request
     *
     * @return array ['filter_by' => array, 'order_by' => array, 'limit' => int, 'offset' => int]
     */
    protected function getRequestParameters(Request $request): array
    {
        $itemsPerPage = $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
        $parameters = $request->query->all();
        $paramPage = 0;
        $orderBy = [];
        $filterBy = [];
        $groupBy = [];
        // Translate query parameters to findBy criteria.
        $isUnique = false;
        foreach ($parameters as $paramName => $paramValue) {
            switch ($paramName) {
                case 'unique':
                    $isUnique = $this->str2Bool($paramValue);
                    break;
                case 'page':
                    $paramPage = $paramValue - 1;
                    break;
                case 'itemsPerPage':
                    if ($paramValue <= $itemsPerPage) {
                        $itemsPerPage = $paramValue;
                    }
                    break;
                case 'order':
                    $orderBy = $paramValue;
                    break;
                default:
                    if (substr($paramName, 0, 6) === 'order[') {
                        $orderBy[substr($paramName, 6, strlen($paramName) - 7)] = $paramValue;
                    } else {
                        $output = [];
                        // '<=' and '>=' operators have problems with parse_str.
                        $paramName = str_replace('<=', 'lte', $paramName);
                        $paramName = str_replace('>=', 'gte', $paramName);
                        // The plus sign, '+', is removed by parse_str. We must urlencode value to prevent it.
                        if (is_array($paramValue)) {
                            $useOperator = "=";
                            $useValue = null;
                            foreach ($paramValue as $oper => $operValue) {
                                if (is_string($oper)) {
                                    $useOperator = $oper;
                                    $useValue = $operValue;
                                }
                                break;
                            }
                            if ('=' !== $useOperator) {
                                $output = [
                                    $paramName => [$useOperator => $useValue],
                                ];
                            } else {
                                parse_str("$paramName=".urlencode(implode(',', $paramValue)), $output);
                            }
                        } else {
                            if ('false' === strtolower($paramValue)) {
                                $paramValue = '0';
                            } elseif ('true' === strtolower($paramValue)) {
                                $paramValue = '1';
                            }
                            parse_str("$paramName=".urlencode($paramValue), $output);
                        }
                        $outputKey = key($output);
                        if (is_array($output[$outputKey])) {
                            foreach ($output[$outputKey] as $key => $value) {
                                $filterBy[$outputKey][$key] = $value;
                            }
                        } else {
                            $filterBy[$outputKey] = $output[$outputKey];
                        }
                    }
                    break;
            }
        }

        if ($isUnique) {
            $orderBy['field_changed'] = 'desc';
            $groupBy[] = 'field_ulid_reference';
        }

        return [
            'filter_by' => $filterBy,
            'order_by' => $orderBy,
            'limit' => $itemsPerPage,
            'offset' => $paramPage * $itemsPerPage,
            'group_by' => $groupBy,
        ];
    }

    /**
     * Add read mark to mail.
     *
     * @param array      $respRecord Record array to update.
     * @param FormRecord $record     Reference mail record.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function markReadMail(array &$respRecord, FormRecord $record)
    {
        // Read mark is unique for each user and mail and replace the field_is_read value.
        $user = $this->sessionService->getUser();
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->find($user->getId());
        $readMarkRepo = $this->entityManager->getRepository(UserReadMail::class);
        $marks = $readMarkRepo->findBy([
            'user' => $user,
            'mail' => $record->getId(),
        ]);

        $respRecord = $record->toArray();
        if (count($marks) > 0) {
            $respRecord["field_is_read"] = '1';
        } else {
            $mark = new UserReadMail();
            $mark->setMail($record->getId());
            $mark->setUser($user);
            $mark->setIsChild($record->{'field_is_child'});
            $readMarkRepo->saveNow($mark);
        }
    }
}
