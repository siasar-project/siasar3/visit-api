<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Repository\ConfigurationRepository;
use App\Security\Handlers\InquiryAccessHandler;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Tools\Tools;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\Value;
use Doctrine\ORM\Query\Expr;
use Jelix\Version\Version;
use Jelix\Version\VersionComparator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Forms structure controllers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormStructureAction
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected FormFactory $formFactory;
    protected ParameterBagInterface $systemSettings;
    protected ConfigurationRepository $configurationRepository;
    protected AccessManager $accessManager;
    protected SessionService $sessionService;
    protected InquiryAccessHandler $inquiryAccessHandler;

    /**
     * Form collection action constructor.
     *
     * @param FormFactory             $formFactory             Form factory.
     * @param ConfigurationRepository $configurationRepository Configuration repository,
     * @param SessionService          $sessionService
     * @param AccessManager           $accessManager
     * @param InquiryAccessHandler    $inquiryAccessHandler
     */
    public function __construct(FormFactory $formFactory, ConfigurationRepository $configurationRepository, SessionService $sessionService, AccessManager $accessManager, InquiryAccessHandler $inquiryAccessHandler)
    {
        $this->formFactory = $formFactory;
        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
        $this->configurationRepository = $configurationRepository;
        $this->accessManager = $accessManager;
        $this->sessionService = $sessionService;
        $this->inquiryAccessHandler = $inquiryAccessHandler;
    }

    /**
     * Get forms list.
     *
     * @param Request $request Petition request.
     *
     * @return Response
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     *
     * @Route(
     *     name="form_structure_list",
     *     path="/form/structure",
     *     methods={"GET"},
     * )
     */
    public function getStructureList(Request $request): Response
    {
        $user = $this->sessionService->getUser();
        if (!$this->accessManager->hasPermission($user, 'read form structure')) {
            return new JsonResponse('[]', 401, [], true);
        }

        $itemsPerPage = $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
        // Prepare data.
        $page = 0;
        if ($qPage = $request->query->get('page')) {
            $page = $qPage - 1;
        }
        $qType = $request->query->get('type', '');
        // Query.
        $criteria = new Criteria();
        if (!empty($qType)) {
            $criteria->andWhere(Criteria::expr()->contains('value', $qType));
        }
        $forms = $this->formFactory->findBy($criteria, [], $itemsPerPage, $page * $itemsPerPage);
        $resp = [];
        /** @var FormManager $form */
        foreach ($forms as &$form) {
            $item = new \stdClass();
            $item->id = $form->getId();
            $item->path = '/form/structure/'.$form->getId();
            $item->type = $form->getType();
            $item->title = !empty($form->getTitle()) ? self::t($form->getTitle()) : '';
            $item->description = !empty($form->getDescription()) ? self::t($form->getDescription()) : '';

            $meta = $form->getMeta();
            $item->{'allow_sdg'} = false;
            if (isset($meta['allow_sdg'])) {
                $item->{'allow_sdg'} = boolval($meta['allow_sdg']);
            }

            $resp[] = json_encode($item);
        }

        return new JsonResponse('['.implode(', ', $resp).']', 200, [], true);
    }

    /**
     * Get forms structure.
     *
     * @param Request $request Petition request.
     * @param string  $fid     Form ID.
     *
     * @return Response
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     *
     * @Route(
     *     name="form_structure",
     *     path="/form/structure/{fid}",
     *     methods={"GET"},
     * )
     */
    public function getFormStructure(Request $request, string $fid): Response
    {
        $user = $this->sessionService->getUser();
        if (!$this->accessManager->hasPermission($user, 'read form structure')) {
            return new JsonResponse('', 401, [], true);
        }

        $cfg = $this->configurationRepository->find($fid);

        if (!$cfg) {
            return new JsonResponse($this->t('Resource not found'), 404);
        }

        $form = $this->formFactory->find($fid);

        if (!$form) {
            return new JsonResponse($this->t('Resource not found'), 404);
        }

        // Process form meta to build the complete field tree.
        $data = $cfg->getValue();
        if (isset($data["meta"]["field_groups"])) {
            $tree = &$data["meta"]["field_groups"];
            // Order field by weight.
            uksort(
                $data['fields'],
                function (mixed $a, mixed $b) use ($data) {
                    $fA = $data['fields'][$a];
                    $fB = $data['fields'][$b];
                    if (intval($fA["settings"]["weight"]) === intval($fB["settings"]["weight"])) {
                        return 0;
                    }

                    return ($fA["settings"]["weight"] > $fB["settings"]["weight"]) ? 1 : -1;
                }
            );
            // Max field level to show.
            $maxLevel = 3;
            // Show SDG fields or not.
            $showSdg = false;
            if ($user->getCountry()) {
                $levels = $user->getCountry()->getFormLevel();
                if (isset($levels[$fid]['level'])) {
                    $maxLevel = $levels[$fid]['level'];
                }
                if (isset($levels[$fid]['sdg'])) {
                    $showSdg = $levels[$fid]['sdg'];
                }
            }
            // Add fields to field_group tree.
            foreach ($data['fields'] as $fieldName => &$value) {
                // Update field settings dynamically.
                $fieldInstance = $form->getFieldDefinition($fieldName);
                $value = $fieldInstance->getSettings(true);
                // Ignore deprecated fields.
                if (isset($value["deprecated"]) && $value["deprecated"]) {
                    unset($data['fields'][$fieldName]);
                    continue;
                }
                // Update maximum_file_size if defined.
                if (isset($value['settings']['maximum_file_size'])) {
                    $systemLimit = Tools::phpIniToBytes(ini_get('upload_max_filesize'));
                    if ($systemLimit < $value['settings']['maximum_file_size']) {
                        $value['settings']['maximum_file_size'] = $systemLimit;
                    }
                }
                // Ignore fields with level outside country level.
//                if (isset($value["settings"]["meta"]["level"])) {
//                    if ($value["settings"]["meta"]["level"] > $maxLevel) {
//                        unset($data['fields'][$fieldName]);
//                        continue;
//                    }
//                }
                // Ignore SDG fields in country without SDG.
//                if (isset($value["settings"]["meta"]["sdg"]) && $value["settings"]["meta"]["sdg"]) {
//                    if (!$showSdg) {
//                        unset($data['fields'][$fieldName]);
//                        continue;
//                    }
//                }
                // Add field to meta field_groups structure.
                if (isset($value["settings"]["meta"]["catalog_id"])) {
                    $this->metaTreeInsert($tree, $fieldName, $value);
                }
            }
            // Sort tree by keys.
            $this->sortTree($tree);
        }

        // Translate structure labels.
        $this->translateChildren($data);

        // Navigate form definition to allow each field type extends meta definitions.
        $form = $this->formFactory->find($fid);
        /** @var FieldTypeInterface $field */
        foreach ($form->getFields() as $field) {
            $data["fields"][$field->getId()]["settings"]["meta"] = $field->getMetaExtended();
        }

        return new JsonResponse(json_encode($data), 200, [], true);
    }

    /**
     * Get an empty record.
     *
     * @param Request $request Petition request.
     * @param string  $fid     Form ID.
     *
     * @return Response
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     *
     * @Route(
     *     name="form_structure_empty_record",
     *     path="/form/structure/empty/{fid}",
     *     methods={"GET"},
     * )
     */
    public function getFormStructureEmptyRecord(Request $request, string $fid): Response
    {
        $user = $this->sessionService->getUser();
        if (!$this->accessManager->hasPermission($user, 'read form structure')) {
            return new JsonResponse('', 401, [], true);
        }

        $cfg = $this->configurationRepository->find($fid);

        if (!$cfg) {
            return new JsonResponse($this->t('Resource not found'), 404);
        }

        /** @var FormManagerInterface $form */
        $form = $this->formFactory->find($fid);

        if (!$form) {
            return new JsonResponse($this->t('Resource not found'), 404);
        }

        // Build empty form record.
        $data = $form->createRecord([]);
        $form->hydrateRecord($data);

        return new JsonResponse(json_encode($data->toArray()), 200, [], true);
    }

    /**
     * Insert a field in the meta tree.
     *
     * @param array  $tree          Tree.
     * @param string $fieldName     Field name.
     * @param array  $fieldSettings Field settings.
     */
    protected function metaTreeInsert(&$tree, string $fieldName, array $fieldSettings): void
    {
        $path = explode('.', $fieldSettings["settings"]["meta"]["catalog_id"]);
        $parent = &$this->getMetaTreeParent($tree, $path);
        $meta = [
            'id' => (string) $fieldSettings["settings"]["meta"]["catalog_id"],
            'title' => $fieldSettings["label"],
            'field_id' => $fieldName,
            'weight' => $fieldSettings["settings"]["weight"],
        ];
        if (!$parent) {
            // Root field.
            $tree[(string) $fieldSettings["settings"]["meta"]["catalog_id"]] = $meta;
        } else {
            // child field.
            $parent['children'][(string) $fieldSettings["settings"]["meta"]["catalog_id"]] = $meta;
        }
    }

    /**
     * Get path parent.
     *
     * @param array $tree The tree.
     * @param array $path Child path.
     *
     * @return array|null
     */
    protected function &getMetaTreeParent(array &$tree, array $path): ?array
    {
        $id = [];
        $max = count($path) - 1;
        for ($i = 0; $i < $max; ++$i) {
            $id[] = $path[$i];
        }
        $parentId = implode('.', $id);
        $parent = &$this->findInMetaTree($tree, $parentId);
        if (!$parent && count($path) > 1) {
            $grandParent = &$this->getMetaTreeParent($tree, explode('.', $parentId));
            $grandParent['children'][$parentId] = [
                'id' => $parentId,
                'title' => $parentId,
                'children' => [],
            ];
            $parent = &$this->findInMetaTree($tree, $parentId);
        }

        return $parent;
    }

    /**
     * Find a node in the tree.
     *
     * @param array  $tree The tree.
     * @param string $id   Node ID.
     *
     * @return array|null
     */
    protected function &findInMetaTree(array &$tree, string $id): ?array
    {
        foreach ($tree as $key => &$value) {
            if ((string) $key === $id) {
                if (!isset($value['children'])) {
                    $value['children'] = [];
                }

                return $value;
            } else {
                if (!isset($value['children'])) {
                    continue;
                }
                $item = &$this->findInMetaTree($value['children'], $id);
                if ($item) {
                    return $item;
                }
            }
        }
        $resp = null;

        return $resp;
    }

    /**
     * Sort a tree.
     *
     * @param array $tree Branch to sort.
     */
    protected function sortTree(array &$tree): void
    {
        // ksort($tree);
        uksort(
            $tree,
            function (mixed $a, mixed $b) use ($tree) {
                if (!isset($tree[$a]["weight"]) || !isset($tree[$b]["weight"]) || 0 === $tree[$a]["weight"] || 0 === $tree[$b]["weight"]) {
                    // Compare like version numbers.
                    $vA = new Version(explode('.', $a));
                    $vB = new Version(explode('.', $b));

                    return VersionComparator::compare($vA, $vB);
                }
                // Order by weight.
                $fA = $tree[$a];
                $fB = $tree[$b];

                if (isset($fA["settings"]["weight"])) {
                    $fA["weight"] = $fA["settings"]["weight"];
                }
                if (isset($fB["settings"]["weight"])) {
                    $fB["weight"] = $fB["settings"]["weight"];
                }
                if ($fA["weight"] === $fB["weight"]) {
                    return 0;
                }

                return ($fA["weight"] > $fB["weight"]) ? 1 : -1;
            }
        );
        foreach ($tree as &$item) {
            if (isset($item['children'])) {
                $this->sortTree($item['children']);
            }
        }
    }

    /**
     * Translate interface strings.
     *
     * @param array $tree The tree.
     */
    protected function translateChildren(array &$tree): void
    {
        foreach ($tree as $key => &$value) {
            if (empty($value)) {
                continue;
            }
            switch ($key) {
                case 'label':
                case 'description':
                case 'title':
                case 'help':
                case 'true_label':
                case 'false_label':
                    $value = $this->t($value);
                    break;
                case 'options':
                    $nValue = [];
                    foreach ($value as $opc => $label) {
                        // Ensure that option keys are string before sending it to the client.
                        $nValue[$opc."\n"] = $this->t($label);
                    }
                    $value = $nValue;
                    break;
                default:
                    if (is_array($value)) {
                        $this->translateChildren($value);
                    }
                    break;
            }
        }
    }
}
