<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use App\Entity\File;
use App\Entity\HouseholdProcess;
use App\Entity\User;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\RepositorySecured\HouseholdProcessRepositorySecured;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Tools\CommunityHouseholdCalcs;
use App\Tools\ExceptionNormalizer;
use App\Tools\Json;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Ulid;

/**
 * Household Process Action.
 */
class HouseholdProcessAction extends AbstractController
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected HouseholdProcessRepositorySecured $householdProcessRepo;
    protected EntityManagerInterface $em;
    protected FormFactory $formFactory;
    protected AccessManager $accessManager;
    protected SessionService $sessionService;
    protected ParameterBagInterface $systemSettings;

    /**
     * CreateFileObjectAction constructor.
     *
     * @param EntityManagerInterface            $em
     * @param HouseholdProcessRepositorySecured $householdProcessRepo
     * @param FormFactory                       $formFactory
     * @param SessionService                    $sessionService
     * @param AccessManager                     $accessManager
     */
    public function __construct(EntityManagerInterface $em, HouseholdProcessRepositorySecured $householdProcessRepo, FormFactory $formFactory, SessionService $sessionService, AccessManager $accessManager)
    {
        $this->em = $em;
        $this->householdProcessRepo = $householdProcessRepo;
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->accessManager = $accessManager;
        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
    }

    /**
     * Controller callable.
     *
     * @Route(
     *     name="close_process",
     *     path="/household_processes/{id}/close",
     *     methods={"PUT"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function closeHouseholdProcess(Request $request): JsonResponse
    {
        try {
            if (!$request->get('id')) {
                // Error with status 400.
                throw new BadRequestHttpException($this->t('"ID" is required'));
            }

            $householdProcess = $this->householdProcessRepo->findOneBy(['id' => $request->get('id')]);

            if (!$householdProcess) {
                return new JsonResponse([$this->t('HouseholdProcess not found.')], 404);
            }

            // Find community inquiry.
            $communityForms = $this->formFactory->find('form.community');
            $inquiry = $communityForms->find($householdProcess->getCommunityReference()->toBinary());
            $fieldDef = $communityForms->getFieldDefinition('field_total_households');
            if (!$inquiry) {
                throw new \Exception(self::t('Community inquiry not found "@id".', ['@id' => $householdProcess->getCommunityReference()->toBase32()]));
            }
            // Check field 1.5 of form.community.
            if (0 === $inquiry->{'field_total_households'}) {
                throw new \Exception(self::t('[@form] Field "@def" is required to close the process.', ['@form' => $communityForms->getId(), '@def' => $fieldDef->getLabelExtended()]));
            }

            // Not allow close if we have more houses than the indicated by 1.5 of form.community.
            if ($householdProcess->getTotalHousehold() > $inquiry->{'field_total_households'}) {
                throw new \Exception(self::t('[@form] The number of household inquiries must be lower or equal to field "@def".', ['@form' => $communityForms->getId(), '@def' => $fieldDef->getLabelExtended()]));
            }

            $this->em->getConnection()->superBeginTransaction();
            try {
                $this->householdProcessRepo->closeProcess($householdProcess);

                // Fire household calcs.
                $executor = new CommunityHouseholdCalcs();
                $executor->executeInits($inquiry);
                $executor->executeCalcs($inquiry);

                $this->em->getConnection()->superCommit();
            } catch (\Exception $e) {
                $this->em->getConnection()->superRollBack();
                throw $e;
            }

            return new JsonResponse([$this->t('The Household Process has been closed.')], 200);
        } catch (\Exception $e) {
            $message = ExceptionNormalizer::normalize($e, true);
        }

        return new JsonResponse($message, 422);
    }

    /**
     * Add household survey to a process.
     *
     * @Route(
     *     name="add_household",
     *     path="/household_processes/{id}/add_household",
     *     methods={"POST"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addHouseholdToProcess(Request $request): JsonResponse
    {
        $householdProcessId = $request->get('id');
        $householdProcess = $this->householdProcessRepo->find($householdProcessId);
        $requestBody = $request->getContent();

        if (!$householdProcess) {
            return new JsonResponse($this->t('Resource not found'), 404);
        }

        if (!$householdProcess->isOpen()) {
            return new JsonResponse([$this->t('The process is closed.')], 422);
        }

        try {
            $newRecord = $requestBody ? (new Json())->decode($requestBody) : [];
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 400, [], true);
        }

        // Get form and insert the new record.
        try {
            $form = $this->formFactory->find('form.community.household');
        } catch (\Exception $e) {
            return new JsonResponse(ExceptionNormalizer::normalize($e, true), 404);
        }

        // Check field 1.5 of form.community.
        $communityForms = $this->formFactory->find('form.community');
        $inquiry = $communityForms->find($householdProcess->getCommunityReference()->toBinary());
        if ($inquiry) {
            $fieldDef = $inquiry->getFieldDefinition('field_total_households');
            if (0 !== $inquiry->{'field_total_households'} && ($householdProcess->getTotalHousehold() + 1) > $inquiry->{'field_total_households'}) {
                $e = new \Exception(self::t('[@form] The number of household inquiries must be lower or equal to field "@def".', ['@form' => $inquiry->getId(), '@def' => $fieldDef->getLabelExtended()]));

                return new JsonResponse(ExceptionNormalizer::normalize($e, true), 409);
            }
        }

        $this->em->getConnection()->superBeginTransaction();
        try {
            $newRecord['field_household_process'] = $householdProcess;
            $newRecord['field_interviewer'] = $this->sessionService->getUser();
            $newRecord['field_community'] = $householdProcess->getAdministrativeDivision();
            $newRecord['field_date'] = new DateTime();
            $newId = $form->insert($newRecord);
            $this->em->getConnection()->superCommit();
        } catch (\Exception $e) {
            $this->em->getConnection()->superRollBack();

            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 422, [], true);
        }

        // Load & send response.
        $record = $form->find($newId);

        return new JsonResponse($record->toJson(), 201, [], true);
    }

    /**
     * List of household surveys by user.
     *
     * @Route(
     *     name="my_households",
     *     path="/household_processes/my_households",
     *     methods={"GET"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getUserHouseholds(Request $request): JsonResponse
    {
        /** @var User $user */
//        $user = $this->sessionService->getUser();
//        $ulid = new Ulid($user->getId());
        $form = $this->formFactory->find('form.community.household');

        if (!$form) {
            return new JsonResponse($this->t('Resource not found'), 404);
        }

        $itemsPerPage = $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
        $parameters = $request->query->all();
        $paramPage = 0;
        $orderBy = ['field_finished' => 'asc'];
//        $filterBy = ['field_interviewer' => $ulid->toBinary()];
        $filterBy = [];
        $groupBy = [];
        foreach ($parameters as $paramName => $paramValue) {
            switch ($paramName) {
                case 'page':
                    $paramPage = $paramValue - 1;
                    break;
                case 'itemsPerPage':
                    if ($paramValue <= $itemsPerPage) {
                        $itemsPerPage = $paramValue;
                    }
                    break;
                case 'field_country':
                    // TODO Don't work, and it's not visible in documentation.
                    $filterBy['field_country'] = $paramValue;
                    break;
                case 'field_interviewer':
                    $ulid = Ulid::fromString($paramValue);
                    $filterBy['field_interviewer'] = $ulid->toBinary();
                    break;
                case 'field_household_process':
                    $ulid = Ulid::fromString($paramValue);
                    $filterBy['field_household_process'] = $ulid->toBinary();
                    break;
            }
        }

        $query = $form->queryBy($filterBy, $orderBy, $itemsPerPage, $paramPage * $itemsPerPage, $groupBy);
        // Don't show closed household process.
        // $query->join(
        //  't',
        //  'household_process',
        //  'hp',
        //  't.field_household_process_value = hp.id'
        // );
        //$query->andWhere("hp.open = '1'");
        $resp = $form->hydrateQuery($query);

        /** @var FormRecord $record */
        foreach ($resp as &$record) {
            $obj = $record->toArray();
            // Add community image if any.
            /** @var HouseholdProcess $process */
            $process = $record->{'field_household_process'};
            $obj['process'] = $process->formatExtraJson();
            $form = $this->formFactory->find('form.community');
            $communityRecord = $form->find($process->getCommunityReference());
            $obj['image'] = '';
            if ($communityRecord) {
                /** @var File[] $images */
                $images = $communityRecord->{'field_community_photos'};
                if (count($images) > 0) {
                    $extras = $images[0]->formatExtraJson();
                    $obj['image'] = $extras['url'];
                }
            }

            // Transform to Json.
            $record = Json::encode($obj);
        }

        return new JsonResponse('['.implode(', ', $resp).']', 200, [], true);
    }
}
