<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Forms\FormFactory;
use App\Repository\LocaleSourceRepository;
use App\Security\Handlers\DoctrineAccessHandler;
use App\Service\AccessManager;
use App\Service\ConfigurationService;
use App\Service\Mailer\MailerService;
use App\Service\Password\EncoderService;
use App\Service\SessionService;
use App\Tools\ExceptionNormalizer;
use App\Tools\Json;
use App\Tools\TranslatableMarkup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Translation controllers.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class I18nAction extends ConfigurationAction
{
    protected SessionService $sessionService;
    protected IriConverterInterface $iriConverter;
    protected DoctrineAccessHandler $accessHandler;

    /**
     * i18nAction constructor
     *
     * @param SessionService         $sessionService       Session service.
     * @param IriConverterInterface  $iriConverter         IriConverter.
     * @param ConfigurationService   $configurationService Configuration service.
     * @param MailerService          $mailer               Mailer service.
     * @param EncoderService         $encoderService       Encoder service.
     * @param AccessManager          $accessManager        Access manager.
     * @param EntityManagerInterface $em                   Entity manager.
     * @param FormFactory            $formFactory          Form factory.
     */
    public function __construct(SessionService $sessionService, IriConverterInterface $iriConverter, ConfigurationService $configurationService, MailerService $mailer, EncoderService $encoderService, AccessManager $accessManager, EntityManagerInterface $em, FormFactory $formFactory)
    {
        parent::__construct($configurationService, $mailer, $encoderService, $accessManager, $em, $formFactory);

        $this->sessionService = $sessionService;
        $this->iriConverter = $iriConverter;
        $this->accessHandler = new DoctrineAccessHandler(User::class, $accessManager, $sessionService, $iriConverter);
    }

    /**
     * Get i18n literals.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="i18n_literals",
     *     path="/configuration/i18n/{iso_code}",
     *     methods={"GET"},
     * )
     */
    public function i18nLiterals(Request $request): JsonResponse
    {
        $context = $request->get('context');
        $lang = $request->get('iso_code');
        $lang = str_replace('-', '_', $lang);
        $resp = [
            'id' => $lang,
            'literals' => new \stdClass(),
        ];

        $languageRepo = $this->em->getRepository(Language::class);
        if ($language = $languageRepo->findBy(['isoCode' => $lang])) {
            /** @var Language $language */
            $language = current($language);
            $sourceRepo = $this->em->getRepository(LocaleSource::class);
            $targetRepo = $this->em->getRepository(LocaleTarget::class);
            $filter = [];
            if (!empty($context)) {
                $filter = ['context' => $context];
            }
            $literals = $sourceRepo->findBy($filter);
            /** @var LocaleSource $literal */
            foreach ($literals as $literal) {
                $options = [
                    'langcode' => $language->getId(),
                ];
                if (!empty($context)) {
                    $options['context'] = $context;
                }
                $str = new TranslatableMarkup($literal->getMessage(), [], $options);
                $trans = $targetRepo->translateString($str);
                if ($literal->getMessage() !== $trans) {
                    $resp['literals']->{$literal->getMessage()} = $targetRepo->translateString($str);
                }
            }
        }

        return new JsonResponse($resp, 200);
    }

    /**
     * Return i18n translations.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="i18n_translator",
     *     path="/configuration/i18n/{iso_code}/translator",
     *     methods={"GET"},
     * )
     */
    public function i18nTranslator(Request $request): JsonResponse
    {
        $lang = $request->get('iso_code');
        $context = $request->get('context');
        $searchIn = $request->get('search_in');
        $stringContains = $request->get('contains');
        $lang = str_replace('-', '_', $lang);
        $itemsPerPage = null;
        $offset = null;
        $qPage = $request->query->get('page');

        if ($qPage) {
            if ($qPage <= 0) {
                return new JsonResponse($this->t('Invalid page.'), 400);
            }
            $page = $qPage - 1;
            $itemsPerPage = $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
            if ($request->query->get('itemsPerPage')) {
                $itemsPerPage = $request->query->get('itemsPerPage');
            }
            $offset = $page * $itemsPerPage;
        }
        if (!$searchIn) {
            $searchIn = 'all';
        }
        $resp = [
            'id' => $lang,
            'literals' => new \stdClass(),
        ];
        $languageRepo = $this->em->getRepository(Language::class);
        /** @var Language $language */
        if (!$language = $languageRepo->findOneBy(['isoCode' => $lang])) {
            return new JsonResponse(
                $this->t(
                    'ISO Code "@isoCode" not found.',
                    ['@isoCode' => $lang]
                ),
                404
            );
        }

        try {
            $options = [
                'langcode' => $language->getId(),
            ];
            /** @var LocaleSourceRepository $sourceRepo */
            $sourceRepo = $this->em->getRepository(LocaleSource::class);
            $targetRepo = $this->em->getRepository(LocaleTarget::class);
            $filter = [];
            if (!empty($context)) {
                $filter['context'] = $context;
            }
            if (!empty($stringContains)) {
                $filter['stringContains'] = $stringContains;
            }
            $literals = $sourceRepo->getLiterals($language->getId(), $searchIn, $filter, $itemsPerPage, $offset);
            /** @var LocaleSource $literal */
            foreach ($literals as $literal) {
                if (!empty($context)) {
                    $options['context'] = $context;
                }
                $str = new TranslatableMarkup($literal->getMessage(), [], $options);
                if ($str->hasTranslation()) {
                    $resp['literals']->{$literal->getMessage()} = $str->getTranslatedMarkup();
                } else {
                    $resp['literals']->{$literal->getMessage()} = '';
                }
            }
            $resp['total'] = count($literals);
        } catch (\Exception $e) {
            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }

        return new JsonResponse($resp, 200);
    }

    /**
     * Get i18n literals.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="i18n_add",
     *     path="/configuration/i18n/add",
     *     methods={"POST"},
     * )
     */
    public function i18nAddLiteral(Request $request): JsonResponse
    {
        $user = $this->accessManager->getSecurity()->getUser();
        $allow = $this->accessManager->hasPermission($user, 'create i18n literal');
        if (!$allow) {
            return new JsonResponse(
                $this->t(
                    'The permission "@permission" is required.',
                    ['@permission' => 'create i18n literal']
                ),
                403
            );
        }

        $body = Json::decode($request->getContent());
        $context = $request->get('context');

        // Validate body.
        $withLiteral = false;
        foreach ($body as $key => $value) {
            if ('literal' !== $key) {
                throw new \Exception(self::t('Key no acceptable: @key.', ['@key' => $key]));
            }
            $withLiteral = true;
            if (!is_string($value)) {
                throw new \Exception(self::t('Literal must be a string, but is @type.', ['@type' => gettype($value)]));
            }
        }
        if (!$withLiteral || empty($body['literal'])) {
            throw new \Exception(self::t('Literal property is required.'));
        }

        $sourceRepo = $this->em->getRepository(LocaleSource::class);
        // Don't duplicate.
        $filter = ['message' => $body['literal']];
        if (!empty($context)) {
            $filter['context'] = $context;
        }
        $source = $sourceRepo->findBy($filter);
        if (count($source) === 0) {
            // Add literal.
            $sourceRepo->create($body['literal'], $context);
        }

        return new JsonResponse("ok", 200);
    }

    /**
     * Get the list of languages availables to translate by user.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="i18n_languages",
     *     path="/configuration/i18n_languages",
     *     methods={"GET"},
     * )
     */
    public function i18nLanguages(Request $request): JsonResponse
    {
        $user = $this->sessionService->getUser();
        $this->accessHandler->checkAccess($user, DoctrineAccessHandler::ACCESS_ACTION_READ, 'doctrine localetarget');

        $resp = [];
        $country = $user->getCountry();
        $langs = $country->getLanguages();

        foreach ($langs as $lang) {
            /** @var Language $lang */
            if ($lang->isTranslatable()) {
                $resp[$lang->getId()] = [
                    'iso_code' => $lang->getIsoCode(),
                    'name' => $lang->getName(),
                    'ltr' => $lang->isLtr(),
                    'plural_formula' => $lang->getPluralFormula(),
                    'plural_number' => $lang->getPluralNumber(),
                    'translatable' => $lang->isTranslatable(),
                ];
            }
        }

        return new JsonResponse($resp, 200);
    }

    /**
     * Add translation for a specific language.
     *
     * If no translation exists, it is added for the language of the iso_code.
     * If it exists, it is updated.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="i18n_translate",
     *     path="/configuration/i18n/{iso_code}",
     *     methods={"POST"},
     * )
     */
    public function i18nAddTranslation(Request $request): JsonResponse
    {
        $isoCode = $request->get('iso_code');
        $requestBody = $request->getContent();

        // Check access
        $user = $this->sessionService->getUser();
        $this->accessHandler->checkAccess($user, DoctrineAccessHandler::ACCESS_ACTION_UPDATE, 'doctrine localetarget');

        // Check language
        $languageRepo = $this->em->getRepository(Language::class);
        /** @var Language */
        if (!$language = $languageRepo->findOneBy(['isoCode' => $isoCode])) {
            return new JsonResponse(
                $this->t(
                    'ISO Code "@isoCode" not found.',
                    ['@isoCode' => $isoCode]
                ),
                404
            );
        }
        // Super admin can translate any language.
        $superAdminAllowed = $this->accessHandler->hasPermission($user, 'all permissions');
        if (!$superAdminAllowed) {
            // Other users only can translate some languages in their country.
            if (!$language->isTranslatable()) {
                return new JsonResponse(
                    $this->t('This language is not translatable.'),
                    403
                );
            }
            // Check if the country has the specific language
            if (!in_array($language, $user->getCountry()->getLanguages()->toArray())) {
                return new JsonResponse(
                    $this->t('You cannot update this language.'),
                    403
                );
            }
        }

        try {
            $content = Json::decode($requestBody);
            $literal = $content['source'];
            $translation = $content['target'];
            if (!isset($literal) || empty($literal) || !isset($translation)) {
                return new JsonResponse(
                    $this->t('Invalid request body.'),
                    400
                );
            }
            $sourceRepo = $this->em->getRepository(LocaleSource::class);
            $targetRepo = $this->em->getRepository(LocaleTarget::class);
            /** @var LocaleSource $source */
            $source = $sourceRepo->findOneBy(['message' => $literal]);
            if (!$source) {
                return new JsonResponse(
                    $this->t('Locale source not found.'),
                    404
                );
            }
            $str = new TranslatableMarkup($source->getMessage(), [], ['langcode' => $language->getId()]);
            if ($str->hasTranslation()) {
                // Check if the translation is in the current (translatable) language.
                // It could have a translation but inherited from another language.
                $target = $targetRepo->findOneBy(['localeSource' => $source->getId(), 'language' => $language->getId()]);
                if ($target) {
                    if (empty($translation)) {
                        $targetRepo->removeNow($target);
                    } else {
                        $target->setTranslation($translation);
                        $targetRepo->saveNow($target);
                    }
                } else {
                    $target = $targetRepo->create($language, $source, $translation);
                }
            } else {
                /** @var LocaleTarget $target */
                $target = $targetRepo->create($language, $source, $translation);
            }
        } catch (\Exception $e) {
            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, true)), 404, [], true);
        }

        return new JsonResponse(['source' => $source->getMessage(), 'translation' => $target->getTranslation()], 201);
    }

    /**
     * Calculates the percentage of translated and untranslated literals.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="i18n_progress",
     *     path="/configuration/i18n/{iso_code}/progress",
     *     methods={"GET"},
     * )
     */
    public function i18nProgress(Request $request): JsonResponse
    {
        $user = $this->sessionService->getUser();
        $isoCode = $request->get('iso_code');
        $isoCode = str_replace('-', '_', $isoCode);

        $this->accessHandler->checkAccess($user, DoctrineAccessHandler::ACCESS_ACTION_READ, 'doctrine localetarget');

        $languageRepo = $this->em->getRepository(Language::class);
        /** @var Language $language */
        if (!$language = $languageRepo->findOneBy(['isoCode' => $isoCode])) {
            return new JsonResponse(
                $this->t(
                    'ISO Code "@isoCode" not found.',
                    ['@isoCode' => $isoCode]
                ),
                404
            );
        }
        // Check if the country has the specific language
        if (!in_array($language, $user->getCountry()->getLanguages()->toArray())) {
            return new JsonResponse(
                $this->t('You cannot read this language.'),
                403
            );
        }
        /** @var LocaleSourceRepository $sourceRepo */
        $sourceRepo = $this->em->getRepository(LocaleSource::class);
        $untranslated = $sourceRepo->getLiterals($language->getId(), 'untranslated');
        $translated = $sourceRepo->getLiterals($language->getId(), 'translated');
        $totalUntranslated = count($untranslated);
        $totalTranslated = count($translated);

        return new JsonResponse([
            'total' => $totalUntranslated + $totalTranslated,
            'translated' => $totalTranslated,
            'untranslated' => $totalUntranslated,
            'progress' => round(($totalTranslated * 100) / ($totalUntranslated + $totalTranslated), 0),
        ], 200);
    }
}
