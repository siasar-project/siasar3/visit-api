<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model\Encoding;
use ApiPlatform\Core\OpenApi\Model\MediaType;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\Model\Response as OpenApiResponse;
use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Entity\InquiryFormLog;
use App\Entity\PointLog;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\PointFormManager;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;
use App\Plugins\PointCheckActionDiscovery;
use App\RepositorySecured\HouseholdProcessRepositorySecured;
use App\RepositorySecured\InquiryFormLogRepositorySecured;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Tools\ExceptionNormalizer;
use App\Tools\Json;
use App\Tools\SiasarPointCompleteChecks;
use App\Tools\SiasarPointWrapper;
use App\Traits\OpenApiSchemaTrait;
use DateTimeZone;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Ulid;

/**
 * SIASAR Point controllers.
 */
class PointAction extends FormCollectionAction
{
    use OpenApiSchemaTrait;

    protected PointCheckActionDiscovery $pointCheckActionDiscovery;
    protected AccessManager $accessManager;

    /**
     * @param PointCheckActionDiscovery $pointCheckActionDiscovery
     * @param FormFactory               $formFactory
     * @param LoggerInterface           $inquiryFormLogger
     * @param SessionService            $sessionService
     * @param Connection                $connection
     * @param IriConverterInterface     $iriConverter
     * @param EntityManagerInterface    $entityManager
     * @param AccessManager             $accessManager
     */
    public function __construct(PointCheckActionDiscovery $pointCheckActionDiscovery, FormFactory $formFactory, LoggerInterface $inquiryFormLogger, SessionService $sessionService, Connection $connection, IriConverterInterface $iriConverter, EntityManagerInterface $entityManager, AccessManager $accessManager)
    {
        parent::__construct($formFactory, $inquiryFormLogger, $sessionService, $connection, $iriConverter, $entityManager);

        $this->pointCheckActionDiscovery = $pointCheckActionDiscovery;
        $this->accessManager = $accessManager;
    }

    /**
     * Get PointLog collection.
     *
     * This method is necessary since Doctrine Entities don't know how to relate PointLog entities with Forms.
     * Open API parameters customized, @see /src/OpenApi/OpenApiFactory.php
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getPointLogCollection(Request $request): Response
    {
        // Possible query parameters:
        // page, createdAt, point, level, levelName, message, country, order
        // Orders: createdAt, point, level, levelName, message
        $parameters = $this->getRequestParameters($request);
        $offset = $parameters['offset'];
        $itemsPerPage = $parameters['limit'];
        $orderBy = $parameters['order_by'];
        $filterBy = $parameters['filter_by'];

        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from('point_log', 'pl');
        foreach ($filterBy as $field => $value) {
            switch ($field) {
                case 'country':
                    $query->andWhere($query->expr()->eq('pl.country_code', "'{$filterBy['country']}'"));
                    break;

                case 'point':
                    $ulid = new Ulid($filterBy['point']);
                    $query->andWhere($query->expr()->eq('HEX(pl.point)', "'".strtoupper(bin2hex($ulid->toBinary()))."'"));
                    break;

                case 'message':
                    $query->andWhere($query->expr()->like('pl.message', "'%{$filterBy['message']}%'"));
                    break;

                case 'levelName':
                    $query->andWhere($query->expr()->eq('pl.level_name', "'{$filterBy['levelName']}'"));
                    break;

                default:
                    $query->andWhere($query->expr()->eq('pl.'.$field, "'{$value}'"));
                    break;
            }
        }

        foreach ($orderBy as $fieldName => $order) {
            switch ($fieldName) {
                case 'levelName':
                    $query->orderBy('level_name', $order);
                    break;

                case 'createdAt':
                    $query->orderBy('created_at', $order);
                    break;

                default:
                    $query->orderBy($fieldName, $order);
                    break;
            }
        }

        $query->setMaxResults($itemsPerPage);
        $query->setFirstResult($offset);
        // Query database.
        $result = $query->execute()->fetchAllAssociative();

        $resp = [];
        // Normalize.
        foreach ($result as $object) {
            $item = [];
            $ulid = Ulid::fromBinary($object['id']);
            $item['id'] = $ulid->toBase32();
            $ulid = Ulid::fromBinary($object['point']);
            $item['point'] = $ulid->toBase32();
            $item['message'] = $object['message'];
            $aux = Json::decode($object['context']);
            $item['context'] = $aux;
            $aux = Json::decode($object['extra']);
            $item['extra'] = $aux;
            $item['level'] = $object['level'];
            $item['level_name'] = $object['level_name'];
            $item['country_code'] = $object['country_code'];
            $date = new \DateTime($object['created_at'], new DateTimezone('UTC'));
            $item['created_at'] = $date->format(\DateTime::W3C);
            $resp[] = $item;
        }
        // Format.
        $json = Json::encode($resp);
        // Response.
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * SIASAR Point get collection controller.
     *
     * @param Request $request
     * @param string  $formId
     *
     * @return Response
     */
    public function getPointCollection(Request $request, string $formId): Response
    {
        // Process parameters.
        $parameters = $this->getRequestParameters($request);
        $offset = $parameters['offset'];
        $itemsPerPage = $parameters['limit'];
        $orderBy = $parameters['order_by'];
        $filterBy = $parameters['filter_by'];

        // Validate filters.
        $allowFilters = ['name', 'code', 'parent', 'region', 'status', 'country', 'field_created', 'field_changed'];
        foreach ($filterBy as $field => $value) {
            if (!in_array($field, $allowFilters)) {
                return new JsonResponse(['message' => 'Expectation Failed'], 417, []);
            }
        }

        if (isset($filterBy["status"])) {
            $filterBy["status"] = explode(',', $filterBy["status"]);
            foreach ($filterBy["status"] as &$status) {
                $status = '"'.$status.'"';
            }
        }

        $queryBuilder = $this->connection->createQueryBuilder();
        $query = $queryBuilder
            ->select(
                'ad.id as ad_id',
                'ad.*',
                'fc.id as fc_id',
                'fpfc.id as fpfc_id',
                'fpfc.record'
            )
            ->from('administrative_division', 'ad');
        // Join form community.
        $query->leftJoin('ad', 'form_community', 'fc', "ad.id = fc.field_region_value && fc.field_deleted = '0'");
        // Join form_point__field_communities.
        $query->leftJoin('fc', 'form_point__field_communities', 'fpfc', 'fc.id = fpfc.field_communities_value');
        // Join form_point.
        $query->leftJoin('fpfc', 'form_point', 'fp', 'fp.id = fpfc.record');

        // Secondary table to limit to last point versions.
        $auxTable = '(SELECT 
      MAX(fp.field_changed_value) as max_changed_value, 
      MAX(fpfc.field_communities_value) as fc_id  
    FROM 
      form_point fp 
      JOIN form_point__field_communities fpfc ON fp.id = fpfc.record 
      JOIN form_community fc ON fc.id = fpfc.field_communities_value && fc.field_deleted = \'0\' 
    GROUP BY 
      fc.field_region_value)';

        // Left join ON.
        $auxTableOn = 'lastpoint.fc_id = fc.id 
    AND (
      fp.field_changed_value = lastpoint.max_changed_value 
      OR fpfc.record IS NULL
    )';
        $query->leftJoin('fpfc', $auxTable, 'lastpoint', $auxTableOn);

        // Filter by division access.
        if (!isset($filterBy['region'])) {
            $filterBy['region'] = [];
        } else {
            $filterBy['region'] = [$filterBy['region']];
        }

        foreach ($filterBy as $field => $value) {
            switch ($field) {
                case 'country':
                    /** @var Country $country */
                    $country = $this->iriConverter->getItemFromIri($filterBy['country']);
                    $query->andWhere($query->expr()->eq('ad.country_code', "'{$country->getCode()}'"));
                    $maxDeep = $country->getDeep();
                    // Limit administrative divisions to communities.
                    $query->andWhere($query->expr()->eq('ad.level', $maxDeep));
                    break;

                case 'parent':
                    // Find all division parents.
                    /** @var AdministrativeDivision $division */
                    $division = $this->iriConverter->getItemFromIri($filterBy['parent']);
                    // $communities = $this->getAllCommunityChildren($division);
                    $query->andWhere($query->expr()->like('ad.branch', '"'.$division->getBranch().'%"'));
                    break;

                case 'region':
                    $vals = [];
                    foreach ($filterBy['region'] as $region) {
                        /** @var AdministrativeDivision $division */
                        $division = $this->iriConverter->getItemFromIri($region);
                        if ($country->getDeep() === $division->getLevel()) {
                            $vals[] = '"'.strtoupper(bin2hex(Ulid::fromString($division->getId())->toBinary())).'"';
                        }
                    }
                    if (count($vals) > 0) {
                        $query->andWhere($query->expr()->in('HEX(ad.id)', $vals));
                    }
                    break;

                case 'status':
                    $filteredValues = [];
                    $withoutPlanning = false;
                    foreach ($filterBy["status"] as $filterStatus) {
                        if ('"without_planning"' === $filterStatus) {
                            $withoutPlanning = true;
                        } else {
                            $filteredValues[] = $filterStatus;
                        }
                    }
                    // Filter by digitizing, or valid status, and without planning if required.
                    $exp = [];
                    if (count($filteredValues) > 0) {
                        $exp[] = $query->expr()->in('fp.field_status', $filteredValues);
                    }
                    if ($withoutPlanning) {
                        $exp[] = $query->expr()->isNull('fpfc.field_communities_value');
                    }
                    if (count($filteredValues) > 0 || $withoutPlanning) {
                        $query->andWhere(call_user_func_array([$query->expr(), 'or'], $exp));
                    }
                    break;
                case 'field_created':
                case 'field_changed':
                    $user = $this->sessionService->getUser();
                    $timezone = new \DateTimeZone($user->getTimezone());
                    $date = new \DateTime($value, $timezone);
                    $date->setTimezone(new \DateTimeZone('UTC'));
                    $query->andWhere($query->expr()->gte('fp.'.$field.'_value', '"'.$date->format('Y-m-d H:i:s').'"'));
                    $query->andWhere($query->expr()->isNotNull('fc.id'));
                    break;
                default:
                    $query->andWhere($query->expr()->in($field, "'{$value}'"));
                    break;
            }
        }
        // Apply user division limits.
        $userDivisions = $this->accessManager->getUserDivisionsLimits(SessionService::getUser());
        $expresions = [];
        foreach ($userDivisions as $divLimit) {
            $expresions[] = $query->expr()->like('branch', '"'.$divLimit.'%"');
        }
        if (count($expresions) > 0) {
            $query->andWhere(
                call_user_func_array([$query->expr(), 'or'], $expresions)
            );
        }

        // Pagination
        $query->setMaxResults($itemsPerPage);
        $query->setFirstResult($offset);
        if (!$orderBy) {
            $query->orderBy('fp.field_changed_value', 'DESC');
        }
        // Apply order by.
        foreach ($orderBy as $field => $order) {
            switch ($field) {
                case 'field_created':
                case 'field_changed':
                    if ('asc' === $order) {
                        $query->andWhere($query->expr()->isNotNull('fc.id'));
                    }
                    $aux = '_value';
                    $query->orderBy('fp.'.$field.$aux, $order);
                    break;
                default:
                    $query->orderBy('fp.'.$field, $order);
                    break;
            }
        }

        // Remove duplicates points.
        $query->groupBy('ad_id')->having('MAX(fp.`field_changed_value`) || fc.id is null');
        // Limit response to the last point version.
        $query->andWhere('((lastpoint.max_changed_value IS NOT NULL AND fpfc.record = fp.id) OR fpfc.record IS NULL)');

        // Get results.
        $result = $query->execute()->fetchAllAssociative();
        $adRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $points = $this->formFactory->find('form.point');
        foreach ($result as &$value) {
            // Load SP record to init wrapper.
            $fPoint = null;
            if (isset($value['record'])) {
                $fPoint = $points->find($value['record']);
            }
            $point = new SiasarPointWrapper($fPoint, $this->entityManager, $this->sessionService, $this->iriConverter);
            /** @var AdministrativeDivision $division */
            $division = $adRepo->find($value['ad_id']);
            $nValue = $point->getMetaRecord($division);
            $value = $nValue;
        }
        $json = Json::encode($result);

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * SIASAR Point OpenAPI context.
     *
     * @param FormManagerInterface $form
     * @param OpenApi              $openApi
     *
     * @return Operation
     */
    public static function getPointCollectionOperation(FormManagerInterface $form, OpenApi $openApi):Operation
    {
        // Parameters.
        $parameters = [
            self::getPageParameter(),
            self::getOrderParameter('id'),
            self::getOrderParameter('name'),
            self::getOrderParameter('code'),
            self::getOrderParameter('field_created'),
            self::getOrderParameter('field_changed'),
            self::getFilterParameter('name'),
            self::getFilterParameter('code'),
            self::getFilterParameter('parent'),
            self::getFilterParameter('parent[]', true),
            self::getFilterParameter('region'),
            self::getFilterParameter('country', false, true),
            self::getFilterParameter('status', true, false),
            self::getFilterParameter('field_created'),
            self::getFilterParameter('field_changed'),
        ];
        // Response item schema.
        $oApifields = [
            'type' => 'object',
            'description' => 'Summarized SIASAR Point',
            'properties' => [
                // SP ID.
                'id' => ['type' => 'string', 'description' => 'Point ID'],
                // Original status code.
                'status_code' => ['type' => 'string', 'description' => 'Status code'],
                // Translated status code.
                'status' => ['type' => 'string', 'description' => 'Translated status code'],
                'version' => ['type' => 'string', 'description' => 'Point version'],
                'updated' => ['type' => 'string', 'format' => 'date-time', 'description' => 'Last update date time'],
                // Administrative division data.
                'administrative_division' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'array',
                        'items' => [
                            'type' => 'object',
                            'properties' => [
                                'id' => ['type' => 'string'],
                                'title' => ['type' => 'string'],
                            ],
                        ],
                    ],
                ],
                'community' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'string',
                        'description' => 'Comunity IDs',
                    ],
                ],
                'wsp' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'array',
                        'items' => [
                            'type' => 'object',
                            'properties' => [
                                'id' => ['type' => 'string'],
                                'title' => ['type' => 'string'],
                            ],
                        ],
                    ],
                ],
                'wsystem' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'array',
                        'items' => [
                            'type' => 'object',
                            'properties' => [
                                'id' => ['type' => 'string'],
                                'title' => ['type' => 'string'],
                            ],
                        ],
                    ],
                ],
                'allowed_actions' => [
                    'type' => 'array',
                    'description' => 'Allowed user actions to this SIASAR point.',
                    'items' => ['type' => 'string'],
                ],
                'notes' => [
                    'type' => 'array',
                    'description' => 'Warnings and errors messages count.',
                    'items' => [
                        'type' => 'object',
                        'properties' => [
                            'warnings' => ['type' => 'integer'],
                            'errors' => ['type' => 'integer'],
                        ],
                    ],
                ],
                'image' => [
                    'type' => 'string',
                    'description' => 'Image',
                ],
                'inquiry_relations' => [
                    'type' => 'object',
                    'properties' => [
                        'communities' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'object',
                                'properties' => [
                                    'id' => ['type' => 'string'],
                                    'label' => ['type' => 'string'],
                                ],
                            ],
                        ],
                        'systems' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'object',
                                'properties' => [
                                    'id' => ['type' => 'string'],
                                    'label' => ['type' => 'string'],
                                    'communities' => [
                                        'type' => 'array',
                                        'items' => [],
                                    ],
                                    'providers' => [
                                        'type' => 'array',
                                        'items' => [],
                                    ],
                                ],
                            ],
                        ],
                        'providers' => [
                            'type' => 'array',
                            'items' => [
                                'type' => 'object',
                                'properties' => [
                                    'id' => ['type' => 'string'],
                                    'label' => ['type' => 'string'],
                                    'f1.4' => ['type' => 'string'],
                                ],
                            ],
                        ],
                    ],
                ],
                'team' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        'properties' => [
                            'value' => ['type' => 'string'],
                            'class' => ['type' => 'string'],
                            'meta' => ['type' => 'object'],
                        ],
                    ],
                ],
            ],
            'required' => [],
        ];
        self::addComponentSchema($openApi, 'SIASARPointCollectionItem-GET', $oApifields);
        // Response.
        $response = new OpenApiResponse(
            self::t('Retrieves the SIASAR Points collection')->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'array',
                                'items' => [
                                    '$ref' => '#/components/schemas/SIASARPointCollectionItem-GET',
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            )
        );

        // Operation.
        return new Operation(
            'getPointCollection',
            [$form->getTitle()],
            [
                '200' => $response,
                '417' => new OpenApiResponse('Expectation Failed'),
            ],
            self::t('Get @desc', ['@desc' => $form->getDescription()])->render(),
            '',
            null,
            $parameters,
            null,
            null,
            false,
            null,
            null
        );
    }

    /**
     * SIASAR Point post collection controller.
     *
     * @param Request $request
     * @param string  $formId
     *
     * @return Response
     */
    public function postPointCollection(Request $request, string $formId): Response
    {
        $allowPlan = false;
        $requestBody = $request->getContent();
        try {
            $requestBody = Json::decode($requestBody);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(['message' => $e->getMessage()]), 400, [], true);
        }

        // Find administrative division.
        try {
            /** @var AdministrativeDivision $division */
            $division = $this->iriConverter->getItemFromIri($requestBody['administrative_division']);
        } catch (\Exception $e) {
            return new JsonResponse(
                json_encode(
                    [
                        'message' => $this->t(
                            'Administrative division not found: @iri',
                            ['@iri' => $requestBody['administrative_division']]
                        ),
                    ]
                ),
                400,
                [],
                true
            );
        }

        /** @var PointFormManager $pointManager */
        $pointManager = $this->formFactory->find('form.point');
        // Search community record not deleted in this region.
        $communityManager = $this->formFactory->find('form.community');

        /** @var FormRecord[] $communityRecords */
        $communityRecords = $communityManager->findBy([
            'field_region' => $division->getId(),
            'field_deleted' => '0',
            ], [
            // Younger first to clone it if it is necessary.
            'field_changed' => 'desc',
        ]);

        $point = null;
        foreach ($communityRecords as $communityRecord) {
            if (in_array($communityRecord->{'field_status'}, ['draft', 'finished'])) {
                $ulid = Ulid::fromString($communityRecord->{'id'});
                $point = current($pointManager->findBy(
                    [
                        'field_communities' => $ulid->toBinary(),
                    ],
                    [],
                    1
                ));

                if ($point) {
                    return new JsonResponse(['message' => self::t('The Point cannot be created. There is a previous version.')], 422);
                }
            }
        }

        // Start transaction
        // If not exist community records, create Point and the inquiry associated.
        $this->connection->superBeginTransaction();
        try {
            if (isset($requestBody['field_chk_to_plan']) && $requestBody['field_chk_to_plan']) {
                $allowPlan = true;
            }
            // Create community inquiry.
            $communityInquiryId = $communityManager->insert(
                [
                    'field_country' => $division->getCountry(),
                    'field_region' => $division,
                ]
            );
            $comm = $communityManager->find($communityInquiryId);
            // Create point with link to the new inquiry.
            $pointId = $pointManager->insert(
                [
                    'field_country' => $division->getCountry(),
                    'field_communities' => [$comm],
                    'field_chk_to_plan' => $allowPlan,
                ]
            );
            $metaPoint = new SiasarPointWrapper($pointManager->find($pointId), $this->entityManager, $this->sessionService, $this->iriConverter);
//            $metaPoint->getLogs()->info($this->t('Point created.'));
            // End transaction
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollback();
            throw $e;
        }

        $json = Json::encode($metaPoint->getMetaRecord());

        return new JsonResponse($json, 201, [], true);
    }

    /**
     * SIASAR Point OpenAPI context.
     *
     * @param FormManagerInterface $form
     * @param OpenApi              $openApi
     *
     * @return Operation
     */
    public static function postPointCollectionOperation(FormManagerInterface $form, OpenApi $openApi):Operation
    {
        $parameters = [];
        $requestBody = new RequestBody(
            self::t('The new @form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'object',
                                'description' => 'Source administrative division',
                                'properties' => [
                                    'administrative_division' => ['type' => 'string'],
                                    'field_chk_to_plan' => ['type' => 'string'],
                                ],
                                'required' => [],
                            ],
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            ),
            true
        );
        $responseOk = new OpenApiResponse(
            self::t('@form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-GET',
                            ]
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            )
        );

        return new Operation(
            'postCollectionFormRecordItem',
            [$form->getTitle()],
            [
                '201' => $responseOk,
                '400' => self::getResponseFail(self::t('Invalid input')->render(), true),
                '422' => self::getResponseFail(self::t('Unprocessable entity')->render(), true),
            ],
            self::t('Add @desc.', ['@desc' => $form->getDescription()])->render().' ('.self::t('Database transaction operation').')',
            '',
            null,
            [],
            $requestBody,
            null,
            false,
            null,
            null
        );
    }

    /**
     * Get SIASAR Point details.
     *
     * @param Request $request
     * @param string  $formId
     * @param string  $id
     *
     * @return Response
     */
    public function getPointItem(Request $request, string $formId, string $id): Response
    {
        $points = $this->formFactory->find('form.point');
        if (!$points) {
            return new JsonResponse(['message' => 'Point form not found'], 500, []);
        }

        $point = $points->find($id);
        if (!$point) {
            return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $id])], 404, []);
        }

        $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
        $json = Json::encode($metaPoint->getMetaRecord());

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * SIASAR Point OpenAPI context.
     *
     * @param FormManagerInterface $form
     * @param OpenApi              $openApi
     *
     * @return Operation
     */
    public static function getPointItemOperation(FormManagerInterface $form, OpenApi $openApi):Operation
    {
        $idParam = self::getIdParam();
        $response = new OpenApiResponse(
            self::t('Retrieves @form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/SIASARPointCollectionItem-GET',
                            ]
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            )
        );

        return new Operation(
            'getFormRecordItem',
            [$form->getTitle()],
            [
                '200' => $response,
                '404' => self::getResponseFail(self::t('Resource not found')->render()),
            ],
            self::t('Get @desc', ['@desc' => $form->getDescription()])->render(),
            '',
            null,
            [$idParam],
            null,
            null,
            false,
            null,
            null
        );
    }

    /**
     * Put & PATCH SIASAR Point.
     *
     * @param Request $request
     * @param string  $formId
     * @param string  $id
     *
     * @return Response
     */
    public function putPointItem(Request $request, string $formId, string $id): Response
    {
        $requestBody = $request->getContent();

        try {
            $requestBody = Json::decode($requestBody);
            $form = $this->formFactory->find($formId);
            $point = $form->find($id);

            if (!$point) {
                return new JsonResponse(["message" => self::t('Point not found.')], 404, []);
            }

            foreach ($requestBody as $fieldName => $value) {
                switch ($fieldName) {
                    // Ignored fields
                    case 'id':
                    case 'form':
                    case 'field_communities':
                    case 'field_wsystems':
                    case 'field_wsps':
                        break;

                    case 'field_status':
                        $protectedStatus = ['planning', 'checking', 'reviewing', 'calculated'];
                        if (in_array($value, $protectedStatus)) {
                            return new JsonResponse(
                                json_encode(
                                    [
                                        'message' => $this->t(
                                            "Point status is not allowed, it's not possible to set it to @status",
                                            ['@status' => implode(', ', $protectedStatus)]
                                        ),
                                    ]
                                ),
                                400,
                                [],
                                true
                            );
                        }
                        $point->set($fieldName, $value);
                        break;

                    default:
                        $point->set($fieldName, $value);
                        break;
                }
            }

            $this->connection->superBeginTransaction();
            try {
                $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
                $pointLogRepo = $this->entityManager->getRepository(PointLog::class);
                $logs = $pointLogRepo->findBy(['point' => $point->getId()]);
                foreach ($logs as $log) {
                    $pointLogRepo->remove($log);
                }
                $this->entityManager->flush();
                // Complete automation.
                // Save the request state.
                $form->update($point);

                $point = $form->find($point->getId());
                if ('complete' === $point->{'field_status'}) {
                    // Go to checking state.
                    $point->{'field_status'} = 'checking';
                    $point->save();
                    // Launch checking actions.
                    $validator = new SiasarPointCompleteChecks();
                    $result = $validator->checkPoint($point);
                    if (!$result) {
                        // If error go to digitizing.
                        $point->{'field_status'} = 'digitizing';
                        $metaPoint->getLogs()->error($this->t('This point have errors inside forms.'));
                    } else {
                        // Else, go to reviewing.
                        $point->{'field_status'} = 'reviewing';
                    }
                    $point->save();
                }

                $this->connection->superCommit();
            } catch (\Exception $e) {
                $this->connection->superRollBack();
                throw $e;
            }

//            $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
//            $metaPoint->getLogs()->info($this->t('Point updated.'));
            $json = Json::encode($metaPoint->getMetaRecord());

            return new JsonResponse($json, 200, [], true);
        } catch (\Exception $e) {
            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, true)), 422, [], true);
        }
    }

    /**
     * SIASAR Point OpenAPI context.
     *
     * @param FormManagerInterface $form
     * @param OpenApi              $openApi
     * @param string               $method  Uppercase HTTP method type.
     *
     * @return Operation
     */
    public static function putPointItemOperation(FormManagerInterface $form, OpenApi $openApi, string $method = 'PUT'):Operation
    {
        $method = strtolower($method);
        $idParam = self::getIdParam();
        $requestBody = new RequestBody(
            self::t('The @form resource updates', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-POST',
                            ]
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            ),
            true
        );
        $response = new OpenApiResponse(
            self::t('Retrieves the collection of @form resources', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'array',
                                'items' => [
                                    '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-GET',
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            )
        );

        return new Operation(
            $method.'FormRecordItem',
            [$form->getTitle()],
            [
                '200' => $response,
                '400' => self::getResponseFail(self::t('Invalid input')->render(), true),
                '404' => self::getResponseFail(self::t('Resource not found')->render()),
                '422' => self::getResponseFail(self::t('Unprocessable entity')->render(), true),
            ],
            self::t('Update @desc', ['@desc' => $form->getDescription()])->render(),
            '',
            null,
            [$idParam],
            $requestBody,
            null,
            false,
            null,
            null
        );
    }

    /**
     * SIASAR Point OpenAPI context.
     *
     * @param FormManagerInterface $form
     * @param OpenApi              $openApi
     * @param string               $method  Uppercase HTTP method type.
     *
     * @return Operation
     */
    public static function patchPointItemOperation(FormManagerInterface $form, OpenApi $openApi, string $method = 'PUT'):Operation
    {
        return self::putPointItemOperation($form, $openApi, 'PATCH');
    }

    /**
     * Delete SIASAR Point.
     *
     * @param Request $request
     * @param string  $formId
     * @param string  $id
     *
     * @return Response
     */
    public function deletePointItem(Request $request, string $formId, string $id): Response
    {
        $pointManager = $this->formFactory->find($formId);

        $this->connection->superBeginTransaction();
        try {
            $point = $pointManager->find($id);
            if (!$point) {
                throw new \Exception($this->t("Resource not found."), 404);
            }
            if ('planning' !== $point->{'field_status'}) {
                throw new \Exception($this->t('Point can\'t be removed.'), 422);
            }
            // Check the inquiries status
            $inquiries = array_merge($point->{'field_communities'}, $point->{'field_wsystems'});
            foreach ($inquiries as $record) {
                if ('draft' !== $record->{'field_status'}) {
                    throw new \Exception($this->t('Point can\'t be removed. It must have all inquiries in draft.'), 422);
                }
            }
            // Don't delete WSP, so unlink from this point
            foreach ($point->{'field_wsps'} as $wsp) {
                $wsp->{'field_point'} = null;
                $wsp->getForm()->update($wsp, true);
            }
            $point->{'field_wsps'} = [];
            $pointManager->update($point, true);

            $pointManager->drop($id);
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();
            throw $e;
        }

        return new JsonResponse(['message' => self::t('Point deleted.')], 204, []);
    }

    /**
     * SIASAR Point OpenAPI context.
     *
     * @param FormManagerInterface $form
     * @param OpenApi              $openApi
     *
     * @return Operation
     */
    public static function deletePointItemOperation(FormManagerInterface $form, OpenApi $openApi):Operation
    {
        $idParam = self::getIdParam();

        return new Operation(
            'deleteFormRecordItem',
            [$form->getTitle()],
            [
                '204' => self::getResponseFail(self::t('@form resource deleted', ['@form' => $form->getId()])->render()),
                '404' => self::getResponseFail(self::t('Resource not found')->render()),
                '422' => self::getResponseFail(self::t('Unprocessable entity')->render()),
            ],
            self::t('Delete @desc', ['@desc' => $form->getDescription()])->render(),
            '',
            null,
            [$idParam],
            null,
            null,
            false,
            null,
            null
        );
    }

    /**
     * SIASAR Point post WSP.
     *
     * @Route(
     *     name="point_wsprovider",
     *     path="/form/data/form.points/{id}/form.wsprovider",
     *     methods={"POST"},
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postPointWSPFormRecordItem(Request $request): Response
    {
        $user = $this->sessionService->getUser();
        $requestBody = $request->getContent();
        try {
            $idPoint = $request->get('id');
            $requestBody = Json::decode($requestBody);
            $pointManager = $this->formFactory->find('form.point');
            $point = $pointManager->find($idPoint);

            if (!$point) {
                return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $idPoint])], 404, []);
            }

            if ($user->getCountry()->getId() !== $point->{'field_country'}->getId()) {
                return new JsonResponse(
                    [
                        'message' => self::t('[form.wsprovider] This form requires the user to be in the same country as the point.'),
                    ],
                    401
                );
            }

            if ('planning' !== $point->{'field_status'} && 'digitizing' !== $point->{'field_status'}) {
                return new JsonResponse(
                    [
                        'message' => self::t('You can\'t add new inquiries in this point. It\'s on status "@status".', ["@status" => $point->{'field_status'}]),
                    ],
                    422,
                    []
                );
            }

            $this->connection->superBeginTransaction();
            try {
                // Insert a new WSP
                $wspForm = $this->formFactory->find('form.wsprovider');
                $requestBody['field_point'] = $point;
                $wspId = $wspForm->insert($requestBody);
                $wsp = $wspForm->find($wspId);

                // Add the WSP to Point
                $wsps = $point->{'field_wsps'};
                $wsps[] = $wsp;
                $point->{'field_wsps'} = $wsps;
                $point->save();

                $point = $pointManager->find($idPoint);
//                $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
//                $metaPoint->getLogs()->info($this->t('Point updated.'));

                $this->connection->superCommit();

                return new JsonResponse($wsp->toJson(), 201, [], true);
            } catch (\Exception $e) {
                $this->connection->superRollback();
                throw $e;
            }
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 400, []);
        }
    }

    /**
     * SIASAR Point post Water Supply System.
     *
     * @Route(
     *     name="point_wssystem",
     *     path="/form/data/form.points/{id}/form.wssystem",
     *     methods={"POST"},
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postPointWSSystemFormRecordItem(Request $request): Response
    {
        $user = $this->sessionService->getUser();
        $requestBody = $request->getContent();
        try {
            $idPoint = $request->get('id');
            $requestBody = Json::decode($requestBody);
            $pointManager = $this->formFactory->find('form.point');
            $point = $pointManager->find($idPoint);

            if (!$point) {
                return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $idPoint])], 404, []);
            }

            if ($user->getCountry()->getId() !== $point->{'field_country'}->getId()) {
                return new JsonResponse(
                    [
                        'message' => self::t('[form.wssystem] This form requires the user to be in the same country as the point.'),
                    ],
                    401
                );
            }

            if ('planning' !== $point->{'field_status'} && 'digitizing' !== $point->{'field_status'}) {
                return new JsonResponse(
                    [
                        'message' => self::t('You can\'t add new inquiries in this point. It\'s on status "@status".', ["@status" => $point->{'field_status'}]),
                    ],
                    422,
                    []
                );
            }

            $this->connection->superBeginTransaction();
            try {
                // Insert a new WSSystem
                $formManager = $this->formFactory->find('form.wssystem');
                $wssystemId = $formManager->insert($requestBody);
                $wssystem = $formManager->find($wssystemId);

                // Add the WSSystem to Point
                $wssystems = $point->{'field_wsystems'};
                $wssystems[] = $wssystem;
                $point->{'field_wsystems'} = $wssystems;
                $point->save();

                // Update System to execute preUpdate event
                $wssystem->set('field_deleted', $wssystem->get('field_deleted'));
                $wssystem->save();

//                $point = $pointManager->find($idPoint);
//                $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
//                $metaPoint->getLogs()->info($this->t('Point updated.'));
                $this->connection->superCommit();

                return new JsonResponse($wssystem->toJson(), 201, [], true);
            } catch (\Exception $e) {
                $this->connection->superRollback();
                throw $e;
            }
        } catch (\Exception $e) {
            return new JsonResponse(['message' => $e->getMessage()], 400, [], false);
        }
    }

    /**
     * SIASAR Point get inquiries.
     *
     * @Route(
     *     name="point_inquiries",
     *     path="/form/data/form.points/{id}/inquiries",
     *     methods={"GET"},
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getPointInquiries(Request $request): Response
    {
        $idPoint = $request->get('id');
        $pointManager = $this->formFactory->find('form.point');
        $point = $pointManager->find($idPoint);

        if (!$point) {
            return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $idPoint])], 404, []);
        }
        $results = [];
        /** @var FormRecord[] $comms */
        $comms = $point->{'field_communities'};
        foreach ($comms as $comm) {
            $deleted = !$comm || $comm->{'field_deleted'};
            if ($deleted) {
                continue;
            }
            $results[] = [
                'form_id' => 'form.community',
                'id' => $comm->getId(),
                'field_reference' => $comm->{'field_reference'},
                'field_ulid_reference' => $comm->{'field_ulid_reference'},
                'field_validated' => $comm->{'field_validated'},
                'field_deleted' => $comm->{'field_deleted'},
                'field_status' => $comm->{'field_status'},
                'field_country_value' => $comm->get('field_country')['value'],
                'field_region_value' => $comm->get('field_region')['value'],
                'field_have_households' => $comm->{'field_have_households'},
            ];
        }
        /** @var FormRecord[] $wsss */
        $wsss = $point->{'field_wsystems'};
        foreach ($wsss as $wss) {
            $deleted = !$wss || $wss->{'field_deleted'};
            if ($deleted) {
                continue;
            }
            $results[] = [
                'form_id' => 'form.wssystem',
                'id' => $wss->getId(),
                'field_reference' => $wss->{'field_reference'},
                'field_ulid_reference' => $wss->{'field_ulid_reference'},
                'field_validated' => $wss->{'field_validated'},
                'field_deleted' => $wss->{'field_deleted'},
                'field_status' => $wss->{'field_status'},
                'field_country_value' => $wss->get('field_country')['value'],
                'field_region_value' => $wss->get('field_region')['value'],
            ];
        }
        /** @var FormRecord[] $wsps */
        $wsps = $point->{'field_wsps'};
        foreach ($wsps as $wsp) {
            $deleted = !$wsp || $wsp->{'field_deleted'};
            if ($deleted) {
                continue;
            }
            $results[] = [
                'form_id' => 'form.wsprovider',
                'id' => $wsp->getId(),
                'field_reference' => $wsp->{'field_reference'},
                'field_ulid_reference' => $wsp->{'field_ulid_reference'},
                'field_validated' => $wsp->{'field_validated'},
                'field_deleted' => $wsp->{'field_deleted'},
                'field_status' => $wsp->{'field_status'},
                'field_country_value' => $wsp->get('field_country')['value'],
                'field_region_value' => $wsp->get('field_region')['value'],
            ];
        }
        $resp = [];
        /** @var InquiryFormLogRepositorySecured $formLogRepo */
        $formLogRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        foreach ($results as $item) {
            $form = $this->getFormByIdCached($item['form_id']);
            $formMeta = $form->getMeta();
            // Find the most recent version.
            $record = $form->find($item["id"]);
            $aRecord = $record->toArray();

            $item = new \stdClass();
            $item->id = $record->getId();
            $item->type = $form->getId();
            $item->{'type_title'} = self::t($form->getTitle());
            $item->{'field_reference'} = $aRecord["field_reference"];
            $item->{'field_ulid_reference'} = $aRecord["field_ulid_reference"];
            $item->{'field_validated'} = $aRecord["field_validated"];
            $item->{'field_deleted'} = $aRecord["field_deleted"];
            $item->{'field_status'} = $aRecord["field_status"];
            $item->{'field_country'} = $aRecord["field_country"];
            $item->{'field_region'} = $aRecord["field_region"];
            $item->{'field_creator'} = $aRecord["field_creator"];
            $item->{'field_editors'} = $aRecord["field_editors"];
            $item->{'field_editors_update'} = $aRecord["field_editors_update"];
            if (isset($aRecord["field_have_households"])) {
                $item->{'field_have_households'} = ($aRecord["field_have_households"] === '1');
            }
            if (isset($formMeta['icon_field']) && !empty($formMeta['icon_field'])) {
                $item->{'field_image'} = $aRecord[$formMeta['icon_field']];
            } else {
                $item->{'field_image'} = '';
            }
            if (isset($formMeta['title_field']) && !empty($formMeta['title_field'])) {
                $item->{'field_title'} = $aRecord[$formMeta['title_field']];
            } else {
                $item->{'field_title'} = '';
            }
            // Add indicator information if exists.
            $item->{'indicator'} = '';
            $item->{'indicator_value'} = AbstractIndicator::VALUE_NOT_CALCULATED;
            $class = $form->getIndicatorClass();
            if (!empty($class)) {
                $context = new PointIndicatorContext($record, $point);
                /** @var AbstractIndicator $indicator */
                $indicator = new $class($context);
                $item->{'indicator'} = $indicator->getLabel(true);
                $item->{'indicator_value'} = $indicator->getValue(true);
            }
            // Add errors and warnings.
            $recordErrors = $formLogRepo->count([
                'formId' => $record->getForm()->getId(),
                'recordId' => $record->getId(),
                'level' => 400,
            ]);
            $recordWarnings = $formLogRepo->count([
                'formId' => $record->getForm()->getId(),
                'recordId' => $record->getId(),
                'level' => 300,
            ]);
            $item->{'logs'} = [
                'errors' => $recordErrors,
                'warnings' => $recordWarnings,
            ];
            $resp[] = Json::encode($item);
        }

        return new JsonResponse('['.implode(', ', $resp).']', 200, [], true);
    }

    /**
     * SIASAR Point delete Water Supply System.
     *
     * @Route(
     *     name="point_delete_wssystem",
     *     path="/form/data/form.points/{pid}/form.wssystem/{id}",
     *     methods={"DELETE"},
     * )
     *
     * @param Request $request
     * @param string  $pid
     * @param string  $id
     *
     * @return Response
     */
    public function deletePointWSSystemFormRecordItem(Request $request, string $pid, string $id): Response
    {
        try {
            $pointForm = $this->formFactory->find('form.point');
            $point = $pointForm->find($pid);

            if (!$point) {
                return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $pid])], 404, []);
            }

            $systemForm = $this->formFactory->find('form.wssystem');
            $system = $systemForm->find($id);

            if (!$system) {
                return new JsonResponse(['message' => self::t('WS System "@id" not found', ['@id' => $id])], 404, []);
            }

            $this->connection->superBeginTransaction();
            try {
                $allowed = false;
                // Check the System exists in the specific Point
                foreach ($point->{'field_wsystems'} as $pointSystem) {
                    if ($system->getId() === $pointSystem->getId()) {
                        $allowed = true;
                        break;
                    }
                }

                if (!$allowed) {
                    return new JsonResponse(['message' => self::t('WS System "@id" not found in the Point "@pid"', ['@id' => $id, '@pid' => $pid])], 404, []);
                }

                // Drop form record.
                $deletedId = $system->getId();
                $system->delete();
                // Remove form from point.
                $updatedValue = [];
                foreach ($point->get('field_wsystems') as $pointSystem) {
                    $ulid = Ulid::fromString($pointSystem['value']);
                    if ($deletedId !== $ulid->toBase32()) {
                        $updatedValue[] = $pointSystem;
                    }
                }
                $point->{'field_wsystems'} = $updatedValue;
                $point->save();
                $this->connection->superCommit();

//                $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
//                $metaPoint->getLogs()->info($this->t('System deleted.'));

                return new Response('', 204);
            } catch (\Exception $e) {
                $this->connection->superRollback();
                throw $e;
            }
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(['message' => $e->getMessage()]), 400, [], true);
        }
    }

    /**
     * SIASAR Point delete Community.
     *
     * @Route(
     *     name="point_delete_community",
     *     path="/form/data/form.points/{pid}/form.community/{sid}",
     *     methods={"DELETE"},
     * )
     *
     * @param Request $request
     * @param string  $pid
     * @param string  $sid
     *
     * @return Response
     */
    public function deletePointCommunityFormRecordItem(Request $request, string $pid, string $sid): Response
    {
        try {
            $pointForm = $this->formFactory->find('form.point');
            $point = $pointForm->find($pid);

            if (!$point) {
                return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $pid])], 404, []);
            }

            $communityForm = $this->formFactory->find('form.community');
            $community = $communityForm->find($sid);

            if (!$community) {
                return new JsonResponse(['message' => self::t('Community "@id" not found', ['@id' => $sid])], 404, []);
            }

            // We can only remove draft communities.
            if ('draft' !== $community->{'field_status'}) {
                return new JsonResponse(['message' => self::t('Community "@id" with status "@status" can\'t be removed', ['@id' => $sid, '@status' => $community->{'field_status'}])], 422, []);
            }

            $this->connection->superBeginTransaction();
            try {
                $allowed = false;
                // Check the Community exists in the specific Point
                foreach ($point->{'field_communities'} as $pointCommunity) {
                    if ($community->getId() === $pointCommunity->getId()) {
                        $allowed = true;
                        break;
                    }
                }

                if (!$allowed) {
                    return new JsonResponse(['message' => self::t('Community "@cid" not found in the Point "@pid"', ['@cid' => $sid, '@pid' => $pid])], 404, []);
                }

                // We can't remove referenced communities.
                /** @var FormRecord $pointSystem */
                foreach ($point->{'field_wsystems'} as $pointSystem) {
                    $references = $pointSystem->{'field_served_communities'};
                    /** @var FormRecord $reference */
                    foreach ($references as $reference) {
                        $refComm = $reference->{'field_community'};
                        if ($refComm && $community->{'field_region'}) {
                            if ($community->{'field_region'}->getId() === $refComm->getId()) {
                                return new JsonResponse(['message' => self::t('Community "@cid" have references in the Point "@pid"', ['@cid' => $sid, '@pid' => $pid])], 422, []);
                            }
                        }
                    }
                }

                // Remove form from point.
                $updatedValue = [];
                foreach ($point->{'field_communities'} as $pointCommunity) {
                    if ($community->getId() !== $pointCommunity->getId()) {
                        $updatedValue[] = $pointCommunity;
                    }
                }
                // We can't remove the last community in the point.
                if (count($updatedValue) === 0) {
                    return new JsonResponse(['message' => self::t('Community "@cid" is the last in the Point "@pid", we can\'t remove it', ['@cid' => $sid, '@pid' => $pid])], 422, []);
                }

                // Drop form record.
                $community->delete();

                $point->{'field_communities'} = $updatedValue;
                $point->save();

                $this->connection->superCommit();

//                $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
//                $metaPoint->getLogs()->info($this->t('Community deleted.'));

                return new Response('', 204);
            } catch (\Exception $e) {
                $this->connection->superRollback();
                throw $e;
            }
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(['message' => $e->getMessage()]), 400, [], true);
        }
    }

    /**
     * SIASAR Point delete Community households.
     *
     * @Route(
     *     name="point_delete_community_households",
     *     path="/form/data/form.points/{pid}/form.community/{sid}/households",
     *     methods={"DELETE"},
     * )
     *
     * @param Request $request
     * @param string  $pid
     * @param string  $sid
     *
     * @return Response
     */
    public function deletePointCommunityHouseholdsItem(Request $request, string $pid, string $sid): Response
    {
        try {
            $pointForm = $this->formFactory->find('form.point');
            $point = $pointForm->find($pid);

            if (!$point) {
                return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $pid])], 404, []);
            }

            $communityForm = $this->formFactory->find('form.community');
            $community = $communityForm->find($sid);

            if (!$community) {
                return new JsonResponse(['message' => self::t('Community "@id" not found', ['@id' => $sid])], 404, []);
            }

            // We can only remove draft communities.
            if ('draft' !== $community->{'field_status'}) {
                return new JsonResponse(['message' => self::t('Community "@id" with status "@status" can\'t remove households', ['@id' => $sid, '@status' => $community->{'field_status'}])], 422, []);
            }

            $this->connection->superBeginTransaction();
            try {
                $allowed = false;
                // Check the Community exists in the specific Point
                foreach ($point->{'field_communities'} as $pointCommunity) {
                    if ($community->getId() === $pointCommunity->getId()) {
                        $allowed = true;
                        break;
                    }
                }

                if (!$allowed) {
                    return new JsonResponse(['message' => self::t('Community "@cid" not found in the Point "@pid"', ['@cid' => $sid, '@pid' => $pid])], 404, []);
                }

                $hasHouseholds = $community->{'field_have_households'};
                if ($hasHouseholds) {
                    // Get process and remove all households inquiries in process.
                    /** @var HouseholdProcess $process */
                    $process = $community->{'field_household_process'};
                    $households = $process->getHouseholdInquiries();
                    foreach ($households as $household) {
                        $household->delete();
                    }
                    // Set have household field to false.
                    $community->{'field_have_households'} = false;
                    // Remove process reference in community.
                    $community->set('field_household_process', []);
                    $community->getForm()->update($community, true);
                    // Remove process.
                    /** @var HouseholdProcessRepositorySecured $hhProcessRepo */
                    $hhProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
                    $process = $hhProcessRepo->find($process->getId());
                    $this->entityManager->remove($process);
                    $this->entityManager->flush();
                } else {
                    $this->connection->superRollback();

                    return new JsonResponse(['message' => self::t('Community "@cid" have not households inquiries.', ['@cid' => $sid])], 422, []);
                }
                $this->connection->superCommit();

                return new Response('', 204);
            } catch (\Exception $e) {
                $this->connection->superRollback();
                throw $e;
            }
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(['message' => $e->getMessage()]), 400, [], true);
        }
    }

    /**
     * SIASAR Point delete WS Provider.
     *
     * @Route(
     *     name="point_delete_wsprovider",
     *     path="/form/data/form.points/{pid}/form.wsprovider/{fid}",
     *     methods={"DELETE"},
     * )
     *
     * @param Request $request
     * @param string  $pid
     * @param string  $fid
     *
     * @return Response
     */
    public function deletePointWSPFormRecordItem(Request $request, string $pid, string $fid): Response
    {
        try {
            $pointForm = $this->formFactory->find('form.point');
            $point = $pointForm->find($pid);

            if (!$point) {
                return new JsonResponse(['message' => self::t('Point "@id" not found', ['@id' => $pid])], 404, []);
            }

            $wspForm = $this->formFactory->find('form.wsprovider');
            $wsp = $wspForm->find($fid);

            if (!$wsp) {
                return new JsonResponse(['message' => self::t('WS Provider "@id" not found', ['@id' => $fid])], 404, []);
            }

            $this->connection->superBeginTransaction();
            try {
                $allowed = false;
                // Check the Provider exists in the specific Point
                foreach ($point->{'field_wsps'} as $pointWsp) {
                    if ($wsp->getId() === $pointWsp->getId()) {
                        $allowed = true;
                        break;
                    }
                }

                if (!$allowed) {
                    return new JsonResponse(['message' => self::t('WS Provider "@fid" not found in the Point "@pid"', ['@fid' => $fid, '@pid' => $pid])], 404, []);
                }

                // // Check if the WSP is linked to any System
                // foreach ($point->{'field_wsystems'} as $pointSystem) {
                //     $servedCommunities = $pointSystem->{'field_served_communities'};
                //     foreach ($servedCommunities as $subrecord) {
                //         if ($wsp->getId() === $subrecord->{'field_provider'}->getId()) {
                //             return new JsonResponse(['message' => self::t('WS Provider "@fid" is linked to system "@sid"', ['@fid' => $fid, '@sid' => $pointSystem->getId()])], 404, []);
                //         }
                //     }
                // }

                // Check if the WSP is in use in another point.
                $allowed = true;
                $page = 0;
                $points = $pointForm->findBy([], [], 25, $page * 25);
                while (count($points) > 0) {
                    foreach ($points as $otherPoint) {
                        // Not see in the point where we want to delete.
                        if ($point->getId() === $otherPoint->getId()) {
                            continue;
                        }
                        // Check the Provider exists in the specific Point
                        foreach ($otherPoint->{'field_wsps'} as $pointWsp) {
                            if ($wsp->getId() === $pointWsp->getId()) {
                                $allowed = false;
                                break;
                            }
                        }
                        if (!$allowed) {
                            break;
                        }
                    }
                    if (!$allowed) {
                        break;
                    }
                    // Get next page.
                    ++$page;
                    $points = $pointForm->findBy([], [], 25, $page * 25);
                }

                if (!$allowed) {
                    // Remove from this point field.
                    $refs = [];
                    foreach ($point->{'field_wsps'} as $pointWsp) {
                        if ($wsp->getId() !== $pointWsp->getId()) {
                            $refs[] = $pointWsp;
                            break;
                        }
                    }
                    $point->{'field_wsps'} = $refs;
                    $point->save();
                } else {
                    // Delete provider.
                    $wsp->delete();
                }

                $this->connection->superCommit();

                return new Response('', 204);
            } catch (\Exception $e) {
                $this->connection->superRollback();
                throw $e;
            }
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(['message' => $e->getMessage()]), 400, [], true);
        }
    }

    /**
     * Clone point
     *
     * @Route(
     *     name="clone_point",
     *     path="/form/data/form.points/{id}/clone",
     *     methods={"PUT", "PATCH"},
     * )
     *
     * @param Request $request Petition request.
     * @param string  $id      Point ID.
     *
     * @return void
     */
    public function clonePoint(Request $request, string $id): Response
    {
        try {
            $pointForm = $this->formFactory->find('form.point');
            $point = $pointForm->find($id);

            if (!$point) {
                return new JsonResponse(["message" => self::t('Point not found.')], 404, []);
            }

            if (!in_array($point->{'field_status'}, ['calculated'])) {
                return new JsonResponse(["message" => self::t("This point can't be cloned.")], 400, []);
            }

            // Check if the community exists in another alive point (not in "calculated" status)
            foreach ($point->{'field_communities'} as $communityRecord) {
                /** @var FormRecord $communityRecord */
                $ulid = Ulid::fromString($communityRecord->{'field_ulid_reference'});
                $pointComm = $pointForm->findBy(
                    [
                        'field_communities' => $ulid->toBinary(),
                        'field_status' => ['NOTIN' => ['calculated']],
                    ],
                    ['field_changed' => 'DESC'],
                    1
                );

                if (count($pointComm) > 0) {
                    return new JsonResponse(["message" => self::t("This point can't be cloned.")], 400, []);
                }
            }

            $this->connection->superBeginTransaction();
            try {
                $clonedRecord = $point->clone();
                $this->connection->superCommit();
            } catch (\Exception $e) {
                $this->connection->superRollBack();
                throw $e;
            }

            return new JsonResponse($clonedRecord->toJson(), 200, [], true);
        } catch (\Exception $e) {
            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, true)), 422, [], true);
        }
    }

    /**
     * Get the history of a Point
     *
     * @Route(
     *     name="point_history",
     *     path="/form/data/form.points/{id}/history",
     *     methods={"GET"},
     * )
     *
     * @param Request $request Petition request.
     * @param string  $id      Point ID.
     *
     * @return Response
     */
    public function getPointHistory(Request $request, string $id): Response
    {
        try {
            // Process parameters.
            $parameters = $this->getRequestParameters($request);
            $offset = $parameters['offset'];
            $itemsPerPage = $parameters['limit'];
            $orderBy = $parameters['order_by'];

            /** @var PointFormManager $pointForm */
            $pointForm = $this->formFactory->find('form.point');
            /** @var FormRecord $point */
            $point = $pointForm->find($id);
            if (!$point) {
                return new JsonResponse(["message" => self::t('Point not found.')], 404, []);
            }
            $communityRefs = [];
            foreach ($point->{'field_communities'} as $community) {
                $communityRefs[] = '"'.strtoupper(bin2hex(Ulid::fromString($community->{'field_ulid_reference'})->toBinary())).'"';
            }
            // Create query
            $query = $this->connection->createQueryBuilder()
                ->select(
                    'ad.id as ad_id',
                    'fp.id as point',
                    'fp.field_status',
                )
                ->from('administrative_division', 'ad');
            // Join form community.
            $query->leftJoin('ad', 'form_community', 'fc', "ad.id = fc.field_region_value && fc.field_deleted = '0'");
            // Join form_point__field_communities.
            $query->leftJoin('fc', 'form_point__field_communities', 'fpfc', 'fc.id = fpfc.field_communities_value');
            // Join form_point.
            $query->leftJoin('fpfc', 'form_point', 'fp', 'fp.id = fpfc.record');
            // Where clause
            $query->andWhere($query->expr()->in('HEX(fc.field_ulid_reference)', $communityRefs));
            $query->andWhere($query->expr()->or(
                $query->expr()->in('fp.field_status', ['"calculating"', '"calculated"']),
                $query->expr()->eq('HEX(fp.id)', '"'.strtoupper(bin2hex(Ulid::fromString($point->getId())->toBinary())).'"')
            ));
            $query->groupBy('fp.id');

            // Apply order by.
            if (!$orderBy) {
                $query->orderBy('fp.field_changed_value', 'DESC');
            }
            foreach ($orderBy as $field => $order) {
                switch ($field) {
                    case 'field_created':
                    case 'field_changed':
                        $aux = '_value';
                        $query->orderBy('fp.'.$field.$aux, $order);
                        break;
                    default:
                        $query->orderBy('fp.'.$field, $order);
                        break;
                }
            }
            // Pagination
            $query->setMaxResults($itemsPerPage);
            $query->setFirstResult($offset);
            $result = $query->execute()->fetchAllAssociative();
            $adRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
            foreach ($result as &$value) {
                // Load SP record to init wrapper.
                $point = null;
                if (isset($value['point'])) {
                    $point = $pointForm->find($value['point']);
                }
                $point = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
                /** @var AdministrativeDivision $division */
                $division = $adRepo->find($value['ad_id']);
                $nValue = $point->getMetaRecord($division);
                $value = $nValue;
            }

            return new JsonResponse(Json::encode($result), 200, [], true);
        } catch (\Exception $e) {
            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, true)), 422, [], true);
        }
    }

    /**
     * Replan point: create new Point from unlinked inquiries of another point.
     *
     * @Route(
     *     name="replan_point",
     *     path="/form/data/form.points/{id}/replan",
     *     methods={"PUT", "PATCH"},
     * )
     *
     * @param Request $request Petition request.
     * @param string  $id      Point ID.
     *
     * @return void
     */
    public function replanPoint(Request $request, string $id): Response
    {
        $requestBody = $request->getContent();
        try {
            $requestBody = Json::decode($requestBody);
            /** @var PointFormManager $pointForm */
            $pointForm = $this->formFactory->find('form.point');
            $currentPoint = $pointForm->find($id);
            if (!$currentPoint) {
                return new JsonResponse(["message" => self::t('Point not found.')], 404, []);
            }
            if ('digitizing' !== $currentPoint->{'field_status'}) {
                return new JsonResponse(["message" => self::t('You can\'t unlink inquiries in this point. It\'s on status "@status".', ["@status" => $currentPoint->{'field_status'}])], 422, []);
            }
            if (!isset($requestBody['form.community']) || empty($requestBody['form.community'])) {
                return new JsonResponse(["message" => self::t('Invalid request body.')], 400, []);
            }
            // Get linked communities with the point systems (field 1.12).
            $commBySystems = [];
            $wspBySystems = [];
            foreach ($currentPoint->{'field_wsystems'} as $systemRecord) {
                foreach ($systemRecord->{'field_served_communities'} as $subformRecord) {
                    if ($subformRecord->{'field_community'}) {
                        $commBySystems[] = $subformRecord->{'field_community'}->getId();
                    }
                    if ($subformRecord->{'field_provider'}) {
                        $wspBySystems[] = $subformRecord->{'field_provider'}->getId();
                    }
                }
            }
            $this->connection->superBeginTransaction();
            try {
                $division = null;
                $commToUnlink = [];
                $sysToUnlink = [];
                $wspToUnlink = [];
                foreach ($requestBody as $formId => $recordsId) {
                    $form = $this->formFactory->find($formId);
                    foreach ($recordsId as $recordId) {
                        /** @var FormRecord $record */
                        $record = $form->find($recordId);
                        if (!$record) {
                            return new JsonResponse(['message' => self::t('@type "@id" not found', ['@type' => $form->getTitle(), '@id' => $recordId])], 404, []);
                        }
                        $insidePoint = false;
                        switch ($formId) {
                            case "form.community":
                                $point = current($pointForm->findByCommunity($record->getId()));
                                if ($currentPoint->getId() === $point->getId()) {
                                    $insidePoint = true;
                                }
                                // Check if the community is linked to any system
                                if (in_array($record->{'field_region'}->getId(), $commBySystems)) {
                                    return new JsonResponse(['message' => self::t('Community "@name" is linked to a system.', ['@name' => $record->{'field_region'}->getName()])], 422, []);
                                }
                                if (!$division) {
                                    /** @var AdministrativeDivision $division */
                                    $division = $record->{'field_region'};
                                }
                                $commToUnlink[$record->getId()] = $record;
                                break;
                            case "form.wssystem":
                                $point = current($pointForm->findByWSystem($record->getId()));
                                if ($currentPoint->getId() === $point->getId()) {
                                    $insidePoint = true;
                                }
                                // Check if has linked inquiries in field 1.12
                                if (count($record->{'field_served_communities'}) > 0) {
                                    return new JsonResponse(['message' => self::t('The system "@id" has linked inquiries.', ['@id' => $record->getId()])], 422, []);
                                }
                                $sysToUnlink[$record->getId()] = $record;
                                break;
                            case "form.wsprovider":
                                foreach ($currentPoint->{'field_wsps'} as $wsp) {
                                    if ($record->getId() === $wsp->getId()) {
                                        $insidePoint = true;
                                        break;
                                    }
                                }
                                // Check if the community is linked to any system
                                if (in_array($record->getId(), $wspBySystems)) {
                                    return new JsonResponse(['message' => self::t('Provider "@id" is linked to a system.', ['@id' => $record->getId()])], 422, []);
                                }
                                $wspToUnlink[$record->getId()] = $record;
                                break;
                        }
                        // Check if the inquiry is in the current point
                        if (!$insidePoint) {
                            return new JsonResponse(['message' => self::t('@type "@id" not found in this point.', ['@type' => $form->getTitle(), '@id' => $record->getId()])], 404, []);
                        }
                    }
                }
                // Check that at least one community will be unlinked to create de new point.
                if (count($commToUnlink) === 0) {
                    return new JsonResponse(['message' => self::t('At least one community must be unlinked.')], 422, []);
                }
                // Check that the current point has as least one community
                if (count($commToUnlink) === count($currentPoint->{'field_communities'})) {
                    return new JsonResponse(['message' => self::t('There must be at least one community left in the Point.')], 422, []);
                }
                // Unlink communities in the current point
                $commToKeep = $currentPoint->{'field_communities'};
                foreach ($commToKeep as $recordId => $record) {
                    if (in_array($record->getId(), array_keys($commToUnlink))) {
                        unset($commToKeep[$recordId]);
                    }
                }
                // Unlink systems in the current point
                $sysToKeep = $currentPoint->{'field_wsystems'};
                foreach ($sysToKeep as $key => $record) {
                    if (in_array($record->getId(), array_keys($sysToUnlink))) {
                        unset($sysToKeep[$key]);
                    }
                }
                // Unlink providers in the current point
                $wspToKeep = $currentPoint->{'field_wsps'};
                foreach ($wspToKeep as $key => $record) {
                    if (in_array($record->getId(), array_keys($wspToUnlink))) {
                        unset($wspToKeep[$key]);
                    }
                }
                $currentPoint->{'field_communities'} = $commToKeep;
                $currentPoint->{'field_wsystems'} = $sysToKeep;
                $currentPoint->{'field_wsps'} = $wspToKeep;
                $currentPoint->save();

                // Add logs to the current point
                $metaCurrentPoint = new SiasarPointWrapper($pointForm->find($currentPoint->getId()), $this->entityManager, $this->sessionService, $this->iriConverter);
                $metaCurrentPoint->getLogs()->info(self::t('Unlinked communities.'));
                if ($sysToUnlink) {
                    $metaCurrentPoint->getLogs()->info(self::t('Unlinked water systems.'));
                }
                if ($wspToUnlink) {
                    $metaCurrentPoint->getLogs()->info(self::t('Unlinked providers.'));
                }

                // Create the new point with the selected inquiries.
                $pointId = $pointForm->insert(
                    [
                        'field_country' => $division->getCountry(),
                        'field_communities' => array_values($commToUnlink),
                        'field_wsystems' => $sysToUnlink ? array_values($sysToUnlink) : null,
                        'field_wsps' => $wspToUnlink ? array_values($wspToUnlink) : null,
                        'field_team' => $currentPoint->{'field_team'},
                    ]
                );
                $newPoint = $pointForm->find($pointId);
                $metaNewPoint = new SiasarPointWrapper($newPoint, $this->entityManager, $this->sessionService, $this->iriConverter);

                $this->connection->superCommit();
            } catch (\Exception $e) {
                $this->connection->superRollBack();
                throw $e;
            }

            return new JsonResponse(Json::encode($metaNewPoint->getMetaRecord()), 201, [], true);
        } catch (\Exception $e) {
            return new JsonResponse(Json::encode(ExceptionNormalizer::normalize($e, true)), 422, [], true);
        }
    }

    /**
     * Get all communities from a parent division.
     *
     * @param AdministrativeDivision $parent
     *
     * @return AdministrativeDivision[]
     */
    protected function getAllCommunityChildren(AdministrativeDivision $parent): array
    {
        $resp = [];
        $country = $parent->getCountry();
        // Is this division a community?
        if ($country->getDeep() === $parent->deep()) {
            // Return the community.
            $ulid = Ulid::fromString($parent->getId())->toBinary();

            return ['\''.strtoupper(bin2hex($ulid)).'\''];
        }
        $children = $parent->getAdministrativeDivisions();
        foreach ($children as $division) {
            $resp = array_merge($resp, $this->getAllCommunityChildren($division));
        }

        return $resp;
    }
}
