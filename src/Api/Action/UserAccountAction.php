<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Action;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\User;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\UserRepository;
use App\Security\Core\user\UserProvider;
use App\Security\Handlers\DoctrineAccessHandler;
use App\Service\AccessManager;
use App\Service\Mailer\MailerService;
use App\Service\Password\EncoderService;
use App\Service\Request\RequestService;
use App\Service\SessionService;
use App\Templating\TwigTemplate;
use App\Tools\ExceptionNormalizer;
use App\Tools\Json;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * User account controllers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserAccountAction
{
    protected UserRepository $userRepository;
    protected MailerService $mailer;
    protected EncoderService $encoderService;
    protected IriConverterInterface $iriConverter;
    protected EntityManagerInterface $em;
    protected DoctrineAccessHandler $accessHandler;

    /**
     * UserAccountAction constructor.
     *
     * @param UserRepository         $userRepository User repository.
     * @param MailerService          $mailer         Mailer service.
     * @param EncoderService         $encoderService Encoder service.
     * @param IriConverterInterface  $iriConverter
     * @param EntityManagerInterface $em
     * @param SessionService         $sessionService
     * @param AccessManager          $accessManager
     */
    public function __construct(UserRepository $userRepository, MailerService $mailer, EncoderService $encoderService, IriConverterInterface $iriConverter, EntityManagerInterface $em, SessionService $sessionService, AccessManager $accessManager)
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
        $this->encoderService = $encoderService;
        $this->iriConverter = $iriConverter;
        $this->em = $em;
        $this->accessHandler = new DoctrineAccessHandler(User::class, $accessManager, $sessionService, $iriConverter);
    }

    /**
     * Register a new user.
     *
     * @param Request $request Current request.
     *
     * @return Response
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function register(Request $request): Response
    {
        $name = RequestService::getField($request, 'username');
        $email = RequestService::getField($request, 'email');
        $password = RequestService::getField($request, 'password');
        $country = $this->iriConverter->getItemFromIri(RequestService::getField($request, 'country'));

        $requestBody = $request->getContent();
        try {
            $newUserData = (new Json())->decode($requestBody);
        } catch (\Exception $e) {
            return new JsonResponse(json_encode(ExceptionNormalizer::normalize($e, true)), 400, [], true);
        }

        $newUser = $this->userRepository->create($name, $email, $password, $country);
        if (isset($newUserData['administrativeDivision'])) {
            try {
                foreach ($newUserData['administrativeDivision'] as $iriDivision) {
                    $newUser->addAdministrativeDivision($this->iriConverter->getItemFromIri($iriDivision));
                }
            } catch (\Exception $e) {
                // Ignore divisions errors.
            }
        }
        if (isset($newUserData['roles'])) {
            $newUser->setRoles($newUserData['roles']);
        }
        if (isset($newUserData['active'])) {
            $newUser->setActive($newUserData['active']);
        }
        if (isset($newUserData['language'])) {
            $newUser->setLanguage($this->iriConverter->getItemFromIri($newUserData['language']));
        }
        if (isset($newUserData['timezone'])) {
            $newUser->setTimezone($newUserData['timezone']);
        }
        $this->userRepository->saveNow($newUser);

        $response = [
            "id" => $newUser->getId(),
            "username" => $newUser->getUsername(),
            "roles" => $newUser->getRoles(),
            "email" => $newUser->getEmail(),
            "active" => $newUser->isActive(),
            "created" => $newUser->getCreated(),
            "updated" => $newUser->getUpdated(),
            "language" => $newUser->getLanguage(),
            "timezone" => $newUser->getTimezone(),
            "gravatar" => $newUser->getGravatar(),
        ];

        return new JsonResponse($response, 201);
    }

    /**
     * Update user information.
     *
     * @param Request $request Current request.
     * @param User    $user    User to update with simple fields updated.
     *
     * @return User
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Request $request, User $user): User
    {
        // Doctrine can lose entities references if we don't use the same instance.
        // Here, we need ensure that all entities are loaded in the same repository instance.
        $user = $this->em->find(User::class, $user->getId());

        // Validate input field names.
        $allowedFieldNames = [
            "username",
            "roles",
            "password",
            "email",
            "active",
            "language",
            "country",
            "administrativeDivisions",
            "timezone",
        ];
        $input = \json_decode($request->getContent(), true);
        $inputFieldNames = array_keys($input);
        foreach ($inputFieldNames as $inputFieldName) {
            if (!in_array($inputFieldName, $allowedFieldNames)) {
                throw new BadRequestHttpException(\sprintf('Not valid field %s', $inputFieldName));
            }
        }

        $username = RequestService::getField($request, 'username', false);
        $roles = RequestService::getField($request, 'roles', false);
        $password = RequestService::getField($request, 'password', false);
        $email = RequestService::getField($request, 'email', false);
        $active = RequestService::getField($request, 'active', false);
        $timezone = RequestService::getField($request, 'timezone', false);

        $administrativeDivisions = RequestService::getField($request, 'administrativeDivisions', false);
        // These fields are auto-update in the request.
        $language = RequestService::getField($request, 'language', false);
        $country = RequestService::getField($request, 'country', false);

        // Update administrative divisions.
        if ($administrativeDivisions) {
            $newDivisions = [];
            // Instantiate all divisions to filter bad request,
            // We need preserve actual values in bad request case.
            /** @var AdministrativeDivisionRepository $divisionsRepository */
            foreach ($administrativeDivisions as $newDivision) {
                /** @var AdministrativeDivision $temp */
                $temp = $this->iriConverter->getItemFromIri($newDivision);
                /** @var AdministrativeDivision $div */
                $div = $this->em->find(AdministrativeDivision::class, $temp->getId());
                $newDivisions[] = $div;
            }
            // All ok, now clear and add new divisions.
            $user->clearAdministrativeDivision();
            foreach ($newDivisions as $newDivision) {
                $user->addAdministrativeDivision($newDivision);
            }
        }

        if (!empty($language)) {
            $user->setLanguage($this->iriConverter->getItemFromIri($language));
        }

        if (!empty($country) && $this->accessHandler->hasPermission($user, 'all countries')) {
            $user->setCountry($this->iriConverter->getItemFromIri($country));
        }

        if ($username && $username !== $user->getUsername()) {
            $user->setUsername($username);
        }

        if ($roles && !empty($roles)) {
            // todo Filter allowed roles
            $user->setRoles($roles);
        }

        if ($email && $email !== $user->getEmail()) {
            $user->setEmail($email);
        }

        if ($active) {
            $user->setActive($active);
        }

        if ($timezone) {
            $validate = new \DateTimeZone($timezone);
            $user->setTimezone($timezone);
        }

        $this->userRepository->saveNow($user);

        if (!empty($password)) {
            // Upgrade password save the entity too.
            $this->userRepository->upgradePassword(
                $user,
                $this->encoderService->generateEncodedPassword($user, $password)
            );
        }

        return $user;
    }

    /**
     * Delete a user.
     *
     * @param User $user User to delete.
     *
     * @return JsonResponse
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(User $user): JsonResponse
    {
        // Remove an user.
        $this->userRepository->removeNow($user);
        // Return an ok status code.
        $response = new JsonResponse();
        $response->setStatusCode(JsonResponse::HTTP_NO_CONTENT);

        return $response;
    }

    /**
     * Resend activation email.
     *
     * @param Request $request Current request.
     *
     * @return JsonResponse
     *
     * @throws NonUniqueResultException
     */
    public function resendActivationEmail(Request $request): JsonResponse
    {
        if (!(bool) $_ENV['APP_USER_AUTO_ACTIVATION']) {
            $email = RequestService::getField($request, 'email');
            $user = $this->userRepository->findOneByEmail($email);
            if ($user) {
                if ($user->isActive()) {
                    return new JsonResponse(['message' => 'Unprocessable entity'], 422);
                }
                // Send activation mail.
                // todo Duplicated code in Repository/UserRepository.php L:147.
                $payload = [
                    'name' => $user->getUsername(),
                    // todo Use Url generation by route.
                    'url' => \sprintf(
                        '%s%s?token=%s&uid=%s',
                        '/',
                        'client route',
                        $user->getToken(),
                        $user->getId()
                    ),
                ];
                $this->mailer->send($user->getEmail(), TwigTemplate::USER_REGISTER, $payload);

                return new JsonResponse(['message' => 'Activation email sent'], 200);
            }
        }

        return new JsonResponse(['message' => 'Unprocessable entity'], 422);
    }

    /**
     * Get users by country.
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route(
     *     name="country_users",
     *     path="/users/country/{country_code}",
     *     methods={"GET"},
     * )
     */
    public function countryUsers(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->accessHandler->getCurrentUser();
        $requestCountryCode = $request->get('country_code');

        // Validate user access in the requested country.
        $this->accessHandler->checkAccess(
            $user,
            'read safe',
            'doctrine user',
            [
                'country' => $this->em->getRepository(Country::class)->find($requestCountryCode),
            ]
        );

        $users = $this->userRepository->findBy(
            [
                'country' => $request->get('country_code'),
            ],
            ['username' => 'asc']
        );
        $resp = [];
        foreach ($users as $user) {
            $resp[] = [
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'gravatar' => $user->getGravatar(),
                'active' => $user->isActive(),
            ];
        }

        return new JsonResponse($resp, 200);
    }

    /**
     * Get current session user ID.
     *
     * @param UserRepository $userRepository User repository.
     * @param UserProvider   $userProvider   Security user provider.
     *
     * @return JsonResponse
     *
     * @throws NonUniqueResultException
     *
     * @Route(
     *     name="user_me",
     *     path="/users/me",
     *     methods={"GET"},
     * )
     */
    public function userMe(UserRepository $userRepository, UserProvider $userProvider): JsonResponse
    {
        $user = $userRepository->findOneByUserName($userProvider->getUser()->getUsername());

        $response = [
            'id' => $user->getId(),
            'name' => $user->getUsername(),
            'email' => $user->getEmail(),
            'avatar' => $user->getGravatar(),
            'country' => $user->getCountry()?->getId(),
            'language' => $user->getLanguage()?->getId(),
            'timezone' => $user->getTimezone(),
            'administrative_divisions' => [],
        ];
        foreach ($user->getAdministrativeDivisions() as $division) {
            $response['administrative_divisions'][] = [
                'id' => $division->getId(),
                'name' => $division->getName(),
            ];
        }

        return new JsonResponse($response, 200);
    }
}
