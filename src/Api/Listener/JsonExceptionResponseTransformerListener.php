<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Listener;

use ErrorException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * General error event handler.
 *
 * Change the error responses to JSON encoded one.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class JsonExceptionResponseTransformerListener
{
    protected LoggerInterface $phpLogger;

    /**
     * @param LoggerInterface $phpLogger
     */
    public function __construct(LoggerInterface $phpLogger)
    {
        $this->phpLogger = $phpLogger;
    }

    /**
     * On kernel exception handler.
     *
     * @param ExceptionEvent $event Exception event.
     *
     * @return void
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HttpExceptionInterface) {
            $data = [
                'class' => \get_class($exception),
                'code' => $exception->getStatusCode(),
                'message' => $exception->getMessage(),
            ];
            $event->setResponse($this->prepareResponse($data, $data['code']));
        } elseif ($exception instanceof ErrorException) {
            // Other unknown listener is transforming response to JSON,
            // but I can't found it.
            $this->phpLogger->error(
                $exception->getMessage(),
                [
                    'code' => $exception->getCode(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                ]
            );
        }
    }

    /**
     * Add extra response headers.
     *
     * @param array $data       Exception data.
     * @param int   $statusCode Final HTTP status code.
     *
     * @return JsonResponse
     */
    protected function prepareResponse(array $data, int $statusCode): JsonResponse
    {
        $response = new JsonResponse($data, $statusCode);
        $response->headers->set('Server-Time', \time());
        $response->headers->set('X-Error-Code', $statusCode);

        return $response;
    }
}
