<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Api\Listener;

use App\Kernel;
use App\Service\AccessManager;
use App\Service\ConfigurationService;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Yaml\Yaml;

/**
 * Restrict access to the site if it's in maintenance mode.
 */
class RequestListener
{
    use GetContainerTrait;

    protected ConfigurationService $configurationService;
    protected AccessManager $accessManager;
    protected ?SessionService $sessionService;

    /**
     * Constructor.
     *
     * @param AccessManager        $accessManager
     * @param ConfigurationService $configurationService
     */
    public function __construct(AccessManager $accessManager, ConfigurationService $configurationService)
    {
        $this->configurationService = $configurationService;
        $this->accessManager = $accessManager;
        $this->sessionService = static::getContainerInstance()->get('session_service');
    }

    /**
     * Stop response if the site it's in maintenance mode.
     *
     * @param RequestEvent $event
     *
     * @return void
     *
     * @throws \Exception
     */
    public function onKernelRequest(RequestEvent $event)
    {
        // Remove xdebug query parameters.
        $request = $event->getRequest();
        $queryParams = $request->query->keys();
        foreach ($queryParams as $param) {
            $lparam = strtolower($param);
            if (stripos($lparam, 'xdebug') !== false) {
                //$request->attributes->remove($param);
                $request->query->remove($param);
            }
        }
        // Manage maintenance mode
        if ($this->configurationService->inMaintenanceMode()) {
            if ($this->isPathException($event)) {
                // If it is a path excepted from maintenance mode run normally.
                return;
            }

            $user = $this->sessionService->getUser();
            if ($user) {
                $access = $this->accessManager->hasPermission($user, 'use the site in maintenance mode');
                if ($access) {
                    return;
                }
            }
            // Send maintenance mode "error".
            // @see https://www.drupal.org/project/drupal/issues/2674900#comment-10897346
            // @see https://developers.google.com/search/blog/2011/01/how-to-deal-with-planned-site-downtime
            $response = new Response('"This site is in maintenance mode."', 503);
            $response->headers->set('Retry-After', gmdate('D, d M Y H:i:s \G\M\T', strtotime('+1 hour')));
            $event->setResponse($response);
        }
    }

    /**
     * Verify if a path is excepted from maintenance mode.
     *
     * We can set path exception in /config/packages/maintenance_mode.yml
     *
     * @param RequestEvent $event
     *
     * @return bool
     */
    protected function isPathException(RequestEvent $event): bool
    {
        $request = $event->getRequest();
        $path = $request->getPathInfo();
        /** @var Kernel $kernel */
        $kernel = static::getKernelInstance();
        $settingsFile = $kernel->getProjectDir().'/config/packages/maintenance_mode.yml';
        if (is_file($settingsFile)) {
            $settings = Yaml::parse(file_get_contents($kernel->getProjectDir().'/config/packages/maintenance_mode.yml'));
            foreach ($settings["exception"] as $url) {
                if ($url === $path) {
                    return true;
                }
            }
        }

        return false;
    }
}
