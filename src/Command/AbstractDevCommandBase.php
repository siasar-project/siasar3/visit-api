<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Country;
use App\Traits\GetContainerTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Base class to dev/test commands.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
abstract class AbstractDevCommandBase extends Command
{
    use GetContainerTrait;

    protected ManagerRegistry $entityManager;
    protected ParameterBagInterface $systemSettings;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param string|null     $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->systemSettings = self::getContainerInstance()->getParameterBag();
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        if (Command::FAILURE === $this->validateEnvironment($io)) {
            return Command::FAILURE;
        }

        return command::INVALID;
    }

    /**
     * Validate and fix country list.
     *
     * @param SymfonyStyle $io           Input/Output handler.
     * @param array        $countryCodes Countries to validate.
     * @param bool         $allowNone
     *
     * @return int Command response code.
     */
    protected function validateCountryCodes(SymfonyStyle $io, array &$countryCodes, bool $allowNone = false): int
    {
        $countryRepository = $this->entityManager->getRepository(Country::class);
        if (in_array('all', $countryCodes)) {
            $haveNone = in_array('none', $countryCodes);
            $countries = $countryRepository->findAll();
            $countryCodes = [];
            /** @var Country $country */
            foreach ($countries as $country) {
                $countryCodes[$country->getId()] = $country->getId();
            }
            if ($allowNone && $haveNone) {
                $countryCodes['none'] = 'none';
            }
        }

        // Validate country codes.
        $countryList = [];
        $cleanCountryCodes = [];
        foreach ($countryCodes as $countryCode) {
            if ('none' !== $countryCode) {
                $country = $countryRepository->find($countryCode);
                if (!$country) {
                    $io->error(sprintf('Not valid country: "%s"', $countryCode));

                    return Command::INVALID;
                }
                $countryList[] = [$countryCode => $country->getName()];
                $cleanCountryCodes[] = $countryCode;
            } elseif ($allowNone) {
                $countryList[] = ['none' => 'Without country'];
                $cleanCountryCodes[] = 'none';
            }
        }
        $countryCodes = $cleanCountryCodes;
        if (count($countryCodes) === 0) {
            $io->error('No countries selected.');

            return Command::FAILURE;
        }

        $io->title('Generate instances to:');
        call_user_func_array([$io, 'definitionList'], $countryList);

        return Command::SUCCESS;
    }

    /**
     * Validate current environment.
     *
     * @param SymfonyStyle $io
     *
     * @return int|void
     */
    protected function validateEnvironment(SymfonyStyle $io)
    {
        $environment = $this->systemSettings->get('kernel.environment');
        $io->info('Environment: '.$environment);
        if ('prod' === $environment) {
            $io->error('This command can\'t run in production mode.');

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
