<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Traits\GetContainerTrait;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;

/**
 * MySQL commands base.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AbstractMysqlCommandBase extends Command
{
    use GetContainerTrait;

    protected Connection $connection;

    /**
     * Command constructor.
     *
     * @param Connection  $connection
     * @param string|null $name       The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($name);
        $this->connection = $connection;

        chdir(self::getKernelInstance()->getProjectDir());
    }

    /**
     * Get database connection settings.
     *
     * array(12) {
     *  ["url"]=> string
     *  ["driver"]=> string
     *  ["host"]=> string
     *  ["port"]=> int
     *  ["user"]=> string
     *  ["password"]=> string
     *  ["driverOptions"]=> array
     *  ["wrapperClass"]=> string
     *  ["defaultTableOptions"]=> array
     *  ["dbname"]=> string
     *  ["serverVersion"]=> string
     *  ["charset"]=> string
     * }
     *
     * @return array
     */
    protected function getDatabaseConfiguration(): array
    {
        return $this->connection->getParams();
    }
}
