<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Traits\GetContainerTrait;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

/**
 * MySQL commands base.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AbstractRemoteCommandBase extends AbstractMysqlCommandBase
{
    use GetContainerTrait;

    protected array $targetServer;
    protected SymfonyStyle $io;
    protected InputInterface $input;

    /**
     * Command constructor.
     *
     * @param Connection  $connection
     * @param string|null $name       The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);

        chdir(self::getKernelInstance()->getProjectDir());
    }

    /**
     * Get defined remote sites.
     *
     * @return array
     */
    protected function getSites(): array
    {
        return Yaml::parseFile('sites/sites.yml');
    }

    /**
     * Select a target server.
     *
     * @param SymfonyStyle $io
     *
     * @return array
     */
    protected function selectTarget(SymfonyStyle $io): array
    {
        $sites = $this->getSites();
        $targetName = $io->choice('Select target', array_keys($sites)+[99 => 'Cancel'], 99);
        if ('Cancel' === $targetName) {
            return [];
        }

        $sites[$targetName]['id'] = $targetName;

        return $sites[$targetName];
    }

    /**
     * Init remote context.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function initProcess(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->input = $input;
        $targetServer = $input->getArgument('server');

        $this->targetServer = $this->getTarget($targetServer);
        if (!$this->targetServer) {
            return command::FAILURE;
        }

        if (!$this->validateTarget()) {
            return command::FAILURE;
        }

        return command::SUCCESS;
    }

    /**
     * Get target server information.
     *
     * @param ?string $targetServer
     *
     * @return array|bool
     */
    protected function getTarget(?string $targetServer): array|bool
    {
        $sites = $this->getSites();
        if (empty($targetServer)) {
            $target = $this->selectTarget($this->io);
        } else {
            if (!isset($sites[$targetServer])) {
                $this->io->error(sprintf('Server %s not defined.', $targetServer));

                return false;
            }
            $target = $sites[$targetServer];
            $target['id'] = $targetServer;
        }

        return $target;
    }

    /**
     * Validate target server.
     *
     * @return bool
     */
    protected function validateTarget(): bool
    {
        $currentHost = gethostname();
        if ($currentHost === $this->targetServer['host']) {
            $this->io->error("This command doesn't run locally.");

            return false;
        }

        return true;
    }

    /**
     * Debug only message.
     *
     * @param string $msg
     *
     * @return void
     */
    protected function debug(string $msg)
    {
        if ($this->io->getVerbosity() === OutputInterface::VERBOSITY_DEBUG) {
            $this->io->text(sprintf('<fg=green>[DEBUG]</> %s', $msg));
        }
    }

    /**
     * Execute another console command.
     *
     * @param string $cmd
     * @param array  $args
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function callConsole(string $cmd, array $args): int
    {
        $command = $this->getApplication()->find($cmd);

        // Propagate no interaction.
        $args['--no-interaction'] = $this->input->getOption('no-interaction');
        if ($this->io->isDebug()) {
            $this->io->title(sprintf('Sub-command: %s', $cmd));
            var_dump($args);
        }

        $resp = $command->run(new ArrayInput($args), $this->io);
        if (command::SUCCESS !== $resp) {
            $this->io->error('Remote command error...');
        }
//        $this->exitWithError = $this->exitWithError || (command::SUCCESS === $resp);

        return $resp;
    }
}
