<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use App\Traits\GetContainerTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationExportCommand extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'configuration:export';
    protected static $defaultDescription = 'Export configuration into files. See "APP_CONFIG_EXPORT_DIR" in ".env".';
    protected ConfigurationRepository $configurationRepository;

    /**
     * Command constructor.
     *
     * @param ConfigurationRepository $configurationRepository Configuration repository.
     * @param string|null             $name                    The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ConfigurationRepository $configurationRepository, string $name = null)
    {
        $this->configurationRepository = $configurationRepository;

        parent::__construct($name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('key', 'k', InputOption::VALUE_OPTIONAL, 'Export a key without asking and exit', '');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        chdir(self::getKernelInstance()->getProjectDir());

        // Single key export?
        $aloneKey = $input->getOption('key');
        if ('' !== $aloneKey) {
            $cfg = $this->configurationRepository->find($aloneKey);
            if ($cfg) {
                file_put_contents(
                    $_ENV['APP_CONFIG_EXPORT_DIR'].'/'.$aloneKey.'.yml',
                    $this->getDatabaseYaml($cfg)
                );
                $io->text('Created: '.$aloneKey);

                return Command::SUCCESS;
            }
            $io->error(sprintf('Key "%s" not found.', $aloneKey));

            return Command::FAILURE;
        }

        // Load lists from files and database.
        $actions = [];
        $finder = new Finder();
        $finder->name('*.yml');
        $flConfigs = $finder->in($_ENV['APP_CONFIG_EXPORT_DIR']);
        $dbConfigs = $this->configurationRepository->findAll();
        // Compare database with files.
        foreach ($dbConfigs as $dbConfig) {
            $values = $dbConfig->getValue();
            if (isset($values['local_only']) && $values['local_only'] === true) {
                // Ignore local_only configurations.
                continue;
            }
            $file = $this->getFile($flConfigs, $dbConfig->getId());
            if ($file) {
                // Compare content.
                if ($this->getMd5File($file) !== $this->getMd5Database($dbConfig)) {
                    // Update.
                    $actions[$dbConfig->getId()] = [
                        'action' => 'update',
                        'id' => $dbConfig->getId(),
                        'object' => $dbConfig,
                    ];
                } else {
                    $actions[$dbConfig->getId()] = ['action' => 'nothing', 'id' => '', 'object' => null];
                }
            } else {
                // Create.
                $actions[$dbConfig->getId()] = [
                    'action' => 'create',
                    'id' => $dbConfig->getId(),
                    'object' => $dbConfig,
                ];
            }
        }
        // Files without database will delete.
        foreach ($flConfigs as $flConfig) {
            if (!in_array($flConfig->getFilenameWithoutExtension(), array_keys($actions))) {
                $actions[$flConfig->getFilenameWithoutExtension()] = [
                    'action' => 'delete',
                    'id' => $flConfig->getFilenameWithoutExtension(),
                    'object' => null,
                ];
            }
        }

        // Display actions before execute.
        $rows = [];
        $anyToDo = false;
        foreach ($actions as $action) {
            if ($action['action'] !== 'nothing') {
                $anyToDo = true;
                $row = [
                    $action['id'],
                    $action['action'],
                ];
                $rows[] = $row;
            }
        }
        if ($anyToDo) {
            $output->writeln('');
            $output->writeln(sprintf('Actions to do: %s', count($rows)));
            $io->table(['Configuration', 'Action'], $rows);
        } else {
            $io->success('Nothing to export.');

            return Command::SUCCESS;
        }

        // Ask user.
        if ($input->getOption('no-interaction')) {
            $io->text('The configuration will be overwrites.'.\PHP_EOL.'Continue? [y/N]');
            $io->info('Response by options: y');
        } else {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                'The configuration will be overwrites.'.\PHP_EOL.'Continue? [y/N]',
                false
            );
            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
            $output->writeln('');
        }

        // Export.
        foreach ($actions as $action) {
            switch ($action['action']) {
                case 'nothing':
                    break;

                case 'update':
                    file_put_contents(
                        $_ENV['APP_CONFIG_EXPORT_DIR'].'/'.$action['object']->getId().'.yml',
                        $this->getDatabaseYaml($action['object'])
                    );
                    $io->text('Updated: '.$action['object']->getId());
                    break;

                case 'create':
                    file_put_contents(
                        $_ENV['APP_CONFIG_EXPORT_DIR'].'/'.$action['object']->getId().'.yml',
                        $this->getDatabaseYaml($action['object'])
                    );
                    $io->text('Created: '.$action['object']->getId());
                    break;

                case 'delete':
                    unlink($_ENV['APP_CONFIG_EXPORT_DIR'].'/'.$action['id'].'.yml');
                    $io->text('Deleted: '.$action['id']);
                    break;
            }
        }

        $io->success('Exported configuration.');

        return Command::SUCCESS;
    }

    /**
     * Get file MD5.
     *
     * @param SplFileInfo $file The file.
     *
     * @return string
     */
    protected function getMd5File(SplFileInfo $file): string
    {
        return md5(file_get_contents($file->getPathname()));
    }

    /**
     * Get configuration MD5.
     *
     * @param ConfigurationRead $config Configuration.
     *
     * @return string
     */
    protected function getMd5Database(ConfigurationRead $config): string
    {
        return md5($this->getDatabaseYaml($config));
    }

    /**
     * Get Yaml configuration version.
     *
     * @param ConfigurationRead $config Configuration.
     *
     * @return string
     */
    protected function getDatabaseYaml(ConfigurationRead $config): string
    {
        return Yaml::dump(
            $config->getValue(),
            10,
            2,
            Yaml::DUMP_EXCEPTION_ON_INVALID_TYPE
        );
    }

    /**
     * Get a file from filter.
     *
     * @param Finder $files Filter.
     * @param string $id    Configuration ID.
     *
     * @return ?Finder
     */
    protected function getFile(Finder $files, string $id): ?SplFileInfo
    {
        foreach ($files as $file) {
            if ($file->getFilenameWithoutExtension() === $id) {
                return $file;
            }
        }

        return null;
    }
}
