<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Command\Event\ConfigurationPostImportEvent;
use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Traits\GetContainerTrait;
use MJS\TopSort\Implementations\ArraySort;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use App\Tools\ImportAction;

/**
 * Import configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationImportCommand extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'configuration:import';
    protected static $defaultDescription = 'Import configuration into database. See "APP_CONFIG_EXPORT_DIR" in ".env".';
    protected ConfigurationRepository $configurationRepository;
    protected EventDispatcherInterface $dispatcher;

    /**
     * Command constructor.
     *
     * @param ConfigurationRepository  $configurationRepository Configuration repository.
     * @param EventDispatcherInterface $eventDispatcher         Event dispatcher.
     * @param string|null              $name                    The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ConfigurationRepository $configurationRepository, EventDispatcherInterface $eventDispatcher, string $name = null)
    {
        $this->configurationRepository = $configurationRepository;
        $this->dispatcher = $eventDispatcher;

        parent::__construct($name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        chdir(self::getKernelInstance()->getProjectDir());

        // Load lists from files and database.
        $actions = [];
        $finder = new Finder();
        $finder->name('*.yml');
        $flConfigs = $finder->in($_ENV['APP_CONFIG_EXPORT_DIR']);
        $dbConfigs = $this->configurationRepository->findAll();
        // Compare files with database.
        foreach ($flConfigs as $flConfig) {
            $dbConfig = $this->configurationRepository->find($flConfig->getFilenameWithoutExtension());
            if ($dbConfig) {
                // Compare content.
                if ($this->getMd5File($flConfig) !== $this->getMd5Database($dbConfig)) {
                    // Update.
                    $newValue = $this->getFileContent($flConfig);
                    $actions[$dbConfig->getId()] = new ImportAction(
                        ImportAction::ACTION_UPDATE,
                        $dbConfig->getId(),
                        $flConfig,
                        0,
                        (isset($newValue['requires'])) ? $newValue['requires'] : []
                    );
                } else {
                    $actions[$dbConfig->getId()] = new ImportAction(ImportAction::ACTION_NOTHING, $dbConfig->getId(), null);
                }
            } else {
                // Create.
                $newValue = $this->getFileContent($flConfig);
                $actions[$flConfig->getFilenameWithoutExtension()] = new ImportAction(
                    ImportAction::ACTION_CREATE,
                    $flConfig->getFilenameWithoutExtension(),
                    $flConfig,
                    0,
                    (isset($newValue['requires'])) ? $newValue['requires'] : []
                );
            }
        }
        // Configurations without files will delete.
        foreach ($dbConfigs as $dbConfig) {
            if (!in_array($dbConfig->getId(), array_keys($actions))) {
                $values = $dbConfig->getValue();
                if (isset($values['local_only']) && $values['local_only'] === true) {
                    // Ignore local_only configurations.
                    continue;
                }
                $actions[$dbConfig->getId()] = new ImportAction(
                    ImportAction::ACTION_DELETE,
                    $dbConfig->getId(),
                    null
                );
            }
        }
        // Shuffle actions to force requirement of dependencies sorting.
        // $this->shuffleAssoc($actions);

        // Sort actions by requirements.
        $sorter = new ArraySort();
        foreach ($actions as $action) {
            $sorter->add($action->getId(), $action->getRequires());
        }
        $result = $sorter->sort();

        // Set the dependency weight.
        foreach ($result as $weight => $id) {
            $actions[$id]->setWeight($weight);
        }

        // Sort by weight.
        uksort(
            $actions,
            function (mixed $a, mixed $b) use ($actions) {
                $aA = $actions[$a];
                $aB = $actions[$b];

                if ($aA->getWeight() === $aB->getWeight()) {
                    return 0;
                }

                return ($aA->getWeight() > $aB->getWeight()) ? 1 : -1;
            }
        );

        // Display actions before execute.
        $rows = [];
        $anyToDo = false;
        foreach ($actions as $action) {
            if ($action->getAction() !== ImportAction::ACTION_NOTHING) {
                $anyToDo = true;
                $requires = '';
                $cnt = 0;
                foreach ($action->getRequires() as $require) {
                    $requires .= $require.' ';
                    $cnt++;
                    if ($cnt >= 3) {
                        $cnt = 0;
                        $requires .= "\n";
                    }
                }
                $row = [
                    $action->getWeight(),
                    $action->getId(),
                    $action->getAction(),
                    $requires,
                ];
                $rows[] = $row;
            }
        }
        if ($anyToDo) {
            $output->writeln('');
            $output->writeln(sprintf('Actions to do: %s', count($rows)));
            $io->table(['Weight', 'Configuration', 'Action', 'Dependencies'], $rows);
        } else {
            $io->success('Nothing to import.');
            $this->dispatchPostImportEvent($io, $input, $output);

            return Command::SUCCESS;
        }

        // Ask user.
        if ($input->getOption('no-interaction')) {
            $io->text('The configuration will be overwrites.'.\PHP_EOL.'Continue? [y/N]');
            $io->info('Response by options: y');
        } else {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                'The configuration will be overwrites.'.\PHP_EOL.'Continue? [y/N]',
                false
            );
            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
            $output->writeln('');
        }

        // Import.
        foreach ($actions as $action) {
            switch ($action->getAction()) {
                case ImportAction::ACTION_NOTHING:
                    break;

                case ImportAction::ACTION_UPDATE:
                    $io->text('<comment>[ ]</comment> Updating: '.$action->getId());
                    $content = Yaml::parseFile($_ENV['APP_CONFIG_EXPORT_DIR'].'/'.$action->getId().'.yml');
                    if (!$content) {
                        $content = [];
                    }
                    /** @var ConfigurationWriteInterface $cfg */
                    $cfg = $this->configurationRepository->findEditable($action->getId());

                    $event = new ConfigurationPreUpdateEvent(
                        $action->getId(),
                        $cfg->getValue(),
                        $content,
                        $io,
                        $input,
                        $output
                    );
                    $this->dispatcher->dispatch($event, ConfigurationPreUpdateEvent::NAME);

                    $cfg->setValue($content);
                    $cfg->saveNow();

                    $event = new ConfigurationPostUpdateEvent(
                        $action->getId(),
                        $cfg->getValue(),
                        $content,
                        $io,
                        $input,
                        $output
                    );
                    $this->dispatcher->dispatch($event, ConfigurationPostUpdateEvent::NAME);

                    $io->text('<info>[X]</info> Updated: '.$action->getId());
                    break;

                case ImportAction::ACTION_CREATE:
                    $io->text('<comment>[ ]</comment> Creating: '.$action->getId());
                    $content = Yaml::parseFile($_ENV['APP_CONFIG_EXPORT_DIR'].'/'.$action->getId().'.yml');
                    if (!$content) {
                        $content = [];
                    }
                    /** @var ConfigurationWriteInterface $cfg */
                    $cfg = $this->configurationRepository->create($action->getId(), []);

                    $event = new ConfigurationPreCreateEvent(
                        $action->getId(),
                        $cfg->getValue(),
                        $content,
                        $io,
                        $input,
                        $output
                    );
                    $this->dispatcher->dispatch($event, ConfigurationPreCreateEvent::NAME);

                    $cfg->setValue($content);
                    $cfg->saveNow();

                    $event = new ConfigurationPostCreateEvent(
                        $action->getId(),
                        $cfg->getValue(),
                        $content,
                        $io,
                        $input,
                        $output
                    );
                    $this->dispatcher->dispatch($event, ConfigurationPostCreateEvent::NAME);

                    $io->text('<info>[X]</info> Created: '.$action->getId());
                    break;

                case ImportAction::ACTION_DELETE:
                    $io->text('<comment>[ ]</comment> Deleting: '.$action->getId());
                    /** @var ConfigurationWriteInterface $cfg */
                    $cfg = $this->configurationRepository->findEditable($action->getId());

                    $event = new ConfigurationPredeleteEvent(
                        $action->getId(),
                        $cfg->getValue(),
                        null,
                        $io,
                        $input,
                        $output
                    );
                    $this->dispatcher->dispatch($event, ConfigurationPredeleteEvent::NAME);

                    $cfg->delete();

                    $event = new ConfigurationPostdeleteEvent(
                        $action->getId(),
                        $cfg->getValue(),
                        null,
                        $io,
                        $input,
                        $output
                    );
                    $this->dispatcher->dispatch($event, ConfigurationPostdeleteEvent::NAME);

                    $io->text('<info>[X]</info> Deleted: '.$action->getId());
                    break;
            }
        }
        $this->dispatchPostImportEvent($io, $input, $output);

        $io->success('Imported configuration.');

        return Command::SUCCESS;
    }

    /**
     * Throw post import event.
     *
     * @param SymfonyStyle|null    $io
     * @param InputInterface|null  $input
     * @param OutputInterface|null $output
     *
     * @return void
     */
    protected function dispatchPostImportEvent(SymfonyStyle $io = null, InputInterface $input = null, OutputInterface $output = null): void
    {
        $event = new ConfigurationPostImportEvent($io, $input, $output);
        $this->dispatcher->dispatch($event, ConfigurationPostImportEvent::NAME);
    }

    /**
     * Shuffle for associative arrays, preserves key=>value pairs.
     *
     * @author Vladimir Kornea of typetango.com
     * @author ahmad at ahmadnassri dot com <https://www.php.net/manual/en/function.shuffle.php#94697>
     *
     * @param $array
     *
     * @return bool
     */
    protected function shuffleAssoc(&$array): bool
    {
        $keys = array_keys($array);
        $new = [];

        shuffle($keys);

        foreach ($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }

    /**
     * Get configuration file content.
     *
     * @param SplFileInfo $file
     *
     * @return array
     */
    protected function getFileContent(SplFileInfo $file): array
    {
        try {
            return Yaml::parseFile($file->getPathname());
        } catch (ParseException $e) {
            throw new ParseException(
                'Malformed inline YAML string',
                $e->getParsedLine(),
                $e->getSnippet(),
                $file->getFilename()
            );
        }
    }

    /**
     * Get file MD5.
     *
     * @param SplFileInfo $file The file.
     *
     * @return string
     */
    protected function getMd5File(SplFileInfo $file): string
    {
        return md5(file_get_contents($file->getPathname()));
    }

    /**
     * Get configuration MD5.
     *
     * @param mixed $config Configuration.
     *
     * @return string
     */
    protected function getMd5Database(mixed $config): string
    {
        return md5($this->getDatabaseYaml($config));
    }

    /**
     * Get Yaml configuration version.
     *
     * @param ConfigurationRead $config Configuration.
     *
     * @return string
     */
    protected function getDatabaseYaml(ConfigurationRead $config): string
    {
        return Yaml::dump(
            $config->getValue(),
            10,
            2,
            Yaml::DUMP_EXCEPTION_ON_INVALID_TYPE
        );
    }

    /**
     * Get a file from filter.
     *
     * @param Finder $files Filter.
     * @param string $id    Configuration ID.
     *
     * @return ?Finder
     */
    protected function getFile(Finder $files, string $id): ?SplFileInfo
    {
        foreach ($files as $file) {
            if ($file->getFilenameWithoutExtension() === $id) {
                return $file;
            }
        }

        return null;
    }
}
