<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\AdministrativeDivision;
use App\Entity\Configuration;
use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\DefaultDiameter;
use App\Entity\Ethnicity;
use App\Entity\FunderIntervention;
use App\Entity\InstitutionIntervention;
use App\Entity\Material;
use App\Entity\OfficialLanguage;
use App\Entity\ProgramIntervention;
use App\Entity\PumpType;
use App\Entity\SpecialComponent;
use App\Entity\TechnicalAssistanceProvider;
use App\Entity\TypeIntervention;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\CountryRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use loophp\phptree\Exporter\Ascii;
use loophp\phptree\Node\ValueNode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineCleanCommand extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:entity:clear';
    protected static $defaultDescription = 'Clean Doctrine entities.';

    protected ManagerRegistry $entityManager;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param string|null     $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, string $name = null)
    {
        parent::__construct($entityManager, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(
                'entity',
                InputArgument::REQUIRED,
                sprintf('Entity class name to clean. <info>E.g. %s</info>', 'AdministrativeDivision')
            )
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Clean entities in this country code.'
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (Command::FAILURE === parent::execute($input, $output)) {
            return Command::FAILURE;
        }
        $io = new SymfonyStyle($input, $output);
        $entityClass = 'App\\Entity\\'.$input->getArgument('entity');
        $countryCode = $input->getArgument('country');
        $countryCodes = [$countryCode];

        if (Command::SUCCESS !== ($returnCode = $this->validateCountryCodes($io, $countryCodes))) {
            return $returnCode;
        }

        $entityTypes = $this->getEntityTypes();
        if (!in_array($entityClass, $entityTypes)) {
            $io->error(sprintf('Entity type "%s" no valid.', $entityClass));

            return Command::FAILURE;
        }
        $entityTypes = [$entityClass];
        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->entityManager->getRepository(Country::class);
        // Clear entities.
        $this->entityManager->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($countryCodes as $countryCode) {
            // Find country.
            /** @var Country $country */
            $country = $countryRepository->find($countryCode);
            $io->info(sprintf('Deleting %s data...', $country->getName()));
            foreach ($entityTypes as $entityType) {
                $deleted = 0;
                switch ($entityType) {
                    case AdministrativeDivision::class:
                        $deleted = $this->clearDivisions($country);
                        break;
                    case Currency::class:
                        $deleted = $country->getCurrencies()->count();
                        $entities = $country->getCurrencies();
                        foreach ($entities as $entity) {
                            $country->removeCurrency($entity);
                        }
                        break;
                    case Ethnicity::class:
                        $deleted = $country->getEthnicities()->count();
                        $entities = $country->getEthnicities();
                        foreach ($entities as $entity) {
                            $country->removeEthnicity($entity);
                        }
                        break;
                    case FunderIntervention::class:
                        $deleted = $country->getFunderInterventions()->count();
                        $entities = $country->getFunderInterventions()->toArray();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                    case InstitutionIntervention::class:
                        $deleted = $country->getInstitutionInterventions()->count();
                        $entities = $country->getInstitutionInterventions();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                    case Material::class:
                        $deleted = $country->getMaterials()->count();
                        $entities = $country->getMaterials();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                    case OfficialLanguage::class:
                        $deleted = $country->getOfficialLanguages()->count();
                        $country->setMainOfficialLanguage(null);
                        $entities = $country->getOfficialLanguages();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                    case ProgramIntervention::class:
                        $deleted = $country->getProgramInterventions()->count();
                        $entities = $country->getProgramInterventions();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                    case TechnicalAssistanceProvider::class:
                        $deleted = $country->getTechnicalAssistanceProviders()->count();
                        $entities = $country->getTechnicalAssistanceProviders();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                    case TypeIntervention::class:
                        $deleted = $country->getTypeInterventions()->count();
                        $entities = $country->getTypeInterventions();
                        foreach ($entities as $entity) {
                            $this->entityManager->getManager()->remove($entity);
                        }
                        break;
                }
                $this->entityManager->getManager()->flush();
                $io->text($deleted.' '.$entityType.' deleted.');
            }
        }
        $this->entityManager->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 1;');
        $this->entityManager->getManager()->flush();
        $this->entityManager->getManager()->clear();

        return Command::SUCCESS;
    }

    /**
     * Format unit list.
     *
     * @param array $list
     *
     * @return string
     */
    protected function formatUnitsList(array $list): string
    {
        return implode(', ', array_keys($list));
    }

    /**
     * Get two random units.
     *
     * @param string $type
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getRandomUnits(string $type): array
    {
        // We insert units how keys.
        $resp = [];
        do {
            $unit = $this->getRandomUnit($type);
            $resp[$unit] = true;
        } while (count($resp) < 2);

        // The response require units how values, not keys.
        return array_keys($resp);
    }

    /**
     * Get a random measurement unit.
     *
     * @param string $type
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function getRandomUnit(string $type): string
    {
        /** @var ConfigurationRepository $unitRepo */
        $unitRepo = $this->entityManager->getRepository(Configuration::class);
        $units = $unitRepo->find('system.units.'.$type)?->getValue();
        $unitKeys = array_keys($units);
        $unitsLen = count($units);

        return $unitKeys[random_int(0, $unitsLen - 1)];
    }

    /**
     * Remove adm. divisions from database.
     *
     * @param Country|AdministrativeDivision $root
     *
     * @return int Entities deleted.
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function clearDivisions(Country|AdministrativeDivision $root): int
    {
        /** @var QueryBuilder $q */
        $q = $this->entityManager->getManager()->createQueryBuilder()
            ->delete(AdministrativeDivision::class, 'u')
            ->where('u.country = :country_code')
            ->setParameter('country_code', $root->getId());
        $query = $q->getQuery();

        return $query->getResult();
    }

    /**
     * Generate adm. divisions tree in database.
     *
     * @param Country                     $country
     * @param array                       $tree
     * @param AdministrativeDivision|null $parent
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function generateAdministrativeDivisions(Country $country, array $tree, AdministrativeDivision $parent = null): void
    {
        /** @var AdministrativeDivisionRepository $divisionsRepo */
        $divisionsRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $adChild = null;
        if ($tree['deep'] > 0) {
            $adChild = new AdministrativeDivision();
            $adChild->setName($tree['label']);
            $adChild->setCountry($country);
            $adChild->setParent($parent);
            $divisionsRepo->save($adChild);
        }
        foreach ($tree['children'] as $child) {
            $this->generateAdministrativeDivisions($country, $child, $adChild);
        }
    }

    /**
     * Build dummy administrative divisions.
     *
     * @param array|Country $country
     * @param string[]      $nodes       New divisions labels.
     * @param string        $label
     * @param int           $currentDeep
     * @param int           $maxDeep
     *
     * @return ?array
     */
    protected function buildDummyTree(array|Country $country, array $nodes, string $label = '', int $currentDeep = 0, int $maxDeep = 0): ?array
    {
        if ($country instanceof Country) {
            $maxDeep = $country->getDeep();
            $resp = [
                'label' => $country->getName(),
                'children' => [],
                'deep' => $currentDeep,
            ];
            foreach ($nodes as $node) {
                $item = $this->buildDummyTree($resp, $nodes, $node, 1, $maxDeep);
                if ($item) {
                    $resp['children'][] = $item;
                }
            }
        } else {
            // It's array.
            if ($currentDeep > $maxDeep) {
                return null;
            }
            $resp = [
                'label' => sprintf('%s, Level %s', $label, $currentDeep),
                'children' => [],
                'deep' => $currentDeep,
            ];
            foreach ($nodes as $node) {
                $item = $this->buildDummyTree($resp, $nodes, $node, $currentDeep+1, $maxDeep);
                if ($item) {
                    $resp['children'][] = $item;
                }
            }
        }

        return $resp;
    }

    /**
     * Render memory tree from array.
     *
     * @param SymfonyStyle $io
     * @param array        $root  ['label' => '', 'children' => [...]]
     * @param int          $limit Children limit.
     *
     * @throws \Exception
     */
    protected function displayArrayTree(SymfonyStyle $io, array $root, int $limit = 10): void
    {
        $tree = $this->treeArrayRecursive($root, $limit);
        $exporter = new Ascii();
        $text = explode(PHP_EOL, $exporter->export($tree));
        $io->title('Administrative divisions generated:');
        foreach ($text as $line) {
            $io->text($line);
        }
    }

    /**
     * Render memory tree from administrative divisions instances.
     *
     * @param SymfonyStyle                   $io
     * @param Country|AdministrativeDivision $root
     * @param int                            $limit
     *
     * @throws \Exception
     */
    protected function displayEntitiesTree(SymfonyStyle $io, Country|AdministrativeDivision $root, int $limit = 10): void
    {
        $tree = $this->treeRecursive($root, $limit);
        $exporter = new Ascii();
        $text = explode(PHP_EOL, $exporter->export($tree));
        foreach ($text as $line) {
            $io->text($line);
        }
    }

    /**
     * Build memory tree.
     *
     * @param array $root  ['label' => '', 'children' => [...]]
     * @param int   $limit
     *
     * @return ValueNode
     *
     * @throws \Exception
     */
    protected function treeArrayRecursive(array $root, int $limit = 10): ValueNode
    {
        $resp = new ValueNode($root['label']);
        $cnt = 0;
        $max = count($root['children']);
        foreach ($root['children'] as $administrativeDivision) {
            $resp->add($this->treeArrayRecursive($administrativeDivision, $limit));
            $cnt++;
            if ($cnt >= $limit && $max >= $limit) {
                $resp->add(new ValueNode('...'));
                break;
            }
        }

        return $resp;
    }

    /**
     * Build memory tree.
     *
     * @param Country|AdministrativeDivision $root
     * @param int                            $limit
     *
     * @return ValueNode
     *
     * @throws \Exception
     */
    protected function treeRecursive(Country|AdministrativeDivision $root, int $limit = 10): ValueNode
    {
        $resp = new ValueNode($root->getName());
        $cnt = 0;
        $max = $root->getAdministrativeDivisions()->count();
        foreach ($root->getAdministrativeDivisions() as $administrativeDivision) {
            $resp->add($this->treeRecursive($administrativeDivision, $limit));
            $cnt++;
            if ($cnt >= $limit && $max >= $limit) {
                $resp->add(new ValueNode('...'));
                break;
            }
        }

        return $resp;
    }

    /**
     * Validate and fix country list.
     *
     * @param SymfonyStyle $io           Input/Output handler.
     * @param array        $countryCodes Countries to validate.
     * @param bool         $allowNone
     *
     * @return int Command response code.
     */
    protected function validateCountryCodes(SymfonyStyle $io, array &$countryCodes, bool $allowNone = false): int
    {
        $countryRepository = $this->entityManager->getRepository(Country::class);
        if (in_array('all', $countryCodes)) {
            $haveNone = in_array('none', $countryCodes);
            $countries = $countryRepository->findAll();
            $countryCodes = [];
            /** @var Country $country */
            foreach ($countries as $country) {
                $countryCodes[$country->getId()] = $country->getId();
            }
            if ($allowNone && $haveNone) {
                $countryCodes['none'] = 'none';
            }
        }

        // Validate country codes.
        $countryList = [];
        $cleanCountryCodes = [];
        foreach ($countryCodes as $countryCode) {
            if ('none' !== $countryCode) {
                $country = $countryRepository->find($countryCode);
                if (!$country) {
                    $io->error(sprintf('Not valid country: "%s"', $countryCode));

                    return Command::INVALID;
                }
                $countryList[] = [$countryCode => $country->getName()];
                $cleanCountryCodes[] = $countryCode;
            } elseif ($allowNone) {
                $countryList[] = ['none' => 'Without country'];
                $cleanCountryCodes[] = 'none';
            }
        }
        $countryCodes = $cleanCountryCodes;
        if (count($countryCodes) === 0) {
            $io->error('No countries selected.');

            return Command::FAILURE;
        }

        $io->title('Clean instances from:');
        call_user_func_array([$io, 'definitionList'], $countryList);

        return Command::SUCCESS;
    }

    /**
     * Get generable entities classes.
     *
     * @return string[]
     */
    protected function getEntityTypes(): array
    {
        return [
            AdministrativeDivision::class,
            Currency::class,
            Ethnicity::class,
            FunderIntervention::class,
            InstitutionIntervention::class,
            Material::class,
            OfficialLanguage::class,
            ProgramIntervention::class,
            TechnicalAssistanceProvider::class,
            TypeIntervention::class,
            PumpType::class,
            DefaultDiameter::class,
            SpecialComponent::class,
        ];
    }
}
