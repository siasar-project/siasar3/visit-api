<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Annotations\IsCountryParametric;
use App\Entity\Country;
use App\Repository\CountryRepository;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

/**
 * Parametrics listing.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineDefaultParametrics extends AbstractDevCommandBase
{
    use GetContainerTrait;

    protected static $defaultName = 'doctrine:parametric:default';
    protected static $defaultDescription = 'Validate and create default parametric by country.';

    protected ManagerRegistry $entityManager;
    protected Reader $annotationReader;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param Reader          $annotationReader
     * @param string|null     $name             The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, Reader $annotationReader, string $name = null)
    {
        parent::__construct($entityManager, $name);
        $this->annotationReader = $annotationReader;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Create default entities in this country code.'
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $countryCode = $input->getArgument('country');

        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->entityManager->getRepository(Country::class);
        $country = $countryRepository->find($countryCode);

        if (!$country) {
            $io->error(sprintf('Not valid country: "%s"', $countryCode));

            return Command::INVALID;
        }

        // Find default entities in /ParametricFixtures, validate and create if they do not exist.
        /** @var ClassMetadata[] $metas */
        $metas = $this->entityManager->getManager()->getMetadataFactory()->getAllMetadata();
        $counter = 0;
        $alreadyExisting = 0;
        foreach ($metas as $meta) {
            $annotations = $this->annotationReader->getClassAnnotations(new ReflectionClass($meta->getName()));

            foreach ($annotations as $note) {
                if ($note instanceof IsCountryParametric) {
                    $parametricName = $this->getClassNameOnly($meta->getName());
                    $fixturesDir = self::getKernelInstance()->getProjectDir().'/src/ParametricFixtures/';
                    if (is_file($fixturesDir.$parametricName.'.yml')) {
                        $fixtures = Yaml::parseFile($fixturesDir.$parametricName.'.yml');
                        $class = $meta->getName();
                        $io->title($class);
                        $repo = $this->entityManager->getRepository($class);
                        foreach ($fixtures as $fixture) {
                            // Validate existence.
                            $criteria = [
                                'country' => $countryCode,
                            ];
                            foreach ($fixture as $propertyName => $propertyValue) {
                                $criteria[$propertyName] = $propertyValue;
                            }
                            $previous = $repo->findBy($criteria);
                            if (count($previous) === 0) {
                                // Create if not exist.
                                $instance = new $class();
                                $instance->setStarting(true);
                                $io->text('New:');
                                foreach ($fixture as $propertyName => $propertyValue) {
                                    $instance->{'set'.ucfirst($propertyName)}($propertyValue);
                                    $io->text(sprintf('%s => %s', $propertyName, $propertyValue));
                                }
                                $country->{'add'.$parametricName}($instance);
                                $counter++;
                            } else {
                                $alreadyExisting++;
                            }
                        }
                    }
                }
            }
        }
        $countryRepository->saveNow($country);

        $io->text(sprintf('%s parametric created.', $counter));
        $io->text(sprintf('%s already existing.', $alreadyExisting));

        return Command::SUCCESS;
    }

    /**
     * Get class name without namespace.
     *
     * @see https://coderwall.com/p/cpxxxw/php-get-class-name-without-namespace
     *
     * @param string $class
     *
     * @return string|null
     */
    protected function getClassNameOnly(string $class): ?string
    {
        $path = explode('\\', $class);

        return array_pop($path);
    }
}
