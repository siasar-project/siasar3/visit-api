<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\AdministrativeDivision;
use App\Entity\CommunityService;
use App\Entity\Configuration;
use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\DefaultDiameter;
use App\Entity\DisinfectingSubstance;
use App\Entity\DistributionMaterial;
use App\Entity\Ethnicity;
use App\Entity\FunctionsCarriedOutTap;
use App\Entity\FunctionsCarriedOutWsp;
use App\Entity\FunderIntervention;
use App\Entity\GeographicalScope;
use App\Entity\InstitutionIntervention;
use App\Entity\InterventionStatus;
use App\Entity\Material;
use App\Entity\OfficialLanguage;
use App\Entity\ProgramIntervention;
use App\Entity\PumpType;
use App\Entity\SpecialComponent;
use App\Entity\TechnicalAssistanceProvider;
use App\Entity\TreatmentTechnologyAeraOxida;
use App\Entity\TreatmentTechnologyCoagFloccu;
use App\Entity\TreatmentTechnologyDesalination;
use App\Entity\TreatmentTechnologyFiltration;
use App\Entity\TreatmentTechnologySedimentation;
use App\Entity\TypeHealthcareFacility;
use App\Entity\TypeIntervention;
use App\Entity\TypeTap;
use App\Entity\TypologyChlorinationInstallation;
use App\Entity\WaterQualityEntity;
use App\Entity\WaterQualityInterventionType;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\CountryRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use loophp\phptree\Exporter\Ascii;
use loophp\phptree\Node\ValueNode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineGenerateDummyCommand extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:generate:dummies';
    protected static $defaultDescription = 'Generate example Doctrine entities.';

    protected ManagerRegistry $entityManager;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param string|null     $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, string $name = null)
    {
        parent::__construct($entityManager, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption(
                'divisions',
                'd',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Generate administrative divisions with this labels, the new divisions will be created for each adm. division type level.',
                // , 'Division D1', 'Division E1', 'Division F1', 'Division G1'
                ['Division A', 'Division B', 'Division C', 'Division D', 'Division E', 'Division F', 'Division G', 'Division A1', 'Division B1' , 'Division C1']
            )
            ->addOption(
                'clear',
                'c',
                InputOption::VALUE_NONE,
                'Remove current doctrine entities in the platform'
            )
            ->addOption(
                'generate',
                'g',
                InputOption::VALUE_NEGATABLE,
                'Generate new entities',
                true
            )
            ->addArgument(
                'country',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Generate entities to this country code.',
                ['all']
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (Command::FAILURE === parent::execute($input, $output)) {
            return Command::FAILURE;
        }
        $io = new SymfonyStyle($input, $output);
        $divisionLabels = $input->getOption('divisions');
        $countryCodes = $input->getArgument('country');
        $clearEntities = boolval($input->getOption('clear'));
        $generate = boolval($input->getOption('generate'));

        if (Command::SUCCESS !== ($returnCode = $this->validateCountryCodes($io, $countryCodes))) {
            return $returnCode;
        }

        $entityTypes = $this->getEntityTypes();
        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->entityManager->getRepository(Country::class);
        // Clear entities.
        if ($clearEntities) {
            $this->entityManager->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 0;');
            foreach ($countryCodes as $countryCode) {
                // Find country.
                /** @var Country $country */
                $country = $countryRepository->find($countryCode);
                $io->info(sprintf('Deleting %s data...', $country->getName()));
                foreach ($entityTypes as $entityType) {
                    $deleted = 0;
                    switch ($entityType) {
                        case AdministrativeDivision::class:
                            $deleted = $this->clearDivisions($country);
                            break;
                        case Currency::class:
                            $deleted = $country->getCurrencies()->count();
                            $entities = $country->getCurrencies();
                            foreach ($entities as $entity) {
                                $country->removeCurrency($entity);
                            }
                            break;
                        case Ethnicity::class:
                            $deleted = $country->getEthnicities()->count();
                            $entities = $country->getEthnicities();
                            foreach ($entities as $entity) {
                                $country->removeEthnicity($entity);
                            }
                            break;
                        case FunderIntervention::class:
                            $deleted = $country->getFunderInterventions()->count();
                            $entities = $country->getFunderInterventions()->toArray();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case InstitutionIntervention::class:
                            $deleted = $country->getInstitutionInterventions()->count();
                            $entities = $country->getInstitutionInterventions();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case Material::class:
                            $deleted = $country->getMaterials()->count();
                            $entities = $country->getMaterials();
                            foreach ($entities as $entity) {
                                if ('other' !== strtolower(trim($entity->getType()))) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case OfficialLanguage::class:
                            $deleted = $country->getOfficialLanguages()->count();
                            $country->setMainOfficialLanguage(null);
                            $entities = $country->getOfficialLanguages();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case ProgramIntervention::class:
                            $deleted = $country->getProgramInterventions()->count();
                            $entities = $country->getProgramInterventions();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case TechnicalAssistanceProvider::class:
                            $deleted = $country->getTechnicalAssistanceProviders()->count();
                            $entities = $country->getTechnicalAssistanceProviders();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case TypeIntervention::class:
                            $deleted = $country->getTypeInterventions()->count();
                            $entities = $country->getTypeInterventions();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case PumpType::class:
                            $deleted = $country->getPumpTypes()->count();
                            $entities = $country->getPumpTypes();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case DefaultDiameter::class:
                            $deleted = $country->getDefaultDiameters()->count();
                            $entities = $country->getDefaultDiameters();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case SpecialComponent::class:
                            $deleted = $country->getSpecialComponents()->count();
                            $entities = $country->getSpecialComponents();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case TreatmentTechnologyAeraOxida::class:
                            $deleted = $country->getTreatmentTechnologyAeraOxidas()->count();
                            $entities = $country->getTreatmentTechnologyAeraOxidas();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case TreatmentTechnologyCoagFloccu::class:
                            $deleted = $country->getTreatmentTechnologyCoagFloccus()->count();
                            $entities = $country->getTreatmentTechnologyCoagFloccus();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case TreatmentTechnologyFiltration::class:
                            $deleted = $country->getTreatmentTechnologyFiltrations()->count();
                            $entities = $country->getTreatmentTechnologyFiltrations();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case TreatmentTechnologySedimentation::class:
                            $deleted = $country->getTreatmentTechnologySedimentations()->count();
                            $entities = $country->getTreatmentTechnologySedimentations();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case TreatmentTechnologyDesalination::class:
                            $deleted = $country->getTreatmentTechnologyDesalinations()->count();
                            $entities = $country->getTreatmentTechnologyDesalinations();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case DistributionMaterial::class:
                            $deleted = $country->getDistributionMaterials()->count();
                            $entities = $country->getDistributionMaterials();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case InterventionStatus::class:
                            $deleted = $country->getInterventionStatuses()->count();
                            $entities = $country->getInterventionStatuses();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case WaterQualityInterventionType::class:
                            $deleted = $country->getWaterQualityInterventionTypes()->count();
                            $entities = $country->getWaterQualityInterventionTypes();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case WaterQualityEntity::class:
                            $deleted = $country->getWaterQualityEntities()->count();
                            $entities = $country->getWaterQualityEntities();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case FunctionsCarriedOutWsp::class:
                            $deleted = $country->getFunctionsCarriedOutWsps()->count();
                            $entities = $country->getFunctionsCarriedOutWsps();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case FunctionsCarriedOutTap::class:
                            $deleted = $country->getFunctionsCarriedOutTaps()->count();
                            $entities = $country->getFunctionsCarriedOutTaps();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case DisinfectingSubstance::class:
                            $deleted = $country->getDisinfectingSubstances()->count();
                            $entities = $country->getDisinfectingSubstances();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case CommunityService::class:
                            $deleted = $country->getCommunityServices()->count();
                            $entities = $country->getCommunityServices();
                            foreach ($entities as $entity) {
                                $this->entityManager->getManager()->remove($entity);
                            }
                            break;
                        case GeographicalScope::class:
                            $deleted = $country->getGeographicalScopes()->count();
                            $entities = $country->getGeographicalScopes();
                            foreach ($entities as $entity) {
                                if ('1' !== $entity->getKeyIndex() || '99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case TypologyChlorinationInstallation::class:
                            $deleted = $country->getTypologyChlorinationInstallations()->count();
                            $entities = $country->getTypologyChlorinationInstallations();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case TypeTap::class:
                            $deleted = $country->getTypeTaps()->count();
                            $entities = $country->getTypeTaps();
                            $lockedKeys = [1, 4, 5];
                            foreach ($entities as $entity) {
                                if (!in_array($entity->getKeyIndex(), $lockedKeys)) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                        case TypeHealthcareFacility::class:
                            $deleted = $country->getTypeHealthcareFacilities()->count();
                            $entities = $country->getTypeHealthcareFacilities();
                            foreach ($entities as $entity) {
                                if ('99' !== $entity->getKeyIndex()) {
                                    $this->entityManager->getManager()->remove($entity);
                                }
                            }
                            break;
                    }
                    $this->entityManager->getManager()->flush();
                    $io->text($deleted.' '.$entityType.' deleted.');
                }
            }
            $this->entityManager->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 1;');
        }
        if (!$generate) {
            return Command::SUCCESS;
        }
        // Generate entities.
        foreach ($countryCodes as $countryCode) {
            // Find country.
            /** @var Country $country */
            $country = $countryRepository->find($countryCode);
            $io->info(sprintf('Generating %s data...', $country->getName()));
            // Generate entities.
            foreach ($entityTypes as $entityType) {
                // The entity manager require refresh itself,
                // and we need reload country to allow manager
                // to know it again.
                /** @var Country $country */
                $country = $countryRepository->find($countryCode);
                $generated = 0;
                switch ($entityType) {
                    case AdministrativeDivision::class:
                        // Generate administrative divisions tree.
                        $dummyDivisions = $this->buildDummyTree($country, $divisionLabels);
                        $this->generateAdministrativeDivisions($country, $dummyDivisions);
                        $this->entityManager->getManager()->flush();
                        $this->entityManager->getManager()->clear();
                        $io->text('Adm. divisions deep is '.$country->getDeep());
                        $this->displayEntitiesTree($io, $country, 1);
                        array_walk_recursive($dummyDivisions, function ($value, $key) use (&$generated) {
                            if ('label' === $key) {
                                $generated++;
                            }
                        });
                        // Discount country node.
                        $generated--;
                        break;
                    case Currency::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new Currency();
                            $entity->setName(sprintf('Currency %s [%s]', $i, $country->getId()));
                            $this->entityManager->getManager()->persist($entity);
                            $country->addCurrency($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case Ethnicity::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new Ethnicity();
                            $entity->setName('Ethnicity '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addEthnicity($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case FunderIntervention::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new FunderIntervention();
                            $entity->setName('Funder Intervention '.$i);
                            $entity->setAddress('Funder Address '.$i);
                            $entity->setContact('Funder Contact '.$i);
                            $entity->setPhone(uniqid('+', true));
                            $this->entityManager->getManager()->persist($entity);
                            $country->addFunderIntervention($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case InstitutionIntervention::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new InstitutionIntervention();
                            $entity->setName('Institution Intervention '.$i);
                            $entity->setAddress('Institution Address '.$i);
                            $entity->setContact('Institution Contact '.$i);
                            $entity->setPhone(uniqid('+', true));
                            $this->entityManager->getManager()->persist($entity);
                            $country->addInstitutionIntervention($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case Material::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new Material();
                            $entity->setType('Material '.$i);
                            $entity->setKeyIndex('Material '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addMaterial($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case OfficialLanguage::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new OfficialLanguage();
                            $entity->setLanguage('Language '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addOfficialLanguage($entity);
                            if (1 === $i) {
                                $country->setMainOfficialLanguage($entity);
                            }
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case ProgramIntervention::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new ProgramIntervention();
                            $entity->setName('Program Intervention '.$i);
                            $entity->setDescription('Program Intervention '.$i.' description.');
                            $this->entityManager->getManager()->persist($entity);
                            $country->addProgramIntervention($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TechnicalAssistanceProvider::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TechnicalAssistanceProvider();
                            $entity->setName('Technical Assistance Provider '.$i);
                            $entity->setDescription('Technical Assistance Provider '.$i.' description.');
                            $entity->setNationalId('TAP_'.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTechnicalAssistanceProvider($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TypeIntervention::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TypeIntervention();
                            $entity->setType('Type Intervention '.$i);
                            $entity->setDescription('Type Intervention '.$i.' description.');
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTypeIntervention($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case PumpType::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new PumpType();
                            $entity->setType('Pump Type '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addPumpType($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case DefaultDiameter::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new DefaultDiameter();
                            $entity->setUnit('milimetre');
                            $entity->setValue(random_int(1, 100));
                            $this->entityManager->getManager()->persist($entity);
                            $country->addDefaultDiameter($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case SpecialComponent::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new SpecialComponent();
                            $entity->setKeyIndex($i);
                            $entity->setName('Special Component '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addSpecialComponent($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TreatmentTechnologyAeraOxida::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TreatmentTechnologyAeraOxida();
                            $entity->setName('Treatment Tech. AeraOxida '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTreatmentTechnologyAeraOxida($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TreatmentTechnologyCoagFloccu::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TreatmentTechnologyCoagFloccu();
                            $entity->setName('Treatment Tech. CoagFloccu '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTreatmentTechnologyCoagFloccu($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TreatmentTechnologyFiltration::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TreatmentTechnologyFiltration();
                            $entity->setName('Treatment Tech. Filtration '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTreatmentTechnologyFiltration($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TreatmentTechnologySedimentation::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TreatmentTechnologySedimentation();
                            $entity->setName('Treatment Tech. Sedimentation '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTreatmentTechnologySedimentation($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TreatmentTechnologyDesalination::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TreatmentTechnologyDesalination();
                            $entity->setName('Treatment Tech. Desalination '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTreatmentTechnologyDesalination($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case DistributionMaterial::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new DistributionMaterial();
                            $entity->setKeyIndex($i);
                            $entity->setType('Distribution Material '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addDistributionMaterial($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case InterventionStatus::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new InterventionStatus();
                            $entity->setKeyIndex($i);
                            $entity->setName('Intervention Status '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addInterventionStatus($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case WaterQualityInterventionType::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new WaterQualityInterventionType();
                            $entity->setKeyIndex($i);
                            $entity->setName('Water Quality Intervention Type '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addWaterQualityInterventionType($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case WaterQualityEntity::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new WaterQualityEntity();
                            $entity->setName('Water Quality Entity '.$i);
                            $entity->setAddress('Water Quality Entity Address '.$i);
                            $entity->setContact('Water Quality Entity Contact '.$i);
                            $entity->setPhone(uniqid('+', true));
                            $this->entityManager->getManager()->persist($entity);
                            $country->addWaterQualityEntity($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case FunctionsCarriedOutWsp::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new FunctionsCarriedOutWsp();
                            $entity->setKeyIndex($i);
                            $entity->setType('Functions Carried Out Wsp '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addFunctionsCarriedOutWsp($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case FunctionsCarriedOutTap::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new FunctionsCarriedOutTap();
                            $entity->setKeyIndex($i);
                            $entity->setType('Functions Carried Out Tap '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addFunctionsCarriedOutTap($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case DisinfectingSubstance::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new DisinfectingSubstance();
                            $entity->setKeyIndex($i);
                            $entity->setName('Disinfecting Substance '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addDisinfectingSubstance($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case CommunityService::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new CommunityService();
                            $entity->setType('Community Service '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addCommunityService($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case GeographicalScope::class:
                        $max = random_int(4, 10);
                        for ($i = 2; $i <= $max; ++$i) {
                            $entity = new GeographicalScope();
                            $entity->setKeyIndex($i);
                            $entity->setName('Geographical Scope '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addGeographicalScope($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TypologyChlorinationInstallation::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TypologyChlorinationInstallation();
                            $entity->setKeyIndex($i);
                            $entity->setName('Typology Chlorination '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTypologyChlorinationInstallation($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TypeTap::class:
                        $max = random_int(4, 10);
                        for ($i = 6; $i <= $max; ++$i) {
                            $entity = new TypeTap();
                            $entity->setKeyIndex($i);
                            $entity->setName('Type Tap '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTypeTap($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                    case TypeHealthcareFacility::class:
                        $max = random_int(4, 10);
                        for ($i = 1; $i <= $max; ++$i) {
                            $entity = new TypeHealthcareFacility();
                            $entity->setKeyIndex($i);
                            $entity->setName('Type Health care Facility '.$i);
                            $this->entityManager->getManager()->persist($entity);
                            $country->addTypeHealthcareFacility($entity);
                            ++$generated;
                        }
                        $this->entityManager->getManager()->flush();
                        break;
                }
                $io->text($generated.' '.$entityType.' created.');
            }
            // Update Country preferred measurement units.
            // Concentration.
            $country->setMainUnitConcentration($this->getRandomUnit('concentration'));
            $country->setUnitConcentrations($this->getRandomUnits('concentration'));
            // Flow.
            $country->setMainUnitFlow($this->getRandomUnit('flow'));
            $country->setUnitFlows($this->getRandomUnits('flow'));
            // Length.
            $country->setMainUnitLength($this->getRandomUnit('length'));
            $country->setUnitLengths($this->getRandomUnits('length'));
            // Volume.
            $country->setMainUnitVolume($this->getRandomUnit('volume'));
            $country->setUnitVolumes($this->getRandomUnits('volume'));
            // Persist.
            $countryRepository->save($country);
            $io->text(sprintf('Main Concentration Unit: %s', $country->getMainUnitConcentration()));
            $io->text(sprintf('Main Flow Unit         : %s', $country->getMainUnitFlow()));
            $io->text(sprintf('Main Length Unit       : %s', $country->getMainUnitLength()));
            $io->text(sprintf('Main Volume Unit       : %s', $country->getMainUnitVolume()));
            //
            $io->text(sprintf('Main Concentration Units: %s', $this->formatUnitsList($country->getUnitConcentrations())));
            $io->text(sprintf('Main Flow Units         : %s', $this->formatUnitsList($country->getUnitFlows())));
            $io->text(sprintf('Main Length Units       : %s', $this->formatUnitsList($country->getUnitLengths())));
            $io->text(sprintf('Main Volume Units       : %s', $this->formatUnitsList($country->getUnitVolumes())));
        }
        $this->entityManager->getManager()->flush();
        $this->entityManager->getManager()->clear();

        return Command::SUCCESS;
    }

    /**
     * Format unit list.
     *
     * @param array $list
     *
     * @return string
     */
    protected function formatUnitsList(array $list): string
    {
        return implode(', ', array_keys($list));
    }

    /**
     * Get two random units.
     *
     * @param string $type
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getRandomUnits(string $type): array
    {
        // We insert units how keys.
        $resp = [];
        do {
            $unit = $this->getRandomUnit($type);
            $resp[$unit] = true;
        } while (count($resp) < 2);

        // The response require units how values, not keys.
        return array_keys($resp);
    }

    /**
     * Get a random measurement unit.
     *
     * @param string $type
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function getRandomUnit(string $type): string
    {
        /** @var ConfigurationRepository $unitRepo */
        $unitRepo = $this->entityManager->getRepository(Configuration::class);
        $units = $unitRepo->find('system.units.'.$type)?->getValue();
        $unitKeys = array_keys($units);
        $unitsLen = count($units);

        return $unitKeys[random_int(0, $unitsLen - 1)];
    }

    /**
     * Remove adm. divisions from database.
     *
     * @param Country|AdministrativeDivision $root
     *
     * @return int Entities deleted.
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function clearDivisions(Country|AdministrativeDivision $root): int
    {
        /** @var QueryBuilder $q */
        $q = $this->entityManager->getManager()->createQueryBuilder()
            ->delete(AdministrativeDivision::class, 'u')
            ->where('u.country = :country_code')
            ->setParameter('country_code', $root->getId());
        $query = $q->getQuery();

        return $query->getResult();
    }

    /**
     * Generate adm. divisions tree in database.
     *
     * @param Country                     $country
     * @param array                       $tree
     * @param AdministrativeDivision|null $parent
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function generateAdministrativeDivisions(Country $country, array $tree, AdministrativeDivision $parent = null): void
    {
        /** @var AdministrativeDivisionRepository $divisionsRepo */
        $divisionsRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $adChild = null;
        if ($tree['deep'] > 0) {
            $adChild = new AdministrativeDivision();
            $adChild->setName($country->getName().' - '.$tree['label']);
            $adChild->setCountry($country);
            $adChild->setParent($parent);
            $divisionsRepo->save($adChild);
        }
        foreach ($tree['children'] as $child) {
            $this->generateAdministrativeDivisions($country, $child, $adChild);
        }
    }

    /**
     * Build dummy administrative divisions.
     *
     * @param array|Country $country
     * @param string[]      $nodes       New divisions labels.
     * @param string        $label
     * @param int           $currentDeep
     * @param int           $maxDeep
     *
     * @return ?array
     */
    protected function buildDummyTree(array|Country $country, array $nodes, string $label = '', int $currentDeep = 0, int $maxDeep = 0): ?array
    {
        if ($country instanceof Country) {
            $maxDeep = $country->getDeep();
            $resp = [
                'label' => $country->getName(),
                'children' => [],
                'deep' => $currentDeep,
            ];
            foreach ($nodes as $node) {
                $item = $this->buildDummyTree($resp, $nodes, $node, 1, $maxDeep);
                if ($item) {
                    $resp['children'][] = $item;
                }
            }
        } else {
            // It's array.
            if ($currentDeep > $maxDeep) {
                return null;
            }
            $resp = [
                'label' => sprintf('%s, Level %s', $label, $currentDeep),
                'children' => [],
                'deep' => $currentDeep,
            ];
            foreach ($nodes as $node) {
                $item = $this->buildDummyTree($resp, $nodes, $node, $currentDeep+1, $maxDeep);
                if ($item) {
                    $resp['children'][] = $item;
                }
            }
        }

        return $resp;
    }

    /**
     * Render memory tree from array.
     *
     * @param SymfonyStyle $io
     * @param array        $root  ['label' => '', 'children' => [...]]
     * @param int          $limit Children limit.
     *
     * @throws \Exception
     */
    protected function displayArrayTree(SymfonyStyle $io, array $root, int $limit = 10): void
    {
        $tree = $this->treeArrayRecursive($root, $limit);
        $exporter = new Ascii();
        $text = explode(PHP_EOL, $exporter->export($tree));
        $io->title('Administrative divisions generated:');
        foreach ($text as $line) {
            $io->text($line);
        }
    }

    /**
     * Render memory tree from administrative divisions instances.
     *
     * @param SymfonyStyle                   $io
     * @param Country|AdministrativeDivision $root
     * @param int                            $limit
     *
     * @throws \Exception
     */
    protected function displayEntitiesTree(SymfonyStyle $io, Country|AdministrativeDivision $root, int $limit = 10): void
    {
        $tree = $this->treeRecursive($root, $limit);
        $exporter = new Ascii();
        $text = explode(PHP_EOL, $exporter->export($tree));
        foreach ($text as $line) {
            $io->text($line);
        }
    }

    /**
     * Build memory tree.
     *
     * @param array $root  ['label' => '', 'children' => [...]]
     * @param int   $limit
     *
     * @return ValueNode
     *
     * @throws \Exception
     */
    protected function treeArrayRecursive(array $root, int $limit = 10): ValueNode
    {
        $resp = new ValueNode($root['label']);
        $cnt = 0;
        $max = count($root['children']);
        foreach ($root['children'] as $administrativeDivision) {
            $resp->add($this->treeArrayRecursive($administrativeDivision, $limit));
            $cnt++;
            if ($cnt >= $limit && $max >= $limit) {
                $resp->add(new ValueNode('...'));
                break;
            }
        }

        return $resp;
    }

    /**
     * Build memory tree.
     *
     * @param Country|AdministrativeDivision $root
     * @param int                            $limit
     *
     * @return ValueNode
     *
     * @throws \Exception
     */
    protected function treeRecursive(Country|AdministrativeDivision $root, int $limit = 10): ValueNode
    {
        $resp = new ValueNode($root->getName());
        $cnt = 0;
        $max = $root->getAdministrativeDivisions()->count();
        foreach ($root->getAdministrativeDivisions() as $administrativeDivision) {
            $resp->add($this->treeRecursive($administrativeDivision, $limit));
            $cnt++;
            if ($cnt >= $limit && $max >= $limit) {
                $resp->add(new ValueNode('...'));
                break;
            }
        }

        return $resp;
    }

    /**
     * Validate and fix country list.
     *
     * @param SymfonyStyle $io           Input/Output handler.
     * @param array        $countryCodes Countries to validate.
     * @param bool         $allowNone
     *
     * @return int Command response code.
     */
    protected function validateCountryCodes(SymfonyStyle $io, array &$countryCodes, bool $allowNone = false): int
    {
        $countryRepository = $this->entityManager->getRepository(Country::class);
        if (in_array('all', $countryCodes)) {
            $haveNone = in_array('none', $countryCodes);
            $countries = $countryRepository->findAll();
            $countryCodes = [];
            /** @var Country $country */
            foreach ($countries as $country) {
                $countryCodes[$country->getId()] = $country->getId();
            }
            if ($allowNone && $haveNone) {
                $countryCodes['none'] = 'none';
            }
        }

        // Validate country codes.
        $countryList = [];
        $cleanCountryCodes = [];
        foreach ($countryCodes as $countryCode) {
            if ('none' !== $countryCode) {
                $country = $countryRepository->find($countryCode);
                if (!$country) {
                    $io->error(sprintf('Not valid country: "%s"', $countryCode));

                    return Command::INVALID;
                }
                $countryList[] = [$countryCode => $country->getName()];
                $cleanCountryCodes[] = $countryCode;
            } elseif ($allowNone) {
                $countryList[] = ['none' => 'Without country'];
                $cleanCountryCodes[] = 'none';
            }
        }
        $countryCodes = $cleanCountryCodes;
        if (count($countryCodes) === 0) {
            $io->error('No countries selected.');

            return Command::FAILURE;
        }

        $io->title('Generate instances to:');
        call_user_func_array([$io, 'definitionList'], $countryList);

        return Command::SUCCESS;
    }

    /**
     * Get generable entities classes.
     *
     * @return string[]
     */
    protected function getEntityTypes(): array
    {
        return [
            AdministrativeDivision::class,
            Currency::class,
            Ethnicity::class,
            FunderIntervention::class,
            InstitutionIntervention::class,
            Material::class,
            OfficialLanguage::class,
            ProgramIntervention::class,
            TechnicalAssistanceProvider::class,
            TypeIntervention::class,
            PumpType::class,
            DefaultDiameter::class,
            SpecialComponent::class,
            TreatmentTechnologyAeraOxida::class,
            TreatmentTechnologyCoagFloccu::class,
            TreatmentTechnologyFiltration::class,
            TreatmentTechnologySedimentation::class,
            TreatmentTechnologyDesalination::class,
            DistributionMaterial::class,
            InterventionStatus::class,
            WaterQualityInterventionType::class,
            WaterQualityEntity::class,
            FunctionsCarriedOutWsp::class,
            FunctionsCarriedOutTap::class,
            DisinfectingSubstance::class,
            CommunityService::class,
            GeographicalScope::class,
            TypologyChlorinationInstallation::class,
            TypeTap::class,
            TypeHealthcareFacility::class,
        ];
    }
}
