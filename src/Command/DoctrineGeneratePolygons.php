<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Country;
use App\Kernel;
use App\Repository\CountryRepository;
use App\Repository\FileRepository;
use App\Service\FileSystem;
use App\Tools\Json;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Uid\Ulid;

/**
 * Display file information.
 *
 * @category Tools
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineGeneratePolygons extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:generate:polygons';
    protected static $defaultDescription = 'Generate polygons to countries.';

    protected ManagerRegistry $entityManager;
    protected Connection $connection;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param Connection      $connection
     * @param string|null     $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, Connection $connection, string $name = null)
    {
        $this->connection = $connection;
        parent::__construct($entityManager, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Generate polygon to this country code.'
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Alert user that this command it used to develop.
        $helper = $this->getHelper('question');
        $io->warning("This command is used for development only and requires extra database tables.");
        $question = new ConfirmationQuestion(
            'Continue? [y/N]',
            false
        );
        if (!$helper->ask($input, $output, $question)) {
            return Command::SUCCESS;
        }
        $output->writeln('');

        // Execute.
        /** @var CountryRepository $countryRepository */
        $countryRepository = $this->entityManager->getRepository(Country::class);
        $country = $countryRepository->find($input->getArgument('country'));

        /** @var Kernel $kernel */
        $kernel = static::getKernelInstance();
        $importsFolder = $kernel->getProjectDir().'/imports/geo';

        $path = $importsFolder.'/'.strtolower($country->getId()).'.geojson';

        if (!is_file($path)) {
            $io->error(sprintf("Source file '%s' not found.", $path));

            return Command::INVALID;
        }

        if (file_exists($path)) {
            $io->info(sprintf('Getting polygon to %s (%s)...', $country->getName(), $country->getCode()));
            $file = file_get_contents($path);
            $content = Json::decode($file);

            $io->progressStart(count($content['features']));
            for ($i = 0; $i < count($content['features']); $i++) {
                $content['features'][$i]['geometry'] = json_encode($content['features'][$i]['geometry']);
            }
            for ($i = 1; $i < count($content['features']); $i++) {
                $sql = "SELECT ST_AsGeoJSON(ST_UNION(ST_GeomFromGeoJSON('{$content['features'][0]['geometry']}'), ST_GeomFromGeoJSON('{$content['features'][$i]['geometry']}')));";
                $query = $this->connection->prepare($sql);
                $query->execute();
                $resultPolygon = current($query->fetch());
                $content['features'][0]['geometry'] = $resultPolygon;
                unset($resultPolygon);
                unset($query);
                unset($sql);

                $content['features'][$i] = null;

                $io->progressAdvance();
            }
            $io->progressFinish();
            $outputFile = $importsFolder.'/'.strtoupper($country->getId()).'_country.geojson';

            $content['features'][0]['geometry'] = json_decode($content['features'][0]['geometry'], true);
            unset($content['features'][0]['properties']['id']);
            unset($content['features'][0]['properties']['adm_level']);
            unset($content['features'][0]['properties']['score']);
            unset($content['features'][0]['properties']['wsp']);
            $content['features'][0]['properties']['name'] = $country->getName();
            file_put_contents($outputFile, json_encode($content['features'][0]));
            $io->info('Output generated: '.$outputFile);
        }

        return command::SUCCESS;
    }

    /**
     * Execute query
     *
     * @param string $sql
     */
    protected function executeDQL(string $sql)
    {
        $query = $this->connection->prepare($sql);
        $query->execute();
    }
}
