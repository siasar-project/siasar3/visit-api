<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWrite;
use App\Repository\ConfigurationRepository;
use App\Tools\TemplateGenerator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineGenerateUploadTplsCommand extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:generate:upload:templates';
    protected static $defaultDescription = 'Generate parametric upload template files.';

    protected ManagerRegistry $entityManager;
    protected ConfigurationRepository $config;
    protected TemplateGenerator $templates;

    /**
     * Command constructor.
     *
     * @param TemplateGenerator       $templates
     * @param ConfigurationRepository $config
     * @param ManagerRegistry         $entityManager
     * @param string|null             $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(TemplateGenerator $templates, ConfigurationRepository $config, ManagerRegistry $entityManager, string $name = null)
    {
        parent::__construct($entityManager, $name);
        $this->config = $config;
        $this->templates = $templates;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (Command::FAILURE === parent::execute($input, $output)) {
            return Command::FAILURE;
        }
        $io = new SymfonyStyle($input, $output);

        $cfg = [];
        $parametrics = $this->templates->getParamtricEntities();
        foreach ($parametrics as $parametric) {
            // Init settings to this entity.
            $cfg[$parametric->getName()] = [
                'label' => $this->templates->getParametricComment($parametric->getName()),
                'file' => sprintf('assets/upload_templates/%s_template.csv', TemplateGenerator::getClassnameOnly($parametric->getName())),
            ];
            // Prepare file folder.
            @mkdir('assets/upload_templates', 0775, true);
            $file = fopen($cfg[$parametric->getName()]['file'], 'wb');
            // Process fields/properties.
            $rows = [];
            $importables = $this->templates->getImportableFields($parametric);
            $csvHeader = [];
            foreach ($importables as $fieldName => $type) {
                $csvHeader[] = '"'.$fieldName.'"';
                $rows[] = [sprintf("%s: %s", $fieldName, $type)];
            }
            $io->table([TemplateGenerator::getClassnameOnly($parametric->getName())], $rows);
            fwrite($file, implode(',', $csvHeader)."\n");
            fclose($file);
        }

        /** @var ConfigurationWrite $templatesConfig */
        $templatesConfig = $this->config->findEditable('system.upload.templates');
        if (!$templatesConfig) {
            $templatesConfig = $this->config->create('system.upload.templates', $cfg);
        } else {
            $templatesConfig->setValue($cfg);
        }
        $templatesConfig->saveNow();

        return Command::SUCCESS;
    }
}
