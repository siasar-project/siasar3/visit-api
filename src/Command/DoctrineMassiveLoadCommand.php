<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Country;
use App\Repository\ConfigurationRepository;
use App\Service\FileSystem;
use App\Tools\ParametricImporter;
use App\Tools\TemplateGenerator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Massive parametric import.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineMassiveLoadCommand extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:import:parametric';
    protected static $defaultDescription = 'Massive parametric import.';

    protected ManagerRegistry $entityManager;
    protected ConfigurationRepository $config;
    protected TemplateGenerator $templates;
    protected FileSystem $fileSystem;

    /**
     * Command constructor.
     *
     * @param FileSystem              $fileSystem
     * @param TemplateGenerator       $templates
     * @param ConfigurationRepository $config
     * @param ManagerRegistry         $entityManager
     * @param string|null             $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(FileSystem $fileSystem, TemplateGenerator $templates, ConfigurationRepository $config, ManagerRegistry $entityManager, string $name = null)
    {
        parent::__construct($entityManager, $name);
        $this->config = $config;
        $this->templates = $templates;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addOption('clear', 'c', InputOption::VALUE_NEGATABLE, 'Clear old entities.', false)
            ->addOption('modify', 'm', InputOption::VALUE_NEGATABLE, 'Do changes to the database.', true)
            ->addOption('validate', 'f', InputOption::VALUE_NEGATABLE, 'Validate filename.', true)
            ->addArgument('country', InputArgument::REQUIRED, "Country code where to import.")
            ->addArgument('class', InputArgument::REQUIRED, "Parametric className without namespace.")
            ->addArgument('file', InputArgument::REQUIRED, "File path to import. The first line must be header.");
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $countryCode = $input->getArgument('country');
        $className = $input->getArgument('class');
        $file = $input->getArgument('file');
        $clearOld = $input->getOption('clear');
        $modify = $input->getOption('modify');
        $validate = $input->getOption('validate');

        $importer = new ParametricImporter($countryCode, $className, $file, $clearOld);
        $importer->setErrorCallback([$io, 'error']);
        $importer->setInfoCallback([$io, 'info']);
        $importer->setWarningCallback([$io, 'warning']);
        $importer->setTextCallback([$io, 'write']);

        if ($importer->import($validate, !$modify)) {
            return Command::SUCCESS;
        }

        return Command::FAILURE;
    }
}
