<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Annotations\IsCountryParametric;
use App\Repository\ConfigurationRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Parametrics listing.
 *
 * @category Tools
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class DoctrineParametricsList extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:parametric:list';
    protected static $defaultDescription = 'List all existing parametrics.';

    protected ManagerRegistry $entityManager;
    protected Reader $annotationReader;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param Reader          $annotationReader
     * @param string|null     $name             The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, Reader $annotationReader, string $name = null)
    {
        parent::__construct($entityManager, $name);
        $this->annotationReader = $annotationReader;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (Command::FAILURE === parent::execute($input, $output)) {
            return Command::FAILURE;
        }

        $io = new SymfonyStyle($input, $output);

        // Find doctrine entities.
        /** @var ClassMetadata[] $metas */
        $metas = $this->entityManager->getManager()->getMetadataFactory()->getAllMetadata();
        $parametrics = [];
        foreach ($metas as $meta) {
            $annotations = $this->annotationReader->getClassAnnotations(new ReflectionClass($meta->getName()));

            foreach ($annotations as $note) {
                if ($note instanceof IsCountryParametric) {
                    $parametrics[] = [$meta->getName()];
                }
            }
        }

        $io->table(['Parametric'], $parametrics);
        $io->text(sprintf('%s parametrics found.', count($parametrics)));

        return Command::SUCCESS;
    }
}
