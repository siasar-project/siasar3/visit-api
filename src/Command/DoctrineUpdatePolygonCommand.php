<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Country;
use App\Entity\CountryGeometry;
use App\Repository\CountryGeometryRepository;
use App\Repository\CountryRepository;
use App\Service\CountryGeometryService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @see https://lo-doctrine-spatial.readthedocs.io/en/latest/Repository.html
 */
class DoctrineUpdatePolygonCommand extends AbstractDevCommandBase
{
    protected static $defaultName = 'doctrine:polygon:update';
    protected static $defaultDescription = 'Update country polygon.';

    protected ManagerRegistry $entityManager;

    /**
     * Command constructor.
     *
     * @param ManagerRegistry        $entityManager
     * @param CountryGeometryService $countryGeometryService
     * @param string|null            $name                   The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, CountryGeometryService $countryGeometryService, string $name = null)
    {
        parent::__construct($entityManager, $name);
        $this->countryGeometryService = $countryGeometryService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(
                'country',
                InputArgument::REQUIRED,
                'Country code to update.'
            )
            ->addArgument(
                'file',
                InputArgument::OPTIONAL,
                'GeoJSON to load in country.'
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (Command::FAILURE === parent::execute($input, $output)) {
            return Command::FAILURE;
        }
        $io = new SymfonyStyle($input, $output);
        $countryCode = $input->getArgument('country');
        $file = $input->getArgument('file');

        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find($countryCode);
        if (!$country) {
            $io->error(sprintf('Country "%s" not found.', $countryCode));

            return Command::FAILURE;
        }

        if (!$file) {
            $geojson = null;
            $this->countryGeometryService->updateGeometry($country, $geojson);
            $io->info(sprintf('Country "%s" geometry erased', $country->getName()));

            return Command::SUCCESS;
        }

        if (!is_file($file)) {
            $io->error(sprintf('File "%s" not found.', $file));

            return Command::FAILURE;
        }

        /** @var CountryGeometryRepository $countryGeometryRepo */
        $countryGeometryRepo = $this->entityManager->getRepository(Country::class);
        if (!$this->countryGeometryService->exist($countryCode, true)) {
            $item = new CountryGeometry($countryCode);
            $countryGeometryRepo->saveNow($item);
        }

        $geojson = file_get_contents($file);
        $this->countryGeometryService->updateGeometry($country, $geojson);
        $io->info(sprintf('Country "%s" geometry updated', $country->getName()));

        return Command::SUCCESS;
    }
}
