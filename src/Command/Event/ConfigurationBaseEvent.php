<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command\Event;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Base configuration event.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationBaseEvent extends Event
{
    protected string $id;
    protected ?SymfonyStyle $io;
    protected mixed $old;
    protected mixed $new;
    protected ?InputInterface $input;
    protected ?OutputInterface $output;

    /**
     * ConfigurationBaseEvent constructor.
     *
     * @param string               $id     Configuration ID.
     * @param mixed                $old    Old configuration.
     * @param mixed                $new    New configuration.
     * @param SymfonyStyle|null    $io     Input/Output interface.
     * @param InputInterface|null  $input  Input interface.
     * @param OutputInterface|null $output Output interface.
     */
    public function __construct(string $id, mixed $old, mixed $new, SymfonyStyle $io = null, InputInterface $input = null, OutputInterface $output = null)
    {
        $this->id = $id;
        $this->io = $io;
        $this->old = $old;
        $this->new = $new;
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * Get configuration id.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set configuration id.
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * Get input/output console interface.
     *
     * @return SymfonyStyle|null
     */
    public function getIo(): ?SymfonyStyle
    {
        return $this->io;
    }

    /**
     * Set input/output console interface.
     *
     * @param SymfonyStyle|null $io
     */
    public function setIo(?SymfonyStyle $io): void
    {
        $this->io = $io;
    }

    /**
     * Get old configuration.
     *
     * @return mixed
     */
    public function getOld(): mixed
    {
        return $this->old;
    }

    /**
     * Set old configuration.
     *
     * @param mixed $old
     */
    public function setOld(mixed $old): void
    {
        $this->old = $old;
    }

    /**
     * Get new configuration.
     *
     * @return mixed
     */
    public function getNew(): mixed
    {
        return $this->new;
    }

    /**
     * Set old configuration.
     *
     * @param mixed $new
     */
    public function setNew(mixed $new): void
    {
        $this->new = $new;
    }

    /**
     * Get input interface.
     *
     * @return InputInterface|null
     */
    public function getInput(): ?InputInterface
    {
        return $this->input;
    }

    /**
     * Set input interface.
     *
     * @param InputInterface|null $input
     *
     * @return ConfigurationBaseEvent
     */
    public function setInput(?InputInterface $input): ConfigurationBaseEvent
    {
        $this->input = $input;

        return $this;
    }

    /**
     * Get output interface.
     *
     * @return OutputInterface|null
     */
    public function getOutput(): ?OutputInterface
    {
        return $this->output;
    }

    /**
     * Set output interface.
     *
     * @param OutputInterface|null $output
     *
     * @return ConfigurationBaseEvent
     */
    public function setOutput(?OutputInterface $output): ConfigurationBaseEvent
    {
        $this->output = $output;

        return $this;
    }
}
