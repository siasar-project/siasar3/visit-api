<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command\Event;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Post import configuration event.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationPostImportEvent extends Event
{
    public const NAME = 'configuration.post.import';

    protected ?SymfonyStyle $io;
    protected ?InputInterface $input;
    protected ?OutputInterface $output;

    /**
     * ConfigurationPostImportEvent constructor.
     *
     * @param SymfonyStyle|null    $io     Input/Output interface.
     * @param InputInterface|null  $input  Input interface.
     * @param OutputInterface|null $output Output interface.
     */
    public function __construct(SymfonyStyle $io = null, InputInterface $input = null, OutputInterface $output = null)
    {
        $this->io = $io;
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * @return SymfonyStyle|null
     */
    public function getIo(): ?SymfonyStyle
    {
        return $this->io;
    }

    /**
     * @return InputInterface|null
     */
    public function getInput(): ?InputInterface
    {
        return $this->input;
    }

    /**
     * @return OutputInterface|null
     */
    public function getOutput(): ?OutputInterface
    {
        return $this->output;
    }
}
