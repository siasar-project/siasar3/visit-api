<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\FileRepository;
use App\Service\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Remove file system orphan files.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FileRemoveOrphan extends Command
{
    protected static $defaultName = 'file:remove:orphan';
    protected static $defaultDescription = 'Remove file system orphan files.';
    protected ?FileRepository $fileRepository;
    protected ?FileSystem $fileSystem;

    /**
     * Command constructor.
     *
     * @param string|null     $name           The name of the command; passing null means it must be set in configure()
     * @param ?FileRepository $fileRepository File repository.
     * @param FileSystem|null $fileSystem     File system.
     */
    public function __construct(string $name = null, FileRepository $fileRepository = null, FileSystem $fileSystem = null)
    {
        parent::__construct($name);
        $this->fileRepository = $fileRepository;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addOption('yes', 'y', InputOption::VALUE_NONE, 'Execute without ask');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $data = $this->fileSystem->systemGetFilesAndFolders($this->fileSystem->getPublicFolder());

        $io->writeln('');

        $fileEntities = [];
        foreach ($data['files'] as $file) {
            $split = explode('._', $file);
            if (count($split) === 2) {
                $fileEntities[] = $file;
            }
        }

        $rows = [
            ['Files', count($data['files'])],
            ['Entities files', count($fileEntities)],
            ['Folders', count($data['folders'])],
        ];
        $io->table(['', 'Amount'], $rows);

        $io->note('Empty folders maybe remain.');

        // Ask user.
        if ($input->getOption('yes')) {
            $io->text('Files may be erase, we can\'t undo it.'.\PHP_EOL.'Continue? [y/N]');
            $io->info('Response by options: y');
        } else {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                'Files may be erase, we can\'t undo it.'.\PHP_EOL.'Continue? [y/N]',
                false
            );
            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
            $output->writeln('');
        }

        $io->writeln('');
        $io->writeln('Cleaning...');
        $io->progressStart(count($fileEntities));
        $orphans = [];
        foreach ($fileEntities as $file) {
            $split = explode('._', $file);
            $id = array_pop($split);
            $fileEntity = $this->fileRepository->find($id);
            if (!$fileEntity) {
                $orphans[] = $file;
                @unlink($file);
            }
            $io->progressAdvance();
        }
        $io->progressFinish();

        $io->success(sprintf('Cleared %s orphan files.', count($orphans)));

        return command::SUCCESS;
    }
}
