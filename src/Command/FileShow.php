<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\FileRepository;
use App\Service\FileSystem;
use Doctrine\DBAL\Types\ConversionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Uid\Ulid;

/**
 * Display file information.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FileShow extends Command
{
    protected static $defaultName = 'file:show';
    protected static $defaultDescription = 'Display file information.';
    protected ?FileRepository $fileRepository;
    protected ?FileSystem $fileSystem;

    /**
     * Command constructor.
     *
     * @param string|null     $name           The name of the command; passing null means it must be set in configure()
     * @param ?FileRepository $fileRepository File repository.
     * @param FileSystem|null $fileSystem     File system.
     */
    public function __construct(string $name = null, FileRepository $fileRepository = null, FileSystem $fileSystem = null)
    {
        parent::__construct($name);
        $this->fileRepository = $fileRepository;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('file_id', InputArgument::REQUIRED, 'The file ID to show.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $fileId = $input->getArgument('file_id');

        // Load file entity.
        try {
            $fileEntity = $this->fileRepository->find($fileId);
        } catch (ConversionException $e) {
            // Try load by binary ULID.
            $ulid = Ulid::fromString(hex2bin($fileId));
            $fileId = $ulid->toBase32();

            $fileEntity = $this->fileRepository->find($fileId);
        }
        if (!$fileEntity) {
            $io->error(sprintf('File %s not found.', $fileId));

            return command::FAILURE;
        }

        $rows = [
            ['ID', $fileEntity->getId()],
            ['Filename', $fileEntity->getFileName()],
            ['Path', $fileEntity->getPath()],
            ['Created', $fileEntity->getCreated()->format('Y-m-d H:i:s')],
            ['Country', ($fileEntity->getCountry() ? $fileEntity->getCountry() : 'World')],
        ];
        $io->table(['Entity property', 'Value'], $rows);

        $rows = [
            ['Is file', @is_file($fileEntity->getPath()) ? 'Yes' : 'No'],
            ['Is link', @is_link($fileEntity->getPath()) ? 'Yes' : 'No'],
            ['Is folder', @is_dir($fileEntity->getPath()) ? 'Yes' : 'No'],
            ['Is readable', @is_readable($fileEntity->getPath()) ? 'Yes' : 'No'],
            ['Last access time', date('Y-m-d H:i:s', @fileatime($fileEntity->getPath()))],
            ['Last change time', date('Y-m-d H:i:s', @filectime($fileEntity->getPath()))],
            ['Last modification time', date('Y-m-d H:i:s', @filemtime($fileEntity->getPath()))],
            ['Group ID', @filegroup($fileEntity->getPath())],
            ['Inode number', @fileinode($fileEntity->getPath())],
            ['User ID (owner)', @fileowner($fileEntity->getPath())],
            ['File\'s permissions', decoct(@fileperms($fileEntity->getPath()))],
            ['Size', $this->fileSystem->formatFileSize($fileEntity->getPath())],
        ];

        $finfo = finfo_open();
        $fileInformation = @finfo_file($finfo, $fileEntity->getPath());
        finfo_close($finfo);
        $rows[] = ['Mime', $fileInformation];

        $pathParts = pathinfo($fileEntity->getPath());
        foreach ($pathParts as $prop => $value) {
            $rows[] = [$prop, $value];
        }

        $io->table(['OS property', 'Value'], $rows);

        $io->success('Ok');

        return command::SUCCESS;
    }
}
