<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FieldTypes\Types\SubformFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\SubformFormManager;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use DateInterval;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Uid\Ulid;

/**
 * Find and clean orphan sub-forms.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsCleanSubforms extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:clean:subforms';
    protected static $defaultDescription = 'Find and clean orphan sub-forms.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected array $processedIds = [];

    /**
     * Command constructor.
     *
     * @param string|null    $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService $sessionService
     * @param FormFactory    $formFactory    Field type manager.
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addUsage('admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $userName = $input->getArgument('user');

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        // $io->text(sprintf('Using "%s" user session.', $userName));

        // Finder control.
        $subformDetected = [];
        // Search record in all forms.
        /** @var FormManagerInterface[] $forms */
        $forms = $this->formFactory->findAll();
        // Count all subforms records.
        foreach ($forms as $form) {
            if (SubformFormManager::class === $form->getType()) {
                $count = $form->countBy([]);
                $subformDetected[$form->getId()]['count'] = $count;
                // $io->text(sprintf('%s records in %s', $subformDetected[$form->getId()]['count'], $form->getId()));
            }
            // See if this subform field is parent to others.
            /** @var SubformFieldType $field */
            foreach ($form->getFields() as $field) {
                if ('subform_reference' === $field->getFieldType()) {
                    $childId = $field->getSubFormId();
                    $subformDetected[$childId]['parents'][] = $form->getId();
                    $subformDetected[$childId]['parent-fields'][] = $field->getId();
                }
            }
        }
        // For each child record, verifies if it has a reference in any field of any parent.
        $orphans = [];
        foreach ($subformDetected as $subformId => $data) {
            if (isset($data['count']) && $data['count'] > 0) {
                $subManager = $this->formFactory->find($subformId);
                $pageSize = 100;
                $page = 1;
                do {
                    $childrenRecords = $subManager->findBy([], [], $pageSize, ($page - 1) * $pageSize);
                    /** @var FormRecord $children */
                    foreach ($childrenRecords as $children) {
                        // Search in each parent field.
                        foreach ($data['parents'] as $parentId) {
                            $parentManager = $this->formFactory->find($parentId);
                            foreach ($data['parent-fields'] as $parentFieldId) {
                                $ulid = Ulid::fromString($children->getId());
                                $record = $parentManager->findBy([$parentFieldId => $ulid->toBinary()]);
                                if (count($record) <= 0) {
                                    $orphans[$subformId][] = $children;
                                }
                            }
                        }
                    }
                    ++$page;
                } while (count($childrenRecords) > 0);
            }
        }
        // Remove detected orphan records.
        /**
         * @var string $fid
         * @var FormRecord[] $orphan
         */
        foreach ($orphans as $fid => $orphan) {
            if (isset($subformDetected[$fid])) {
                $io->text(sprintf('%s / %s orphans in %s', count($orphan), $subformDetected[$fid]['count'], $fid));
            } else {
                $io->text(sprintf('%s orphans in %s', $subformDetected[$fid]['count'], $fid));
            }
            foreach ($orphan as $item) {
                // Remove orphan records with 1-week-old.
                /** @var DateTime $date */
                $updated = $item->{'field_changed'};
                $date = $updated->sub(DateInterval::createFromDateString('-1 week'));
                if ($date <= new DateTime('now')) {
                    // Remove this.
                    $item->getForm()->drop($item->getId());
                    $io->text(sprintf(
                        '%s: Removed %s from %s dated on %s',
                        (new DateTime('now'))->format('Y-m-d'),
                        $item->getId(),
                        $item->getForm()->getId(),
                        $updated->format('Y-m-d')
                    ));
                } else {
                    // Preserve this because have less than a week.
                    $io->text(sprintf(
                        '%s: preserved %s from %s dated on %s [It have less than a week]',
                        (new DateTime('now'))->format('Y-m-d'),
                        $item->getId(),
                        $item->getForm()->getId(),
                        $updated->format('Y-m-d')
                    ));
                }
            }
        }

        return command::SUCCESS;
    }
}
