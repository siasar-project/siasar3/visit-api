<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FieldTypes\Types\SelectFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Kernel;
use App\Repository\FileRepository;
use App\Service\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Update forms default configuration structure.
 *
 * Form manager classes must update default structure if required.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsConfigurationUpdate extends Command
{
    protected static $defaultName = 'forms:configuration:update';
    protected static $defaultDescription = 'Update forms default configuration structure.';
    protected FormFactory $formFactory;

    /**
     * Command constructor.
     *
     * @param string|null $name        The name of the command; passing null means it must be set in configure()
     * @param FormFactory $formFactory Field type manager.
     */
    public function __construct(string $name = null, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('form_id', InputArgument::OPTIONAL, 'The form to update.', 'all');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $formId = $input->getArgument('form_id');
        $forms = $this->formFactory->findAll();

        /** @var FormManagerInterface $form */
        foreach ($forms as $formKey => $form) {
            if ('all' === $formId || $formId === $formKey) {
                $nForm = $this->formFactory->findEditable($formKey);
                $nForm->saveNow();
                $io->text(sprintf('%s updated.', $formKey));
            }
        }

        return command::SUCCESS;
    }
}
