<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Kernel;
use App\Repository\FileRepository;
use App\Service\FileSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Helper;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Display forms field type list.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsFieldTypeList extends Command
{
    protected static $defaultName = 'forms:fieldtype:list';
    protected static $defaultDescription = 'Display forms field type list.';
    protected FieldTypeManager $fieldTypeManager;

    /**
     * Command constructor.
     *
     * @param string|null      $name             The name of the command; passing null means it must be set in configure()
     * @param FieldTypeManager $fieldTypeManager Field type manager.
     */
    public function __construct(string $name = null, FieldTypeManager $fieldTypeManager)
    {
        parent::__construct($name);
        $this->fieldTypeManager = $fieldTypeManager;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('field_id', InputArgument::OPTIONAL, 'The field type ID to show.', 'all');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $fieldId = $input->getArgument('field_id');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();

        foreach ($fieldTypes as $fieldKey => $fieldType) {
            if ('all' === $fieldId || $fieldId === $fieldKey) {
                /** @var FieldType $annotation */
                $annotation = $fieldType['annotation'];

                $rows = [
                    ['<comment>id</comment>', ' => ', $annotation->getId()],
                    ['<comment>description</comment>', ' => ', $annotation->getDescription()],
                    ['<comment>class</comment>', ' => ', $fieldType['class']],
                    new TableSeparator(),
                    ['<info>Settings</>'],
                    new TableSeparator(),
                ];
                foreach ($annotation->getSettings() as $setKey => $setting) {
                    $rows[] = ['<comment>'.$setKey.'</comment>', ' => ', $setting];
                }
                $rows[] = new TableSeparator();
                $rows[] = ['<info>[· = Main] Property => default value</>'];
                $rows[] = new TableSeparator();
                $field = $this->fieldTypeManager->create($annotation->getId());
                $props = $field->getProperties();
                foreach ($props as $propKey => $default) {
                    if ($propKey === $field->getPrimaryProperty()) {
                        $propKey = '· '.$propKey;
                    }
                    ob_start();
                    var_dump($default);
                    $def = ob_get_clean();
                    $rows[] = ['<comment>**'.$propKey.'**</comment>', ' => ', trim($def)];
                }

                $io->writeln('<info>### '.$annotation->getLabel()->render().'</>');
                $io->table([], $rows);
            }
        }

        return command::SUCCESS;
    }
}
