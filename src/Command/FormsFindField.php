<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\Types\SelectFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\HouseholdFormManager;
use App\Forms\SubformFormManager;
use App\Traits\GetContainerTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Find form fields.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsFindField extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:find:field';
    protected static $defaultDescription = 'Find field ID by catalog ID.';
    protected FormFactory $formFactory;
    protected bool $useYaml;

    /**
     * Command constructor.
     *
     * @param string|null $name        The name of the command; passing null means it must be set in configure()
     * @param FormFactory $formFactory Field type manager.
     */
    public function __construct(string $name = null, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('field_id', InputArgument::REQUIRED, 'The field to search on. Accept paper codes.')
            ->addArgument('form_id', InputArgument::OPTIONAL, 'The form to search on.', 'all')
            ->addOption(
                'yaml',
                'm',
                InputOption::VALUE_NEGATABLE,
                'Search in Yaml files from configuration export folder.',
                false
            )
            ->addUsage('form.school')
            ->addUsage('-m')
            ->addUsage('-m form.school');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->useYaml = $input->getOption('yaml');
        $fieldId = $this->formatFieldId($input->getArgument('field_id'));
        $formId = $input->getArgument('form_id');

        if (!$this->useYaml) {
            $io->info('Searching in database.');
            $forms = $this->formFactory->findAll();
        } else {
            $io->info('Searching in configuration export files.');
            chdir(self::getKernelInstance()->getProjectDir());
            $finder = new Finder();
            $finder->name('form.*.yml');
            $flConfigs = $finder->in($_ENV['APP_CONFIG_EXPORT_DIR']);

            $forms = [];
            foreach ($flConfigs as $flConfig) {
                $content = $this->getFileContent($flConfig);
                $forms[$flConfig->getFilenameWithoutExtension()] = $this->formFactory->create($flConfig->getFilenameWithoutExtension(), $content['type'], $content);
            }
        }

        $io->info(sprintf('Find field "%s" in form "%s".', $fieldId, $formId));

        $rows = [];
        /** @var FormManagerInterface $form */
        foreach ($forms as $formKey => $form) {
            if ('all' === $formId || $formId === $formKey) {
                $fields = $form->getFields();
                /** @var FieldTypeInterface $field */
                foreach ($fields as $field) {
                    $meta = $field->getMeta();
                    if (isset($meta['catalog_id'])) {
                        $catalogId = $meta['catalog_id'];
                        if ($catalogId === $fieldId) {
                            $row = [
                                $field->getId(),
                                $field->getFieldType(),
                                $catalogId,
                                $form->getId(),
                            ];
                            $rows[] = $row;
                        }
                    }
                }
            }
        }
        if (count($rows) > 0) {
            $io->table(['Field', 'Type', 'Catalog', 'Form'], $rows);
        } else {
            $io->info('Not found');
        }

        return command::SUCCESS;
    }

    /**
     * Normalize a field ID to the internal forms ID format.
     *
     * @param string $id
     *
     * @return string
     */
    protected function formatFieldId(string $id): string
    {
        $id = strtolower($id);
        $startAlfaCharts = [
            'a' => 1,
            'b' => 2,
            'c' => 3,
            'd' => 4,
            'e' => 5,
            'f' => 6,
            'g' => 7,
            'h' => 8,
            'i' => 9,
            'j' => 10,
            'k' => 11,
            'l' => 12,
            'm' => 13,
            'o' => 15,
            'p' => 16,
            'q' => 17,
            'r' => 18,
            's' => 19,
            't' => 20,
            'u' => 21,
            'v' => 22,
            'w' => 23,
            'x' => 24,
            'y' => 25,
            'z' => 26,
        ];
        $first = substr($id, 0, 1);
        if (in_array($first, array_keys($startAlfaCharts))) {
            if ('h' === $first) {
                $second = substr($id, 1, 1);
                if (!in_array($second, array_keys($startAlfaCharts))) {
                    return $startAlfaCharts[$first].'.'.substr($id, 1);
                }

                return $this->formatFieldId(substr($id, 1));
            }
            $id = $startAlfaCharts[$first].'.'.substr($id, 1);
        }

        return $id;
    }

    /**
     * Get FormManager from database or configuration files.
     *
     * @param string $formId
     *
     * @return FormManagerInterface|array|null
     *
     * @throws \Exception
     */
    protected function getFormDefinition(string $formId)
    {
        if (!$this->useYaml) {
            return $this->formFactory->find($formId);
        }

        chdir(self::getKernelInstance()->getProjectDir());
        $finder = new Finder();
        $finder->name($formId.'.yml');
        $flConfigs = $finder->in($_ENV['APP_CONFIG_EXPORT_DIR']);
        foreach ($flConfigs as $flConfig) {
            $content = $this->getFileContent($flConfig);

            return $this->formFactory->create($flConfig->getFilenameWithoutExtension(), $content['type'], $content);
        }

        return [];
    }

    /**
     * Get configuration file content.
     *
     * @param SplFileInfo $file
     *
     * @return array
     */
    protected function getFileContent(SplFileInfo $file): array
    {
        try {
            return Yaml::parseFile($file->getPathname());
        } catch (ParseException $e) {
            throw new ParseException(
                'Malformed inline YAML string',
                $e->getParsedLine(),
                $e->getSnippet(),
                $file->getFilename()
            );
        }
    }
}
