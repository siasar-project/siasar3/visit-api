<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Service\SessionService;
use App\Tools\Json;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Uid\Ulid;

/**
 * Find form record.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <aaaaa976@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsFindRecord extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:find:record';
    protected static $defaultDescription = 'Display information about a form record.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected array $processedIds = [];

    /**
     * Command constructor.
     *
     * @param string|null    $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService $sessionService
     * @param FormFactory    $formFactory    Field type manager.
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('record', InputArgument::REQUIRED, 'Form record to find.')
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addUsage('01GHGBYCFRYEXCARQDYBHPAE72 admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $recordId = $input->getArgument('record');
        $userName = $input->getArgument('user');

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        // $io->text(sprintf('Using "%s" user session.', $userName));

        // Search record in all inquiry forms.
        $forms = $this->formFactory->findAll();
        $record = null;
        $ulidRecordId = new Ulid($recordId);
        /** @var FormManagerInterface $form */
        foreach ($forms as $form) {
            $record = $form->find($recordId);
            if ($record) {
                break;
            } elseif ($form->isField('field_ulid_reference')) {
                // Try to find by serial number.
                $record = current($form->findBy(['field_ulid_reference' => $ulidRecordId->toBinary()]));
                if ($record) {
                    break;
                }
            }
        }

        $data = [];

        if (!$record) {
            $io->error(sprintf('Form record "%s" not found.', $recordId));

            return Command::FAILURE;
        }

        $data[] = ['Record' => $record->getId()];
        if ($form->isField('field_ulid_reference')) {
            $data[] = ['Reference' => $record->{'field_ulid_reference'}];
        }
        $data[] = ['Form' => sprintf('%s [%s]', $record->getForm()->getTitle(), $record->getForm()->getId())];
        /** @var AdministrativeDivision|null $region */
        $region = $record->{'field_region'};
        $data[] = ['Community' => $this->getCommunityPath($region)];
        /** @var DateTime $changed */
        $changed = $record->{'field_changed'};
        $data[] = ['Changed' => sprintf('%s %s', $changed->format('Y-m-d H:i:s'), $changed->getTimezone()->getName())];
        /** @var Country $country */
        $country = $record->{'field_country'};
        $data[] = ['Country' => sprintf('%s [%s]', $country->getName(), $country->getCode())];
        $data[] = ['Status' => $record->{'field_status'}];

        // Llamar a definitionList con los parámetros descompuestos
        $io->definitionList(...$data);

        return command::SUCCESS;
    }

    /**
     * Get path from region.
     *
     * @param AdministrativeDivision|null $region Region to convert to string.
     *
     * @return string Region path.
     */
    protected function getCommunityPath(AdministrativeDivision|null $region): string
    {
        if ($region) {
            $regionData = $region->formatExtraJson();

            return $this->buildPathFromCommunityExtraJson($regionData);
        }

        return 'Without defined region.';
    }

    /**
     * Build path from extraJson about community.
     *
     * @param array $data Administrative community extraJson data.
     *
     * @return string Path string.
     */
    protected function buildPathFromCommunityExtraJson(array $data): string
    {
        $resp = '';
        if (isset($data["parent"])) {
            $resp = $this->buildPathFromCommunityExtraJson($data["parent"]);
        }

        return $resp.'/'.$data["name"];
    }
}
