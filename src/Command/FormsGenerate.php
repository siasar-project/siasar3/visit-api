<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Repository\UserRepository;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\TestBrowserToken;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Display forms field type list.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsGenerate extends AbstractDevCommandBase
{
    protected static $defaultName = 'forms:generate:dummies';
    protected static $defaultDescription = 'Generate dummy form records.';
    protected ManagerRegistry $entityManager;
    protected ParameterBagInterface $systemSettings;
    protected FormFactory $formFactory;
    protected SessionService $sessionService;

    /**
     * Command constructor.
     *
     * @param SessionService  $sessionService
     * @param ManagerRegistry $entityManager
     * @param FormFactory     $formFactory
     * @param string|null     $name           The name of the command; passing null means it must be set in configure()
     */
    public function __construct(SessionService $sessionService, ManagerRegistry $entityManager, FormFactory $formFactory, string $name = null)
    {
        parent::__construct($entityManager, $name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addOption(
                'clear',
                'c',
                InputOption::VALUE_NEGATABLE,
                'Remove current form records in the platform',
                false
            )
            ->addOption(
                'max-records',
                'r',
                InputOption::VALUE_OPTIONAL,
                'Maximum records to create by each form, -1 to generate between 1000 and 5000 or 0 to clear forms',
                -1
            )
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addArgument(
                'country',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Generate form records to this country code.',
                ['all']
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (Command::FAILURE === parent::execute($input, $output)) {
            return Command::FAILURE;
        }
        $io = new SymfonyStyle($input, $output);

        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }

        $userName = $input->getArgument('user');
        $countryCodes = $input->getArgument('country');
        $clearEntities = boolval($input->getOption('clear'));
        $maxRecords = intval($input->getOption('max-records'));

        if (Command::SUCCESS !== ($returnCode = $this->validateCountryCodes($io, $countryCodes))) {
            return $returnCode;
        }

        $this->sessionService->loginFakeUser($userName);
        $io->text(sprintf('Using "%s" user session.', $userName));

        /** @var FormManager[] $forms */
        $forms = $this->formFactory->findAll();
        $io->info(sprintf('Found %s forms.', count($forms)));
        // Clear forms.
        foreach ($forms as $form) {
            if ($form->isInstalled()) {
                $this->clearForm($form, $io, $clearEntities);
            }
        }
        $io->text('');
        // Generate records.
        foreach ($forms as $form) {
            if ($form->isInstalled()) {
                if ('App\Forms\SubformFormManager' === $form->getType()) {
                    // Subform.
                    $io->text(sprintf('Ignoring subform "%s":<info>%s</info>, subform of "%s". Parent form will generate records to this form.', $form->getTitle(), $form->getId(), $form->getParentForm()));
                } elseif ('App\Forms\PointFormManager' === $form->getType()) {
                    // Ignore this form types.
                } elseif ('App\Forms\HouseholdFormManager' === $form->getType()) {
                    // Ignore this form types.
                } else {
                    // Generate form records.
                    $io->text(sprintf('Generating data to form "%s":<info>%s</info>.', $form->getTitle(), $form->getId()));
                    $result = count($this->generateRecords($form, $io, $countryCodes, $maxRecords, true));
                }
            } else {
                $io->text(sprintf('Form "%s":<info>%s</info> not installed.', $form->getTitle(), $form->getId()));
            }
        }

        return command::SUCCESS;
    }

    /**
     * Clear records if allowed.
     *
     * @param FormManagerInterface $form
     * @param SymfonyStyle         $io
     * @param bool                 $clear Must we allow clear records?
     */
    protected function clearForm(FormManagerInterface $form, SymfonyStyle $io, bool $clear = true): void
    {
        if ($clear) {
            // Remove all content.
            $form->uninstall();
            $form->install();
            $io->text(sprintf('"%s":<info>%s</info> - All data wiped.', $form->getTitle(), $form->getId()));
        }
    }

    /**
     * Generate dummy records.
     *
     * @param FormManagerInterface $form
     * @param SymfonyStyle         $io
     * @param string[]             $countryCodes
     * @param int                  $amount
     * @param bool                 $progress
     *
     * @return string[] Generated records IDs.
     *
     * @throws \ReflectionException
     */
    protected function generateRecords(FormManagerInterface $form, SymfonyStyle $io, array $countryCodes, int $amount = 0, bool $progress = false): array
    {
        $resp = [];
        if ($amount < 0) {
            $amount = random_int(1000, 5000);
        }
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $countryRepository = $this->entityManager->getRepository(Country::class);
        $fields = $form->getFields();
        // Add dummy records.
        if ($progress) {
            $io->progressStart($amount);
        }
        for ($i = 0; $i < $amount; ++$i) {
            $recordData = [];
            // Set random country.
            $cCode = $countryCodes[random_int(0, count($countryCodes) - 1)];
            /** @var Country $country */
            $country = $countryRepository->find($cCode);
            /** @var FieldTypeInterface $field */
            foreach ($fields as $field) {
                if (InquiryFormManager::class === $form->getType() && $field->getId() === 'field_country') {
                    // Move to a country.
                    $recordData["field_country"]["value"] = $cCode;
                    continue;
                }
                switch ($field->getFieldType()) {
                    case 'form_record_reference':
                    case 'wsprovider_reference':
                    case 'community_reference':
                    case 'subform_reference':
                    case 'wsystem_reference':
                    case 'household_process_reference':
                    case 'point_reference':
                        continue;
                        // Generate subform data and link here.
                        // $settings = $field->getSettings();
                        // if (isset($settings["settings"]["meta"]["subform"])) {
                        //     $subform = $this->formFactory->find($settings["settings"]["meta"]["subform"]);
                        //     if ($subform) {
                        //         if ($field->isMultivalued()) {
                        //             // Multivalued.
                        //             $ids = $this->generateRecords($subform, $io, $countryCodes, random_int(1, 5));
                        //             $recordData[$field->getId()] = [];
                        //             foreach ($ids as $id) {
                        //                 $recordData[$field->getId()][] = [
                        //                     'value' => $id,
                        //                     'form' => $subform->getId(),
                        //                 ];
                        //             }
                        //         } else {
                        //             // Single value.
                        //             $ids = $this->generateRecords($subform, $io, $countryCodes, 1);
                        //             $recordData[$field->getId()] = [
                        //                 'value' => current($ids),
                        //                 'form' => $subform->getId(),
                        //             ];
                        //         }
                        //     }
                        // }
                        break;
                    case 'select':
                        if ($field->isRequired()) {
                            if ($field->isMultivalued()) {
                                $recordData[$field->getId()] = [
                                    $field->getExampleData($recordData),
                                    $field->getExampleData($recordData),
                                ];
                            } else {
                                $recordData[$field->getId()] = $field->getExampleData($recordData);
                            }
                        }
                        break;
                    case 'administrative_division_reference':
                        if ($field->isMultivalued()) {
                            if ($country->getDeep() > 0) {
                                $recordData[$field->getId()] = [
                                    $field->getExampleData($recordData),
                                    $field->getExampleData($recordData),
                                ];
                            }
                        } else {
                            if ($country->getDeep() > 0) {
                                $recordData[$field->getId()] = $field->getExampleData($recordData);
                            }
                        }
                        break;
                    default:
                        if ($field->isMultivalued()) {
                            $recordData[$field->getId()] = [
                                $field->getExampleData($recordData),
                                $field->getExampleData($recordData),
                            ];
                        } else {
                            $recordData[$field->getId()] = $field->getExampleData($recordData);
                        }
                        break;
                }
            }
            if (InquiryFormManager::class === $form->getType()) {
                // Move to a country.
                // Set random country.
                //$cCode = $countryCodes[random_int(0, count($countryCodes) - 1)];
                $recordData["field_country"]["value"] = $cCode;
                // Set random administrative division inside country.
                $rootRegions = $divisionRepository->findBy(
                    [
                        'country' => $country->getId(),
                        'level' => $country->getDeep(),
                    ],
                    [],
                    10
                );
                if (count($rootRegions) > 0) {
                    $recordData["field_region"]["value"] = $rootRegions[random_int(0, count($rootRegions) - 1)]->getId();
                } else {
                    unset($recordData["field_region"]);
                }
            }
            $resp[] = $form->insert($recordData);
            if ($progress) {
                $io->progressAdvance();
            }
        }
        if ($progress) {
            $io->progressFinish();
        }

        return $resp;
    }
}
