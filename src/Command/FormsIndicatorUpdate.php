<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\User;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Forms\PointFormManager;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Find form fields.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsIndicatorUpdate extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:indicator:update';
    protected static $defaultDescription = 'Update a form record indicator value.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected EntityManagerInterface $entityManager;

    /**
     * Command constructor.
     *
     * @param string|null            $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService         $sessionService
     * @param FormFactory            $formFactory    Field type manager.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->entityManager = $entityManager;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('record', InputArgument::REQUIRED, 'Form record to update indicator.')
            ->addArgument(
                'user',
                InputArgument::OPTIONAL,
                'Administrator username.',
                'any'
            )
            ->addUsage('01GJPZP961X9VVT8J98F1GKFF1 admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $recordId = $input->getArgument('record');
        $userName = $input->getArgument('user');

        if ('any' === $userName) {
            // Find and use any admin user.
            $admins = $this->entityManager
                ->getRepository(User::class)
                ->findByRole(['ROLE_ADMIN']);
            $admin = current($admins);
            if ($admins) {
                $userName = $admin->getUsername();
            }
        }

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        $io->text(sprintf('Using "%s" user session.', $userName));

        // Search record in all inquiry forms.
        $forms = $this->formFactory->findByType(InquiryFormManager::class);
        $record = null;
        /** @var FormManagerInterface $form */
        foreach ($forms as $form) {
            $record = $form->find($recordId);
            if ($record) {
                break;
            }
        }

        if (!$record) {
            $io->error(sprintf('Form record "%s" not found.', $recordId));

            return Command::FAILURE;
        }

        /** @var DateTime $changed */
        $changed = $record->{'field_changed'};
        $formMetaDefinition = $record->getForm()->getMeta();
        if (!isset($formMetaDefinition['indicator_class'])) {
            $io->error(sprintf('Form "%s" have no indicator resolver.', $record->getForm()->getId()));

            return Command::FAILURE;
        }
        $indicatorClass = $formMetaDefinition['indicator_class'];
        $context = null;
        if ($formMetaDefinition['point_associated']) {
            // Created point context.
            /** @var InquiryFormManager $form */
            $form = $record->getForm();
            /** @var PointFormManager $pointManager */
            $pointManager = $this->formFactory->find('form.point');
            switch ($form->getId()) {
                case 'form.community':
                    $point = current($pointManager->findByCommunity($recordId));
                    break;
                case 'form.wssystem':
                    $point = current($pointManager->findByWSystem($recordId));
                    break;
                case 'form.wsprovider':
                    $point = current($pointManager->findByWsp($recordId));
                    break;
                default:
                    $io->error(sprintf('Unknown form "%s" inside a point.', $record->getForm()->getId()));

                    return Command::FAILURE;
            }
            if (!$point) {
                $io->error(sprintf('Form "%s"\'s point not found.', $record->getForm()->getId()));

                return Command::FAILURE;
            }
            $context = new PointIndicatorContext($record, $point);
        } else {
            // Create simple context.
            $context = new SimpleIndicatorContext($record);
        }
        /** @var AbstractIndicator $indicator */
        $indicator = new $indicatorClass($context);
        $rows = [
            [
                $record->getId(),
                $record->getForm()->getId(),
                $record->getForm()->getType(),
                $changed->format('Y-m-d H:i.s'),
                $record->{'field_status'},
                sprintf('%s | %s', $indicator->getLabel(true), $indicator->getValue(true)),
            ],
        ];
        $io->table(['ID', 'Manager', 'Type', 'Updated', 'Status', 'Indicator'], $rows);

        if ('validated' !== $record->{'field_status'} && 'locked' !== $record->{'field_status'}) {
            $io->error(sprintf('Form record "%s" not validated or locked.', $recordId));

            return Command::FAILURE;
        }

        // Reset and update indicator.
        $indicator->reset();
        $newValue = $indicator->getValue();
        $io->info(sprintf('New indicator value: %s', $newValue));

        return command::SUCCESS;
    }
}
