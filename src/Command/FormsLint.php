<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\Types\SelectFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\HouseholdFormManager;
use App\Forms\InquiryFormManager;
use App\Forms\MailFormManager;
use App\Forms\SubformFormManager;
use App\Traits\GetContainerTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Validate forms meta.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsLint extends Command
{
    use GetContainerTrait;

    const VALID_TABLE_FIELDS_TYPES = ['integer', 'decimal', 'long_text', 'short_text', 'date', 'phone', 'boolean', 'radio_boolean', 'ulid'];

    protected static $defaultName = 'forms:lint';
    protected static $defaultDescription = 'Validate forms meta from database, or configuration files, default option.';
    protected FormFactory $formFactory;
    protected bool $displayWarnings;
    protected bool $finishedWithErrors;
    protected bool $finishedWithWarnings;
    protected bool $useYaml;

    /**
     * Command constructor.
     *
     * @param string|null $name        The name of the command; passing null means it must be set in configure()
     * @param FormFactory $formFactory Field type manager.
     */
    public function __construct(string $name = null, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->displayWarnings = true;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('form_id', InputArgument::OPTIONAL, 'The form to validate.', 'all')
            ->addOption(
                'yaml',
                'm',
                InputOption::VALUE_NEGATABLE,
                'Validate Yaml files from configuration export folder.',
                true
            )
            ->addOption(
                'warnings',
                'w',
                InputOption::VALUE_NEGATABLE,
                'Display warnings.',
                true
            )
            ->addUsage('form.school')
            ->addUsage('-m')
            ->addUsage('-m form.school');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->useYaml = $input->getOption('yaml');
        $this->displayWarnings = $input->getOption('warnings');
        $this->finishedWithErrors = false;
        $this->finishedWithWarnings = false;
        $formId = $input->getArgument('form_id');

        if (!$this->useYaml) {
            $io->info('Validating from database.');
            $forms = $this->formFactory->findAll();
        } else {
            $io->info('Validating from configuration export files.');
            chdir(self::getKernelInstance()->getProjectDir());
            $finder = new Finder();
            $finder->name('form.*.yml');
            $flConfigs = $finder->in($_ENV['APP_CONFIG_EXPORT_DIR']);

            $forms = [];
            foreach ($flConfigs as $flConfig) {
                $content = $this->getFileContent($flConfig);
                $forms[$flConfig->getFilenameWithoutExtension()] = $this->formFactory->create($flConfig->getFilenameWithoutExtension(), $content['type'], $content);
            }
        }

        $failed = false;
        /** @var FormManagerInterface $form */
        foreach ($forms as $formKey => $form) {
            if ('all' === $formId || $formId === $formKey) {
                $currentFailed = false;
                $io->newLine();
                $io->title(sprintf('Validating %s...', $formKey));
                if (!$this->validateFormMeta($io, $form)) {
                    $failed = true;
                    $currentFailed = true;
                }
                /** @var FieldTypeInterface[] $fields */
                $fields = $form->getFields();
                foreach ($fields as $field) {
                    if (!$this->validateFieldMeta($io, $form, $field)) {
                        $failed = true;
                        $currentFailed = true;
                    }
                }
                if (!$currentFailed) {
                    $io->success(sprintf('%s is valid.', $formKey));
                }
            }
        }
        $io->newLine();
        $io->title('Process completed.');

        if ($this->finishedWithErrors) {
            $this->error($io, 'Finished with errors.');

            return Command::FAILURE;
        }
        if ($this->finishedWithWarnings) {
            $io->warning('Finished ok with warnings or comments.');
        } else {
            $io->success('Finished.');
        }

        return command::SUCCESS;
    }

    /**
     * Get FormManager from database or configuration files.
     *
     * @param string $formId
     *
     * @return FormManagerInterface|array|null
     *
     * @throws \Exception
     */
    protected function getFormDefinition(string $formId)
    {
        if (!$this->useYaml) {
            return $this->formFactory->find($formId);
        }

        chdir(self::getKernelInstance()->getProjectDir());
        $finder = new Finder();
        $finder->name($formId.'.yml');
        $flConfigs = $finder->in($_ENV['APP_CONFIG_EXPORT_DIR']);
        foreach ($flConfigs as $flConfig) {
            $content = $this->getFileContent($flConfig);

            return $this->formFactory->create($flConfig->getFilenameWithoutExtension(), $content['type'], $content);
        }

        return [];
    }

    /**
     * Validate field meta.
     *
     * @param SymfonyStyle         $io
     * @param FormManagerInterface $form
     * @param FieldTypeInterface   $field
     *
     * @return bool
     */
    protected function validateFieldMeta(SymfonyStyle $io, FormManagerInterface $form, FieldTypeInterface $field): bool
    {
        $resp = true;
        $meta = $field->getMeta();

        if (isset($meta['catalog_id']) && empty($meta['catalog_id'])) {
            $this->error($io, sprintf('[%s] Meta catalog_id is required if defined.', $form->getId()));
            $resp = false;
        }
        if (isset($meta['level']) && empty($meta['level'])) {
            $this->error($io, sprintf('[%s] Meta level is required if defined.', $form->getId()));
            $resp = false;
        }
        if (isset($meta['field_other']) && empty($meta['field_other'])) {
            $this->error($io, sprintf('[%s] Meta field_other is required if defined.', $form->getId()));
            $resp = false;
        }

        foreach ($meta as $property => $value) {
            switch ($property) {
                case 'catalog_id':
                case 'field_other':
                    // Without extra validations.
                    break;
                case 'level':
                    if (!is_numeric($value) || !($value >= 1 && $value <= 3)) {
                        $this->error($io, sprintf('[%s] Meta level must be between 1 and 3, both inclusive.', $form->getId()));
                        $resp = false;
                    }
                    break;
                case 'sdg':
                    if (!$this->isYamlBool($value)) {
                        $this->error($io, sprintf('[%s] Meta sdg must be a boolean value.', $form->getId()));
                        $resp = false;
                    }
                    break;
                case 'hidden':
                    if (!$this->isYamlBool($value)) {
                        $this->error($io, sprintf('[%s] Meta hidden must be a boolean value.', $form->getId()));
                        $resp = false;
                    }
                    break;
                case 'disabled':
                    if (!$this->isYamlBool($value)) {
                        $this->error($io, sprintf('[%s] Meta disabled must be a boolean value.', $form->getId()));
                        $resp = false;
                    }
                    break;
                case 'displayId':
                    if (!$this->isYamlBool($value)) {
                        $this->error($io, sprintf('[%s] Meta displayId must be a boolean value.', $form->getId()));
                        $resp = false;
                    }
                    break;
                case 'conditional':
                    $resp = $resp && $this->validateFormMetaFieldGroupConditional($io, $form, $field->getId(), $meta);
                    break;
                case 'filtered':
                    if (!$this->isYamlBool($value)) {
                        $this->error($io, sprintf('[%s] Meta filtered must be a boolean value.', $form->getId()));
                        $resp = false;
                    }
                    break;
                default:
                    $this->comment($io, 'Unknown field meta property: '.$property);
                    break;
            }
        }

        if (!$resp) {
            $io->info(print_r($meta, true));
        }

        return $resp;
    }

    /**
     * Validate form meta.
     *
     * @param SymfonyStyle         $io
     * @param FormManagerInterface $form
     *
     * @return bool
     */
    protected function validateFormMeta(SymfonyStyle $io, FormManagerInterface $form): bool
    {
        $resp = true;
        $formMeta = $form->getMeta();

        // Alert if it finds known meta property.
        $known = ['title', 'version', 'allow_sdg', 'icon_field', 'title_field', 'interviewer_field', 'table_fields', 'parent_form', 'field_groups', 'point_associated', 'indicator_class', 'isSubForm'];
        foreach ($formMeta as $property => $value) {
            if (!in_array($property, $known)) {
                $this->comment($io, 'Unknown form meta property: '.$property);
            }
        }

        // Validate each meta property.

        if (!isset($formMeta['title']) || empty($formMeta['title'])) {
            $this->error($io, sprintf('[%s] Meta title is required.', $form->getId()));
            $resp = false;
        }
        if (!isset($formMeta['version']) || empty($formMeta['version'])) {
            $this->error($io, sprintf('[%s] Meta version is required.', $form->getId()));
            $resp = false;
        }
        // Validate that if allow_sdg is defined, optional, and it's a bool value.
        if (isset($formMeta['allow_sdg']) && !$this->isYamlBool($formMeta['allow_sdg'])) {
            $this->error($io, sprintf('[%s] Meta allow_sdg must be a boolean value.', $form->getId()));
            $resp = false;
        }
        // Validate that if isSubForm is defined, optional, and it's a bool value.
        if (isset($formMeta['isSubForm']) && !$this->isYamlBool($formMeta['isSubForm'])) {
            $this->error($io, sprintf('[%s] Meta isSubForm must be a boolean value.', $form->getId()));
            $resp = false;
        }
        // Validate that if icon_field is defined reference a valid field and the field is an image field type.
        if (isset($formMeta['icon_field']) && !$this->isImageField($formMeta['icon_field'], $form)) {
            $this->error($io, sprintf('[%s] Meta icon_field must be a defined field, and an image field type.', $form->getId()));
            $resp = false;
        }
        // Validate that if title_field is defined reference a valid field and the field is a short_text field type.
        if (isset($formMeta['title_field']) &&
            (!$this->isShortTextField($formMeta['title_field'], $form) && !$this->isTapReferenceField($formMeta['title_field'], $form))
        ) {
            $this->error($io, sprintf('[%s] Meta title_field must be a defined field, and an short text or tap reference field type.', $form->getId()));
            $resp = false;
        }
        // Validate that if interviewer_field is defined reference a valid field and the field is a user_reference field type.
        if (isset($formMeta['interviewer_field']) &&
            (!$this->isTypeField('user_reference', $formMeta['interviewer_field'], $form))
        ) {
            $this->error($io, sprintf('[%s] Meta interviewer_field must be a defined field, and an user reference field type.', $form->getId()));
            $resp = false;
        }
        // Validate that if table_fields is defined reference a valid field and the fields are valid field types.
        if (isset($formMeta['table_fields']) && !$this->isValidTableFields($formMeta['table_fields'], $form)) {
            $this->error(
                $io,
                sprintf(
                    '[%s] Meta table_fields must be a defined field, and a valid field type: %s.',
                    $form->getId(),
                    implode(', ', self::VALID_TABLE_FIELDS_TYPES)
                )
            );
            $resp = false;
        }

        if (in_array($form->getType(), [SubformFormManager::class, HouseholdFormManager::class, MailFormManager::class])) {
            if (!isset($formMeta['parent_form']) || empty($formMeta['parent_form'])) {
                $this->error($io, sprintf('[%s] Meta parent_form is required to sub-forms or household.', $form->getId()));
                $resp = false;
            }
        } else {
            if (isset($formMeta['parent_form'])) {
                $this->error($io, sprintf('[%s] Meta parent_form only is valid in sub-forms or household.', $form->getId()));
                $resp = false;
            }
        }
        if (InquiryFormManager::class === $form->getType()) {
            if (isset($formMeta['point_associated'])) {
                if (!$this->isYamlBool($formMeta['point_associated'])) {
                    $this->error($io, sprintf('[%s] Meta point_associated must be a boolean value.', $form->getId()));
                    $resp = false;
                }
            } else {
                if (!isset($formMeta['point_associated'])) {
                    $this->error($io, sprintf('[%s] Meta point_associated is required.', $form->getId()));
                    $resp = false;
                }
            }
        } else {
            if (isset($formMeta['point_associated'])) {
                $this->error(
                    $io,
                    sprintf(
                        '[%s] Meta point_associated not must exist in this form type: %s.',
                        $form->getId(),
                        $form->getType()
                    )
                );
                $resp = false;
            }
        }
        if (in_array($form->getType(), [InquiryFormManager::class])) {
            if (isset($formMeta['indicator_class']) && !$this->isValidClass($formMeta['indicator_class'])) {
                $this->error($io, sprintf('[%s] Meta indicator_class must be a valid class.', $form->getId()));
                $resp = false;
            }
        } else {
            if (isset($formMeta['indicator_class'])) {
                $this->error($io, sprintf('[%s] Meta indicator_class only is valid in Inquiries form types.', $form->getId()));
                $resp = false;
            }
        }
        if (isset($formMeta['field_groups'])) {
            if (!is_array($formMeta['field_groups'])) {
                $this->error($io, sprintf('[%s] Meta field_groups must by an array.', $form->getId()));
                $resp = false;
            } else {
                foreach ($formMeta['field_groups'] as $grpId => $grp) {
                    if (!$this->validateFormMetaFieldGroup($io, $form, $grpId, $grp)) {
                        $resp = false;
                    }
                }
            }
        }

        return $resp;
    }

    /**
     * Validate form meta field groups and children.
     *
     * @param SymfonyStyle         $io
     * @param FormManagerInterface $form
     * @param string               $grpId
     * @param array                $grp
     * @param bool                 $isChildren
     *
     * @return bool
     */
    protected function validateFormMetaFieldGroup(SymfonyStyle $io, FormManagerInterface $form, string $grpId, array $grp, bool $isChildren = false): bool
    {
        $resp = true;
        $childrenMark = $isChildren ? ', field_groups.children:'.$grpId : '';
        if (!isset($grp['title']) || empty($grp['title'])) {
            $this->error($io, sprintf('[%s%s] Meta field_groups.[].title is required.', $form->getId(), $childrenMark));
            $resp = false;
        }
        if (!isset($grp['id']) || (empty($grp['id']) && 0 !== intval($grp['id']))) {
            $this->error($io, sprintf('[%s%s] Meta field_groups.[].id is required.', $form->getId(), $childrenMark));
            $resp = false;
        } else {
            if ($isChildren && $grpId !== $grp['id']) {
                $this->error($io, sprintf('[%s%s] Meta field_groups.[].id must be equal to the children id.', $form->getId(), $childrenMark));
                $resp = false;
            }
        }
        if (isset($grp['help']) && empty($grp['help'])) {
            $this->warning($io, sprintf('Warning: [%s%s] Meta field_groups.[].help is empty.', $form->getId(), $childrenMark));
        }
        if (isset($grp['level']) && empty($grp['level'])) {
            $this->error($io, sprintf('[%s%s] Meta field_groups.[].level is required if defined.', $form->getId(), $childrenMark));
            $resp = false;
        }
        if (isset($grp['level']) && (!is_numeric($grp['level']) || !($grp['level'] >= 1 && $grp['level'] <= 3))) {
            $this->error($io, sprintf('[%s%s] Meta field_groups.[].level must be between 1 and 3, both inclusive.', $form->getId(), $childrenMark));
            $resp = false;
        }
        if (isset($grp['sdg']) && !$this->isYamlBool($grp['sdg'])) {
            $this->error($io, sprintf('[%s%s] Meta field_groups.[].sdg must be a boolean value.', $form->getId(), $childrenMark));
            $resp = false;
        }
        if (!$resp) {
            $io->info(print_r($grp, true));
        }
        if (isset($grp['children'])) {
            if (!is_array($grp['children'])) {
                $this->error($io, sprintf('[%s%s] Meta field_groups.[].children must by an array.', $form->getId(), $childrenMark));
                $io->info(print_r($grp, true));
                $resp = false;
            } else {
                foreach ($grp['children'] as $grpId => $grpChild) {
                    if (!$this->validateFormMetaFieldGroup($io, $form, $grpId, $grpChild, true)) {
                        $resp = false;
                    }
                }
            }
        }
        if (isset($grp['conditional'])) {
            if (!is_array($grp['conditional'])) {
                $this->error($io, sprintf('[%s%s] Meta field_groups.[].conditional must by an array.', $form->getId(), $childrenMark));
                $io->info(print_r($grp, true));
                $resp = false;
            } else {
                $resp = $resp && $this->validateFormMetaFieldGroupConditional($io, $form, $grpId, $grp);
            }
        }

        return $resp;
    }

    /**
     * Validate a conditional meta property.
     *
     * @param SymfonyStyle         $io
     * @param FormManagerInterface $form
     * @param string               $grpId
     * @param array                $grp
     * @param bool                 $isChildren
     *
     * @return bool
     */
    protected function validateFormMetaFieldGroupConditional(SymfonyStyle $io, FormManagerInterface $form, string $grpId, array $grp, bool $isChildren = false): bool
    {
        $resp = true;
        $childrenMark = $isChildren ? ', field_groups.children:'.$grpId : '';

        foreach ($grp['conditional'] as $property => $value) {
            if (!is_array($value)) {
                $this->error($io, sprintf('[%s%s] Meta conditional condition must by an array.', $form->getId(), $childrenMark));
                $resp = false;
            } else {
                switch ($property) {
                    case 'visible':
                        if (!is_array($value)) {
                            $this->error($io, sprintf('[%s%s] Meta conditional condition must by an array.', $form->getId(), $childrenMark));
                            $resp = false;
                        } else {
                            $resp = $resp && $this->validateFormMetaFieldGroupCondition($io, $form, $grpId, $grp, $value, $isChildren);
                        }
                        break;
                    case 'option':
                        // Validate option conditions.
                        /** @var SelectFieldType $field */
                        $field = $form->getFieldDefinition($grpId);
                        if ('select' !== $field->getFieldType() && 'radio_select' !== $field->getFieldType()) {
                            $this->error($io, sprintf('[%s%s] Meta conditional option condition must be a select field type.', $form->getId(), $childrenMark));
                            $resp = false;
                        } else {
                            $options = $field->getOptions();
                            foreach ($value as $opt => $val) {
                                if (!isset($options[$opt])) {
                                    $this->error($io, sprintf('[%s%s] Meta conditional option condition must be a valid option: %s.', $form->getId(), $childrenMark, $opt));
                                    $resp = false;
                                } else {
                                    $resp = $resp && $this->validateFormMetaFieldGroupCondition($io, $form, $grpId, $grp, $val);
                                }
                            }
                        }
                        break;
                    default:
                        $this->error($io, sprintf('[%s%s] Meta field_groups.[].conditional option not valid, it must be "visible" or "option".', $form->getId(), $childrenMark));
                        $resp = false;
                        break;
                }
            }
        }

        return $resp;
    }

    /**
     * Validate a condition group.
     *
     * @param SymfonyStyle         $io
     * @param FormManagerInterface $form
     * @param string               $grpId
     * @param array                $grp
     * @param array                $condition
     * @param bool                 $isChildren
     *
     * @return bool
     */
    protected function validateFormMetaFieldGroupCondition(SymfonyStyle $io, FormManagerInterface $form, string $grpId, array $grp, array $condition, bool $isChildren = false): bool
    {
        $resp = true;
        $childrenMark = $isChildren ? ', field_groups.children:'.$grpId : '';

        foreach ($condition as $operand => $value) {
            switch ($operand) {
                case 'and':
                case 'or':
                    $resp = $resp && $this->validateFormMetaFieldGroupCondition($io, $form, $grpId, $grp, $value, $isChildren);
                    break;
                default:
                    if (!is_array($value)) {
                        $this->error($io, sprintf('[%s%s] Meta conditional condition must by an array.', $form->getId(), $childrenMark));
                        $resp = false;
                    } else {
                        foreach ($value as $key => $val) {
                            // Must be a valid field ID.
                            if (!stripos($key, '.')) {
                                if ('or' === $key || 'and' === $key) {
                                    $resp = $resp && $this->validateFormMetaFieldGroupCondition($io, $form, $grpId, $grp, $val, $isChildren);
                                } else {
                                    // Field id in this form.
                                    $fieldDef = $form->getFieldDefinition($key);
                                    if (!$fieldDef) {
                                        $this->error($io, sprintf('[%s%s] Condition not valid: %s. Field not found: %s.', $form->getId(), $childrenMark, $key.' = "'.$val.'"', $key));
                                        $resp = false;
                                    } else {
                                        if (!$fieldDef->isValidConditionalType($val)) {
                                            $this->error($io, sprintf('[%s%s] Condition not valid. Field type can not be "%s".', $form->getId(), ', '.$fieldDef->getId(), gettype($val)));
                                            $resp = false;
                                        }
                                    }
                                }
                            } else {
                                // Field in other form.
                                $operandParts = explode('.', $key);
                                if (count($operandParts) !== 2) {
                                    $this->error($io, sprintf('[%s%s] Condition not valid: %s.', $form->getId(), $childrenMark, $key));
                                    $resp = false;
                                } elseif ('parent' === $operandParts[0]) {
                                    // Field in the parent.
                                    if ('App\Forms\SubformFormManager' !== $form->getType()) {
                                        $this->error($io, sprintf('[%s%s] Condition not valid: %s. Parent only can be used in a sub-form.', $form->getId(), $childrenMark, $key));
                                        $resp = false;
                                    } else {
                                        $parentFormId = $form->getMeta()['parent_form'];
                                        $subForm = $this->getFormDefinition($parentFormId);
                                        if (!$subForm) {
                                            $this->error($io, sprintf('[%s%s] Condition not valid: %s. Parent not found: %s.', $form->getId(), $childrenMark, $key, $parentFormId));
                                            $resp = false;
                                        } else {
                                            $fieldDef = $subForm->getFieldDefinition($operandParts[1]);
                                            if (!$fieldDef) {
                                                $this->error($io, sprintf('[%s%s] Condition not valid: %s. Parent field not found: %s.', $form->getId(), $childrenMark, $key, $parentFormId.'.'.$operandParts[1]));
                                                $resp = false;
                                            } else {
                                                if (!$fieldDef->isValidConditionalType($val)) {
                                                    $this->error($io, sprintf('[%s%s] Condition not valid. Field type can not be "%s".', $form->getId(), ', '.$fieldDef->getId(), gettype($val)));
                                                    $resp = false;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    // Field in a sub-form of this form.
                                    $refField = $form->getFieldDefinition($operandParts[0]);
                                    $fieldSettings = $refField->getSettings();
                                    $subFormId = $fieldSettings['subform'];
                                    $subForm = $this->getFormDefinition($subFormId);
                                    if (!$subForm) {
                                        $this->error($io, sprintf('[%s%s] Condition not valid: %s. Sub-form not found: %s.', $form->getId(), $childrenMark, $key, $subFormId));
                                        $resp = false;
                                    } else {
                                        $fieldDef = $subForm->getFieldDefinition($operandParts[1]);
                                        if (!$fieldDef) {
                                            $this->error($io, sprintf('[%s%s] Condition not valid: %s. Sub-form field not found: %s.', $form->getId(), $childrenMark, $key, $subFormId.'.'.$operandParts[1]));
                                            $resp = false;
                                        } else {
                                            if (!$fieldDef->isValidConditionalType($val)) {
                                                $this->error($io, sprintf('[%s%s] Condition not valid. Field type can not be "%s".', $form->getId(), ', '.$fieldDef->getId(), gettype($val)));
                                                $resp = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        return $resp;
    }

    /**
     * Verify that the value is a boolean representation.
     *
     * @param mixed $value Value to validate.
     *
     * @return bool
     *   True if $value is any of 'y, Y, yes, Yes, YES, n, N, no, No, NO, true,
     *   True, TRUE, false, False, FALSE, on, On, ON, off, Off, OFF'.
     *
     * @see https://yaml.org/type/bool.html
     */
    protected function isYamlBool(mixed $value): bool
    {
        if (is_bool($value)) {
            return true;
        }

        if (is_string($value)) {
            $tokens = ['y', 'Y', 'yes', 'Yes', 'YES', 'n', 'N', 'no', 'No', 'NO', 'true', 'True', 'TRUE', 'false', 'False', 'FALSE', 'on', 'On', 'ON', 'off', 'Off', 'OFF'];

            return in_array($value, $tokens);
        }

        return false;
    }

    /**
     * Verify that the value is a valid class.
     *
     * @param string $value Class name.
     *
     * @return bool use class_exists to validate the class.
     */
    protected function isValidClass(string $value): bool
    {
        return class_exists($value);
    }

    /**
     * Verify if the field name is an image field in this form.
     *
     * @param string               $fieldName Field to validate.
     * @param FormManagerInterface $form      The form.
     *
     * @return bool
     *   True if the field is defined and is an image field.
     *
     * @throws \ReflectionException
     */
    protected function isImageField(string $fieldName, FormManagerInterface $form): bool
    {
        $fields = $form->getFields();
        $isField = false;
        foreach ($fields as $field) {
            if ($field->getId() === $fieldName) {
                if ('image_reference' === $field->getFieldType()) {
                    $isField = true;
                }
                // The field is not an image field.
                break;
            }
        }

        return $isField;
    }

    /**
     * Verify if the field name is a short text field in this form.
     *
     * @param string               $fieldName Field to validate.
     * @param FormManagerInterface $form      The form.
     *
     * @return bool
     *   True if the field is defined and is a short text field.
     *
     * @throws \ReflectionException
     */
    protected function isShortTextField(string $fieldName, FormManagerInterface $form): bool
    {
        return $this->isTypeField('short_text', $fieldName, $form);
    }

    /**
     * Verify if the field name is a TAP reference field in this form.
     *
     * @param string               $fieldName Field to validate.
     * @param FormManagerInterface $form      The form.
     *
     * @return bool
     *   True if the field is defined and is a short text field.
     *
     * @throws \ReflectionException
     */
    protected function isTapReferenceField(string $fieldName, FormManagerInterface $form): bool
    {
        return $this->isTypeField('tap_reference', $fieldName, $form);
    }

    /**
     * Verify if the field name is a TAP reference field in this form.
     *
     * @param string               $type      Required field type.
     * @param string               $fieldName Field to validate.
     * @param FormManagerInterface $form      The form.
     *
     * @return bool
     *   True if the field is defined and is a short text field.
     *
     * @throws \ReflectionException
     */
    protected function isTypeField(string $type, string $fieldName, FormManagerInterface $form): bool
    {
        $fields = $form->getFields();
        $isField = false;
        foreach ($fields as $field) {
            if ($field->getId() === $fieldName) {
                if ($type === $field->getFieldType()) {
                    $isField = true;
                }
                // The field is not a valid field.
                break;
            }
        }

        return $isField;
    }

    /**
     * Verify if the field names are valid field in this form.
     *
     * @param string[]             $fieldNames Fields to validate.
     * @param FormManagerInterface $form       The form.
     *
     * @return bool
     *   True if the fields are defined and are a valid field type.
     *
     * @throws \ReflectionException
     */
    protected function isValidTableFields(array $fieldNames, FormManagerInterface $form): bool
    {
        $validFieldTypes = self::VALID_TABLE_FIELDS_TYPES;
        foreach ($fieldNames as $fieldName) {
            $field = $form->getFieldDefinition($fieldName);
            if (!$field) {
                return false;
            }
            if (!in_array($field->getFieldType(), $validFieldTypes)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get configuration file content.
     *
     * @param SplFileInfo $file
     *
     * @return array
     */
    protected function getFileContent(SplFileInfo $file): array
    {
        try {
            return Yaml::parseFile($file->getPathname());
        } catch (ParseException $e) {
            throw new ParseException(
                'Malformed inline YAML string',
                $e->getParsedLine(),
                $e->getSnippet(),
                $file->getFilename()
            );
        }
    }

    /**
     * Display warnings if it's allowed.
     *
     * @param SymfonyStyle $io
     * @param string       $msg
     *
     * @return void
     */
    protected function warning(SymfonyStyle $io, string $msg)
    {
        if ($this->displayWarnings) {
            $io->warning($msg);
        }
        $this->finishedWithWarnings = true;
    }

    /**
     * Display comments if it's allowed.
     *
     * @param SymfonyStyle $io
     * @param string       $msg
     *
     * @return void
     */
    protected function comment(SymfonyStyle $io, string $msg)
    {
        if ($this->displayWarnings) {
            $io->comment($msg);
        }
        $this->finishedWithWarnings = true;
    }

    /**
     * Display error.
     *
     * @param SymfonyStyle $io
     * @param string       $msg
     *
     * @return void
     */
    protected function error(SymfonyStyle $io, string $msg)
    {
        $io->error($msg);
        $this->finishedWithErrors = true;
    }
}
