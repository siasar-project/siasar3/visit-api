<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * List defined forms with manager type.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsListCommand extends Command
{
    protected static $defaultName = 'forms:list';
    protected static $defaultDescription = 'List defined forms with manager type.';
    protected FormFactory $formFactory;

    /**
     * Command constructor.
     *
     * @param FormFactory $formFactory Field type manager.
     * @param string|null $name        The name of the command; passing null means it must be set in configure()
     */
    public function __construct(FormFactory $formFactory, string $name = null)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
             ->addOption('fix', null, InputOption::VALUE_NONE, 'If a form is not installed, install it.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $fixInstall = $input->getOption('fix');
        $forms = $this->formFactory->findAll();

        if ($fixInstall) {
            $io->info('This command will try to install uninstalled forms.');
        }

        $rows = [];
        $formsToFix = [];
        /** @var FormManagerInterface $form */
        foreach ($forms as $formKey => $form) {
            $rows[] = [($form->isInstalled() ? 'X' : ' '), $formKey, $form->getType()];
            if (!$form->isInstalled()) {
                $formsToFix[$formKey] = $form;
            }
        }
        $io->table(['Installed', 'ID', 'Type'], $rows);
        $io->text(sprintf('%s configured forms.', count($forms)));

        if ($fixInstall) {
            foreach ($formsToFix as $formKey => $form) {
                $io->text(sprintf('Installing %s...', $formKey));
                $form->install();
            }
        }

        return command::SUCCESS;
    }
}
