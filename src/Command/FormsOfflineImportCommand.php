<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FieldTypes\Types\FormRecordReferenceFieldType;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Service\FileSystem;
use App\Service\SessionService;
use App\Tools\ImportOfflinePack;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Import offline change's pack.
 */
class FormsOfflineImportCommand extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:offline:import';
    protected static $defaultDescription = 'Import an offline change\'s pack. IMPORTANT: This command uses a database transaction.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected array $processedIds = [];
    protected Connection $connection;
    protected EntityManagerInterface|null $entityManager;
    protected SymfonyStyle $io;

    /**
     * Command constructor.
     *
     * @param string|null            $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService         $sessionService
     * @param FormFactory            $formFactory    Field type manager.
     * @param EntityManagerInterface $entityManager
     * @param Connection             $connection
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory, EntityManagerInterface $entityManager, Connection $connection)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->entityManager = $entityManager;
        $this->connection = $connection;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('file', InputArgument::REQUIRED, 'ZIP pack file.')
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Virtual session username.'
            )
            ->addUsage('imports/AdminlocalHn_2023-4-20.zip AdminlocalHn');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->io = $io;
        $file = $input->getArgument('file');
        $userName = $input->getArgument('user');

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        // $io->text(sprintf('Using "%s" user session.', $userName));

        // File mime type application/zip
        $fileSystem = new FileSystem();
        /** @var UploadedFile $file */
        $file = new UploadedFile($file, $fileSystem->getFileNameWithExtension($file));
        $this->connection->superBeginTransaction();
        try {
            $importer = new ImportOfflinePack($file, $this->entityManager);
            $importer->setErrorCallback([$io, 'error']);
            $importer->setInfoCallback([$io, 'info']);
            $importer->setWarningCallback([$io, 'warning']);
            $importer->setTextCallback([$io, 'writeln']);
            $importer->import();
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollBack();
            $io->error($e->getMessage());
            $io->info($e->getTraceAsString());

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    /**
     * Get record how array.
     *
     * @param FormRecord $record
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getRecordRaw(FormRecord $record): array
    {
        $resp = [
            'subforms' => [],
            'meta' => [
                'form' => $record->getForm()->getId(),
                'id' => $record->getId(),
            ],
            'data' => [],
        ];
        foreach ($record as $fieldName => $value) {
            if ('id' !== $fieldName) {
                $field = $record->getFieldDefinition($fieldName);
                if ($field instanceof FormRecordReferenceFieldType) {
                    if ($field->isMultivalued()) {
                        $values = $record->{$fieldName};
                        foreach ($values as $value) {
                            if ($value) {
                                if (!isset($this->processedIds[$value->getId()])) {
                                    $this->processedIds[$value->getId()] = $value->getId();
                                    $resp['subforms'][$value->getId()] = $this->getRecordRaw($value);
                                }
                            }
                        }
                    } else {
                        $value = $record->{$fieldName};
                        if (!isset($this->processedIds[$value->getId()])) {
                            $this->processedIds[$value->getId()] = $value->getId();
                            $resp['subforms'][$value->getId()] = $this->getRecordRaw($value);
                        }
                    }
                }
            }
            // Get internal data forcing load multivalued data.
            if ('id' !== $fieldName && $field->isMultivalued()) {
                $record->{$fieldName};
            }
            $raw = $record->getCurrentRaw();
            $resp['data'][$fieldName] = $raw[$fieldName];
            // Format data.
            if ('id' !== $fieldName && $field->isMultivalued()) {
                if (is_array($resp['data'][$fieldName])) {
                    foreach ($resp['data'][$fieldName] as &$item) {
                        if (is_array($item) && count($item) === 1) {
                            // Simple values must be direct.
                            $item = current($item);
                        } else {
                            // Flat data.
                            $item = $field->formatToUse($item, true);
                        }
                    }
                }
            } else {
                // Simple values must be direct.
                if (is_array($resp['data'][$fieldName]) && count($resp['data'][$fieldName]) === 1) {
                    $resp['data'][$fieldName] = current($resp['data'][$fieldName]);
                } else {
                    if ('id' !== $fieldName) {
                        // Flat data.
                        $resp['data'][$fieldName] = $field->formatToUse($resp['data'][$fieldName]);
                    }
                }
            }
        }

        return $resp;
    }
}
