<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\User;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Service\SessionService;
use App\Tools\SiasarPointWrapper;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Find form fields.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsPointIndicatorUpdate extends Command
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected static $defaultName = 'forms:point:indicator:update';
    protected static $defaultDescription = 'Update a SIASAR point indicator values.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected EntityManagerInterface $entityManager;
    protected IriConverterInterface $iriConverter;

    /**
     * Command constructor.
     *
     * @param string|null            $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService         $sessionService
     * @param FormFactory            $formFactory    Field type manager.
     * @param EntityManagerInterface $entityManager
     * @param IriConverterInterface  $iriConverter
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory, EntityManagerInterface $entityManager, IriConverterInterface $iriConverter)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->entityManager = $entityManager;
        $this->iriConverter = $iriConverter;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('point', InputArgument::REQUIRED, 'SIASAR point to update indicators.')
            ->addArgument(
                'user',
                InputArgument::OPTIONAL,
                'Administrator username.',
                'any'
            )
            ->addUsage('01GJPZP961X9VVT8J98F1GKFF1 admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $pointId = $input->getArgument('point');
        $userName = $input->getArgument('user');

        if ('any' === $userName) {
            // Find and use any admin user.
            $admins = $this->entityManager
                ->getRepository(User::class)
                ->findByRole(['ROLE_ADMIN']);
            $admin = current($admins);
            if ($admins) {
                $userName = $admin->getUsername();
            }
        }

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        $io->text(sprintf('Using "%s" user session.', $userName));

        // Search point.
        $pointsManager = $this->formFactory->find('form.point');
        $point = $pointsManager->find($pointId);
        if (!$point) {
            $io->error(sprintf('SIASAR point "%s" not found.', $pointId));

            return Command::FAILURE;
        }
        // Get all forms.
        $communities = $point->{'field_communities'};
        $wsps = $point->{'field_wsps'};
        $systems = $point->{'field_wsystems'};
        $records = array_merge($communities, $wsps, $systems);
        // Update each form indicator.
        $fail = false;
        // Reset old indicators.
        /** @var FormRecord $record */
        foreach ($records as $record) {
            if (!$record || $record->{'field_deleted'}) {
                continue;
            }
            $io->writeln(sprintf('Erasing inquiry "%s" indicators...', $record->getId()));
            $context = new PointIndicatorContext($record, $point);
            $formMetaDefinition = $record->getForm()->getMeta();
            $indicatorClass = $formMetaDefinition['indicator_class'];
            /** @var AbstractIndicator $indicator */
            $indicator = new $indicatorClass($context);
            $indicator->reset();
        }
        // Calculate new indicators.
        foreach ($records as $record) {
            if (!$record || $record->{'field_deleted'}) {
                continue;
            }
            $io->writeln(sprintf('Calculating inquiry "%s"...', $record->getId()));
            $context = new PointIndicatorContext($record, $point);
            $formMetaDefinition = $record->getForm()->getMeta();
            $indicatorClass = $formMetaDefinition['indicator_class'];
            /** @var AbstractIndicator $indicator */
            $indicator = new $indicatorClass($context);
            $value = $indicator->getValue();
            // If value < 0 then add point log and change point status to digitizing
            if ($value < 0) {
                $label = $indicator->getLabel(true);
                $metaPoint = new SiasarPointWrapper($point, $this->entityManager, $this->sessionService, $this->iriConverter);
                $errorMsg = $this->t('This point can\'t calculate indicators by [@error].', ['@error' => $label]);
                $metaPoint->getLogs()->error($errorMsg);
                $io->error($errorMsg);
                $fail = true;
                break;
            }
        }

        // If all ok, update point status to calculated.
        if (!$fail) {
            $point->{'field_status'} = 'calculated';
        } else {
            $point->{'field_status'} = 'digitizing';
        }
        $pointsManager->update($point, true);

        return command::SUCCESS;
    }
}
