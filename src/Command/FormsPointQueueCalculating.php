<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use Dtc\QueueBundle\Entity\Job;
use Dtc\QueueBundle\Manager\JobManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Find form fields.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsPointQueueCalculating extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:point:queue:calculating';
    protected static $defaultDescription = 'Search calculating SIASAR points to create queue jobs.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected JobManagerInterface $jobManager;

    /**
     * Command constructor.
     *
     * @param string|null    $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService $sessionService
     * @param FormFactory    $formFactory    Field type manager.
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->jobManager = static::getContainerInstance()->get('dtc_queue.manager.job.orm');
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addUsage('admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $userName = $input->getArgument('user');

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        $io->text(sprintf('Using "%s" user session.', $userName));

        // Search calculating status points.
        $pointManager = $this->formFactory->find('form.point');
        $points = $pointManager->findBy(['field_status' => 'calculating']);

        $queuedPointIDs = $this->getQueuedPoints();

        $ok = true;
        /** @var FormRecord $point */
        foreach ($points as $point) {
            if (!isset($queuedPointIDs[$point->getId()])) {
                $cmd = sprintf(
                    'bin/console dtc:queue:create_job Console execute forms:point:indicator:update "%s %s"',
                    $point->getId(),
                    $userName
                );
                $output = '';
                $respCode = 0;
                exec($cmd, $output, $respCode);
                $ok = $ok && (0 === $respCode);
                $io->writeln($output);
            }
        }

        if (!$ok) {
            return command::FAILURE;
        }

        return command::SUCCESS;
    }

    /**
     * Get all calculating status points queued.
     *
     * @return string[]
     */
    protected function getQueuedPoints(): array
    {
        $resp = [];
        $jobRepo = $this->jobManager->getRepository();
        $jobs = $jobRepo->findAll();

        /** @var Job $job */
        foreach ($jobs as $job) {
            $argsString = $job->getArgs();
            if ('forms:point:indicator:update' === $argsString[0]) {
                $args = explode(' ', $argsString[1]);
                $resp[$args[0]] = true;
            }
        }

        return $resp;
    }
}
