<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 * @author   Pedro Pelaez <aaaaa976@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use App\Entity\Indicator;
use App\Repository\IndicatorRepository;
use Dtc\QueueBundle\Entity\Job;
use Dtc\QueueBundle\Manager\JobManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Find form fields.
 *
 * @category Tools
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsRecalculateIndicators extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:recalculate:indicators';
    protected static $defaultDescription = 'Delete all calculated indicators and recalculate all those with "calculated" status';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected JobManagerInterface $jobManager;
    protected IndicatorRepository $indicatorRepo;

    /**
     * Command constructor.
     *
     * @param string|null    $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService $sessionService
     * @param FormFactory    $formFactory    Field type manager.
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->jobManager = static::getContainerInstance()->get('dtc_queue.manager.job.orm');
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->indicatorRepo = $this->entityManager->getRepository(Indicator::class);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addUsage('admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $userName = $input->getArgument('user');

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        $io->text(sprintf('Using "%s" user session.', $userName));

        // Search calculating status points.
        $pointManager = $this->formFactory->find('form.point');
        $count = $pointManager->countBy(['field_status' => 'calculated']);
        $ok = true;
        $io->writeln('Adding points to recalculation queue:');
        $io->progressStart($count);

        $page = 0;
        $io->progressStart($count);
        do {
            $points = $pointManager->findBy(['field_status' => 'calculated'], null, 10, $page * 10);

            /** @var FormRecord $point */
            foreach ($points as $point) {
                if (!$this->isQueuedPoint($point->getId())) {
                    $command = $this->getApplication()->find('dtc:queue:create_job');
                    $arguments = [
                        'worker_name' => 'Console',
                        'method' => 'execute',
                        'args' => [
                            'forms:point:indicator:update',
                            $point->getId(),
                            $userName,
                        ],
                    ];
                    $exportInput = new ArrayInput($arguments);
                    $returnCode = $command->run($exportInput, $output);
                    $ok = $ok && (0 === $returnCode);
                }
                $io->progressAdvance();
            }

            $page++;
        } while (count($points) > 0);
        $io->progressFinish();

        // Search validated and locked status simple queues.
        $io->writeln('Adding simple inquiries to recalculation queue:');
        $simpleFormTypes = ['form.health.care', 'form.school', 'form.tap'];
        foreach ($simpleFormTypes as $simpleFormType) {
            $ok = $ok && $this->updateSimpleFormType($io, $simpleFormType, $output);
        }

        if (!$ok) {
            return command::FAILURE;
        }

        return command::SUCCESS;
    }

    /**
     * Update simple inquiries indicators.
     *
     * @param SymfonyStyle    $io             Symfony style tool.
     * @param string          $simpleFormType Form type to process.
     * @param OutputInterface $output         Symfony output way.
     * @param string          $status         Use internal only.
     *
     * @return bool true if all inquiries processed.
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function updateSimpleFormType(SymfonyStyle $io, string $simpleFormType, OutputInterface $output, string $status = ''): bool
    {
        $ok = true;
        if (!$status) {
            $ok = $ok && $this->updateSimpleFormType($io, $simpleFormType, $output, 'validated');
            $ok = $ok && $this->updateSimpleFormType($io, $simpleFormType, $output, 'locked');

            return $ok;
        }
        $formManager = $this->formFactory->find($simpleFormType);
        $io->writeln(sprintf('Adding "%s" / "%s" inquiries to recalculation queue:', $formManager->getTitle(), $status));
        $count = $formManager->countBy(['field_status' => $status]);

        $page = 0;
        if (0 !== $count) {
            $io->progressStart($count);
        }
        do {
            $records = $formManager->findBy(['field_status' => $status], null, 10, 10 * $page);
            foreach ($records as $record) {
                if (!$this->isQueuedRecord($record->getId())) {
                    $command = $this->getApplication()->find('dtc:queue:create_job');
                    $arguments = [
                        'worker_name' => 'Console',
                        'method' => 'execute',
                        'args' => [
                            'forms:indicator:update',
                            $record->getId(),
                        ],
                    ];
                    $exportInput = new ArrayInput($arguments);
                    $returnCode = $command->run($exportInput, $output);
                    $ok = $ok && (0 === $returnCode);
                }
                $io->progressAdvance();
            }
            $page++;
        } while (count($records) > 0);
        if (0 !== $count) {
            $io->progressFinish();
        } else {
            $io->writeln('<fg=yellow>Without inquiries.</>');
        }

        return $ok;
    }

    /**
     * Is this form record queued?
     *
     * @param string $id Point ID.
     *
     * @return bool TRUE if this form record have a job to recalculate indicators.
     */
    protected function isQueuedRecord(string $id): bool
    {
        $jobRepo = $this->jobManager->getRepository(Job::class);
        $queryBuilder = $jobRepo->createQueryBuilder('j');
        $queryBuilder->select('count(j.id)')
            ->where('j.args LIKE :searchTerm')
            ->setParameter('searchTerm', '%forms:indicator:update%'.$id.'%');

        $count = $queryBuilder->getQuery()->getSingleScalarResult();

        return $count > 0;
    }

    /**
     * Is this point queued?
     *
     * @param string $id Point ID.
     *
     * @return bool TRUE if this point have a job to recalculate indicators.
     */
    protected function isQueuedPoint(string $id): bool
    {
        $jobRepo = $this->jobManager->getRepository(Job::class);
        $queryBuilder = $jobRepo->createQueryBuilder('j');
        $queryBuilder->select('count(j.id)')
            ->where('j.args LIKE :searchTerm')
            ->setParameter('searchTerm', '%forms:point:indicator:update%'.$id.'%');

        $count = $queryBuilder->getQuery()->getSingleScalarResult();

        return $count > 0;
    }

    /**
     * Get all calculating status points queued.
     *
     * @return string[]
     */
    protected function getQueuedPoints(): array
    {
        $resp = [];
        $jobRepo = $this->jobManager->getRepository();
        $jobs = $jobRepo->findAll();

        /** @var Job $job */
        foreach ($jobs as $job) {
            $argsString = $job->getArgs();
            if ('forms:point:indicator:update' === $argsString[0]) {
                $args = explode(' ', $argsString[1]);
                $resp[$args[0]] = true;
            }
        }

        return $resp;
    }
}
