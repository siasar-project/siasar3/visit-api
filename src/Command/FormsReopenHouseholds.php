<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\HouseholdProcess;
use App\Forms\FieldTypes\Types\FormRecordReferenceFieldType;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Repository\HouseholdProcessRepository;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Find form fields.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsReopenHouseholds extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'forms:reopen:households';
    protected static $defaultDescription = 'Re-open household process by community inquiry.';
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected array $processedIds = [];
    protected EntityManagerInterface|null $entityManager;

    /**
     * Command constructor.
     *
     * @param string|null            $name           The name of the command; passing null means it must be set in configure()
     * @param SessionService         $sessionService
     * @param FormFactory            $formFactory    Field type manager.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(string $name = null, SessionService $sessionService, FormFactory $formFactory, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->entityManager = $entityManager;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('record', InputArgument::REQUIRED, 'Inquiry community form record to dump.')
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addUsage('01GHGBYCFRYEXCARQDYBHPAE72 admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $recordId = $input->getArgument('record');
        $userName = $input->getArgument('user');

        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
        // $io->text(sprintf('Using "%s" user session.', $userName));

        // Search record in all inquiry forms.
        $form = $this->formFactory->find('form.community');
        $record = $form->find($recordId);

        if (!$record) {
            $io->error(sprintf('Form record "%s" not found, is it a form.community record?.', $recordId));

            return Command::FAILURE;
        }

        /** @var HouseholdProcess $process */
        $process = $record->{'field_household_process'};
        if (!$process) {
            $io->error(sprintf('Form record "%s" do not have household process.', $recordId));

            return Command::FAILURE;
        }

        /** @var HouseholdProcessRepository $processRepo */
        $processRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $process = $processRepo->find($process->getId());
        $process->setOpen(true);
        $processRepo->saveNow($process);

        return command::SUCCESS;
    }
}
