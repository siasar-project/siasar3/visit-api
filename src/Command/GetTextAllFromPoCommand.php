<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\Language;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Gettext\Generator\PoGenerator;
use Gettext\Translation;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * Import all languages from PO files.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GetTextAllFromPoCommand extends Command
{
    protected static $defaultName = 'gettext:import-all-po';
    protected static $defaultDescription = 'Import all languages from PO files.';
    protected ?LanguageRepository $languageRepository;

    /**
     * Command constructor.
     *
     * @param string|null        $name               The name of the command; passing null means it must be set in configure()
     * @param LanguageRepository $languageRepository Language repository.
     */
    public function __construct(string $name = null, LanguageRepository $languageRepository)
    {
        parent::__construct($name);
        $this->languageRepository = $languageRepository;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('folder', InputArgument::OPTIONAL, 'Folder from import', 'Translations');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     * @see https://thisinterestsme.com/php-list-all-files-in-a-directory/
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $folder = $input->getArgument('folder');
        if (substr($folder, -1, 1) !== '/') {
            $folder .= '/';
        }
        if (!is_dir($folder)) {
            $io->error(sprintf('Folder "%s" not found.', $folder));

            return Command::FAILURE;
        }

        // Get a list of file paths using the glob function.
        $fileList = glob($folder.'*.po');

        // Loop through the array that glob returned.
        $errors = [];
        $progress = $io->createProgressBar(count($fileList));
        foreach ($fileList as $filename) {
            // Simply print them out onto the screen.
            $command = $this->getApplication()->find('gettext:import-from-po');

            $arguments = [
                'file'  => $filename,
            ];

            $exportInput = new ArrayInput($arguments);
            $returnCode = $command->run($exportInput, new NullOutput());
            if ($returnCode !== $command::SUCCESS) {
                $errors[$filename] = $output;
            }
            $progress->advance();
        }
        $progress->finish();

        foreach ($errors as $id => $code) {
            $io->error('Error in '.$id);
        }
        $io->writeln('');

        if (empty($errors)) {
            return command::SUCCESS;
        }

        return command::FAILURE;
    }
}
