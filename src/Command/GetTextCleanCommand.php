<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use Doctrine\Persistence\ManagerRegistry;
use Gettext\Loader\PoLoader;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Remove all translation literals.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GetTextCleanCommand extends Command
{
    protected static $defaultName = 'gettext:clean';
    protected static $defaultDescription = 'Remove all translation literals.';
    protected ?LocaleSourceRepository $sourceRepository;
    protected ?LocaleTargetRepository $targetRepository;
    protected ?LanguageRepository $languageRepository;
    protected ManagerRegistry $entityManager;

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription);
    }

    /**
     * Command constructor.
     *
     * @param ManagerRegistry        $entityManager
     * @param LocaleSourceRepository $sourceRepository   Source repository.
     * @param LocaleTargetRepository $targetRepository   Target repository.
     * @param LanguageRepository     $languageRepository Language repository.
     * @param string|null            $name               The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, LocaleSourceRepository $sourceRepository, LocaleTargetRepository $targetRepository, LanguageRepository $languageRepository, string $name = null)
    {
        parent::__construct($name);
        $this->sourceRepository = $sourceRepository;
        $this->targetRepository = $targetRepository;
        $this->languageRepository = $languageRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $entityTypes = [
            LocaleTarget::class,
            LocaleSource::class,
        ];

        // Ask user.
        if ($input->getOption('no-interaction')) {
            $io->text('Translation literals will be erased.'.\PHP_EOL.'Continue? [y/N]');
            $io->info('Response by options: y');
        } else {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                'Translation literals will be erased.'.\PHP_EOL.'Continue? [y/N]',
                false
            );
            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
            $output->writeln('');
        }

        foreach ($entityTypes as $entityType) {
            $affectedRows = $this->entityManager->getManager()->createQuery(
                sprintf('DELETE FROM %s e', $entityType)
            )->execute();
            $io->info(sprintf('Removed %s "%s" entities.', $affectedRows, $entityType));
        }

        return command::SUCCESS;
    }
}
