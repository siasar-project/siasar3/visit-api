<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Gettext\Generator\PoGenerator;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * Export the list of defined languages.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GetTextExportSystemLanguages extends Command
{
    protected static $defaultName = 'gettext:export-languages';
    protected static $defaultDescription = 'Export the list of defined languages.';
    protected ?LocaleSourceRepository $sourceRepository;
    protected ?LocaleTargetRepository $targetRepository;
    protected ?LanguageRepository $languageRepository;

    /**
     * Command constructor.
     *
     * @param string|null            $name               The name of the command; passing null means it must be set in configure()
     * @param LocaleSourceRepository $sourceRepository   Source repository.
     * @param LocaleTargetRepository $targetRepository   Target repository.
     * @param LanguageRepository     $languageRepository Language repository.
     */
    public function __construct(string $name = null, LocaleSourceRepository $sourceRepository, LocaleTargetRepository $targetRepository, LanguageRepository $languageRepository)
    {
        parent::__construct($name);
        $this->sourceRepository = $sourceRepository;
        $this->targetRepository = $targetRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('file', InputArgument::OPTIONAL, 'File where to export', 'languages.yml');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $file = $input->getArgument('file');
        $io->info('File: '.$file);

        $languages = $this->languageRepository->findAll();
        $progress = $io->createProgressBar(count($languages));
        $output = [];
        foreach ($languages as $language) {
            $output[$language->getId()] = [
                'iso_code' => $language->getId(),
                'name' => $language->getName(),
                'ltr' => true,
                'plural_number' => $language->getPluralNumber(),
                'plural_formula' => $language->getPluralFormula(),
            ];
            $progress->advance();
        }
        $progress->finish();
        $io->writeln('');

        file_put_contents($file, Yaml::dump($output, 10, 2, Yaml::DUMP_EXCEPTION_ON_INVALID_TYPE));

        return command::SUCCESS;
    }
}
