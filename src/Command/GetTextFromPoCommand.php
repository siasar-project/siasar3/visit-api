<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Gettext\Generator\PoGenerator;
use Gettext\Loader\PoLoader;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * Import translation literals from an PO file.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GetTextFromPoCommand extends Command
{
    protected static $defaultName = 'gettext:import-from-po';
    protected static $defaultDescription = 'Import translation literals from an PO file.';
    protected ?LocaleSourceRepository $sourceRepository;
    protected ?LocaleTargetRepository $targetRepository;
    protected ?LanguageRepository $languageRepository;

    /**
     * Command constructor.
     *
     * @param string|null            $name               The name of the command; passing null means it must be set in configure()
     * @param LocaleSourceRepository $sourceRepository   Source repository.
     * @param LocaleTargetRepository $targetRepository   Target repository.
     * @param LanguageRepository     $languageRepository Language repository.
     */
    public function __construct(string $name = null, LocaleSourceRepository $sourceRepository, LocaleTargetRepository $targetRepository, LanguageRepository $languageRepository)
    {
        parent::__construct($name);
        $this->sourceRepository = $sourceRepository;
        $this->targetRepository = $targetRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('file', InputArgument::OPTIONAL, 'File from import', 'dump.po');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $file = $input->getArgument('file');

        $po = new PoLoader();
        $translations = $po->loadFile($file);

        if ('en' === $translations->getLanguage()) {
            $io->error('"en" is the base language.');

            return command::FAILURE;
        }

        $language = $this->languageRepository->findOneByField('isoCode', $translations->getLanguage());
        if (!$language) {
            $io->error(sprintf('"%s" not found', $translations->getLanguage()));

            return command::FAILURE;
        }

        $io->writeln('');
        $io->writeln('Langcode: '.$translations->getLanguage().' - '.$language->getId().' - '.$language->getName());
        $io->writeln('File: '.$file);

        $progress = $io->createProgressBar($translations->count());

        /** @var Translation $translation */
        foreach ($translations->getTranslations() as $translation) {
            $original = $translation->getOriginal();
            $trans = $translation->getTranslation();
            // By default add source.
            $source = $this->sourceRepository->findOneByMessage($original);
            if (!$source) {
                $source = $this->sourceRepository->create($original, $translation->getContext());
            }
            if (!empty($trans)) {
                $target = $this->targetRepository->findOneBy(
                    [
                        'localeSource' => $source->getId(),
                        'language' => $language->getId(),
                    ]
                );
                if (!$target) {
                    $this->targetRepository->create(
                        $language,
                        $source,
                        $trans
                    );
                } else {
                    $target->setTranslation($trans);
                }
            }
            $progress->advance();
        }
        $progress->finish();
        $io->writeln('');

        return command::SUCCESS;
    }
}
