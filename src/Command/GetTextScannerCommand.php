<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\LocaleSourceRepository;
use Gettext\Scanner\PhpScanner;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Gettext\Translations;

/**
 * Scan PHP files to find translatable strings and create it in database.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GetTextScannerCommand extends Command
{
    protected static $defaultName = 'gettext:scan';
    protected static $defaultDescription = 'Scan PHP files to find translatable strings and create it in database.';
    protected ?LocaleSourceRepository $localeSourceRepository;

    /**
     * Command constructor.
     *
     * @param string|null                 $name                   The name of the command; passing null means it must be set in configure()
     * @param LocaleSourceRepository|null $localeSourceRepository
     */
    public function __construct(string $name = null, LocaleSourceRepository $localeSourceRepository = null)
    {
        parent::__construct($name);
        $this->localeSourceRepository = $localeSourceRepository;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('folder', InputArgument::OPTIONAL, 'Folder to scan', 'src');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     * @see https://thisinterestsme.com/php-list-all-files-in-a-directory/
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $folder = $input->getArgument('folder');
        if (substr($folder, -1, 1) !== '/') {
            $folder .= '/';
        }
        if (!is_dir($folder)) {
            $io->error(sprintf('Folder "%s" not found.', $folder));

            return Command::FAILURE;
        }

        // Get a list of file paths using the glob function.
        $fileList = $this->globRecursive($folder.'*.php');

        // Loop through the array that glob returned.
        $phpScanner = new PhpScanner(Translations::create('siasar'));
        $phpScanner->setFunctions(
            array_merge(
                $phpScanner->getFunctions(),
                ['t' => 'gettext'],
            )
        );

        $phpScanner->setDefaultDomain('siasar');
        $phpScanner->extractCommentsStartingWith('i18n:', 'Translators:');
        $phpScanner->ignoreInvalidFunctions(true);

        $errors = [];
        $progress = $io->createProgressBar(count($fileList));
        foreach ($fileList as $filename) {
            // Process file.
            $phpScanner->scanFile($filename);
            $progress->advance();
        }
        $progress->finish();

        foreach ($phpScanner->getTranslations() as $domain => $translations) {
            $io->info(sprintf('[%s] Found %s strings.', $domain, count($translations)));
            $created = 0;
            /** @var Translation $translation */
            foreach ($translations as $translation) {
                $literal = $translation->getOriginal();
                $source = $this->localeSourceRepository->findOneBy(
                    [
                        'message' => $literal,
                    ]
                );
                if (!$source) {
                    $this->localeSourceRepository->create($translation->getOriginal());
                    ++$created;
                }
            }
            $io->info(sprintf('[%s] %s strings created.', $domain, $created));
        }

        foreach ($errors as $id => $code) {
            $io->error('Error in '.$id);
        }
        $io->writeln('');

        if (empty($errors)) {
            return command::SUCCESS;
        }

        return command::FAILURE;
    }

    /**
     * Recursive glob.
     *
     * Does not support flag GLOB_BRACE
     *
     * @see https://www.php.net/manual/en/function.glob.php#106595
     *
     * @param string $pattern
     * @param int    $flags
     *
     * @return array|false
     */
    protected function globRecursive(string $pattern, int $flags = 0): array|false
    {
        $files = glob($pattern, $flags);

        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
            $files = array_merge($files, $this->globRecursive($dir.'/'.basename($pattern), $flags));
        }

        return $files;
    }
}
