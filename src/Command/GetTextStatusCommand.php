<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use Doctrine\Persistence\ManagerRegistry;
use Gettext\Loader\PoLoader;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Remove all translation literals.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GetTextStatusCommand extends Command
{
    protected static $defaultName = 'gettext:status';
    protected static $defaultDescription = 'Translation literals status.';
    protected ?LocaleSourceRepository $sourceRepository;
    protected ?LocaleTargetRepository $targetRepository;
    protected ?LanguageRepository $languageRepository;
    protected ManagerRegistry $entityManager;

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription);
    }

    /**
     * Command constructor.
     *
     * @param ManagerRegistry        $entityManager
     * @param LocaleSourceRepository $sourceRepository   Source repository.
     * @param LocaleTargetRepository $targetRepository   Target repository.
     * @param LanguageRepository     $languageRepository Language repository.
     * @param string|null            $name               The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ManagerRegistry $entityManager, LocaleSourceRepository $sourceRepository, LocaleTargetRepository $targetRepository, LanguageRepository $languageRepository, string $name = null)
    {
        parent::__construct($name);
        $this->sourceRepository = $sourceRepository;
        $this->targetRepository = $targetRepository;
        $this->languageRepository = $languageRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Locale surces.
        /** @var LocaleSourceRepository $sourceRepo */
        $sourceRepo = $this->entityManager->getRepository(LocaleSource::class);
        $sources = $sourceRepo->count([]);
        $rows[] = ['Sources', $sources];

        // Languages.
        $langRepo = $this->entityManager->getRepository(Language::class);
        $languages = $langRepo->findAll();
        $rows[] = ['Languages', count($languages)];

        $io->table(['', 'Amount'], $rows);

        // Translations.
        $rows = [];
        $targetRepo = $this->entityManager->getRepository(LocaleTarget::class);
        foreach ($languages as $language) {
            if ('ENGL' !== $language->getId()) {
                $target = $targetRepo->count(['language' => $language->getId()]);
                if (0 === $sources) {
                    $translated = 0;
                } else {
                    $translated = round($target * 100 / $sources, 2);
                }
                $rows[] = [$language->getName(), $target, $translated];
            } else {
                $rows[] = [$language->getName(), $sources, 'Source'];
            }
        }

        $io->table(['', 'Amount', '%'], $rows);

        return command::SUCCESS;
    }
}
