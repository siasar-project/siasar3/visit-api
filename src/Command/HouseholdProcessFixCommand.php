<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <aaaaa976@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\HouseholdProcess;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Repository\HouseholdProcessRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Fix duplicate household processes.
 */
class HouseholdProcessFixCommand extends HouseholdProcessValidateCommand
{

    protected static $defaultName = 'hhp:fix';
    protected static $defaultDescription = 'Fix household process entities.';
    protected SymfonyStyle $io;

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $userName = $input->getArgument('user');

        $this->initSession($userName);
        $nCommunities = $this->countCommunities();
        $this->io->info(sprintf('Found %s communities with households processing...', $nCommunities));

        /** @var HouseholdProcessRepository $HHPRepo */
        $HHPRepo = $this->entityManager->getRepository(HouseholdProcess::class);

        /** @var FormManagerInterface $householdsForm */
        $householdsForm = $this->formFactory->find('form.community.household');

        $commsWithDups = 0;
        $page = 0;
        do {
            // Get a page of communities and prepare the next page index.
            $comms = $this->getCommunitiesPage($page++);
            if (count($comms) > 0) {
                /** @var FormRecord $comm */
                foreach ($comms as $comm) {
                    $dups = $this->getHHPDuplicates($comm);
                    if (count($dups) > 1) {
                        $this->infoWarn(sprintf('Processing community "%s"...', $comm->getId()));
                        $commsWithDups++;
                        // Fix duplicate.
                        // Find main household process.
                        $mainHHP = $this->getMainHHP($comm, $dups);
                        if (!$mainHHP) {
                            $this->io->error('Not main Household process found.');

                            return Command::FAILURE;
                        }
                        // For each HHP find household inquiries and move it to main HHP.
                        $nDups = count($dups);
                        foreach ($dups as $dup) {
                            if ($dup->getId() !== $mainHHP->getId()) {
                                $households = $householdsForm->findBy([
                                    'field_household_process' => $dup->getId(),
                                ]);
                                $nHouseholds = count($households);
                                if ($nHouseholds > 0) {
                                    $this->infoWarn(sprintf(
                                        'Moving %s households from process "%s" to "%s".',
                                        $nHouseholds,
                                        $dup->getId(),
                                        $mainHHP->getId()
                                    ));
                                    $this->io->progressStart($nDups);
                                    /** @var FormRecord $household */
                                    foreach ($households as $household) {
                                        $household->set(
                                            'field_household_process',
                                            [
                                                'value' => $mainHHP->getId(),
                                                'class' => HouseholdProcess::class,
                                            ]
                                        );
                                        $householdsForm->update($household, true);
                                        $this->io->progressAdvance();
                                    }
                                    $this->io->progressFinish();
                                } else {
                                    $this->infoOk(sprintf('Not households found in process "%s".', $dup->getId()));
                                }
                                $this->infoWarn(sprintf(
                                    'Deleted households process "%s".',
                                    $dup->getId()
                                ));
                                $HHPRepo->removeNow($dup);
                            }
                        }
                    } else {
                        $this->infoOk(sprintf('Community "%s" without duplicates.', $comm->getId()));
                    }
                }
            } else {
                break;
            }
        } while (true);

        $this->io->info(sprintf('Found %s communities with households process duplicates.', $commsWithDups));

        return Command::SUCCESS;
    }

    /**
     * Get main household process.
     *
     * @param FormRecord         $community Reference community.
     * @param HouseholdProcess[] $hhps      Household processes.
     *
     * @return HouseholdProcess|null
     */
    protected function getMainHHP(FormRecord $community, array $hhps): ?HouseholdProcess
    {
        $commRefHHP = $community->{'field_household_process'};
        if (!$commRefHHP) {
            $this->infoError(sprintf(
                'The community %S not have Household process associated !',
                $community->getId()
            ));

            return null;
        }
        foreach ($hhps as $hhp) {
            if ($hhp->getCommunityReference()->toBase32() !== $community->getId()) {
                $this->infoError(sprintf(
                    'Found Household process how duplicate in community %s, but process is in community %s !',
                    $community->getId(),
                    $hhp->getCommunityReference()->toBase32()
                ));

                return null;
            }
            if ($commRefHHP->getId() === $hhp->getId()) {
                return $hhp;
            }
        }
        $this->infoError(sprintf(
            'The reference Household process in community %S not found how duplicate !',
            $community->getId()
        ));

        return null;
    }

    /**
     * Display a message.
     *
     * @param string $msg Message to display.
     *
     * @return void
     */
    protected function infoWarn(string $msg): void
    {
        $this->io->writeln(sprintf('[<fg=yellow>Warning</>] %s', $msg));
    }

    /**
     * Display a message.
     *
     * @param string $msg Message to display.
     *
     * @return void
     */
    protected function infoOk(string $msg): void
    {
        $this->io->writeln(sprintf('[<fg=green>Success</>] %s', $msg));
    }

    /**
     * Display a message.
     *
     * @param string $msg Message to display.
     *
     * @return void
     */
    protected function infoError(string $msg): void
    {
        $this->io->writeln(sprintf('[<fg=red>Error  </>] %s', $msg));
    }
}
