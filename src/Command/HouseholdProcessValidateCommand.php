<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <aaaaa976@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\HouseholdProcess;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Tools\VulnerableInMemorySessionHandler;
use App\Traits\GetContainerTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Validate household process.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <aaaaa976@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class HouseholdProcessValidateCommand extends Command
{
    use GetContainerTrait;

    protected static $defaultName = 'hhp:validate';
    protected static $defaultDescription = 'Validate household process entities.';
    protected FormFactory $formFactory;
    protected ManagerRegistry $entityManager;
    protected SessionService $sessionService;

    /**
     * Command constructor.
     *
     * @param string|null     $name           The name of the command; passing null means it must be set in configure()
     * @param FormFactory     $formFactory    Field type manager.
     * @param ManagerRegistry $entityManager  Entity manager.
     * @param SessionService  $sessionService User session service.
     *  */
    public function __construct(string $name = null, FormFactory $formFactory, ManagerRegistry $entityManager, SessionService $sessionService)
    {
        parent::__construct($name);
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->sessionService = $sessionService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument(
                'user',
                InputArgument::REQUIRED,
                'Administrator username.'
            )
            ->addUsage('admin');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $userName = $input->getArgument('user');

        $this->initSession($userName);

        $nCommunities = $this->countCommunities();
        $io->info(sprintf('Found %s communities with households processing...', $nCommunities));

        $io->progressStart($nCommunities);

        $commsWithDups = 0;
        $page = 0;
        do {
            // Get a page of communities and prepare the next page index.
            $comms = $this->getCommunitiesPage($page++);
            if (count($comms) > 0) {
                foreach ($comms as $comm) {
                    $dups = $this->getHHPDuplicates($comm);
                    if (count($dups) > 1) {
                        $commsWithDups++;
                    }
                    $io->progressAdvance();
                }
            } else {
                break;
            }
        } while (true);
        $io->progressFinish();

        if ($commsWithDups <= 0) {
            $io->info(sprintf('Found %s communities with households process duplicates.', $commsWithDups));
        } else {
            $io->error(sprintf('Found %s communities with households process duplicates.', $commsWithDups));
        }

        return command::SUCCESS;
    }

    /**
     * Init user session.
     *
     * @param string $userName Username to login.
     *
     * @return void
     */
    protected function initSession(string $userName): void
    {
        // Load user session.
        // Change session handler to an in memory one.
        try {
            $handler = new VulnerableInMemorySessionHandler();
            session_set_save_handler($handler, true);
            session_start();
        } catch (\Exception $e) {
        }
        $this->sessionService->loginFakeUser($userName);
    }

    /**
     * Get a communities page.
     *
     * @param int $page Page to get.
     *
     * @return array Communities page.
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function getCommunitiesPage(int $page): array
    {
        $communities = $this->formFactory->find('form.community');

        return $communities->findBy(
            [
            'field_have_households' => true,
            ],
            [],
            10,
            $page * 10
        );
    }

    /**
     * Count communities with household process.
     *
     * @return int Amount.
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function countCommunities():int
    {
        $communities = $this->formFactory->find('form.community');

        return $communities->countBy([
            'field_have_households' => true,
        ]);
    }

    /**
     * Get all Household process linked to a community record.
     *
     * @param FormRecord $community
     *
     * @return HouseholdProcess[]
     */
    protected function getHHPDuplicates(FormRecord $community): array
    {
        $hhpRepo = $this->entityManager->getRepository(HouseholdProcess::class);

        return $hhpRepo->findBy(['communityReference' => $community->getId()]);
    }
}
