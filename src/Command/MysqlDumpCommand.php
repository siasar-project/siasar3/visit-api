<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\ConfigurationRepository;
use App\Service\ConfigurationService;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class MysqlDumpCommand extends AbstractMysqlCommandBase
{
    protected static $defaultName = 'mysql:dump';
    protected static $defaultDescription = 'Dump database. While dumping the server will be in maintenance mode.';
    protected ConfigurationRepository $configurationRepository;
    protected ParameterBagInterface $systemSettings;
    protected ConfigurationService $configurationService;

    /**
     * Command constructor.
     *
     * @param ConfigurationService $configurationService
     * @param Connection           $connection
     * @param string|null          $name                 The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ConfigurationService $configurationService, Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);

        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
        $this->configurationService = $configurationService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $prevMaintenance = $this->configurationService->inMaintenanceMode();
        $this->configurationService->setMaintenanceMode(true);

        $dbParams = $this->getDatabaseConfiguration();
        $cmd = sprintf(
            'mysqldump %s -u %s --password=%s -h %s --port=%s',
            $dbParams['dbname'],
            $dbParams['user'],
            $dbParams['password'],
            $dbParams['host'],
            $dbParams['port']
        );
        passthru($cmd);

        $this->configurationService->setMaintenanceMode($prevMaintenance);

        return Command::SUCCESS;
    }
}
