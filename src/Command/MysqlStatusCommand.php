<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\ConfigurationRepository;
use App\Service\ConfigurationService;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class MysqlStatusCommand extends AbstractMysqlCommandBase
{
    protected static $defaultName = 'mysql:status';
    protected static $defaultDescription = 'Display database status and settings.';
    protected ConfigurationRepository $configurationRepository;
    protected ConfigurationService $configurationService;
    protected ParameterBagInterface $systemSettings;

    /**
     * Command constructor.
     *
     * @param ConfigurationService $configurationService
     * @param Connection           $connection
     * @param string|null          $name                 The name of the command; passing null means it must be set in configure()
     */
    public function __construct(ConfigurationService $configurationService, Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);

        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
        $this->configurationService = $configurationService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $this->connection->connect();
        } catch (\Exception $e) {
            // Ignore connection exceptions.
        }

        $headers = [
            'Status',
            'Platform',
            'Maintenance mode',
        ];
        $rows = [
            [
                $this->connection->isConnected() ? '👌 Connected' : '👎 Offline',
                $this->connection->getDatabasePlatform()->getName(),
                $this->configurationService->inMaintenanceMode() ? '🔴 Active' : '🟢 Inactive',
            ],
        ];
        $dbParams = $this->getDatabaseConfiguration();
        foreach ($dbParams as $dbParamName => $dbParam) {
            if (!in_array($dbParamName, ['url', 'password'])) {
                $headers[] = $dbParamName;
                $rows[0][] = is_string($dbParam) ? $dbParam : print_r($dbParam, true);
            }
        }
        $headers[] = 'PHP version';
        $rows[0][] = phpversion();

        $headers[] = 'SIASAR API version';
        $rows[0][] = $this->systemSettings->get('api_platform.version');

        $headers[] = 'Symfony version';
        $rows[0][] = Kernel::VERSION;

        $headers[] = 'Symfony end of maintenance';
        $rows[0][] = $this->endTimeIcon(Kernel::END_OF_MAINTENANCE).' '.Kernel::END_OF_MAINTENANCE;

        $headers[] = 'Symfony end of life';
        $rows[0][] = $this->endTimeIcon(Kernel::END_OF_LIFE).' '.Kernel::END_OF_LIFE;

        $io->horizontalTable($headers, $rows);

        return Command::SUCCESS;
    }

    /**
     * Get an icon to alert about date limit.
     *
     * @param string $monthYear
     *
     * @return string
     */
    protected function endTimeIcon(string $monthYear): string
    {
        $date = explode('/', $monthYear);
        $now = new \DateTime();
        if ($date[1] < $now->format('Y')) {
            return '👎';
        }
        if ($date[0] < $now->format('m')) {
            return '👎';
        }

        return '👌';
    }
}
