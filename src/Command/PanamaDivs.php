<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Display file information.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class PanamaDivs extends Command
{
    protected static $defaultName = 'panama:divs';
    protected static $defaultDescription = 'Panama CSV transformer remains for reference.';

    /**
     * Command constructor.
     *
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     */
    public function __construct(string $name = null)
    {
        parent::__construct($name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        // Tests
        $sourcePath = 'web/imports/Panama/panama_divisionadministrativa_final.csv';
        $targetPath = 'web/imports/Panama/01_AdministrativeDivision_template.csv';
//        // CLI
//        $sourcePath = 'imports/Panama/panama_divisionadministrativa_final.csv';
//        $targetPath = 'imports/Panama/01_AdministrativeDivision_template.csv';
        $sourceDivs = [];
        $sourceDivs1 = [];
        $sourceDivs2 = [];
        $sourceDivs3 = [];
        $sourceDivs4 = [];
        $index = [
            "provincia" => 0,
            "codigo_provincia" => 1 ,
            "distrito" => 2 ,
            "codigo_distrito" => 3 ,
            "corregimiento" => 4,
            "codigo_corregimiento" => 5,
            "lugar_poblado" => 6,
            "codigo_lugar_poblado" => 7,
        ];
        $cnt = 0;
        if (($fh = fopen($sourcePath, "r")) !== false) {
            while (($data = fgetcsv($fh, 0, ",")) !== false) {
                $cnt++;
                if (1 === $cnt) {
                    continue;
                }
                $codigoProv = $data[$index['codigo_provincia']];
                $codigoDist = $codigoProv.$data[$index['codigo_distrito']];
                $codigoCorr = $codigoDist.$data[$index['codigo_corregimiento']];
                $codigoLugar = $codigoCorr.$data[$index['codigo_lugar_poblado']];
                // Linea $data:
                // "provincia"
                // "codigo_provincia"
                    // "distrito"
                    // "codigo_distrito"
                        // "corregimiento"
                        // "codigo_corregimiento"
                            // "lugar_poblado"
                            // "codigo_lugar_poblado"
                if (!isset($sourceDivs[$codigoProv])) {
                    $sourceDivs1[$codigoProv] = [
                        'name' => $data[$index['provincia']],
                        'code' => $codigoProv,
                        'level' => 1,
                        'parent' => 'null',
                    ];
                }
                if (!isset($sourceDivs[$codigoDist])) {
                    $sourceDivs2[$codigoDist] = [
                        'name' => $data[$index['distrito']],
                        'code' => $codigoDist,
                        'level' => 2,
                        'parent' => $codigoProv,
                    ];
                }
                if (!isset($sourceDivs[$codigoCorr])) {
                    $sourceDivs3[$codigoCorr] = [
                        'name' => $data[$index['corregimiento']],
                        'code' => $codigoCorr,
                        'level' => 3,
                        'parent' => $codigoDist,
                    ];
                }
                if (!isset($sourceDivs[$codigoLugar])) {
                    $sourceDivs4[$codigoLugar] = [
                        'name' => $data[$index['lugar_poblado']],
                        'code' => $codigoLugar,
                        'level' => 4,
                        'parent' => $codigoCorr,
                    ];
                } else {
                    $io->error(sprintf('Lugar duplicado: %s - %s', $codigoLugar, $data[$index['lugar_poblado']]));
                    $io->text('Primero:');
                    var_dump($sourceDivs[$codigoLugar]);
                    $io->text('Segundo:');
                    var_dump($data);
                }
            }
            fclose($fh);
        }
        foreach ($sourceDivs1 as $key1 => $sourceDiv1) {
            $sourceDivs[$key1] = $sourceDiv1;
        }
        foreach ($sourceDivs2 as $key2 => $sourceDiv2) {
            $sourceDivs[$key2] = $sourceDiv2;
        }
        foreach ($sourceDivs3 as $key3 => $sourceDiv3) {
            $sourceDivs[$key3] = $sourceDiv3;
        }
        foreach ($sourceDivs4 as $key4 => $sourceDiv4) {
            $sourceDivs[$key4] = $sourceDiv4;
        }
        $io->info(sprintf('Encontradas %s divisiones.', count($sourceDivs)));

        if (($fh = fopen($targetPath, "w")) !== false) {
            fputcsv($fh, ['name', 'code', 'level', 'parent']);
            foreach ($sourceDivs as $sourceDiv) {
                fputcsv($fh, $sourceDiv);
            }
            fclose($fh);
        }

        return command::SUCCESS;
    }
}
