<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class RemoteConsoleCommand extends RemoteExecCommand
{
    protected static $defaultName = 'remote:console';
    protected static $defaultDescription = 'Execute console in a remote server.';

    /**
     * Command constructor.
     *
     * @param Connection  $connection
     * @param string|null $name       The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('line', InputArgument::REQUIRED, "Command to execute.")
            ->addArgument('server', InputArgument::OPTIONAL, "Remote server");
    }

    /**
     * Build ssh command.
     *
     * @param array  $target
     * @param string $command
     *
     * @return string
     */
    protected function buildCommand(array $target, string $command): string
    {
        return parent::buildCommand($target, sprintf('%s %s', $target['paths']['console-script'], $command));
    }
}
