<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class RemoteExecCommand extends AbstractRemoteCommandBase
{
    protected static $defaultName = 'remote:exec';
    protected static $defaultDescription = 'Execute a command in a remote server.';

    /**
     * Command constructor.
     *
     * @param Connection  $connection
     * @param string|null $name       The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('line', InputArgument::REQUIRED, "Command to execute.")
            ->addArgument('server', InputArgument::OPTIONAL, "Remote server");
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $status = $this->initProcess($input, $output);
        if (command::SUCCESS !== $status) {
            return $status;
        }
        $command = $input->getArgument('line');

        $cmd = $this->buildCommand($this->targetServer, $command);
        $this->debug(sprintf('Command: %s', $cmd));
        passthru($cmd, $result);

        return $result;
    }

    /**
     * Build ssh command.
     *
     * @param array  $target
     * @param string $command
     *
     * @return string
     */
    protected function buildCommand(array $target, string $command): string
    {
        return sprintf(
            'ssh %s@%s -p %s %s \'cd %s; %s\'',
            $target['user'],
            $target['host'],
            $target['port'],
            $target['ssh']['options'],
            $target['root'],
            $command,
        );
    }
}
