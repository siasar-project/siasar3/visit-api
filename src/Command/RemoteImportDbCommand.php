<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Service\FileSystem;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class RemoteImportDbCommand extends AbstractRemoteCommandBase
{
    protected static $defaultName = 'remote:import:database';
    protected static $defaultDescription = 'Import remote database locally.';
    protected bool $exitWithError = false;

    /**
     * Command constructor.
     *
     * @param Connection  $connection
     * @param string|null $name       The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('server', InputArgument::OPTIONAL, "Remote server");
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $status = $this->initProcess($input, $output);
        if (command::SUCCESS !== $status) {
            return $status;
        }

        $processId = uniqid();
        $dumpsPath = 'var/dumps_'.$processId;
        $fileSql = $processId.'.sql';

        // Create remote temporal folder.
        $this->callConsole(
            'remote:exec',
            [
                'line' => 'mkdir -p '.$dumpsPath,
                'server' => $this->targetServer['id'],
            ]
        );
        // Dump remote database to temporal folder.
        $this->callConsole(
            'remote:console',
            [
                'line' => 'mysql:dump > '.$dumpsPath.'/'.$fileSql,
                'server' => $this->targetServer['id'],
            ]
        );
        $this->io->info(sprintf('Created database dump "%s"', $dumpsPath.'/'.$fileSql));
        // Download remote dump.
        $this->debug(sprintf('Create local directory: %s', $dumpsPath));
        @mkdir($dumpsPath);
        // rsync -e 'ssh -p 2234' -av --progress --human-readable <src> <dest>
        $cmd = sprintf(
            'rsync -e \'ssh -p %s\' -av --progress --human-readable %s@%s:%s %s',
            $this->targetServer['port'],
            $this->targetServer['user'],
            $this->targetServer['host'],
            $this->targetServer['root'].'/'.$dumpsPath.'/'.$fileSql,
            $dumpsPath.'/'.$fileSql
        );
        $this->debug($cmd);
        passthru($cmd, $result);
        $this->exitWithError = $this->exitWithError || (command::SUCCESS === $result);
        $this->io->info('Downloaded dump');
        // Clear remote dump.
        $this->callConsole(
            'remote:exec',
            [
                'line' => 'rm '.$dumpsPath.'/'.$fileSql,
                'server' => $this->targetServer['id'],
            ]
        );
        $this->callConsole(
            'remote:exec',
            [
                'line' => 'rmdir '.$dumpsPath,
                'server' => $this->targetServer['id'],
            ]
        );
        // Clear current database: console doctrine:database:drop --force
        if (!$this->input->getOption('no-interaction')) {
            if (!$this->io->confirm('The local database will be erased. Continue?')) {
                $this->io->error('User cancel');

                return command::FAILURE;
            }
        } else {
            $this->io->info('The local database will be erased. Continue? Yes');
        }

        $this->callConsole(
            'doctrine:database:drop',
            [
                '--force' => true,
            ]
        );
        $this->callConsole(
            'doctrine:database:create',
            []
        );
        // Import remote dump.
        $dbConf = $this->getDatabaseConfiguration();
        // mysql -u db -pPass -h host dbname < <file>
        $cmd = sprintf(
            'mysql -u %s -p%s -h %s --max_allowed_packet=1073741824 %s --force < %s',
            $dbConf['user'],
            $dbConf['password'],
            $dbConf['host'],
            $dbConf['dbname'],
            static::getKernelInstance()->getProjectDir().'/'.$dumpsPath.'/'.$fileSql
        );
        $this->debug($cmd);
        passthru($cmd, $result);
        $this->exitWithError = $this->exitWithError || (command::SUCCESS === $result);
        $this->io->info('Database imported');
        // Refresh connection.
        $this->connection->superClose();
        $this->connection->connect();
        // Disable maintenance mode.
        $this->callConsole(
            'configuration:maintenance:off',
            []
        );
        // Clear local import.
        FileSystem::systemFileRemove($dumpsPath);
        $this->debug(sprintf('Remove local directory: %s', $dumpsPath));

//        $status = $this->exitWithError ? command::FAILURE : Command::SUCCESS;
//        if (command::FAILURE === $status) {
//            $this->io->warning('End with error.');
//        }

        return Command::SUCCESS;
    }
}
