<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Export configuration command.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class RemoteImportFilesCommand extends AbstractRemoteCommandBase
{
    protected static $defaultName = 'remote:import:files';
    protected static $defaultDescription = 'Import remote content files locally.';
    protected bool $exitWithError = false;

    /**
     * Command constructor.
     *
     * @param Connection  $connection
     * @param string|null $name       The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Connection $connection, string $name = null)
    {
        parent::__construct($connection, $name);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('server', InputArgument::OPTIONAL, "Remote server");
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $status = $this->initProcess($input, $output);
        if (command::SUCCESS !== $status) {
            return $status;
        }

        $publicFolder = $_ENV['APP_CONFIG_PUBLIC_FILES'];

        // rsync -e 'ssh -p 2234' -av --progress --human-readable <src> <dest>
        $cmd = sprintf(
            'rsync -e \'ssh -p %s\' -av --progress --human-readable %s@%s:%s %s',
            $this->targetServer['port'],
            $this->targetServer['user'],
            $this->targetServer['host'],
            $this->targetServer['root'].'/'.$publicFolder,
            '.'
        );
        $this->debug($cmd);
        passthru($cmd, $result);
        $this->exitWithError = $this->exitWithError || (command::SUCCESS === $result);
        $this->io->info('Files downloaded');

//        $status = $this->exitWithError ? command::FAILURE : Command::SUCCESS;
//        if (command::FAILURE === $status) {
//            $this->io->warning('End with error.');
//        }

        return Command::SUCCESS;
    }
}
