<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Generate and update app secret value.
 *
 * @see https://stackoverflow.com/a/60839651/6385708
 * @see https://symfony.com/doc/current/reference/configuration/framework.html#secret
 *
 * This is a string that should be unique to your application and it's commonly used to add more entropy
 * to security related operations.
 *
 * In practice, Symfony uses this value for encrypting the cookies used in the remember me functionality
 * and for creating signed URIs when using ESI (Edge Side Includes). That's why you should treat this
 * value as if it were a sensitive credential and never make it public.
 */
#[AsCommand(
    name: 'security:regenerate-app-secret',
    description: 'Generate and update app secret value.',
)]
class SecurityRegenerateAppSecretCommand extends Command
{
    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this
            ->addArgument('file', InputArgument::OPTIONAL, '.env file to update')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Generate secret.
        $digits = '0123456789abcdef';
        $secret = '';
        for ($i = 0; $i < 32; $i++) {
            $secret .= $digits[rand(0, 15)];
        }

        // Store or display value.
        $file = $input->getArgument('file');
        if ($file) {
            $content = file_get_contents($file);
            if (stripos($content, 'APP_SECRET') === false) {
                $io->error(sprintf('The file %s not contains APP_SECRET !', $file));

                return Command::FAILURE;
            }
            shell_exec('sed -i -E "s/^APP_SECRET=.{32}$/APP_SECRET='.$secret.'/" '.$file);
            $io->success(sprintf('New APP_SECRET was generated, %s, and it was stored in %s', $secret, $file));
        } else {
            $io->success(sprintf('New APP_SECRET was generated: %s', $secret));
        }

        return Command::SUCCESS;
    }
}
