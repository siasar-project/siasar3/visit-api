<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Repository\CountryRepository;
use App\Repository\UserRepository;
use App\Service\AccessManager;
use App\Tools\Json;
use App\Tools\Siasar2Client;
use App\Tools\Tools;
use Doctrine\ORM\EntityManagerInterface;
use loophp\phptree\Exporter\Ascii;
use loophp\phptree\Node\ValueNode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Display role information.
 */
class Siasar2Command extends Command
{
    protected static $defaultName = 'siasar2:query';
    protected static $defaultDescription = 'Query SIASAR 2 server.';
    protected static array $commands = [
        'login' => 'Validate login credentials.',
        'divisions' => 'Get remote user visible administrative divisions.',
    ];
    protected Siasar2Client $client;
    protected SymfonyStyle $io;
    protected bool $onlyData;
    private CountryRepository $countryRepository;
    private EntityManagerInterface $entityManager;

    /**
     * Command constructor.
     *
     * @param Siasar2Client          $client
     * @param EntityManagerInterface $entityManager
     * @param string|null            $name          The name of the command; passing null means it must be set in configure()
     */
    public function __construct(Siasar2Client $client, EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->client = $client;
        $this->entityManager = $entityManager;
        $this->countryRepository = $entityManager->getRepository(Country::class);
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $commandDescription = <<<TEXT
Remote command to query.
Allowed commands:

TEXT;
        foreach (static::$commands as $cmd => $desc) {
            $commandDescription .= sprintf("<info>%s</info>: %s\n", $cmd, $desc);
        }
        $this->setDescription(self::$defaultDescription)
            ->addArgument('country', InputArgument::OPTIONAL, 'Country where process local data if select import.')
            ->addOption('command', 'c', InputOption::VALUE_OPTIONAL, $commandDescription, 'login')
            ->addOption('only-data', 'd', InputOption::VALUE_NEGATABLE, 'Not display information messages, only display data.', false)
            ->addOption('import', 'i', InputOption::VALUE_NONE, 'Import data to local country.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->io = $io;

        if (!boolval($_ENV['SIASAR2_API_ACTIVE'])) {
            $io->warning('SIASAR 2 integration is disabled.');

            return Command::FAILURE;
        }

        $command = strtolower($input->getOption('command'));
        $onlyData = $input->getOption('only-data');
        $this->onlyData = $onlyData;
        $mustImport = $input->getOption('import');
        $countryCode = $input->getArgument('country');
        if ($mustImport) {
            if (!$countryCode) {
                $io->error('Country is required to import data.');

                return Command::FAILURE;
            }
            $country = $this->countryRepository->find($countryCode);
            if (!$country) {
                $io->error(sprintf('Required Country, "%s", not found.', $countryCode));

                return Command::FAILURE;
            }
        }

        $this->info('Quering SIASAR 2 API...');
        $loginResponse = $this->client->login();
        switch ($command) {
            case 'login':
                if ($loginResponse->getStatusCode() === 200) {
                    $io->success('Credentials are ok.');

                    return command::SUCCESS;
                }
                $io->error(sprintf('Status code %s', $loginResponse->getStatusCode()));

                return command::FAILURE;
            case 'divisions':
                $resp = $this->client->getUserAdministrativeDivisions();

                $root = new \stdClass();
                $root->name = 'SIASAR 2 Administrative divisions';
                $root->children = $resp;

                if (!$mustImport) {
                    $this->displayEntitiesTree($root, 9999);
                } else {
                    $this->info(sprintf('Importing divisions to "%s"...', $country->getName()));
                    $io->progressStart(count($resp));
                    foreach ($resp as $nid => $node) {
                        $node->nid = $nid;
                        $this->createAdministrativeDivision($node, $country);
                        $io->progressAdvance(1);
                    }
                    $this->entityManager->flush();
                    $io->progressFinish();
                }

                return command::SUCCESS;

            default:
                $io->error(sprintf('Command "%s" not found.', $command));

                return command::FAILURE;
        }
    }

    /**
     * Create Administrative division instance from imported node.
     *
     * @param \stdClass                   $node
     * @param Country                     $country
     * @param AdministrativeDivision|null $parent
     *
     * @return void
     */
    protected function createAdministrativeDivision(\stdClass $node, Country $country, AdministrativeDivision $parent = null)
    {
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = new AdministrativeDivision();
        $division->setCountry($country);
        $division->setName($node->name);
        $division->setCode($node->code);
        $division->setOldid($node->nid);
        $division->setParent($parent);
        $divisionRepo->save($division);

        foreach ($node->children as $cid => $child) {
            $child->nid = $cid;
            $this->createAdministrativeDivision($child, $country, $division);
        }
    }

    /**
     * Render memory tree from administrative divisions instances.
     *
     * @param \stdClass $root
     * @param int       $limit
     *
     * @throws \Exception
     */
    protected function displayEntitiesTree(\stdClass $root, int $limit = 10): void
    {
        $tree = $this->treeRecursive($root, $limit);
        $exporter = new Ascii();
        $text = explode(PHP_EOL, $exporter->export($tree));
        foreach ($text as $line) {
            $this->io->text($line);
        }
    }

    /**
     * Build memory tree.
     *
     * @param \stdClass $root
     * @param int       $limit
     *
     * @return ValueNode
     *
     * @throws \Exception
     */
    protected function treeRecursive(\stdClass $root, int $limit = 10): ValueNode
    {
        $resp = new ValueNode($root->name);
        $cnt = 0;
        $max = count($root->children);
        foreach ($root->children as $administrativeDivision) {
            $resp->add($this->treeRecursive($administrativeDivision, $limit));
            $cnt++;
            if ($cnt >= $limit && $max >= $limit) {
                $resp->add(new ValueNode('...'));
                break;
            }
        }

        return $resp;
    }

    /**
     * Display message if it's allowed.
     *
     * @param $message
     *
     * @return void
     */
    protected function info($message)
    {
        if (!$this->onlyData) {
            $this->io->info($message);
        }
    }
}
