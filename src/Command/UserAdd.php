<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\LocaleTarget;
use App\Repository\CountryRepository;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Repository\UserRepository;
use App\Service\AccessManager;
use App\Service\Password\EncoderService;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Gettext\Generator\PoGenerator;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * Add a new user.
 */
class UserAdd extends UserSetPassword
{
    protected static $defaultName = 'user:add';
    protected static $defaultDescription = 'Add a new user.';

    protected ?UserRepository $userRepository;
    protected AccessManager $accessManager;
    protected ?CountryRepository $countryRepository;

    /**
     * Command constructor.
     *
     * @param string|null            $name              The name of the command; passing null means it must be set in configure()
     * @param ?UserRepository        $userRepository    User repository.
     * @param CountryRepository|null $countryRepository
     * @param EncoderService|null    $encoderService
     * @param AccessManager          $accessManager
     */
    public function __construct(string $name = null, UserRepository $userRepository = null, CountryRepository $countryRepository = null, EncoderService $encoderService = null, AccessManager $accessManager)
    {
        parent::__construct($name, $userRepository, $encoderService);
        $this->accessManager = $accessManager;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('username', InputArgument::REQUIRED, 'The new user name, must be unique.')
            ->addArgument('mail', InputArgument::REQUIRED, 'The new user mail, must be unique.')
            ->addArgument('country', InputArgument::REQUIRED, 'The new user country code.')
            ->addArgument('roles', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, 'Roles to set to user.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $username = $input->getArgument('username');
        $userMail = $input->getArgument('mail');
        $userCountryCode = $input->getArgument('country');
        $userRoles = $input->getArgument('roles');
        $password = current($this->randomPassword(16, 1, "lower_case,upper_case,numbers"));

        $userCountry = $this->countryRepository->find($userCountryCode);
        if (!$userCountry) {
            $io->error(
                sprintf(
                    'Invalid country code "%s".',
                    $userCountryCode
                )
            );

            return command::INVALID;
        }

        $systemRoles = array_keys($this->accessManager->getSystemRoles());
        $badRoles = [];
        foreach ($userRoles as &$userRole) {
            $userRole = strtoupper($userRole);
            if (!in_array($userRole, $systemRoles)) {
                $badRoles[] = $userRole;
            }
        }
        if (count($badRoles) > 0) {
            $io->error(
                sprintf(
                    'Found not valid roles: "%s". Valid roles are: "%s"',
                    implode(', ', $badRoles),
                    implode(', ', $systemRoles)
                )
            );

            return command::INVALID;
        }

        $user = $this->userRepository->create($username, $userMail, $password, $userCountry);
        $user->setRoles($userRoles);
        $this->userRepository->saveNow($user);

        $io->success(
            sprintf(
                'New user "%s" created with password "%s".',
                $input->getArgument('username'),
                $password
            )
        );

        return command::SUCCESS;
    }
}
