<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Country;
use App\Repository\CountryRepository;
use App\Repository\UserRepository;
use App\Service\AccessManager;
use App\Service\Password\EncoderService;
use App\Traits\GetContainerTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Generate dummy users.
 */
class UserGenerateDummy extends UserSetPassword
{
    use GetContainerTrait;

    protected static $defaultName = 'user:generate:dummies';
    protected static $defaultDescription = 'Generate dummy users.';

    protected ?UserRepository $userRepository;
    protected AccessManager $accessManager;
    protected ?CountryRepository $countryRepository;
    protected ManagerRegistry $entityManager;
    protected ParameterBagInterface $systemSettings;

    /**
     * Command constructor.
     *
     * @param string|null            $name              The name of the command; passing null means it must be set in configure()
     * @param ?UserRepository        $userRepository    User repository.
     * @param CountryRepository|null $countryRepository
     * @param EncoderService|null    $encoderService
     * @param AccessManager          $accessManager
     * @param ManagerRegistry        $entityManager
     */
    public function __construct(string $name = null, UserRepository $userRepository = null, CountryRepository $countryRepository = null, EncoderService $encoderService = null, AccessManager $accessManager, ManagerRegistry $entityManager)
    {
        parent::__construct($name, $userRepository, $encoderService);
        $this->accessManager = $accessManager;
        $this->countryRepository = $countryRepository;
        $this->entityManager = $entityManager;
        $this->systemSettings = self::getContainerInstance()->getParameterBag();
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addOption(
                'password',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Generate users with this password',
                'generatedPassword'
            )
            ->addOption(
                'mail',
                'm',
                InputOption::VALUE_OPTIONAL,
                'Generate users with this base email, this generate mails like "user+AdminNone@example.com"',
                'user@example.com'
            )
            ->addOption(
                'clear',
                'c',
                InputOption::VALUE_NEGATABLE,
                'Remove current users registered in the platform',
                false
            )
            ->addArgument(
                'country',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'Generate users to this country code, use "none" to generate users without country.',
                ['all']
            );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        if (Command::FAILURE === $this->validateEnvironment($io)) {
            return Command::FAILURE;
        }

        $countryCodes = $input->getArgument('country');
        $password = $input->getOption('password');
        $clearUsers = boolval($input->getOption('clear'));
        $mail = $input->getOption('mail');
        $baseMail = explode('@', $mail);

        $countryRepository = $this->entityManager->getRepository(Country::class);
        if (in_array('all', $countryCodes)) {
            $haveNone = in_array('none', $countryCodes);
            $countries = $countryRepository->findAll();
            $countryCodes = [];
            /** @var Country $country */
            foreach ($countries as $country) {
                $countryCodes[$country->getId()] = $country->getId();
            }
            if ($haveNone) {
                $countryCodes['none'] = 'none';
            }
        }

        // Validate country codes.
        $countryList = [];
        foreach ($countryCodes as $countryCode) {
            if ('none' !== $countryCode) {
                $country = $countryRepository->find($countryCode);
                if (!$country) {
                    $io->error(sprintf('Not valid country: "%s"', $countryCode));

                    return Command::INVALID;
                }
                $countryList[] = [$countryCode => $country->getName()];
            } else {
                $countryList[] = ['none' => 'Without country'];
            }
        }

        $io->title('Generate users to:');
        call_user_func_array([$io, 'definitionList'], $countryList);

        // Prepare roles.
        $roles = $this->accessManager->getSystemRoles();
        $rolesKeys = array_keys($this->accessManager->getSystemRoles());

        $roleList = [];
        foreach ($rolesKeys as $rolesKey) {
            $roleList[] = [$rolesKey => $roles[$rolesKey]['label']];
        }
        $io->title('Generate users with roles:');
        call_user_func_array([$io, 'definitionList'], $roleList);

        $io->info(sprintf('Using password "%s" for generated users.', $password));

        // Clear old users.
        if ($clearUsers) {
            $users = $this->userRepository->findAll();
            foreach ($users as $user) {
                $this->userRepository->remove($user);
            }
            $this->entityManager->getManager()->flush();
        }

        // Process.
        $io->progressStart(count($countryCodes)*(count($rolesKeys)-1));
        foreach ($countryCodes as $countryCode) {
            $country = $countryRepository->find($countryCode);
            $divisions = $country->getAdministrativeDivisions()->toArray();
            $countDivisions = count($divisions)-1;
            foreach ($rolesKeys as $rolesKey) {
                if ('ROLE_ADMIN' === $rolesKey) {
                    continue;
                }
                $roleSuffix = ucfirst(strtolower(str_replace('_', '', str_replace('ROLE_', '', $rolesKey))));
                $userName = sprintf('%s%s', $roleSuffix, ucfirst($countryCode));
                $userMail = $baseMail[0].'+'.strtolower($userName).'@'.$baseMail[1];

                $user = $this->userRepository->create($userName, $userMail, $password, $country);
                if ('ROLE_USER' !== $rolesKey) {
                    if ('ROLE_READER' !== $rolesKey) {
                        // Add reader role to all users that are not ROLE_USER, not ROLE_ADMIN, not ROLE_READER.
                        $user->setRoles([$rolesKey, 'ROLE_READER']);
                    } else {
                        $user->setRoles([$rolesKey]);
                    }
                }
                if ($countDivisions > 0) {
                    $user->addAdministrativeDivision($divisions[random_int(0, $countDivisions)]);
                }
                $this->userRepository->saveNow($user);

                $io->progressAdvance();
            }
        }
        $io->progressFinish();
        // Create admin user.
        $country = $countryRepository->find(reset($countryCodes));
        $user = $this->userRepository->create('Admin', $mail, $password, $country);
        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
        $this->userRepository->saveNow($user);

        return command::SUCCESS;
    }

    /**
     * Validate current environment.
     *
     * @param SymfonyStyle $io
     *
     * @return int|void
     */
    protected function validateEnvironment(SymfonyStyle $io)
    {
        $environment = $this->systemSettings->get('kernel.environment');
        $io->info('Environment: '.$environment);
        if ('prod' === $environment) {
            $io->error('This command can\'t run in production mode.');

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
