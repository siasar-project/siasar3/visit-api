<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Get authentication token.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserGetToken extends Command
{
    protected static $defaultName = 'user:token';
    protected static $defaultDescription = 'Get authentication token.';
    protected ?UserRepository $userRepository;
    private ?JWTTokenManagerInterface $tokenManager;

    /**
     * Command constructor.
     *
     * @param string|null               $name           The name of the command; passing null means it must be set in configure()
     * @param ?UserRepository           $userRepository User repository.
     * @param ?JWTTokenManagerInterface $tokenManager   Token manager.
     */
    public function __construct(string $name = null, UserRepository $userRepository = null, JWTTokenManagerInterface $tokenManager = null)
    {
        parent::__construct($name);
        $this->userRepository = $userRepository;
        $this->tokenManager = $tokenManager;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('username', InputArgument::REQUIRED, 'The user username to take token.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $user = $this->userRepository->findOneByUserName($input->getArgument('username'));
        if (!$user) {
            $io->error('User not found');

            return command::FAILURE;
        }

        $token = $this->tokenManager->create($user);
        $io->text('Token:');
        $io->text($token);

        return command::SUCCESS;
    }
}
