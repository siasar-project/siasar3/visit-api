<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Repository\UserRepository;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Gettext\Generator\PoGenerator;
use Gettext\Translation;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * List users with ROLE_ADMIN.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserListAdmins extends Command
{
    protected static $defaultName = 'user:list';
    protected static $defaultDescription = 'List users by role.';
    protected ?UserRepository $userRepository;

    /**
     * Command constructor.
     *
     * @param string|null     $name           The name of the command; passing null means it must be set in configure()
     * @param ?UserRepository $userRepository User repository.
     */
    public function __construct(string $name = null, UserRepository $userRepository = null)
    {
        parent::__construct($name);
        $this->userRepository = $userRepository;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('roles', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, 'Roles to display.', ['ROLE_ADMIN']);
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $roles = $input->getArgument('roles');

        $users = $this->userRepository->findByRole($roles);
        $rows = [];
        foreach ($users as $user) {
            $rows[] = [
                $user->getCreated()->format('Y-m-d H:i:s'),
                $user->getId(),
                $user->getUsername(),
                $user->getEmail(),
                implode(', ', $user->getRoles()),
                $user->getLanguage()?->getId().' - '.$user->getLanguage()?->getName(),
                $user->getCountry()?->getId().' - '.$user->getCountry()?->getName(),
            ];
        }
        $io->table(
            [
                'Created',
                'Id',
                'Username',
                'Email',
                'Roles',
                'Language',
                'Country',
            ],
            $rows
        );

        return command::SUCCESS;
    }
}
