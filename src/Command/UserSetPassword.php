<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Repository\UserRepository;
use App\Service\Password\EncoderService;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Set user password.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserSetPassword extends Command
{
    protected static $defaultName = 'user:password';
    protected static $defaultDescription = 'Set user password.';
    protected ?UserRepository $userRepository;
    protected ?EncoderService $encoderService;

    /**
     * Command constructor.
     *
     * @param string|null         $name           The name of the command; passing null means it must be set in configure()
     * @param UserRepository|null $userRepository User repository.
     * @param EncoderService|null $encoderService Password encoder.
     */
    public function __construct(string $name = null, UserRepository $userRepository = null, EncoderService $encoderService = null)
    {
        parent::__construct($name);
        $this->userRepository = $userRepository;
        $this->encoderService = $encoderService;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('username', InputArgument::REQUIRED, 'The user username to take token.')
            ->addArgument('password', InputArgument::OPTIONAL, 'The new password. If not defined generate one.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $user = $this->userRepository->findOneByUserName($input->getArgument('username'));
        if (!$user) {
            $io->error('User not found');

            return command::FAILURE;
        }

        $password = current($this->randomPassword(16, 1, "lower_case,upper_case,numbers"));
        if ($input->getArgument('password')) {
            $password = $input->getArgument('password');
        }

        $user->setPassword($this->encoderService->generateEncodedPassword($user, $password));
        $this->userRepository->saveNow($user);

        $io->success(sprintf('New password "%s" to "%s".', $password, $input->getArgument('username')));

        return command::SUCCESS;
    }

    /**
     * Generate password.
     *
     * @param int    $length
     *  The length of the generated password.
     * @param int    $count
     *  Number of passwords to be generated.
     * @param string $characters
     *  Char types: lower_case, upper_case, numbers, special_symbols.
     *
     * @return array
     *
     * @see https://www.phpjabbers.com/generate-a-random-password-with-php-php70.html
     */
    protected function randomPassword(int $length, int $count, string $characters): array
    {

        // $length - the length of the generated password
        // $count - number of passwords to be generated
        // $characters - types of characters to be used in the password

        // define variables used within the function
        $symbols = [];
        $passwords = [];
        $usedSymbols = '';
        $pass = '';

        // an array of different character types
        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = '!?~@#-_+<>[]{}';

        $characters = explode(",", $characters); // get characters types to be used for the passsword
        foreach ($characters as $key => $value) {
            $usedSymbols .= $symbols[$value]; // build a string with all characters
        }
        $symbolsLength = strlen($usedSymbols) - 1; //strlen starts from 0 so to get number of characters deduct 1

        for ($p = 0; $p < $count; $p++) {
            $pass = '';
            for ($i = 0; $i < $length; $i++) {
                $n = rand(0, $symbolsLength); // get a random character from the string with all characters
                $pass .= $usedSymbols[$n]; // add the character to the password string
            }
            $passwords[] = $pass;
        }

        return $passwords; // return the generated password
    }
}
