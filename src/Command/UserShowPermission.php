<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Repository\UserRepository;
use App\Service\AccessManager;
use App\Service\Password\EncoderService;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Doctrine\DBAL\Schema\Table;
use Gettext\Generator\PoGenerator;
use Gettext\Translation;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableCellStyle;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * Display permission information.
 */
class UserShowPermission extends UserSetPassword
{
    protected static $defaultName = 'user:show:permission';
    protected static $defaultDescription = 'Display permission information.';
    protected ?UserRepository $userRepository;
    private AccessManager $accessManager;

    /**
     * Command constructor.
     *
     * @param string|null         $name           The name of the command; passing null means it must be set in configure()
     * @param ?UserRepository     $userRepository User repository.
     * @param EncoderService|null $encoderService
     * @param AccessManager       $accessManager
     */
    public function __construct(string $name = null, UserRepository $userRepository = null, EncoderService $encoderService = null, AccessManager $accessManager)
    {
        parent::__construct($name, $userRepository, $encoderService);
        $this->accessManager = $accessManager;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('permissions', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, 'Permissions to display.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $permissions = $input->getArgument('permissions');

        $systemPermissions = $this->accessManager->getSystemPermissions();
        if (empty($permissions)) {
            $permissions = array_keys($systemPermissions);
        }

        $badPermissions = [];
        foreach ($permissions as &$permission) {
            $permission = strtolower($permission);
            if (!in_array($permission, array_keys($systemPermissions))) {
                $badPermissions[] = $permission;
            }
        }
        if (count($badPermissions) > 0) {
            $io->error(
                sprintf(
                    'Permissions not found: "%s".',
                    implode(', ', $badPermissions)
                )
            );

            return command::INVALID;
        }

        foreach ($permissions as $key) {
            $item = [];
            foreach ($systemPermissions[$key] as $id => $value) {
                if (is_bool($value)) {
                    $item[] = [
                        $id,
                        new TableCell(
                            $value,
                            [
                                'style' => new TableCellStyle(
                                    [
                                        'cellFormat' => ($value ? '<bg=red;options=bold>          </>' : '<bg=green;options=bold>          </>'),
                                    ]
                                ),
                            ],
                        ),
                    ];
                } else {
                    $item[] = [$id, $value];
                }
            }
            $io->title($key);
            $io->table(['Key', 'Value'], $item);
        }

        return command::SUCCESS;
    }
}
