<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Command;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Repository\UserRepository;
use App\Service\AccessManager;
use App\Service\Password\EncoderService;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Doctrine\DBAL\Schema\Table;
use Gettext\Generator\PoGenerator;
use Gettext\Translation;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableCellStyle;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Command\Event\ConfigurationPostCreateEvent;
use App\Command\Event\ConfigurationPostDeleteEvent;
use App\Command\Event\ConfigurationPostUpdateEvent;
use App\Command\Event\ConfigurationPreCreateEvent;
use App\Command\Event\ConfigurationPreDeleteEvent;
use App\Command\Event\ConfigurationPreUpdateEvent;
use App\Entity\Configuration\ConfigurationRead;
use App\Repository\ConfigurationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Gettext\Translations;

/**
 * Display role information.
 */
class UserShowRole extends UserSetPassword
{
    protected static $defaultName = 'user:show:role';
    protected static $defaultDescription = 'Display role information.';
    protected ?UserRepository $userRepository;
    private AccessManager $accessManager;

    /**
     * Command constructor.
     *
     * @param string|null         $name           The name of the command; passing null means it must be set in configure()
     * @param ?UserRepository     $userRepository User repository.
     * @param EncoderService|null $encoderService
     * @param AccessManager       $accessManager
     */
    public function __construct(string $name = null, UserRepository $userRepository = null, EncoderService $encoderService = null, AccessManager $accessManager)
    {
        parent::__construct($name, $userRepository, $encoderService);
        $this->accessManager = $accessManager;
    }

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('roles', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, 'Roles to display.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface  $input  Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return int 0 if everything went fine, or an exit code
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $userRoles = $input->getArgument('roles');

        $systemRoles = $this->accessManager->getSystemRoles();
        if (empty($userRoles)) {
            $userRoles = array_keys($systemRoles);
        }

        $badRoles = [];
        foreach ($userRoles as &$userRole) {
            $userRole = strtoupper($userRole);
            if (!in_array($userRole, array_keys($systemRoles))) {
                $badRoles[] = $userRole;
            }
        }
        if (count($badRoles) > 0) {
            $io->error(
                sprintf(
                    'Roles not found: "%s". Valid roles are: "%s".',
                    implode(', ', $badRoles),
                    implode(', ', array_keys($systemRoles))
                )
            );

            return command::INVALID;
        }

        foreach ($userRoles as &$userRole) {
            $item = [
                ['Label', $systemRoles[$userRole]['label']],
                ['description', $systemRoles[$userRole]['description']],
            ];
            $permissions = $this->accessManager->getPermissionsByRoles([$userRole]);
            $first = true;
            foreach ($permissions as $permission) {
                if ($first) {
                    $item[] = ['Permissions', $permission];
                    $first = false;
                } else {
                    $item[] = ['', $permission];
                }
            }
            $io->title($userRole);
            $io->table(['Property', 'Value'], $item);
        }

        return command::SUCCESS;
    }
}
