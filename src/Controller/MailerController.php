<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Controller;

use App\Traits\StringTranslationTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Test mail controller.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @deprecated This is a development artifact. Not acceptable to production sites.
 */
class MailerController extends AbstractController
{
    use StringTranslationTrait;

    /**
     * Send user email to test mailer.
     *
     * @param MailerInterface $mailer Mailer service.
     *
     * @return Response
     *
     * @Route("/email")
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @throws TransportExceptionInterface
     *
     * @see config/packages/security.yaml
     */
    public function sendEmail(MailerInterface $mailer): Response
    {
        $email = (new TemplatedEmail())
            ->from('hello@example.com')
            ->to('you@example.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject($this->t('Time for Symfony Mailer!'))
            ->htmlTemplate('emails/example.html.twig')
            ->context(
                [
                    'expiration_date' => new \DateTime('+7 days'),
                    'username' => 'username',
                ]
            );

        $mailer->send($email);

        return new Response();
    }
}
