<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Cron;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use VM\Cron\JobInterface;

/**
 * Find calculating status points and create jobs if no exist.
 */
class CreatePointCalcJobsCron implements JobInterface
{
    protected ?Application $application;
    private EntityManagerInterface $entityManager;

    /**
     * @param KernelInterface        $kernel
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(KernelInterface $kernel, EntityManagerInterface $entityManager)
    {
        $this->application = new Application($kernel);
        $this->application->setAutoExit(false);
        $this->entityManager = $entityManager;
    }

    /**
     * Execute job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function run(): void
    {
        // Find a ROLE_ADMIN user to use how owner.
        /** @var UserRepository $userRepo */
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = current($userRepo->findByRole(['ROLE_ADMIN']));
        if ($user) {
            // Execute the console command.
            $input = new ArrayInput([
                'command' => 'forms:point:queue:calculating',
                'user' => $user->getUsername(),
            ]);

            // You can use NullOutput() if you don't need the output
            $output = new NullOutput();
            $this->application->run($input, $output);
        }
    }
}
