<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Cron;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use VM\Cron\JobInterface;

/**
 * Run dtc prune jobs.
 */
class RemoveOrphanFilesCron implements JobInterface
{
    protected ?Application $application;

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->application = new Application($kernel);
        $this->application->setAutoExit(false);
    }

    /**
     * Execute job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function run(): void
    {
        $inputs = [
            // Delete orphan files.
            new ArrayInput([
                'command' => 'file:remove:orphan',
                '--yes' => true,
            ]),
        ];

        foreach ($inputs as $input) {
            // You can use NullOutput() if you don't need the output
            $output = new NullOutput();
            $this->application->run($input, $output);
        }
    }
}
