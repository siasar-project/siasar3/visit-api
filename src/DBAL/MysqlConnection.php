<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DBAL;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Exception;

/**
 * Connection wrapper to reduce MySQL connection usage.
 */
class MysqlConnection extends Connection
{
    protected static $connection;

    /**
     * Initializes a new instance of the Connection class.
     *
     * @internal The connection can be only instantiated by the driver manager.
     *
     * @param array<string,mixed> $params       The connection parameters.
     * @param Driver              $driver       The driver to use.
     * @param Configuration|null  $config       The configuration, optional.
     * @param EventManager|null   $eventManager The event manager, optional.
     *
     * @psalm-param Params $params
     *
     * @phpstan-param array<string,mixed> $params
     *
     * @throws Exception
     */
    public function __construct(array $params, Driver $driver, ?Configuration $config = null, ?EventManager $eventManager = null)
    {
        parent::__construct($params, $driver, $config, $eventManager);
    }

    /**
     * Close connection before destroy this instance.
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * Close connection.
     *
     * @return void
     */
    public function close()
    {
        if (isset($_ENV['TEST_ENVIRONMENT']) && $_ENV['TEST_ENVIRONMENT']) {
            return;
        }

        if ($this->isConnected()) {
            try {
                //Kill the connection with a KILL Statement.
                $this->_conn->query('KILL CONNECTION CONNECTION_ID()');
            } catch (\Exception $e) {
            }
        }
        // Set the PDO object to NULL.
        parent::close();
        // Try to clean memory to free connection.
        gc_collect_cycles();
    }

    /**
     * Close connection.
     *
     * @return void
     */
    public function superClose()
    {
        static::$connection = null;
    }

    /**
     * Establishes the connection with the database.
     *
     * @return bool TRUE if the connection was successfully established, FALSE if
     *              the connection is already open.
     */
    public function connect(): bool
    {
//        if (isset($_ENV['TEST_ENVIRONMENT']) && $_ENV['TEST_ENVIRONMENT']) {
//            parent::connect();
//
//            return true;
//        }

        if (null === static::$connection) {
            parent::connect();
            static::$connection = $this->_conn;
        }

        $this->_conn = static::$connection;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function superBeginTransaction()
    {
        if (isset($_ENV['TEST_ENVIRONMENT']) && $_ENV['TEST_ENVIRONMENT']) {
            return;
        }

        parent::beginTransaction();
    }

    /**
     * {@inheritDoc}
     *
     * @throws ConnectionException If the commit failed due to no active transaction or
     *                                            because the transaction was marked for rollback only.
     */
    public function superCommit()
    {
        if (isset($_ENV['TEST_ENVIRONMENT']) && $_ENV['TEST_ENVIRONMENT']) {
            return;
        }

        parent::commit();
    }

    /**
     * Cancels any database changes done during the current transaction.
     *
     * @return bool
     *
     * @throws ConnectionException If the rollback operation failed.
     */
    public function superRollBack()
    {
        if (isset($_ENV['TEST_ENVIRONMENT']) && $_ENV['TEST_ENVIRONMENT']) {
            return true;
        }

        return parent::rollBack();
    }

        /**
         * {@inheritDoc}
         */
    public function beginTransaction()
    {
        // Nothing to do.
    }

    /**
     * {@inheritDoc}
     *
     * @throws ConnectionException If the commit failed due to no active transaction or
     *                                            because the transaction was marked for rollback only.
     */
    public function commit()
    {
        // Nothing to do.
    }

    /**
     * Cancels any database changes done during the current transaction.
     *
     * @return bool
     *
     * @throws ConnectionException If the rollback operation failed.
     */
    public function rollBack()
    {
        // Nothing to do.
        return true;
    }
}
