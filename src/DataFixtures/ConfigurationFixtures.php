<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataFixtures;

use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationWrite;
use App\Entity\User;
use App\Repository\ConfigurationRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Configuration fixtures.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationFixtures extends Fixture
{
    protected ConfigurationRepository $configurationRepository;

    /**
     * ConfigurationFixtures constructor.
     *
     * @param ConfigurationRepository $configurationRepository Configuration repository.
     */
    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * Load predefined users.
     *
     * @param ObjectManager $manager Object manager.
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
    }
}
