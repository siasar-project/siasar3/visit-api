<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataFixtures;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\ChangedFormManager;
use App\Forms\FormFactory;
use App\Forms\MailFormManager;
use App\Forms\PointFormManager;
use App\Repository\ConfigurationRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Form fixtures.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormFixtures extends Fixture
{
    protected ?FormFactory $forms;
    protected ?ConfigurationRepository $configurationRepository;
    protected FieldTypeManager $fieldTypeManager;
    protected Connection $connection;

    /**
     * Forms Fixtures constructor.
     *
     * @param Connection              $connection
     * @param FormFactory             $forms                   Forms factory.
     * @param ConfigurationRepository $configurationRepository Configuration repository.
     * @param FieldTypeManager        $fieldTypeManager        Form field type manager.
     */
    public function __construct(Connection $connection, FormFactory $forms, ConfigurationRepository $configurationRepository, FieldTypeManager $fieldTypeManager)
    {
        $this->forms = $forms;
        $this->configurationRepository = $configurationRepository;
        $this->fieldTypeManager = $fieldTypeManager;
        $this->connection = $connection;
    }

    /**
     * Load predefined forms.
     *
     * @param ObjectManager $manager Object manager.
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        // Since forms require change database tables, only must be used in fixtures
        // to create the configuration export, or this will generate a
        // "There is no active transaction" error.
        $this->connection->superBeginTransaction();
        try {
            // $this->createSchoolsQuestionnaire();
            // $this->generateAllFieldTypesForm();
            // $this->generateSiasarPointForm();
//            $this->generateMailForm();
            $this->connection->superCommit();
        } catch (\Exception $e) {
            $this->connection->superRollback();
            throw $e;
        }
    }

    /**
     * Generate mail form.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function generateMailForm(): void
    {
        $formSchools = $this->forms->create('mail', MailFormManager::class);
        $formSchools->setTitle('Mail');
        $formSchools->setDescription('Internal mail system.');
        $formSchools->saveNow();
        $formSchools->install();
    }

    /**
     * Generate SIASAR Point form.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function generateSiasarPointForm(): void
    {
        $formSchools = $this->forms->create('point', PointFormManager::class);
        $formSchools->setTitle('SIASAR Point');
        $formSchools->setDescription('Community, Water Supply System, and Water Service Provider formulary groups.');
        $formSchools->saveNow();
        $formSchools->install();
    }

    /**
     * Generate a dummy form with all defined field types.
     */
    protected function generateAllFieldTypesForm(): void
    {
        $form = $this->forms->create('all.field.type');
        $form->setTitle('Dummy form');
        $form->setDescription('Example form structure with all field types, in single and multivalued variations.');
        $form->setMeta(
            [
                'title' => 'Dummy form',
                'version' => '0.1',
                'field_groups' => [],
            ]
        );
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        $order = 0;
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            // Single field.
            ++$order;
            $options = [
                'weight' => $order,
                'meta' => [
                    'catalog_id' => 's_'.$i,
                ],
            ];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Single field '.$fieldTypeId, $options);
            // Multivalued field.
            ++$order;
            $options = [
                'weight' => $order,
                'multivalued' => true,
                'meta' => [
                    'catalog_id' => 'm_'.$i,
                ],
            ];
            // Set required field options.
            $continueFor = false;
            switch ($fieldTypeId) {
                case 'publishing_status':
                    $continueFor = true;
                    break;
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
            }
            //
            ++$i;
            if ($continueFor) {
                continue;
            }
            $form->addField($fieldTypeId, 'm_'.$fieldTypeId, 'Multivalued field '.$fieldTypeId, $options);
        }
        $form->saveNow();
        $form->install();
    }

    /**
     * Create school form.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function createSchoolsQuestionnaire(): void
    {
        $formSchools = $this->forms->create('school', ChangedFormManager::class);
        $formSchools->setTitle('Schools Questionnaire');
        $formSchools->setDescription('Collected Schools information.');
        $formSchools->setMeta(
            [
                'title' => 'Schools Questionnaire',
                'version' => 'Version 12.4 – April 2021',
                'field_groups' => [
                    '0' => [
                        'id' => '0',
                        'title' => 'QUESTIONNAIRE DATA',
                    ],
                    'A' => [
                        'id' => 'A',
                        'title' => 'GENERAL INFORMATION',
                        'children' => [
                            'A.1' => [
                                'id' => 'A.1',
                                'title' => 'Location of the school',
                            ],
                            'A.5' => [
                                'id' => 'A.5',
                                'title' => 'Teaching and administrative staff',
                            ],
                            'A.6' => [
                                'id' => 'A.6',
                                'title' => 'Student population',
                            ],
                        ],
                    ],
                    'B' => [
                        'id' => 'B',
                        'title' => 'WATER SUPPLY',
                    ],
                    'C' => [
                        'id' => 'C',
                        'title' => 'SANITATION',
                        'children' => [
                            'C.3' => [
                                'id' => 'C.3',
                                'title' => 'How many toilets / latrines are at the school?',
                                'help' => 'it’s recommended to involve, to the extent possible, the teacher to help with the answer.',
                            ],
                        ],
                    ],
                    'D' => [
                        'id' => 'D',
                        'title' => 'HYGIENE',
                    ],
                ],
            ]
        );
        // Group 0.
        $formSchools->addField(
            'date',
            'questionnaire_date',
            'Date',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => '0.1',
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'questionnaire_interviewer',
            'Interviewer',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => '0.2',
                ],
            ]
        );
        $formSchools->addField(
            'form_record_reference',
            'questionnaire_contacts',
            'Interviewee(s)/contacts',
            [
                'multivalued' => true,
                'meta' => [
                    'catalog_id' => '0.3',
                    'subform' => 'school.contact',
                ],
            ]
        );
        $this->createSchoolsContacts();

        // Group A.
        $formSchools->addField(
            'decimal',
            'location_lat',
            'Latitude',
            [
                'required' => true,
                'precision' => 12,
                'scale' => 8,
                'meta' => [
                    'catalog_id' => 'A.1.1',
                ],
            ]
        )
            ->setDescription('Decimal degrees');
        $formSchools->addField(
            'decimal',
            'location_lon',
            'Longitude',
            [
                'required' => true,
                'precision' => 12,
                'scale' => 8,
                'meta' => [
                    'catalog_id' => 'A.1.2',
                ],
            ]
        )
            ->setDescription('Decimal degrees');
        // TODO To change to a specialized field.
        $formSchools->addField(
            'decimal',
            'location_alt',
            'Altitude',
            [
                'required' => true,
                'precision' => 12,
                'scale' => 3,
                'meta' => [
                    'catalog_id' => 'A.1.3',
                ],
            ]
        )
            ->setDescription('TODO To change to a specialized field.');
        $formSchools->addField(
            'short_text',
            'minor_local_entity',
            'Minor local entity',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.1.4',
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'mayor_local_entity',
            'Major local entity',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.1.5',
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'regional_entity',
            'Regional entity',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.1.6',
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'school_name',
            'Name of School',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.2',
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'school_code',
            'School Code',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'A.3',
                ],
            ]
        )
            ->setDescription('Only if schools are assigned codes in the country');
        $formSchools->addField(
            'select',
            'school_type',
            'Type of school',
            [
                'required' => true,
                'multivalued' => true,
                'options' => [
                    1 => 'Pre-primary',
                    2 => 'Primary',
                    3 => 'Secondary',
                    99 => 'Other, specify',
                ],
                'meta' => [
                    'catalog_id' => 'A.4',
                    'field_other' => 'school_type_other',
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'school_type_other',
            'Other type of school',
            [
                'required' => false,
                'multivalued' => true,
                'meta' => [
                    'catalog_id' => 'A.4.1',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_type' => 99],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'staff_total_number_of_women',
            'Total number of women',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.5.1',
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'staff_total_number_of_men',
            'Total number of men',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.5.2',
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'student_total_number_of_female',
            'Total number of female students',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.6.1',
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'student_total_number_of_male',
            'Total number of male students',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.6.2',
                ],
            ]
        );
        $formSchools->addField(
            'long_text',
            'rural_communities_served',
            'Names of rural communities served by the school',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'A.7',
                ],
            ]
        )
            ->setDescription('Communities whose members the school welcomes as student at the time of the visit');
        $formSchools->addField(
            'short_text',
            'national_classification',
            'National classifications',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'A.8',
                ],
            ]
        );
        $formSchools->addField(
            'long_text',
            'school_observations',
            'Observations about the school',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'A.9',
                ],
            ]
        );

        // Group B.
        $formSchools->addField(
            'select',
            'have_water_supply_system',
            'Does the school have a water supply system?',
            [
                'required' => true,
                'multivalued' => true,
                'options' => [
                    1 => 'Yes, shared use with the community',
                    2 => 'Yes, exclusively used by the school',
                    3 => 'No',
                ],
                'meta' => [
                    'catalog_id' => 'B.1',
                ],
            ]
        )
            ->setDescription('System as understood by SIASAR: Piped water, well or protected spring, Rainwater collection, always from an improved water source');
        $formSchools->addField(
            'short_text',
            'name_water_supply_system',
            'Name of system that supplies the school',
            [
                'required' => false,
                'multivalued' => true,
                'meta' => [
                    'catalog_id' => 'B.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_have_water_supply_system' => 1],
                                ['field_have_water_supply_system' => 2],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'select',
            'functioning_of_water_system',
            'Functioning of the school’s water supply system',
            [
                'required' => true,
                'options' => [
                    'A' => 'Good: Operational and functions correctly throughout the year',
                    'B' => 'Fair: Operational, but sometimes does not function correctly during the year',
                    'C' => 'Poor: Operational, but does not function correctly most of the year',
                    'D' => 'Inoperable: Non-operational',
                ],
                'meta' => [
                    'catalog_id' => 'B.3',
                ],
            ]
        )
            ->setDescription('Confirm at the time of the visit');
        $formSchools->addField(
            'boolean',
            'water_for_drinking',
            'Is the water provided by the system used by the school for drinking?',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'B.4',
                ],
            ]
        );
        $formSchools->addField(
            'select',
            'main_source_for_drinking',
            'What is the main source of drinking water for the school?',
            [
                'required' => false,
                'options' => [
                    1 => 'Rainwater',
                    2 => 'Unprotected well/spring',
                    3 => 'Packaged bottled water',
                    4 => 'Tanker-truck or cart',
                    5 => 'Surface water (lake, river, stream)',
                    6 => 'No water source',
                ],
                'meta' => [
                    'catalog_id' => 'B.5.1',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_have_water_supply_system' => 3],
                                ['field_water_for_drinking' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'select',
            'reasons_not_using_water_system',
            'Indicate the main reasons for not using water provided by the system',
            [
                'required' => false,
                'options' => [
                    1 => 'Acceptability (i.e. taste, odor, color, etc.)',
                    2 => 'Quality',
                    3 => 'Affordability',
                    4 => 'Accessibility',
                    5 => 'Availability (continuity and reliability)',
                    99 => 'Other, specify',
                ],
                'meta' => [
                    'catalog_id' => 'B.5.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_water_for_drinking' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'short_text',
            'reasons_not_using_water_system_other',
            'Other reason',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'B.5.2.1',
                    'conditional' => [
                        'required' => [
                            'and' => [
                                ['field_reasons_not_using_water_system' => 99],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'boolean',
            'main_source_is_water_available_at_school',
            'Is drinking water from the main source currently available at the school?',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'B.6',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_have_water_supply_system' => 3],
                                ['field_water_for_drinking' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );

        // Group C.
        $formSchools->addField(
            'boolean',
            'school_have_toilets',
            'Does the school have toilets or latrines?',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.1',
                ],
            ]
        );
        $formSchools->addField(
            'select',
            'type_toilets',
            'What type of student toilets/latrines are at the school?',
            [
                'required' => true,
                'options' => [
                    1 => 'Flush / Pour-flush toilets',
                    2 => 'Pit latrines with slab',
                    3 => 'Composting toilets',
                    4 => 'Pit latrines without slab',
                    5 => 'Hanging latrines',
                    6 => 'Bucket latrines',
                ],
                'meta' => [
                    'catalog_id' => 'C.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        )
            ->setDescription('Check one - most common');
        $formSchools->addField(
            'integer',
            'girls_only_toilets_total',
            'Girls’ only toilets / Total',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.3.1.1',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'girls_only_toilets_usable',
            'Girls’ only toilets / Usable',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.3.1.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        )
            ->setDescription('Available, functional, private');
        $formSchools->addField(
            'integer',
            'boys_only_toilets_total',
            'Boys’ only toilets / Total',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.3.2.1',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'boys_only_toilets_usable',
            'Boys’ only toilets / Usable',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.3.2.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        )
            ->setDescription('Available, functional, private');
        $formSchools->addField(
            'integer',
            'common_use_toilets_total',
            'Common use toilets / Total',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.3.3.1',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'integer',
            'common_use_toilets_usable',
            'Common use toilets / Usable',
            [
                'required' => true,
                'meta' => [
                    'catalog_id' => 'C.3.3.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        )
            ->setDescription('Available, functional, private');
        $formSchools->addField(
            'select',
            'how_clean_student_toilets',
            'In general, how clean are the student toilets/latrines?',
            [
                'required' => false,
                'options' => [
                    1 => 'Clean: all toilets do not have a strong smell or significant numbers of flies or mosquitos, and there is no visible faeces on the floor, walls, seat (or pan) or around the facility',
                    2 => 'Somewhat clean: there is some smell and/or some sign of faecal matter in some of the toilets/latrines.',
                    3 => 'Not clean: there is a strong smell and/or presence of faecal matter in most toilets/latrines',
                ],
                'meta' => [
                    'catalog_id' => 'C.4',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'boolean',
            'limited_mobility_vision_toilet',
            'Is there at least one usable toilet/latrine that is accessible to those with limited mobility or vision?',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'C.5',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_school_have_toilets' => 'false'],
                            ],
                        ],
                    ],
                ],
            ]
        );

        // Group D.
        $formSchools->addField(
            'boolean',
            'handwashing_facilities',
            'Are there handwashing facilities at the school?',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'D.1',
                ],
            ]
        );
        $formSchools->addField(
            'select',
            'handwashing_facilities_soap_water',
            'Are both soap and water currently available at the handwashing facilities?',
            [
                'required' => false,
                'options' => [
                    1 => 'Yes, water and soap',
                    2 => 'Water only',
                    3 => 'Soap only',
                    4 => 'Neither water nor soap',
                ],
                'meta' => [
                    'catalog_id' => 'D.2',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_handwashing_facilities' => 'true'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'select',
            'menstrual_facilities_soap_water',
            'Are water and soap available in a private space for girls to manage menstrual hygiene?',
            [
                'required' => false,
                'options' => [
                    1 => 'Yes, water and soap',
                    2 => 'Water only',
                    3 => 'Soap only',
                    4 => 'Neither water nor soap',
                ],
                'meta' => [
                    'catalog_id' => 'D.3',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_handwashing_facilities' => 'true'],
                            ],
                        ],
                    ],
                ],
            ]
        );
        $formSchools->addField(
            'boolean',
            'menstrual_covered_bins',
            'Are there covered bins for disposal of menstrual hygiene materials in girls’ toilets?',
            [
                'required' => false,
                'meta' => [
                    'catalog_id' => 'D.4',
                    'conditional' => [
                        'required' => [
                            'or' => [
                                ['field_handwashing_facilities' => 'true'],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $formSchools->saveNow();
        $formSchools->install();
    }

    /**
     * Create schools .contacts subform.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function createSchoolsContacts(): void
    {
        $formSchoolsContacts = $this->forms->create('school.contact', ChangedFormManager::class);
        $formSchoolsContacts->setTitle('Schools Interviewee(s)/contacts');
        $formSchoolsContacts->setDescription('List of school contacts listed in field 0.3 of Schools form.');
        $formSchoolsContacts->setMeta(
            [
                'title' => 'Interviewee(s)/contacts',
                'version' => 'Version 12.4 – April 2021',
                'parent_form' => 'schools',
            ]
        );
        $formSchoolsContacts->addField(
            'short_text',
            'name',
            'Name of person interviewed',
            [
                'meta' => [
                    'catalog_id' => '0.3.1',
                ],
            ]
        );
        $formSchoolsContacts->addField(
            'short_text',
            'occupation',
            'Occupation',
            [
                'meta' => [
                    'catalog_id' => '0.3.2',
                ],
            ]
        );
        $formSchoolsContacts->addField(
            'phone',
            'phone',
            'Telephone number',
            [
                'meta' => [
                    'catalog_id' => '0.3.3',
                ],
            ]
        );
        $formSchoolsContacts->addField(
            'mail',
            'mail',
            'Email address',
            [
                'meta' => [
                    'catalog_id' => '0.3.4',
                ],
            ]
        );
        $formSchoolsContacts->addField(
            'boolean',
            'consent_use',
            'Consent to use information for SIASAR',
            [
                'meta' => [
                    'catalog_id' => '0.3.5',
                ],
            ]
        );
        $formSchoolsContacts->saveNow();
        $formSchoolsContacts->install();
    }
}
