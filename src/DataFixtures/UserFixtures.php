<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataFixtures;

use App\Entity\Language;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * User fixtures.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserFixtures extends Fixture
{
    protected UserPasswordHasherInterface $passwordEncoder;

    /**
     * UserFixtures constructor.
     *
     * @param UserPasswordHasherInterface $passwordEncoder Password encoder.
     */
    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Load predefined users.
     *
     * @param ObjectManager $manager Object manager.
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        // $langRepo = $manager->getRepository(Language::class);
        // $esLang = $langRepo->find('es');
        // $countryRepo = $manager->getRepository(Country::class);
        // $country = $countryRepo->find('es');
        // $this->createUser($manager, 'Admin', 'pedro@front.id', 'admin', true, ['ROLE_USER', 'ROLE_ADMIN'], $country, $esLang);
        // $manager->flush();
    }

    /**
     * Create a user.
     *
     * @param ObjectManager $manager  Object manager.
     * @param string        $username User name.
     * @param string        $email    User email.
     * @param string        $password User password.
     * @param bool          $active   Is the user active?
     * @param array         $roles    User roles.
     * @param ?Language     $lang     User language.
     *
     * @return void
     */
    protected function createUser(ObjectManager $manager, string $username, string $email, string $password, bool $active = false, array $roles = [], ?Language $lang = null): void
    {
        $user = new User();
        $user->setRoles($roles)
            ->setEmail($email)
            ->setUsername($username)
            ->setPassword($this->passwordEncoder->hashPassword($user, $password))
            ->setActive($active)
            ->setLanguage($lang);
        $manager->persist($user);
    }
}
