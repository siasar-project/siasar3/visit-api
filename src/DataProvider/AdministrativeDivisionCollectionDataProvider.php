<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\DataProvider\Base\CollectionDataProviderBase;
use App\DataProvider\Extension\AdministrativeDivisionCollectionExtensionInterface;
use App\Entity\AdministrativeDivision;
use App\RepositorySecured\AdministrativeDivisionRepositorySecured;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Administrative division collection data provider.
 */
final class AdministrativeDivisionCollectionDataProvider extends CollectionDataProviderBase implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @param ContainerInterface                                 $container
     * @param IriConverterInterface                              $iriConverter
     * @param AdministrativeDivisionRepositorySecured            $repository
     * @param AdministrativeDivisionCollectionExtensionInterface $paginationExtension
     */
    public function __construct(ContainerInterface $container, IriConverterInterface $iriConverter, AdministrativeDivisionRepositorySecured $repository, AdministrativeDivisionCollectionExtensionInterface $paginationExtension)
    {
        parent::__construct($container, $iriConverter, $repository, $paginationExtension);
        $this->resourceClass = AdministrativeDivision::class;
    }
}
