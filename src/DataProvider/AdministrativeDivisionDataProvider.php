<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderInterface;
use ApiPlatform\Core\DataProvider\SerializerAwareDataProviderTrait;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\AdministrativeDivision;
use App\RepositorySecured\AdministrativeDivisionRepositorySecured;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use PHPUnit\Util\Exception;

/**
 * Administrative division data provider.
 */
final class AdministrativeDivisionDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface, SerializerAwareDataProviderInterface
{
    use SerializerAwareDataProviderTrait;

    protected AdministrativeDivisionRepositorySecured $divisionRepositorySecured;
    protected IriConverterInterface $iriConverter;

    /**
     * @param AdministrativeDivisionRepositorySecured $divisionRepositorySecured
     * @param IriConverterInterface                   $iriConverter
     */
    public function __construct(AdministrativeDivisionRepositorySecured $divisionRepositorySecured, IriConverterInterface $iriConverter)
    {
        $this->divisionRepositorySecured = $divisionRepositorySecured;
        $this->iriConverter = $iriConverter;
    }

    /**
     * Is it a supported operation?
     *
     * @param string      $resourceClass
     * @param string|null $operationName
     * @param array       $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return AdministrativeDivision::class === $resourceClass && isset($context["item_operation_name"]) && $context["item_operation_name"] === 'delete';
    }

    /**
     * Retrieves an item.
     *
     * @param string           $resourceClass
     * @param array|int|string $id
     * @param string|null      $operationName
     * @param array            $context
     *
     * @return object|null
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?AdministrativeDivision
    {
        if (AdministrativeDivision::class !== $resourceClass) {
            return null;
        }

        // Retrieve data from anywhere you want, in a custom format
        $data = $this->divisionRepositorySecured->find($id);
        if (!$data) {
            return null;
        }
        $item = $this->dataToarray($data);
        switch ($operationName) {
            case 'delete':
                $children = count($data->getAdministrativeDivisions());
                if ($children > 0) {
                    throw new InvalidArgumentException("Cannot delete a administrative division with children, it have $children.");
                }
                // We need response with the removed entity.
                $item = $this->dataToarray($data);
                try {
                    $this->divisionRepositorySecured->removeNow($data);
                } catch (ForeignKeyConstraintViolationException $e) {
                    throw new Exception("This administrative division is in use by other entity, maybe User.");
                }
                break;
        }
        // Deserialize data using the Serializer
        if ($item) {
            return $this->getSerializer()->deserialize(json_encode($item), AdministrativeDivision::class, 'json');
        }

        return null;
    }

    /**
     * Build data array from entity.
     *
     * @param mixed $data
     *
     * @return array
     */
    protected function dataToarray(mixed $data): array
    {
        $item = [
            'id' => $data->getId(),
            'country' => $this->iriConverter->getIriFromItem($data->getCountry()),
            'name' => $data->getName(),
            'administrativeDivision' => [], //count($data->getAdministrativeDivisions()),
            'code' => $data->getCode(),
        ];
        if ($data->getParent()) {
            $item['parent'] = $this->iriConverter->getIriFromItem($data->getParent());
        }

        return $item;
    }
}
