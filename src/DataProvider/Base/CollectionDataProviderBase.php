<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider\Base;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Traits\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Collection data provider.
 *
 * @template T
 */
class CollectionDataProviderBase implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    use StringTranslationTrait;

    protected mixed $repository;
    protected mixed $paginationExtension;
    protected ParameterBagInterface $systemSettings;
    protected IriConverterInterface $iriConverter;
    protected ?string $resourceClass;

    /**
     * @param ContainerInterface    $container
     * @param IriConverterInterface $iriConverter
     * @param mixed                 $repository
     * @param mixed                 $paginationExtension
     */
    public function __construct(ContainerInterface $container, IriConverterInterface $iriConverter, mixed $repository, mixed $paginationExtension)
    {
        $this->repository = $repository;
        $this->paginationExtension = $paginationExtension;
        $this->systemSettings = $container->getParameterBag();
        $this->iriConverter = $iriConverter;
        $this->resourceClass = null;
    }

    /**
     * @param string           $resourceClass
     * @param string|null      $operationName
     * @param array<string, T> $context
     *
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        if (!$this->resourceClass) {
            throw new \Exception('[CollectionDataProviderBase] resourceClass not initialized.');
        }

        return $this->resourceClass === $resourceClass;
    }

    /**
     * @param string           $resourceClass
     * @param string|null      $operationName
     * @param array<string, T> $context
     *
     * @return iterable<T>
     */
    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        try {
            $itemsPerPage = $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
            $paramPage = 0;
            $orderBy = [];
            $filterBy = [];
            if (isset($context["filters"])) {
                foreach ($context["filters"] as $paramName => $paramValue) {
                    $lparam = strtolower($paramName);
                    // Ignore xdebug query parameter.
                    if (stripos($lparam, 'xdebug') !== false) {
                        continue;
                    }
                    switch ($paramName) {
                        case 'order':
                            $orderBy += $paramValue;
                            break;
                        case 'page':
                            $paramPage = $paramValue - 1;
                            break;
                        // Filter by relation.
                        case 'parent':
                        case 'country':
                            if (!empty($paramValue)) {
                                if ('null' === $paramValue) {
                                    $filterBy[$paramName] = null;
                                } elseif (is_array($paramValue)) {
                                    $values = [];
                                    foreach ($paramValue as $value) {
                                        if ('parent' === $paramName) {
                                            $ulid = new Ulid($this->iriConverter->getItemFromIri($value)->getId());
                                            $values[] = $ulid->toBinary();
                                        } else {
                                            $values[] = $this->iriConverter->getItemFromIri($value)->getId();
                                        }
                                    }
                                    $filterBy[$paramName] = ['in' => $values];
                                } else {
                                    $filterBy[$paramName] = $this->iriConverter->getItemFromIri($paramValue)->getId();
                                }
                            }
                            break;
                        case 'administrativeDivision':
                            if (!empty($paramValue)) {
                                if (is_array($paramValue)) {
                                    $filterBy[$paramName] = ['in' => $paramValue];
                                } else {
                                    $ulid = new Ulid($paramValue);
                                    $filterBy[$paramName] = $ulid->toBinary();
                                }
                            }
                            break;
                        default:
                            if (!empty($paramValue)) {
                                if (is_array($paramValue)) {
                                    $filterBy[$paramName] = ['in' => $paramValue];
                                } else {
                                    $filterBy[$paramName] = $paramValue;
                                }
                            }
                            break;
                    }
                }
            }

            $collection = $this->repository->findBy($filterBy, $orderBy, $itemsPerPage, $paramPage * $itemsPerPage);
        } catch (\Exception $e) {
            $className = (new \ReflectionClass($this->resourceClass))->getShortName();
            throw new \RuntimeException(sprintf('Unable to retrieve %s: %s', $className, $e->getMessage()));
        }

        if (!$this->paginationExtension->isEnabled($resourceClass, $operationName, $context)) {
            return $collection;
        }
        $context["filters"]["page"] = 1;

        return $this->paginationExtension->getResult($collection, $resourceClass, $operationName, $context);
    }
}
