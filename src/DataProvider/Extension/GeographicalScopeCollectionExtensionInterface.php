<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider\Extension;

use App\Entity\GeographicalScope;

/**
 * GeographicalScope collection extension interface.
 */
interface GeographicalScopeCollectionExtensionInterface
{
    /**
     * Returns the final result object.
     *
     * @param array<int, GeographicalScope> $collection
     * @param string                        $resourceClass
     * @param string|null                   $operationName
     * @param array<string, mixed>          $context
     *
     * @return iterable<GeographicalScope>
     */
    public function getResult(array $collection, string $resourceClass, string $operationName = null, array $context = []): iterable;

    /**
     * Tells if the extension is enabled or not.
     *
     * @param string|null          $resourceClass
     * @param string|null          $operationName
     * @param array<string, mixed> $context
     *
     * @return bool
     */
    public function isEnabled(string $resourceClass = null, string $operationName = null, array $context = []): bool;
}
