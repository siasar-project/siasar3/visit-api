<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider\Extension;

use ApiPlatform\Core\DataProvider\ArrayPaginator;
use ApiPlatform\Core\DataProvider\Pagination;

/**
 * Pagination extension.
 */
class PaginationExtensionBase
{
    protected Pagination $pagination;

    /**
     * @param Pagination $pagination
     */
    public function __construct(Pagination $pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(array $collection, string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        [, $offset, $itemPerPage] = $this->pagination->getPagination($resourceClass, $operationName, $context);

        if (count($collection) === 0) {
            // Workaround oversize error throwing.
            return new ArrayPaginator($collection, 0, 0);
        }

        return new ArrayPaginator($collection, $offset, $itemPerPage);
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(string $resourceClass = null, string $operationName = null, array $context = []): bool
    {
        return $this->pagination->isEnabled($resourceClass, $operationName, $context);
    }
}
