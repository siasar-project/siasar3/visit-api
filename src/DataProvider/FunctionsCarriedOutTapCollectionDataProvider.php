<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\DataProvider\Base\CollectionDataProviderBase;
use App\DataProvider\Extension\FunctionsCarriedOutTapCollectionExtensionInterface;
use App\Entity\FunctionsCarriedOutTap;
use App\RepositorySecured\FunctionsCarriedOutTapRepositorySecured;

/**
 * FunctionsCarriedOutTap collection data provider.
 */
final class FunctionsCarriedOutTapCollectionDataProvider extends CollectionDataProviderBase implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @param ContainerInterface                                 $container
     * @param IriConverterInterface                              $iriConverter
     * @param FunctionsCarriedOutTapRepositorySecured            $repository
     * @param FunctionsCarriedOutTapCollectionExtensionInterface $paginationExtension
     */
    public function __construct(ContainerInterface $container, IriConverterInterface $iriConverter, FunctionsCarriedOutTapRepositorySecured $repository, FunctionsCarriedOutTapCollectionExtensionInterface $paginationExtension)
    {
        parent::__construct($container, $iriConverter, $repository, $paginationExtension);
        $this->resourceClass = FunctionsCarriedOutTap::class;
    }
}
