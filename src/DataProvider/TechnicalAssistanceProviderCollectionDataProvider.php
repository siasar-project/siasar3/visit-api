<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\DataProvider;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\DataProvider\Extension\TechnicalAssistanceProviderCollectionExtensionInterface;
use App\Entity\TechnicalAssistanceProvider;
use App\RepositorySecured\TechnicalAssistanceProviderRepositorySecured;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\DataProvider\Base\CollectionDataProviderBase;

/**
 * collection data provider.
 */
final class TechnicalAssistanceProviderCollectionDataProvider extends CollectionDataProviderBase implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @param ContainerInterface                                      $container
     * @param IriConverterInterface                                   $iriConverter
     * @param TechnicalAssistanceProviderRepositorySecured            $repository
     * @param TechnicalAssistanceProviderCollectionExtensionInterface $paginationExtension
     */
    public function __construct(ContainerInterface $container, IriConverterInterface $iriConverter, TechnicalAssistanceProviderRepositorySecured $repository, TechnicalAssistanceProviderCollectionExtensionInterface $paginationExtension)
    {
        parent::__construct($container, $iriConverter, $repository, $paginationExtension);
        $this->resourceClass = TechnicalAssistanceProvider::class;
    }
}
