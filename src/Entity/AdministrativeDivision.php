<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Annotations\IsCountryParametric;
use App\Forms\FormReferenceEntityInterface;
use App\Repository\AdministrativeDivisionRepository;
use App\Traits\FormReferenceStartingTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Iterator;
use RecursiveIterator;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotations\UseHowCounter;
use App\Annotations\SecuredRepository;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Administrative division.
 *
 * @ORM\Entity(repositoryClass=AdministrativeDivisionRepository::class)
 * @ORM\Table(name="administrative_division",indexes={
 *     @ORM\Index(name="branchx", columns={"branch"}),
 *     @ORM\Index(name="codex", columns={"code"})
 * })
 *
 * @ORM\HasLifecycleCallbacks
 *
 * @IsCountryParametric
 *
 * @SecuredRepository(class=App\RepositorySecured\AdministrativeDivisionRepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"administrative_division:write"}},
 *     normalizationContext={"groups":{"administrative_division:read"}},
 *     itemOperations={
 *          "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *          "put" = { "method" = "PUT", "security" = "is_granted('UPDATE', object)" },
 *          "patch" = { "method" = "PATCH", "security" = "is_granted('UPDATE', object)" },
 *          "delete" = { "method" = "DELETE", "security" = "is_granted('DELETE', object)" }
 *     },
 *     collectionOperations={
 *      "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)"
 *      },
 *      "ad_by_country"={
 *          "route_name" = "ad_by_country",
 *          "method" = "GET",
 *          "security_get_denormalize" = "is_granted('READ', object)",
 *          "openapi_context"={
 *              "description"="Retrieves the collection of administrative divisions resources in the selected Country.",
 *              "parameters"={
 *                  {
 *                      "name"="page",
 *                      "description"="The collection page number. Note: This pages will have 500 items.",
 *                      "in"="path",
 *                      "required"=true,
 *                      "type"="number",
 *                  },
 *                  {
 *                      "name"="country_code",
 *                      "description"="Country to query",
 *                      "in"="path",
 *                      "required"=true,
 *                      "type"="string",
 *                  },
 *              },
 *          },
 *      },
 *      "post" = { "method" = "POST", "security_post_denormalize" = "is_granted('CREATE', object)" },
 *     }
 * )
 */
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'partial',
    'code' => 'partial',
    'parent' => 'exact',
    'country' => 'exact',
    'level' => 'exact',
])]
class AdministrativeDivision implements FormReferenceEntityInterface
{
    use FormReferenceStartingTrait;

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"administrative_division:read", "user:read"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="administrativeDivisions", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code", name="country_code" )
     *
     * @Groups({"administrative_division:write"})
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"administrative_division:read", "administrative_division:write", "user:read"})
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity=App\Entity\AdministrativeDivision::class, inversedBy="administrativeDivisions", fetch="EXTRA_LAZY")
     *
     * @MaxDepth(2)
     *
     * @Groups({"administrative_division:read", "administrative_division:write"})
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity=AdministrativeDivision::class, mappedBy="parent", fetch="EXTRA_LAZY")
     *
     * @MaxDepth(2)
     *
     * @-Groups({"administrative_division:read"})
     *
     * @UseHowCounter
     */
    protected $administrativeDivisions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"administrative_division:read", "administrative_division:write"})
     */
    protected $code;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"administrative_division:read"})
     */
    protected $level;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"administrative_division:read", "administrative_division:write"})
     */
    protected $oldid;

    /**
     * Administrative division branch.
     *
     * A comma separated list of this node and parents.
     *
     * @ORM\Column(type="text")
     *
     * @Groups({"administrative_division:read"})
     */
    protected $branch;

    /**
     * constructor.
     *
     * @param string $id
     */
    public function __construct(string $id = null)
    {
        if (null === $id) {
            $ulid = new Ulid();
            $id = $ulid->toBase32();
        }
        $this->id = $id;
        $this->administrativeDivisions = new ArrayCollection();
    }

    /**
     * Calculate and get deepness of the division.
     *
     * @return integer
     */
    public function deep()
    {
        if (null === $this->getParent()) {
            return 1;
        }

        return 1 + $this->getParent()->deep();
    }

    /**
     * Update level or deep, and branch property.
     *
     * @ORM\PrePersist
     * @ORM\PostUpdate
     *
     * @return void
     */
    public function updateInternals(): void
    {
        // If not level defined.
        if (0 === $this->level || null === $this->level) {
            // Calculate it.
            $this->level = $this->deep();
        }
        // Update branch.
        $ids = [];
        $current = $this;
        do {
            $ulid = Ulid::fromString($current->getId());
            array_unshift($ids, $ulid->toBase32());
            $current = $current->getParent();
        } while ($current);
        $this->branch = implode(',', $ids);
    }

    /**
     * Get level, or deep, from database.
     *
     * @return int
     */
    public function getLevel(): int
    {
        // If not level defined.
        if (0 === $this->level || null === $this->level) {
            // Calculate it.
            $this->level = $this->deep();
        }

        return $this->level;
    }

    /**
     * get the id
     *
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * get the country
     *
     * @return Country
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * set the country
     *
     * @param Country $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * get the name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * set the name
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * get parent division
     *
     * @return AdministrativeDivision
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * set the paren division
     *
     * @param AdministrativeDivision $parent
     *
     * @return $this
     */
    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getAdministrativeDivisions(): Collection
    {
        return $this->administrativeDivisions;
    }

    /**
     * add an addAdministrativeDivision to $adminitrativeDivisions
     *
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return $this
     */
    public function addAdministrativeDivision(self $administrativeDivision): self
    {
        if (!$this->administrativeDivisions->contains($administrativeDivision)) {
            $this->administrativeDivisions[] = $administrativeDivision;
            $administrativeDivision->setParent($this);
        }

        return $this;
    }

    /**
     * remove an AdministrativeDivision from $adminitrativeDivisions
     *
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return $this
     */
    public function removeAdministrativeDivision(self $administrativeDivision): self
    {
        if ($this->administrativeDivisions->removeElement($administrativeDivision)) {
            // set the owning side to null (unless already changed)
            if ($administrativeDivision->getParent() === $this) {
                $administrativeDivision->setParent(null);
            }
        }

        return $this;
    }

    /**
     * get code
     *
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     *  set the code
     *
     *  @param string $code
     *
     *  @return $this
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get old ID.
     *
     * @return mixed
     */
    public function getOldid()
    {
        return $this->oldid;
    }

    /**
     * Set old ID.
     *
     * @param mixed $oldid
     */
    public function setOldid($oldid): void
    {
        $this->oldid = $oldid;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param mixed $branch
     */
    public function setBranch($branch): void
    {
        $this->branch = $branch;
    }

    /**
     * Get instance internal label.
     *
     * @return string
     */
    public function __toString(): string
    {
        return self::class.":{$this->id}:{$this->name}";
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'id' => $this->id,
            'country' => $this->country?->getId(),
            'name' => $this->name,
            'parent' => $this->parent?->getId(),
            'administrativeDivisions' => $this->administrativeDivisions->count(),
            'code' => $this->code,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        $resp = [
            'name' => $this->name,
            'code' => $this->code,
            'parent_id' => null,
            'parent' => null,
        ];
        if ($this->parent) {
            $resp['parent_id'] = $this->parent->getId();
            $resp['parent'] = $this->parent->formatExtraJson($forceString);
        }

        return $resp;
    }
}
