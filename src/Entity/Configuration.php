<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Configuration\ConfigurationReadInterface;
use App\Repository\ConfigurationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Configuration entity.
 *
 * @ORM\Entity(repositoryClass=App\Repository\ConfigurationRepository::class)
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ApiResource(
 *      normalizationContext={"groups"={"GET"}},
 *      itemOperations={},
 *      collectionOperations={
 *          "i18n_add"={
 *              "method" = "POST",
 *              "controller" = "App\Api\Action\I18nAction::i18nAddLiteral",
 *              "path"="/configuration/i18n/add",
 *              "normalization_context"={
 *                  "groups"={"POST"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Add a new english i18n literal.",
 *                  "description"="Add a new english i18n literal.",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required" = {
 *                                      "literal",
 *                                  },
 *                                  "properties"={
 *                                      "literal"={
 *                                          "type"="string",
 *                                      },
 *                                   },
 *                              }
 *                          }
 *                      }
 *                  },
 *                  "parameters"={
 *                      {
 *                          "name"="context",
 *                          "description"="Context label. We recommend using your application name and version, e.x 'SIASAR App v1.0.0'",
 *                          "in"="query",
 *                          "required"=false,
 *                          "type"="string",
 *                      },
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="string",
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "i18n_literals"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\I18nAction::i18nLiterals",
 *              "path"="/configuration/i18n/{iso_code}",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Return i18n literals.",
 *                  "description"="Return i18n literals.",
 *                  "parameters"={
 *                      {
 *                          "name"="iso_code",
 *                          "description"="ISO language code",
 *                          "in"="path",
 *                          "required"=true,
 *                          "schema"={
 *                              "type"="string"
 *                          },
 *                      },
 *                      {
 *                          "name"="context",
 *                          "description"="Context label. We recommend using your application name and version, e.x 'SIASAR App v1.0.0'",
 *                          "in"="query",
 *                          "required"=false,
 *                          "schema"={
 *                              "type"="string"
 *                          },
 *                      },
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "id"= {
 *                                                  "type"="string",
 *                                              },
 *                                              "literals"= {
 *                                                  "type"="object",
 *                                                  "properties"={
 *                                                      "key"= {
 *                                                          "type"="string",
 *                                                      },
 *                                                  },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "i18n_translator"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\I18nAction::i18nTranslator",
 *              "path"="/configuration/i18n/{iso_code}/translator",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Return i18n translations.",
 *                  "description"="Return i18n translations.",
 *                  "parameters"={
 *                      {
 *                          "name"="iso_code",
 *                          "description"="ISO language code",
 *                          "in"="path",
 *                          "required"=true,
 *                          "schema"={
 *                              "type"="string"
 *                          },
 *                      },
 *                      {
 *                          "name"="context",
 *                          "description"="Context label. We recommend using your application name and version, e.x 'SIASAR App v1.0.0'",
 *                          "in"="query",
 *                          "required"=false,
 *                          "schema"={
 *                              "type"="string"
 *                          },
 *                      },
 *                      {
 *                          "name"="search_in",
 *                          "description"="Search for translated strings, untranslated strings or both.",
 *                          "in"="query",
 *                          "required"=false,
 *                          "schema"={
 *                              "type"="string",
 *                              "enum"={"all", "untranslated", "translated"},
 *                          },
 *                      },
 *                      {
 *                          "name"="contains",
 *                          "description"="String contains.",
 *                          "in"="query",
 *                          "required"=false,
 *                          "schema"={
 *                              "type"="string"
 *                          },
 *                      },
 *                      {
 *                          "name"="page",
 *                          "description"="The collection page number.",
 *                          "in"="query",
 *                          "required"=false,
 *                          "schema"={
 *                              "type"="integer",
 *                          },
 *                      },
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "id"= {
 *                                                  "type"="string",
 *                                              },
 *                                              "literals"= {
 *                                                  "type"="object",
 *                                                  "properties"={
 *                                                      "key"= {
 *                                                          "type"="string",
 *                                                      },
 *                                                  },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "i18n_languages"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\I18nAction::i18nLanguages",
 *              "path"="/configuration/i18n_languages",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Returns a list of languages translatable.",
 *                  "description"="Returns a list of languages translatable by the user.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "id"= {
 *                                                  "type"="object",
 *                                                  "properties"={
 *                                                      "iso_code"= {
 *                                                          "type"="string",
 *                                                      },
 *                                                      "name"= {
 *                                                          "type"="string",
 *                                                      },
 *                                                      "ltr"= {
 *                                                          "type"="boolean",
 *                                                      },
 *                                                      "plural_formula"= {
 *                                                          "type"="string",
 *                                                      },
 *                                                      "plural_number"= {
 *                                                          "type"="integer",
 *                                                      },
 *                                                      "translatable"= {
 *                                                          "type"="boolean",
 *                                                      },
 *                                                  },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "i18n_translate"={
 *              "method" = "POST",
 *              "controller" = "App\Api\Action\I18nAction::i18nAddTranslation",
 *              "path"="/configuration/i18n/{iso_code}",
 *              "normalization_context"={
 *                  "groups"={"POST"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Add a translation.",
 *                  "description"="Add a translation.",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required" = {
 *                                      "source",
 *                                      "target",
 *                                  },
 *                                  "properties"={
 *                                      "source"={
 *                                          "type"="string",
 *                                      },
 *                                      "target"={
 *                                          "type"="string",
 *                                      },
 *                                   },
 *                              }
 *                          }
 *                      }
 *                  },
 *                  "parameters"={
 *                      {
 *                          "name"="iso_code",
 *                          "description"="ISO Code",
 *                          "in"="path",
 *                          "required"=true,
 *                          "type"="string",
 *                      },
 *                  }
 *              },
 *          },
 *          "i18n_progress"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\I18nAction::i18nProgress",
 *              "path"="/configuration/i18n/{iso_code}/progress",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Calculates the percentage of translated and untranslated literals.",
 *                  "description"="Calculates the percentage of translated and untranslated literals.",
 *                  "parameters"={
 *                      {
 *                          "name"="iso_code",
 *                          "description"="ISO language code",
 *                          "in"="path",
 *                          "required"=true,
 *                          "type"="string",
 *                      },
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "iso_code"= {
 *                                                  "type"="string",
 *                                              },
 *                                              "n_translated"= {
 *                                                  "type"="integer",
 *                                              },
 *                                              "n_untranslated"= {
 *                                                  "type"="integer",
 *                                              },
 *                                              "percentage"= {
 *                                                  "type"="integer",
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "api_version"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\ConfigurationAction::getVersion",
 *              "path"="/configuration/version",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get API version.",
 *                  "description"="Get API version.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="Version",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "title"={
 *                                              "type"="string",
 *                                          },
 *                                          "description"={
 *                                              "type"="string",
 *                                          },
 *                                          "version"={
 *                                              "type"="string",
 *                                          },
 *                                          "terms_of_service"={
 *                                              "type"="string",
 *                                          },
 *                                          "licence"={
 *                                              "type"="string",
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "form_mutatebles"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\ConfigurationAction::mutatebles",
 *              "path"="/configuration/mutatebles",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get each mutateble field, in any form.",
 *                  "description"="Get each mutateble field, in any form.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="Mutateble fields",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "form.id"={
 *                                              "type"="array",
 *                                              "items"={"type"="string"},
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_ping"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\ConfigurationAction::systemPing",
 *              "path"="/configuration/ping",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Return ok status.",
 *                  "description"="Return ok status.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "maintenance_mode"={
 *                                              "type"="boolean",
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "maintenance_mode_status"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\ConfigurationAction::maintenanceModeStatus",
 *              "path"="/configuration/maintenance-mode",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get maintenance mode status.",
 *                  "description"="Get maintenance mode status.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "active"={
 *                                              "type"="boolean",
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "stats"={
 *              "method" = "GET",
 *              "controller" = "App\Api\Action\ConfigurationAction::systemStats",
 *              "path"="/configuration/stats",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get system stats.",
 *                  "description"="Get system stats.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={},
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "maintenance_mode"={
 *              "method" = "POST",
 *              "controller" = "App\Api\Action\ConfigurationAction::maintenanceMode",
 *              "path"="/configuration/maintenance-mode",
 *              "normalization_context"={
 *                  "groups"={"POST"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Update maintenance mode status.",
 *                  "description"="Update maintenance mode status.",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required" = {
 *                                      "active",
 *                                  },
 *                                  "properties"={
 *                                      "active"={
 *                                          "type"="boolean",
 *                                      },
 *                                   },
 *                              }
 *                          }
 *                      }
 *                  },
 *                  "responses"={
 *                      "201"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "active"={
 *                                              "type"="boolean",
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_units_length"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::lengthUnits",
 *              "path"="/configuration/system.units.length",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get allowed length units.",
 *                  "description"="Get allowed length units.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                              "properties"={
 *                                                  "key"= {
 *                                                      "type"="object",
 *                                                          "properties"={
 *                                                              "rate"= {
 *                                                                  "type"="number",
 *                                                              },
 *                                                              "symbol"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                              "deprecated"= {
 *                                                                  "type"="boolean",
 *                                                              },
 *                                                              "label"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                          },
 *                                                  },
 *                                              },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_units_flow"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::flowUnits",
 *              "path"="/configuration/system.units.flow",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get allowed flow units.",
 *                  "description"="Get allowed flow units.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                              "properties"={
 *                                                  "key"= {
 *                                                      "type"="object",
 *                                                          "properties"={
 *                                                              "rate"= {
 *                                                                  "type"="number",
 *                                                              },
 *                                                              "symbol"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                              "deprecated"= {
 *                                                                  "type"="boolean",
 *                                                              },
 *                                                              "label"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                          },
 *                                                  },
 *                                              },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_units_volume"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::volumeUnits",
 *              "path"="/configuration/system.units.volume",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get allowed volume units.",
 *                  "description"="Get allowed volume units.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                              "properties"={
 *                                                  "key"= {
 *                                                      "type"="object",
 *                                                          "properties"={
 *                                                              "rate"= {
 *                                                                  "type"="number",
 *                                                              },
 *                                                              "symbol"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                              "deprecated"= {
 *                                                                  "type"="boolean",
 *                                                              },
 *                                                              "label"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                          },
 *                                                  },
 *                                              },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_units_concentration"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::concentrationUnits",
 *              "path"="/configuration/system.units.concentration",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get allowed concentration units.",
 *                  "description"="Get allowed concentration units.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                              "properties"={
 *                                                  "key"= {
 *                                                      "type"="object",
 *                                                          "properties"={
 *                                                              "rate"= {
 *                                                                  "type"="number",
 *                                                              },
 *                                                              "symbol"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                              "deprecated"= {
 *                                                                  "type"="boolean",
 *                                                              },
 *                                                              "label"= {
 *                                                                  "type"="string",
 *                                                              },
 *                                                          },
 *                                                  },
 *                                              },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_role"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::systemRole",
 *              "path"="/configuration/system.role",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get system user roles.",
 *                  "description"="Get system user roles and permissions by role.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "role"= {
 *                                                  "type"="object",
 *                                                   "properties"={
 *                                                       "label"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "description"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "permissions"= {
 *                                                           "type"="array",
 *                                                           "items"={
 *                                                              "type"="string",
 *                                                           },
 *                                                       },
 *                                                   },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_permissions"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::systemPermissions",
 *              "path"="/configuration/system.permissions",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get system permissions.",
 *                  "description"="Get system permissions.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "permission"= {
 *                                                  "type"="object",
 *                                                   "properties"={
 *                                                       "label"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "description"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "security_warning"= {
 *                                                           "type"="boolean",
 *                                                       },
 *                                                       "group"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                   },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *          "system_languages"={
 *              "method" = "get",
 *              "controller" = "App\Api\Action\ConfigurationAction::systemLanguages",
 *              "path"="/configuration/system.languages",
 *              "normalization_context"={
 *                  "groups"={"GET"}
 *              },
 *              "openapi_context"={
 *                  "summary"="Get system languages.",
 *                  "description"="Get system languages.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="array",
 *                                      "items"={
 *                                          "type"="object",
 *                                          "properties"={
 *                                              "permission"= {
 *                                                  "type"="object",
 *                                                   "properties"={
 *                                                       "iso_code"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "name"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "ltr"= {
 *                                                           "type"="boolean",
 *                                                       },
 *                                                       "plural_number"= {
 *                                                           "type"="integer",
 *                                                       },
 *                                                       "plural_formula"= {
 *                                                           "type"="string",
 *                                                       },
 *                                                       "translatable"= {
 *                                                           "type"="boolean",
 *                                                       },
 *                                                   },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  }
 *              },
 *          },
 *      }
 * )
 */
class Configuration implements ConfigurationReadInterface
{
    /**
     * ID.
     *
     * The ID must be alphanumeric, with dots to split sections.
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(
     *     type="string",
     *     unique=true
     * )
     */
    protected string $id;

    /**
     * Configuration value.
     *
     * @ORM\Column(type="array")
     */
    protected array $value = [];

    /**
     * Get entity ID.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get configuration value.
     *
     * @return array|null
     */
    public function getValue(): ?array
    {
        return $this->value;
    }

    /**
     * Set configuration value.
     *
     * @param array $value The new configuration.
     *
     * @return $this
     */
    public function setValue(array $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Configuration constructor.
     *
     * It's inaccesible.
     */
    protected function __construct()
    {
        // It's inaccesible.
    }

    /**
     * Set configuration ID.
     *
     * @param string $id ID.
     *
     * @return void
     */
    protected function setId(string $id): void
    {
        $this->id = $id;
    }
}
