<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity\Configuration;

/**
 * Read only configuration.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationRead implements ConfigurationReadInterface
{
    protected array $value;
    protected string $id;

    /**
     * ConfigurationRead constructor.
     *
     * @param string $id    Id.
     * @param array  $value Configuration value.
     */
    public function __construct(string $id, array $value)
    {
        $this->value = $value;
        $this->id = $id;
    }

    /**
     * Get entity ID.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get configuration value.
     *
     * @return array|null
     */
    public function getValue(): ?array
    {
        return $this->value;
    }
}
