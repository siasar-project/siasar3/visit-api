<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity\Configuration;

use App\Entity\Configuration;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\EntityManager;

/**
 * Writable configuration.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationWrite extends Configuration implements ConfigurationWriteInterface
{
    use StringTranslationTrait;

    protected Configuration $configuration;
    protected EntityManager $manager;

    /**
     * ConfigurationWrite constructor.
     *
     * @param EntityManager      $manager       Entity manager.
     * @param string             $id            Configuration ID.
     * @param Configuration|null $configuration Wrapped configuration entity.
     *
     * @throws \Exception
     */
    public function __construct(EntityManager $manager, string $id, Configuration $configuration = null)
    {
        if (isset($configuration)) {
            if ($id !== $configuration->getId()) {
                throw new \Exception(
                    $this->t(
                        'Wrong id "@newId" to writable in ConfigurationWrite "@id".',
                        [
                            '@newId' => $id,
                            '@id' => $configuration->getId(),
                        ]
                    )
                );
            }
        } else {
            $configuration = new Configuration();
            $configuration->setId($id);
        }

        if (filter_var($id, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) === false) {
            throw new \Exception(
                $this->t('Not valid id name "@id".', ['@id' => $id])
            );
        }

        if (isset($configuration)) {
            $this->configuration = $configuration;
        }
        $this->manager = $manager;
    }

    /**
     * Get entity ID.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->configuration->getId();
    }

    /**
     * Get configuration value.
     *
     * @return array|null
     */
    public function getValue(): ?array
    {
        return $this->configuration->getValue();
    }

    /**
     * Set configuration value.
     *
     * @param array $value The new configuration.
     *
     * @return $this
     */
    public function setValue(array $value): self
    {
        $this->configuration->setValue($value);

        return $this;
    }

    /**
     * Save the configuration.
     *
     * This method only persist wrapped entity but not flush it.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(): void
    {
        $this->manager->persist($this->configuration);
    }

    /**
     * Save and flush configuration.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveNow(): void
    {
        $this->save();
        $this->manager->flush();
    }

    /**
     * Delete this configuration.
     */
    public function delete(): void
    {
        $this->manager->remove($this->configuration);
        $this->manager->flush();
    }
}
