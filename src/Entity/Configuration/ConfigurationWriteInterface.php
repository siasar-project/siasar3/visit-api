<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity\Configuration;

/**
 * Read and write configuration interface.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
interface ConfigurationWriteInterface extends ConfigurationReadInterface
{
    /**
     * Set configuration value.
     *
     * @param array $value The new configuration.
     *
     * @return $this
     */
    public function setValue(array $value): self;

    /**
     * Save the configuration.
     *
     * This method only persist wrapped entity but not flush it.
     *
     * @return void
     */
    public function save(): void;

    /**
     * Save and flush the configuration.
     *
     * @return void
     */
    public function saveNow(): void;

    /**
     * Delete this configuration.
     */
    public function delete(): void;
}
