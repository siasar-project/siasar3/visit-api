<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity\Configuration;

use Doctrine\Persistence\ObjectManager;

/**
 * Entity to configuration interface.
 *
 * Allow convert an entity instance into a configuration.
 * This entities must use 'code' how ID field, primary key.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
interface EntityToConfigurationInterface
{
    /**
     * Get a specific configuration ID from the current entity.
     *
     * @return string
     */
    public function configuratioGetConfigurationId(): string;

    /**
     * Get this entity how an array.
     *
     * @return array
     */
    public function configuratioGetConfigurationValue(): array;

    /**
     * Create an entity with the configuration values.
     *
     * @param array         $value         The configuration values.
     * @param ObjectManager $objectManager Object manager.
     *
     * @return self
     */
    public static function createFromConfigurationValue(array $value, ObjectManager $objectManager): self;

    /**
     * Update this entity with the configuration values.
     *
     * @param array         $value         The configuration values.
     * @param ObjectManager $objectManager Object manager.
     *
     * @return self
     */
    public function updateFromConfigurationValue(array $value, ObjectManager $objectManager): self;

    /**
     * Prepare the instance to be removed from database.
     *
     * @param array         $value         The configuration values.
     * @param ObjectManager $objectManager Object manager.
     *
     * @return $this
     */
    public function preDeleteFromConfigurationValue(array $value, ObjectManager $objectManager): self;
}
