<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Forms\FormReferenceEntityInterface;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\CountryRepository;
use App\Resolver\CountryMutationResolver;
use App\Service\ConfigurationService;
use App\Traits\FormReferenceStartingTrait;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Configuration\EntityToConfigurationInterface;
use Doctrine\Persistence\ObjectManager;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotations\SecuredRepository;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * Country entity.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ORM\Entity(repositoryClass=App\Repository\CountryRepository::class)
 *
 * @SecuredRepository(class=CountryRepositorySecured::class)
 *
 * @see https://api-platform.com/docs/core/graphql/#custom-queries
 */
#[ApiResource(
    collectionOperations: [
        "get" => [
            "method" => "GET",
            "security_get_denormalize" => "is_granted('READ', object)",
        ],
    ],
    graphql: [
        // Auto-generated queries and mutations
        'item_query',
        'collection_query',
        // 'create',
        'update',
        'delete',
        // Custom mutations.
        'create' => [
            'mutation' => CountryMutationResolver::class,
            'args' => [
                'code' => ['type' => 'String!', 'description' => 'Country code' ],
                'name' => ['type' => 'String!', 'description' => 'Country name' ],
            ],
        ],
    ],
    itemOperations: [
        "GET" => [
            "method" => "GET",
            "security" => "is_granted('READ', object)",
        ],
        "PUT" => [ "method" => "PUT", "security" => "is_granted('UPDATE', object)" ],
        "PATCH" => [ "method" => "PATCH", "security" => "is_granted('UPDATE', object)" ],
    ],
    denormalizationContext: [ 'groups' => ['country:write']],
    normalizationContext: [ 'groups' => ['country:read']],
)]
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'partial', 'name' => 'partial', 'level' => 'exact'])]
class Country implements EntityToConfigurationInterface, FormReferenceEntityInterface
{
    use GetContainerTrait;
    use StringTranslationTrait;
    use FormReferenceStartingTrait;

    /**
     * This property is required to allow use this trait in Entities
     * and workaround normalization errors.
     *
     * @Ignore
     */
    protected $container;

    /**
     * This property is required to allow use this trait in Entities
     * and workaround normalization errors.
     *
     * @Ignore
     */
    protected $kernelContainer;

    /**
     * This property is required to allow use this trait in Entities
     * and workaround normalization errors.
     *
     * @Ignore
     */
    protected $kernel;

    /**
     * Form level 1 - Standard
     *
     * @var int
     */
    const FORM_LEVEL_1 = 1;

    /**
     * Form level 2 - Intermediate
     *
     * @var int
     */
    const FORM_LEVEL_2 = 2;

    /**
     * Form level 3 - Advanced
     *
     * @var int
     */
    const FORM_LEVEL_3 = 3;

    /**
     * Form levels enumeration.
     *
     * @var string[]
     */
    const FORM_LEVEL_LABELS = [
        self::FORM_LEVEL_1 => 'Level 1: Standard',
        self::FORM_LEVEL_2 => 'Level 2: Intermediate',
        self::FORM_LEVEL_3 => 'Level 3: Advanced',
    ];

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="code",
     *     type="string",
     *     length=50,
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @ApiProperty(identifier=true)
     */
    #[Groups(["country:read", "user:read"])]
    protected string $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(["country:read", "country:write", "user:read"])]
    protected string $name;

    /**
     * @ORM\ManyToMany(targetEntity=Language::class, cascade={}, fetch="EXTRA_LAZY")
     * @ORM\JoinTable(joinColumns={@ORM\JoinColumn(name="country_code", referencedColumnName="code")})
     */
    #[Groups(["country:read", "country:write"])]
    protected $languages;

    /**
     * @ORM\ManyToOne(targetEntity=File::class, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write", "user:read"])]
    protected $flag;

    /**
     * @ORM\OneToMany(targetEntity=TreatmentTechnologySedimentation::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $treatmentTechnologySedimentations;

    /**
     * @ORM\OneToMany(targetEntity=TreatmentTechnologyCoagFloccu::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $treatmentTechnologyCoagFloccus;

    /**
     * @ORM\OneToMany(targetEntity=TreatmentTechnologyDesalination::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $treatmentTechnologyDesalinations;

    /**
     * @ORM\OneToMany(targetEntity=TreatmentTechnologyAeraOxida::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $treatmentTechnologyAeraOxidas;

    /**
     * @ORM\OneToMany(targetEntity=TreatmentTechnologyFiltration::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $treatmentTechnologyFiltrations;

    /**
     * @ORM\OneToMany(targetEntity=DistributionMaterial::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $distributionMaterials;

    /**
     * @ORM\OneToMany(targetEntity=Material::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $materials;

    /**
     * @ORM\OneToMany(targetEntity=CommunityService::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $communityServices;

    /**
     * @ORM\OneToMany(targetEntity=InterventionStatus::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $interventionStatuses;

    /**
     * @ORM\OneToMany(targetEntity=WaterQualityInterventionType::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $waterQualityInterventionTypes;

    /**
     * @ORM\OneToMany(targetEntity=WaterQualityEntity::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $waterQualityEntities;

    /**
     * @ORM\OneToMany(targetEntity=FunctionsCarriedOutTap::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $functionsCarriedOutTaps;

    /**
     * @ORM\OneToMany(targetEntity=FunctionsCarriedOutWsp::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $functionsCarriedOutWsps;

    /**
     * @ORM\OneToMany(targetEntity=FunderIntervention::class, mappedBy="country", fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $funderInterventions;

    /**
     * @ORM\OneToMany(targetEntity=TypeIntervention::class, mappedBy="country", fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $typeInterventions;

    /**
     * @ORM\OneToMany(targetEntity=InstitutionIntervention::class, mappedBy="country", fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $institutionInterventions;

    /**
     * @ORM\OneToMany(
     *     targetEntity=AdministrativeDivisionType::class,
     *     cascade={"persist", "remove"},
     *     mappedBy="country",
     *     orphanRemoval=true,
     *     fetch="EXTRA_LAZY"
     * )
     */
    #[Groups(["country:read"])]
    protected $divisionTypes;

    /**
     * Main official language.
     *
     * @ORM\ManyToOne(targetEntity=OfficialLanguage::class)
     */
    #[Groups(["country:read", "country:write"])]
    protected $mainOfficialLanguage;

    /**
     * @ORM\OneToMany(targetEntity=OfficialLanguage::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $officialLanguages;

    /**
     * Main concentration unit.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["country:read", "country:write"])]
    protected $mainUnitConcentration;

    /**
     * @ORM\Column(type="json")
     */
    #[Groups(["country:read", "country:write"])]
    protected $unitConcentrations;

    /**
     * Main flow unit.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["country:read", "country:write"])]
    protected $mainUnitFlow;

    /**
     * @ORM\Column(type="json")
     */
    #[Groups(["country:read", "country:write"])]
    protected $unitFlows;

    /**
     * Main length unit.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["country:read", "country:write"])]
    protected $mainUnitLength;

    /**
     * @ORM\Column(type="json")
     */
    #[Groups(["country:read", "country:write"])]
    protected $unitLengths;

    /**
     * Main volume unit.
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(["country:read", "country:write"])]
    protected $mainUnitVolume;

    /**
     * @ORM\Column(type="json")
     */
    #[Groups(["country:read", "country:write"])]
    protected $unitVolumes;

    /**
     * @ORM\OneToMany(targetEntity=Ethnicity::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $ethnicities;

    /**
     * @ORM\OneToMany(targetEntity=Currency::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $currencies;

    /**
     * @ORM\OneToMany(targetEntity=DisinfectingSubstance::class, mappedBy="country", orphanRemoval=true, cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $disinfectingSubstances;

    /**
     * @ORM\OneToMany(targetEntity=TypologyChlorinationInstallation::class, mappedBy="country", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $typologyChlorinationInstallations;

    /**
     * @ORM\OneToMany(targetEntity=GeographicalScope::class, mappedBy="country", orphanRemoval=true, cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $geographicalScopes;

    /**
     * @ORM\OneToMany(targetEntity=TypeTap::class, mappedBy="country", orphanRemoval=true, cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $typeTaps;

    /**
     * @ORM\OneToMany(targetEntity=TypeHealthcareFacility::class, mappedBy="country", orphanRemoval=true, cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $typeHealthcareFacilities;

    /**
     * @ORM\OneToMany(targetEntity=ProgramIntervention::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $programInterventions;

    /**
     * @ORM\OneToMany(targetEntity=TechnicalAssistanceProvider::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $technicalAssistanceProviders;

    /**
     * @ORM\OneToMany(targetEntity=AdministrativeDivision::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     *
     * @Ignore
     */
    protected $administrativeDivisions;

    /**
     * Form levels.
     *
     * Example: [{"form.id"': {"level": 1}}]
     *
     * @ORM\Column(type="json", nullable=false)
     */
    #[Groups(["country:read", "country:write"])]
    protected array $formLevel = [];

    /**
     * Mutateble field settings.
     *
     * Example: {
     *      "form.id": {
     *          "field_id": {
     *              "options": {
     *                  "key": "label",
     *                  "key1": "label 1"
     *              },
     *              "mutate_to": "long_text",
     *              "true_label": "Yes",
     *              "false_label": "No",
     *              "show_labels": true
     *          }
     *      }
     * }
     *
     * @ORM\Column(type="json")
     */
    #[Groups(["country:read", "country:write"])]
    protected array $mutatebleFields;

    /**
     * @ORM\OneToMany(targetEntity=PumpType::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $pumpTypes;

    /**
     * @ORM\OneToMany(targetEntity=DefaultDiameter::class, mappedBy="country", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $defaultDiameters;

    /**
     * @ORM\OneToMany(targetEntity=SpecialComponent::class, mappedBy="country", orphanRemoval=true, cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    #[Groups(["country:read", "country:write"])]
    protected $specialComponents;

    /**
     * Country constructor.
     *
     * @param ?string $code Country code.
     */
    public function __construct(?string $code = null)
    {
        if ($code) {
            $this->code = $code;
        }
        $this->languages = new ArrayCollection();
        $this->treatmentTechnologyAeraOxidas = new ArrayCollection();
        $this->treatmentTechnologyFiltrations = new ArrayCollection();
        $this->treatmentTechnologyDesalinations = new ArrayCollection();
        $this->treatmentTechnologyCoagFloccus = new ArrayCollection();
        $this->treatmentTechnologySedimentations = new ArrayCollection();
        $this->materials = new ArrayCollection();
        $this->distributionMaterials = new ArrayCollection();
        $this->communityServices = new ArrayCollection();
        $this->interventionStatuses = new ArrayCollection();
        $this->waterQualityEntities = new ArrayCollection();
        $this->waterQualityInterventionTypes = new ArrayCollection();
        $this->functionsCarriedOutTaps = new ArrayCollection();
        $this->functionsCarriedOutWsps = new ArrayCollection();
        $this->typeInterventions = new ArrayCollection();
        $this->institutionInterventions = new ArrayCollection();
        $this->divisionTypes = new ArrayCollection();
        $this->mainOfficialLanguage = null;
        $this->officialLanguages = new ArrayCollection();
        $this->ethnicities = new ArrayCollection();
        $this->currencies = new ArrayCollection();
        $this->geographicalScopes = new ArrayCollection();
        $this->typeTaps = new ArrayCollection();
        $this->typeHealthcareFacilities = new ArrayCollection();
        $this->disinfectingSubstances = new ArrayCollection();
        $this->typologyChlorinationInstallations = new ArrayCollection();
        $this->programInterventions = new ArrayCollection();
        $this->technicalAssistanceProviders = new ArrayCollection();
        $this->administrativeDivisions = new ArrayCollection();
        $this->pumpTypes = new ArrayCollection();
        $this->defaultDiameters = new ArrayCollection();
        $this->specialComponents = new ArrayCollection();
        $this->mainUnitConcentration = '';
        $this->unitConcentrations = [];
        $this->mainUnitFlow = '';
        $this->unitFlows = [];
        $this->mainUnitLength = '';
        $this->unitLengths = [];
        $this->mainUnitVolume = '';
        $this->unitVolumes = [];
        $this->formLevel = [];
        $this->mutatebleFields = [];
    }

    /**
     * Country code.
     *
     * @return ?string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * Country code.
     *
     * @return ?string
     */
    public function getId(): ?string
    {
        return $this->getCode();
    }

    /**
     * Get country name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set country name.
     *
     * @param string $name Name.
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get country languages.
     *
     * @return Collection|Language[]
     */
    public function getLanguages(): Collection
    {
        return $this->languages;
    }

    /**
     * Add a new language in this country.
     *
     * The language must exist before add here.
     *
     * @param Language $language Language.
     *
     * @return $this
     */
    public function addLanguage(Language $language): self
    {
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
        }

        return $this;
    }

    /**
     * Remove a language from this country.
     *
     * @param Language $language Language.
     *
     * @return $this
     */
    public function removeLanguage(Language $language): self
    {
        $this->languages->removeElement($language);

        return $this;
    }

    /**
     * Remove all languages from this countr.
     *
     * @return $this
     */
    public function cleanLanguages(): self
    {
        $this->languages->clear();

        return $this;
    }

    /**
     * Get flag.
     *
     * @return File|null
     */
    public function getFlag(): ?File
    {
        return $this->flag;
    }

    /**
     * Set flag file.
     *
     * @param File|null $flag
     *
     * @return $this
     */
    public function setFlag(?File $flag): self
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Country configuration ID.
     *
     * @return string
     */
    public function configuratioGetConfigurationId(): string
    {
        return strtolower((str_replace('\\', '.', self::class)).'.'.$this->code);
    }

    /**
     * Get this entity how an array.
     *
     * @return array
     */
    public function configuratioGetConfigurationValue(): array
    {
        $resp = [
            'code' => $this->code,
            'type' => self::class,
            'name' => $this->name,
            'languages' => [],
            'divisionTypes' => [],
            'requires' => ['system.languages'],
        ];

        if ($this->getFlag()) {
            $resp['flag'] = $this->getFlag()->getFileName();
        }

        /** @var Language $langauge */
        foreach ($this->languages as $langauge) {
            $resp['languages'][] = $langauge->getId();
        }

        /** @var AdministrativeDivisionType $divisionType */
        foreach ($this->divisionTypes as $divisionType) {
            $resp['divisionTypes'][] = $divisionType->getName();
        }

        return $resp;
    }

    /**
     * Create an entity with the configuration values.
     *
     * @param array         $value         The configuration values.
     * @param ObjectManager $objectManager Object manager.
     *
     * @return self
     */
    public static function createFromConfigurationValue(array $value, ObjectManager $objectManager): self
    {
        if (!isset($value['type']) || $value['type'] !== self::class) {
            throw new \Exception(
                sprintf(
                    'Invalid createFromConfigurationValue in class "%s" with values: %s',
                    self::class,
                    print_r($value, true)
                )
            );
        }

        $resp = new Country($value['code']);
        $resp->setName($value['name']);

        if (isset($value['flag'])) {
            $flagPath = 'assets://flags/'.$value['flag'];
            if (is_file($flagPath)) {
                $fileRepository  = $objectManager->getRepository(File::class);
                $flag = $fileRepository->create($flagPath, null, false);
                $resp->setFlag($flag);
            }
        }

        if (isset($value['languages'])) {
            $languageRepository  = $objectManager->getRepository(Language::class);
            foreach ($value['languages'] as $langId) {
                $language = $languageRepository->find($langId);
                $resp->addLanguage($language);
            }
        }

        if (isset($value['divisionTypes']) && is_array($value['divisionTypes'])) {
            foreach ($value['divisionTypes'] as $key => $typeName) {
                $level = $key + 1;
                $adtRepository = $objectManager->getRepository(AdministrativeDivisionType::class);
                $divisionType = $adtRepository->create($typeName, $level);
                $resp->addDivisionType($divisionType);
            }
        }

        return $resp;
    }

    /**
     * Update this entity with the configuration values.
     *
     * @param array         $value         The configuration values.
     * @param ObjectManager $objectManager Object manager.
     *
     * @return self
     */
    public function updateFromConfigurationValue(array $value, ObjectManager $objectManager): self
    {
        // $value was validated in a pre-update event.
        $this->name = $value['name'];

        // Update flag file.
        $fileRepository  = $objectManager->getRepository(File::class);
        $notUpdate = false;
        if (isset($value['flag']) && $this->getFlag()) {
            // Update
            $notUpdate = ($this->getFlag()->getFileName() === $value['flag']);
        }

        if (!$notUpdate) {
            if ($this->getFlag()) {
                // Remove old flag.
                $fileRepository->remove($this->flag);
                $this->setFlag(null);
            }

            if (isset($value['flag'])) {
                // Add the new flag.
                $flagPath = 'assets://flags/'.$value['flag'];
                if (is_file($flagPath)) {
                    $flag = $fileRepository->create($flagPath, null, false);
                    $this->setFlag($flag);
                }
            }
        }

        if (isset($value['languages'])) {
            $this->cleanLanguages();
            $languageRepository  = $objectManager->getRepository(Language::class);
            foreach ($value['languages'] as $langId) {
                $language = $languageRepository->find($langId);
                $this->addLanguage($language);
            }
        }

        if (isset($value['divisionTypes']) && is_array($value['divisionTypes'])) {
            $adtRepository = $objectManager->getRepository(AdministrativeDivisionType::class);
            $this->divisionTypes->clear();
            foreach ($value['divisionTypes'] as $key => $typeName) {
                $level = $key + 1;
                $divisionType = $adtRepository->create($typeName, $level);
                $this->addDivisionType($divisionType);
            }
        }

        return $this;
    }

    /**
     * Prepare the instance to be removed from database.
     *
     * @param array         $value         The configuration values.
     * @param ObjectManager $objectManager Object manager.
     *
     * @return $this
     */
    public function preDeleteFromConfigurationValue(array $value, ObjectManager $objectManager): self
    {
        $this->cleanLanguages();
        $this->clearDivisionTypes();

        return $this;
    }

    /**
     * Human friendly entity representation.
     *
     * @return string
     */
    public function __toString(): string
    {
        return self::class.":{$this->code}";
    }

    /**
     * Get default Country flag icon.
     *
     * @return string
     *
     * @Ignore()
     */
    public function getDefaultFlagPath(): string
    {
        return 'assets://flags/'.$this->code.'.png';
    }

    /**
     * @return Collection|FunctionsCarriedOutWsp[]
     */
    public function getFunctionsCarriedOutWsps(): Collection
    {
        return $this->functionsCarriedOutWsps;
    }

    /**
     * add a FunctionsCarriedOutWsp to a country
     *
     * @param FunctionsCarriedOutWsp $instance
     *
     * @return $this
     */
    public function addFunctionsCarriedOutWsp(FunctionsCarriedOutWsp $instance): self
    {
        if (!$this->functionsCarriedOutWsps->contains($instance)) {
            $this->functionsCarriedOutWsps[] = $instance;
            $instance->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a FunctionsCarriedOutWsp to a country
     *
     * @param FunctionsCarriedOutWsp $instance
     *
     * @return $this
     */
    public function removeFunctionsCarriedOutWsp(FunctionsCarriedOutWsp $instance): self
    {
        if ($this->functionsCarriedOutWsps->removeElement($instance)) {
            // set the owning side to null (unless already changed)
            if ($instance->getCountry() === $this) {
                $instance->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FunctionsCarriedOutTap[]
     */
    public function getFunctionsCarriedOutTaps(): Collection
    {
        return $this->functionsCarriedOutTaps;
    }

    /**
     * add a FunctionsCarriedOutTap to a country
     *
     * @param FunctionsCarriedOutTap $instance
     *
     * @return $this
     */
    public function addFunctionsCarriedOutTap(FunctionsCarriedOutTap $instance): self
    {
        if (!$this->functionsCarriedOutTaps->contains($instance)) {
            $this->functionsCarriedOutTaps[] = $instance;
            $instance->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a FunctionsCarriedOutTap to a country
     *
     * @param FunctionsCarriedOutTap $instance
     *
     * @return $this
     */
    public function removeFunctionsCarriedOutTap(FunctionsCarriedOutTap $instance): self
    {
        if ($this->functionsCarriedOutTaps->removeElement($instance)) {
            // set the owning side to null (unless already changed)
            if ($instance->getCountry() === $this) {
                $instance->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TreatmentTechnologySedimentation[]
     */
    public function getTreatmentTechnologySedimentations(): Collection
    {
        return $this->treatmentTechnologySedimentations;
    }

    /**
     * add a TreatmentTechnologySedimentation to a country
     *
     * @param TreatmentTechnologySedimentation $tech
     *
     * @return $this
     */
    public function addTreatmentTechnologySedimentation(TreatmentTechnologySedimentation $tech): self
    {
        if (!$this->treatmentTechnologySedimentations->contains($tech)) {
            $this->treatmentTechnologySedimentations[] = $tech;
            $tech->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TreatmentTechnologySedimentation to a country
     *
     * @param TreatmentTechnologySedimentation $tech
     *
     * @return $this
     */
    public function removeTreatmentTechnologySedimentation(TreatmentTechnologySedimentation $tech): self
    {
        if ($this->treatmentTechnologySedimentations->removeElement($tech)) {
            // set the owning side to null (unless already changed)
            if ($tech->getCountry() === $this) {
                $tech->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TreatmentTechnologyDesalination[]
     */
    public function getTreatmentTechnologyDesalinations(): Collection
    {
        return $this->treatmentTechnologyDesalinations;
    }

    /**
     * add a TreatmentTechnologyDesalination to a country
     *
     * @param TreatmentTechnologyDesalination $tech
     *
     * @return $this
     */
    public function addTreatmentTechnologyDesalination(TreatmentTechnologyDesalination $tech): self
    {
        if (!$this->treatmentTechnologyDesalinations->contains($tech)) {
            $this->treatmentTechnologyDesalinations[] = $tech;
            $tech->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TreatmentTechnologyDesalination to a country
     *
     * @param TreatmentTechnologyDesalination $tech
     *
     * @return $this
     */
    public function removeTreatmentTechnologyDesalination(TreatmentTechnologyDesalination $tech): self
    {
        if ($this->treatmentTechnologyDesalinations->removeElement($tech)) {
            // set the owning side to null (unless already changed)
            if ($tech->getCountry() === $this) {
                $tech->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TreatmentTechnologyCoagFloccu[]
     */
    public function getTreatmentTechnologyCoagFloccus(): Collection
    {
        return $this->treatmentTechnologyCoagFloccus;
    }

    /**
     * add a TreatmentTechnologyCoagFloccu to a country
     *
     * @param TreatmentTechnologyCoagFloccu $tech
     *
     * @return $this
     */
    public function addTreatmentTechnologyCoagFloccu(TreatmentTechnologyCoagFloccu $tech): self
    {
        if (!$this->treatmentTechnologyCoagFloccus->contains($tech)) {
            $this->treatmentTechnologyCoagFloccus[] = $tech;
            $tech->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TreatmentTechnologyCoagFloccu to a country
     *
     * @param TreatmentTechnologyCoagFloccu $tech
     *
     * @return $this
     */
    public function removeTreatmentTechnologyCoagFloccu(TreatmentTechnologyCoagFloccu $tech): self
    {
        if ($this->treatmentTechnologyCoagFloccus->removeElement($tech)) {
            // set the owning side to null (unless already changed)
            if ($tech->getCountry() === $this) {
                $tech->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TreatmentTechnologyFiltration[]
     */
    public function getTreatmentTechnologyFiltrations(): Collection
    {
        return $this->treatmentTechnologyFiltrations;
    }

    /**
     * add a TreatmentTechnologyFiltration to a country
     *
     * @param TreatmentTechnologyFiltration $tech
     *
     * @return $this
     */
    public function addTreatmentTechnologyFiltration(TreatmentTechnologyFiltration $tech): self
    {
        if (!$this->treatmentTechnologyFiltrations->contains($tech)) {
            $this->treatmentTechnologyFiltrations[] = $tech;
            $tech->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TreatmentTechnologyFiltration to a country
     *
     * @param TreatmentTechnologyFiltration $tech
     *
     * @return $this
     */
    public function removeTreatmentTechnologyFiltration(TreatmentTechnologyFiltration $tech): self
    {
        if ($this->treatmentTechnologyFiltrations->removeElement($tech)) {
            // set the owning side to null (unless already changed)
            if ($tech->getCountry() === $this) {
                $tech->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TreatmentTechnologyAeraOxida[]
     */
    public function getTreatmentTechnologyAeraOxidas(): Collection
    {
        return $this->treatmentTechnologyAeraOxidas;
    }

    /**
     * add a TreatmentTechnologyAeraOxida to a country
     *
     * @param TreatmentTechnologyAeraOxida $tech
     *
     * @return $this
     */
    public function addTreatmentTechnologyAeraOxida(TreatmentTechnologyAeraOxida $tech): self
    {
        if (!$this->treatmentTechnologyAeraOxidas->contains($tech)) {
            $this->treatmentTechnologyAeraOxidas[] = $tech;
            $tech->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TreatmentTechnologyAeraOxida to a country
     *
     * @param TreatmentTechnologyAeraOxida $tech
     *
     * @return $this
     */
    public function removeTreatmentTechnologyAeraOxida(TreatmentTechnologyAeraOxida $tech): self
    {
        if ($this->treatmentTechnologyAeraOxidas->removeElement($tech)) {
            // set the owning side to null (unless already changed)
            if ($tech->getCountry() === $this) {
                $tech->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DistributionMaterial[]
     */
    public function getDistributionMaterials(): Collection
    {
        return $this->distributionMaterials;
    }

    /**
     * add a DistributionMaterial to a country
     *
     * @param DistributionMaterial $material
     *
     * @return $this
     */
    public function addDistributionMaterial(DistributionMaterial $material): self
    {
        if (!$this->distributionMaterials->contains($material)) {
            $this->distributionMaterials[] = $material;
            $material->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a Distribution Material to a country
     *
     * @param DistributionMaterial $material
     *
     * @return $this
     */
    public function removeDistributionMaterial(DistributionMaterial $material): self
    {
        if ($this->distributionMaterials->removeElement($material)) {
            // set the owning side to null (unless already changed)
            if ($material->getCountry() === $this) {
                $material->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WaterQualityInterventionType[]
     */
    public function getWaterQualityInterventionTypes(): Collection
    {
        return $this->waterQualityInterventionTypes;
    }

    /**
     * add a WaterQualityInterventionType to a country
     *
     * @param WaterQualityInterventionType $entity
     *
     * @return $this
     */
    public function addWaterQualityInterventionType(WaterQualityInterventionType $entity): self
    {
        if (!$this->waterQualityInterventionTypes->contains($entity)) {
            $this->waterQualityInterventionTypes[] = $entity;
            $entity->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a WaterQualityInterventionType to a country
     *
     * @param WaterQualityInterventionType $entity
     *
     * @return $this
     */
    public function removeWaterQualityInterventionType(WaterQualityInterventionType $entity): self
    {
        if ($this->waterQualityInterventionTypes->removeElement($entity)) {
            // set the owning side to null (unless already changed)
            if ($entity->getCountry() === $this) {
                $entity->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|WaterQualityEntity[]
     */
    public function getWaterQualityEntities(): Collection
    {
        return $this->waterQualityEntities;
    }

    /**
     * add a WaterQualityEntity to a country
     *
     * @param WaterQualityEntity $entity
     *
     * @return $this
     */
    public function addWaterQualityEntity(WaterQualityEntity $entity): self
    {
        if (!$this->waterQualityEntities->contains($entity)) {
            $this->waterQualityEntities[] = $entity;
            $entity->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a WaterQualityEntity to a country
     *
     * @param WaterQualityEntity $entity
     *
     * @return $this
     */
    public function removeWaterQualityEntity(WaterQualityEntity $entity): self
    {
        if ($this->waterQualityEntities->removeElement($entity)) {
            // set the owning side to null (unless already changed)
            if ($entity->getCountry() === $this) {
                $entity->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InterventionStatus[]
     */
    public function getInterventionStatuses(): Collection
    {
        return $this->interventionStatuses;
    }

    /**
     * add a InterventionStatus to a country
     *
     * @param InterventionStatus $status
     *
     * @return $this
     */
    public function addInterventionStatus(InterventionStatus $status): self
    {
        if (!$this->interventionStatuses->contains($status)) {
            $this->interventionStatuses[] = $status;
            $status->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a InterventionStatus to a country
     *
     * @param InterventionStatus $status
     *
     * @return $this
     */
    public function removeInterventionStatus(InterventionStatus $status): self
    {
        if ($this->interventionStatuses->removeElement($status)) {
            // set the owning side to null (unless already changed)
            if ($status->getCountry() === $this) {
                $status->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CommunityService[]
     */
    public function getCommunityServices(): Collection
    {
        return $this->communityServices;
    }

    /**
     * add a CommunityService to a country
     *
     * @param CommunityService $service
     *
     * @return $this
     */
    public function addCommunityService(CommunityService $service): self
    {
        if (!$this->communityServices->contains($service)) {
            $this->communityServices[] = $service;
            $service->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a CommunityService to a country
     *
     * @param CommunityService $service
     *
     * @return $this
     */
    public function removeCommunityServices(CommunityService $service): self
    {
        if ($this->communityServices->removeElement($service)) {
            // set the owning side to null (unless already changed)
            if ($service->getCountry() === $this) {
                $service->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Material[]
     */
    public function getMaterials(): Collection
    {
        return $this->materials;
    }

    /**
     * add a material to a country
     * @param Material $material
     *
     * @return $this
     */
    public function addMaterial(Material $material): self
    {
        if (!$this->materials->contains($material)) {
            $this->materials[] = $material;
            $material->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a material to a country
     *
     * @param Material $material
     *
     * @return $this
     */
    public function removeMaterial(Material $material): self
    {
        if ($this->materials->removeElement($material)) {
            // set the owning side to null (unless already changed)
            if ($material->getCountry() === $this) {
                $material->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FunderIntervention[]
     */
    public function getFunderInterventions(): Collection
    {
        return $this->funderInterventions;
    }

    /**
     * add a funderIntervention to a country
     * @param Material $funderIntervention
     *
     * @return $this
     */
    public function addFunderIntervention(FunderIntervention $funderIntervention): self
    {
        if (!$this->funderInterventions->contains($funderIntervention)) {
            $this->funderInterventions[] = $funderIntervention;
            $funderIntervention->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a funderIntervention to a country
     * @param Material $funderIntervention
     *
     * @return $this
     */
    public function removeFunderIntervention(FunderIntervention $funderIntervention): self
    {
        if ($this->funderInterventions->removeElement($funderIntervention)) {
            // set the owning side to null (unless already changed)
            if ($funderIntervention->getCountry() === $this) {
                $funderIntervention->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeIntervention[]
     */
    public function getTypeInterventions(): Collection
    {
        return $this->typeInterventions;
    }

    /**
     * add a typeIntervention to a country
     * @param TypeIntervention $typeIntervention
     *
     * @return $this
     */
    public function addTypeIntervention(TypeIntervention $typeIntervention): self
    {
        if (!$this->typeInterventions->contains($typeIntervention)) {
            $this->typeInterventions[] = $typeIntervention;
            $typeIntervention->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a typeIntervention to a country
     *
     * @param TypeIntervention $typeIntervention
     *
     * @return $this
     */
    public function removeTypeIntervention(TypeIntervention $typeIntervention): self
    {
        if ($this->typeInterventions->removeElement($typeIntervention)) {
            // set the owning side to null (unless already changed)
            if ($typeIntervention->getCountry() === $this) {
                $typeIntervention->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InstitutionIntervention[]
     */
    public function getInstitutionInterventions(): Collection
    {
        return $this->institutionInterventions;
    }

    /**
     * add a institutionIntervention to a country
     * @param InstitutionIntervention $institutionIntervention
     *
     * @return $this
     */
    public function addInstitutionIntervention(InstitutionIntervention $institutionIntervention): self
    {
        if (!$this->institutionInterventions->contains($institutionIntervention)) {
            $this->institutionInterventions[] = $institutionIntervention;
            $institutionIntervention->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a institutionIntervention to a country
     *
     * @param InstitutionIntervention $institutionIntervention
     *
     * @return $this
     */
    public function removeInstitutionIntervention(InstitutionIntervention $institutionIntervention): self
    {
        if ($this->institutionInterventions->removeElement($institutionIntervention)) {
            // set the owning side to null (unless already changed)
            if ($institutionIntervention->getCountry() === $this) {
                $institutionIntervention->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AdministrativeDivisionType[]
     */
    public function getDivisionTypes(): Collection
    {
        return $this->divisionTypes;
    }

    /**
     * Get administrative divisions maximum deep.
     *
     * @return int
     */
    public function getDeep(): int
    {
        return $this->divisionTypes->count();
    }

    /**
     * add a divisionType to a country
     * @param AdministrativeDivisionType $divisionType
     *
     * @return $this
     */
    public function addDivisionType(AdministrativeDivisionType $divisionType): self
    {
        if (!$this->divisionTypes->contains($divisionType)) {
            $this->divisionTypes[] = $divisionType;
            $divisionType->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a divisionType to a country
     *
     * @param AdministrativeDivisionType $divisionType
     *
     * @return $this
     */
    public function removeDivisionType(AdministrativeDivisionType $divisionType): self
    {
        if ($this->divisionTypes->removeElement($divisionType)) {
            // set the owning side to null (unless already changed)
            if ($divisionType->getCountry() === $this) {
                $divisionType->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * Remove all divisionTypes from this countr.
     *
     * @return $this
     */
    public function clearDivisionTypes(): self
    {
        $this->divisionTypes->clear();

        return $this;
    }

    /**
     * Get mainOfficialLanguage official language.
     *
     * @return OfficialLanguage | null
     */
    public function getMainOfficialLanguage(): ?OfficialLanguage
    {
        return $this->mainOfficialLanguage;
    }

    /**
     * Set mainOfficialLanguage official language.
     *
     * @param OfficialLanguage | null $mainOfficialLanguage
     *
     * @return $this
     */
    public function setMainOfficialLanguage(?OfficialLanguage $mainOfficialLanguage): self
    {
        if (null !== $mainOfficialLanguage && !$this->officialLanguages->contains($mainOfficialLanguage)) {
            throw new InvalidArgumentException(
                $this->t(
                    'Main Official Language "@mainOfficialLanguage" must be present in country official languages list',
                    [
                        '@mainOfficialLanguage' => $mainOfficialLanguage,
                    ]
                )
            );
        }

        $this->mainOfficialLanguage = $mainOfficialLanguage;

        return $this;
    }

    /**
     * @return Collection|OfficialLanguage[]
     */
    public function getOfficialLanguages(): Collection
    {
        return $this->officialLanguages;
    }

    /**
     * add a OfficialLanguage to a country
     * @param OfficialLanguage $officialLanguage
     *
     * @return $this
     */
    public function addOfficialLanguage(OfficialLanguage $officialLanguage): self
    {
        if (!$this->officialLanguages->contains($officialLanguage)) {
            $this->officialLanguages[] = $officialLanguage;
            $officialLanguage->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a OfficialLanguage to a country
     *
     * @param OfficialLanguage $officialLanguage
     *
     * @return $this
     */
    public function removeOfficialLanguage(OfficialLanguage $officialLanguage): self
    {
        if ($this->mainOfficialLanguage && $officialLanguage->getId() === $this->mainOfficialLanguage->getId()) {
            $this->setMainOfficialLanguage(null);
        }
        if ($this->officialLanguages->removeElement($officialLanguage)) {
            // set the owning side to null (unless already changed)
            if ($officialLanguage->getCountry() === $this) {
                $officialLanguage->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * Get main concentration unit.
     *
     * @return string
     */
    public function getMainUnitConcentration(): string
    {
        return $this->mainUnitConcentration;
    }

    /**
     * Set main concentration unit.
     *
     * @param string $mainUnitConcentration
     *
     * @return Country
     */
    public function setMainUnitConcentration(string $mainUnitConcentration): self
    {
        $this->validateMeasurementUnit('concentration', $mainUnitConcentration);
        $this->mainUnitConcentration = $mainUnitConcentration;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainUnitFlow(): string
    {
        return $this->mainUnitFlow;
    }

    /**
     * @param string $mainUnitFlow
     *
     * @return Country
     */
    public function setMainUnitFlow(string $mainUnitFlow): self
    {
        $this->validateMeasurementUnit('flow', $mainUnitFlow);
        $this->mainUnitFlow = $mainUnitFlow;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainUnitLength(): string
    {
        return $this->mainUnitLength;
    }

    /**
     * @param string $mainUnitLength
     *
     * @return Country
     */
    public function setMainUnitLength(string $mainUnitLength):self
    {
        $this->validateMeasurementUnit('length', $mainUnitLength);
        $this->mainUnitLength = $mainUnitLength;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainUnitVolume(): string
    {
        return $this->mainUnitVolume;
    }

    /**
     * @param string $mainUnitVolume
     *
     * @return Country
     */
    public function setMainUnitVolume(string $mainUnitVolume): self
    {
        $this->validateMeasurementUnit('volume', $mainUnitVolume);
        $this->mainUnitVolume = $mainUnitVolume;

        return $this;
    }

    /**
     * @return array
     */
    public function getUnitConcentrations(): array
    {
        return $this->unitConcentrations;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function addUnitConcentration(string $unit): self
    {
        $this->validateMeasurementUnit('concentration', $unit);
        $this->unitConcentrations[] = $unit;

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setUnitConcentrations(array $value): self
    {
        $this->cleanUnitConcentrations();
        foreach ($value as $unit) {
            $this->addUnitConcentration($unit);
        }

        return $this;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function removeUnitConcentration(string $unit): self
    {
        $tmp = [];
        foreach ($this->unitConcentrations as $key => $value) {
            if ($value !== $unit) {
                $tmp[] = $value;
            }
        }
        $this->unitConcentrations = $tmp;

        return $this;
    }

    /**
     * @return $this
     */
    public function cleanUnitConcentrations(): self
    {
        $this->unitConcentrations = [];

        return $this;
    }

    /**
     * @return array
     */
    public function getUnitFlows(): array
    {
        return $this->unitFlows;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function addUnitFlow(string $unit): self
    {
        $this->validateMeasurementUnit('flow', $unit);
        $this->unitFlows[] = $unit;

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setUnitFlows(array $value): self
    {
        $this->cleanUnitFlows();
        foreach ($value as $unit) {
            $this->addUnitFlow($unit);
        }

        return $this;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function removeUnitFlow(string $unit): self
    {
        $tmp = [];
        foreach ($this->unitFlows as $key => $value) {
            if ($value !== $unit) {
                $tmp[] = $value;
            }
        }
        $this->unitFlows = $tmp;

        return $this;
    }

    /**
     * @return $this
     */
    public function cleanUnitFlows(): self
    {
        $this->unitFlows = [];

        return $this;
    }

    /**
     * @return array
     */
    public function getUnitLengths(): array
    {
        return $this->unitLengths;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function addUnitLength(string $unit): self
    {
        $this->validateMeasurementUnit('length', $unit);
        $this->unitLengths[] = $unit;

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setUnitLengths(array $value): self
    {
        $this->cleanUnitLengths();
        foreach ($value as $unit) {
            $this->addUnitLength($unit);
        }

        return $this;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function removeUnitLength(string $unit): self
    {
        $tmp = [];
        foreach ($this->unitLengths as $key => $value) {
            if ($value !== $unit) {
                $tmp[] = $value;
            }
        }
        $this->unitLengths = $tmp;

        return $this;
    }

    /**
     * @return $this
     */
    public function cleanUnitLengths(): self
    {
        $this->unitLengths = [];

        return $this;
    }

    /**
     * @return array
     */
    public function getUnitVolumes(): array
    {
        return $this->unitVolumes;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function addUnitVolume(string $unit): self
    {
        $this->validateMeasurementUnit('volume', $unit);
        $this->unitVolumes[] = $unit;

        return $this;
    }

    /**
     * @param array $value
     *
     * @return $this
     */
    public function setUnitVolumes(array $value): self
    {
        $this->cleanUnitVolumes();
        foreach ($value as $unit) {
            $this->addUnitVolume($unit);
        }

        return $this;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function removeUnitVolume(string $unit): self
    {
        $tmp = [];
        foreach ($this->unitVolumes as $key => $value) {
            if ($value !== $unit) {
                $tmp[] = $value;
            }
        }
        $this->unitVolumes = $tmp;

        return $this;
    }

    /**
     * @return $this
     */
    public function cleanUnitVolumes(): self
    {
        $this->unitVolumes = [];

        return $this;
    }

    /**
     * @return Collection|Ethnicity[]
     */
    public function getEthnicities(): Collection
    {
        return $this->ethnicities;
    }

    /**
     * add a Ethnicity to a country
     * @param Ethnicity $ethnicity
     *
     * @return $this
     */
    public function addEthnicity(Ethnicity $ethnicity): self
    {
        if (!$this->ethnicities->contains($ethnicity)) {
            $this->ethnicities[] = $ethnicity;
            $ethnicity->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a Ethnicity to a country
     *
     * @param Ethnicity $ethnicity
     *
     * @return $this
     */
    public function removeEthnicity(Ethnicity $ethnicity): self
    {
        if ($this->ethnicities->removeElement($ethnicity)) {
            // set the owning side to null (unless already changed)
            if ($ethnicity->getCountry() === $this) {
                $ethnicity->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypologyChlorinationInstallation[]
     */
    public function getTypologyChlorinationInstallations(): Collection
    {
        return $this->typologyChlorinationInstallations;
    }

    /**
     * add a TypologyChlorinationInstallation to a country
     *
     * @param TypologyChlorinationInstallation $typologyChlorination
     *
     * @return $this
     */
    public function addTypologyChlorinationInstallation(TypologyChlorinationInstallation $typologyChlorination): self
    {
        if (!$this->typologyChlorinationInstallations->contains($typologyChlorination)) {
            $this->typologyChlorinationInstallations[] = $typologyChlorination;
            $typologyChlorination->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TypologyChlorinationInstallation to a country
     *
     * @param TypologyChlorinationInstallation $typologyChlorination
     *
     * @return $this
     */
    public function removeTypologyChlorinationInstallation(TypologyChlorinationInstallation $typologyChlorination): self
    {
        if ($this->typologyChlorinationInstallations->removeElement($typologyChlorination)) {
            // set the owning side to null (unless already changed)
            if ($typologyChlorination->getCountry() === $this) {
                $typologyChlorination->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypologyChlorinationInstallation[]
     */
    public function getDisinfectingSubstances(): Collection
    {
        return $this->disinfectingSubstances;
    }

    /**
     * add a DisinfectingSubstance to a country
     *
     * @param DisinfectingSubstance $disinfectingSubstance
     *
     * @return $this
     */
    public function addDisinfectingSubstance(DisinfectingSubstance $disinfectingSubstance): self
    {
        if (!$this->disinfectingSubstances->contains($disinfectingSubstance)) {
            $this->disinfectingSubstances[] = $disinfectingSubstance;
            $disinfectingSubstance->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a DisinfectingSubstance to a country
     *
     * @param DisinfectingSubstance $disinfectingSubstance
     *
     * @return $this
     */
    public function removeDisinfectingSubstance(DisinfectingSubstance $disinfectingSubstance): self
    {
        if ($this->disinfectingSubstances->removeElement($disinfectingSubstance)) {
            // set the owning side to null (unless already changed)
            if ($disinfectingSubstance->getCountry() === $this) {
                $disinfectingSubstance->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeHealthcareFacility[]
     */
    public function getTypeHealthcareFacilities(): Collection
    {
        return $this->typeHealthcareFacilities;
    }

    /**
     * add a Type Healthcare Facility to a country
     *
     * @param TypeHealthcareFacility $typeHealthcareFacility
     *
     * @return $this
     */
    public function addTypeHealthcareFacility(TypeHealthcareFacility $typeHealthcareFacility): self
    {
        if (!$this->typeHealthcareFacilities->contains($typeHealthcareFacility)) {
            $this->typeHealthcareFacilities[] = $typeHealthcareFacility;
            $typeHealthcareFacility->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a type Healthcare Facilities to a country
     *
     * @param TypeHealthcareFacility $typeHealthcareFacility
     *
     * @return $this
     */
    public function removeTypeHealthcareFacility(TypeHealthcareFacility $typeHealthcareFacility): self
    {
        if ($this->typeHealthcareFacilities->removeElement($typeHealthcareFacility)) {
            // set the owning side to null (unless already changed)
            if ($typeHealthcareFacility->getCountry() === $this) {
                $typeHealthcareFacility->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TypeTap[]
     */
    public function getTypeTaps(): Collection
    {
        return $this->typeTaps;
    }

    /**
     * add a TypeTap to a country
     *
     * @param TypeTap $typeTap
     *
     * @return $this
     */
    public function addTypeTap(TypeTap $typeTap): self
    {
        if (!$this->typeTaps->contains($typeTap)) {
            $this->typeTaps[] = $typeTap;
            $typeTap->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TypeTap to a country
     *
     * @param TypeTap $typeTap
     *
     * @return $this
     */
    public function removeTypeTap(TypeTap $typeTap): self
    {
        if ($this->typeTaps->removeElement($typeTap)) {
            // set the owning side to null (unless already changed)
            if ($typeTap->getCountry() === $this) {
                $typeTap->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GeographicalScope[]
     */
    public function getGeographicalScopes(): Collection
    {
        return $this->geographicalScopes;
    }

    /**
     * add a Geographical Scope to a country
     *
     * @param GeographicalScope $geographicalScope
     *
     * @return $this
     */
    public function addGeographicalScope(GeographicalScope $geographicalScope): self
    {
        if (!$this->geographicalScopes->contains($geographicalScope)) {
            $this->geographicalScopes[] = $geographicalScope;
            $geographicalScope->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a Geographical Scope to a country
     *
     * @param GeographicalScope $geographicalScope
     *
     * @return $this
     */
    public function removeGeographicalScope(GeographicalScope $geographicalScope): self
    {
        if ($this->geographicalScopes->removeElement($geographicalScope)) {
            // set the owning side to null (unless already changed)
            if ($geographicalScope->getCountry() === $this) {
                $geographicalScope->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Currency[]
     */
    public function getCurrencies(): Collection
    {
        return $this->currencies;
    }

    /**
     * add a Currency to a country
     * @param Currency $currency
     *
     * @return $this
     */
    public function addCurrency(Currency $currency): self
    {
        if (!$this->currencies->contains($currency)) {
            $this->currencies[] = $currency;
            $currency->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a Currency to a country
     *
     * @param Currency $currency
     *
     * @return $this
     */
    public function removeCurrency(Currency $currency): self
    {
        if ($this->currencies->removeElement($currency)) {
            // set the owning side to null (unless already changed)
            if ($currency->getCountry() === $this) {
                $currency->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProgramIntervention[]
     */
    public function getProgramInterventions(): Collection
    {
        return $this->programInterventions;
    }

    /**
     * add a ProgramIntervention to a country
     * @param ProgramIntervention $programIntervention
     *
     * @return $this
     */
    public function addProgramIntervention(ProgramIntervention $programIntervention): self
    {
        if (!$this->programInterventions->contains($programIntervention)) {
            $this->programInterventions[] = $programIntervention;
            $programIntervention->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a ProgramIntervention to a country
     *
     * @param ProgramIntervention $programIntervention
     *
     * @return $this
     */
    public function removeProgramIntervention(ProgramIntervention $programIntervention): self
    {
        if ($this->programInterventions->removeElement($programIntervention)) {
            // set the owning side to null (unless already changed)
            if ($programIntervention->getCountry() === $this) {
                $programIntervention->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TechnicalAssistanceProvider[]
     */
    public function getTechnicalAssistanceProviders(): Collection
    {
        return $this->technicalAssistanceProviders;
    }

    /**
     * add a TechnicalAssistanceProvider to a country
     * @param TechnicalAssistanceProvider $technicalAssistanceProvider
     *
     * @return $this
     */
    public function addTechnicalAssistanceProvider(TechnicalAssistanceProvider $technicalAssistanceProvider): self
    {
        if (!$this->technicalAssistanceProviders->contains($technicalAssistanceProvider)) {
            $this->technicalAssistanceProviders[] = $technicalAssistanceProvider;
            $technicalAssistanceProvider->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a TechnicalAssistanceProvider to a country
     *
     * @param TechnicalAssistanceProvider $technicalAssistanceProvider
     *
     * @return $this
     */
    public function removeTechnicalAssistanceProvider(TechnicalAssistanceProvider $technicalAssistanceProvider): self
    {
        if ($this->technicalAssistanceProviders->removeElement($technicalAssistanceProvider)) {
            // set the owning side to null (unless already changed)
            if ($technicalAssistanceProvider->getCountry() === $this) {
                $technicalAssistanceProvider->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|AdministrativeDivision[]
     *
     * @see https://stackoverflow.com/a/23458717/6385708
     */
    public function getAdministrativeDivisions(): Collection
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('parent', null));

        return $this->administrativeDivisions->matching($criteria);
    }

    /**
     * add a AdministrativeDivision to a country
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return $this
     */
    public function addAdministrativeDivision(AdministrativeDivision $administrativeDivision): self
    {
        if (!$this->administrativeDivisions->contains($administrativeDivision)) {
            $this->administrativeDivisions[] = $administrativeDivision;
            $administrativeDivision->setCountry($this);
        }

        return $this;
    }

    /**
     * remove a AdministrativeDivision to a country
     *
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return $this
     */
    public function removeAdministrativeDivision(AdministrativeDivision $administrativeDivision): self
    {
        if ($this->administrativeDivisions->removeElement($administrativeDivision)) {
            // set the owning side to null (unless already changed)
            if ($administrativeDivision->getCountry() === $this) {
                $administrativeDivision->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * Get mutateble field settings.
     *
     * @return mixed
     */
    public function getMutatebleFields(): mixed
    {
        return $this->mutatebleFields;
    }

    /**
     * Set mutateble field settings.
     *
     * @param array $value
     */
    public function setMutatebleFields(mixed $value): void
    {
        $this->mutatebleFields = $value;
    }

    /**
     * Get form level.
     *
     * @return array
     */
    public function getFormLevel(): array
    {
        return $this->formLevel;
    }

    /**
     * Set form level.
     *
     * @param array $formLevel
     */
    public function setFormLevel(array $formLevel): void
    {
        // Validate form levels.
        $value = [];
        foreach ($formLevel as $id => $form) {
            if (!isset($form['level'])) {
                $value[$id] = ['level' => 1];
            } else {
                $value[$id] = ['level' => $form['level']];
            }
            if (!isset($form['sdg'])) {
                $value[$id] += ['sdg' => false];
            } else {
                $value[$id] += ['sdg' => $form['sdg']];
            }
        }

        $this->formLevel = $value;
    }

    /**
     * @return Collection|PumpType[]
     */
    public function getPumpTypes(): Collection
    {
        return $this->pumpTypes;
    }

    /**
     * @param PumpType $pumpType
     *
     * @return $this
     */
    public function addPumpType(PumpType $pumpType): self
    {
        if (!$this->pumpTypes->contains($pumpType)) {
            $this->pumpTypes[] = $pumpType;
            $pumpType->setCountry($this);
        }

        return $this;
    }

    /**
     * @param PumpType $pumpType
     *
     * @return $this
     */
    public function removePumpType(PumpType $pumpType): self
    {
        if ($this->pumpTypes->removeElement($pumpType)) {
            // set the owning side to null (unless already changed)
            if ($pumpType->getCountry() === $this) {
                $pumpType->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DefaultDiameter[]
     */
    public function getDefaultDiameters(): Collection
    {
        return $this->defaultDiameters;
    }

    /**
     * @param DefaultDiameter $defaultDiameter
     *
     * @return $this
     */
    public function addDefaultDiameter(DefaultDiameter $defaultDiameter): self
    {
        if (!$this->defaultDiameters->contains($defaultDiameter)) {
            $this->defaultDiameters[] = $defaultDiameter;
            $defaultDiameter->setCountry($this);
        }

        return $this;
    }

    /**
     * @param DefaultDiameter $defaultDiameter
     *
     * @return $this
     */
    public function removeDefaultDiameter(DefaultDiameter $defaultDiameter): self
    {
        if ($this->defaultDiameters->removeElement($defaultDiameter)) {
            // set the owning side to null (unless already changed)
            if ($defaultDiameter->getCountry() === $this) {
                $defaultDiameter->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SpecialComponent[]
     */
    public function getSpecialComponents(): Collection
    {
        return $this->specialComponents;
    }

    /**
     * @param SpecialComponent $specialComponent
     *
     * @return $this
     */
    public function addSpecialComponent(SpecialComponent $specialComponent): self
    {
        if (!$this->specialComponents->contains($specialComponent)) {
            $this->specialComponents[] = $specialComponent;
            $specialComponent->setCountry($this);
        }

        return $this;
    }

    /**
     * @param SpecialComponent $specialComponent
     *
     * @return $this
     */
    public function removeSpecialComponent(SpecialComponent $specialComponent): self
    {
        if ($this->specialComponents->removeElement($specialComponent)) {
            // set the owning side to null (unless already changed)
            if ($specialComponent->getCountry() === $this) {
                $specialComponent->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'code' => $this->code,
            'name' => $this->name,
            'languages' => $this->languages?->count(),
            'flag' => $this->getFlag()?->getFileName(),
            'materials' => $this->materials?->count(),
            'funderInterventions' => $this->funderInterventions?->count(),
            'typeInterventions' => $this->typeInterventions?->count(),
            'institutionInterventions' => $this->institutionInterventions?->count(),
            'divisionTypes' => $this->divisionTypes?->count(),
            'mainOfficialLanguage' => $this->mainOfficialLanguage,
            'officialLanguages' => $this->officialLanguages?->count(),
            'mainUnitConcentration' => $this->mainUnitConcentration,
            'unitConcentrations' => count($this->unitConcentrations),
            'mainUnitFlow' => $this->mainUnitFlow,
            'unitFlows' => count($this->unitFlows),
            'mainUnitLength' => $this->mainUnitLength,
            'unitLengths' => count($this->unitLengths),
            'mainUnitVolume' => $this->mainUnitVolume,
            'unitVolumes' => count($this->unitVolumes),
            'ethnicities' => $this->ethnicities?->count(),
            'currencies' => $this->currencies?->count(),
            'programInterventions' => $this->programInterventions?->count(),
            'technicalAssistanceProviders' => $this->technicalAssistanceProviders?->count(),
            'administrativeDivisions' => $this->administrativeDivisions?->count(),
            'formLevel' => $this->formLevel,
            'pumpTypes' => $this->pumpTypes,
            'defaultDiameters' => $this->defaultDiameters?->count(),
            'specialComponents' => $this->specialComponents?->count(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return [
            'name' => $this->name,
            'flag' => $this->getFlag()?->formatExtraJson(),
            'formLevel' => $this->formLevel,
        ];
    }

    /**
     * Not valid units throw an exception.
     *
     * @param string $measure
     * @param string $unit
     *
     * @return void
     */
    protected function validateMeasurementUnit(string $measure, string $unit): void
    {
        /** @var ConfigurationService $confService */
        $confService = static::getContainerInstance()->get('configuration_service');
        if (!$confService->existSystemUnit('system.units.'.$measure, $unit)) {
            throw new InvalidArgumentException(
                $this->t(
                    'Main unit "@unit" must be present in system @measure units.',
                    [
                        '@unit' => $unit,
                        '@measure' => $measure,
                    ]
                )
            );
        }
    }
}
