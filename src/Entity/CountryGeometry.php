<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use App\Repository\CountryGeometryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryGeometryRepository::class)
 */
class CountryGeometry
{
    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="string",
     *     length=50,
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @ORM\Column(type="geometry", nullable=true)
     */
    protected $polygon;

    /**
     * Country constructor.
     *
     * @param ?string $id Country code.
     */
    public function __construct(?string $id = null)
    {
        if ($id) {
            $this->id = $id;
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }
}
