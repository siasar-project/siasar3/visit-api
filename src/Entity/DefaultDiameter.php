<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Annotations\IsCountryParametric;
use App\Forms\FormReferenceEntityInterface;
use App\Repository\ConfigurationRepository;
use App\Repository\DefaultDiameterRepository;
use App\Traits\FormReferenceStartingTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Annotations\SecuredRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * [SYS | 3.4] Default diameter.
 *
 * @ORM\Entity(repositoryClass=DefaultDiameterRepository::class)
 *
 * @IsCountryParametric
 *
 * @SecuredRepository(class=App\RepositorySecured\DefaultDiameterRepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"default_diameter:write"}},
 *     normalizationContext={"groups":{"default_diameter:read"}},
 *     itemOperations={
 *          "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *          "put" = { "method" = "PUT", "security" = "is_granted('UPDATE', object)" },
 *          "patch" = { "method" = "PATCH", "security" = "is_granted('UPDATE', object)" },
 *          "delete" = { "method" = "DELETE", "security" = "is_granted('DELETE', object)" }
 *     },
 *     collectionOperations={
 *      "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)"
 *      },
 *      "post" = { "method" = "POST", "security_post_denormalize" = "is_granted('CREATE', object)" },
 *     }
 * )
 */
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'value' => 'exact',
    'unit' => 'partial',
    'country' => 'exact',
])]
class DefaultDiameter implements FormReferenceEntityInterface
{
    use FormReferenceStartingTrait;

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"default_diameter:read"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="defaultDiameters", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code", name="country_code")
     *
     * @Groups({"default_diameter:read", "default_diameter:write"})
     */
    protected $country;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=8)
     *
     * @Groups({"default_diameter:read", "default_diameter:write"})
     */
    protected $value;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"default_diameter:read", "default_diameter:write"})
     */
    protected $unit;

    /**
     * constructor.
     *
     * @param string $id
     */
    public function __construct(string $id = null)
    {
        if (null === $id) {
            $ulid = new Ulid();
            $id = $ulid->toBase32();
        }
        $this->id = $id;
        $this->value = 0;
        $this->unit = 'metre';
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param float $value
     *
     * @return $this
     */
    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function setUnit(string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'id' => $this->id,
            'country' => $this->country?->getId(),
            'unit' => $this->unit,
            'value' => $this->value,
        ];
    }

    /**
     * @inheritDoc
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return [];
    }
}
