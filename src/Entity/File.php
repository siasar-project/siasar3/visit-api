<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Api\Action\CreateFileObjectAction;
use App\Api\Action\DownloadFileObjectAction;
use App\Forms\FormReferenceEntityInterface;
use App\Service\FileSystem;
use App\Traits\FormReferenceStartingTrait;
use App\Traits\GetContainerTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Ulid;
use App\Annotations\SecuredRepository;

/**
 * File entity.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ORM\Entity(repositoryClass=App\Repository\FileRepository::class)
 *
 * @SecuredRepository(class=App\RepositorySecured\FileRepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"file:write"}},
 *     normalizationContext={"groups"={"file:read"}},
 *     itemOperations={
 *      "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *      "delete" = { "method" = "DELETE", "security" = "is_granted('DELETE', object)" }
 *     },
 *     collectionOperations={
 *      "get"={ "method" = "GET", "security_get_denormalize"="is_granted('READ', object)" },
 *      "post"={
 *          "security"="is_granted('CREATE', object)",
 *          "method"="POST",
 *          "route_name" = "file_upload",
 *          "path"="/files",
 *          "denormalization_context"={
 *              "groups"={"file:update"}
 *          },
 *          "validation_groups"={"Default", "file:create"},
 *          "openapi_context"={
 *              "requestBody"={
 *                  "content"={
 *                      "multipart/form-data"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                                  "file"={
 *                                      "type"="string",
 *                                      "format"="binary",
 *                                  },
 *                                  "country"={
 *                                      "type"="App\Entity\Country",
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  },
 *              },
 *          },
 *      },
 *      "download"={
 *          "security"="is_granted('READ', object)",
 *          "method"="GET",
 *          "route_name" = "file_download",
 *          "path"="/files/{id}/download",
 *          "requirements" = {"id" = "\d+"},
 *          "openapi_context"={
 *              "summary"="Download file.",
 *              "description"="This operation can be resumed.",
 *              "parameters"={
 *                  {
 *                      "name"="id",
 *                      "description"="Resource identifier",
 *                      "in"="path",
 *                      "required"=true,
 *                      "type"="string",
 *                  },
 *              },
 *              "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/octet-stream"={
 *                                  "schema"={
 *                                      "type"="string",
 *                                      "format"="binary",
 *                                  },
 *                              },
 *                          },
 *                      },
 *                      "404"={
 *                          "description"="Not found",
 *                      }
 *              },
 *          },
 *      },
 *     },
 * )
 */
#[ApiFilter(SearchFilter::class, properties: ['fileName' => 'partial'])]
#[ApiFilter(OrderFilter::class, properties: ['fileName'], arguments: ['orderParameterName' => 'order'])]
class File implements FormReferenceEntityInterface
{
    use GetContainerTrait;
    use FormReferenceStartingTrait;

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     */
    #[Groups(['file:read', 'file:update'])]
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    #[Groups(['file:read'])]
    protected $fileName;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="country_code", referencedColumnName="code")
     */
    #[Groups(['file:read', 'file:create', 'file:update'])]
    protected $country;

    /**
     * @ORM\Column(type="text")
     */
    protected $path;

    /**
     * Creation date.
     *
     * @ORM\Column(type="datetime")
     */
    #[Groups(['file:read'])]
    protected \DateTime $created;

    /**
     * File system service.
     *
     * @var FileSystem
     */
    protected FileSystem $fileSystem;

    /**
     * File constructor.
     *
     * @param string|null $id
     */
    public function __construct(string $id = null)
    {
        if (!$id) {
            $ulid = new Ulid();
            $this->id = $ulid->toBase32();
        } else {
            $this->id = $id;
        }
        $this->created = new \DateTime();
    }

    /**
     * Get ID.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get filename.
     *
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * Set filename.
     *
     * @param string $fileName
     *
     * @return $this
     */
    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get country.
     *
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get file path.
     *
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * Set file path.
     *
     * @param string $path
     *
     * @return $this
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * Get file size in bytes.
     *
     * @return int
     */
    public function getSize(): int
    {
        $fileName = $this->getFileSystem()->getPublicFolder().'/'.$this->getPath();
        if (!is_file($fileName)) {
            return 0;
        }

        return filesize($fileName);
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'id' => $this->id,
            'fileName' => $this->fileName,
            'country' => $this->country?->getId(),
            'path' => $this->path,
            'created' => $this->created->format('c'),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return [
            'created' => $this->created->format('c'),
            'size' => !is_file('files://'.$this->path) ? 0 : filesize('files://'.$this->path),
            'url' => sprintf('/api/v1/files/%s/download', $this->getId()),
        ];
    }

    /**
     * Get file system service.
     *
     * @return FileSystem
     */
    protected function getFileSystem(): FileSystem
    {
        if (!isset($this->fileSystem) || $this->fileSystem) {
            $this->fileSystem = $this->getContainerInstance()->get('os_file_system');
        }

        return $this->fileSystem;
    }
}
