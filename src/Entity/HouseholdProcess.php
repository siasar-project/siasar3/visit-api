<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Annotations\SecuredRepository;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\FormReferenceEntityInterface;
use App\Forms\PointFormManager;
use App\Traits\FormReferenceStartingTrait;
use App\Traits\GetContainerTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Uid\Ulid;

/**
 * Household Process entity.
 *
 * @ORM\Entity(repositoryClass=App\Repository\HouseholdProcessRepository::class)
 * @ORM\Table(name="household_process",indexes={@ORM\Index(name="community_idx", columns={"community_reference"})})
 * @ORM\HasLifecycleCallbacks
 *
 * @SecuredRepository(class=App\RepositorySecured\HouseholdProcessRepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"household_process:write"}},
 *     normalizationContext={"groups":{"household_process:read"}},
 *     itemOperations={
 *          "get" = {
 *              "method" = "GET",
 *              "security" = "is_granted('READ', object)",
 *              "openapi_context"={
 *                  "summary"="Retrieves a HouseholdProcess resource.",
 *                  "description"="Retrieves a HouseholdProcess resource.",
 *                  "responses"={
 *                      "200"={
 *                          "description"="HouseholdProcess resource",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "communityReference"={
 *                                              "type"="string",
 *                                          },
 *                                          "administrativeDivision"={
 *                                              "type"="string",
 *                                          },
 *                                          "finishedHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "open"={
 *                                              "type"="boolean",
 *                                          },
 *                                          "country"={
 *                                              "type"="string",
 *                                          },
 *                                          "totalHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "draftHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "communityHouseholds"={
 *                                              "type"="number",
 *                                          },
 *                                          "image"={
 *                                              "type"="string",
 *                                          },
 *                                          "point"={
 *                                              "type"="string",
 *                                          },
 *                                      }
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  },
 *              },
 *          },
 *          "put" = { "method" = "PUT", "security" = "is_granted('UPDATE', object)" },
 *          "patch" = { "method" = "PATCH", "security" = "is_granted('UPDATE', object)" },
 *          "close_process" = {
 *              "security" = "is_granted('CREATE', object)",
 *              "route_name" = "close_process",
 *              "method": "PUT",
 *              "path"="/household_processes/{id}/close",
 *              "openapi_context"={
 *                  "summary"="Close a HouseholdProcess resource.",
 *                  "description"="Close a HouseholdProcess resource.",
 *                  "requestBody"={
 *                      "content"={},
 *                      "required"=false
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="HouseholdProcess resource closed",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "communityReference"={
 *                                              "type"="string",
 *                                          },
 *                                          "administrativeDivision"={
 *                                              "type"="string",
 *                                          },
 *                                          "finishedHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "open"={
 *                                              "type"="boolean",
 *                                          },
 *                                          "country"={
 *                                              "type"="string",
 *                                          },
 *                                      }
 *                                  },
 *                              },
 *                          },
 *                      },
 *                      "422"={
 *                          "description"="Unprocessable entity",
 *                      }
 *                  },
 *              },
 *          }
 *     },
 *     collectionOperations={
 *          "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)",
 *              "openapi_context"={
 *                  "summary"="Retrieves the collection of HouseholdProcess resources.",
 *                  "description"="Retrieves the collection of HouseholdProcess resources.",
 *                  "responses"={
 *                      "200"={
 *                          "description"="HouseholdProcess collection",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "communityReference"={
 *                                              "type"="string",
 *                                          },
 *                                          "administrativeDivision"={
 *                                              "type"="string",
 *                                          },
 *                                          "finishedHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "open"={
 *                                              "type"="boolean",
 *                                          },
 *                                          "country"={
 *                                              "type"="string",
 *                                          },
 *                                          "totalHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "draftHousehold"={
 *                                              "type"="number",
 *                                          },
 *                                          "communityHouseholds"={
 *                                              "type"="number",
 *                                          },
 *                                          "image"={
 *                                              "type"="string",
 *                                          },
 *                                          "point"={
 *                                              "type"="string",
 *                                          },
 *                                      }
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  },
 *              },
 *          },
 *          "add_household" = {
 *              "security"="is_granted('CREATE', object)",
 *              "route_name" = "add_household",
 *              "method": "POST",
 *              "path"="/household_processes/{id}/add_household",
 *              "openapi_context"={
 *                  "summary"="Add household survey to a process.",
 *                  "description"="Add household survey to a process.",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required" = {},
 *                                  "properties"={},
 *                              }
 *                          }
 *                      }
 *                  },
 *                  "parameters"={
 *                      {
 *                          "name"="id",
 *                          "description"="Household Process ID",
 *                          "in"="path",
 *                          "required"=true,
 *                          "type"="string",
 *                      },
 *                  }
 *              }
 *          },
 *          "my_households" = {
 *              "security" = "is_granted('READ', object)",
 *              "route_name" = "my_households",
 *              "method": "GET",
 *              "path"="/household_processes/my_households",
 *              "openapi_context"={
 *                  "summary"="Get households from an user.",
 *                  "description"="Get households from an user.",
 *                  "parameters"={
 *                      {
 *                          "name"="page",
 *                          "description"="The collection page number.",
 *                          "value"="1",
 *                          "in"="query",
 *                          "required"=true,
 *                          "type"="number",
 *                      },
 *                      {
 *                          "name"="field_household_process",
 *                          "description"="Process ID to query",
 *                          "in"="query",
 *                          "required"=false,
 *                          "type"="string",
 *                      },
 *                      {
 *                          "name"="field_interviewer",
 *                          "description"="Interviewer ID to query",
 *                          "in"="query",
 *                          "required"=false,
 *                          "type"="string",
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="form.community.household resource.",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={}
 *                                  },
 *                              },
 *                          },
 *                      },
 *                  },
 *              }
 *          }
 *     }
 * )
 */
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'communityReference' => 'exact',
    'administrativeDivision' => 'exact',
    'open' => 'exact',
    'country' => 'exact',
])]
class HouseholdProcess implements FormReferenceEntityInterface
{
    use GetContainerTrait;
    use FormReferenceStartingTrait;

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"household_process:read"})
     */
    protected $id;

    /**
     * Record community reference
     *
     * @ORM\Column(type="ulid")
     *
     * @Groups({"household_process:read", "household_process:write"})
     */
    protected $communityReference;

    /**
     * @ORM\ManyToOne(targetEntity=AdministrativeDivision::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="id", name="administrative_division_id")
     *
     * @Groups({"household_process:read", "household_process:write"})
     */
    protected $administrativeDivision;

    /**
     * Total of finished households
     *
     * @ORM\Column(type="integer")
     *
     * @Groups({"household_process:read", "household_process:write"})
     */
    protected $finishedHousehold;

    /**
     * Is open (if it allows new data)
     *
     * @ORM\Column(type="boolean")
     *
     * @Groups({"household_process:read", "household_process:write"})
     */
    protected bool $open;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code", name="country_code")
     *
     * @Groups({"household_process:read", "household_process:write"})
     */
    protected $country;

    /**
     * @Ignore
     */
    protected $formFactory;

    /**
     * Household Process constructor.
     *
     * @param string|null $id
     */
    public function __construct(string $id = null)
    {
        if (null === $id) {
            $ulid = new Ulid();
            $id = $ulid->toBase32();
        }
        $this->id = $id;
        $this->finishedHousehold = 0;
        $this->open = true;
    }

    /**
     * Id.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get community reference.
     *
     * @return Ulid
     */
    public function getCommunityReference()
    {
        return $this->communityReference;
    }

    /**
     * Set community reference.
     *
     * @param Ulid $communityReference
     *
     * @return HouseholdProcess
     */
    public function setCommunityReference($communityReference)
    {
        $this->communityReference = $communityReference;

        return $this;
    }

    /**
     * Get administrative division
     *
     * @return AdministrativeDivision
     */
    public function getAdministrativeDivision(): AdministrativeDivision
    {
        return $this->administrativeDivision;
    }

    /**
     * Set the administrative division
     *
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return self
     */
    public function setAdministrativeDivision(AdministrativeDivision $administrativeDivision): self
    {
        $this->administrativeDivision = $administrativeDivision;

        return $this;
    }

    /**
     * Get households in this process.
     *
     * @return FormRecord[]
     */
    public function getHouseholdInquiries(): array
    {
        $householdForm = $this->getFormFactory()->find('form.community.household');
        $inThisProcess = $householdForm->findBy(
            [
                'field_household_process' => $this->getId(),
            ]
        );

        return $inThisProcess;
    }

    /**
     * Get total of finished households
     *
     * @return int
     */
    public function getFinishedHousehold(): int
    {
        $householdForm = $this->getFormFactory()->find('form.community.household');
        $inThisProcess = $householdForm->countBy(
            [
                'field_household_process' => $this->getId(),
                'field_finished' => true,
            ]
        );

        return $inThisProcess;
    }

    /**
     * Set total of finished households
     *
     * @param int $finishedHousehold
     *
     * @return self
     */
    public function setFinishedHousehold(int $finishedHousehold): self
    {
        $this->finishedHousehold = $finishedHousehold;

        return $this;
    }

    /**
     * Get total of households
     *
     * @return int
     */
    public function getTotalHousehold(): int
    {
        $householdForm = $this->getFormFactory()->find('form.community.household');
        $inThisProcess = $householdForm->countBy(
            [
                'field_household_process' => $this->getId(),
            ]
        );

        return $inThisProcess;
    }

    /**
     * Get total of draft households
     *
     * @return int
     */
    public function getDraftHousehold(): int
    {
        return $this->getTotalHousehold() - $this->getFinishedHousehold();
    }

    /**
     * Is this user open?
     *
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->open;
    }

    /**
     * Change status.
     *
     * @param bool $open New status.
     *
     * @return self
     */
    public function setOpen(bool $open): self
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get country.
     *
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * Set the country.
     *
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'id' => $this->id,
            'country' => $this->country?->getId(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        // Get division meta.
        $metaDivision = [];
        $division = $this->administrativeDivision;
        if ($division) {
            $metaDivision = $division->formatExtraJson();
        }
        // Get point ID.
        $pointRecordId = '';
        if ($this->communityReference) {
            /** @var PointFormManager $pointManager */
            $pointManager = $this->getFormFactory()->find('form.point');
            /** @var FormRecord $point */
            $point = current($pointManager->findByCommunity($this->communityReference));
            if ($point) {
                $pointRecordId = $point->getId();
            }
        }

        return [
            'community_record' => $this->communityReference,
            'point_record' => $pointRecordId,
            'administrative_division' => $metaDivision,
            'is_open' => $this->isOpen(),
        ];
    }

    /**
     * Get form factory service.
     *
     * @return FormFactory
     */
    protected function getFormFactory(): FormFactory
    {
        if (!isset($this->formFactory) || $this->formFactory) {
            $this->formFactory = $this->getContainerInstance()->get('form_factory');
        }

        return $this->formFactory;
    }
}
