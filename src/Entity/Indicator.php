<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use App\Repository\IndicatorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

/**
 * @ORM\Entity(repositoryClass=IndicatorRepository::class)
 *
 *
 * @ORM\Table(name="indicator",uniqueConstraints={@ORM\UniqueConstraint(name="unique_key", columns={"name", "record"})})
 */
class Indicator
{

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     */
    protected $id;

    /**
     * @ORM\Column(
     *     name="record",
     *     type="ulid"
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     */
    protected $record;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $recordType;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=3)
     */
    protected $value;

    /**
     * Error message, if any.
     *
     * @ORM\Column(type="text", options={"default" : ""})
     */
    protected $message;

    /**
     * Indicator.
     *
     * @param ?Ulid  $record Form record LUID.
     * @param string $name   Indicator name.
     */
    public function __construct(Ulid $record = null, string $name = '')
    {
        $this->record = $record;
        $this->name = $name;
        $this->message = '';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Ulid|null
     */
    public function getRecord(): ?Ulid
    {
        return $this->record;
    }

    /**
     * @param Ulid|null $record
     */
    public function setRecord(?Ulid $record): void
    {
        $this->record = $record;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * Get form record type.
     *
     * @return mixed
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * Set form record type.
     *
     * @param mixed $recordType
     */
    public function setRecordType($recordType): void
    {
        $this->recordType = $recordType;
    }
}
