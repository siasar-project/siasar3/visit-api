<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Kevin Hirczy <https://www.linkedin.com/in/kevin-hirczy-7a9377106/>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Inquiry form log record.
 *
 * @category SIASAR_3
 *
 * @author   Kevin Hirczy <https://www.linkedin.com/in/kevin-hirczy-7a9377106/>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ORM\Entity(repositoryClass="App\Repository\InquiryFormLogRepository")
 * @ORM\Table(
 *     name="inquiry_log",
 *     indexes={
 *          @ORM\Index(name="level_idx", columns={"level"}),
 *          @ORM\Index(name="level_name_idx", columns={"level_name"}),
 *          @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *          @ORM\Index(name="form_id_idx", columns={"form_id"}),
 *          @ORM\Index(name="record_id_idx", columns={"record_id"}),
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 *
 * @ApiResource(
 *     normalizationContext={ "groups": {"inquiry_form_log:read"} },
 *     itemOperations={
 *          "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *     },
 *     collectionOperations={
 *          "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)"
 *          },
 *          "post"={
 *              "method" = "POST",
 *              "security_post_denormalize" = "is_granted('CREATE', object)"
 *          },
 *     }
 * )
 */
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'createdAt' => 'partial',
    'level' => 'partial',
    'levelName' => 'partial',
    'message' => 'partial',
    'country' => 'exact',
    'formId' => 'partial',
    'recordId' => 'partial',
])]
class InquiryFormLog
{
    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"inquiry_form_log:read"})
     */
    protected $id;

    /**
     * @ORM\Column(name="message", type="text")
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $message;

    /**
     * @ORM\Column(name="context", type="json")
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $context;

    /**
     * @ORM\Column(name="level", type="smallint")
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $level;

    /**
     * @ORM\Column(name="level_name", type="string", length=50)
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $levelName;

    /**
     * @ORM\Column(name="extra", type="json")
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $extra;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"inquiry_form_log:read"})
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code", name="country_code")
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $country;

    /**
     * @ORM\Column(name="form_id", type="string", length=50, nullable=true)
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $formId;

    /**
     * @ORM\Column(name="record_id", type="ulid", nullable=true)
     *
     * @Groups({"inquiry_form_log:read", "inquiry_form_log:write"})
     */
    protected $recordId;

    /**
     * File constructor.
     *
     * @param string|null $id
     */
    public function __construct(string $id = null)
    {
        if (!$id) {
            $ulid = new Ulid();
            $this->id = $ulid->toBase32();
        } else {
            $this->id = $id;
        }
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     *
     * @return InquiryFormLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param mixed $context
     *
     * @return InquiryFormLog
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     *
     * @return InquiryFormLog
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * @param mixed $levelName
     *
     * @return InquiryFormLog
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param mixed $extra
     *
     * @return InquiryFormLog
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get country.
     *
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * Set the country.
     *
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * @param mixed $formId
     *
     * @return InquiryFormLog
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRecordId()
    {
        return $this->recordId;
    }

    /**
     * @param mixed $recordId
     *
     * @return InquiryFormLog
     */
    public function setRecordId($recordId)
    {
        $this->recordId = $recordId;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }
}
