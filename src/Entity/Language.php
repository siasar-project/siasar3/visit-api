<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Language Entity.
 *
 * Structure of the internal language code:
 * <L1 generic backup language>_<L2 backup language>_<L3 language>
 *
 * Considerations:
 *
 * Each L must have 4 characters. If necessary, fill with hyphens, -, on the right.
 * There may be codes, and translations, without L3, or without L2 or L3.
 *
 * Nesting example:
 *
 * Code             |   Idiom
 * ESPA             |   Spanish
 * ESPA_LATI        |   Latin American Spanish
 * ESPA_LATI_HOND   |   Honduran
 *
 * When consulting a translation of ESPA_LATI_HOND, it must give it if it exists,
 * if it does not exist it must give one of ESPA_LATI, if it does not exist,
 * respond with one of ESPA, and if it does not exist, the original string must be take.
 * (This must be done in a single query, except for returning the original string, which
 * will be done from the method)
 *
 * @ORM\Entity(repositoryClass=App\Repository\LanguageRepository::class)
 * @ORM\Table(
 *     name="language",
 *     indexes={
 *          @ORM\Index(name="iso_code_idx", columns={"iso_code"}),
 *     }
 * )
 *
 * @category Tools
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ApiResource(
 *     itemOperations={"GET"},
 *     collectionOperations={"GET"}
 * )
 */
class Language
{
    /**
     * Language ID
     * @Groups({"oficialLanguage_read", "oficialLanguage_write"})
     *
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="string",
     *     length=50,
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected string $id;

    /**
     * Language ISO code.
     *
     * @ORM\Column(
     *      type="string",
     *      nullable=false
     * )
     * @Assert\NotBlank
     */
    protected string $isoCode;

    /**
     * Language name.
     *
     * @ORM\Column(
     *      type="string",
     *      nullable=false
     * )
     * @Assert\NotBlank
     */
    protected string $name;

    /**
     * Is Left To Right language .
     *
     * @ORM\Column(
     *      type="boolean",
     *      nullable=false,
     *      options={"default"=true}
     * )
     * @Assert\NotBlank
     */
    protected bool $ltr;

    /**
     * Language plural formula.
     *
     * @ORM\Column(
     *      type="string",
     *      length=512,
     *      nullable=true
     * )
     */
    protected string $pluralFormula;

    /**
     * Language plural number.
     *
     * @ORM\Column(
     *      type="integer",
     *      nullable=true
     * )
     */
    protected int $pluralNumber;

    /**
     * Is translatable.
     *
     * @ORM\Column(
     *      type="boolean",
     *      nullable=false,
     *      options={"default"=false}
     * )
     * @Assert\NotBlank
     */
    protected bool $translatable;

    /**
     * Language constructor.
     *
     * @param string $id Language ID
     */
    public function __construct(string $id)
    {
        // Validate ID.
        $idParts = explode('_', $id);
        foreach ($idParts as $idPart) {
            if (strlen($idPart) !== 4) {
                throw new \Exception(
                    sprintf(
                        '[%s] Language IDs must be 4 characters blocks separated by underscore.',
                        $id
                    )
                );
            }
        }
        $id = strtoupper($id);
        // Init.
        $this->id = $id;
        $this->pluralFormula = 'n != 1';
        $this->pluralNumber = 2;
        $this->ltr = true;
        $this->translatable = false;
    }

    /**
     * Convert to string.
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getId();
    }

    /**
     * Get language ID.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get the name of the language.
     *
     * @return string
     */
    public function getName(): string
    {
        return (string) $this->name;
    }

    /**
     * Set the name of the language.
     *
     * @param string $name New name of language
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the plural formula.
     *
     * @return string
     */
    public function getPluralFormula(): string
    {
        return $this->pluralFormula;
    }

    /**
     * Set plural formula.
     *
     * @param string $pluralFormula The plural formula.
     *
     * @return self
     */
    public function setPluralFormula(string $pluralFormula): self
    {
        $this->pluralFormula = $pluralFormula;

        return $this;
    }

    /**
     * Get plural number.
     *
     * @return int
     */
    public function getPluralNumber(): int
    {
        return $this->pluralNumber;
    }

    /**
     * Set plural number.
     *
     * @param int $pluralNumber
     *
     * @return self
     */
    public function setPluralNumber(int $pluralNumber): self
    {
        $this->pluralNumber = $pluralNumber;

        return $this;
    }

    /**
     * Get ISO code.
     *
     * @return string
     */
    public function getIsoCode(): string
    {
        return $this->isoCode;
    }

    /**
     * Set ISO code.
     *
     * @param string $isoCode The ISO code.
     */
    public function setIsoCode(string $isoCode): void
    {
        $this->isoCode = $isoCode;
    }

    /**
     * Is this language Left To Right?
     *
     * @return bool
     */
    public function isLtr(): bool
    {
        return $this->ltr;
    }

    /**
     * Set Left To Right condition.
     *
     * @param bool $ltr
     */
    public function setLtr(bool $ltr): void
    {
        $this->ltr = $ltr;
    }

    /**
     * Is this language translatable?
     *
     * @return bool
     */
    public function isTranslatable(): bool
    {
        return $this->translatable;
    }

    /**
     * Set translatable condition.
     *
     * @param bool $translatable
     */
    public function setTranslatable(bool $translatable): void
    {
        $this->translatable = $translatable;
    }
}
