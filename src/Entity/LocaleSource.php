<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\LocaleSourceRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

/**
 * LocaleSource Entity.
 *
 * @ORM\Entity(repositoryClass=App\Repository\LocaleSourceRepository::class)
 * @ORM\Table(
 *     name="locale_source",
 *     indexes={
 *          @ORM\Index(name="message_idx", columns={"message"}),
 *          @ORM\Index(name="context_idx", columns={"context"}),
 *     }
 * )
 *
 * @category Tools
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LocaleSource
{
    /**
     * Locale source ID
     *
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     */
    protected string $id;

    /**
     * String message.
     *
     * @ORM\Column(
     *      type="text",
     *      nullable=false
     * )
     * @Assert\NotBlank
     */
    protected string $message;

    /**
     * Context.
     *
     * @ORM\Column(
     *      type="string",
     *      nullable=true
     * )
     */
    protected ?string $context;

    /**
     * Is this source an plural?
     *
     * @ORM\Column(
     *      type="boolean"
     * )
     */
    protected bool $isPlural;

    /**
     * Locale Source constructor.
     */
    public function __construct()
    {
        $ulid = new Ulid();
        $this->id = $ulid->toBase32();
        $this->isPlural = false;
    }

    /**
     * Get locale source ID.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get string message.
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Set string message.
     *
     * @param string $message String message
     *
     * @return self
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get context.
     *
     * @return string
     */
    public function getContext(): string
    {
        return $this->context ?? '';
    }

    /**
     * Set context.
     *
     * @param ?string $context The context
     *
     * @return self
     */
    public function setContext(?string $context): self
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Is this source an plural?
     *
     * @return bool
     */
    public function isPlural(): bool
    {
        return $this->isPlural;
    }

    /**
     * Set if is an plural source.
     *
     * @param bool $isPlural
     *
     * @return self
     */
    public function setIsPlural(bool $isPlural): self
    {
        $this->isPlural = $isPlural;

        return $this;
    }
}
