<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LocaleTarget Entity.
 *
 * @ORM\Entity(repositoryClass="App\Repository\LocaleTargetRepository")
 * @ORM\Table(
 *     name="locale_target",
 *     indexes={
 *          @ORM\Index(name="context_idx", columns={"context"}),
 *     }
 * )
 *
 * @category Tools
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LocaleTarget
{
    /**
     * Language ID
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Language")
     */
    protected Language $language;

    /**
     * Locale Source ID
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="LocaleSource")
     * @ORM\JoinColumn(
     *     name="locale_source_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     */
    protected LocaleSource $localeSource;

    /**
     * Translation string
     *
     * @ORM\Column(
     *      type="text",
     *      nullable=false
     * )
     * @Assert\NotBlank
     */
    protected string $translation;

    /**
     * Context
     *
     * @ORM\Column(
     *      type="string",
     *      nullable=true
     * )
     */
    protected ?string $context;

    /**
     * Locale Target constructor.
     *
     * @param Language     $language     Language entity
     * @param LocaleSource $localeSource Locale Source entity
     */
    public function __construct(Language $language, LocaleSource $localeSource)
    {
        $this->language = $language;
        $this->localeSource = $localeSource;
    }

    /**
     * Get translation string
     *
     * @return string
     */
    public function getTranslation(): string
    {
        return $this->translation;
    }

    /**
     * Set translation string
     *
     * @param string $translation Translation string message
     *
     * @return self
     */
    public function setTranslation($translation): self
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get context
     *
     * @return string|null
     */
    public function getContext(): ?string
    {
        return $this->context;
    }

    /**
     * Set context
     *
     * @param ?string $context Context
     *
     * @return self
     */
    public function setContext(?string $context): self
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get language ID
     *
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /**
     * Set language
     *
     * @param Language $language Language entity
     *
     * @return self
     */
    public function setLanguage($language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get locale Source ID
     *
     * @return LocaleSource
     */
    public function getLocaleSource(): LocaleSource
    {
        return $this->localeSource;
    }

    /**
     * Set locale Source ID
     *
     * @param LocaleSource $localeSource LocaleSource entity
     *
     * @return self
     */
    public function setLocaleSource($localeSource): self
    {
        $this->localeSource = $localeSource;

        return $this;
    }
}
