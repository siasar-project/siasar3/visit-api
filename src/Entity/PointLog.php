<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Kevin Hirczy <https://www.linkedin.com/in/kevin-hirczy-7a9377106/>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * SIASAR Point log record.
 *
 * @category SIASAR_3
 *
 * @author   Kevin Hirczy <https://www.linkedin.com/in/kevin-hirczy-7a9377106/>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ORM\Entity(repositoryClass="App\Repository\PointLogRepository")
 * @ORM\Table(
 *     name="point_log",
 *     indexes={
 *          @ORM\Index(name="level_idx", columns={"level"}),
 *          @ORM\Index(name="level_name_idx", columns={"level_name"}),
 *          @ORM\Index(name="created_at_idx", columns={"created_at"}),
 *          @ORM\Index(name="point_idx", columns={"point"}),
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 *
 * @ApiResource(
 *     normalizationContext={ "groups": {"point_log:read"} },
 *     itemOperations={"get" = { "method" = "GET", "security" = "is_granted('READ', object)" }},
 *     collectionOperations={"get"={ "method" = "GET", "controller"="App\Api\Action\PointAction::getPointLogCollection", "security_get_denormalize"="is_granted('READ', object)" }}
 * )
 */
#[ApiFilter(OrderFilter::class, properties: [
    'createdAt',
    'point',
    'level',
    'levelName',
    'message',
    'country',
])]
#[ApiFilter(SearchFilter::class, properties: [
    'point' => 'exact',
    'level' => 'partial',
    'levelName' => 'exact',
    'message' => 'partial',
    'country' => 'exact',
])]
class PointLog
{
    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"point_log:read"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="country_code", referencedColumnName="code")
     */
    #[Groups(['point_log:read'])]
    protected $country;

    /**
     * @ORM\Column(
     *     name="point",
     *     type="ulid"
     * )
     *
     * @Groups({"point_log:read"})
     */
    protected $point;

    /**
     * @ORM\Column(name="message", type="text")
     *
     * @Groups({"point_log:read"})
     */
    protected $message;

    /**
     * @ORM\Column(name="context", type="json")
     *
     * @Groups({"point_log:read"})
     */
    protected $context;

    /**
     * @ORM\Column(name="level", type="smallint")
     *
     * @Groups({"point_log:read"})
     */
    protected $level;

    /**
     * @ORM\Column(name="level_name", type="string", length=50)
     *
     * @Groups({"point_log:read"})
     */
    protected $levelName;

    /**
     * @ORM\Column(name="extra", type="json")
     *
     * @Groups({"point_log:read"})
     */
    protected $extra;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Groups({"point_log:read"})
     */
    protected $createdAt;

    /**
     * File constructor.
     *
     * @param string|null $id
     */
    public function __construct(string $id = null)
    {
        if (!$id) {
            $ulid = new Ulid();
            $this->id = $ulid->toBase32();
        } else {
            $this->id = $id;
        }
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     *
     * @return PointLog
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get SIASAR Point ID.
     *
     * @return string
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set SIASAR Point ID.
     *
     * @param string $point
     *
     * @return PointLog
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     *
     * @return PointLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param mixed $context
     *
     * @return PointLog
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     *
     * @return PointLog
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * @param mixed $levelName
     *
     * @return PointLog
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param mixed $extra
     *
     * @return PointLog
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }
}
