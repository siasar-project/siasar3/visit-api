<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use Yivoff\JwtRefreshBundle\Contracts\RefreshTokenInterface;
use App\Repository\RefreshTokenRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Uid\Ulid;

/**
 * Refresh session token-
 *
 * @ORM\Entity(repositoryClass=RefreshTokenRepository::class)
 * @ORM\Table(
 *     name="refresh_token",
 *     indexes={
 *          @ORM\Index(name="username_idx", columns={"username"}),
 *          @ORM\Index(name="identifier_idx", columns={"identifier"}),
 *          @ORM\Index(name="verifier_idx", columns={"verifier"}),
 *          @ORM\Index(name="valid_until_idx", columns={"valid_until"}),
 *     }
 * )
 */
class RefreshToken implements RefreshTokenInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $verifier;

    /**
     * @ORM\Column(type="integer")
     */
    protected $validUntil;

    /**
     * Refresh token constructor.
     */
    public function __construct()
    {
        $this->id = (new Ulid())->toBase32();
        $this->validUntil = time() + $_ENV['JWT_REFRESH_TOKEN_TTL'];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getVerifier(): string
    {
        return $this->verifier;
    }

    /**
     * @param string $verifier
     *
     * @return $this
     */
    public function setVerifier(string $verifier): self
    {
        $this->verifier = $verifier;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidUntil(): int
    {
        return $this->validUntil;
    }

    /**
     * @param int $validUntil
     *
     * @return $this
     */
    public function setValidUntil(int $validUntil): self
    {
        $this->validUntil = $validUntil;

        return $this;
    }
}
