<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Annotations\IsCountryParametric;
use App\Annotations\Label;
use App\Forms\FormReferenceEntityInterface;
use App\Repository\SpecialComponentRepository;
use App\Traits\FormReferenceStartingTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Annotations\SecuredRepository;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

/**
 * [SYS | 3.6] Special component.
 *
 * @ORM\Entity(repositoryClass=SpecialComponentRepository::class)
 *
 * @IsCountryParametric
 *
 * @ORM\Table(name="special_component",uniqueConstraints={@ORM\UniqueConstraint(name="unique_key", columns={"key_index", "country_code"})})
 *
 * @SecuredRepository(class=App\RepositorySecured\SpecialComponentRepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"special_component:write"}},
 *     normalizationContext={"groups":{"special_component:read"}},
 *     itemOperations={
 *          "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *          "put" = { "method" = "PUT", "security" = "is_granted('UPDATE', object)" },
 *          "patch" = { "method" = "PATCH", "security" = "is_granted('UPDATE', object)" },
 *          "delete" = { "method" = "DELETE", "security" = "is_granted('DELETE', object)" }
 *     },
 *     collectionOperations={
 *      "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)"
 *      },
 *      "post" = { "method" = "POST", "security_post_denormalize" = "is_granted('CREATE', object)" },
 *     }
 * )
 */
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'partial',
    'keyIndex' => 'partial',
    'country' => 'exact',
])]
class SpecialComponent implements FormReferenceEntityInterface
{
    use FormReferenceStartingTrait;

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"special_component:read"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="specialComponents")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code", name="country_code")
     *
     * @Groups({"special_component:read", "special_component:write"})
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"special_component:read", "special_component:write"})
     */
    protected $keyIndex;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Label
     *
     * @Groups({"special_component:read", "special_component:write"})
     */
    protected $name;

    /**
     * constructor.
     *
     * @param string $id
     */
    public function __construct(string $id = null)
    {
        if (null === $id) {
            $ulid = new Ulid();
            $id = $ulid->toBase32();
        }
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the keyIndex.
     *
     * @return string|null
     */
    public function getKeyIndex(): ?string
    {
        return $this->keyIndex;
    }

    /**
     * Set the keyIndex
     *
     * @param string|null $keyIndex
     *
     * @return $this
     */
    public function setKeyIndex(string $keyIndex): self
    {
        if ((!$this->starting) && self::getParametricTool()->isDefaultParametric($this)) {
            throw new \Exception(self::t("You can't update this entity."));
        }

        $this->keyIndex = $keyIndex;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        if ((!$this->starting) && self::getParametricTool()->isDefaultParametric($this)) {
            throw new \Exception(self::t("You can't update this entity."));
        }

        $this->name = $name;

        return $this;
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'id' => $this->id,
            'country' => $this->country?->getId(),
            'key_index' => $this->keyIndex,
            'name' => $this->name,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return [
            'key_index' => $this->keyIndex,
            'name' => $this->name,
        ];
    }
}
