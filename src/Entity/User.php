<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\FormReferenceStartingTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Pyrrah\GravatarBundle\GravatarApi;
use Symfony\Component\Security\Core\User\LegacyPasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotations\SecuredRepository;

/**
 * User entity.
 *
 * @ApiResource(
 *      denormalizationContext={"groups":{"user:post", "user:put"}},
 *      normalizationContext={"groups"={"user:read", "user:read_safe"}},
 *      itemOperations={
 *          "GET"={
 *              "method" = "GET",
 *              "security" = "is_granted('READ', object)",
 *              "normalization_context"={
 *                  "groups"={"user:read"}
 *              }
 *          },
 *          "PUT"={
 *              "method" = "PUT",
 *              "security" = "is_granted('UPDATE', object)",
 *              "controller" = "App\Api\Action\UserAccountAction::update",
 *              "denormalization_context"={"groups"={"user:put"}},
 *          },
 *          "DELETE"={
 *              "method" = "DELETE",
 *              "security" = "is_granted('DELETE', object)",
 *              "controller" = "App\Api\Action\UserAccountAction::delete",
 *          },
 *      },
 *      collectionOperations={
 *          "GET"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)",
 *              "normalization_context"={
 *                  "groups"={"user:read_collection"}
 *              }
 *          },
 *          "country_users"={
 *              "route_name" = "country_users",
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)",
 *              "normalization_context"={
 *                  "groups"={"user:read_safe"}
 *              },
 *              "openapi_context"={
 *                  "description"="Retrieves the collection of User resources in the selected Country.",
 *                  "parameters"={
 *                      {
 *                          "name"="country_code",
 *                          "description"="Country to query",
 *                          "in"="path",
 *                          "required"=true,
 *                          "type"="string",
 *                      },
 *                  },
 *              },
 *          },
 *          "user_me"={
 *              "route_name" = "user_me",
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)",
 *              "openapi_context"={
 *                  "summary"="Get current user",
 *                  "description"="Get current session user entity.",
 *                  "parameters"={},
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "id"={
 *                                              "type"="string",
 *                                          },
 *                                          "name"={
 *                                              "type"="string",
 *                                          },
 *                                          "email"={
 *                                              "type"="string",
 *                                          },
 *                                          "avatar"={
 *                                              "type"="string",
 *                                          },
 *                                          "country"={
 *                                              "type"="string",
 *                                          },
 *                                          "language"={
 *                                              "type"="string",
 *                                          },
 *                                          "timezone"={
 *                                              "type"="string",
 *                                          },
 *                                          "administrative_divisions"={
 *                                              "type"="array",
 *                                              "items"={
 *                                                  "type"="object",
 *                                                  "properties"={
 *                                                      "id"={
 *                                                          "type"="string",
 *                                                      },
 *                                                      "name"={
 *                                                          "type"="string",
 *                                                      },
 *                                                  },
 *                                              },
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                      "404"={
 *                          "description"="Not found",
 *                      }
 *                  }
 *              }
 *          },
 *          "login"={
 *              "method"="POST",
 *              "path"="/users/login",
 *              "openapi_context"={
 *                  "summary"="Login a user in the API",
 *                  "description"="Get a JWT token to log in. You must use 'username' and 'password' or 'refresh_token' to call. Prefix token with 'Bearer ' to use it.",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required" = {},
 *                                  "properties"={
 *                                      "username"={
 *                                          "type"="string",
 *                                      },
 *                                      "password"={
 *                                          "type"="string",
 *                                      },
 *                                      "refresh_token"={
 *                                          "type"="string",
 *                                      },
 *                                   },
 *                              }
 *                          }
 *                      }
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "token"={
 *                                              "type"="string",
 *                                          },
 *                                          "refresh_token"={
 *                                              "type"="string",
 *                                          },
 *                                          "refresh_token_expire"={
 *                                              "type"="integer",
 *                                          },
 *                                          "token_expire"={
 *                                              "type"="integer",
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                      "401"={
 *                          "description"="UNAUTHORIZED",
 *                      }
 *                  }
 *              }
 *          },
 *          "POST"={
 *              "method" = "POST",
 *              "security_post_denormalize" = "is_granted('CREATE', object)",
 *              "controller" = "App\Api\Action\UserAccountAction::register",
 *              "denormalization_context"={"groups"={"user:post"}},
 *          },
 *          "resend_activation_email"={
 *              "method"="POST",
 *              "path"="/users/resend_activation_email",
 *              "openapi_context"={
 *                  "summary"="Resend activation email",
 *                  "description"="Sends activation email to inactive user",
 *                  "requestBody"={
 *                      "content"={
 *                          "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required" = {
 *                                      "email",
 *                                  },
 *                                  "properties"={
 *                                      "email"={
 *                                          "type"="string",
 *                                      },
 *                                   },
 *                               },
 *                           },
 *                      },
 *                  },
 *                  "responses"={
 *                      "200"={
 *                          "description"="OK",
 *                          "content"={
 *                              "application/json"={
 *                                  "schema"={
 *                                      "type"="object",
 *                                      "properties"={
 *                                          "message"={
 *                                              "type"="string",
 *                                          },
 *                                      },
 *                                  },
 *                              },
 *                          },
 *                      },
 *                      "422"={
 *                          "description"="Unprocessable entity",
 *                      }
 *                  },
 *              },
 *              "controller"="App\Api\Action\UserAccountAction::resendActivationEmail",
 *              "deserialize"=false,
 *          },
 *      }
 * )
 *
 * @ORM\Entity(repositoryClass=App\Repository\UserRepository::class)
 * @ORM\HasLifecycleCallbacks
 *
 * @SecuredRepository(class=App\RepositorySecured\UserRepositorySecured::class)
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
#[ApiFilter(OrderFilter::class, properties: [
    'email' => 'partial',
    'username' => 'partial',
    'active' => 'exact',
    ], arguments: [
    'orderParameterName' => 'order',
])]
#[ApiFilter(SearchFilter::class, properties: [
    'email' => 'partial',
    'username' => 'partial',
    'active' => 'exact',
    'administrativeDivision' => 'exact',
    'language' => 'exact',
    'country' => 'exact',
])]
class User implements UserInterface, FormReferenceEntityInterface, PasswordAuthenticatedUserInterface, LegacyPasswordAuthenticatedUserInterface
{
    use StringTranslationTrait;
    use FormReferenceStartingTrait;

    /**
     * User ID.
     *
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"user:read","user:read_collection", "user:read_safe"})
     */
    protected string $id;

    /**
     * User name.
     *
     * @ORM\Column(
     *     type="string",
     *     length=180,
     *     unique=true,
     *     nullable=false
     * )
     *
     * @Assert\NotBlank
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:read_safe",
     *     "user:post",
     *     "user:put",
     * })
     */
    protected string $username;

    /**
     * User roles.
     *
     * @ORM\Column(type="json")
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:put",
     * })
     */
    protected array $roles = [];

    /**
     * The hashed password
     *
     * @ORM\Column(
     *     type="string",
     *     nullable=true
     * )
     *
     * @Groups({
     *     "user:post",
     *     "user:put",
     * })
     */
    protected ?string $password;

    /**
     * User email.
     *
     * @ORM\Column(
     *     type="string",
     *     length=255
     * )
     *
     * @Assert\NotBlank
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:post",
     *     "user:put",
     * })
     */
    protected ?string $email;

    /**
     * Account token.
     *
     * @ORM\Column(
     *     type="string",
     *     length=100,
     *     nullable=true
     * )
     *
     * @Ignore()
     */
    protected string $token;

    /**
     * Reset password token.
     *
     * @ORM\Column(
     *     type="string",
     *     length=100,
     *     nullable=true
     * )
     *
     * @Ignore()
     */
    protected ?string $resetPasswordToken;

    /**
     * Is active account.
     *
     * @ORM\Column(type="boolean")
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:put",
     * })
     */
    protected bool $active;

    /**
     * Creation date.
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"user:read_collection","user:read"})
     */
    protected \DateTime $created;

    /**
     * Updated date.
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"user:read_collection","user:read"})
     */
    protected \DateTime $updated;

    /**
     * User language.
     *
     * @ORM\ManyToOne(targetEntity=Language::class, fetch="EXTRA_LAZY")
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:post",
     *     "user:put",
     * })
     */
    protected $language;

    /**
     * User country.
     *
     * @ORM\ManyToOne(targetEntity="Country", fetch="EAGER")
     * @ORM\JoinColumn(name="country_code", referencedColumnName="code", nullable=false)
     *
     * @Groups({
     *     "user:read",
     *     "user:post",
     *     "user:put",
     * })
     */
    protected $country;

    /**
     * @ORM\ManyToMany(targetEntity="AdministrativeDivision", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="user_administrative_division",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="administrative_division_id", referencedColumnName="id")}
     * )
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:post",
     *     "user:put",
     * })
     *
     * @var ArrayCollection
     */
    protected $administrativeDivisions;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({
     *     "user:read",
     *     "user:read_collection",
     *     "user:post",
     *     "user:put",
     * })
     */
    protected $timezone;

    /**
     * User constructor.
     *
     * @param string|null $id
     */
    public function __construct(string $id = null)
    {
        if (null === $id) {
            $ulid = new Ulid();
            $id = $ulid->toBase32();
        }
        $this->id = $id;
        $this->password = null;
        $this->token = sha1(uniqid());
        $this->resetPasswordToken = null;
        $this->active = false;
        $this->created = new \DateTime();
        $this->roles = ['ROLE_USER'];
        $this->markAsUpdated();
        $this->administrativeDivisions = new ArrayCollection();
    }

    /**
     * Id.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        if (!$this->timezone) {
            return date_default_timezone_get();
        }

        return $this->timezone;
    }

    /**
     * @param mixed $timezone
     */
    public function setTimezone($timezone): void
    {
        $this->timezone = $timezone;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     *
     * @return string
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * Set user name.
     *
     * @param string $username New user name.
     *
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get roles.
     *
     * @see UserInterface
     *
     * @return array
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        sort($roles);

        return array_unique($roles);
    }

    /**
     * Set user roles.
     *
     * @param array $roles New roles.
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get current encoded password.
     *
     * @see UserInterface
     *
     * @return string
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * Set new encoded passord.
     *
     * @param string $password Encoded password.
     *
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     *
     * @Ignore()
     *
     * @return string
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * Ignore this.
     *
     * @see UserInterface
     *
     * @return void
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Get user email.
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set user email.
     *
     * @param string $email New user email.
     *
     * @return $this
     */
    public function setEmail(string $email): self
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \LogicException($this->t('Invalid email'));
        }

        $this->email = $email;

        return $this;
    }

    /**
     * Get current activation token.
     *
     * This token will be null after account activation successful.
     *
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Set new activation token.
     *
     * @param string $token New activation token.
     *
     * @return void
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * Get reset password token.
     *
     * @return ?string
     */
    public function getResetPasswordToken(): ?string
    {
        return $this->resetPasswordToken;
    }

    /**
     * Set new reset password token.
     *
     * @param ?string $resetPasswordToken New password token.
     *
     * @return void
     */
    public function setResetPasswordToken(?string $resetPasswordToken): void
    {
        $this->resetPasswordToken = $resetPasswordToken;
    }

    /**
     * Is this user active?
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * Change user activation status.
     *
     * @param bool $active New activation status.
     *
     * @return self
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * Get last modification date.
     *
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * Mark how update.
     *
     * @ORM\PreUpdate
     *
     * @return void
     */
    public function markAsUpdated(): void
    {
        $this->updated = new \DateTime();
    }

    /**
     * Get user language.
     *
     * @return Language|null
     */
    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    /**
     * Set user language.
     *
     * @param Language|null $language
     *
     * @return $this
     */
    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get user country.
     *
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * Set user country.
     *
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get administrative division collection.
     *
     * @return Collection|AdministrativeDivision[]
     */
    public function getAdministrativeDivisions(): Collection|array
    {
        return $this->administrativeDivisions;
    }

    /**
     * Add administrative division.
     *
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return $this
     */
    public function addAdministrativeDivision(AdministrativeDivision $administrativeDivision): self
    {
        if (!$this->administrativeDivisions->contains($administrativeDivision)) {
            $this->administrativeDivisions[] = $administrativeDivision;
        }

        return $this;
    }

    /**
     * Get user identity property.
     *
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * Remove administrative division.
     *
     * @param AdministrativeDivision $administrativeDivision
     *
     * @return $this
     */
    public function removeAdministrativeDivision(AdministrativeDivision $administrativeDivision): self
    {
        $this->administrativeDivisions->removeElement($administrativeDivision);

        return $this;
    }

    /**
     * Clear administrative divisions
     *
     * @return $this
     */
    public function clearAdministrativeDivision(): self
    {
        $this->administrativeDivisions->clear();

        return $this;
    }

    /**
     * @return string
     *
     * @Groups({
     *     "user:read",
     *     "user:read_safe",
     *     "user:read_collection",
     * })
     */
    public function getGravatar(): string
    {
        $resp = static::getGravatarApi()->getUrl($this->getEmail());

        return $resp;
    }

    /**
     * @return string
     *
     * @Groups({
     *     "user:read",
     *     "user:read_safe",
     *     "user:read_collection",
     * })
     */
    public function getCountryId(): string
    {
        if (!$this->country) {
            return '';
        }

        return $this->country->getId();
    }

    /**
     * @return string
     *
     * @Groups({
     *     "user:read",
     *     "user:read_safe",
     *     "user:read_collection",
     * })
     */
    public function getCountryFlag(): string
    {
        if (!$this->country || !$this->country->getFlag()) {
            return '';
        }

        return sprintf('/api/v1/files/%s/download', $this->country->getFlag()->getId());
    }

    /**
     * @return string
     *
     * @Groups({
     *     "user:read",
     *     "user:read_safe",
     *     "user:read_collection",
     * })
     */
    public function getCountryFlagId(): string
    {
        if (!$this->country || !$this->country->getFlag()) {
            return '';
        }

        return $this->country->getFlag()->getId();
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'username' => $this->username,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return [
            'username' => $this->username,
            'gravatar' => $this->getGravatar(),
        ];
    }

    /**
     * @return GravatarApi
     *
     * @Ignore()
     */
    protected static function getGravatarApi(): GravatarApi
    {
        return new GravatarApi([
            // Maximum rating (inclusive) [ g | pg | r | x ]
            'rating' => 'g',
            // Size in pixels, defaults to 80px [ 1 - 2048 ]
            'size' => 150,
            // Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
            'default' => 'identicon',
            'secure' => true,
        ]);
    }
}
