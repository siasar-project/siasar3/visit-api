<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Annotations\IsCountryParametric;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\FormReferenceStartingTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use App\Annotations\SecuredRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * [COM | 1.10.2.2] Water Quality Entity.
 *
 * @ORM\Entity(repositoryClass=App\Repository\WaterQualityEntityRepository::class)
 *
 * @IsCountryParametric
 *
 * @SecuredRepository(class=App\RepositorySecured\WaterQualityEntityRepositorySecured::class)
 *
 * @ApiResource(
 *     denormalizationContext={"groups":{"water_quality_entity:write"}},
 *     normalizationContext={"groups":{"water_quality_entity:read"}},
 *     itemOperations={
 *          "get" = { "method" = "GET", "security" = "is_granted('READ', object)" },
 *          "put" = { "method" = "PUT", "security" = "is_granted('UPDATE', object)" },
 *          "patch" = { "method" = "PATCH", "security" = "is_granted('UPDATE', object)" },
 *          "delete" = { "method" = "DELETE", "security" = "is_granted('DELETE', object)" }
 *     },
 *     collectionOperations={
 *      "get"={
 *              "method" = "GET",
 *              "security_get_denormalize" = "is_granted('READ', object)"
 *      },
 *      "post" = { "method" = "POST", "security_post_denormalize" = "is_granted('CREATE', object)" },
 *     }
 * )
 */
#[ApiFilter(OrderFilter::class)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'partial',
    'address' => 'partial',
    'contact' => 'partial',
    'phone' => 'partial',
    'country' => 'exact',
])]
class WaterQualityEntity implements FormReferenceEntityInterface
{
    use StringTranslationTrait;
    use FormReferenceStartingTrait;

    /**
     * @ORM\Id
     * @ORM\Column(
     *     name="id",
     *     type="ulid",
     *     unique=true
     * )
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @Groups({"water_quality_entity:read"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="waterQualityEntities")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code", name="country_code" )
     *
     * @Groups({"water_quality_entity:read", "water_quality_entity:write"})
     */
    #[ApiSubresource]
    protected $country;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"water_quality_entity:read", "water_quality_entity:write"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"water_quality_entity:read", "water_quality_entity:write"})
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"water_quality_entity:read", "water_quality_entity:write"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Groups({"water_quality_entity:read", "water_quality_entity:write"})
     */
    protected $contact;

    /**
     * constructor.
     * @param string $id
     */
    public function __construct(string $id = null)
    {
        if (null === $id) {
            $ulid = new Ulid();
            $id = $ulid->toBase32();
        }
        $this->id = $id;
    }

    /**
     * Get Id.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get country.
     *
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * Set the country.
     *
     * @param Country|null $country
     *
     * @return $this
     */
    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set the name
     *
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the name.
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set the address
     *
     * @param string|null $address
     *
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the phone.
     *
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set the phone
     *
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the contact info.
     *
     * @return string|null
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * Set the contact info
     *
     * @param string|null $contact
     *
     * @return $this
     */
    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return [
            'type' => static::class,
            'id' => $this->id,
            'country' => $this->country?->getId(),
            'name' => $this->name,
            'address' => $this->address,
            'phone' => $this->phone,
            'contact' => $this->contact,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return [
            'name' => $this->name,
            'contact' => $this->contact,
        ];
    }
}
