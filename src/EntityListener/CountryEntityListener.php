<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EntityListener;

use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Service\AccessManager;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use App\Entity\Country;

/**
 * onFlush Country validator.
 */
class CountryEntityListener
{
    use StringTranslationTrait;

    protected ?FormFactory $formFactory;
    protected AccessManager $accessManager;

    /**
     * Listener constructor.
     *
     * @param FormFactory   $formFactory   Form factory.
     * @param AccessManager $accessManager Access manager.
     */
    public function __construct(FormFactory $formFactory, AccessManager $accessManager)
    {
        $this->formFactory = $formFactory;
        $this->accessManager = $accessManager;
    }

    /**
     * Validate country before save it.
     *
     * @param OnFlushEventArgs $event
     *
     * @return void
     */
    public function onFlush(OnFlushEventArgs $event)
    {
        // Don't validate countries while loading test fixtures.
        if (isset($_ENV['TESTS_LOADING_FIXTURES']) && $_ENV['TESTS_LOADING_FIXTURES']) {
            return;
        }

        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();
        foreach ($entities as $entity) {
            if ($entity instanceof Country) {
                // Validate.
                $this->validateCountry($entity, $uow->getEntityChangeSet($entity));
            }
        }
    }

    /**
     * Validate country::formLevel changes.
     *
     * @param Country $country
     * @param array   $changes
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function validateCountry(Country $country, array $changes)
    {
        if (!isset($changes["formLevel"])) {
            // We need validate formLevel changes only.
            return;
        }

        $forms = $this->formFactory->findByType(InquiryFormManager::class);
        $formIds = [];
        /** @var FormManagerInterface $form */
        foreach ($forms as $form) {
            $formIds[$form->getId()] = $form;
        }
        $countryLevels = $country->getFormLevel();
        foreach ($countryLevels as $formId => $settings) {
            if (!isset($formIds[$formId])) {
                throw new \Exception(
                    $this->t(
                        'Form "@formid" not found, country "@country" level not valid.',
                        [
                            '@formid' => $formId,
                            '@country' => $country->getId(),
                        ]
                    )
                );
            }
            if (!isset($settings['level']) || $settings['level'] < 1 || $settings['level'] > 3) {
                throw new \Exception(
                    $this->t(
                        'Country "@country" level "@value" not valid to form "@formid".',
                        [
                            '@formid' => $formId,
                            '@country' => $country->getId(),
                            '@value' => isset($settings['level']) ? $settings['level'] : $this->t('Undefined'),
                        ]
                    )
                );
            }

            $currentUser = $this->accessManager->getSecurity()->getUser();
            if (!$currentUser || !$this->accessManager->hasPermission($currentUser, 'can force inquiry level')) {
                // Validate form status.
                // @see https://latteandcode.medium.com/symfony-c%C3%B3mo-detectar-cambios-en-una-colecci%C3%B3n-de-una-entidad-f7e1bf5f93ff
                $form = $formIds[$formId];
                $records = $form->countBy(
                    [
                        'field_status' => ['IN' => ['draft', 'finished']],
                        'field_country' => $country->getId(),
                    ]
                );
                if ($records > 0) {
                    throw new \Exception(
                        $this->t(
                            'Country "@country" level "@value" not valid to form "@formid" because have not validated inquiries.',
                            [
                                '@formid' => $formId,
                                '@country' => $country->getId(),
                                '@value' => $settings['level'],
                            ]
                        )
                    );
                }
            }
        }
    }
}
