<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EntityListener;

use App\Entity\InquiryFormLog;
use App\Entity\User;
use App\Forms\FormFactory;
use App\Service\SessionService;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Asset\Exception\InvalidArgumentException;

/**
 * InquiryFormLog LifeCycle listener
 */
class InquiryFormLogListener
{
    use StringTranslationTrait;

    protected ?FormFactory $formFactory;
    protected SessionService $sessionService;

    /**
     * constructor
     *
     * @param FormFactory|null $formFactory
     * @param SessionService   $sessionService
     */
    public function __construct(?FormFactory $formFactory, SessionService $sessionService)
    {
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
    }

    /**
     * prePersist entity
     *
     * @param InquiryFormLog     $entity
     * @param LifecycleEventArgs $event
     */
    public function prePersist(InquiryFormLog $entity, LifecycleEventArgs $event): void
    {
        $context = $entity->getContext();

        /** @var User $user */
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        if ($entity->getFormId() && !isset($context['form_id'])) {
            $context['form_id'] = $entity->getFormId();
            $entity->setContext($context);
        }
        if ($entity->getRecordId() && !isset($context['record_id'])) {
            $context['record_id'] = $entity->getRecordId();
            $entity->setContext($context);
        }

        if (!isset($context['form_id'])) {
            throw new InvalidArgumentException(
                $this->t(
                    "The logger requires a context 'form_id'"
                ),
                401
            );
        }

        if (!isset($context['record_id'])) {
            throw new InvalidArgumentException(
                $this->t(
                    "The logger requires a context 'record_id'",
                ),
                401
            );
        }

        // Check if form_id exists
        if (isset($_ENV['TESTS_LOADING_FIXTURES']) && !$_ENV['TESTS_LOADING_FIXTURES']) {
            $form = $this->formFactory->find($context['form_id'], false);

            if (!$form) {
                throw new \Exception(
                    $this->t(
                        "Form '@form' not found.",
                        ['@form' => $context['form_id']],
                    )
                );
            }
        }


        $entity->setFormId($context['form_id']);
        $entity->setRecordId($context['record_id']);
    }
}
