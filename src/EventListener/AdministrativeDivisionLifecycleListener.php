<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use ApiPlatform\Core\Exception\InvalidArgumentException;
use App\Entity\AdministrativeDivision;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\Persistence\Event\LifecycleEventArgs;

/**
 * Administrative division' entity change listener.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AdministrativeDivisionLifecycleListener
{
    use StringTranslationTrait;

    /**
     * Validate prePersist.
     *
     * @param AdministrativeDivision $division
     * @param LifecycleEventArgs     $event
     */
    public function prePersist(AdministrativeDivision $division, LifecycleEventArgs $event): void
    {
        if (isset($_ENV['TESTS_LOADING_FIXTURES']) && $_ENV['TESTS_LOADING_FIXTURES']) {
            return;
        }
        // Validate.
        $deep = $division->getCountry()->getDivisionTypes()->count();
        if ($deep < $division->deep()) {
            throw new InvalidArgumentException(
                self::t("No Administrative Division Type at level @deep.", ['@deep' => $division->deep()])
            );
        }
    }

    /**
     * Validate administrative division before save it.
     *
     * @param OnFlushEventArgs $event
     *
     * @return void
     */
    public function onFlush(OnFlushEventArgs $event)
    {
        // Don't validate countries while loading test fixtures.
        if (isset($_ENV['TESTS_LOADING_FIXTURES']) && $_ENV['TESTS_LOADING_FIXTURES']) {
            return;
        }

        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof AdministrativeDivision) {
                // Update internal values using the definitive ID.
                $logMetadata = $em->getClassMetadata(AdministrativeDivision::class);
                $entity->updateInternals();
                $uow->recomputeSingleEntityChangeSet($logMetadata, $entity);
            }
        }
    }
}
