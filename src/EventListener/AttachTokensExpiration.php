<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

declare(strict_types=1);

namespace App\EventListener;

use DateInterval;
use DateTimeImmutable;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Yivoff\JwtRefreshBundle\Contracts\HasherInterface;
use Yivoff\JwtRefreshBundle\Contracts\RefreshTokenProviderInterface;
use Yivoff\JwtRefreshBundle\Contracts\TokenIdGeneratorInterface;
use Yivoff\JwtRefreshBundle\Model\RefreshToken;
use function method_exists;

/**
 * Add expiration times to log in return.
 */
final class AttachTokensExpiration
{

    /**
     * @param HasherInterface               $hasher
     * @param TokenIdGeneratorInterface     $tokenIdGenerator
     * @param string                        $parameterName
     * @param int                           $tokenShelfLife
     * @param RefreshTokenProviderInterface $refreshTokenProvider
     * @param JWTTokenManagerInterface      $tokenManager
     */
    public function __construct(private HasherInterface $hasher, private TokenIdGeneratorInterface $tokenIdGenerator, private string $parameterName, private int $tokenShelfLife, private RefreshTokenProviderInterface $refreshTokenProvider, private JWTTokenManagerInterface $tokenManager)
    {
    }

    /**
     * @param AuthenticationSuccessEvent $event
     *
     * @return void
     */
    public function __invoke(AuthenticationSuccessEvent $event): void
    {
        /** @var UserInterface $user */
        $data = $event->getData();
        [$refreshTokenId, $userProvidedVerification] = explode(':', $data[$this->parameterName]);
        /** @var RefreshToken $refreshToken */
        $refreshToken = $this->refreshTokenProvider->getTokenWithIdentifier($refreshTokenId);
        $data['refresh_token_expire'] = $refreshToken->getValidUntil();

        $token = $this->tokenManager->parse($data['token']);
        $data['token_expire'] = $token["exp"];

        $event->setData($data);
    }
}
