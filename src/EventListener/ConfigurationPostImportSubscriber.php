<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Command\Event\ConfigurationBaseEvent;
use App\Command\Event\ConfigurationPostImportEvent;
use App\Forms\FormDatabaseUpdater;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Service\ConfigurationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Configuration end import event subscriber.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationPostImportSubscriber implements EventSubscriberInterface
{
    protected ConfigurationService $configurationService;

    /**
     * FormsConfigurationSubscriber constructor.
     *
     * @param ConfigurationService $configurationService
     */
    public function __construct(ConfigurationService $configurationService)
    {
        $this->configurationService = $configurationService;
    }

    /**
     * Get subscribed events.
     *
     * @return \string[][]
     */
    public static function getSubscribedEvents():array
    {
        return [
            'configuration.post.import' => ['onConfigurationPostImport'],
        ];
    }

    /**
     * On configuration post import.
     *
     * @param ConfigurationPostImportEvent $event The event information.
     */
    public function onConfigurationPostImport(ConfigurationPostImportEvent $event): void
    {
        // Custom database updates.
        $this->configurationService->runUpdate($event->getIo());
    }

    /**
     * Display a note.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function note(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->note($message);
        }
    }

    /**
     * Display a comment.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function comment(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->comment($message);
        }
    }
}
