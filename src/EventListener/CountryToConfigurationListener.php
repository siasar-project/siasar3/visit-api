<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Entity\Configuration;
use App\Entity\Country;
use App\Entity\DisinfectingSubstance;
use App\Entity\DistributionMaterial;
use App\Entity\FunctionsCarriedOutTap;
use App\Entity\FunctionsCarriedOutWsp;
use App\Entity\GeographicalScope;
use App\Entity\Material;
use App\Entity\SpecialComponent;
use App\Entity\TypeHealthcareFacility;
use App\Entity\TypeTap;
use App\Entity\TypologyChlorinationInstallation;
use App\Repository\ConfigurationRepository;
use App\Repository\GeographicalScopeRepository;
use App\Traits\GetContainerTrait;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Country to configuration listener.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class CountryToConfigurationListener
{
    use GetContainerTrait;

    protected ConfigurationRepository $configurationRepository;
    protected ?Registry $entityManager;

    /**
     * CountryToConfigurationListener constructor.
     */
    public function __construct()
    {
        $this->entityManager = $this->getContainerInstance()->get('doctrine');
        $this->configurationRepository = $this->entityManager->getRepository(Configuration::class);
    }

    /**
     * After create entity.
     *
     * @param Country            $country Entity.
     * @param LifecycleEventArgs $event   Event.
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postPersist(Country $country, LifecycleEventArgs $event): void
    {
        if (isset($country->_importing) && $country->_importing) {
            // Don't allow to enter while importing.
            return;
        }

        $cfg = $this->configurationRepository->create(
            $country->configuratioGetConfigurationId(),
            $country->configuratioGetConfigurationValue()
        );
        $cfg->saveNow();
    }

    /**
     * Post update listener.
     *
     * Update the configuration after entity updates.
     *
     * @param Country            $country Country.
     * @param LifecycleEventArgs $event   Event data.
     *
     * @throws \Exception
     */
    public function postUpdate(Country $country, LifecycleEventArgs $event): void
    {
        if (isset($country->_importing) && $country->_importing) {
            return;
        }

        /** @var Configuration\ConfigurationWriteInterface $cfg */
        $cfg = $this->configurationRepository->findEditable($country->configuratioGetConfigurationId());
        $cfg->setValue($country->configuratioGetConfigurationValue());
        $cfg->saveNow();
    }

    /**
     * Remove obsoleted configurations.
     *
     * @param Country            $country Country.
     * @param LifecycleEventArgs $event   Event data.
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function postRemove(Country $country, LifecycleEventArgs $event): void
    {
        if (isset($country->_importing) && $country->_importing) {
            return;
        }

        $this->configurationRepository->delete($country->configuratioGetConfigurationId());
    }
}
