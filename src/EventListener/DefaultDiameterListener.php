<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Entity\DefaultDiameter;
use App\Repository\ConfigurationRepository;
use App\Repository\DefaultDiameterRepository;
use App\Traits\StringTranslationTrait;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Asset\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Default Diameter Entity LifeCycle listener
 */
class DefaultDiameterListener
{
    use StringTranslationTrait;

    protected DefaultDiameterRepository $defaultDiameterRepository;
    protected ConfigurationRepository $configurationRepository;

    /**
     * constructor
     *
     * @param DefaultDiameterRepository $defaultDiameterRepository
     * @param ConfigurationRepository   $configurationRepository
     */
    public function __construct(DefaultDiameterRepository $defaultDiameterRepository, ConfigurationRepository $configurationRepository)
    {
        $this->defaultDiameterRepository = $defaultDiameterRepository;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * Don't allow unknown units.
     *
     * @param DefaultDiameter    $defaultDiameter the entity to check
     * @param LifecycleEventArgs $event           the event that was dispatch
     *
     */
    public function prePersist(DefaultDiameter $defaultDiameter, LifecycleEventArgs $event):void
    {
        $units = $this->configurationRepository->find('system.units.length');
        if (!empty($units)) {
            /** @var DefaultDiameter $entity */
            $entity = $event->getObject();
            $unit = $entity->getUnit();
            if (!isset($units->getValue()[$unit])) {
                throw new InvalidArgumentException(
                    $this->t(
                        'Unknown length unit: "@unit"',
                        ['@unit' => $entity->getUnit()]
                    ),
                    401
                );
            }
        }
    }

    /**
     * Don't allow unknown units.
     *
     * @param DefaultDiameter    $defaultDiameter the entity to check
     * @param LifecycleEventArgs $event           the event that was dispatch
     *
     */
    public function preUpdate(DefaultDiameter $defaultDiameter, LifecycleEventArgs $event):void
    {
        $this->prePersist($defaultDiameter, $event);
    }
}
