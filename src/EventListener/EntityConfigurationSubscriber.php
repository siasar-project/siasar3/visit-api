<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Command\Event\ConfigurationBaseEvent;
use App\Entity\AdministrativeDivision;
use App\Entity\Configuration\EntityToConfigurationInterface;
use App\Entity\Country;
use App\Entity\Language;
use App\Forms\FormFactory;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\CountryRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Entity to configuration change listener.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class EntityConfigurationSubscriber implements EventSubscriberInterface
{

    protected ?Registry $entityManager;

    /**
     * FormsConfigurationSubscriber constructor.
     *
     * @param Registry|null $entityManager
     */
    public function __construct(Registry $entityManager = null)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get subscribed events.
     *
     * @return \string[][]
     */
    public static function getSubscribedEvents():array
    {
        return [
            // 'configuration.pre.create' => ['onConfigurationPreCreate'],
            'configuration.pre.update' => ['onConfigurationPreUpdate'],
            // 'configuration.pre.delete' => ['onConfigurationPreDelete'],
            'configuration.post.create' => ['onConfigurationPostCreate'],
            'configuration.post.update' => ['onConfigurationPostUpdate'],
            'configuration.post.delete' => ['onConfigurationPostDelete'],
        ];
    }

    /**
     * On configuration post create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostCreate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isCompatible($event)) {
            return;
        }
        $values = $event->getOld();
        $class = $values['type'];
        $entity = $class::createFromConfigurationValue($values, $this->entityManager->getManager());
        // We are using a not defined property to mark this entity how an imported instance.
        // This is a PHP functionality.
        $entity->_importing = true;

        $this->entityManager->getManager()->persist($entity);
        $this->entityManager->getManager()->flush();
    }

    /**
     * On configuration post update.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostUpdate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isCompatible($event) && $event->getId() !== 'system.languages') {
            return;
        }
        $values = $event->getNew();
        if ($event->getId() === 'system.languages') {
            $class = Language::class;
        } else {
            $class = $values['type'];
        }

        switch ($class) {
            case Country::class:
                $repo = $this->entityManager->getManager()->getRepository($class);
                $entity = $repo->find($values['code']);
                $entity->updateFromConfigurationValue($values, $this->entityManager->getManager());
                // We are using a not defined property to mark this entity how an imported instance.
                // This is a PHP functionality.
                $entity->_importing = true;

                $this->entityManager->getManager()->persist($entity);
                $this->entityManager->getManager()->flush();
                break;
            case Language::class:
                $repo = $this->entityManager->getManager()->getRepository($class);
                foreach ($values as $id => $value) {
                    /** @var Language $entity */
                    $entity = $repo->find($id);
                    $entity->setTranslatable($value['translatable']);
                    $entity->setIsoCode($value['iso_code']);
                    $entity->setName($value['name']);
                    $entity->setLtr($value['ltr']);
                    $entity->setPluralNumber($value['plural_number']);
                    $entity->setPluralFormula($value['plural_formula']);
                    // We are using a not defined property to mark this entity how an imported instance.
                    // This is a PHP functionality.
                    $entity->_importing = true;
                    $this->entityManager->getManager()->persist($entity);
                }
                $this->entityManager->getManager()->flush();
                break;
        }
    }

    /**
     * On configuration post create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostDelete(ConfigurationBaseEvent $event): void
    {
        if (!$this->isCompatible($event)) {
            return;
        }
        $values = $event->getOld();
        $class = $values['type'];
        switch ($class) {
            case Country::class:
                /** @var CountryRepository $repo */
                $repo = $this->entityManager->getManager()->getRepository($class);
                $entity = $repo->find($values['code']);
                // We are using a not defined property to mark this entity how an imported instance.
                // This is a PHP functionality.
                $entity->_importing = true;
                $entity->preDeleteFromConfigurationValue($values, $this->entityManager->getManager());

                $repo->removeNow($entity);
                break;
        }
    }

    /**
     * On configuration previous create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreCreate(ConfigurationBaseEvent $event): void
    {
    }

    /**
     * On configuration previous update.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreUpdate(ConfigurationBaseEvent $event): void
    {
        $values = $event->getNew();
        if (isset($values['type'])) {
            $class = $values['type'];
            switch ($class) {
                case Country::class:
                    $repo = $this->entityManager->getManager()->getRepository($class);
                    /** @var Country $entity */
                    $entity = $repo->find($values['code']);
                    // Validate the new configuration.
                    if (!isset($values['code']) || !isset($values['type']) || $values['type'] !== Country::class || $values['code'] !== $entity->getId()) {
                        throw new \Exception(
                            sprintf(
                                'Invalid update configuration value in class "%s" with values: %s',
                                self::class,
                                print_r($values, true)
                            )
                        );
                    }
                    // New value must have at least the same item's number, if the country has administrative divisions.
                    $divisionTypesCount = (isset($values['divisionTypes']) ? count($values['divisionTypes']) : 0);
                    if ($divisionTypesCount < $entity->getDivisionTypes()->count()) {
                        /** @var AdministrativeDivisionRepository $adRepository */
                        $adRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
                        if ($adRepository->count(['country' => $entity->getId()]) > 0) {
                            $adts = $entity->getDivisionTypes();
                            $oldLabels = [];
                            foreach ($adts as $adt) {
                                $oldLabels[] = $adt->getName();
                            }
                            throw new \Exception(
                                sprintf(
                                    "Error updating Country '%s', if the country has administrative divisions can't change administrative divisions types number.\nOld: %s\nNew: %s\nTo do the update requires custom implementation.",
                                    $entity->getId(),
                                    print_r($oldLabels, true),
                                    print_r(isset($values['divisionTypes']) ? $values['divisionTypes'] : [], true),
                                )
                            );
                        }
                    }
                    // If the new length it's greater don't require actions because the country doesn't have
                    // administrative divisions in the new levels.
                    break;
            }
        }
    }

    /**
     * On configuration previous delete.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreDelete(ConfigurationBaseEvent $event): void
    {
    }

    /**
     * Display a note.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function note(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->note($message);
        }
    }

    /**
     * Display a comment.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function comment(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->comment($message);
        }
    }

    /**
     * Is this a compatible configuration?
     *
     * @param ConfigurationBaseEvent $event The event data.
     *
     * @return bool
     */
    protected function isCompatible(ConfigurationBaseEvent $event): bool
    {
        if (substr($event->getId(), 0, 11) !== 'app.entity.') {
            return false;
        }

        $new = $event->getNew();
        if (!$new) {
            $new = $event->getOld();
        }
        if (!isset($new['type'])) {
            return false;
        }

        $implements = class_implements($new['type']);

        return (in_array('App\Entity\Configuration\EntityToConfigurationInterface', $implements));
    }
}
