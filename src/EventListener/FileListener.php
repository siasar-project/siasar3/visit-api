<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Entity\File;
use App\Forms\FieldTypes\Types\FileReferenceEntity;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\RepositorySecured\CountryRepositorySecured;
use App\Tools\ImageOptimizer;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Uid\Ulid;

/**
 * File entity LifeCycle listener
 */
class FileListener
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected FormFactory $formFactory;
    protected CountryRepositorySecured $countryReporsitory;
    protected ImageOptimizer $imageOptimizer;

    /**
     * constructor
     *
     * @param FormFactory              $formFactory
     * @param CountryRepositorySecured $countryReporsitory
     * @param ImageOptimizer           $imageOptimizer
     */
    public function __construct(FormFactory $formFactory, CountryRepositorySecured $countryReporsitory, ImageOptimizer $imageOptimizer)
    {
        $this->formFactory = $formFactory;
        $this->countryReporsitory = $countryReporsitory;
        $this->imageOptimizer = $imageOptimizer;
    }

    /**
     * pre remove check
     *
     * @param File               $file  the file to remove
     * @param LifecycleEventArgs $event the event that was dispatch
     */
    public function preRemove(File $file, LifecycleEventArgs $event):void
    {
        // If the event is executed from CLI, we shouldn't check if the record is in use in any form
        // because it doesn't have a user session.
//        if (PHP_SAPI !== 'cli') {
//            // Check if this file is used in forms
//            // Note: This functionality is also implemented in src/EventListener/ParametricListener.php
//            $forms = $this->formFactory->findAll();
//
//            /** @var FormManagerInterface $form */
//            foreach ($forms as $form) {
//                if (!$form->isInstalled()) {
//                    continue;
//                }
//
//                $fields = $form->getFields();
//
//                foreach ($fields as $field) {
//                    if ($field instanceof FileReferenceEntity) {
//                        $fieldProperties = $field->getProperties();
//
//                        if ($fieldProperties['class'] === $file::class) {
//                            $ulid = new Ulid($file->getId());
//                            $record = $form->findBy([$field->getId() => $ulid->toBinary()], [], 1);
//
//                            if (count($record) > 0) {
//                                throw new \Exception(
//                                    $this->t(
//                                        '@object couldn\'t be removed. It\'s used in @formId.',
//                                        [
//                                            '@object' => $file::class,
//                                            '@formId' => $form->getId(),
//                                        ]
//                                    )
//                                );
//                            }
//                        }
//                    }
//                }
//            }
//        }

        // Check if this file is used by other entity
        $country = $this->countryReporsitory->findOneBy(['flag' => $file->getId()]);

        if (!is_null($country)) {
            throw new \Exception(
                $this->t(
                    'This file couldn\'t be removed. It\'s in use by the entity @class, @country.',
                    [
                        '@class' => $country::class,
                        '@country' => $country->getName(),
                    ]
                )
            );
        }

        $this->imageOptimizer->clearOptimizations($file->getPath());
    }
}
