<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener\Forms;

use App\Forms\Event\FormRecordFormatEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Form record format subscriber.
 */
class FormRecordFormatSubscriber implements EventSubscriberInterface
{

    /**
     * form.community
     *
     * @param FormRecordFormatEvent $event
     */
    public function onRecordFormCommunityFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.community.contact
     *
     * @param FormRecordFormatEvent $event
     */
    public function onRecordFormCommunityContactFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();
        $format['field_name'] = $event->getRecord()->{'field_name'};
        $format['field_occupation'] = $event->getRecord()->{'field_occupation'};
        $event->setFormat($format);
    }

    /**
     * form.community.household
     *
     * @param FormRecordFormatEvent $event
     */
    public function onRecordFormCommunityHouseholdFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.community.intervention
     *
     * @param FormRecordFormatEvent $event
     */
    public function onRecordFormCommunityInterventionFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.health.care
     *
     * @param FormRecordFormatEvent $event
     */
    public function onRecordFormHealthCareFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.health.contact
     *
     * @param FormRecordFormatEvent $event
     */
    public function onRecordFormHealthCareContactFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.point
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormPointFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.school
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormSchoolFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.school.contact
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormSchoolContactFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.tap
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormTapFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.tap.contact
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormTapContactFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wsprovider
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWsproviderFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wsprovider.contact
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWsproviderContactFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wsprovider.tap
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWsproviderTapFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.communities
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemCommunitiesFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.contact
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemContactFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.intakeflow
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemIntakeflowFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.projects
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemProjectsFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.qualitybacteriological
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemQualitybacteriologicalFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.qualityphysiochemical
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemQualityphysiochemicalFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.residualtests
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemResidualtestsFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.sourceflow
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemSourceflowFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * form.wssystem.transmisionline
     *
     * @param FormRecordFormatEvent $event
     */
    public function onFormRecordFormWssystemTransmisionlineFormat(FormRecordFormatEvent $event)
    {
        $format = $event->getFormat();

        $event->setFormat($format);
    }

    /**
     * Events subscribed to.
     *
     * @return string[]
     */
    public static function getSubscribedEvents()
    {
        return [
            'formrecord.form.community' => 'onRecordFormCommunityFormat',
            'formrecord.format.form.community.contact' => 'onRecordFormCommunityContactFormat',
            'formrecord.form.community.household' => 'onRecordFormCommunityHouseholdFormat',
            'formrecord.form.community.intervention' => 'onRecordFormCommunityInterventionFormat',
            'formrecord.form.health.care' => 'onRecordFormHealthCareFormat',
            'formrecord.form.health.care.contact' => 'onRecordFormHealthCareContactFormat',
            'formrecord.form.point' => 'onFormRecordFormPointFormat',
            'formrecord.form.school' => 'onFormRecordFormSchoolFormat',
            'formrecord.form.school.contact' => 'onFormRecordFormSchoolContactFormat',
            'formrecord.form.tap' => 'onFormRecordFormTapFormat',
            'formrecord.form.tap.contact' => 'onFormRecordFormTapContactFormat',
            'formrecord.form.wsprovider' => 'onFormRecordFormWsproviderFormat',
            'formrecord.form.wsprovider.contact' => 'onFormRecordFormWsproviderContactFormat',
            'formrecord.form.wsprovider.tap' => 'onFormRecordFormWsproviderTapFormat',
            'formrecord.form.wssystem' => 'onFormRecordFormWssystemFormat',
            'formrecord.form.wssystem.communities' => 'onFormRecordFormWssystemCommunitiesFormat',
            'formrecord.form.wssystem.contact' => 'onFormRecordFormWssystemContactFormat',
            'formrecord.form.wssystem.intakeflow' => 'onFormRecordFormWssystemIntakeflowFormat',
            'formrecord.form.wssystem.projects' => 'onFormRecordFormWssystemProjectsFormat',
            'formrecord.form.wssystem.qualitybacteriological' => 'onFormRecordFormWssystemQualitybacteriologicalFormat',
            'formrecord.form.wssystem.qualityphysiochemical' => 'onFormRecordFormWssystemQualityphysiochemicalFormat',
            'formrecord.form.wssystem.residualtests' => 'onFormRecordFormWssystemResidualtestsFormat',
            'formrecord.form.wssystem.sourceflow' => 'onFormRecordFormWssystemSourceflowFormat',
            'formrecord.form.wssystem.transmisionline' => 'onFormRecordFormWssystemTransmisionlineFormat',
        ];
    }
}
