<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener\Forms;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Entity\InquiryFormLog;
use App\Forms\Event\InquiryFormManagerEvent;
use App\Forms\FieldTypes\Types\BooleanFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\PointFormManager;
use App\Forms\SubformFormManager;
use App\Repository\HouseholdProcessRepository;
use App\RepositorySecured\HouseholdProcessRepositorySecured;
use App\Service\SessionService;
use App\Tools\CommunityWithoutHouseholdCalcs;
use App\Tools\SiasarPointWrapper;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Form manager event subscriber.
 */
class InquiryFormManagerSubscriber implements EventSubscriberInterface
{
    use StringTranslationTrait;

    protected ?EntityManagerInterface $em;
    protected HouseholdProcessRepositorySecured $householdProcessRepo;
    protected FormFactory $formFactory;
    protected SessionService $sessionService;
    protected IriConverterInterface $iriConverter;

    /**
     * InquiryFormManagerSubscriber constructor.
     *
     * @param EntityManagerInterface|null       $em
     * @param HouseholdProcessRepositorySecured $householdProcessRepo
     * @param FormFactory                       $formFactory
     * @param SessionService                    $sessionService
     * @param IriConverterInterface             $iriConverter
     */
    public function __construct(EntityManagerInterface $em = null, HouseholdProcessRepositorySecured $householdProcessRepo, FormFactory $formFactory, SessionService $sessionService, IriConverterInterface $iriConverter)
    {
        $this->em = $em;
        $this->householdProcessRepo = $householdProcessRepo;
        $this->formFactory = $formFactory;
        $this->sessionService = $sessionService;
        $this->iriConverter = $iriConverter;
    }

    /**
     * Events subscribed to.
     *
     * @return string[]
     */
    public static function getSubscribedEvents()
    {
        return array(
            // 'form.record.precreate' => 'onPreCreateRecord',
            'form.inquiry.record.postcreate' => 'onPostCreateRecord',
            'form.inquiry.record.preupdate' => 'onPreUpdateRecord',
            'form.subform.record.preupdate' => 'onPreUpdateRecord',
            'form.inquiry.record.postupdate' => 'onPostUpdateRecord',
            'form.subform.record.postupdate' => 'onPostUpdateRecord',
            'form.inquiry.record.predrop' => 'onPreDropRecord',
            // 'form.inquiry.record.postdrop' => 'onPostDropRecord',
            'form.inquiry.record.postclone' => 'onPostCloneRecord',
        );
    }

    /**
     * On post create new form record.
     *
     * @param InquiryFormManagerEvent $event The event information.
     */
    public function onPostCreateRecord(InquiryFormManagerEvent $event)
    {
        /** @var FormManager $formManager */
        $formManager = $event->getFormManager();
        $formId = $formManager->getId();
        $data = $event->getData();
        $record = $formManager->find($data['id']);

        switch ($formId) {
            case "form.community":
                if (!isset($data['field_region']) || (!$data['field_region'] instanceof AdministrativeDivision && empty($data['field_region']['value']))) {
                    return;
                }
                // If the community form has "households" checked, create a new householdProcess
                if (isset($data['field_have_households']) && $data['field_have_households']) {
                    // Get the country
                    $countryRepo = $this->em->getRepository(Country::class);
                    $country = $countryRepo->find($data['field_country']['value']);

                    // Get the administrative division
                    $admDivRepo = $this->em->getRepository(AdministrativeDivision::class);
                    if ($data['field_region'] instanceof AdministrativeDivision) {
                        $admDiv = $admDivRepo->find($data['field_region']->getId());
                    } else {
                        $admDiv = $admDivRepo->find($data['field_region']['value']);
                    }

                    /** @var HouseholdProcess $householdProcess */
                    $householdProcess = new HouseholdProcess();
                    $householdProcess->setCommunityReference($data['id']);
                    $householdProcess->setCountry($country);
                    $householdProcess->setAdministrativeDivision($admDiv);
                    $this->householdProcessRepo->saveNow($householdProcess);
                    $record->{'field_household_process'} = $householdProcess;
                    $record->save();
                }
                break;
        }
    }

    /**
     * On pre update a form record.
     *
     * @param InquiryFormManagerEvent $event The event information.
     */
    public function onPreUpdateRecord(InquiryFormManagerEvent $event)
    {
        $formId = $event->getFormManager()->getId();
        /** @var FormRecord $record */
        $record = $event->getRecord();
        $originalData = $record->getOriginal();
        $newData = $record->getCurrentRaw();

        switch ($formId) {
            case "form.community":
                // Check that inquiry don't go to validated with errors.
                $this->stopValidatingInquiryWithErrors($record);
                // Check point status to allow update inquiries
                $this->checkCanUpdateInquiriesOnPoint($record);

                foreach ($record->getModifiedFields() as $fieldName) {
                    switch ($fieldName) {
                        // Validate if the community form is trying to change the value of field_have_households
                        case 'field_have_households':
                            if ($originalData[$fieldName]) {
                                $original = BooleanFieldType::isTrue($originalData[$fieldName]);
                                $newValue = BooleanFieldType::isTrue($newData[$fieldName]);
                                if ($original && $original !== $newValue) {
                                    $householdProcess = $this->householdProcessRepo->findBy([
                                        "communityReference" => $record->{'id'},
                                    ]);

                                    if (count($householdProcess) > 0) {
                                        throw new \Exception(
                                            sprintf(
                                                "Error updating the form '%s'. The 'form.community::field_have_households' field cannot be modified.",
                                                $formId
                                            )
                                        );
                                    }
                                }
                                // Get the country
                                $countryRepo = $this->em->getRepository(Country::class);
                                $country = $countryRepo->find($record->{'field_country'}->getId());

                                // Get the administrative division
                                $admDivRepo = $this->em->getRepository(AdministrativeDivision::class);
                                $admDiv = $admDivRepo->find($record->{'field_region'}->getId());

                                // Create household record if not exist.
                                $oldProcess = $record->{'field_household_process'};
                                if (!$oldProcess) {
                                    /** @var HouseholdProcess $householdProcess */
                                    $householdProcess = new HouseholdProcess();
                                    $householdProcess->setCommunityReference($record->{'id'});
                                    $householdProcess->setCountry($country);
                                    $householdProcess->setAdministrativeDivision($admDiv);
                                    $this->householdProcessRepo->saveNow($householdProcess);
                                    $record->{'field_household_process'} = $householdProcess;
                                }
                            }
                            break;
                        case 'field_status':
                            if ('draft' === $record->{'field_status'} && $record->{'field_have_households'}) {
                                // Reopen household process to allow fix it.
                                /** @var HouseholdProcessRepository $householdProcessRepo */
                                $householdProcessRepo = $this->em->getRepository(HouseholdProcess::class);
                                $householdProcess = $record->{'field_household_process'};
                                // Reload the process to update the doctrine entity manager.
                                /** @var HouseholdProcess $householdProcess */
                                $householdProcess = $householdProcessRepo->find($householdProcess->getId());
                                $householdProcess->setOpen(true);
                                $householdProcessRepo->saveNow($householdProcess);
                            }
                            if ('finished' === $record->{'field_status'} && !$record->{'field_have_households'}) {
                                // Execute without household calcs.
                                // Fire household calcs.
                                $executor = new CommunityWithoutHouseholdCalcs();
                                $executor->executeInits($record);
                                $executor->executeCalcs($record);
                                $record->getForm()->update($record, true);
                            }
                            break;
                    }
                }

                // Check if the community form change the administrative division to update the Household Process too.
                $recordUlid = Ulid::fromString($record->{'id'});
                if (in_array('field_region', $record->getModifiedFields())) {
                    $householdProcess = $this->householdProcessRepo->findOneBy([
                        "communityReference" => $recordUlid->toBinary(),
                    ]);
                    if ($householdProcess) {
                        // Get the administrative division
                        $admDivRepo = $this->em->getRepository(AdministrativeDivision::class);
                        $admDiv = $admDivRepo->find($record->{'field_region'}->getId());

                        $householdProcess->setAdministrativeDivision($admDiv);
                        $this->householdProcessRepo->saveNow($householdProcess);
                    }
                }

                // Check if the form was logical deleted to close the related Household Process.
                if (in_array('field_deleted', $record->getModifiedFields()) && $record->{'field_deleted'}) {
                    $householdProcess = $this->householdProcessRepo->findOneBy([
                        "communityReference" => $recordUlid->toBinary(),
                    ]);

                    if ($householdProcess) {
                        $this->householdProcessRepo->closeProcess($householdProcess);
                    }
                }
                // If field 1.5 is modified then we must reopen household process.
                if (in_array('field_total_households', $record->getModifiedFields())) {
                    $householdProcess = $this->householdProcessRepo->findOneBy([
                        "communityReference" => $recordUlid->toBinary(),
                    ]);

                    if ($householdProcess) {
                        $householdProcess->setOpen(true);
                        $this->householdProcessRepo->saveNow($householdProcess);
                    }
                }
                // If we are finishing we must clear errors.
                $this->afterFinishInquiryClearErrors($record);
                break;
            case "form.community.household":
                // Check if it is "finished" to prevent updating
                if ($originalData['field_finished']['value']) {
                    throw new \Exception(
                        sprintf(
                            "Error updating the form '%s'. The survey is closed.",
                            $formId
                        )
                    );
                }
                // Throw exception if is trying to update the HouseholdProcess ID.
                if (in_array('field_household_process', $record->getModifiedFields())) {
                    if ($originalData['field_household_process']['value'] !== $newData['field_household_process']['value']) {
                        throw new \Exception(
                            sprintf(
                                "Error updating the form '%s'. The 'field_household_process' cannot be modified.",
                                $formId
                            )
                        );
                    }
                }
                break;
            case 'form.wssystem':
                // Check that inquiry don't go to validated with errors.
                $this->stopValidatingInquiryWithErrors($record);
                // Check point status to allow update inquiries
                $this->checkCanUpdateInquiriesOnPoint($record);
                // Point fusion checks.
                $this->detectSurveysToUpdatePoint($record);
                // If we are finishing we must clear errors.
                $this->afterFinishInquiryClearErrors($record);
                break;
            case "form.wssystem.communities":
                /** @var SubformFormManager $subform */
                $subform = $record->getForm();
                $parentRecord = $subform->getParentRecord($record);
                if (!$parentRecord) {
                    return;
                }
                // Check point status to allow update inquiries
                $this->checkCanUpdateInquiriesOnPoint($parentRecord);
                break;
            case "form.wsprovider":
                // Check that inquiry don't go to validated with errors.
                $this->stopValidatingInquiryWithErrors($record);
                // Check point status to allow update inquiries
                $this->checkCanUpdateInquiriesOnPoint($record);
                // If we are finishing we must clear errors.
                $this->afterFinishInquiryClearErrors($record);
                break;
            default:
                if (InquiryFormManager::class === $record->getForm()->getType()) {
                    // Check that inquiry don't go to validated with errors.
                    $this->stopValidatingInquiryWithErrors($record);
                    // If we are finishing we must clear errors.
                    $this->afterFinishInquiryClearErrors($record);
                }
                break;
        }
    }

    /**
     * On Post Update a form record.
     *
     * @param InquiryFormManagerEvent $event The event information.
     */
    public function onPostUpdateRecord(InquiryFormManagerEvent $event)
    {
        $formId = $event->getFormManager()->getId();
        /** @var FormRecord $record */
        $record = $event->getRecord();
        $originalData = $record->getOriginal();
        $newData = $record->getCurrentRaw();

        switch ($formId) {
            case "form.community.household":
                // If it is marked as "finished", update the HouseholdProcess counter
                if (in_array('field_finished', $record->getModifiedFields()) && $record->{'field_finished'}) {
                    $householdProcess = $this->householdProcessRepo->find($record->{'field_household_process'});
                    $this->householdProcessRepo->updateFinishedCounter($householdProcess);
                }
                break;
            case "form.wsprovider":
                // "Finish" transition: from 'draft' to 'finished'
                if ('draft' === $originalData['field_status']['value'] && 'finished' === $newData['field_status']['value']) {
                    $this->detectFusionAtFinishWsp($record);
                }
                // "Validate" transition: from 'finished' to 'validated'
                if ('finished' === $originalData['field_status']['value'] && 'validated' === $newData['field_status']['value']) {
                    if ('3' === $record->{'field_provider_type'} || '4' === $record->{'field_provider_type'}) {
                        $this->recalculatePoints($record);
                    }
                }
                break;
        }
    }

    /**
     * On pre delete a form record.
     *
     * @param InquiryFormManagerEvent $event The event information.
     */
    public function onPreDropRecord(InquiryFormManagerEvent $event)
    {
        $formId = $event->getFormManager()->getId();
        $record = $event->getRecord();

        switch ($formId) {
            case "form.community":
                $pointManager = $this->formFactory->find('form.point');
                $point = current($pointManager->findByCommunity($record->getId()));

                $this->checkCanDeleteInquiryOnPoint($record, $point);

                if ('planning' !== $point->{'field_status'}) {
                    // Unset community in the point
                    $communities = $point->{'field_communities'};
                    foreach ($point->{'field_communities'} as $key => $community) {
                        if ($record->getId() === $community->getId()) {
                            unset($communities[$key]);
                        }
                    }
                    $point->{'field_communities'} = array_values($communities);
                    $point->save();
                }

                // Remove household process
                $recordUlid = Ulid::fromString($record->{'id'});
                $householdProcess = $this->householdProcessRepo->findOneBy([
                    "communityReference" => $recordUlid->toBinary(),
                ]);
                if (!$householdProcess) {
                    return;
                }
                $this->householdProcessRepo->removeNow($householdProcess);
                break;

            case "form.wssystem":
                $pointManager = $this->formFactory->find('form.point');
                $point = current($pointManager->findByWSystem($record->getId()));
                if ($point) {
                    $this->checkCanDeleteInquiryOnPoint($record, $point);
                    if ('planning' !== $point->{'field_status'}) {
                        // Remove system from Point
                        $pointSystems = $point->{'field_wsystems'};
                        foreach ($point->{'field_wsystems'} as $key => $system) {
                            if ($record->getId() === $system->getId()) {
                                unset($pointSystems[$key]);
                            }
                        }
                        $point->{'field_wsystems'} = $pointSystems;
                        $point->save();
                    }
                }
                break;

            case 'form.wsprovider':
                $pointManager = $this->formFactory->find('form.point');
                $point = current($pointManager->findByWsp($record->getId()));
                if ($point) {
                    $this->checkCanDeleteInquiryOnPoint($record, $point);
                    if ('planning' !== $point->{'field_status'}) {
                        // Remove WSP from Point.
                        $pointWsps = $point->{'field_wsps'};
                        foreach ($point->{'field_wsps'} as $key => $wsp) {
                            if ($record->getId() === $wsp->getId()) {
                                unset($pointWsps[$key]);
                            }
                        }
                        $point->{'field_wsps'} = $pointWsps;
                        $point->save();
                    }
                }
                break;
        }
    }

    /**
     * On post clone a form record.
     *
     * @param InquiryFormManagerEvent $event The event information.
     */
    public function onPostCloneRecord(InquiryFormManagerEvent $event)
    {
        /** @var FormManager $formManager */
        $formManager = $event->getFormManager();
        $formId = $formManager->getId();
        $data = $event->getData();
        $sourceRecord = $formManager->find($data['oldId']);
        $clonRecord = $formManager->find($data['newId']);

        switch ($formId) {
            case 'form.wsprovider':
                // Update alive points to the new WSP
                /** @var PointFormManager $pointManager */
                $pointManager = $this->formFactory->find('form.point');
                $points = $pointManager->findByWspRef($clonRecord->{'field_ulid_reference'});

                foreach ($points as $point) {
                    // Update WSPs reference in field 1.12 of form.wssystem
                    foreach ($point->{'field_wsystems'} as $system) {
                        if (!$system || $system->{'field_deleted'}) {
                            continue;
                        }
                        $servedCommunities = $system->{'field_served_communities'};
                        foreach ($servedCommunities as $subformRecord) {
                            /** @var SubformFormManager $subformManager */
                            $subformManager = $subformRecord->getForm();
                            $provider = $subformRecord->{'field_provider'};
                            if ($sourceRecord->getId() === $provider->getId()) {
                                $subformRecord->set('field_provider', ['value' => $clonRecord->getId(), 'form' => 'form.wsprovider']);
                                $subformManager->update($subformRecord, true);
                            }
                        }
                    }
                    // Update WSPs in Point
                    $pointWsps[] = $clonRecord;
                    foreach ($point->{'field_wsps'} as $wsp) {
                        if (!$wsp || $wsp->{'field_deleted'}) {
                            continue;
                        }
                        if ($wsp->getId() !== $sourceRecord->getId()) {
                            $pointWsps[] = $wsp;
                        }
                    }
                    $point->{'field_wsps'} = $pointWsps;
                    $pointManager->update($point, true);
                }
                break;
        }
    }

    /**
     * Detect surveys on create, update or delete form.wssystem to update Point.
     *
     * @param FormRecord $wssystem
     */
    protected function detectSurveysToUpdatePoint(FormRecord $wssystem): void
    {
        $newData = $wssystem->getCurrentRaw();
        if ('locked' === $newData["field_status"]["value"] || 'validated' === $newData["field_status"]["value"]) {
            return;
        }

        // Get Siasar Point
        /** @var PointFormManager $pointManager */
        $pointManager = $this->formFactory->find('form.point');
        /** @var FormRecord $point */
        $point = current($pointManager->findByWSystem($wssystem->{'id'}));
        if (!$point) {
            return;
        }

        $metaPoint = new SiasarPointWrapper($point, $this->em, $this->sessionService, $this->iriConverter);

        $divisionIds = [];
        $pointCommunities = $point->{'field_communities'};
        foreach ($pointCommunities as $community) {
            $divisionIds[] = $community->{'field_region'}->getId();
        }

        $pointWsps = [];
        $newProviders = [];
        foreach ($point->{'field_wsps'} as $wsp) {
            $pointWsps[$wsp->getId()] = $wsp;
        }

        // Communities served by the Water Supply System.
        $servedCommunities = $wssystem->{'field_served_communities'};
        $prev = [];
        foreach ($servedCommunities as $subformRecord) {
            // Validate if the subform fields are not empty
            if (is_null($subformRecord->{'field_community'}) || is_null($subformRecord->{'field_provider'}) || $subformRecord->{'field_households'} === 0) {
                return;
            }
            // Prevent duplicates (community and provider)
            if (count($prev) > 0) {
                foreach ($prev as $value) {
                    if ($subformRecord->{'field_community'}->getId() === $value['community'] && $subformRecord->{'field_provider'}->getId() === $value['provider']) {
                        throw new \Exception(
                            sprintf(
                                "A record already exists for community '%s' and service provider '%s'",
                                $subformRecord->{'field_community'}->getName(),
                                $subformRecord->{'field_provider'}->{'field_provider_name'},
                            )
                        );
                    }
                }
            }
            $prev[] = [
                'community' => $subformRecord->{'field_community'}->getId(),
                'provider' => $subformRecord->{'field_provider'}->getId(),
            ];

            // If the community doesn't exists in the Siasar Point, add it to the Point
            if (!in_array($subformRecord->{'field_community'}->getId(), $divisionIds)) {
                $communityManager = $this->formFactory->find('form.community');
                /** @var FormRecord $community */
                $community = current($communityManager->findBy(
                    [
                        'field_region' => $subformRecord->{'field_community'}->getId(),
                        'field_deleted' => '0',
                    ],
                    ['field_changed' => 'DESC'],
                ));
                // Without fusion
                if (!$community) {
                    // Point log: New community added.
                    $metaPoint->getLogs()->info($this->t(
                        'Added new community "@name"@code.',
                        [
                            '@name' => $subformRecord->{'field_community'}->getName(),
                            '@code' => ($subformRecord->{'field_community'}->getCode()) ? ' ('.$subformRecord->{'field_community'}->getCode().')' : '',
                        ]
                    ));
                    // Create community inquiry.
                    $communityInquiryId = $communityManager->insert(
                        [
                            'field_country' => $subformRecord->{'field_community'}->getCountry(),
                            'field_region' => $subformRecord->{'field_community'},
                        ]
                    );
                    $divisionIds[] = $subformRecord->{'field_community'}->getId();
                    $pointCommunities[] = $communityManager->find($communityInquiryId);
                }
            }

            // If the WSP doesn't exists in the Siasar Point, add it to the Point
            if (!in_array($subformRecord->{'field_provider'}->getId(), array_keys($pointWsps))) {
                $pointWsps[$subformRecord->{'field_provider'}->getId()] = $subformRecord->{'field_provider'};
                $newProviders[] = $subformRecord->{'field_provider'};
            }
        }
        // Update Point
        $point->{'field_communities'} = $pointCommunities;
        $point->{'field_wsps'} = $this->cleanWSPofPoint($point, $wssystem, $pointWsps);
        $pointManager->update($point);

        // WITH FUSION
        $point = $pointManager->find($point->getId());
        foreach ($point->{'field_communities'} as $community) {
            $divisionIds[] = $community->{'field_region'}->getId();
        }
        $pointsToDelete = [];
        foreach ($servedCommunities as $subformRecord) {
            $point = $pointManager->find($point->getId());
            // If the community doesn't exists in the Siasar Point, add it to the Point
            if (!in_array($subformRecord->{'field_community'}->getId(), $divisionIds)) {
                $communityManager = $this->formFactory->find('form.community');
                /** @var FormRecord $community */
                $community = current($communityManager->findBy(
                    [
                        'field_region' => $subformRecord->{'field_community'}->getId(),
                        'field_deleted' => '0',
                    ],
                    ['field_changed' => 'DESC'],
                ));
                // With fusion
                if ($community) {
                    /** @var FormRecord $pointComm */
                    $pointComm = current($pointManager->findByCommunity($community->{'id'}));

                    if (!$pointComm || $point->getId() === $pointComm->getId()) {
                        continue;
                    }

                    if ('calculating' === $pointComm->{'field_status'} || 'calculated' === $pointComm->{'field_status'}) {
                        $this->cloneInquiries($point, $pointComm);
                        $point->save();
                    } else {
                        $this->mergePoints($point, $pointComm);
                        $point->save();
                        $pointsToDelete[] = $pointComm;
                        // Is the source point in planning?
                        if ('planning' === $pointComm->{'field_status'}) {
                            // Yes, check it to allow save. After that the point will be erased.
                            $pointComm->{'field_chk_to_plan'} = true;
                        }
                        $pointComm->save();
                    }

                    $metaPoint->getLogs()->info($this->t(
                        'Point fusion caused by community "@name"@code.',
                        [
                            '@name' => $community->{'field_region'}->getName(),
                            '@code' => ($community->{'field_region'}->getCode()) ? ' ('.$community->{'field_region'}->getCode().')' : '',
                        ]
                    ));
                }
            }
        }

        if (count($pointsToDelete) > 0) {
            /** @var FormRecord $record */
            foreach ($pointsToDelete as $record) {
                $record->delete();
            }
        }

        $point = $pointManager->find($point->getId());
        foreach ($newProviders as $provider) {
            if ('draft' !== $provider->{'field_status'}) {
                $this->detectFusionAtFinishWsp($provider, $point);
            }
            // Point log: Added wsprovider.
            $metaPoint->getLogs()->info($this->t(
                'Added new provider "@name"@code.',
                [
                    '@name' => $subformRecord->{'field_provider'}->{'field_provider_name'},
                    '@code' => ($subformRecord->{'field_provider'}->{'field_provider_code'}) ? ' ('.$subformRecord->{'field_provider'}->{'field_provider_code'}.')' : '',
                ]
            ));
        }
    }

    /**
     * Clean point of unused WSP (not created in this Point)
     *
     * @param FormRecord $point
     * @param FormRecord $updatedSystem The record to update
     * @param array      $wsps          Array of WSP in this Point
     *
     * @return array
     */
    protected function cleanWSPofPoint(FormRecord $point, FormRecord $updatedSystem, array $wsps): array
    {
        $systemsProviders = [];
        $systemInquiries = $point->{'field_wsystems'};
        foreach ($systemInquiries as $systemInquiry) {
            if ($systemInquiry->getId() === $updatedSystem->getId()) {
                $systemInquiry = $updatedSystem;
            }

            // Field 1.12 of form.wssystem
            $servedCommunities = $systemInquiry->{'field_served_communities'};

            foreach ($servedCommunities as $subformRecord) {
                if (null === $subformRecord) {
                    continue;
                }
                if ($subformRecord->{'field_provider'}) {
                    $systemsProviders[] = $subformRecord->{'field_provider'}->getId();
                }
            }
        }

        $filteredWsps = [];
        foreach ($wsps as $wsp) {
            if (!(!in_array($wsp->getId(), $systemsProviders) && $wsp->{'field_point'}->getId() !== $point->getId())) {
                $filteredWsps[] = $wsp;
            }
        }

        return $filteredWsps;
    }

    /**
     * Merge points (with fusion)
     *
     * @param FormRecord $targetPoint
     * @param FormRecord $sourcePoint
     */
    protected function mergePoints(FormRecord $targetPoint, FormRecord $sourcePoint): void
    {
        $metaPoint = new SiasarPointWrapper($targetPoint, $this->em, $this->sessionService, $this->iriConverter);
        $communities = [];
        $wsps = [];
        $wssystems = [];
        $team = [];

        // Merge communities
        foreach ($targetPoint->{'field_communities'} as $community) {
            $communities[$community->getId()] = $community;
        }
        foreach ($sourcePoint->{'field_communities'} as $communityToMerge) {
            $communities[$communityToMerge->getId()] = $communityToMerge;
        }
        $targetPoint->{'field_communities'} = array_values($communities);
        if (count($sourcePoint->{'field_communities'}) > 0) {
            // Point log: External communities merged.
            $metaPoint->getLogs()->info($this->t(
                'Merged communities.'
            ));
        }
        $sourcePoint->{'field_communities'} = [];

        // Merge WS Providers
        foreach ($targetPoint->{'field_wsps'} as $wsp) {
            $wsps[$wsp->getId()] = $wsp;
        }
        foreach ($sourcePoint->{'field_wsps'} as $wsp) {
            // Update point_reference fieldtype to the new Point
            if (is_null($wsp->{'field_point'}) || $wsp->{'field_point'}->getId() === $sourcePoint->getId()) {
                $wsp->set('field_point', ['value' => $targetPoint->getId(), 'form' => 'form.point']);
                $wsp->save();
            }
            $wsps[$wsp->getId()] = $wsp;
        }
        $targetPoint->{'field_wsps'} = array_values($wsps);
        if (count($sourcePoint->{'field_wsps'}) > 0) {
            // Point log: External wsproviders merged.
            $metaPoint->getLogs()->info($this->t(
                'Merged providers.'
            ));
        }
        $sourcePoint->{'field_wsps'} = [];

        // Merge WS Systems
        foreach ($targetPoint->{'field_wsystems'} as $wssystem) {
            $wssystems[$wssystem->getId()] = $wssystem;
        }
        foreach ($sourcePoint->{'field_wsystems'} as $wssystem) {
            $wssystems[$wssystem->getId()] = $wssystem;
        }
        $targetPoint->{'field_wsystems'} = array_values($wssystems);
        if (count($sourcePoint->{'field_wsystems'}) > 0) {
            // Point log: External wssystems merged.
            $metaPoint->getLogs()->info($this->t(
                'Merged water systems.'
            ));
        }
        $sourcePoint->{'field_wsystems'} = [];

        // Merge Team
        foreach ($targetPoint->{'field_team'} as $user) {
            $team[$user->getId()] = $user;
        }
        foreach ($sourcePoint->{'field_team'} as $user) {
            $team[$user->getId()] = $user;
        }
        $targetPoint->{'field_team'} = array_values($team);
        if (count($sourcePoint->{'field_team'}) > 0) {
            // Point log: Team merged.
            $metaPoint->getLogs()->info($this->t(
                'Merged teams.'
            ));
        }
        $sourcePoint->{'field_team'} = [];
    }

    /**
     * Clone Inquiries for Points with status "calculating" or "calculated"
     *
     * @param FormRecord $targetPoint
     * @param FormRecord $sourcePoint
     */
    protected function cloneInquiries(FormRecord $targetPoint, FormRecord $sourcePoint): void
    {
        $metaPoint = new SiasarPointWrapper($targetPoint, $this->em, $this->sessionService, $this->iriConverter);
        // Clone communities
        $communities = $targetPoint->{'field_communities'};
        foreach ($sourcePoint->{'field_communities'} as $community) {
            /** @var FormRecord $community */
            $communities[] = $community->clone();
        }
        $targetPoint->{'field_communities'} = $communities;
        if (count($sourcePoint->{'field_communities'}) > 0) {
            // Point log: New community cloned and added.
            $metaPoint->getLogs()->info($this->t(
                'Added updated communities.'
            ));
        }

        // Clone WS Systems
        $wssystems = $targetPoint->{'field_wsystems'};
        foreach ($sourcePoint->{'field_wsystems'} as $wssystem) {
            $wssystems[] = $wssystem->clone();
        }
        $targetPoint->{'field_wsystems'} = $wssystems;
        if (count($sourcePoint->{'field_wsystems'}) > 0) {
            // Point log: New wssystems cloned and added.
            $metaPoint->getLogs()->info($this->t(
                'Added updated water systems.'
            ));
        }

        // Merge WS Providers
        $wsps = [];
        foreach ($targetPoint->{'field_wsps'} as $wsp) {
            $wsps[$wsp->getId()] = $wsp;
        }
        foreach ($sourcePoint->{'field_wsps'} as $wsp) {
            // Update point_reference fieldtype to the new Point
            if (is_null($wsp->{'field_point'}) || $wsp->{'field_point'}->getId() === $sourcePoint->getId()) {
                $wsp->set('field_point', ['value' => $targetPoint->getId(), 'form' => 'form.point']);
                // Set 'force' to true to not return the status to draft after update
                $wsp->getForm()->update($wsp, true);
            }
            $wsps[$wsp->getId()] = $wsp;
        }
        $targetPoint->{'field_wsps'} = array_values($wsps);
        if (count($sourcePoint->{'field_wsps'}) > 0) {
            // Point log: External wsproviders merged.
            $metaPoint->getLogs()->info($this->t(
                'Merged providers.'
            ));
        }

        // Merge Team
        $team = [];
        foreach ($targetPoint->{'field_team'} as $user) {
            $team[$user->getId()] = $user;
        }
        foreach ($sourcePoint->{'field_team'} as $user) {
            $team[$user->getId()] = $user;
        }
        $targetPoint->{'field_team'} = array_values($team);
        if (count($sourcePoint->{'field_team'}) > 0) {
            // Point log: Team merged.
            $metaPoint->getLogs()->info($this->t(
                'Merged teams.'
            ));
        }
    }

    /**
     * Check that this record don't go to validated with errors.
     *
     * @param FormRecord $record
     *
     * @return void
     */
    protected function stopValidatingInquiryWithErrors(FormRecord $record): void
    {
        if ('validated' === $record->{'field_status'}) {
            $logRepo = $this->em->getRepository(InquiryFormLog::class);
            $withErrors = $logRepo->findBy(
                [
                    'recordId' => $record->getId(),
                    'levelName' => 'error',
                ]
            );
            if (count($withErrors) > 0) {
                $msgs = [];
                foreach ($withErrors as $error) {
                    $msgs[] = $error->getMessage();
                }
                throw new \Exception(
                    $this->t('You can\'t validate an inquiry with errors: @errors', ['@errors' => implode(', ', $msgs)]),
                    401
                );
            }
        }
    }

    /**
     * After finish an inquiry clear all errors.
     *
     * @param FormRecord $record
     *
     * @return void
     *
     * @deprecated After finish, we need persist warnings.
     */
    protected function afterFinishInquiryClearErrors(FormRecord $record): void
    {
        return;
//        if ('finished' === $record->{'field_status'}) {
//            $logRepo = $this->em->getRepository(InquiryFormLog::class);
//            $logs = $logRepo->findBy(
//                [
//                    'recordId' => $record->getId(),
//                ]
//            );
//            foreach ($logs as $log) {
//                $logRepo->remove($log);
//            }
//            $this->em->flush();
//        }
    }

    /**
     * Check point status to allow update inquiries
     *
     * @param FormRecord $record
     */
    protected function checkCanUpdateInquiriesOnPoint(FormRecord $record): void
    {
        $form = $record->getForm();
        $newData = $record->getCurrentRaw();

        if ('locked' === $newData["field_status"]["value"] || 'validated' === $newData["field_status"]["value"]) {
            return;
        }
        /** @var PointFormManager $pointManager */
        $pointManager = $this->formFactory->find('form.point');
        $checkStatusPoint = true;
        // Get Point by community, system or provider record
        switch ($form->getId()) {
            case "form.community":
                $points = $pointManager->findByCommunity($record->getId());
                break;
            case "form.wssystem":
                $points = $pointManager->findByWSystem($record->getId());
                break;
            case "form.wsprovider":
                $points = $pointManager->findByWsp($record->getId(), false);
                $checkStatusPoint = false;
                break;
        }
        if (0 === count($points)) {
            return;
        }
//        // Check point status
//        // Does changed record content only of status?
//        $allowChange = true;
//        foreach ($record->getModifiedFields() as $field) {
//            if (!in_array($field, ['field_status', 'field_editors', 'field_editors_update'])) {
//                if ('field_deleted' === $field && $newData["field_deleted"]["value"] === $record->getCurrentRaw()["field_deleted"]["value"]) {
//                    // Field deleted is used to force a record update, but if is with the same value we must allow
//                    // update it.
//                    continue;
//                }
//                $allowChange = false;
//                break;
//            }
//        }

        foreach ($points as $point) {
            if ($checkStatusPoint) {
                // Don't allow modify content.
                $a = $point->{'field_status'};
                if (!in_array($point->{'field_status'}, ['planning', 'digitizing', 'checking', 'reviewing']) /*&& !$allowChange*/) {
                    throw new \Exception(
                        $this->t(
                            'You can\'t edit inquiries in this point. It\'s on status "@status"',
                            ['@status' => $point->{'field_status'}]
                        ),
                        401
                    );
                }
            } elseif (!$checkStatusPoint && !in_array($point->{'field_status'}, ['planning', 'digitizing', 'checking', 'reviewing'])) {
                // WSP may have points in the final status, so they shouldn't be updated.
                continue;
            } else {
                // Update field_changed of Point if the inquiry will be updated.
                $point->{'field_changed'} = new \DateTime();
                $point->save();
            }
        }
    }

    /**
     * Check point fusion by WSP
     *
     * @param FormRecord      $wsp
     * @param FormRecord|null $currentPoint
     */
    protected function detectFusionAtFinishWsp(FormRecord $wsp, FormRecord $currentPoint = null): void
    {
        // Without fusion
        if ('3' === $wsp->{'field_provider_type'} || '4' === $wsp->{'field_provider_type'}) {
            return;
        }

        /** @var PointFormManager $pointManager */
        $pointManager = $this->formFactory->find('form.point');
        if (!$currentPoint) {
            $currentPoint = current($pointManager->findByWsp($wsp->getId()));
        }
        $metaPoint = new SiasarPointWrapper($currentPoint, $this->em, $this->sessionService, $this->iriConverter);

        // With fusion
        if (is_null($wsp->{'field_point'}) || $wsp->{'field_point'}->getId() !== $currentPoint->getId()) {
            // Update 'field_point' to the current point.
            $wsp->set('field_point', ['value' => $currentPoint->getId(), 'form' => 'form.point']);
            // Set 'force' to true to not return the status to draft after update
            $wsp->getForm()->update($wsp, true);
        }

        $divisionIds = [];
        foreach ($currentPoint->{'field_communities'} as $community) {
            $divisionIds[] = $community->{'field_region'}->getId();
        }

        $pointsToMerge = [];
        $pointsWithWspRef = $pointManager->findByWspRef($wsp->{'field_ulid_reference'}, false);
        foreach ($pointsWithWspRef as $key => &$pointWithWspRef) {
            $point = $pointManager->getLastVersion($pointWithWspRef);
            $pointDivisions = [];
            if ($currentPoint->getId() === $point->getId()) {
                unset($pointsWithWspRef[$key]);
                continue;
            }

            foreach ($point->{'field_communities'} as $community) {
                if (in_array($community->{'field_region'}->getId(), $divisionIds)) {
                    unset($pointsWithWspRef[$key]);
                    $pointDivisions = [];
                    break;
                }
                $pointDivisions[] = $community->{'field_region'}->getId();
            }
            if (count($pointDivisions) > 0) {
                $divisionIds = array_merge($divisionIds, $pointDivisions);
                $pointsToMerge[$point->getId()] = $point;
            }
        }

        foreach ($pointsToMerge as $key => $sourcePoint) {
            $currentPoint = $pointManager->find($currentPoint->getId());
            if ('calculating' === $sourcePoint->{'field_status'} || 'calculated' === $sourcePoint->{'field_status'}) {
                $this->cloneInquiries($currentPoint, $sourcePoint);
                $currentPoint->save();
            } else {
                $this->mergePoints($currentPoint, $sourcePoint);
                $currentPoint->save();
                // Is the source point in planning?
                if ('planning' === $sourcePoint->{'field_status'}) {
                    // Yes, check it to allow save. After that the point will be erased.
                    $sourcePoint->{'field_chk_to_plan'} = true;
                }
                $sourcePoint->save();
                $sourcePoint->delete();
            }
        }

        if (count($pointsToMerge) > 0) {
            $metaPoint->getLogs()->info($this->t(
                'Point fusion caused by water service provider "@name"@code.',
                [
                    '@name' => $wsp->{'field_provider_name'},
                    '@code' => ($wsp->{'field_provider_code'}) ? ' ('.$wsp->{'field_provider_code'}.')' : '',
                ]
            ));
        }
    }

    /**
     * Recalculate all points where the WSP is referenced.
     *
     * @param FormRecord $record WSP Record
     */
    protected function recalculatePoints(FormRecord $record)
    {
        // WSP must be previously validated to recalculate points.
        $originalData = $record->getOriginal();
        if (!$originalData['field_validated']['value']) {
            return;
        }
        /** @var PointFormManager $pointManager */
        $pointManager = $this->formFactory->find('form.point');
        $points = $pointManager->findByWsp($record->getId(), false);

        foreach ($points as $point) {
            if ('calculated' === $point->{'field_status'}) {
                $pointManager->recalculate($point);
            }
        }
    }

    /**
     * Check can delete inquiries in a point (communities, systems and providers)
     *
     * @param FormRecord $record
     * @param FormRecord $point
     */
    protected function checkCanDeleteInquiryOnPoint(FormRecord $record, FormRecord $point)
    {
        $allowedStatus = ['draft'];

        switch ($record->getForm()->getId()) {
            case 'form.community':
                $allowedStatus[] = 'finished';
                // Check if community is linked to any system (field 1.12).
                if ('planning' !== $point->{'field_status'}) {
                    foreach ($point->{'field_wsystems'} as $systemRecord) {
                        foreach ($systemRecord->{'field_served_communities'} as $subformRecord) {
                            if ($record->{'field_region'}->getId() === $subformRecord->{'field_community'}->getId()) {
                                throw new \Exception(
                                    sprintf(
                                        "Community '%s' is linked to a system.",
                                        $record->{'field_region'}->getName()
                                    )
                                );
                            }
                        }
                    }
                }
                break;
            case 'form.wssystem':
                // Check if the record already has associated communities (field 1.12)
                if (count($record->{'field_served_communities'}) > 0) {
                    throw new \Exception(
                        sprintf(
                            "The record '%s' cannot be deleted. It already has associated communities.",
                            $record->getId()
                        )
                    );
                }
                break;
            case 'form.wsprovider':
                // Check if the WSP is linked to any system
                if ('planning' !== $point->{'field_status'}) {
                    foreach ($point->{'field_wsystems'} as $systemRecord) {
                        foreach ($systemRecord->{'field_served_communities'} as $subformRecord) {
                            if ($record->getId() === $subformRecord->{'field_provider'}->getId()) {
                                throw new \Exception(
                                    $this->t(
                                        'WS Provider "@fid" is linked to system "@sid"',
                                        [
                                            '@fid' => $record->getId(),
                                            '@sid' => $systemRecord->getId(),
                                        ],
                                    )
                                );
                            }
                        }
                    }
                }
                break;
        }

        // Check the status before delete record.
        if (!in_array($record->{'field_status'}, $allowedStatus)) {
            throw new \Exception(
                sprintf(
                    "The record '%s' couldn't be removed. The current status is '%s'.",
                    $record->getId(),
                    $record->{'field_status'},
                )
            );
        }
    }
}
