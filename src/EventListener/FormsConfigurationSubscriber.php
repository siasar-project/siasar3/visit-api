<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Command\Event\ConfigurationBaseEvent;
use App\Forms\FormDatabaseUpdater;
use App\Forms\FormFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Forms configuration change listener.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormsConfigurationSubscriber implements EventSubscriberInterface
{

    protected ?FormFactory $formFactory;

    /**
     * FormsConfigurationSubscriber constructor.
     *
     * @param FormFactory|null $formFactory
     */
    public function __construct(FormFactory $formFactory = null)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * Get subscribed events.
     *
     * @return \string[][]
     */
    public static function getSubscribedEvents():array
    {
        return [
            // 'configuration.pre.create' => ['onConfigurationPreCreate'],
            'configuration.pre.update' => ['onConfigurationPreUpdate'],
            'configuration.pre.delete' => ['onConfigurationPreDelete'],
            'configuration.post.create' => ['onConfigurationPostCreate'],
            'configuration.post.update' => ['onConfigurationPostUpdate'],
            // 'configuration.post.delete' => ['onConfigurationPostDelete'],
        ];
    }

    /**
     * On configuration previous create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreCreate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isForm($event->getId())) {
            return;
        }
    }

    /**
     * On configuration post create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostCreate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isForm($event->getId())) {
            return;
        }
        if ($form = $this->formFactory->find($event->getId())) {
            $form->install();
            $this->note($event, sprintf('Form "%s" installed.', $event->getId()));
        }
    }

    /**
     * On configuration previous update.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreUpdate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isForm($event->getId())) {
            return;
        }
        if ($form = $this->formFactory->find($event->getId())) {
            $old = $event->getOld();
            $new = $event->getNew();

            if ($old['type'] !== $new['type']) {
                $event->getIo()->caution(
                    sprintf('Form manager will be changed, from %s to %s.', $old['type'], $new['type'])
                );
                if ($event->getInput()->getOption('no-interaction')) {
                    $event->getIo()->text('Continue? [y/N]');
                    $event->getIo()->info('Response by options: y');
                    $continue = true;
                } else {
                    $continue = $event->getIo()->confirm('Continue?', false);
                }
                if (!$continue) {
                    throw new \Exception('User cancel.');
                }
            }

            if (isset($old['title']) && isset($new['title'])) {
                if ($old['title'] !== $new['title']) {
                    $form->setTitle($new['title']);
                }
            } elseif (isset($new['title'])) {
                $form->setTitle($new['title']);
            }

            if (isset($old['description']) && isset($new['description'])) {
                if ($old['description'] !== $new['description']) {
                    $form->setDescription($new['description']);
                }
            } elseif (isset($new['description'])) {
                $form->setTitle($new['description']);
            }

            if (isset($new['meta'])) {
                if (!isset($old['meta'])) {
                    $old['meta'] = [];
                }
                if (serialize($old['meta']) !== serialize($new['meta'])) {
                    $form->setMeta($new['meta']);
                }
            }

            unset($old);
            unset($new);

            if ($form->isInstalled()) {
                // Posible field changes: new field, field removed, field deprecated.
                $updater = new FormDatabaseUpdater($this->formFactory, $event->getOld(), $event->getNew());
                $actions = $updater->updateSchema();
                // Display to user.
                if (count($actions['added']) > 0) {
                    $event->getIo()->info(sprintf('New fields: %s', implode(', ', $actions['added'])));
                }
                if (count($actions['removed']) > 0) {
                    $event->getIo()->info(sprintf('Removed fields: %s', implode(', ', $actions['removed'])));
                }
                if (count($actions['updated']) > 0) {
                    $event->getIo()->info(sprintf('Indexation updated fields: %s', implode(', ', $actions['updated'])));
                }
                if (count($actions['unchanged']) > 0) {
                    // $event->getIo()->info(sprintf('Unchanged or not database changed fields: %s', implode(', ', $actions['unchanged'])));
                }
            }
        }
    }

    /**
     * On configuration post update.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostUpdate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isForm($event->getId())) {
            return;
        }
        if ($form = $this->formFactory->find($event->getId())) {
            // Do nothing, the form was updated.
            if (!$form->isInstalled()) {
                $form->install();
            }
        }
    }

    /**
     * On configuration previous delete.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreDelete(ConfigurationBaseEvent $event): void
    {
        if (!$this->isForm($event->getId())) {
            return;
        }
        if ($form = $this->formFactory->find($event->getId())) {
            $form->uninstall();
            $this->note($event, sprintf('Form "%s" uninstalled.', $event->getId()));
        }
    }

    /**
     * On configuration post create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostDelete(ConfigurationBaseEvent $event): void
    {
        if (!$this->isForm($event->getId())) {
            return;
        }
    }

    /**
     * Display a note.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function note(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->note($message);
        }
    }

    /**
     * Display a comment.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function comment(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->comment($message);
        }
    }

    /**
     * Is this a form configuration?
     *
     * @param string $id Configuration ID.
     *
     * @return bool
     */
    protected function isForm(string $id): bool
    {
        return (substr($id, 0, 5) === 'form.');
    }
}
