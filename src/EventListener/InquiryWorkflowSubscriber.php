<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\CountryRepository;
use App\RepositorySecured\HouseholdProcessRepositorySecured;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Tools\InquiryConditionalResolver;
use App\Tools\InquiryFinishChecks;
use App\Traits\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

/**
 * Inquiry form workflow event subscriber.
 */
class InquiryWorkflowSubscriber implements EventSubscriberInterface
{
    use StringTranslationTrait;

    protected AccessManager $accessManager;
    protected SessionService $sessionService;
    protected CountryRepository $countryRepository;
    protected HouseholdProcessRepositorySecured $householdProcessRepo;

    /**
     * InquiryWorkflowSubscriber constructor.
     *
     * @param AccessManager                     $accessManager
     * @param SessionService                    $sessionService
     * @param CountryRepository                 $countryRepository
     * @param HouseholdProcessRepositorySecured $householdProcessRepo
     */
    public function __construct(AccessManager $accessManager, SessionService $sessionService, CountryRepository $countryRepository, HouseholdProcessRepositorySecured $householdProcessRepo)
    {
        $this->accessManager = $accessManager;
        $this->sessionService = $sessionService;
        $this->countryRepository = $countryRepository;
        $this->householdProcessRepo = $householdProcessRepo;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.inquiring.entered' => 'onTransitionGuard',
        ];
    }

    /**
     * Throw exception if not complete guard ok.
     *
     * @param Event $event
     */
    public function onTransitionGuard(Event $event): void
    {
        $context = $event->getContext();
        if (isset($context['validation_stage']) && 'workflow_apply' === $context['validation_stage']) {
            $user = $this->sessionService->getUser();
            $permission = sprintf(
                'can do %s transition in workflow %s',
                $event->getTransition()->getName(),
                $event->getWorkflowName()
            );
            if (!$this->accessManager->isSystemPermission($permission)) {
                return;
            }
            $countries = [];
            $division = null;
            /** @var FieldTypeInterface $subject */
            $subject = $event->getSubject();
            if ($subject && $subject->getFormRecord() && $subject->getFormRecord()->getForm() instanceof InquiryFormManager) {
                // Get country and division to use in context.
                $countries = [$subject->getFormRecord()->{'field_country'}];
                $division = $subject->getFormRecord()->{'field_region'};
            }
            if (!$user || !$this->accessManager->hasPermission($user, $permission, $countries, $division)) {
                throw new \Exception(
                    $this->t(
                        'Transition "@transition" in workflow "@workflow" require "@permission" permission.',
                        [
                            '@transition' => $this->t($event->getTransition()->getName()),
                            '@workflow' => $this->t($event->getWorkflowName()),
                            '@permission' => $permission,
                        ]
                    )
                );
            }
            if ($subject && $subject->getFormRecord()) {
                $form = $subject->getFormRecord()->getForm();
                switch ($event->getTransition()->getName()) {
                    // From 'draft' to 'finished'
                    case 'finish':
                        if ($subject->getFormRecord()->getForm() instanceof InquiryFormManager) {
                            $this->cleanFormField($event, $form, $subject->getFormRecord());
                            // Execute validation.
                            $validator = new InquiryFinishChecks();
                            $valid = $validator->checkInquiry($subject->getFormRecord());
                            if (!$valid) {
                                // Reopen community household process if it has one, and it's open.
                                if ('form.community' === $subject->getFormRecord()->getForm()->getId()) {
                                    /** @var HouseholdProcess $process */
                                    $process = $subject->getFormRecord()->{'field_household_process'};
                                    if ($process) {
                                        $process->setOpen(true);
                                        $this->householdProcessRepo->saveNow($process);
                                    }
                                }
                                // Don't throw exception to save inquirylogs during transaction
                                $event->stopPropagation();
                            }
                        }
                        break;
                }
            }
        }
    }

    /**
     * Process each field in this form, and sub forms, to clear invisible value.
     *
     * @param Event                $event
     * @param FormManagerInterface $form
     * @param FormRecord           $record
     * @param string               $parentField The field name from a possible parent form.
     *
     * @throws \ReflectionException
     */
    protected function cleanFormField(Event $event, FormManagerInterface $form, FormRecord $record, string $parentField = ''): void
    {
        switch ($form->getType()) {
            case InquiryFormManager::class:
            case SubformFormManager::class:
                $fields = $form->getFields();
                $conditionalValidator = new InquiryConditionalResolver($record);
                // Validate content.
                /** @var FieldTypeInterface $field */
                foreach ($fields as $field) {
                    if ($field->isInternal()) {
                        continue;
                    }
                    // Clear not visible fields.
                    // Meta conditional: visible.
                    if (!$conditionalValidator->isVisible($field->getId(), $parentField)) {
                        $this->cleanFieldValue($record, $field);
                    }
                    switch ($field->getFieldType()) {
                        case 'select':
                            // Validate conditional option visibility.
                            // Meta conditional: option.
                            if ($field->isMultivalued()) {
                                $values = $record->{$field->getId()};
                                if ($values && count($values) > 0) {
                                    foreach ($values as $value) {
                                        if (!$conditionalValidator->isVisibleOption($field->getId(), $value, $parentField)) {
                                            throw new \Exception(
                                                $this->t(
                                                    "[@form->@field] The selected option, @option, it's not visible.",
                                                    [
                                                        '@form' => $form->getId(),
                                                        '@field' => $field->getId(),
                                                        '@option' => $record->{$field->getId()},
                                                    ]
                                                )
                                            );
                                        }
                                    }
                                }
                            } else {
                                if (!$conditionalValidator->isVisibleOption($field->getId(), $record->get($field->getId()), $parentField)) {
                                    throw new \Exception(
                                        $this->t(
                                            "[@form->@field] The selected option, @option, it's not visible.",
                                            [
                                                '@form' => $form->getId(),
                                                '@field' => $field->getId(),
                                                '@option' => $record->{$field->getId()},
                                            ]
                                        )
                                    );
                                }
                            }
                            break;
                        case 'subform_reference':
                            // Clear subform records.
                            if ($field->isMultivalued()) {
                                /** @var FormRecord[] $values */
                                $values = $record->{$field->getId()};
                                if (count($values) > 0) {
                                    $subform = $values[0]->getForm();
                                    foreach ($values as &$value) {
                                        $this->cleanFormField($event, $subform, $value, $field->getId());
                                        $subform->update($value, true);
                                    }
                                }
                            } else {
                                $value = $record->{$field->getId()};
                                if ($value) {
                                    $subform = $value->getForm();
                                    $this->cleanFormField($event, $subform, $value, $field->getId());
                                    $subform->update($value, true);
                                }
                            }
                            break;
                    }
                }

                // Filter by field level.
                /** @var Country $country */
                $country = $record->{'field_country'};
                if (!$country) {
                    throw new \Exception(
                        $this->t(
                            'Transition "@transition" in workflow "@workflow" require a valid country in field_country.',
                            [
                                '@transition' => $this->t($event->getTransition()->getName()),
                                '@workflow' => $this->t($event->getWorkflowName()),
                            ]
                        )
                    );
                }
                $this->filterByLevel($record, $country);
                break;
        }
    }

    /**
     * Filter record content by level.
     *
     * @param FormRecord $record
     * @param Country    $country
     *
     * @throws \ReflectionException
     */
    protected function filterByLevel(FormRecord $record, Country $country): void
    {
        $form = $record->getForm();
        $meta = $form->getMeta();
        $allowSDG = (isset($meta['allow_sdg'])) ? $meta['allow_sdg'] : false;
        $fields = $form->getFields();
        $formCountryLevels = $country->getFormLevel();
        $countryAllowSDG = isset($formCountryLevels[$form->getId()]["sdg"]) ? $formCountryLevels[$form->getId()]["sdg"] : false;
        /**
         * @var FieldTypeInterface $field
         */
        foreach ($fields as $field) {
            $meta = $field->getMeta();
            if (isset($meta['level'])) {
                if ($form->getType() === SubformFormManager::class) {
                    /** @var SubformFormManager $form */
                    $parentRecord = $form->getParentRecord($record);
                    if (!$parentRecord) {
                        continue;
                    } else {
                        $cLevel = isset($formCountryLevels[$parentRecord->getForm()->getId()]['level']) ? $formCountryLevels[$parentRecord->getForm()->getId()]['level'] : 1;
                    }
                } else {
                    $cLevel = isset($formCountryLevels[$form->getId()]['level']) ? $formCountryLevels[$form->getId()]['level'] : 1;
                }
                if ($meta['level'] > $cLevel) {
                    // This field is out the country level.
                    $this->cleanFieldValue($record, $field);
                }
            }
            if ((!$allowSDG || !$countryAllowSDG) && isset($meta['sdg']) && $meta['sdg']) {
                // This field is SDG but the country not.
                $this->cleanFieldValue($record, $field);
            }
        }
    }

    /**
     * Clean a field value.
     *
     * @param FormRecord         $record
     * @param FieldTypeInterface $field
     *
     * @return void
     */
    protected function cleanFieldValue(FormRecord $record, FieldTypeInterface $field): void
    {
        if ($field->isMultivalued()) {
            $record->{$field->getId()} = [];
        } else {
            // The field it's not visible, must be empty.
            $emptyValue = $field->getDefaultValue();
            if (count($emptyValue) === 1) {
                $emptyValue = $emptyValue[array_keys($emptyValue)[0]];
            }
            $record->{$field->getId()} = $emptyValue;
        }
    }
}
