<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Symfony\Routing\IriConverter;
use App\Entity\User;
use App\Traits\StringTranslationTrait;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Extend JWT payload.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class JWTCreatedListener
{
    use StringTranslationTrait;

    protected RequestStack $requestStack;
    protected IriConverterInterface $iriConverter;

    /**
     * Init instance.
     *
     * @param RequestStack          $requestStack Request stack.
     * @param IriConverterInterface $iriConverter
     */
    public function __construct(RequestStack $requestStack, IriConverterInterface $iriConverter)
    {
        $this->requestStack = $requestStack;
        $this->iriConverter = $iriConverter;
    }

    /**
     * Extend JWT payload.
     *
     * @param JWTCreatedEvent $event Event object.
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $request = $this->requestStack->getCurrentRequest();

        /** @var User $user */
        $user = $event->getUser();

        $payload       = $event->getData();
        // Add request IP.
        $payload['ip'] = ($request) ? $request->getClientIp() : (isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '');
        // Add current user ID.
        $payload['id'] = $user->getId();
        $payload['country'] = [
            'code' => $user->getCountry()?->getId(),
        ];
        if ($user->getLanguage()) {
            $payload['language_id'] = $user->getLanguage()->getIsoCode();
        } else {
            $payload['language_id'] = 'en';
        }
        if ($user->getCountry()) {
            foreach ($user->getCountry()->getDivisionTypes() as $divisionType) {
                $payload['country']['divisionTypes'][] = [
                    "id" => $divisionType->getId(),
                    "name" => self::t(
                        $divisionType->getName(),
                        [],
                        [
                            'context' => 'Administrative division type',
                        ]
                    ),
                    "real_name" => $divisionType->getName(),
                    "level" => $divisionType->getLevel(),
                ];
            }
        }
        $payload['gravatar'] = $user->getGravatar();
        $payload['timezone'] = $user->getTimezone();

        $event->setData($payload);
    }
}
