<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Annotations\IsCountryParametric;
use App\Forms\FieldTypes\Types\EntityReferenceFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ReflectionClass;
use Symfony\Component\Uid\Ulid;

/**
 * Parametric LifeCycle listener
 */
class ParametricListener
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected FormFactory $formFactory;
    protected Reader $annotationReader;
    protected ?SessionService $sessionService;

    /**
     * constructor
     *
     * @param FormFactory $formFactory
     * @param Reader      $annotationReader
     */
    public function __construct(FormFactory $formFactory, Reader $annotationReader)
    {
        $this->formFactory = $formFactory;
        $this->annotationReader = $annotationReader;
        $this->sessionService = $this->getContainerInstance()->get('session_service');
    }

    /**
     * pre remove check
     *
     * @param LifecycleEventArgs $event the event that was dispatch
     */
    public function preRemove(LifecycleEventArgs $event):void
    {
        $object = $event->getObject();
        $annotations = $this->annotationReader->getClassAnnotations(new ReflectionClass($object::class));
        $isParametric = false;

        foreach ($annotations as $annotation) {
            if ($annotation instanceof IsCountryParametric) {
                $isParametric = true;
                break;
            }
        }

        if (!$isParametric) {
            return;
        }

        // If the event is executed from CLI, we shouldn't check if the record is in use in any form
        // because it doesn't have a user session.
        if (PHP_SAPI !== 'cli') {
            // Check if the object is used in forms
            // Note: This functionality is also implemented in src/EventListener/FileListener.php
            $forms = $this->formFactory->findAll();

            /** @var FormManagerInterface $form */
            foreach ($forms as $form) {
                if (!$form->isInstalled()) {
                    continue;
                }

                $fields = $form->getFields();

                foreach ($fields as $field) {
                    if ($field instanceof EntityReferenceFieldType) {
                        $fieldProperties = $field->getProperties();

                        if ($fieldProperties['class'] === $object::class) {
                            $ulid = new Ulid($object->getId());
                            $record = $form->findBy([$field->getId() => $ulid->toBinary()], [], 1);

                            if (count($record) > 0) {
                                throw new \Exception(
                                    $this->t(
                                        '@object couldn\'t be removed. It\'s used in @formId.',
                                        [
                                            '@object' => $object::class,
                                            '@formId' => $form->getId(),
                                        ]
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}
