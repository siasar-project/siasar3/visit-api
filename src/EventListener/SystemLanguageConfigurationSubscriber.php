<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Command\Event\ConfigurationBaseEvent;
use App\Entity\Language;
use App\Entity\LocaleTarget;
use App\Entity\User;
use App\Forms\FormDatabaseUpdater;
use App\Forms\FormFactory;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Forms configuration change listener.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class SystemLanguageConfigurationSubscriber implements EventSubscriberInterface
{

    protected ?Registry $entityManager;

    /**
     * FormsConfigurationSubscriber constructor.
     *
     * @param ?Registry $entityManager Entity manager.
     */
    public function __construct(Registry $entityManager = null)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get subscribed events.
     *
     * @return \string[][]
     */
    public static function getSubscribedEvents():array
    {
        return [
            //'configuration.pre.create' => ['onConfigurationPreCreate'],
            'configuration.pre.update' => ['onConfigurationPreUpdate'],
            //'configuration.pre.delete' => ['onConfigurationPreDelete'],
            'configuration.post.create' => ['onConfigurationPostCreate'],
            //'configuration.post.update' => ['onConfigurationPostUpdate'],
            // 'configuration.post.delete' => ['onConfigurationPostDelete'],
        ];
    }

    /**
     * On configuration previous create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreCreate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isSystemLanguages($event->getId())) {
            return;
        }
    }

    /**
     * On configuration post create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostCreate(ConfigurationBaseEvent $event): void
    {
        // Create this configuration allow do the code conversion from old ISO ID model
        // to the new inherits model.
        if (!$this->isSystemLanguages($event->getId())) {
            return;
        }
        // Clean users language.
        $userRepo = $this->entityManager->getRepository(User::class);
        $users = $userRepo->findAll();
        /** @var User $user */
        foreach ($users as $user) {
            $user->setLanguage(null);
            $userRepo->save($user);
        }
        $this->entityManager->getManager()->flush();
        // Clean LocaleTarget entities.
        $localeTargetRepo = $this->entityManager->getRepository(LocaleTarget::class);
        $targets = $localeTargetRepo->findAll();
        foreach ($targets as $target) {
            $localeTargetRepo->remove($target);
        }
        $this->entityManager->getManager()->flush();
        // Clean language entities.
        $languageRepo = $this->entityManager->getRepository(Language::class);
        $languages = $languageRepo->findAll();
        foreach ($languages as $language) {
            $languageRepo->remove($language);
        }
        $this->entityManager->getManager()->flush();
        // Create each language.
        $newLanguages = $event->getNew();
        $defaultUserLang = null;
        foreach ($newLanguages as $code => $newLanguage) {
            $lang = new Language($code);
            $lang->setIsoCode($newLanguage['iso_code']);
            $lang->setName($newLanguage['name']);
            $lang->setLtr($newLanguage['ltr']);
            $lang->setPluralNumber($newLanguage['plural_number']);
            $lang->setPluralFormula($newLanguage['plural_formula']);
            $lang->setTranslatable($newLanguage['translatable']);
            $languageRepo->save($lang);
            if ('ENGL' === $code) {
                $defaultUserLang = $lang;
            }
        }
        $this->entityManager->getManager()->flush();
        // Set users default language.
        /** @var User $user */
        foreach ($users as $user) {
            $user->setLanguage($defaultUserLang);
            $userRepo->save($user);
        }
        $this->entityManager->getManager()->flush();
    }

    /**
     * On configuration previous update.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreUpdate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isSystemLanguages($event->getId())) {
            return;
        }
        $languageRepo = $this->entityManager->getRepository(Language::class);
        // Detect actions.
        $oldKeys = array_keys($event->getOld());
        $newKeys = array_keys($event->getNew());
        // New keys.
        $added = array_diff($newKeys, $oldKeys);
        // Removed keys.
        $removed = array_diff($oldKeys, $newKeys, $added);
        // Old keys that aren't added or removed.
        $old = array_diff($oldKeys, $added, $removed);
        // Update.
        if (count($old) > 0) {
            $event->getIo()->writeln('Updating languages...');
            foreach ($old as $key) {
                $def = $event->getNew()[$key];
                $lang = $languageRepo->find($key);
                $lang->setIsoCode($def['iso_code']);
                $lang->setName($def['name']);
                $lang->setLtr($def['ltr']);
                $lang->setPluralNumber($def['plural_number']);
                $lang->setPluralFormula($def['plural_formula']);
                $languageRepo->save($lang);
            }
            $this->entityManager->getManager()->flush();
            $event->getIo()->writeln(sprintf('Updated %s new languages', count($old)));
        }
        // Add new.
        if (count($added) > 0) {
            $event->getIo()->writeln('Adding languages...');
            foreach ($added as $key) {
                $def = $event->getNew()[$key];
                $lang = new Language($key);
                $lang->setIsoCode($def['iso_code']);
                $lang->setName($def['name']);
                $lang->setLtr($def['ltr']);
                $lang->setPluralNumber($def['plural_number']);
                $lang->setPluralFormula($def['plural_formula']);
                $lang->setTranslatable($def['translatable']);
                $languageRepo->save($lang);
            }
            $this->entityManager->getManager()->flush();
            $event->getIo()->writeln(sprintf('Added %s new languages', count($added)));
        }
        // Remove keys.
        if (count($removed) > 0) {
            $event->getIo()->writeln('Removing languages...');
            foreach ($removed as $key) {
                $lang = $languageRepo->find($key);
                if ($lang) {
                    $languageRepo->remove($lang);
                }
            }
            $this->entityManager->getManager()->flush();
            $event->getIo()->writeln(sprintf('Removed %s new languages', count($removed)));
        }
    }

    /**
     * On configuration post update.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostUpdate(ConfigurationBaseEvent $event): void
    {
        if (!$this->isSystemLanguages($event->getId())) {
            return;
        }
    }

    /**
     * On configuration previous delete.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPreDelete(ConfigurationBaseEvent $event): void
    {
        if (!$this->isSystemLanguages($event->getId())) {
            return;
        }
    }

    /**
     * On configuration post create.
     *
     * @param ConfigurationBaseEvent $event The event information.
     */
    public function onConfigurationPostDelete(ConfigurationBaseEvent $event): void
    {
        if (!$this->isSystemLanguages($event->getId())) {
            return;
        }
    }

    /**
     * Display a note.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function note(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->note($message);
        }
    }

    /**
     * Display a comment.
     *
     * @param ConfigurationBaseEvent $event   Event information.
     * @param string                 $message Message to display.
     */
    protected function comment(ConfigurationBaseEvent $event, string $message): void
    {
        if ($event->getIo()) {
            $event->getIo()->comment($message);
        }
    }

    /**
     * Is this a form configuration?
     *
     * @param string $id Configuration ID.
     *
     * @return bool
     */
    protected function isSystemLanguages(string $id): bool
    {
        return ('system.languages' === $id);
    }
}
