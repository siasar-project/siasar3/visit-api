<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener;

use App\Entity\User;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * User LifeCycle listener
 */
class UserListener
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected AccessManager $accessManager;
    protected SessionService $sessionService;
    protected ?Registry $entityManager;

    /**
     * constructor
     *
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        $this->accessManager = $accessManager;
        $this->entityManager = $this->getContainerInstance()->get('doctrine');
        $this->sessionService = $this->getContainerInstance()->get('session_service');
    }

    /**
     * preUpdate user entity
     *
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event): void
    {
        /** @var User $currentUser */
        $currentUser = $this->sessionService->getUser();
        /** @var User $userToUpdate */
        $userToUpdate = $event->getEntity();

        if ($event->hasChangedField('country')) {
            if ($currentUser->getId() !== $userToUpdate->getId()) {
                throw new \Exception(
                    $this->t("You can't update the country of other users.")
                );
            }

            if (!$this->accessManager->hasPermission($currentUser, 'all countries')) {
                throw new \Exception(
                    $this->t('To update own Country requires the permission "all countries".')
                );
            }
        }
    }
}
