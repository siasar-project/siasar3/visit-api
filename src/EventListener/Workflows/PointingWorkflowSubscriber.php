<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\EventListener\Workflows;

use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormRecord;
use App\Forms\PointFormManager;
use App\Repository\CountryRepository;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Traits\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

/**
 * Point form workflow event subscriber.
 */
class PointingWorkflowSubscriber implements EventSubscriberInterface
{
    use StringTranslationTrait;

    protected AccessManager $accessManager;
    protected SessionService $sessionService;
    protected CountryRepository $countryRepository;

    /**
     * PointingWorkflowSubscriber constructor.
     *
     * @param AccessManager     $accessManager
     * @param SessionService    $sessionService
     * @param CountryRepository $countryRepository
     */
    public function __construct(AccessManager $accessManager, SessionService $sessionService, CountryRepository $countryRepository)
    {
        $this->accessManager = $accessManager;
        $this->sessionService = $sessionService;
        $this->countryRepository = $countryRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.pointing.entered' => 'onTransitionGuard',
        ];
    }

    /**
     * Throw exception if not complete guard ok.
     *
     * @param Event $event
     */
    public function onTransitionGuard(Event $event): void
    {
        $context = $event->getContext();
        if (isset($context['validation_stage']) && 'workflow_apply' === $context['validation_stage']) {
            $user = $this->sessionService->getUser();
            $permission = sprintf(
                'can do %s transition in workflow %s',
                $event->getTransition()->getName(),
                $event->getWorkflowName()
            );
            if (!$this->accessManager->isSystemPermission($permission)) {
                return;
            }
        }
        $countries = [];
        /** @var FieldTypeInterface $subject */
        $subject = $event->getSubject();
        if ($subject && $subject->getFormRecord() && $subject->getFormRecord()->getForm() instanceof PointFormManager) {
            // Get country to use in context.
            $countries = [$subject->getFormRecord()->{'field_country'}];
        }
        if (!$user || !$this->accessManager->hasPermission($user, $permission, $countries)) {
            throw new \Exception(
                $this->t(
                    'Transition "@transition" in workflow "@workflow" require "@permission" permission.',
                    [
                        '@transition' => $this->t($event->getTransition()->getName()),
                        '@workflow' => $this->t($event->getWorkflowName()),
                        '@permission' => $permission,
                    ]
                )
            );
        }
        /** @var FormRecord $point */
        $point = $subject->getFormRecord();

        switch ($event->getTransition()->getName()) {
            case 'complete':
                $linkedRecords = [
                    'communities' => [],
                    'providers' => [],
                ];
                $unlinkedRecords = [];
                foreach ($point->{'field_wsystems'} as $systemRecord) {
                    if (!$systemRecord || $systemRecord->{'field_deleted'}) {
                        continue;
                    }
                    // Check the status
                    if ('finished' !== $systemRecord->{'field_status'} && !in_array($systemRecord->{'field_status'}, ['validated', 'locked'])) {
                        throw new \Exception(
                            $this->t(
                                'Transition "@transition" requires all forms finished',
                                [
                                    '@transition' => $this->t($event->getTransition()->getName()),
                                ]
                            )
                        );
                    }
                    // Unlinked system (without field 1.12)
                    if (count($systemRecord->{'field_served_communities'}) === 0) {
                        throw new \Exception(
                            $this->t(
                                'Transition "@transition" requires all forms linked',
                                [
                                    '@transition' => $this->t($event->getTransition()->getName()),
                                ]
                            )
                        );
                    }

                    foreach ($systemRecord->{'field_served_communities'} as $subformRecord) {
                        $linkedRecords['communities'][] = $subformRecord->{'field_community'}->getId();
                        $linkedRecords['providers'][] = $subformRecord->{'field_provider'}->getId();
                    }
                }

                foreach ($point->{'field_communities'} as $communityRecord) {
                    if (!$communityRecord || $communityRecord->{'field_deleted'}) {
                        continue;
                    }
                    // Check the status
                    if ('finished' !== $communityRecord->{'field_status'} && !in_array($communityRecord->{'field_status'}, ['validated', 'locked'])) {
                        throw new \Exception(
                            $this->t(
                                'Transition "@transition" requires all forms finished',
                                [
                                    '@transition' => $this->t($event->getTransition()->getName()),
                                ]
                            )
                        );
                    }

                    $divisionId = $communityRecord->{'field_region'}->getId();
                    if (!in_array($divisionId, $linkedRecords['communities'])) {
                        $unlinkedRecords[] = $communityRecord->getId();
                    }
                }

                foreach ($point->{'field_wsps'} as $wspRecord) {
                    if (!$wspRecord || $wspRecord->{'field_deleted'}) {
                        continue;
                    }
                    // Check the status
                    if ('finished' !== $wspRecord->{'field_status'} && !in_array($wspRecord->{'field_status'}, ['validated', 'locked'])) {
                        throw new \Exception(
                            $this->t(
                                'Transition "@transition" requires all forms finished',
                                [
                                    '@transition' => $this->t($event->getTransition()->getName()),
                                ]
                            )
                        );
                    }

                    if (!in_array($wspRecord->getId(), $linkedRecords['providers'])) {
                        $unlinkedRecords[] = $wspRecord->getId();
                    }
                }

                $totalForms = count($point->{'field_communities'}) + count($point->{'field_wsps'}) + count($point->{'field_wsystems'});
                // A point can have 1 community form only.
                if (count($unlinkedRecords) > 0 && 1 !== $totalForms && count($point->{'field_communities'}) === 1) {
                    throw new \Exception(
                        $this->t(
                            'Transition "@transition" requires all forms linked',
                            [
                                '@transition' => $this->t($event->getTransition()->getName()),
                            ]
                        )
                    );
                }
                break;
        }
    }
}
