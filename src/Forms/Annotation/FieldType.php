<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\Annotation;

use App\Tools\TranslatableMarkup;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\Annotation;

/**
 * Field type annotation.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @Annotation
 *
 * @Target("CLASS")
 */
class FieldType
{
    use StringTranslationTrait;

    /**
     * Field type unique ID.
     *
     * @Required
     */
    public string $id;

    /**
     * Field type label.
     *
     * @Required
     */
    public string $label;

    /**
     * Field type description.
     */
    public string $description = '';

    /**
     * Default settings descriptions.
     *
     * @var array
     */
    public array $settings;

    /**
     * Get field type ID.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get field type label.
     *
     * @return TranslatableMarkup
     */
    public function getLabel(): TranslatableMarkup
    {
        return $this->t($this->label);
    }

    /**
     * Get default field settings descriptions.
     *
     * @return array|bool
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return FieldType
     */
    public function setDescription(string $description): FieldType
    {
        $this->description = $description;

        return $this;
    }
}
