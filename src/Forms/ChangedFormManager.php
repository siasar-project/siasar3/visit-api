<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

/**
 * Forms entity manager.
 *
 * With base changed and created base fields.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ChangedFormManager extends FormManager
{
    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        parent::addBaseFields();

        $this->addField(
            'date',
            'changed',
            'Changed',
            [
                'sort' => true,
                'filter' => true,
            ]
        )
            ->setDescription('Last change time.')
            ->setInternal(true)
            ->setIndexable(true);

        $this->addField(
            'date',
            'created',
            'Created',
            [
                'sort' => true,
                'filter' => true,
            ]
        )
            ->setDescription('Creation time.')
            ->setInternal(true)
            ->setIndexable(true);
    }

    /**
     * Insert a new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        if (!isset($data['field_created'])) {
            $data['field_created'] = new \DateTime();
        }
        if (!isset($data['field_changed'])) {
            $data['field_changed'] = new \DateTime();
        }

        return parent::insert($data);
    }

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update. If have a 'id' key it will be used instead the $id parameter.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$force) {
            if (!$record->isModified()) {
                return;
            }

            $record->{'field_changed'} = new \DateTime();
        }

        parent::update($record, $force);
    }

    /**
     * Get a clean copy of record field values.
     *
     * @param FormRecord $record
     *
     * @return array
     */
    protected function getRecordInternalValues(FormRecord $record): array
    {
        // Get current values.
        $internals = parent::getRecordInternalValues($record);
        unset($internals['field_changed']);
        unset($internals['field_created']);

        return $internals;
    }
}
