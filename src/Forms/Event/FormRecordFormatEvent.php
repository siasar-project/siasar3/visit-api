<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\Event;

use App\Forms\FormRecord;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Form format event.
 */
class FormRecordFormatEvent extends Event
{
    protected FormRecord $record;
    protected array $format;
    protected bool $forceString;

    /**
     * @param FormRecord $record
     * @param bool       $forceString
     */
    public function __construct(FormRecord $record, bool $forceString = false)
    {
        $this->record = $record;
        $this->forceString = $forceString;
        $this->format = [];
    }

    /**
     * @return FormRecord
     */
    public function getRecord(): FormRecord
    {
        return $this->record;
    }

    /**
     * @param FormRecord $record
     *
     * @return FormRecordFormatEvent
     */
    public function setRecord(FormRecord $record): FormRecordFormatEvent
    {
        $this->record = $record;

        return $this;
    }

    /**
     * @return array
     */
    public function getFormat(): array
    {
        return $this->format;
    }

    /**
     * @param array $format
     *
     * @return FormRecordFormatEvent
     */
    public function setFormat(array $format): FormRecordFormatEvent
    {
        $this->format = $format;

        return $this;
    }
}
