<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\Event;

use App\Forms\FormManager;
use App\Forms\FormRecord;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Inquiry form manager event.
 */
class InquiryFormManagerEvent extends Event
{
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';
    public const ACTION_CLONE = 'clone';

    protected FormManager $formManager;
    protected string $action;
    protected array $data;
    protected ?FormRecord $record;

    /**
     * @param FormManager $formManager
     * @param string      $action
     * @param array       $data
     * @param ?FormRecord $record
     */
    public function __construct(FormManager $formManager, string $action, array $data, ?FormRecord $record = null)
    {
        $this->formManager = $formManager;
        $this->action = $action;
        $this->data = $data;
        $this->record = $record;
    }

    /**
     * Get formManager
     *
     * @return FormManager
     */
    public function getFormManager(): FormManager
    {
        return $this->formManager;
    }

    /**
     * Set formManager
     *
     * @param FormManager $formManager
     */
    public function setFormManager(FormManager $formManager): void
    {
        $this->formManager = $formManager;
    }

    /**
     * Get the form action
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Set the value of action
     *
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * Get the value of data
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Set the value of data
     *
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * Get the FormRecord
     *
     * @return FormRecord|null
     */
    public function getRecord(): ?FormRecord
    {
        return $this->record;
    }

    /**
     * Set the FormRecord
     *
     * @param FormRecord|null $record
     */
    public function setRecord(?FormRecord $record): void
    {
        $this->record = $record;
    }
}
