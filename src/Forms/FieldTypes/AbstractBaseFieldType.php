<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes;

use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Tools\TranslatableMarkup;
use App\Traits\StringTranslationTrait;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Base code to field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
abstract class AbstractBaseFieldType implements FieldTypeInterface
{
    use StringTranslationTrait;

    protected array $settings;
    protected array $lastErrors;
    protected string $fieldName;
    protected ?FormManagerInterface $formManager;
    protected ?FormRecord $record;

    /**
     * Field type constructor.
     *
     * @param array $settings Field form settings.
     */
    public function __construct(array $settings = [])
    {
        $this->setSettings($settings);
        $this->lastErrors = [];
        $this->record = null;
        $this->formManager = null;
    }

    /**
     * Get the main property key.
     *
     * This property will be used to sort or filter queries.
     *
     * @return string
     */
    public function getPrimaryProperty(): string
    {
        return 'value';
    }

    /**
     * Set doctrine connection.
     *
     * @param ManagerRegistry $registry Doctrine entity manager.
     *
     * @return void
     */
    public function setRegistry(ManagerRegistry $registry): void
    {
        $this->registry = $registry;
    }

    /**
     * Set form factory.
     *
     * @param FormFactory $formFactory Form factory.
     *
     * @return void
     */
    public function setFormFactory(FormFactory $formFactory): void
    {
        $this->formFactory = $formFactory;
    }

    /**
     * Set form manager.
     *
     * @param FormManagerInterface $formManager Form factory.
     *
     * @return void
     */
    public function setFormManager(FormManagerInterface $formManager): void
    {
        $this->formManager = $formManager;
    }

    /**
     * Get owner form manager.
     *
     * @return FormManagerInterface
     */
    public function getFormManager(): FormManagerInterface
    {
        return $this->formManager;
    }

    /**
     * Get multivalued table name.
     *
     * @return string|null
     */
    public function getFieldTableName(): ?string
    {
        if ($this->isMultivalued()) {
            return $this->getFormManager()->getTableName().'__'.$this->getId();
        }

        return null;
    }

    /**
     * Get multivalued table alias.
     *
     * @return string|null
     */
    public function getFieldTableAlias(): ?string
    {
        if ($this->isMultivalued()) {
            return str_replace('_', '', $this->getFieldTableName());
        }

        return null;
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        $settings = [
            'required' => false,
            'multivalued' => false,
            'weight' => 0,
            'meta' => [],
        ];
        if ($this->isSortable()) {
            $settings['sort'] = false;
        }
        if ($this->isFilterable()) {
            $settings['filter'] = false;
        }

        return $settings;
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        // A multivalued field can't be sortable.
        if (isset($this->settings['settings']['sort']) && $this->settings['settings']['sort'] && $this->isMultivalued()) {
            throw new \Exception(
                $this->t(
                    'The field "@id" can\'t be sortable and multivalued.',
                    [
                        '@id' => $this->getLabelExtended(),
                    ]
                )
            );
        }
        // A multivalued field can't be filterable.
        if (isset($this->settings['indexable']) &&
            $this->settings['indexable'] &&
            isset($this->settings['settings']['filter']) &&
            $this->settings['settings']['filter'] &&
            $this->isMultivalued()
        ) {
            throw new \Exception(
                $this->t(
                    'The field "@id" can\'t be filterable and multivalued.',
                    [
                        '@id' => $this->getLabelExtended(),
                    ]
                )
            );
        }

        return true;
    }

    /**
     * Get field configuration requirements.
     *
     * @return string[]
     */
    public function getConfigurationRequirements(): array
    {
        return [];
    }

    /**
     * Get field ID.
     *
     * The returned value is data base compatible.
     *
     * @return string
     */
    public function getId(): string
    {
        return strtolower($this->settings['id']);
    }

    /**
     * Get field settings.
     *
     * @param bool $dynamic TRUE to add virtual settings.
     *
     * @return array
     */
    public function getSettings(bool $dynamic = false): array
    {
        return $this->settings;
    }

    /**
     * Set field settings.
     *
     * @param array $settings Field settings.
     *
     * @return self
     */
    public function setSettings(array $settings): self
    {
        if (isset($settings['settings'])) {
            $settings['settings'] = array_merge($this->getDefaultSettings(), $settings['settings']);
        } else {
            $settings['settings'] = $this->getDefaultSettings();
        }
        // Settings validation.
        // Multi-properties fields can't be sort or filter.
        if (isset($settings['settings']['sort']) && $settings['settings']['sort'] && !$this->isSortable()) {
            throw new \Exception(
                $this->t(
                    'An "@type" field type can\'t be a sort.',
                    ['@type' => static::class]
                )
            );
        }
        if (isset($settings['settings']['filter']) && $settings['settings']['filter'] && !$this->isFilterable()) {
            throw new \Exception(
                $this->t(
                    'An "@type" field type can\'t be a filter.',
                    ['@type' => static::class]
                )
            );
        }
        // Merge default settings.
        $this->settings = array_merge(
            [
                'id' => '',
                'type' => '',
                'label' => '',
                'description' => '',
                'indexable' => false,
                'internal' => false,
                'deprecated' => false,
                'settings' => [],
            ],
            $settings
        );

        return $this;
    }

    /**
     * Validate field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidContent(mixed &$value, mixed &$row): bool
    {
        if ($this->isMultivalued()) {
            if (!is_null($value)) {
                foreach ($value as &$content) {
                    $isValid = $this->isValidProperties($content, $row);
                    if (!$isValid) {
                        return false;
                    }
                }
            }

            return true;
        }

        return $this->isValidProperties($value, $row);
    }

    /**
     * Validate single field data.
     *
     * If $value is not an array the child class must do the requires basic verifications.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        $properties = $this->getProperties();

        // If value is not array.
        if (!is_array($value)) {
            // This field type only have a property?
            if (count($properties) === 1) {
                // Yes, move value to the property.
                $aux = $properties;
                $aux[key($properties)] = $value;
                $value = $aux;
            } else {
                // Not evaluate it.
                return true;
            }
        }

        $this->lastErrors = [];

        // Have values same values that it's required?
        $vKeys = array_keys($value);
        $pKeys = array_keys($properties);

        $vKeys = sort($vKeys);
        $pKeys = sort($pKeys);
        if ($vKeys !== $pKeys) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" have incomplete values.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }

        // Data it's valid.
        return true;
    }

    /**
     * Get last validation error messages.
     *
     * @return array
     */
    public function getLastValidationMessages(): array
    {
        return $this->lastErrors;
    }

    /**
     * Is it a required field?
     *
     * @param bool $inquiryCheck When InquiryCheckFormRequiredFields calls it, you must call it true to get the actual required fields..
     *
     * @return bool
     */
    public function isRequired(bool $inquiryCheck = false): bool
    {
        if ($this->isDeprecated()) {
            // A deprecated field can't be required.
            return false;
        }

        if (!$inquiryCheck && $this->formManager && $this->formManager->mustIgnoreRequiredFields()) {
            // If the form is in ignore required mode, return not required.
            return false;
        }

        return boolval($this->settings['settings']['required']);
    }

    /**
     * Is it a multivalued field?
     *
     * @return bool
     */
    public function isMultivalued(): bool
    {
        return boolval($this->settings['settings']['multivalued']);
    }

    /**
     * Can we sort by this field?
     *
     * @return bool
     */
    public function canSortBy(): bool
    {
        return (isset($this->settings['settings']['sort']) && boolval($this->settings['settings']['sort']));
    }

    /**
     * Can we filter by this field?
     *
     * @return bool
     */
    public function canFilterBy(): bool
    {
        return (isset($this->settings['settings']['filter']) && boolval($this->settings['settings']['filter']));
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        $nProps = count($this->getProperties());

        return (1 === $nProps);
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        $nProps = count($this->getProperties());

        return (1 === $nProps);
    }

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string
    {
        return $this->getId();
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        return $value;
    }

    /**
     * Get field type ID.
     *
     * @return string
     */
    public function getFieldType(): string
    {
        return $this->settings['type'];
    }

    /**
     * Get SQL line that define this field.
     *
     * @return string
     */
    public function getInstallField(): string
    {
        if (!$this->isMultivalued()) {
            return $this->getSqlFieldDefinitions();
        }
        // Create a new table.
        $ddlFieldList = $this->getSqlFieldDefinitions();
        if (!str_ends_with($ddlFieldList, ', ')) {
            $ddlFieldList .= ', ';
        }

        return sprintf(
            "CREATE TABLE %s (
                          id BINARY(16) NOT NULL COMMENT '(DC2Type:ulid)',
                          record BINARY(16) NOT NULL COMMENT '(DC2Type:ulid)',
                          %s
                          PRIMARY KEY(id),
                          INDEX (record)
                        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB",
            $this->getFieldTableName(),
            $ddlFieldList
        );
    }

    /**
     * Get indexes.
     *
     * Require that each field type define table columns how
     * <field_id>_<property_name>.
     *
     * @return array
     *   [
     *      [
     *          'index' => <index name>,
     *          'column' => <column_name>,
     *          'table' => <table_name>,
     *      ],
     *      ...
     *   ]
     */
    public function getInstallIndexes(): array
    {
        $resp = [];
        $props = $this->getProperties();
        $tableName = $this->getFormManager()->getTableName();

        if ($this->isMultivalued()) {
            $tableName = $this->getFieldTableName();
        }

        // One property field.
        if (count($props) === 1) {
            return [
                [
                    'index' => 'ix_'.$this->getId(),
                    'column' => $this->getId(),
                    'table' => $tableName,
                ],
            ];
        }

        // Multi-property field.
        foreach ($props as $key => $default) {
            $resp[] = [
                'index' => 'ix_'.$this->getId().'_'.$key,
                'column' => $this->getId().'_'.$key,
                'table' => $tableName,
            ];
        }

        return $resp;
    }

    /**
     * Get SQL line that drop this field.
     *
     * @return ?string
     */
    public function getUninstallField(): ?string
    {
        if ($this->isMultivalued()) {
            return sprintf('DROP TABLE IF EXISTS %s', $this->getFieldTableName());
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function haveSqlDefinition(): bool
    {
        return !empty($this->getSqlFieldDefinitions());
    }

    /**
     * Allow this instance know the current state in the form record.
     *
     * @param string     $fieldName Field name.
     * @param FormRecord $record    Form record.
     *
     * @return self
     *
     * @internal
     */
    public function setFormRecord(string $fieldName, FormRecord $record): self
    {
        $this->fieldName = $fieldName;
        $this->record = $record;

        return $this;
    }

    /**
     * Get form record if any.
     *
     * @return ?FormRecord
     */
    public function getFormRecord(): ?FormRecord
    {
        return $this->record;
    }

    /**
     * Set field name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setFieldName(string $name): self
    {
        $this->fieldName = $name;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * Get field label.
     *
     * @return string|TranslatableMarkup
     */
    public function getLabel(): string|TranslatableMarkup
    {
        if (empty($this->settings['label'])) {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" require a label.',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->settings['id'],
                    ]
                )
            );
        }

        return isset($this->settings['label']) ? $this->t($this->settings['label']) : '';
    }

    /**
     * @inheritDoc
     */
    public function getLabelExtended(): string|TranslatableMarkup
    {
        $label = sprintf('%s [%s]', $this->getLabel(), $this->getId());
        $meta = $this->getMeta();
        if (isset($meta['catalog_id'])) {
            $label = sprintf('%s - %s', $meta['catalog_id'], $label);
        }

        return $label;
    }

    /**
     * Set field label.
     *
     * @param string $value New label.
     *
     * @return self
     */
    public function setLabel(string $value): self
    {
        $this->settings['label'] = $value;

        return $this;
    }

    /**
     * Get field description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return (isset($this->settings['description']) && !empty($this->settings['description'])) ? $this->t($this->settings['description'])->render() : '';
    }

    /**
     * Set field description.
     *
     * @param string $value The new description.
     *
     * @return self
     */
    public function setDescription(string $value): self
    {
        $this->settings['description'] = $value;

        return $this;
    }

    /**
     * Set raw value.
     *
     * @param mixed $value Raw value.
     *
     * @return array
     */
    public function setRaw(mixed $value): array
    {
        if (!is_array($value)) {
            $value = [key($this->getProperties()) => $value];
        }

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return $this->getProperties();
    }

    /**
     * Set indexable.
     *
     * @param bool $value Have this field an index?
     *
     * @return self
     */
    public function setIndexable(bool $value = true): self
    {
        $this->settings['indexable'] = $value;

        return $this;
    }

    /**
     * Have this field an index?
     *
     * @return bool
     */
    public function isIndexabe(): bool
    {
        return isset($this->settings['indexable']) ? $this->settings['indexable'] : false;
    }

    /**
     * Set how internal field.
     *
     * @param bool $value Have this field an index?
     *
     * @return self
     */
    public function setInternal(bool $value = true): self
    {
        $this->settings['internal'] = $value;

        return $this;
    }

    /**
     * Is this an internal field?
     *
     * @return bool
     */
    public function isInternal(): bool
    {
        return isset($this->settings['internal']) ? $this->settings['internal'] : false;
    }

    /**
     * Set this field how deprecated.
     *
     * @param string $reason Deprecation value.
     *
     * @return $this
     */
    public function setDeprecated(string $reason = ''): self
    {
        if ('' === $reason && isset($this->settings['deprecated'])) {
            unset($this->settings['deprecated']);
        } else {
            $this->settings['deprecated'] = $reason;
        }

        return $this;
    }

    /**
     * Is this field deprecated?
     *
     * @return bool
     */
    public function isDeprecated(): bool
    {
        return (isset($this->settings['deprecated']) && boolval($this->settings['deprecated']));
    }

    /**
     * Get the deprecation reason if any.
     *
     * @return string
     */
    public function getDeprecationReason(): string
    {
        if ($this->isDeprecated()) {
            return $this->settings['deprecated'];
        }

        return '';
    }

    /**
     * Get field meta.
     *
     * @return string
     */
    public function getMeta(): array
    {
        return $this->settings['settings']['meta'];
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaExtended(): array
    {
        return $this->getMeta();
    }

    /**
     * Set field meta.
     *
     * @param array $value The new meta.
     *
     * @return self
     */
    public function setMeta(array $value): self
    {
        $this->settings['settings']['meta'] = $value;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function dropContent(): self
    {
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function cloneContent(): mixed
    {
        return $this->record->get($this->getId());
    }
}
