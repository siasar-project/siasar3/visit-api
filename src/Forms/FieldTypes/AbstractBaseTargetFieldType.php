<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes;

use App\Forms\FormRecordTargetInterface;

/**
 * Base code to field type that require data type conversion.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
abstract class AbstractBaseTargetFieldType extends AbstractBaseFieldType implements FormRecordTargetInterface
{
    /**
     * Field source.
     *
     * This method allow to this field get extra information from the source instance.
     *
     * @param mixed $value Field data source, ex. a user instance.
     *
     * @return mixed
     */
    public function setSource(mixed $value): mixed
    {
        // Nothing to do by default.
        return $value;
    }
}
