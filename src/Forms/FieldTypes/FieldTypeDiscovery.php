<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes;

use App\Forms\Annotation\FieldType;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Field types discovery service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeDiscovery
{
    protected string $namespace;
    protected string $directory;
    protected Reader $annotationReader;
    /**
     * The Kernel root directory.
     */
    protected string $rootDir;
    protected array $fieldTypes = [];

    /**
     * Field type discovery constructor.
     *
     * @param string $namespace        Annotation name space.
     * @param string $directory        Field types definitions folder.
     * @param string $rootDir          Application root folder.
     * @param Reader $annotationReader Annotation reader.
     */
    public function __construct(string $namespace, string $directory, string $rootDir, Reader $annotationReader)
    {
        $this->namespace = $namespace;
        $this->annotationReader = $annotationReader;
        $this->directory = $directory;
        $this->rootDir = $rootDir;
    }

    /**
     * Returns all the workers
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getFieldTypes(): array
    {
        if (!$this->fieldTypes) {
            $this->discoverFieldTypes();
        }

        return $this->fieldTypes;
    }

    /**
     * Discovers field types.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    protected function discoverFieldTypes(): void
    {
        // Field types are in the types folder in this discoverer.
        $path = __DIR__.'/Types/';
        // $path = $curdir . '/../src/' . $this->directory;
        $finder = new Finder();
        $finder->files()->in($path);
        /**
         * Field type definition file.
         *
         * @var SplFileInfo $file
         */
        foreach ($finder as $file) {
            $class = $this->namespace.'\\'.$file->getBasename('.php');
            $annotation = $this->annotationReader
                ->getClassAnnotation(new \ReflectionClass($class), 'App\Forms\Annotation\FieldType');
            if (!$annotation) {
                continue;
            }

            /**
             * Annotation setting.
             *
             * @var FieldType $annotation
             */
            $this->fieldTypes[$annotation->getId()] = [
                'class' => $class,
                'annotation' => $annotation,
            ];
        }
    }
}
