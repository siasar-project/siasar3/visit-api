<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes;

use App\Forms\FormRecord;
use App\Tools\TranslatableMarkup;

/**
 * Field type interface.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
interface FieldTypeInterface
{
    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array;

    /**
     * Default value.
     *
     * @return array
     */
    public function getDefaultValue(): array;

    /**
     * Get the main property key.
     *
     * This property will be used to sort or filter queries.
     *
     * @return string
     */
    public function getPrimaryProperty(): string;

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array;

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool;

    /**
     * Get field configuration requirements.
     *
     * @return string[]
     */
    public function getConfigurationRequirements(): array;

    /**
     * Get field ID.
     *
     * The returned value is data base compatible.
     *
     * @return string
     */
    public function getId(): string;

    /**
     * Get multivalued table name.
     *
     * @return string|null
     */
    public function getFieldTableName(): ?string;

    /**
     * Get multivalued table alias.
     *
     * @return string|null
     */
    public function getFieldTableAlias(): ?string;

    /**
     * Get SQL line that define this field.
     *
     * This method is generic to all fields, and call to
     * getSqlFieldDefinitions() that is specific.
     *
     * @return string
     */
    public function getInstallField(): string;

    /**
     * Get indexes.
     *
     *
     * @return array
     *   [
     *      [
     *          'index' => <index name>,
     *          'column' => <column_name>,
     *          'table' => <table_name>,
     *      ],
     *      ...
     *   ]
     */
    public function getInstallIndexes(): array;

    /**
     * Get SQL table field definitions.
     *
     * @see FieldTypeInterface::getInstallField()
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string;

    /**
     * Have this field a database representation?
     *
     * @return bool
     */
    public function haveSqlDefinition(): bool;

    /**
     * Get SQL line that drop this field.
     *
     * @return ?string
     */
    public function getUninstallField(): ?string;

    /**
     * Get field settings.
     *
     * @param bool $dynamic TRUE to add virtual settings.
     *
     * @return array
     */
    public function getSettings(bool $dynamic = false): array;

    /**
     * Set field settings.
     *
     * @param array $settings Field settings.
     *
     * @return self
     */
    public function setSettings(array $settings): self;

    /**
     * Validate field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidContent(mixed &$value, mixed &$row): bool;

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool;

    /**
     * Get last validation error messages.
     *
     * @return array
     */
    public function getLastValidationMessages(): array;

    /**
     * Is it a required field?
     *
     * @return bool
     */
    public function isRequired(): bool;

    /**
     * Is it a multivalued field?
     *
     * @return bool
     */
    public function isMultivalued(): bool;

    /**
     * Can we sort by this field?
     *
     * @return bool
     */
    public function canSortBy(): bool;

    /**
     * Can we filter by this field?
     *
     * @return bool
     */
    public function canFilterBy(): bool;

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool;

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool;

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string;

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void;

    /**
     * Format data to be database writable.
     *
     * @param string $property Field property name.
     * @param mixed  $value    Property raw data.
     * @param array  $item     Field raw data.
     *
     * @return mixed
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed;

    /**
     * Format data to be usable.
     *
     * @param mixed $value       Read formatted data.
     * @param bool  $forceString TRUE to get a string response.
     *
     * @return mixed
     */
    public function formatToUse($value, bool $forceString = false): mixed;

    /**
     * Format data that it's derivative from the current field value.
     *
     * e.g. If this is a reference field, this extra data have properties from the
     * referenced entity.
     *
     * @param mixed $value       Current field value.
     * @param bool  $forceString Is required a string response values?
     *
     * @return array
     */
    public function formatExtraJson($value, bool $forceString = false): array;

    /**
     * Get field type ID.
     *
     * @return string
     */
    public function getFieldType(): string;

    /**
     * Allow this instance know the current state in the form record.
     *
     * @param string     $fieldName Field name.
     * @param FormRecord $record    Form record.
     *
     * @return self
     *
     * @internal
     */
    public function setFormRecord(string $fieldName, FormRecord $record): self;

    /**
     * Get form record if any.
     *
     * @return ?FormRecord
     */
    public function getFormRecord(): ?FormRecord;

    /**
     * Set field name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setFieldName(string $name): self;

    /**
     * Get field name.
     *
     * @return string
     */
    public function getFieldName(): string;

    /**
     * Get field label.
     *
     * @return string|TranslatableMarkup
     */
    public function getLabel(): string|TranslatableMarkup;

    /**
     * Get extended field label.
     *
     * This method extend field label with data in meta.
     *
     * @return string|TranslatableMarkup
     */
    public function getLabelExtended(): string|TranslatableMarkup;

    /**
     * Set field label.
     *
     * @param string $value New label.
     *
     * @return self
     */
    public function setLabel(string $value): self;

    /**
     * Get field description.
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set field description.
     *
     * @param string $value The new description.
     *
     * @return self
     */
    public function setDescription(string $value): self;

    /**
     * Set raw value.
     *
     * @param mixed $value Raw value.
     *
     * @return array
     */
    public function setRaw(mixed $value): array;

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array;

    /**
     * Set indexable.
     *
     * @param bool $value Have this field an index?
     *
     * @return self
     */
    public function setIndexable(bool $value = true): self;

    /**
     * Have this field an index?
     *
     * @return bool
     */
    public function isIndexabe(): bool;

    /**
     * Set how internal field.
     *
     * @param bool $value Have this field an index?
     *
     * @return self
     */
    public function setInternal(bool $value = true): self;

    /**
     * Is this an internal field?
     *
     * @return bool
     */
    public function isInternal(): bool;

    /**
     * Set this field how deprecated.
     *
     * @param string $reason Deprecation value.
     *
     * @return $this
     */
    public function setDeprecated(string $reason = ''): self;

    /**
     * Is this field deprecated?
     *
     * @return bool
     */
    public function isDeprecated(): bool;

    /**
     * Get the deprecation reason if any.
     *
     * @return string
     */
    public function getDeprecationReason(): string;

    /**
     * Is this field empty?
     *
     * @return bool
     */
    public function isEmpty(): bool;

    /**
     * Get field meta.
     *
     * @return array
     */
    public function getMeta(): array;

    /**
     * Get field meta with extra structural properties.
     *
     * @return array
     */
    public function getMetaExtended(): array;

    /**
     * Set field meta.
     *
     * @param array $value The new meta.
     *
     * @return self
     */
    public function setMeta(array $value): self;

    /**
     * Drop current record field content.
     *
     * @return self
     */
    public function dropContent(): self;

    /**
     * Clone current record field content.
     *
     * @return mixed New data
     */
    public function cloneContent(): mixed;

    /**
     * Validate conditional type.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public function isValidConditionalType(mixed $value): bool;
}
