<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes;

use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Traits\InjectableObjectInterface;
use App\Traits\StringTranslationTrait;
use Doctrine\Persistence\ManagerRegistry;
use App\Traits\GetContainerTrait;

/**
 * Field type manager.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeManager
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected FieldTypeDiscovery $discovery;
    protected ManagerRegistry $registry;
    protected FormFactory $formFactory;
    protected ?FormManagerInterface $formManager;

    /**
     * Field Type Manager constructor.
     *
     * @param FieldTypeDiscovery $discovery   Field typed discovery service.
     * @param ManagerRegistry    $registry    Doctrine entity manager.
     * @param FormFactory        $formFactory Form factory.
     */
    public function __construct(FieldTypeDiscovery $discovery, ManagerRegistry $registry, FormFactory $formFactory)
    {
        $this->discovery = $discovery;
        $this->registry = $registry;
        $this->formFactory = $formFactory;
        $this->formManager = null;
    }

    /**
     * Set owner form manager.
     *
     * @param FormManagerInterface $formManager Owner form manager.
     *
     * @return void
     */
    public function setFormManager(FormManagerInterface $formManager): void
    {
        $this->formManager = $formManager;
    }

    /**
     * Returns a list of available workers.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getFieldTypes()
    {
        return $this->discovery->getFieldTypes();
    }

    /**
     * Returns one field type by name
     *
     * @param string $id Field type ID.
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getFieldType($id): array
    {
        $fieldTypes = $this->discovery->getFieldTypes();
        if (isset($fieldTypes[$id])) {
            return $fieldTypes[$id];
        }

        throw new \Exception(
            $this->t(
                'Field type "@id" not found. Available types: @keys',
                [
                    '@id' => $id,
                    '@keys' => implode(', ', array_keys($fieldTypes)),
                ]
            )
        );
    }

    /**
     * Creates a field type.
     *
     * @param string $id       The field type ID.
     * @param array  $settings Optional field settings, must include ID and other form field settings.
     *
     * @return FieldTypeInterface
     *
     * @throws \ReflectionException
     */
    public function create($id, array $settings = []): FieldTypeInterface
    {
        $fieldTypes = $this->discovery->getFieldTypes();
        if (array_key_exists($id, $fieldTypes)) {
            $class = $fieldTypes[$id]['class'];
            if (!class_exists($class)) {
                throw new \Exception($this->t('[@id] Field type class does not exist.', ['@id' => $id]));
            }

            if (is_subclass_of($class, InjectableObjectInterface::class)) {
                $fieldType = $class::instantiate(self::getContainerInstance());
                $fieldType->setSettings($settings);
            } else {
                $fieldType = new $class($settings);
            }
            if ($this->formManager) {
                $fieldType->setFormManager($this->formManager);
            }
            // Require that default setting are defined in annotations.
            $defaults = $fieldType->getDefaultSettings();
            foreach ($defaults as $key => $description) {
                if (!isset($fieldTypes[$id]['annotation']->settings[$key])) {
                    throw new \Exception(
                        $this->t(
                            '[@id] Field type default setting "@key" not defined in annotation.',
                            [
                                '@id' => $id,
                                '@key' => $key,
                            ]
                        )
                    );
                }
            }

            return $fieldType;
        }

        throw new \Exception(
            $this->t(
                'Field type "@id" not found. Available types: @types',
                [
                    '@id' => $id,
                    '@types' => implode(', ', array_keys($fieldTypes)),
                ]
            )
        );
    }
}
