<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\EntityCountryReferenceFieldTrait;
use ReflectionClass;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "administrative_division_reference",
 *     label = "Administrative division reference field type.",
 *     description = "Requires a country field in same form. The reference must be a community's administrative division.",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "country_field": "The country reference field",
 *     }
 * )
 */
class AdministrativeDivisionReferenceEntity extends EntityReferenceFieldType
{
    use EntityCountryReferenceFieldTrait;

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $value = parent::getProperties();
        $value['class'] = AdministrativeDivision::class;

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $divisionRepo = $this->registry->getRepository(AdministrativeDivision::class);
        $countryRepo = $this->registry->getRepository(Country::class);

        if (isset($row[$this->getCountryFieldName()]['value'])) {
            // If we have a linked country field.
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                /** @var Country $country */
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if ($country) {
                    /** @var AdministrativeDivision $division */
                    $division = current($divisionRepo->findBy(
                        [
                            'country' => $country->getId(),
                            'level' => $country->getDeep(),
                        ],
                        [],
                        1
                    ));
                    if ($division) {
                        return [
                            'value' => $division->getId(),
                            'class' => AdministrativeDivision::class,
                        ];
                    }
                }
            }
        }

        /** @var Country $honduras */
        $honduras = $countryRepo->find('hn');
        $divisions = $divisionRepo->findBy(
            [
                'country' => $honduras->getId(),
                'level' => $honduras->getDeep(),
            ],
            [],
            100
        );
        $divisionId = '';
        if (count($divisions) > 0) {
            $division = $divisions[rand(0, count($divisions) - 1)];
            $divisionId = $division->getId();
        }

        return [
            'value' => $divisionId,
            'class' => AdministrativeDivision::class,
        ];
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        parent::isValidSettings();
//        if ($this->isMultivalued()) {
//            throw new \Exception(
//                $this->t(
//                    'An "@type" field type can\'t be multivalued.',
//                    ['@type' => $this->getFieldType()]
//                )
//            );
//        }

        $this->isValidSettingsTrait();

        return true;
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }

        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (is_null($value)) {
            $aux = [];
            $aux['class'] = 'App\Entity\AdministrativeDivision';
            $aux['value'] = '';
            $value = $aux;
        } else {
            $aux = [];
            if (!$value instanceof AdministrativeDivision) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\AdministrativeDivision".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                );

                return false;
            }
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // If not empty value.
        if ('' !== $value['value']) {
            // Get country field value.
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isMultivalued()) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" can\'t use a multivalued field how country reference, "@ref".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                        '@ref' => $this->getCountryFieldName(),
                    ]
                );

                return false;
            }
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                // Validate division with the selected country.
                $divisionRepo = $this->registry->getRepository($value['class']);
                /** @var AdministrativeDivision $division */
                $division = $divisionRepo->find($value['value']);
                if (!$division) {
                    // Division not found.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a wrong reference: "@ref".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@ref' => $value['value'],
                        ]
                    );

                    return false;
                }
                /** @var Country $country */
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if (!$country) {
                    // Country not found.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a wrong reference: "@ref".',
                        [
                            '@type' => $countryField->getFieldType(),
                            '@id' => $countryField->getLabelExtended(),
                            '@ref' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
                if ($division->getCountry()->getId() !== $country->getId()) {
                    // Division not into country.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a reference, "@ref", outside of country "@country".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@ref' => $value['value'],
                            '@country' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
                if ($division->getLevel() !== $country->getDeep()) {
                    // Division it's not a community.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a reference, "@ref", that it\'s not an community of country "@country".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@ref' => $value['value'],
                            '@country' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
            } else {
                return false;
            }
        }

        // Data it's valid.
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        $resp = [];
        /** @var AdministrativeDivision $target */
        $target = $this->getTarget($value);
        if ($target) {
            $resp = $target->formatExtraJson($forceString);
        }

        return $resp;
    }
}
