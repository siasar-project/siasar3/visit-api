<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;

/**
 * Boolean field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "boolean",
 *     label = "Boolean field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "true_label": "Label to show TRUE value",
 *         "false_label": "Label to show FALSE value",
 *         "show_labels": "Must this field show value labels?",
 *     }
 * )
 */
class BooleanFieldType extends AbstractBaseTargetFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value.
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => false,
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        // A boolean field always have a value.
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return $this->getProperties();
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return [
            'value' => rand(0, 1),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'true_label' => 'Yes',
                'false_label' => 'No',
                'show_labels' => true,
            ],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        // Column definition.
        return sprintf(
            '%s VARCHAR(1) DEFAULT \'0\' %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label'])
        );
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }
        // Force bool value.
        $value['value'] = self::isTrue($value['value']);
        // Data it's valid.
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isValidSettings(): bool
    {
        parent::isValidSettings();

        // Value labels must have a value.
        if (empty(trim($this->settings['settings']['true_label'])) || empty(trim($this->settings['settings']['false_label']))) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require value labels.',
                    [
                        '@id' => $this->getId(),
                    ]
                )
            );
        }

        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if (is_string($data)) {
            $aux = $data;
            $data = [
                'value' => $aux,
            ];
        }
        $data = ($data['value'] ? '1' : '0');
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        return self::isTrue(intval($value['value']));
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'boolean' === gettype($value);
    }

    /**
     * Is this value true?
     *
     * @param mixed $val The value to convert.
     *
     * @return bool|mixed
     *
     * @see https://www.php.net/manual/en/function.boolval.php#116547
     */
    public static function isTrue(mixed $val): ?bool
    {
        if (is_bool($val)) {
            return $val;
        }
        $boolval = (
        is_string($val) ?
            filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) :
            (bool) $val );

        return $boolval;
    }
}
