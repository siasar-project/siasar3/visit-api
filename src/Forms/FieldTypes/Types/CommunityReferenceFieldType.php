<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Traits\InjectableObjectInterface;
use App\Forms\Annotation\FieldType;
use Symfony\Component\Uid\Ulid;

/**
 * Form record reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "community_reference",
 *     label = "Community form record reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "country_field": "The country reference field",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class CommunityReferenceFieldType extends WSProviderReferenceFieldType implements InjectableObjectInterface
{
    protected const REFERRED_FORM_ID = 'form.community';

    /**
     * {@inheritDoc}
     */
    public function cloneContent(): mixed
    {
        $resp = [];
        if ($this->isMultivalued()) {
            $records = $this->record->{$this->getId()};
            /** @var FormRecord $record */
            foreach ($records as $record) {
                $clone = $record->clone();
                $ulid = Ulid::fromString($clone->getId());
                $resp[] = [
                    'value' => $ulid->toBase32(),
                    'form' => $clone->getForm()->getId(),
                ];
            }
        } else {
            $record = $this->record->{$this->getId()};
            $clone = $record->clone();
            $ulid = Ulid::fromString($clone->getId());
            $resp = [
                'value' => $ulid->toBase32(),
                'form' => $clone->getForm()->getId(),
            ];
        }

        return $resp;
    }
}
