<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FormReferenceEntityInterface;
use ReflectionClass;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "country_reference",
 *     label = "Country reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class CountryReferenceEntity extends EntityReferenceFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $value = parent::getProperties();
        $value['class'] = 'App\Entity\Country';

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return [
            'value' => 'hn',
            'class' => Country::class,
        ];
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return true;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return true;
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        $columns[] = sprintf(
            '%s_value TEXT(50) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );
        $columns[] = sprintf(
            '%s_class TEXT(255) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' class'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        if (is_array($value)) {
            if (!empty($value['value'])) {
                $value['class'] = 'App\Entity\Country';
            } else {
                $value['class'] = '';
            }
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (is_null($value)) {
            $aux = [];
            $aux['class'] = 'App\Entity\Country';
            $aux['value'] = '';
            $value = $aux;
        } else {
            $aux = [];
            if (!$value instanceof Country) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\Country".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                );

                return false;
            }
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $value = $data;
        if (is_object($value)) {
            if (!$value instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            if (!$value instanceof Country) {
                throw new \Exception(
                    $this->t(
                        '[@type] Field "@id" requires values of type "App\Entity\Country".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            $aux['class'] = 'App\Entity\Country';
            $aux['value'] = $value->getId();
            $data = $aux;
        }
        if (empty($value['class']) && empty($value['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_class'] = '';
        } else {
            $data[$this->getId().'_value'] = $data['value'];
            $reflectionClass = new ReflectionClass($data['class']);
            if ($data['class'] === 'App\Entity\Country' || $reflectionClass->isSubclassOf('App\Entity\Country')) {
                $data[$this->getId().'_class'] = 'App\Entity\Country';
            } else {
                $data[$this->getId().'_class'] = $data['class'];
            }
        }
        unset($data['value']);
        unset($data['class']);
        // Force entity type.
        if (!empty($data[$this->getId().'_class']) && $data[$this->getId().'_class'] !== 'App\Entity\Country') {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\Country".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                )
            );
        }
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        return $value;
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        if (!class_exists($value['class'], true)) {
            return null;
        }

        return $this->registry->getRepository($value['class'])->find($value['value']);
    }
}
