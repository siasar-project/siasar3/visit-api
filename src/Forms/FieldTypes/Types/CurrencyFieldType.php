<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\Currency;
use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FormReferenceEntityInterface;
use App\Tools\CurrencyAmount;
use App\Traits\EntityCountryReferenceFieldTrait;
use ReflectionClass;
use Symfony\Component\Uid\Ulid;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "currency",
 *     label = "Currency reference field type.",
 *     description = "Requires a country field in same form",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "country_field": "The country reference field",
 *         "unknowable": "Can this field be unknow? If TRUE will be unknow if value is -9999999.",
 *     }
 * )
 */
class CurrencyFieldType extends EntityReferenceFieldType
{
    use EntityCountryReferenceFieldTrait;

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $value = parent::getProperties();
        $value['class'] = Currency::class;
        $value['amount'] = 0;

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $countryId = 'hn';
        if (isset($row[$this->getCountryFieldName()]['value'])) {
            $countryId = $row[$this->getCountryFieldName()]['value'];
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                /** @var Country $country */
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if ($country) {
                    $currency = $country->getCurrencies()->first();
                    if ($currency) {
                        return [
                            'value' => $currency->getId(),
                            'class' => Currency::class,
                            'amount' => rand(0, 1000),
                        ];
                    }
                }
            }
        }

        $currenciesRepo = $this->registry->getRepository(Currency::class);
        $currencies = $currenciesRepo->findBy(['country' => $countryId], [], 100, 0);
        $currencyId = '';
        if (count($currencies) > 0) {
            $currency = $currencies[rand(0, count($currencies) - 1)];
            $currencyId = $currency->getId();
        }

        return [
            'value' => $currencyId,
            'class' => Currency::class,
            'amount' => rand(0, 1000),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        $settings = $this->getDefaultSettingsFromTrait();
        $settings['unknowable'] = false;

        return $settings;
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = explode(', ', parent::getSqlFieldDefinitions());
        $columns[] = sprintf(
            '%s_amount DECIMAL(24,6) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        parent::isValidSettings();

        $this->isValidSettingsTrait();

        return true;
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return false;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return false;
    }

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string
    {
        return $this->getId().'_amount';
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (is_null($value)) {
            $aux = [];
            $aux['class'] = '';
            $aux['value'] = '';
            $aux['amount'] = 0;
            $value = $aux;
        } else {
            if (!$value instanceof CurrencyAmount) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values of type "@class".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                        '@class' => CurrencyAmount::class,
                    ]
                );

                return false;
            }
            $value = $value->toArray();
        }

        // If not empty value.
        if ('' !== $value['value']) {
            // Get country field value.
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isMultivalued()) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" can\'t use a multivalued field how country reference, "@ref".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                        '@ref' => $this->getCountryFieldName(),
                    ]
                );

                return false;
            }
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                // Validate Currency with the selected country.
                $currencyRepo = $this->registry->getRepository($value['class']);
                /** @var Currency $currency */
                $currency = $currencyRepo->find($value['value']);
                if (!$currency) {
                    // Currency not found.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a wrong reference: "@ref".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                            '@ref' => $value['value'],
                        ]
                    );

                    return false;
                }
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if (!$country) {
                    // Country not found.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a wrong reference: "@ref".',
                        [
                            '@type' => $countryField->getFieldType(),
                            '@id' => $countryField->getId(),
                            '@ref' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
                if ($currency->getCountry()->getId() !== $country->getId()) {
                    // Currency not into country.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a reference, "@ref", outside of country "@country".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                            '@ref' => $value['value'],
                            '@country' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
            } else {
                return false;
            }
        }

        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $aux = $data;
        if (!isset($data['amount'])) {
            $data['amount'] = 0;
        }
        parent::preInsert($data);
        if (empty($aux)) {
            $data[$this->getId().'_amount'] = 0;
        } else {
            $data[$this->getId().'_amount'] = floatval($data['amount']);
        }
        unset($data['amount']);
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        if (!class_exists($value['class'], true)) {
            return null;
        }

        $ulid = Ulid::fromString($value['value']);
        $currency = $this->registry->getRepository($value['class'])->find($ulid->toBase32());

        return new CurrencyAmount($value['amount'], $currency);
    }

    /**
     * Field source.
     *
     * This method allow to this field get extra information from the source instance.
     *
     * @param mixed $value Field data source, ex. a user instance.
     *
     * @return mixed
     */
    public function setSource(mixed $value): mixed
    {
        if (is_object($value)) {
            if (!$value instanceof CurrencyAmount) {
                throw new \Exception(
                    $this->t(
                        '[currency] This field requires an instance of "CurrencyAmount", in form "@form".',
                        [
                            '@form' => $this->formManager->getId(),
                        ]
                    )
                );
            }
            $aux['class'] = Currency::class;
            $aux['value'] = $value?->currency->getId();
            $aux['amount'] = $value->amount;
            $value = $aux;
        } elseif (isset($value["value"]) && is_object($value["value"])) {
            // Do nothing.
        } elseif (is_null($value) || (isset($value["value"]) && is_null($value["value"]))) {
            $value['class'] = '';
            $value['value'] = '';
            $value['amount'] = 0;
        } elseif (is_array($value)) {
            // Do nothing.
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        if ('value' === $property && !empty($value)) {
            $ulid = Ulid::fromString($value);
            $value = $ulid->toBase32();
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        $aux = parent::formatToUse($value, $forceString);

        return $aux;
    }
}
