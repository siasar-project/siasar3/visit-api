<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Forms\FormRecordTargetInterface;
use App\Forms\FormReferenceEntityInterface;
use Symfony\Component\Validator\Constraints\Timezone;

/**
 * Date field type.
 *
 * Date information must be saved in the time zone UTC and read in input time zone.
 * The input time zone will be saved in the timezone field in database.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "date",
 *     label = "Date field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class DateFieldType extends AbstractBaseTargetFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => new \DateTime(),
            'timezone' => new \DateTimeZone('UTC'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => '',
            'timezone' => '',
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $year = rand(2000, 2100);
        $month = str_pad(rand(1, 12), 2, "0", STR_PAD_LEFT);
        $day = str_pad(rand(1, 28), 2, "0", STR_PAD_LEFT);
        $hour = str_pad(rand(1, 23), 2, "0", STR_PAD_LEFT);
        $min = str_pad(rand(0, 59), 2, "0", STR_PAD_LEFT);
        $sec = str_pad(rand(0, 59), 2, "0", STR_PAD_LEFT);

        return [
            'value' => "$year-$month-$day $hour:$min:$sec",
            'timezone' => 'Europe/Madrid',
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [],
        );
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return true;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return true;
    }

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string
    {
        return parent::getSortOrFilterTableColumn().'_value';
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        /*
         * @see https://dev.mysql.com/doc/refman/8.0/en/datetime.html
         *
         * The DATE type is used for values with a date part but no time part.
         * MySQL retrieves and displays DATE values in 'YYYY-MM-DD' format. The
         * supported range is '1000-01-01' to '9999-12-31'.
         *
         * The DATETIME type is used for values that contain both date and time
         * parts. MySQL retrieves and displays DATETIME values in
         * 'YYYY-MM-DD hh:mm:ss' format.
         * The supported range is '1000-01-01 00:00:00' to
         * '9999-12-31 23:59:59'.
         *
         * The TIMESTAMP data type is used for values that contain both date
         * and time parts. TIMESTAMP has a range of '1970-01-01 00:00:01' UTC
         * to '2038-01-19 03:14:07' UTC.
         */
        $columns[] = sprintf(
            '%s_value DATETIME %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );
        $columns[] = sprintf(
            '%s_timezone TEXT %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' timezone'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (is_array($value)) {
            // Array is the default $value format.
            if ($value['value'] instanceof \DateTime && empty($value['timezone'])) {
                $value['timezone'] = $value['value']->getTimezone()->getName();
            }
        } elseif (is_null($value)) {
            $aux['timezone'] = '';
            $aux['value'] = '';
            $value = $aux;
        } elseif (!$value instanceof \DateTime) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is not an Datetime, it\'s an "@valuetype".',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getId(),
                    '@valuetype' => gettype($value),
                ]
            );

            return false;
        } else {
            $aux = [];
            $aux['timezone'] = $value->getTimezone()->getName();
            $aux['value'] = $value;
            $value = $aux;
        }

        // We need save value and timezone.
        if (empty($value['value']) xor empty($value['timezone'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is uncompleted. "value" or "timezone" is empty.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getId(),
                ]
            );

            return false;
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if (empty($data['timezone']) && empty($data['value'])) {
            // Allow insert empty fields.
            $data = [
                $this->getId().'_value' => null,
                $this->getId().'_timezone' => '',
            ];
        }
        // Convert date to UTC time zone.
        if (isset($data['value'])) {
            if (is_string($data['value'])) {
                $data['value'] = new \DateTime($data['value'], new \DateTimeZone($data['timezone']));
            }
            /**
             * Value.
             *
             * @var \DateTime $date
             */
            $date = $data['value'];
            $date->setTimezone(new \DateTimeZone('UTC'));
            $data = [
                $this->getId().'_value' => $date->format('Y-m-d H:i:s'),
                $this->getId().'_timezone' => ($data['timezone'] instanceof \DateTimeZone) ?
                    $data['timezone']->getName() :
                    $data['timezone'],
            ];
        }
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        switch ($property) {
            case 'value':
                if (is_null($value)) {
                    $value = null;
                } elseif ($value instanceof \DateTime) {
                    // No update required.
                } else {
                    $value = new \DateTime($value, new \DateTimeZone('UTC'));
                    if (!empty($item["timezone"])) {
                        $value->setTimezone(new \DateTimeZone($item["timezone"]));
                    }
                }
                break;

            case 'timezone':
                break;
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        if (empty($value['value'])) {
            return $value;
        }

        if (is_string($value['value'])) {
            $value['value'] = new \DateTime($value['value'], new \DateTimeZone('UTC'));
        }
        if ($value['value'] instanceof \DateTime) {
            if (empty($value["timezone"])) {
                $value["timezone"] = $value['value']->getTimezone()->getName();
            }
            if ($value["timezone"] instanceof \DateTimeZone) {
                $value['value']->setTimezone($value["timezone"]);
            } else {
                $value['value']->setTimezone(new \DateTimeZone($value["timezone"]));
            }
            $value['value'] = $value['value']->format('Y-m-d H:i:s');
        }

        return $value;
    }

    /**
     * Field target.
     *
     * Return a final data representation to this field content.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        if (empty($value['value']) && empty($value['timezone'])) {
            return null;
        }

        // Convert date to $value['timezone'] from UTC.
        if (empty($value['timezone'])) {
            $value['timezone'] = 'UTC';
        }
        if ($value['timezone'] instanceof \DateTimeZone) {
            $timezone = $value['timezone'];
        } else {
            $timezone = new \DateTimeZone($value['timezone']);
        }
        if ($value['value'] instanceof \DateTime) {
            $date = $value['value'];
        } else {
            /**
             * Value.
             *
             * @var \DateTime $date
             */
            $date = new \DateTime($value['value'], new \DateTimeZone('UTC'));
        }

        return $date->setTimezone($timezone);
    }

    /**
     * Field source.
     *
     * This method allow to this field get extra information from the source instance.
     *
     * @param mixed $value Field data source, ex. a user instance.
     *
     * @return mixed
     */
    public function setSource(mixed $value): mixed
    {
        // Get time zone from value.
        if ($value instanceof \DateTime) {
            $aux['timezone'] = $value->getTimezone()->getName();
            $aux['value'] = $value->setTimezone(new \DateTimeZone('UTC'));
            $value = $aux;
        } elseif (isset($value["value"]) && $value["value"] instanceof \DateTime) {
            // Do nothing.
        } elseif (is_null($value) || is_null($value["value"])) {
            $value['timezone'] = '';
            $value['value'] = '';
        } elseif (is_array($value)) {
            /**
             * Value.
             *
             * @var \DateTime $date
             */
            $date = new \DateTime($value['value'], new \DateTimeZone('UTC'));
            $date->setTimezone(new \DateTimeZone($value['timezone']));
            $value['value'] = $date;
        }

        return $value;
    }

    /**
     * Set raw value.
     *
     * @param array $value Raw value.
     *
     * @return array
     */
    public function setRaw(mixed $value): array
    {
        if (!is_array($value)) {
            return $value;
        }

        if ($this->isMultivalued()) {
            foreach ($value as $vKey => &$vValue) {
                $date = new \DateTime($vValue['value'], new \DateTimeZone($vValue['timezone']));
                $vValue['value'] = $date;
            }
        } else {
            if (empty($value['timezone'])) {
                $value['timezone'] = 'UTC';
            }
            $date = new \DateTime($value['value'], new \DateTimeZone($value['timezone']));
            $value['value'] = $date;
        }

        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        $resp = [];
        /** @var \DateTime $target */
        $target = $this->getTarget($value);
        if ($target) {
            if ($value['timezone'] instanceof \DateTimeZone) {
                $target = $target->setTimezone($value["timezone"]);
            } else {
                $target = $target->setTimezone(new \DateTimeZone($value["timezone"]));
            }
            $resp['real'] = $target->format('Y-m-d H:i:s');
        }

        return $resp;
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'string' === gettype($value);
    }
}
