<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;

/**
 * Decimal field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "decimal",
 *     label = "Decimal field type",
 *     settings = {
 *         "min": "Minimum value",
 *         "max": "Maximun value",
 *         "unknowable": "Can this field be unknow? If TRUE will be unknow if value is -9999999.",
 *         "precision": "The maximum number of digits (the precision). It has a range of 1 to 65.",
 *         "scale": "The number of digits to the right of the decimal point (the scale). It has a range of 0 to 30 and must be no larger than precision",
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class DecimalFieldType extends AbstractBaseTargetFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => 0,
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if ('0' !== $value && !$value) {
            return true;
        }

        if ($this->isMultivalued()) {
            return count($value) > 0;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => 0,
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return [
            'value' => random_int(100, 200),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'min' => PHP_INT_MIN,
                'max' => PHP_INT_MAX,
                'precision' => 10,
                'scale' => 0,
                'unknowable' => false,
            ],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        // Settings validation.
        if ($this->settings['settings']['precision'] < 1 || $this->settings['settings']['precision'] > 65) {
            throw new \Exception(
                $this->t(
                    'Field "@id" have a invalid precision, it must be @min <= @value <= @max.',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@min' => 1,
                        '@value' => $this->settings['settings']['precision'],
                        '@max' => 65,
                    ]
                )
            );
        }
        if ($this->settings['settings']['scale'] < 0 || $this->settings['settings']['scale'] > 30) {
            throw new \Exception(
                $this->t(
                    'Field "@id" have a invalid scale, it must be @min <= @value <= @max.',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@min' => 0,
                        '@value' => $this->settings['settings']['scale'],
                        '@max' => 30,
                    ]
                )
            );
        }
        if ($this->settings['settings']['scale'] > $this->settings['settings']['precision']) {
            throw new \Exception(
                $this->t(
                    'Field "@id" have a invalid scale, it must lower that precision, @scale > @precision.',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@scale' => $this->settings['settings']['scale'],
                        '@precision' => $this->settings['settings']['precision'],
                    ]
                )
            );
        }
        // Column definition.
        return sprintf(
            '%s DECIMAL(%s,%s) %s COMMENT \'%s\'',
            $this->getId(),
            $this->settings['settings']['precision'],
            $this->settings['settings']['scale'],
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label'])
        );
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }
        // Empty not required value?
//        if (!$this->isRequired() && empty($value['value'])) {
//            $value['value'] = 0;
//        }

        if (!empty($value['value'])) {
            // Is value in the range?
            if ($value['value'] < $this->settings['settings']['min']
                || $value['value'] > $this->settings['settings']['max']
            ) {
                if (!(-9999999 === $value['value'] && $this->settings['settings']['unknowable'])) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a invalid value, it must be @min <= @value <= @max.',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@min' => $this->settings['settings']['min'],
                            '@value' => strlen($value['value']),
                            '@max' => $this->settings['settings']['max'],
                        ]
                    );

                    return false;
                }
            }
            // Is value in the database range?
            $maxPrecisionLength = $this->settings['settings']['precision'] - $this->settings['settings']['scale'];
            $absMaxValue = '';
            for ($i = 1; $i <= $maxPrecisionLength; ++$i) {
                $absMaxValue .= '9';
            }
            $absMaxValue = intval($absMaxValue);
            if ($value['value'] < (-1 * $absMaxValue) || $value['value'] > $absMaxValue) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" have a invalid value, it must be @min <= @value <= @max.',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@min' => (-1 * $absMaxValue),
                        '@value' => strlen($value['value']),
                        '@max' => $absMaxValue,
                    ]
                );

                return false;
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && !is_numeric($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if (empty($data['value'])) {
            $data['value'] = null;
        }
        $data = $data['value'];
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        return floatval($value['value']);
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'double' === gettype($value);
    }
}
