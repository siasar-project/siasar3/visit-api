<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;

/**
 * Map point field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "editor_update",
 *     label = "Editor + Update field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "This field type can't be multivalued.",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "editor_field": "The editor reference field",
 *         "update_field": "The update reference field",
 *     }
 * )
 */
class EditorUpdateFieldType extends AbstractBaseFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value.
     *
     * @return array
     */
    public function getProperties(): array
    {
        // Mano-property fields have problems in tests.
        return [
            'value' => '',
            'value1' => '',
        ];
    }

    /**
     * Get the main property key.
     *
     * This property will be used to sort or filter queries.
     *
     * @return string
     */
    public function getPrimaryProperty(): string
    {
        return 'value';
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return $this->getProperties();
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return [
            'value' => '',
            'value1' => '',
        ];
    }

    /**
     * Set field settings.
     *
     * @param array $settings Field settings.
     *
     * @return self
     */
    public function setSettings(array $settings): self
    {
        parent::setSettings($settings);

        return $this;
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'editor_field' => '',
                'update_field' => '',
            ],
        );
    }

    /**
     * Get field meta.
     *
     * @return string
     */
    public function getMeta(): array
    {
        // This field type always is disabled, readonly.
        if (!isset($this->settings['settings']['meta']['disabled'])) {
            $this->settings['settings']['meta']['disabled'] = true;
        }

        return $this->settings['settings']['meta'];
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        // Column definition.
        return '';
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isValidSettings(): bool
    {
        if ($this->isMultivalued()) {
            throw new \Exception(
                $this->t(
                    'An "@type" field type can\'t be multivalued.',
                    ['@type' => $this->getFieldType()]
                )
            );
        }

        if (!isset($this->settings["settings"]["editor_field"])) {
            $this->settings["settings"]["editor_field"] = '';
        }
        if (!isset($this->settings["settings"]["update_field"])) {
            $this->settings["settings"]["update_field"] = '';
        }

        // editor field validation.
        $externalField = $this->formManager->getFieldDefinition($this->settings["settings"]["editor_field"]);
        if (!$externalField) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require "@country" field in form "@form", but it\'s not found.',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->settings["settings"]["editor_field"],
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }
        if ($externalField->getFieldType() !== 'user_reference') {
            throw new \Exception(
                $this->t(
                    'The field "@id" require a "user_reference" field in form "@form", but is "@type".',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->settings["settings"]["editor_field"],
                        '@form' => $this->formManager->getId(),
                        '@type' => $externalField->getFieldType(),
                    ]
                )
            );
        }
        // update field validation.
        $externalField = $this->formManager->getFieldDefinition($this->settings["settings"]["update_field"]);
        if ($externalField->getFieldType() !== 'date') {
            throw new \Exception(
                $this->t(
                    'The field "@id" require a "date" field in form "@form", but is "@type".',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->settings["settings"]["editor_field"],
                        '@form' => $this->formManager->getId(),
                        '@type' => $externalField->getFieldType(),
                    ]
                )
            );
        }
        if (!$externalField) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require "@country" field in form "@form", but it\'s not found.',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->settings["settings"]["update_field"],
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }

        return parent::isValidSettings();
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        // This field type don't have database column.
        $data = [];
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return false;
    }
}
