<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\User;
use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\InjectableObjectInterface;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Object_;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "entity_reference",
 *     label = "Entity reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "meta": "Field metadata properties.",
 *     }
 * )
 */
class EntityReferenceFieldType extends AbstractBaseTargetFieldType implements InjectableObjectInterface
{
    protected ManagerRegistry $registry;

    /**
     * Instantiate with required dependencies injected.
     *
     * @param ContainerInterface $container Service container.
     *
     * @return $this
     */
    public static function instantiate(ContainerInterface $container): mixed
    {
        $instance = new static();
        $instance->registry = $container->get('doctrine');

        return $instance;
    }

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => '',
            'class' => '',
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if (isset($value['value']) && empty($value['value'])) {
            return true;
        }

        if ($this->isMultivalued()) {
            if (!$value) {
                return true;
            }
            foreach ($value as $item) {
                if (isset($item['value']) && !empty($item['value'])) {
                    return false;
                }
            }

            return true;
        }

        return empty($value['value']) || '00000000000000000000000000' === $value['value'];
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => '',
            'class' => '',
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $ulid = new Ulid();

        return [
            'value' => $ulid->toBase32(),
            'class' => User::class,
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        $columns[] = sprintf(
            '%s_value binary(16) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );
        $columns[] = sprintf(
            '%s_class TEXT(255) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' class'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (!is_object($value) && $this->isRequired()) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is not an object, it\'s an "@class". May be required?',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@class' => gettype($value),
                ]
            );

            return false;
        } elseif (is_null($value)) {
            $aux['class'] = '';
            $aux['value'] = '';
            $value = $aux;
        } else {
            $aux = [];
            if (!$value instanceof FormReferenceEntityInterface) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                    ]
                );

                return false;
            }
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // We need save value and class.
        if ('00000000000000000000000000' === $value["value"]) {
            $value = ['value' => '', 'form' => ''];
        }
        if (empty($value['value']) xor empty($value['class'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is uncompleted. "value" or "class" is empty.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getId(),
                ]
            );

            return false;
        }

        // The class name must be valid.
        if (!empty($value['class']) && !class_exists($value['class'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" required a valid referenced class, "@class" don\'t exist. ',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getId(),
                    '@class' => $value['class'],
                ]
            );

            return false;
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return true;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return true;
    }

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string
    {
        return parent::getSortOrFilterTableColumn().'_value';
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $value = $data;
        if (is_object($value)) {
            if (!$value instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $data = $aux;
        }
        if (empty($value['class']) && empty($value['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_class'] = '';
        } else {
            if (!empty($data['value'])) {
                $ulid = Ulid::fromString($data['value']);
                $data[$this->getId().'_value'] = $ulid->toBinary();
                $data[$this->getId().'_class'] = $data['class'];
            } else {
                $data[$this->getId().'_value'] = '';
                $data[$this->getId().'_class'] = '';
            }
        }
        unset($data['value']);
        unset($data['class']);
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        if (is_null($value)) {
            return null;
        }

        if ('value' === $property) {
            if (!empty(trim($value))) {
                $ulid = Ulid::fromString($value);
                $value = $ulid->toBase32();
            } else {
                $value = '';
            }
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        if (/*$forceString &&*/ !empty($value) && !empty($value['value'])) {
            $ulid = Ulid::fromString($value['value']);
            $value['value'] = $ulid->toBase32();
        }

        return $value;
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        if (!class_exists($value['class'], true)) {
            return null;
        }

        if (is_null($value['value'])) {
            return null;
        }

        $ulid = Ulid::fromString($value['value']);

        return $this->registry->getRepository($value['class'])->find($ulid->toBase32());
    }

    /**
     * Field source.
     *
     * This method allow to this field get extra information from the source instance.
     *
     * @param mixed $value Field data source, ex. a user instance.
     *
     * @return mixed
     */
    public function setSource(mixed $value): mixed
    {
        // Get time zone from value.
        if (is_object($value)) {
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $value = $aux;
        } elseif (isset($value["value"]) && is_object($value["value"])) {
            // Do nothing.
        } elseif (is_null($value) || (isset($value["value"]) && is_null($value["value"]))) {
            $value['class'] = '';
            $value['value'] = '';
        } elseif (is_array($value)) {
            // Do nothing.
        }

        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        $resp = [];
        /** @var FormReferenceEntityInterface $target */
        $target = $this->getTarget($value);
        if ($target) {
            $resp = $target->formatExtraJson($forceString);
        }

        return $resp;
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'string' === gettype($value);
    }
}
