<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\Country;
use App\Entity\File;
use App\Forms\Annotation\FieldType;
use App\Forms\FormReferenceEntityInterface;
use App\Repository\FileRepository;
use App\Tools\Tools;
use App\Traits\GetContainerTrait;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "file_reference",
 *     label = "File reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "allow_extension": "Comma separated list of valid extensions",
 *         "maximum_file_size": "Maximum file size, by default, use the server settings",
 *     }
 * )
 */
class FileReferenceEntity extends EntityReferenceFieldType
{
    use GetContainerTrait;

    protected FileRepository $fileRepository;

    /**
     * Instantiate with required dependencies injected.
     *
     * @param ContainerInterface $container Service container.
     *
     * @return $this
     */
    public static function instantiate(ContainerInterface $container): mixed
    {
        $instance = new static();
        $instance->registry = $container->get('doctrine');
        $instance->fileRepository = $instance->registry->getRepository(File::class);

        return $instance;
    }

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $value = parent::getProperties();
        $value['class'] = 'App\Entity\File';
        $value['filename'] = '';

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $countryRepo = $this->registry->getRepository(Country::class);
        /** @var Country $hnCountry */
        $hnCountry = $countryRepo->find('hn');
        /** @var FileRepository $fileRepo */
        $fileRepo = $this->registry->getRepository(File::class);
        $file = $fileRepo->create($this->getKernelInstance()->getProjectDir().'/assets/flags/hn.png', $hnCountry, false);

        return [
            'value' => $file->getId(),
            'class' => File::class,
            'filename' => $file->getFileName(),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'allow_extension' => ['*'],
                'maximum_file_size' => Tools::phpIniToBytes(ini_get('upload_max_filesize')),
            ],
        );
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return true;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return true;
    }

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string
    {
        return $this->getId().'_filename';
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        $columns[] = sprintf(
            '%s_value binary(16) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );
        $columns[] = sprintf(
            '%s_class TEXT(255) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' class'
        );
        $columns[] = sprintf(
            '%s_filename TEXT(255) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' filename'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        if ($value instanceof File) {
            $aux = [];
            $aux['class'] = 'App\Entity\File';
            $aux['value'] = $value->getId();
            $aux['filename'] = $value->getFileName();
            $value = $aux;
        }

        if (!parent::isValidProperties($value, $row)) {
            return false;
        }

        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
            if (!empty($value["value"])) {
                $file = $this->fileRepository->find($value["value"]);
                if (!$file) {
                    $ulid = Ulid::fromString($value["value"]);
                    $this->lastErrors[] = $this->t(
                        '[@type] File "@fileId" not found, field "@id" requires values of type "App\Entity\File".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@fileId' => $ulid->toBase32(),
                        ]
                    );

                    return false;
                }
                $value['filename'] = $file->getFileName();
            }
        } elseif (is_null($value)) {
            $aux = [];
            $aux['class'] = 'App\Entity\File';
            $aux['value'] = '';
            $aux['filename'] = '';
            $value = $aux;
        } else {
            $aux = [];
            if (!$value instanceof File) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\File".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                );

                return false;
            }
            /** @var File $value */
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $aux['filename'] = $value->getFileName();
            $value = $aux;
        }

        // Have filename?
        if (!empty($value['filename'])) {
            $file = $this->fileRepository->find($value["value"]);
            // Allow any file extension?
            if (!in_array('*', $this->settings['settings']['allow_extension'])) {
                $fileExtension = strtolower(pathinfo($file->getFileName(), PATHINFO_EXTENSION));
                if (!in_array($fileExtension, $this->settings['settings']['allow_extension'])) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" file extension not valid, must be @exts. File removed.',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                            '@exts' => implode(', ', $this->settings['settings']['allow_extension']),
                        ]
                    );

                    // Remove the file.
                    $this->fileRepository->removeNow($file);

                    return false;
                }
            }
            // Limit file size.
            if ($this->settings['settings']['maximum_file_size'] > 0) {
                if ($file->getSize() > $this->settings['settings']['maximum_file_size']) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" file size is too big, @size, must be less than @max. File removed.',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                            '@size' => Tools::formatBytes($file->getSize()),
                            '@max' => Tools::formatBytes($this->settings['settings']['maximum_file_size']),
                        ]
                    );

                    // Remove the file.
                    $this->fileRepository->removeNow($file);

                    return false;
                }
            }
        }

        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $value = $data;
        if (is_object($value)) {
            if (!$value instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            if (!$value instanceof File) {
                throw new \Exception(
                    $this->t(
                        '[@type] Field "@id" requires values of type "App\Entity\File".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            /** @var File $value */
            $aux['class'] = 'App\Entity\File';
            $ulid = Ulid::fromString($value->getId());
            $aux['value'] = $ulid->toBinary();
            $aux['filename'] = $value->getFileName();
            $data = $aux;
        }
        if (empty($value['class']) && empty($value['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_class'] = '';
            $data[$this->getId().'_filename'] = '';
        } else {
            $ulid = Ulid::fromString($data['value']);
            $data[$this->getId().'_value'] = $ulid->toBinary();
            $reflectionClass = new ReflectionClass($data['class']);
            if ($data['class'] === 'App\Entity\File' || $reflectionClass->isSubclassOf('App\Entity\File')) {
                $data[$this->getId().'_class'] = 'App\Entity\File';
            } else {
                $data[$this->getId().'_class'] = $data['class'];
            }
            // Instance File to get file name.
            /** @var File $file */
            $file = $this->registry->getRepository($data['class'])->find($data['value']);
            if ($file) {
                $data[$this->getId().'_filename'] = $file->getFileName();
            } else {
                $data[$this->getId().'_filename'] = '';
            }
        }
        unset($data['value']);
        unset($data['class']);
        unset($data['filename']);
        // Force entity type.
        if (!empty($data[$this->getId().'_class']) && $data[$this->getId().'_class'] !== 'App\Entity\File') {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\File".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                )
            );
        }
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        if (!class_exists($value['class'], true)) {
            return null;
        }

        return $this->registry->getRepository($value['class'])->find($value['value']);
    }

    /**
     * Field source.
     *
     * This method allow to this field get extra information from the source instance.
     *
     * @param mixed $value Field data source, ex. a user instance.
     *
     * @return mixed
     */
    public function setSource(mixed $value): mixed
    {
        if (is_object($value)) {
            /** @var File $value */
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $aux['filename'] = $value->getFileName();
            $value = $aux;
        } elseif (isset($value["value"]) && is_object($value["value"])) {
            $value['filename'] = $value["value"]->getFileName();
        } elseif (is_null($value) || (isset($value["value"]) && is_null($value["value"]))) {
            $value['class'] = '';
            $value['value'] = '';
            $value['filename'] = '';
        } elseif (is_array($value)) {
            // Do nothing.
        }

        return $value;
    }

    /**
     * Valid extensions list.
     *
     * @return string[]
     */
    public function getAllowedExtensions(): array
    {
        if (!isset($this->settings["settings"]["allow_extension"])) {
            return ['*'];
        }

        return $this->settings["settings"]["allow_extension"];
    }

    /**
     * Maximum file size.
     *
     * @return int
     */
    public function getMaximumFileSize(): int
    {
        if (!isset($this->settings["settings"]["maximum_file_size"])) {
            return Tools::phpIniToBytes(ini_get('upload_max_filesize'));
        }

        return $this->settings["settings"]["maximum_file_size"];
    }
}
