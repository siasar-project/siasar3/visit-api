<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\InjectableObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Form record reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "form_record_reference",
 *     label = "Form record reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *     }
 * )
 */
class FormRecordReferenceFieldType extends AbstractBaseTargetFieldType implements InjectableObjectInterface
{
    protected FormFactory $formFactory;

    /**
     * Instantiate with required dependencies injected.
     *
     * @param ContainerInterface $container Service container.
     *
     * @return $this
     */
    public static function instantiate(ContainerInterface $container): mixed
    {
        $instance = new static();
        $instance->formFactory = $container->get('form_factory');

        return $instance;
    }

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => 0,
            'form' => '',
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if (!$value) {
            return true;
        }

        if ($this->isMultivalued()) {
            foreach ($value as $item) {
                if (!empty($item['value'])) {
                    return false;
                }
            }

            return true;
        }

        return empty($value['value']) || '00000000000000000000000000' === $value['value'];
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => '',
            'form' => '',
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $ulid = new Ulid();

        return [
            'value' => $ulid->toBase32(),
            'form' => $this->formManager->getId(),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        $columns[] = sprintf(
            '%s_value binary(16) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );
        $columns[] = sprintf(
            '%s_form TEXT(255) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' form ID'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
            if (empty($value)) {
                $aux['form'] = '';
                $aux['value'] = '';
                $value = $aux;
            }
        } elseif (is_null($value)) {
            $aux['form'] = '';
            $aux['value'] = '';
            $value = $aux;
        } elseif (!$value instanceof FormRecord) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is not an FormRecord, it\'s an "@class".',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@class' => !is_object($value) ? gettype($value) : get_class($value),
                ]
            );

            return false;
        } else {
            $aux = [];
            if (!$value instanceof FormReferenceEntityInterface) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                    ]
                );

                return false;
            }
            $aux['form'] = $value->getForm()->getId();
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // We need save value and form.
        if ('00000000000000000000000000' === $value['value']) {
            $value['value'] = '';
            $value['form'] = '';
        }
        if (empty($value['value']) && !empty($value['form'])) {
            $value['form'] = '';
        }
        if (empty($value['value']) xor empty($value['form'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is uncompleted. "value" or "form" is empty.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }

        // The form id must be valid.
        if (!empty($value['form'])) {
            $form = $this->formFactory->find($value['form']);
            if (!$form) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" required a installed form, "@form" don\'t exist. ',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@form' => $value['form'],
                    ]
                );

                return false;
            }
            if (!$form->isInstalled()) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" required a installed form, "@form" don\'t installed. ',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@form' => $value['form'],
                    ]
                );

                return false;
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if ($data instanceof FormRecord) {
            if (!$data instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            $aux['form'] = $data->getForm()->getId();
            $aux['value'] = $data->getId();
            $data = $aux;
        } elseif (is_object($data)) {
            throw new \Exception(
                $this->t(
                    'Field "@id" requires values of type FormRecord, but get "@class" class.',
                    [
                        '@id' => $this->getId(),
                        '@class' => get_class($data),
                    ]
                )
            );
        }
        if (empty($data['form']) && empty($data['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_form'] = '';
        } else {
            $ulid = Ulid::fromString($data['value']);
            $data[$this->getId().'_value'] = $ulid->toBinary();
            $data[$this->getId().'_form'] = $data['form'];
        }
        unset($data['value']);
        unset($data['form']);
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        if (!empty($value) && 'value' === $property) {
            $ulid = Ulid::fromString($value);
            $value = $ulid->toBase32();
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        if ($forceString) {
            $ulid = Ulid::fromString($value['value']);
            $value['value'] = $ulid->toBase32();
        }

        return $value;
    }

    /**
     * Field target.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        $form = $this->formFactory->find($value['form'], false);
        if (!$form) {
            return null;
        }

        return $form->find($value['value']);
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        $resp = [];
        /** @var FormRecord $target */
        $target = $this->getTarget($value);
        if ($target) {
            $resp = $target->formatExtraJson($forceString);
        }

        return $resp;
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'double' === gettype($value);
    }
}
