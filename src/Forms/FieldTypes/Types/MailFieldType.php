<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;

/**
 * Mail field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "mail",
 *     label = "Mail field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class MailFieldType extends AbstractBaseFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => '',
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if (!$value) {
            return true;
        }

        if ($this->isMultivalued()) {
            foreach ($value as $item) {
                if (!empty($item['value'])) {
                    return false;
                }
            }

            return true;
        }

        return empty($value);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => '',
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $starts = ['A', 'B', 'C', 'D'];

        return [
            'value' => 'aaaaa976+'.$starts[rand(0, 3)].'@gmail.com',
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        return sprintf(
            '%s TEXT %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label'])
        );
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }
        // Is valid email?
        if (!empty($value['value']) && !filter_var($value['value'], FILTER_VALIDATE_EMAIL)) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" have not a valid mail: "@mail".',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@mail' => $value['value'],
                ]
            );

            return false;
        }
        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $data = $data['value'];
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'string' === gettype($value);
    }
}
