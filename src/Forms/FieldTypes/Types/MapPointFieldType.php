<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;

/**
 * Map point field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "map_point",
 *     label = "Map point field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "This field type can't be multivalued.",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "lat_field": "The Latitude field",
 *         "lon_field": "The Longitude field",
 *     }
 * )
 */
class MapPointFieldType extends AbstractBaseFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value.
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'lat' => 0,
            'lon' => 0,
        ];
    }

    /**
     * Get the main property key.
     *
     * This property will be used to sort or filter queries.
     *
     * @return string
     */
    public function getPrimaryProperty(): string
    {
        return 'lat';
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return $this->getProperties();
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return [
            'lat' => rand(0, 33),
            'lon' => rand(-33, 0),
        ];
    }

    /**
     * Set field settings.
     *
     * @param array $settings Field settings.
     *
     * @return self
     */
    public function setSettings(array $settings): self
    {
        if (isset($settings['settings']['multivalued']) && boolval($settings['settings']['multivalued'])) {
            throw new \Exception($this->t('An "@type" field type can\'t be multivalued.', ['@type' => $settings['type']]));
        }
        parent::setSettings($settings);

        return $this;
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'lat_field' => '',
                'lon_field' => '',
            ],
        );
    }

    /**
     * Get field meta.
     *
     * @return string
     */
    public function getMeta(): array
    {
        // This field type always is disabled, readonly.
        if (!isset($this->settings['settings']['meta']['disabled'])) {
            $this->settings['settings']['meta']['disabled'] = true;
        }

        return $this->settings['settings']['meta'];
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        // Column definition.
        return '';
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isValidSettings(): bool
    {
        if (!isset($this->settings["settings"]["lat_field"])) {
            $this->settings["settings"]["lat_field"] = '';
        }
        if (!isset($this->settings["settings"]["lon_field"])) {
            $this->settings["settings"]["lon_field"] = '';
        }

        // Lat field validation.
        $countryField = $this->formManager->getFieldDefinition($this->settings["settings"]["lat_field"]);
        if (!$countryField) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require "@country" field in form "@form", but it\'s not found.',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->settings["settings"]["lat_field"],
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }
        if ($countryField->isMultivalued()) {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" can\'t use a multivalued field how country reference, "@ref".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                        '@ref' => $this->settings["settings"]["lat_field"],
                    ]
                )
            );
        }
        // Lon field validation.
        $countryField = $this->formManager->getFieldDefinition($this->settings["settings"]["lon_field"]);
        if (!$countryField) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require "@country" field in form "@form", but it\'s not found.',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->settings["settings"]["lon_field"],
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }
        if ($countryField->isMultivalued()) {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" can\'t use a multivalued field how country reference, "@ref".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                        '@ref' => $this->settings["settings"]["lon_field"],
                    ]
                )
            );
        }

        return parent::isValidSettings();
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        // This field type don't have database column.
        $data = [];
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return false;
    }
}
