<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;
use App\Tools\FormattableMarkup;

/**
 * Long text field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "month_year",
 *     label = "Month and year field type",
 *     description = "Month are from 1, that is January, to 12, that is December. The years are from 1900 to 9999.",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *     }
 * )
 */
class MonthYearFieldType extends AbstractBaseFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'month' => '',
            'year' => '',
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if ((isset($value["year"]) && empty($value["year"])) || is_null($value["year"])) {
            return true;
        }

        if ($this->isMultivalued()) {
            return count($value) > 0;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'month' => 1,
            'year' => 1900,
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        return [
            'month' => mt_rand(1, 12),
            'year' => mt_rand(2020, 2050),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [],
        );
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        if (empty($value)) {
            return null;
        }

        return $value;
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        $columns[] = sprintf(
            '%s_month int %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label'])
        );
        $columns[] = sprintf(
            '%s_year int %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label'])
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }

        // We need save value and class.
        if (empty($value['month']) xor empty($value['year'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is uncompleted. "month" or "year" is empty.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }

        // Validate ranges.
        if (!empty($value['month'])) {
            $v = $value['month'];
            if ($v < 1 || $v > 12) {
                $this->lastErrors[] = $this->t(
                    '[@type] Wrong month, "@month", in field "@id". Values must be between 1 and 12.',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@month' => $v,
                    ]
                );

                return false;
            }
        }
        if (!empty($value['year'])) {
            $v = $value['year'];
            if ($v < 1900 || $v > 9999) {
                $this->lastErrors[] = $this->t(
                    '[@type] Wrong year, "@year", in field "@id". Values must be between 1900 and 9999.',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@year' => $v,
                    ]
                );

                return false;
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['month'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $data[$this->getId().'_month'] = isset($data['month']) ? $data['month'] : null;
        $data[$this->getId().'_year'] = isset($data['year']) ? $data['year'] : null;

        unset($data['month']);
        unset($data['year']);
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'integer' === gettype($value);
    }
}
