<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;
use App\Traits\InjectableObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\Workflow;

/**
 * Workflow field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "publishing_status",
 *     label = "Publishing status field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "This field type can't be multivalued.",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class PublishingStatusFieldType extends AbstractBaseFieldType implements InjectableObjectInterface
{
    public const WORKFLOW_NAME = 'publishing';

    protected ?Workflow $workflow;

    /**
     * Instantiate with required dependencies injected.
     *
     * @param ContainerInterface $container Service container.
     *
     * @return $this
     */
    public static function instantiate(ContainerInterface $container): mixed
    {
        $instance = new static();
        $instance->workflow = $container->get('workflow.registry')->get($instance, static::WORKFLOW_NAME);

        return $instance;
    }

    /**
     * Get current workflow place.
     *
     * @return string
     */
    public function getMarking(): string
    {
        // Are we in a form context?
        if (is_null($this->record)) {
            // No, return default value.
            return $this->getProperties()['value'];
        }
        $original = $this->record->getOriginal();

        return $original[$this->fieldName]['value'];
    }

    /**
     * Sets a Marking to a subject.
     *
     * @param string $marking
     * @param array  $context
     */
    public function setMarking(string $marking, array $context = [])
    {
        // Ignore this method.
    }

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => 'draft',
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => 'draft',
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        $columns[] = sprintf(
            '%s TEXT(255) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' class'
        );

        return implode(', ', $columns);
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }

        // Changed value?
        $marking = $this->getMarking();
        if ($value["value"] !== $marking) {
            if ($this->record) {
                $transitionName = $this->getTransitionName($marking, $value["value"]);
                // Can we change the workflow place?
                if (!$this->workflow->can($this, $transitionName)) {
                    $allowedTransitions = $this->workflow->getEnabledTransitions($this);
                    $allowed = [];
                    foreach ($allowedTransitions as $transition) {
                        $allowed = array_merge($allowed, $transition->getTos());
                    }
                    $this->lastErrors[] = $this->t(
                        '[@type\"@id"] This form can\'t change to "@value" from "@from" status. Allowed values are: @allowed',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@value' => $value['value'],
                            '@from' => $marking,
                            '@allowed' => implode(', ', $allowed),
                        ]
                    );

                    return false;
                }
                // We must dispatch guards events.
                $context = ['validation_stage' => 'workflow_apply'];
                $this->workflow->apply($this, $transitionName, $context);
            } else {
                $value['value'] = $this->getProperties()['value'];
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $data = $data['value'];
    }

    /**
     * Set field settings.
     *
     * @param array $settings Field settings.
     *
     * @return self
     */
    public function setSettings(array $settings): self
    {
        if (isset($settings['settings']['multivalued']) && boolval($settings['settings']['multivalued'])) {
            throw new \Exception($this->t('An "@type" field type can\'t be multivalued.', ['@type' => $settings['type']]));
        }
        parent::setSettings($settings);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        $allowedTransitions = $this->workflow->getEnabledTransitions($this);
        $allowed = [];
        foreach ($allowedTransitions as $transition) {
            $allowed = array_merge($allowed, $transition->getTos());
        }

        return [
            'allowed' => $allowed,
        ];
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'string' === gettype($value);
    }

    /**
     * Get transition name by source / destination, or false if not transition enabled.
     *
     * @param string $from Transition source.
     * @param string $to   Transition destination.
     *
     * @return string
     */
    protected function getTransitionName(string $from, string $to): string
    {
        /**
         * Valid transitions.
         *
         * @var Transition[] $transitions
         */
        $transitions = $this->workflow->getEnabledTransitions($this);
        foreach ($transitions as $transition) {
            if (in_array($from, $transition->getFroms()) && in_array($to, $transition->getTos())) {
                return $transition->getName();
            }
        }

        return false;
    }
}
