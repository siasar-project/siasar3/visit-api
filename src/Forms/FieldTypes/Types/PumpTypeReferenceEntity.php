<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\PumpType;
use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\EntityCountryReferenceFieldTrait;
use ReflectionClass;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "pump_type_reference",
 *     label = "Pump type reference field type.",
 *     description = "Requires a country field in same form",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *         "country_field": "The country reference field",
 *     }
 * )
 */
class PumpTypeReferenceEntity extends EntityReferenceFieldType
{
    use EntityCountryReferenceFieldTrait;

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $value = parent::getProperties();
        $value['class'] = PumpType::class;

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        if (isset($row[$this->getCountryFieldName()]['value'])) {
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                /** @var Country $country */
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if ($country) {
                    $pumpType = $country->getPumpTypes()->first();
                    if ($pumpType) {
                        return [
                            'value' => $pumpType->getId(),
                            'class' => PumpType::class,
                        ];
                    }
                }
            }
        }

        $pumpTypesRepo = $this->registry->getRepository(PumpType::class);
        $pumpTypes = $pumpTypesRepo->findBy(['country' => 'hn'], [], 100, 0);
        $pumpTypeId = '';
        if (count($pumpTypes) > 0) {
            $pumpType = $pumpTypes[rand(0, count($pumpTypes) - 1)];
            $pumpTypeId = $pumpType->getId();
        }

        return [
            'value' => $pumpTypeId,
            'class' => PumpType::class,
        ];
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        parent::isValidSettings();

        $this->isValidSettingsTrait();

        return true;
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return false;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return false;
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }

        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (is_null($value)) {
            $aux = [];
            $aux['class'] = PumpType::class;
            $aux['value'] = '';
            $value = $aux;
        } else {
            $aux = [];
            if (!$value instanceof PumpType) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\PumpType".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                    ]
                );

                return false;
            }
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // If not empty value.
        if ('' !== $value['value']) {
            // Get country field value.
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isMultivalued()) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" can\'t use a multivalued field how country reference, "@ref".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@ref' => $this->getCountryFieldName(),
                    ]
                );

                return false;
            }
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                // Validate PumpType with the selected country.
                $pumpTypeRepo = $this->registry->getRepository($value['class']);
                /** @var PumpType $pumpType */
                $pumpType = $pumpTypeRepo->find($value['value']);
                if (!$pumpType) {
                    // PumpType not found.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a wrong reference: "@ref".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@ref' => $value['value'],
                        ]
                    );

                    return false;
                }
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if (!$country) {
                    // Country not found.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a wrong reference: "@ref".',
                        [
                            '@type' => $countryField->getFieldType(),
                            '@id' => $countryField->getLabelExtended(),
                            '@ref' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
                if ($pumpType->getCountry()->getId() !== $country->getId()) {
                    // Pump type not into country.
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" have a reference, "@ref", outside of country "@country".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@ref' => $value['value'],
                            '@country' => $row[$this->getCountryFieldName()]['value'],
                        ]
                    );

                    return false;
                }
            } else {
                return false;
            }
        }

        // Data it's valid.
        return true;
    }
}
