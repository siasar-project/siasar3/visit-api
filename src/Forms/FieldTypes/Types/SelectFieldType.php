<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseFieldType;

/**
 * Select field type.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "select",
 *     label = "Select field type",
 *     description = "We can add option groups using keys that start with '_'.",
 *     settings = {
 *         "options": "Allowed options",
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class SelectFieldType extends AbstractBaseFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        return [
            'value' => '',
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if (!$value) {
            return true;
        }

        if ($this->isMultivalued()) {
            foreach ($value as $item) {
                if (!empty($item)) {
                    return false;
                }
            }

            return true;
        }

        return empty($value);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return [
            'value' => '',
        ];
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        if (count($this->settings["settings"]["options"]) === 0) {
            return ['value' => ''];
        }
        $keys = array_keys($this->settings["settings"]["options"]);
        $max = count($keys);

        return [
            'value' => $keys[rand(0, $max - 1)],
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'options' => [],
            ],
        );
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        if (parent::isValidSettings()) {
            $cntOpts = 0;
            foreach (array_keys($this->settings["settings"]["options"]) as $option) {
                if (!str_starts_with($option, '_')) {
                    $cntOpts++;
                }
            }
            if (0 === $cntOpts) {
                throw new \Exception(
                    $this->t(
                        'The select field "@id" requires at least one option to select.',
                        [
                            '@id' => $this->getLabelExtended(),
                        ]
                    )
                );
            }
        }

        return true;
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        return sprintf(
            '%s TEXT %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label'])
        );
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values.
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }
        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Is selected a valid option?
        if (!in_array($value['value'], $this->getValidOptions($row))) {
            $validOptions = $this->getValidOptions($row);
            foreach ($validOptions as &$option) {
                $option = '`'.$option.'`';
            }
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" have a invalid option, "@value". Options are: @options.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@value' => $value['value'],
                    '@options' => implode(', ', $validOptions),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if (is_array($data)) {
            $data = trim($data['value']);
        } elseif (is_string($data)) {
            $data = trim($data);
        }
    }

    /**
     * Get valid options.
     *
     * @param mixed $row Complete row values.
     *
     * @return array
     */
    public function getValidOptions(mixed &$row): array
    {
        $validOptions = array_keys($this->settings["settings"]["options"]);
        if (!$this->isRequired()) {
            $validOptions[] = '';
        }

        return $validOptions;
    }

    /**
     * Get options with labels.
     *
     * @return array
     */
    public function getOptions(): array
    {
        return $this->settings["settings"]["options"];
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'string' === gettype($value);
    }
}
