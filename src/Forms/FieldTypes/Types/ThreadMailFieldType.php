<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\FormReferenceEntityInterface;
use App\Forms\MailFormManager;
use App\Forms\SubformFormManager;
use App\Traits\EntityCountryReferenceFieldTrait;
use App\Traits\InjectableObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Form record reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "thread_mail_reference",
 *     label = "Mail response record reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "country_field": "The country reference field",
 *         "subform": "The subform id",
 *     }
 * )
 */
class ThreadMailFieldType extends FormRecordReferenceFieldType implements InjectableObjectInterface
{
    use EntityCountryReferenceFieldTrait;

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $resp = parent::getProperties();
        $resp['form'] = '';

        return $resp;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $form = $this->formFactory->find($this->getSubformId());
        // Try to find a record inside the record country.
        if (isset($row[$this->getCountryFieldName()]['value'])) {
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                /** @var Country $country */
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if ($country) {
                    /** @var FormRecord $record */
                    $record = current($form->findBy(
                        [
                            'field_country' => $country->getId(),
                        ],
                        [],
                        1
                    ));
                    if ($record) {
                        return [
                            'value' => $record->getId(),
                            'form' => $this->getSubformId(),
                        ];
                    }
                }
            }
        }
        // If no record found then use a fake ULID.
        $ulid = new Ulid();
        // Try to find any record.
        if ($form) {
            /** @var FormRecord $record */
            $record = current($form->findBy([], [], 1));
            if ($record) {
                $ulid = Ulid::fromString($record->getId());
            }
        }

        return [
            'value' => $ulid->toBase32(),
            'form' => $this->getSubformId(),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        $settings = $this->getDefaultSettingsFromTrait();
        $settings['subform'] = '';

        return $settings;
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        parent::isValidSettings();

        if (empty($this->getSubformId())) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require a subform in form "@form".',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }

        $subform = $this->formFactory->find($this->getSubformId());
        if ($subform->getType() !== $this->formManager->getType() && (!$subform || !$subform->isInstalled())) {
            throw new \Exception(
                $this->t(
                    'The field "@id", in form "@form", require the "@subformId" form, but don\'t exist or it\'s not installed.',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@form' => $this->formManager->getId(),
                        '@subformId' => $this->getSubFormId(),
                    ]
                )
            );
        }
        if ($subform->getType() !== MailFormManager::class) {
            throw new \Exception(
                $this->t(
                    'Field "@id", in form "@form": The "@subformId" sub form must be managed by "@subformClass".',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@form' => $this->formManager->getId(),
                        '@subformId' => $this->getSubFormId(),
                        '@subformClass' => MailFormManager::class,
                    ]
                )
            );
        }
        $subMeta = $subform->getMeta();
        if (!isset($subMeta['parent_form'])) {
            throw new \Exception(
                $this->t(
                    'Field "@id", in form "@form": The "@subformId" sub form must have a "parent_form" meta value, and must have this form id how value.',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@form' => $this->formManager->getId(),
                        '@subformId' => $this->getSubFormId(),
                        '@subformClass' => MailFormManager::class,
                    ]
                )
            );
        }
//        if ($subMeta['parent_form'] !== $this->getFormManager()->getId()) {
//            throw new \Exception(
//                $this->t(
//                    'Field "@id", in form "@form": The "@subformId" sub form must have this form ID in "parent_form" meta value.',
//                    [
//                        '@id' => $this->getLabelExtended(),
//                        '@form' => $this->formManager->getId(),
//                        '@subformId' => $this->getSubFormId(),
//                        '@subformClass' => MailFormManager::class,
//                    ]
//                )
//            );
//        }

        $this->isValidSettingsTrait();

        return true;
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
            // If it has a binary ID convert to base32.
            $ulid = Ulid::fromString($value['value']);
            $value['value'] = $ulid->toBase32();
        } elseif (is_null($value)) {
            $aux['form'] = '';
            $aux['value'] = '';
            $value = $aux;
        } elseif (!$value instanceof FormRecord) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is not an FormRecord, it\'s an "@class".',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@class' => !is_object($value) ? gettype($value) : get_class($value),
                ]
            );

            return false;
        } else {
            $aux = [];
            if (!$value instanceof FormReferenceEntityInterface) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                    ]
                );

                return false;
            }
            $aux['form'] = $this->getSubformId();
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // We need save value and form.
        if (empty($value['value']) xor empty($value['form'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is uncompleted. "value" or "form" is empty.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }

        // The form id must be valid.
        if (!empty($value['form'])) {
            $form = $this->formFactory->find($value['form']);
            if (!$form) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" required a installed form, "@form" don\'t exist. ',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@form' => $value['form'],
                    ]
                );

                return false;
            }
            if (!$form->isInstalled()) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" required a installed form, "@form" don\'t installed. ',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@form' => $value['form'],
                    ]
                );

                return false;
            }
            if (!empty($value['value'])) {
                // If reference a record, it must exist and be valid.
                $record = $form->findBy(
                    [
                        'id' => $value['value'],
                    ],
                    ['field_changed' => 'desc']
                );
                if (!$record) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" require a valid reference to "@referred_form_id", the ID "@record" don\'t exist.',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                            '@record' => $value['value'],
                            '@referred_form_id' => $this->getSubformId(),
                        ]
                    );

                    return false;
                }
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if ($data instanceof FormRecord) {
            if (!$data instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getLabelExtended(),
                        ]
                    )
                );
            }
            $aux['form'] = $this->getSubformId();
            $aux['value'] = $data->getId();
            $data = $aux;
        } elseif (is_object($data)) {
            throw new \Exception(
                $this->t(
                    'Field "@id" requires values of type FormRecord, but get "@class" class.',
                    [
                        '@id' => $this->getLabelExtended(),
                        '@class' => get_class($data),
                    ]
                )
            );
        }
        if (empty($data['form']) && empty($data['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_form'] = '';
        } else {
            $ulid = Ulid::fromString($data['value']);
            $data[$this->getId().'_value'] = $ulid->toBinary();
            $data[$this->getId().'_form'] = $this->getSubformId();
        }
        unset($data['value']);
        unset($data['form']);
    }

    /**
     * Get subform ID.
     *
     * @return string
     */
    public function getSubFormId(): string
    {
        return $this->settings["settings"]["subform"];
    }

    /**
     * {@inheritDoc}
     */
    public function dropContent(): self
    {
        $myData = $this->record->{$this->getId()};
        $sub = $this->formFactory->find($this->getSubFormId());
        if ($this->isMultivalued()) {
            /** @var FormRecord $data */
            foreach ($myData as $data) {
                $sub->drop($data->getId());
            }
        } else {
            /** @var FormRecord $myData */
            $sub->drop($myData->getId());
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function cloneContent(): mixed
    {
        $resp = [];
        if ($this->isMultivalued()) {
            $records = $this->record->{$this->getId()};
            /** @var FormRecord $record */
            foreach ($records as $record) {
                $clone = $record->clone();
                $resp[] = [
                    'value' => $clone->getId(),
                    'form' => $clone->getForm()->getId(),
                ];
            }
        } else {
            $record = $this->record->{$this->getId()};
            $clone = $record->clone();
            $resp = [
                'value' => $clone->getId(),
                'form' => $clone->getForm()->getId(),
            ];
        }

        return $resp;
    }
}
