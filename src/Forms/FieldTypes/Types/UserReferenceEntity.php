<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\User;
use App\Forms\Annotation\FieldType;
use App\Forms\FormReferenceEntityInterface;
use ReflectionClass;
use Symfony\Component\Uid\Ulid;

/**
 * Entity reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "user_reference",
 *     label = "User reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class UserReferenceEntity extends EntityReferenceFieldType
{

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $value = parent::getProperties();
        $value['class'] = 'App\Entity\User';

        return $value;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $userRepo = $this->registry->getRepository(User::class);
        /** @var User[] $users */
        $users = $userRepo->findBy([], [], 1);
        $userId = '';
        if (count($users) > 0) {
            $user = current($users);
            $userId = $user->getId();
        }

        return [
            'value' => $userId,
            'class' => User::class,
        ];
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return true;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return true;
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        if (!parent::isValidProperties($value, $row)) {
            return false;
        }

        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (is_null($value)) {
            $aux = [];
            $aux['class'] = 'App\Entity\User';
            $aux['value'] = '';
            $value = $aux;
        } else {
            $aux = [];
            if (!$value instanceof User) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\User".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                    ]
                );

                return false;
            }
            $aux['class'] = get_class($value);
            $aux['value'] = $value->getId();
            $value = $aux;
        }

        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        $value = $data;
        if (is_object($value)) {
            if (!$value instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            if (!$value instanceof User) {
                throw new \Exception(
                    $this->t(
                        '[@type] Field "@id" requires values of type "App\Entity\User".',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            $aux['class'] = 'App\Entity\User';
            $aux['value'] = $value->getId();
            $data = $aux;
        }
        if (empty($value['class']) && empty($value['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_class'] = '';
        } else {
            $ulid = Ulid::fromString($data['value']);
            $data[$this->getId().'_value'] = $ulid->toBinary();
            $reflectionClass = new ReflectionClass($data['class']);
            if ($data['class'] === 'App\Entity\User' || $reflectionClass->isSubclassOf('App\Entity\User')) {
                $data[$this->getId().'_class'] = 'App\Entity\User';
            } else {
                $data[$this->getId().'_class'] = $data['class'];
            }
            $data[$this->getId().'_class'] = $data['class'];
            if (empty($data['value'])) {
                $data[$this->getId().'_value'] = '';
                $data[$this->getId().'_class'] = '';
            }
        }
        unset($data['value']);
        unset($data['class']);
        // Force entity type.
        if (!empty($data[$this->getId().'_class']) && $data[$this->getId().'_class'] !== 'App\Entity\User') {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" requires values of type "App\Entity\User".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                )
            );
        }
    }
}
