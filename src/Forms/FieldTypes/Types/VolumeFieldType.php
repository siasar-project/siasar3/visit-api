<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Tools\Measurements\VolumeMeasurement;
use App\Traits\GetContainerTrait;

/**
 * Volume field type.
 *
 * Date information must be saved in cubic metres and read in input units.
 * The input unit will be saved in the unit field in database.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "volume",
 *     label = "Volume field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "unknowable": "Can this field be unknow? If TRUE will be unknow if value is -9999999.",
 *     }
 * )
 */
class VolumeFieldType extends AbstractBaseTargetFieldType
{
    use GetContainerTrait;

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $def = new VolumeMeasurement();

        return [
            'value' => $def->getValue(),
            'unit' => $def->getUnit(),
        ];
    }

    /**
     * @inheritDoc
     */
    public function isEmpty(): bool
    {
        if (!$this->record) {
            return true;
        }

        $value = $this->record->get($this->getId());

        if (!$value) {
            return true;
        }

        if ($this->isMultivalued()) {
            foreach ($value as $item) {
                if (!empty($item['unit'])) {
                    return false;
                }
            }

            return true;
        }

        return empty($value['unit']);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultValue(): array
    {
        return $this->getProperties();
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $def = new VolumeMeasurement(rand(0, 100));

        return [
            'value' => (string) $def->getValue(),
            'unit' => $def->getUnit(),
        ];
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return array_merge(
            parent::getDefaultSettings(),
            [
                'unknowable' => false,
            ],
        );
    }

    /**
     * Get SQL table field definitions.
     *
     * @return string
     */
    public function getSqlFieldDefinitions(): string
    {
        $columns = [];
        // TODO Save original data too?
        $columns[] = sprintf(
            '%s_value DECIMAL(40,14) %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' value'
        );
        $columns[] = sprintf(
            '%s_unit TEXT %s COMMENT \'%s\'',
            $this->getId(),
            '',
            // ($this->isRequired() ? 'NOT NULL' : ''),
            str_replace("'", '"', $this->settings['label']).' unit'
        );

        return implode(', ', $columns);
    }

    /**
     * Get field configuration requirements.
     *
     * @return string[]
     */
    public function getConfigurationRequirements(): array
    {
        return [
            'system.units.volume',
        ];
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Field types default validations.
        if (is_array($value)) {
            // Array is the default $value format.
            if ($value['value'] instanceof VolumeMeasurement) {
                $value['unit'] = $value['value']->getUnit();
                if (empty($value['unit'])) {
                    $value['value'] = '';
                }
            }
        } elseif (is_null($value)) {
            $value['unit'] = '';
            $value['value'] = '';
        } elseif (!$value instanceof VolumeMeasurement) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is not an VolumeMeasurement, it\'s an "@valuetype".',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@valuetype' => gettype($value),
                ]
            );

            return false;
        } else {
            $aux = [];
            $aux['unit'] = $value->getUnit();
            $aux['value'] = $value;
            $value = $aux;
        }

        if (isset($value['value']) && $value['value'] instanceof VolumeMeasurement) {
            $def = $value['value'];
            $value['unit'] = $def->getUnit();
            $value['value'] = $def->getValue();
        }

        // We need save value and timezone.
        if ('' !== $value['value'] || '' !== $value['unit']) {
            if (!(0.0 === $value['value'] && '' === $value['unit'])) {
                if ((empty($value['value']) && (float) $value['value'] !== 0.0 && (float) $value['value'] !== -9999999.0) xor empty($value['unit'])) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" is uncompleted. "value" or "unit" is empty.',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getLabelExtended(),
                        ]
                    );

                    return false;
                }
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if (empty($data['unit']) || empty($data['value'])) {
            // Allow insert empty fields.
            $data = [
                $this->getId().'_value' => null,
                $this->getId().'_unit' => '',
            ];
        }
        // Convert date to default units.
        if (isset($data['value'])) {
            if (is_numeric($data['value'])) {
                $data['value'] = new VolumeMeasurement($data['value'], $data['unit']);
            }
            $volume = $data['value']->clone();
            $volume->setUnit((new VolumeMeasurement())->getReferenceUnit());
            $data = [
                $this->getId().'_value' => $volume->getValue(),
                $this->getId().'_unit' => $data['unit'],
            ];
        }
    }

    /**
     * @inheritDoc
     */
    public function formatToDatabase(string $property, mixed $value, array $item): mixed
    {
        switch ($property) {
            case 'value':
                if (is_null($value)) {
                    return null;
                }
                $value = new VolumeMeasurement($value);
                $value->setUnit($item['unit']);
                break;

            case 'unit':
                break;
        }

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function formatToUse($value, bool $forceString = false): mixed
    {
        if (empty($value['value'])) {
            return $value;
        }

        if (is_numeric($value['value'])) {
            $value['value'] = new VolumeMeasurement($value['value']);
        }
        if ($value['value'] instanceof VolumeMeasurement) {
            $value['value'] = $value['value']->clone(false);
            $value['value']->setUnit($value['unit']);
            $value['value'] = $value['value']->getValue();
        }

        return $value;
    }

    /**
     * Field target.
     *
     * Return a final data representation to this field content.
     *
     * @param array|null $value Complete record value.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function getTarget(array|null $value): mixed
    {
        if (!$value) {
            return null;
        }

        // Convert to $value['unit'] from default unit.
        $unit = $value['unit'];
        if ($value['value'] instanceof VolumeMeasurement) {
            $volume = $value['value'];
        } elseif (!empty($value['value'])) {
            $volume = new VolumeMeasurement($value['value']);
        } else {
            $volume = new VolumeMeasurement();
        }

        return $volume->setUnit($unit);
    }

    /**
     * Field source.
     *
     * This method allow to this field get extra information from the source instance.
     *
     * @param mixed $value Field data source, ex. a user instance.
     *
     * @return mixed
     */
    public function setSource(mixed $value): mixed
    {
        // Get time zone from value.
        if ($value instanceof VolumeMeasurement) {
            $value = $value->clone();
            $aux['unit'] = $value->getUnit();
            $aux['value'] = $value->setUnit($value->getReferenceUnit());
            $value = $aux;
        } elseif (isset($value["value"]) && $value["value"] instanceof VolumeMeasurement) {
            // Do nothing.
        } elseif (is_null($value) || is_null($value["value"])) {
            $value['unit'] = (new VolumeMeasurement())->getReferenceUnit();
            $value['value'] = 0;
        } elseif (is_array($value)) {
            $volume = new VolumeMeasurement($value['value']);
            $volume->setUnit($value['unit']);
            $value['value'] = $volume;
        }

        return $value;
    }

    /**
     * Set raw value.
     *
     * @param array $value Raw value.
     *
     * @return array
     */
    public function setRaw(mixed $value): array
    {
        if (!is_array($value)) {
            return $value;
        }

        if (!empty($value['value']) && !empty($value['unit'])) {
            $volume = new VolumeMeasurement($value['value'], $value['unit']);
            $value['value'] = $volume;
        }

        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson($value, bool $forceString = false): array
    {
        $resp = [];
        /** @var VolumeMeasurement $target */
        $target = $this->getTarget($value);
        if ($target) {
            $target->setUnit($target->getReferenceUnit());
            $resp['reference_unit'] = $target->getUnit();
            $resp['value'] = $target->getValue();
        }

        return $resp;
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'double' === gettype($value);
    }
}
