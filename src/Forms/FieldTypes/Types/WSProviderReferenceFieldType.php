<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\EntityCountryReferenceFieldTrait;
use App\Traits\InjectableObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Form record reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "wsprovider_reference",
 *     label = "Water service provider form record reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "country_field": "The country reference field",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class WSProviderReferenceFieldType extends FormRecordReferenceFieldType implements InjectableObjectInterface
{
    use EntityCountryReferenceFieldTrait;

    protected const REFERRED_FORM_ID = 'form.wsprovider';

    /**
     * Field type properties.
     *
     * Used to define stored values in this field type how key value and default value how array value..
     *
     * @return array
     */
    public function getProperties(): array
    {
        $resp = parent::getProperties();
        $resp['form'] = static::REFERRED_FORM_ID;

        return $resp;
    }

    /**
     * Get a valid example raw data array.
     *
     * @param array $row
     *   Optional example row generated, to allow this field generates relative values.
     *
     * @return array
     */
    public function getExampleData(array $row = []): array
    {
        $form = $this->formFactory->find(static::REFERRED_FORM_ID);
        // Try to find a record inside the record country.
        if (isset($row[$this->getCountryFieldName()]['value'])) {
            /** @var CountryReferenceEntity $countryField */
            $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
            if ($countryField->isValidProperties($row[$this->getCountryFieldName()], $row)) {
                /** @var Country $country */
                $country = $countryField->getTarget($row[$this->getCountryFieldName()]);
                if ($country) {
                    /** @var FormRecord $record */
                    $record = current($form->findBy(
                        [
                            'field_country' => $country->getId(),
                            'field_deleted' => 0,
                            'field_status' => ['IN' => ['draft', 'finished', 'validated', 'locked']],
                        ],
                        [],
                        1
                    ));
                    if ($record) {
                        return [
                            'value' => $record->getId(),
                            'form' => static::REFERRED_FORM_ID,
                        ];
                    }
                }
            }
        }
        // If no record found then use a fake ULID.
        $ulid = new Ulid();
        // Try to find any record.
        if ($form) {
            /** @var FormRecord $record */
            $record = current($form->findBy([], [], 1));
            if ($record) {
                $ulid = Ulid::fromString($record->getId());
            }
        }

        return [
            'value' => $ulid->toBase32(),
            'form' => static::REFERRED_FORM_ID,
        ];
    }

    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettings(): bool
    {
        parent::isValidSettings();

        $this->isValidSettingsTrait();

        return true;
    }

    /**
     * Validate single field data.
     *
     * @param mixed $value Value in each property.
     * @param mixed $row   Complete row values..
     *
     * @return bool
     */
    public function isValidProperties(mixed &$value, mixed &$row): bool
    {
        // Trust that value and form data are valid.
        $isSafeRecord = false;
        $safeRecord = null;

        // Field types default validations.
        if (is_array($value)) {
            if (!parent::isValidProperties($value, $row)) {
                return false;
            }
        } elseif (is_null($value)) {
            $aux['form'] = '';
            $aux['value'] = '';
            $value = $aux;
        } elseif (!$value instanceof FormRecord) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is not an FormRecord, it\'s an "@class".',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                    '@class' => !is_object($value) ? gettype($value) : get_class($value),
                ]
            );

            return false;
        } else {
            $aux = [];
            if (!$value instanceof FormReferenceEntityInterface) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                    ]
                );

                return false;
            }
            $safeRecord = $value;
            $aux['form'] = static::REFERRED_FORM_ID;
            $ulid = Ulid::fromString($value->getId());
            $aux['value'] = $ulid->toBase32();
            $value = $aux;
            $isSafeRecord = true;
        }

        // We need save value and form.
        if (empty($value['value']) xor empty($value['form'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is uncompleted. "value" or "form" is empty.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getId(),
                ]
            );

            return false;
        }

        // The form id must be valid.
        if (!$isSafeRecord) {
            if (!empty($value['form'])) {
                $form = $this->formFactory->find($value['form']);
                if (!$form) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" required a installed form, "@form" don\'t exist. ',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                            '@form' => $value['form'],
                        ]
                    );

                    return false;
                }
                if (!$form->isInstalled()) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" required a installed form, "@form" don\'t installed. ',
                        [
                            '@type' => $this->getFieldType(),
                            '@id' => $this->getId(),
                            '@form' => $value['form'],
                        ]
                    );

                    return false;
                }
                if (!empty($value['value']) && '00000000000000000000000000' !== $value['value']) {
                    // If reference a record, it must exist and be valid.
                    $ulidValue = Ulid::fromString($value['value']);
                    $record = current($form->findBy(
                        [
                            'id' => $ulidValue->toBinary(),
                            //'field_deleted' => 0,
                            'field_status' => ['IN' => ['draft', 'finished', 'validated', 'locked']],
                        ],
                        ['field_changed' => 'desc'],
                        null,
                        null,
                        ['field_ulid_reference']
                    ));
                    if (!$record) {
                        $this->lastErrors[] = $this->t(
                            '[@type] Field "@id" require a valid reference to "@referred_form_id", the ID "@record" don\'t exist.',
                            [
                                '@type' => $this->getFieldType(),
                                '@id' => $this->getLabelExtended(),
                                '@record' => $ulidValue->toBase32(),
                                '@referred_form_id' => static::REFERRED_FORM_ID,
                            ]
                        );

                        return false;
                    }
                    if ($record->{'field_deleted'}) {
                        $aux['form'] = '';
                        $aux['value'] = '';
                        $value = $aux;
                    }
                }
            }
        } else {
            if (static::REFERRED_FORM_ID !== $safeRecord->getForm()->getId()) {
                $this->lastErrors[] = $this->t(
                    '[@type] Field "@id" require a valid reference to "@referred_form_id", the ID "@record" don\'t exist.',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getLabelExtended(),
                        '@record' => $safeRecord->getId(),
                        '@referred_form_id' => static::REFERRED_FORM_ID,
                    ]
                );

                return false;
            }
        }

        // Is required but it's empty?
        if ($this->isRequired() && empty($value['value'])) {
            $this->lastErrors[] = $this->t(
                '[@type] Field "@id" is required.',
                [
                    '@type' => $this->getFieldType(),
                    '@id' => $this->getLabelExtended(),
                ]
            );

            return false;
        }
        // Data it's valid.
        return true;
    }

    /**
     * Can this field type be used to sort?
     *
     * @return bool
     */
    public function isSortable(): bool
    {
        return true;
    }

    /**
     * Can this field type be used to filter?
     *
     * @return bool
     */
    public function isFilterable(): bool
    {
        return true;
    }

    /**
     * Get table column name where do sort or filter.
     *
     * @return string
     */
    public function getSortOrFilterTableColumn(): string
    {
        return parent::getSortOrFilterTableColumn().'_value';
    }

    /**
     * Prepare data to insert in form.
     *
     * @param array $data Data to insert in form. Data must be a single field data to insert.
     *
     * @return void
     */
    public function preInsert(&$data): void
    {
        if ($data instanceof FormRecord) {
            if (!$data instanceof FormReferenceEntityInterface) {
                throw new \Exception(
                    $this->t(
                        'Field "@id" requires values implementing interface "App\Forms\FormReferenceEntityInterface".',
                        [
                            '@id' => $this->getId(),
                        ]
                    )
                );
            }
            $aux['form'] = static::REFERRED_FORM_ID;
            $aux['value'] = $data->getId();
            $data = $aux;
        } elseif (is_object($data)) {
            throw new \Exception(
                $this->t(
                    'Field "@id" requires values of type FormRecord, but get "@class" class.',
                    [
                        '@id' => $this->getId(),
                        '@class' => get_class($data),
                    ]
                )
            );
        }
        if (empty($data['form']) && empty($data['value'])) {
            // Allow insert empty values.
            $data[$this->getId().'_value'] = '';
            $data[$this->getId().'_form'] = '';
        } else {
            $ulid = Ulid::fromString($data['value']);
            $data[$this->getId().'_value'] = $ulid->toBinary();
            $data[$this->getId().'_form'] = static::REFERRED_FORM_ID;
        }
        unset($data['value']);
        unset($data['form']);
    }

    /**
     * @inheritDoc
     */
    public function isValidConditionalType(mixed $value): bool
    {
        return 'string' === gettype($value);
    }
}
