<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms\FieldTypes\Types;

use App\Entity\Country;
use App\Forms\Annotation\FieldType;
use App\Forms\FieldTypes\AbstractBaseTargetFieldType;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\FormReferenceEntityInterface;
use App\Forms\SubformFormManager;
use App\Traits\InjectableObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Form record reference field type.
 *
 * Only entities that implements interface FormReferenceEntityInterface
 * can be referenced by this field.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @FieldType(
 *     id = "wsystem_reference",
 *     label = "Water system form record reference field type",
 *     settings = {
 *         "required": "Is this field require?",
 *         "multivalued": "Is this field multivalued?",
 *         "weight": "Field weight",
 *         "meta": "Field metadata properties.",
 *         "country_field": "The country reference field",
 *         "sort": "Is this field sortable?",
 *         "filter": "Is this field filterable?",
 *     }
 * )
 */
class WSystemReferenceFieldType extends WSProviderReferenceFieldType implements InjectableObjectInterface
{
    protected const REFERRED_FORM_ID = 'form.wssystem';


    /**
     * {@inheritDoc}
     */
    public function cloneContent(): mixed
    {
        $resp = [];
        if ($this->isMultivalued()) {
            $records = $this->record->{$this->getId()};
            /** @var FormRecord $record */
            foreach ($records as $record) {
                if (SubformFormManager::class === $record->getForm()->getType()) {
                    $clone = $record->clone();
                    $ulid = Ulid::fromString($clone->getId());
                    $resp[] = [
                        'value' => $ulid->toBase32(),
                        'form' => $clone->getForm()->getId(),
                    ];
                } else {
                    $resp[] = [
                        'value' => $record->getId(),
                        'form' => $record->getForm()->getId(),
                    ];
                }
            }
        } else {
            $record = $this->record->{$this->getId()};
            if ($record) {
                if (SubformFormManager::class === $record->getForm()->getType()) {
                    $clone = $record->clone();
                    $ulid = Ulid::fromString($clone->getId());
                    $resp = [
                        'value' => $ulid->toBase32(),
                        'form' => $clone->getForm()->getId(),
                    ];
                } else {
                    $resp = [
                        'value' => $record->getId(),
                        'form' => $record->getForm()->getId(),
                    ];
                }
            }
        }

        return $resp;
    }
}
