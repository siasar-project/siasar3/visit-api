<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use Doctrine\DBAL\Schema\AbstractSchemaManager;
use PHPUnit\Util\Exception;

/**
 * Database form update manager.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormDatabaseUpdater
{
    protected FormFactory $formFactory;
    protected array $oldSchema;
    protected array $newSchema;

    /**
     * FormDatabaseUpdater constructor.
     *
     * @param FormFactory $formFactory The form factory.
     * @param array       $oldSchema   Old schema.
     * @param array       $newSchema   New schema.
     */
    public function __construct(FormFactory $formFactory, array $oldSchema, array $newSchema)
    {
        /*
         * Form structure:
         *  id: configuration ID
         *  type: Form manager class.
         *  fields: [] Field list.
         *  title: Form name.
         *  description: Form description.
         *  meta: [] Meta data list.
         */
        $this->formFactory = $formFactory;
        $this->oldSchema = $oldSchema;
        $this->newSchema = $newSchema;
    }

    /**
     * Update the form table.
     *
     * @return array Fields processed.
     *
     * @throws \Doctrine\DBAL\Exception
     * @throws \ReflectionException
     */
    public function updateSchema(): array
    {
        // Don't allow field type changes.
        /*
         * Field structure:
         *  id: field_changed
         *  type: date
         *  label: Changed
         *  description: 'Last change time.'
         *  indexable: false
         *  internal: true
         *  deprecated: 'This is a deprecated field.'
         *  settings:
         *      required: false
         *      multivalued: false
         *      weight: 0
         *      meta: {  }
         *      sort: true
         *      filter: true
         */
        $oldForm = $this->formFactory->find($this->oldSchema['id']);
        if (!$oldForm->isInstalled()) {
            throw new Exception(sprintf('[%s] Not installed form can\'t be updated.', $this->oldSchema['id']));
        }
        $formTableName = $oldForm->getTableName();

        $oldFieldKeys = array_keys($this->oldSchema['fields']);
        $newFieldKeys = array_keys($this->newSchema['fields']);
        $addedFields = array_diff($newFieldKeys, $oldFieldKeys);
        $removedFields = array_diff($oldFieldKeys, $newFieldKeys, $addedFields);
        $oldFields = array_diff($oldFieldKeys, $addedFields, $removedFields);
        $updatedFields = [];
        $unchangedFields = [];

        // Unchanged fields.
        if (count($oldFields) > 0) {
            // Verify field update compatibility.
            foreach ($oldFields as $oldField) {
                $updated = false;
                $oldFieldDefinition = $this->oldSchema['fields'][$oldField];
                $newFieldDefinition = $this->newSchema['fields'][$oldField];
                // Compare field types.
                if ($oldFieldDefinition["type"] !== $newFieldDefinition["type"]
                    // A radio_select field is the same that a select.
                    && ('select' !== $oldFieldDefinition["type"] && 'radio_select' !== $newFieldDefinition["type"])
                    && ('radio_select' !== $oldFieldDefinition["type"] && 'select' !== $newFieldDefinition["type"])
                ) {
                    throw new \Exception(
                        sprintf(
                            "[%s] A form field can't change the field type, from '%s' to '%s'.",
                            $oldField,
                            $oldFieldDefinition["type"],
                            $newFieldDefinition["type"]
                        )
                    );
                }
                // Compare multivalued property.
                $field = $this->formFactory->getFieldTypeManager()->create($newFieldDefinition['type'], $newFieldDefinition);
                if ($field->haveSqlDefinition() && $oldFieldDefinition["settings"]["multivalued"] !== $newFieldDefinition["settings"]["multivalued"]) {
                    throw new \Exception(
                        sprintf(
                            "[%s] A form field can't change the field multivalued property.",
                            $oldField
                        )
                    );
                }
                if ($oldFieldDefinition["indexable"] !== $newFieldDefinition["indexable"]) {
                    $updated = true;
                    $indexes = $field->getInstallIndexes();
                    foreach ($indexes as $index) {
                        $sql = false;
                        if ($newFieldDefinition["indexable"]) {
                            // Add index.
                            if (!$this->hasIndex($index['index'], $index['table'])) {
                                $sql = sprintf('CREATE INDEX %s ON %s (%s)', $index['index'], $index['table'], $index['column']);
                            }
                        } else {
                            // Remove index.
                            if ($this->hasIndex($index['index'], $index['table'])) {
                                $sql = sprintf('DROP INDEX IF EXISTS %s ON %s', $index['index'], $index['table']);
                            }
                        }
                        if (false !== $sql) {
                            $this->formFactory->getConnection()->executeStatement($sql);
                        }
                    }
                }
                if ($updated) {
                    $updatedFields[] = $oldField;
                } else {
                    $unchangedFields[] = $oldField;
                }
            }
        }
        // Add new fields.
        if (count($addedFields) > 0) {
            foreach ($addedFields as $addedField) {
                $fieldDefinition = $this->newSchema['fields'][$addedField];
                $field = $this->formFactory->getFieldTypeManager()->create($fieldDefinition['type'], $fieldDefinition);
                if (!$field->haveSqlDefinition()) {
                    continue;
                }
                $requiredIndexes = [];
                if ($fieldDefinition["settings"]["multivalued"]) {
                    // Install multivalued table.
                    $sql = $field->getInstallField();
                    $this->formFactory->getConnection()->executeStatement($sql);
                } else {
                    // Update form table.
                    $sqlFieldInstall = $oldForm->getSqlInstallField($fieldDefinition, $addedField, $requiredIndexes);
                    if (empty($sqlFieldInstall)) {
                        continue;
                    }
                    $sqlFieldInstall = ' ADD '.str_replace(', ', ', ADD ', $sqlFieldInstall);
                    $sql = sprintf('ALTER TABLE %s %s', $formTableName, $sqlFieldInstall);
                    $this->formFactory->getConnection()->executeStatement($sql);
                    // Indexes.
                    if ($field->isIndexabe()) {
                        $indexes = $field->getInstallIndexes();
                        foreach ($indexes as $index) {
                            if (!$this->hasIndex($index['index'], $index['table'])) {
                                $sql = sprintf('CREATE INDEX %s ON %s (%s)', $index['index'], $index['table'], $index['column']);
                                $this->formFactory->getConnection()->executeStatement($sql);
                            }
                        }
                    }
                }
            }
        }
        // field removed: The field don't are in the new configuration, it must be removed from database table.
        if (count($removedFields) > 0) {
            foreach ($removedFields as $removedField) {
                $fieldDefinition = $this->oldSchema['fields'][$removedField];
                $field = $this->formFactory->getFieldTypeManager()->create($fieldDefinition['type'], $fieldDefinition);
                if (!$field->haveSqlDefinition()) {
                    continue;
                }
                if ($fieldDefinition["settings"]["multivalued"]) {
                    // Uninstall multivalued table.
                    $sql = sprintf('DROP TABLE IF EXISTS %s', $field->getFieldTableName());
                    $this->formFactory->getConnection()->executeStatement($sql);
                } else {
                    $props = $field->getProperties();
                    if (count($props) === 1) {
                        $sql = sprintf('ALTER TABLE %s DROP %s', $formTableName, $removedField);
                    } else {
                        $drops = [];
                        foreach ($props as $prop => $default) {
                            $drops[] = $removedField.'_'.$prop;
                        }
                        $sql = sprintf('ALTER TABLE %s DROP %s', $formTableName, implode(', DROP ', $drops));
                    }
                    $this->formFactory->getConnection()->executeStatement($sql);
                }
                // Indexes.
                // Removing a table column remove the index too.
                // $indexes = $field->getInstallIndexes();
                // foreach ($indexes as $index) {
                //     $sql = sprintf('DROP INDEX IF EXISTS %s ON %s', $index['index'], $index['table']);
                //     $this->formFactory->getConnection()->executeStatement($sql);
                // }
            }
        }
        // field deprecated: The field don't change, and is detected automatically by the form managers.

        return [
            'added' => $addedFields,
            'removed' => $removedFields,
            'updated' => $updatedFields,
            'unchanged' => $unchangedFields,
        ];
    }

    /**
     * Has the table the index?
     *
     * @param string $indexName     Index name.
     * @param string $formTableName Table name.
     *
     * @return bool
     */
    protected function hasIndex(string $indexName, string $formTableName): bool
    {
        $schemaManager = $this->formFactory->getConnection()->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($formTableName);

        return (in_array($indexName, array_keys($tableData->getIndexes())));
    }
}
