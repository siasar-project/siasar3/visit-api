<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Entity\Configuration;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Repository\ConfigurationRepository;
use App\Traits\StringTranslationTrait;
use Doctrine\DBAL\Connection;
use Doctrine\Common\Collections\Criteria;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Forms factory.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormFactory
{
    use StringTranslationTrait;

    protected ConfigurationRepository $configRepo;
    protected FieldTypeManager $fieldTypeManager;
    protected Connection $connection;
    protected EventDispatcherInterface $dispatcher;

    /**
     * Form factory constructor.
     *
     * @param ContainerInterface $container        Configuration repository.
     * @param FieldTypeManager   $fieldTypeManager Field type manager.
     * @param Connection         $connection       Data base connection.
     */
    public function __construct(ContainerInterface $container, FieldTypeManager $fieldTypeManager, Connection $connection)
    {
        $this->configRepo = $container->get('doctrine')->getManager()->getRepository(Configuration::class);
        $this->dispatcher = $container->get('event_dispatcher');
        $this->fieldTypeManager = $fieldTypeManager;
        $this->connection = $connection;
    }

    /**
     * Get the field type manager.
     *
     * @return FieldTypeManager
     */
    public function getFieldTypeManager(): FieldTypeManager
    {
        return $this->fieldTypeManager;
    }

    /**
     * Get database connection.
     *
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * Factory method to create FormManager instances.
     *
     * If the $id not start with 'form.' this method add that prefix.
     *
     * @param string     $id                 FormManager ID.
     * @param string     $managerType        FormManager class.
     * @param array|null $forceConfiguration If defined don't load database configuration, use this configuration.
     *
     * @return FormManagerInterface The created EntityManager.
     *
     * @throws \Exception
     */
    public function create(string $id, string $managerType = FormManager::class, array $forceConfiguration = null): FormManagerInterface
    {
        // Form IDs only can content alphanumerics, dots, and underscore.
        $matches = null;
        $pattern = '/^[.A-Za-z0-9]*/';
        $match = preg_match($pattern, $id, $matches);
        if (0 === $match || current($matches) !== $id) {
            throw new \Exception(
                $this->t(
                    'Form ID "@id" not valid, must be "@pattern".',
                    [
                        '@id' => $id,
                        '@pattern' => $pattern,
                    ]
                )
            );
        }
        // Prepare form.
        $id = $this->formatId($id);
        if (!$this->isForcedConfiguration($forceConfiguration)) {
            // Try to load configuration from database.
            $configuration = $this->getConfiguration($id, false);
        } else {
            // We need create a virtual form from a defined configuration.
            $configuration = $this->configRepo->createVirtual($id, $forceConfiguration);
        }

        if (!$configuration) {
            $configuration = $this->configRepo->create($id, []);
            $value = $configuration->getValue();
            $value['id'] = $id;
            $configuration->setValue($value);
        }

        /** @var FormManagerInterface $manager */
        $manager = new $managerType(
            $configuration,
            $this->fieldTypeManager,
            $this->connection,
            $this->dispatcher
        );
        if (!$this->isForcedConfiguration($forceConfiguration)) {
            $manager->addBaseFields();
        }

        return $manager;
    }

    /**
     * Exist this form?
     *
     * @param string $id Form Id.
     *
     * @return bool
     */
    public function exist(string $id): bool
    {
        $id = $this->formatId($id);

        return $this->configRepo->exist($id);
    }

    /**
     * Find forms by manager type.
     *
     * @param string $type
     *
     * @return array
     */
    public function findByType(string $type): array
    {
        $criteria = new Criteria();
        if (!empty($type)) {
            $criteria->andWhere(Criteria::expr()->contains('value', str_replace('\\', '\\\\', $type)));
        }

        return $this->findBy($criteria);
    }

    /**
     * Get forms by criteria.
     *
     * @param Criteria $criteria Find criteria.
     * @param array    $orderBy  Response order.
     * @param ?int     $limit    Response limit.
     * @param ?int     $offset   Response offset.
     *
     * @return array[FormManager]
     */
    public function findBy(Criteria $criteria, array $orderBy = [], ?int $limit = null, ?int $offset = null): array
    {
        $criteria = $this->getFindFormCriteria($criteria);
        $criteria->orderBy($orderBy)
            ->setMaxResults($limit)
            ->setFirstResult($offset);
        $qb = $this->configRepo->createQueryBuilder('t')
            ->select()
            ->addCriteria($criteria);
        $configurations = $qb->getQuery()->getResult();
        $resp = [];
        foreach ($configurations as $configuration) {
            $type = $configuration->getValue()['type'];
            $resp[] = new $type(
                $configuration,
                $this->fieldTypeManager,
                $this->connection,
                $this->dispatcher
            );
        }

        return $resp;
    }

    /**
     * Get a form manager by ID.
     *
     * @param string $id              Form id.
     * @param bool   $onFailException Throw exception if not found.
     *
     * @return FormManagerInterface|null
     *
     * @throws \Exception
     */
    public function find(string $id, bool $onFailException = true): ?FormManagerInterface
    {
        $id = $this->formatId($id);
        $configuration = $this->getConfiguration($id, false);
        if (!$configuration) {
            if ($onFailException) {
                throw new \Exception($this->t('Form "@id" not found', ['@id' => $id]));
            }

            return null;
        }

        $type = $configuration->getValue()['type'];

        return new $type(
            $configuration,
            $this->fieldTypeManager,
            $this->connection,
            $this->dispatcher
        );
    }

    /**
     * Get a form manager by ID.
     *
     * @param string $id Form id.
     *
     * @return FormManagerInterface|null
     *
     * @throws \Exception
     */
    public function findEditable(string $id): ?FormManagerInterface
    {
        $id = $this->formatId($id);
        $configuration = $this->getConfiguration($id, true);
        if (!$configuration) {
            throw new \Exception($this->t('Form "@id" not found', ['@id' => $id]));
        }

        $type = $configuration->getValue()['type'];

        return new $type(
            $configuration,
            $this->fieldTypeManager,
            $this->connection,
            $this->dispatcher
        );
    }

    /**
     * Remove a FormManager.
     *
     * @param string $id Form ID.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(string $id): void
    {
        $id = $this->formatId($id);
        $configuration = $this->getConfiguration($id, false);
        if ($configuration) {
            $form = $this->find($id);
            if ($form->isInstalled()) {
                $form->uninstall();
            }
            $this->configRepo->delete($id);
        }
    }

    /**
     * Find all forms.
     *
     * @param array $orderBy Response order.
     * @param ?int  $limit   Response limit.
     * @param ?int  $offset  Response offset.
     *
     * @return array
     *
     * @throws \Exception
     */
    public function findAll(array $orderBy = [], ?int $limit = null, ?int $offset = null): array
    {
        $criteria = $this->getFindFormCriteria();
        $criteria->orderBy($orderBy)
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $results = $this->configRepo->matching($criteria)->getValues();
        $resp = [];
        /**
         * Configuration entity.
         *
         * @var Configuration $result
         */
        foreach ($results as $result) {
            $resp[$result->getId()] = $this->find($result->getId());
        }

        return $resp;
    }

    /**
     * Format a Form ID.
     *
     * @param string $id
     *
     * @return string
     */
    public static function formatId(string $id): string
    {
        $prefix = substr($id, 0, 5);
        if ('form.' !== $prefix) {
            $id = 'form.'.$id;
        }

        return $id;
    }

    /**
     * Get fields that can mutate in any form.
     *
     * @param string $type Form manager type.
     *
     * @return array
     */
    public function getMutatebleFields(string $type): array
    {
        $forms = $this->findByType($type);

        $mutatebles = [];
        /** @var FormManagerInterface $form */
        foreach ($forms as $form) {
            $fields = $form->getFields();
            foreach ($fields as $field) {
                if ($field->getFieldType() === 'mutateble') {
                    if (!isset($mutatebles[$form->getId()])) {
                        $mutatebles[$form->getId()] = [];
                    }
                    $mutatebles[$form->getId()][] = $field->getId();
                }
            }
        }

        return $mutatebles;
    }

    /**
     * Allow to any form manager to extends a record array response.
     *
     * @param array $data
     *
     * @return array
     */
    public function updateFormRecordArray(array $data): array
    {
        return $data;
    }

    /**
     * Get a configuration entity.
     *
     * @param string $id       Configuration Id.
     * @param bool   $editable Get editable?
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function getConfiguration(string $id, bool $editable): mixed
    {
        if ($editable) {
            $configuration = $this->configRepo->findEditable($id);
        } else {
            $configuration = $this->configRepo->find($id);
        }

        return $configuration;
    }

    /**
     * Filter result by form configuration key.
     *
     * @param Criteria|null $criteria Optional criteria to update.
     *
     * @return Criteria
     */
    protected function getFindFormCriteria(Criteria $criteria = null): Criteria
    {
        if (!$criteria) {
            $criteria = new Criteria();
        }
        $criteria->andWhere($criteria->expr()->startsWith('id', 'form.'));

        return $criteria;
    }

    /**
     * This value force configuration?
     *
     * Used in create method to known if is a forced configuration.
     *
     * @param array|null $value
     *
     * @return bool
     */
    protected function isForcedConfiguration(?array $value): bool
    {
        return !is_null($value);
    }
}
