<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Entity\Configuration\ConfigurationReadInterface;
use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FieldTypes\Types\CountryReferenceEntity;
use App\Forms\FieldTypes\Types\EntityReferenceFieldType;
use App\Forms\FieldTypes\Types\SubformFieldType;
use App\Traits\StringTranslationTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception as Driver_Exception;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Forms entity manager.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormManager implements FormManagerInterface
{
    use StringTranslationTrait;

    public const ENDPOINT_COLLECTION_GET = 'endpoint_collection_get';
    public const ENDPOINT_COLLECTION_POST = 'endpoint_collection_post';
    public const ENDPOINT_ITEM_GET = 'endpoint_item_get';
    public const ENDPOINT_ITEM_PUT = 'endpoint_item_put';
    public const ENDPOINT_ITEM_DELETE = 'endpoint_item_delete';
    public const ENDPOINT_ITEM_PATCH = 'endpoint_item_patch';
    public const ENDPOINT_ITEM_CLONE = 'endpoint_item_clone';
    public const ENDPOINT_ITEM_HISTORY = 'endpoint_item_history';

    protected ConfigurationReadInterface $config;
    protected ?array $confValues;
    protected FieldTypeManager $fieldTypeManager;
    protected Connection $connection;
    protected array $lastErrors;
    protected array $fieldDefinitionCache;
    protected EventDispatcherInterface $dispatcher;
    /**
     * If TRUE required fields lose this property.
     *
     * @var bool
     */
    protected bool $ignoreRequiredFields;

    /**
     * FormManager constructor.
     *
     * @param ConfigurationReadInterface $config           Form configuration.
     * @param FieldTypeManager           $fieldTypeManager Field type manager.
     * @param Connection                 $connection       Database connection.
     * @param EventDispatcherInterface   $dispatcher       Event dispatcher.
     */
    public function __construct(ConfigurationReadInterface $config, FieldTypeManager $fieldTypeManager, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        // Remember services.
        $this->fieldTypeManager = $fieldTypeManager;
        $this->fieldTypeManager->setFormManager($this);
        $this->connection = $connection;
        $this->dispatcher = $dispatcher;
        // Init.
        $this->ignoreRequiredFields = false;
        $this->lastErrors = [];
        $this->config = $config;
        $this->confValues = $config->getValue();
        if ($this->isConfigurable()) {
            // Register manager class.
            $this->confValues['type'] = static::class;
            // Ensure that we have a field collection.
            if (!isset($this->confValues['fields'])) {
                $this->confValues['fields'] = [];
            }
            // Ensure that we have endpoints properties.
            if (!isset($this->confValues['endpoint'])) {
                $this->confValues['endpoint'] = [
                    'collection' => [
                        'get' => true,
                        'post' => true,
                    ],
                    'item' => [
                        'get' => true,
                        'put' => true,
                        'delete' => true,
                        'patch' => true,
                    ],
                ];
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function mustIgnoreRequiredFields(): bool
    {
        return $this->ignoreRequiredFields;
    }

    /**
     * @inheritDoc
     */
    public function setIgnoreRequiredFields(bool $value): self
    {
        $this->ignoreRequiredFields = $value;

        return $this;
    }

    /**
     * Get form manager class.
     *
     * @return string
     */
    public function getType(): string
    {
        if (isset($this->confValues['type'])) {
            return $this->confValues['type'];
        }

        return '';
    }

    /**
     * Get form title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        if (isset($this->confValues['title'])) {
            return $this->confValues['title'];
        }

        return '';
    }

    /**
     * Set form title.
     *
     * @param string $title New form title.
     *
     * @return void
     */
    public function setTitle(string $title): void
    {
        $this->confValues['title'] = $title;
    }

    /**
     * Get form description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        if (isset($this->confValues['description'])) {
            return $this->confValues['description'];
        }

        return '';
    }

    /**
     * Set form description.
     *
     * @param string $description New form description.
     *
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->confValues['description'] = $description;
    }

    /**
     * Get if this manager has endpoint.
     *
     * The class FormManager have constants defined about
     * valid end-points types. The constants start with:
     * "ENDPOINT_".
     *
     * @param string $type Any of FormManager::ENDPOINT_???.
     *
     * @return bool
     */
    public function haveEndpoint(string $type): bool
    {
        $setting = $this->getEndpointRawValue($type);

        return (false !== $setting);
    }

    /**
     * Get alternative FormCollectionAction method if defined.
     *
     * @see FormManagerInterface::haveEndpoint()
     *
     * @param string $type
     *
     * @return string|null
     */
    public function getEndpointMethod(string $type): string|null
    {
        $setting = $this->getEndpointRawValue($type);
        if (is_bool($setting)) {
            return null;
        }

        if (is_array($setting)) {
            if (isset($setting['controller'])) {
                return $setting['controller'];
            }

            return null;
        }

        return $setting;
    }

    /**
     * Get raw endpoint setting.
     *
     * @see FormManagerInterface::haveEndpoint()
     *
     * @param string $type
     *
     * @return string
     */
    public function getEndpointRawValue(string $type): mixed
    {
        $defaultResponse = true;
        switch ($type) {
            case self::ENDPOINT_COLLECTION_GET:
                $space = 'collection';
                $method = 'get';
                break;
            case self::ENDPOINT_COLLECTION_POST:
                $space = 'collection';
                $method = 'post';
                break;
            case self::ENDPOINT_ITEM_GET:
                $space = 'item';
                $method = 'get';
                break;
            case self::ENDPOINT_ITEM_PUT:
                $space = 'item';
                $method = 'put';
                break;
            case self::ENDPOINT_ITEM_DELETE:
                $space = 'item';
                $method = 'delete';
                break;
            case self::ENDPOINT_ITEM_PATCH:
                $space = 'item';
                $method = 'patch';
                break;
            case self::ENDPOINT_ITEM_CLONE:
                $space = 'item';
                $method = 'clone';
                $defaultResponse = false;
                break;
            case self::ENDPOINT_ITEM_HISTORY:
                $space = 'item';
                $method = 'history';
                $defaultResponse = false;
                break;
            default:
                throw new \Exception($this->t('Unknown endpoint setting: @set.', ['@set' => $type]));
        }

        if (!isset($this->confValues['endpoint'][$space][$method])) {
            return $defaultResponse;
        }

        return $this->confValues['endpoint'][$space][$method];
    }

    /**
     * Is this form configurable?
     *
     * @return bool
     */
    public function isConfigurable(): bool
    {
        return ($this->config instanceof ConfigurationWriteInterface);
    }

    /**
     * Get form Id.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->config->getId();
    }

    /**
     * Save definition.
     *
     * @param bool $flush If true flush to database.
     *
     * @return void
     */
    public function save(bool $flush = false): void
    {
        if ($this->isConfigurable()) {
            // Update field settings.
            $fields = $this->getFields();
            /** @var FieldTypeInterface $field */
            foreach ($fields as $field) {
                $this->confValues['fields'][$field->getId()] = $field->getSettings();
            }
            // Set form settings.
            $this->config->setValue($this->confValues);
            if ($flush) {
                $this->config->saveNow();
            } else {
                $this->config->save();
            }
        }
    }

    /**
     * Save and flush definition.
     *
     * @return void
     */
    public function saveNow(): void
    {
        $this->save(true);
    }

    /**
     * Add or update a field definition.
     *
     * @param string $fieldTypeId The field type.
     * @param string $fieldName   The field Id.
     * @param string $fieldLabel  The field label.
     * @param array  $options     Field settings.
     *
     * @return ?FieldTypeInterface
     *
     * @throws \Exception
     */
    public function addField(string $fieldTypeId, string $fieldName, string $fieldLabel, array $options = []): ?FieldTypeInterface
    {
        if (!$this->isConfigurable()) {
            return null;
        }

        // Enforce lower case field names.
        $fieldName = strtolower($fieldName);
        // Add preffix if required.
        $prefix = substr($fieldName, 0, 6);
        if ('field_' !== $prefix) {
            $fieldName = 'field_'.$fieldName;
        }

        if ('field_id' === $fieldName) {
            throw new \Exception($this->t('All forms have already an id field.'));
        }

        if ($this->isField($fieldName)) {
            throw new \Exception($this->t('Field "@id" already exists.', ['@id' => $fieldName]));
        }

        // Verify that the field type exist.
        $fieldType = $this->fieldTypeManager->create($fieldTypeId);
        $settings = [
            'type' => $fieldTypeId,
            'id' => $fieldName,
            'label' => $fieldLabel,
            'settings' => array_merge($fieldType->getDefaultSettings(), $options),
        ];
        $fieldType->setSettings($settings);

        // Validation moved to self::install() to allow invalidating by field meta references
        // without sorting problems.
        /*
        if (!$fieldType->isValidSettings()) {
            throw new \Exception($this->t('Invalid field settings.'));
        }
        */

        // add the new field.
        $this->confValues['fields'][$fieldName] = $settings;

        return $this->getFieldDefinition($fieldName);
    }

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        return;
    }

    /**
     * Get form data base table name.
     *
     * @return string
     */
    public function getTableName(): string
    {
        return str_replace('.', '_', $this->getId());
    }

    /**
     * Is this form installed?
     *
     * @return bool
     */
    public function isInstalled(): bool
    {
        return $this->connection->getSchemaManager()->tablesExist($this->getTableName());
    }

    /**
     * Get field definitions.
     *
     * @return array
     */
    public function getFieldDefinitions(): array
    {
        return $this->confValues['fields'];
    }

    /**
     * {@inheritDoc}
     */
    public function getFields(): array
    {
        $fields = [];
        foreach ($this->confValues['fields'] as $fieldName => $fieldDef) {
            $fields[] = $this->getFieldDefinition($fieldName);
        }

        return $fields;
    }

    /**
     * Get field definition.
     *
     * @param string $name Field name.
     *
     * @return FieldTypeInterface|null
     *
     * @throws \ReflectionException
     */
    public function getFieldDefinition(string $name): ?FieldTypeInterface
    {
        if (isset($this->confValues['fields'][$name])) {
            if (isset($this->fieldDefinitionCache[$name])) {
                return $this->fieldDefinitionCache[$name];
            }
            // Because field type manager is autowired, we need update the form manager to use.
            $this->fieldTypeManager->setFormManager($this);
            $field = $this->fieldTypeManager->create(
                $this->confValues['fields'][$name]['type'],
                $this->confValues['fields'][$name]
            );
            $field->setFieldName($name);
            $field->setFormManager($this);
            $this->fieldDefinitionCache[$name] = $field;

            return $field;
        }

        return null;
    }

    /**
     * Is a form field?
     *
     * @param string $name Field name.
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function isField(string $name): bool
    {
        return isset($this->confValues['fields'][$name]);
    }

    /**
     * Get form metadata.
     *
     * @return array
     */
    public function getMeta(): array
    {
        if (isset($this->confValues['meta'])) {
            return $this->confValues['meta'];
        }

        return [];
    }

    /**
     * Set form metadata.
     *
     * @param array $meta New metadata.
     *
     * @return void
     */
    public function setMeta(array $meta): void
    {
        if ($this->isConfigurable()) {
            $this->confValues['meta'] = $meta;
        }
    }

    /**
     * Get fields marked how sortable.
     *
     * @return FieldTypeInterface[]
     */
    public function getSortFields(): array
    {
        $resp = [];
        $fields = $this->getFields();

        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            if ($field->canSortBy()) {
                $resp[] = $field;
            }
        }

        return $resp;
    }

    /**
     * Get fields marked how filterable.
     *
     * @return FieldTypeInterface[]
     */
    public function getFilterFields(): array
    {
        $resp = [];
        $fields = $this->getFields();

        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            if ($field->canFilterBy()) {
                $resp[] = $field;
            }
        }

        return $resp;
    }

    /**
     * Install form in data base.
     *
     * @return void
     *
     * @throws Exception
     */
    public function install(): void
    {
        if ($this->isInstalled()) {
            return;
        }

        // Add fields to table.
        $ddlFieldList = '';
        $requiredIndexes = [
            'PRIMARY KEY(id)',
        ];
        $multivaluedFields = [];
        foreach ($this->getFieldDefinitions() as $fieldName => $fieldDefinition) {
            // Validate field settings.
            $field = $this->fieldTypeManager->create(
                $fieldDefinition['type'],
                $this->confValues['fields'][$fieldName]
            );
            $field->setFormManager($this);
            if (!$field->isValidSettings()) {
                throw new \Exception($this->t('Invalid field settings.'));
            }
            $sqlFieldInstall = $this->getSqlInstallField($fieldDefinition, $fieldName, $requiredIndexes);
            if (empty($sqlFieldInstall)) {
                if ($fieldDefinition['settings']['multivalued']) {
                    $multivaluedFields[$fieldName] = $field;
                }
                continue;
            }

            $ddlFieldList .= ' '.$sqlFieldInstall.', ';
        }

        $sql = sprintf(
            'CREATE TABLE %s (
            id BINARY(16) NOT NULL COMMENT \'(DC2Type:ulid)\',
            %s
            %s
          ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB',
            $this->getTableName(),
            $ddlFieldList,
            implode(", \n", $requiredIndexes)
        );

        $this->connection->executeStatement($sql);

        // Install multi valued field.
        foreach ($multivaluedFields as $fieldName => $field) {
            // Get create table sql.
            $sql = $field->getInstallField();
            $this->connection->executeStatement($sql);
        }
    }

    /**
     * Get SQL to create/alter the field.
     *
     * @param array  $fieldDefinition The field definition.
     * @param string $fieldName       The field name.
     * @param array  $requiredIndexes List of new indexes to add. This method add the new index if required.
     *
     * @return string
     *  Empty if the new field is multivalued, and not processed.
     *
     * @throws \ReflectionException
     */
    public function getSqlInstallField(array $fieldDefinition, string $fieldName, array &$requiredIndexes): string
    {
        $field = $this->fieldTypeManager->create(
            $fieldDefinition['type'],
            $fieldDefinition
        );
        $field->setFormManager($this);
        if ($field->isMultivalued()) {
            return '';
        }
        if ($field->isIndexabe()) {
            $indexes = $field->getInstallIndexes();
            foreach ($indexes as $index) {
                $requiredIndexes[] = sprintf('INDEX %s (%s)', $index['index'], $index['column']);
            }
        }

        return $field->getInstallField();
    }

    /**
     * Uninstall form in data base.
     *
     * @return void
     *
     * @throws Exception
     * @throws \ReflectionException
     */
    public function uninstall(): void
    {
        if (!$this->isInstalled()) {
            return;
        }

        $sql = sprintf('DROP TABLE IF EXISTS %s', $this->getTableName());
        $this->connection->executeStatement($sql);

        foreach ($this->getFieldDefinitions() as $fieldName => $fieldDefinition) {
            $field = $this->fieldTypeManager->create(
                $fieldDefinition['type'],
                $this->confValues['fields'][$fieldName]
            );
            $field->setFormManager($this);
            if ($field->isMultivalued()) {
                $sql = $field->getUninstallField();
                if (!is_null($sql)) {
                    $this->connection->executeStatement($sql);
                }
            }
        }
    }

    /**
     * Insert a new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        if (!$this->isValidFieldValues($data, false)) {
            // TODO: Each error message must be translated while is generated.
            throw new \Exception(implode("\n", $this->getLastValidationMessages()));
        }

        // Generate a new ULID.
        $ulid = new Ulid();
        $data['id'] = $ulid->toBinary();
        $multivalued = [];
        $fieldDefinitions = $this->getFieldDefinitions();
        foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
            if (!isset($fieldDefinition['type'])) {
                continue;
            }
            $field = $this->fieldTypeManager->create(
                $fieldDefinition['type'],
                $this->confValues['fields'][$fieldName]
            );
            $field->setFormManager($this);
            $this->prepareFieldToDatabase($field, $data, $multivalued);
            // todo Find where we are adding the "form" key to data. :\
            unset($data['form']);
        }
//        $this->connection->beginTransaction();
//        try {
            // Insert in main table.
            $this->connection->insert($this->getTableName(), $data);
            // Insert multivalued fields.
            $this->insertMultiValuedToDatabase($multivalued, $ulid);
//            $this->connection->commit();
//        } catch (\Exception $e) {
//            $this->connection->rollback();
//            throw $e;
//        }
        // Return main table record ID.
        return $ulid->toBase32();
    }

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update. If have a 'id' key it will be used instead the $id parameter.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$record->isModified()) {
            return;
        }
        $id = $record->getId();
        $ulid = Ulid::fromString($id);

        // Data to validate.
        $data = $record->getCurrentRaw();
        unset($data['id']);
        if (!$force) {
            // Validate.
            if (!$this->isValidFieldValues($data, true, $record)) {
                throw new \Exception(
                    self::t(
                        '@form errors: @msgs',
                        [
                            '@form' => trim(sprintf('%s [%s]', $this->getTitle(), $this->getId())),
                            '@msgs' => sprintf('%s%s', "\n", implode("\n", $this->getLastValidationMessages())),
                        ]
                    )
                );
            }
            // Data maybe updated while validating.
            $data = $record->getCurrentRaw();
            unset($data['id']);
        }

        // Save data.
        $multivalued = [];
        foreach ($record->getFieldDefinitions() as $fieldName => $fieldDefinition) {
            $field = $record->getFieldDefinition($fieldName);
            $this->prepareFieldToDatabase($field, $data, $multivalued);
        }

        if (!empty($data)) {
            $this->connection->update($this->getTableName(), $data, ['id' => $ulid->toBinary()]);
        }
            // Remove old multivalued fields.
        foreach ($multivalued as $mFieldName => $value) {
            $field = $record->getFieldDefinition($mFieldName);
            $this->connection->delete($field->getFieldTableName(), ['record' => $ulid->toBinary()]);
        }

        // Insert multivalued fields.
        $this->insertMultiValuedToDatabase($multivalued, $ulid);
    }

    /**
     * Drop a record.
     *
     * @param string $id Record ID.
     *
     * @return void
     *
     * @throws Exception
     */
    public function drop(string $id): void
    {
//        $this->connection->beginTransaction();
//        try {
            $ulid = Ulid::fromString($id);
            $record = $this->find($id);
        if (!$record) {
            return;
        }
            $fields = $record->getForm()->getFields();

            /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            // Query each field to remove self external data, if required.
            $field->dropContent();
            if ($field->isMultivalued()) {
                // Remove multivalued fields.
                $this->connection->delete($field->getFieldTableName(), ['record' => $ulid->toBinary()]);
            }
        }
            // Remove record.
            $this->connection->delete($this->getTableName(), ['id' => $ulid->toBinary()]);
//            $this->connection->commit();
//        } catch (\Exception $e) {
//            $this->connection->rollback();
//            throw $e;
//        }
    }

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param string $id The identifier.
     *
     * @return FormRecord|null The object.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function find(string $id): ?FormRecord
    {
        $resp = current($this->findBy(['id' => $id]));
        if (false === $resp) {
            return null;
        }

        return $resp;
    }

    /**
     * Finds all objects in the repository.
     *
     * @return array<int, array> The objects.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function findAll(): array
    {
        return $this->findBy([]);
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array         $criteria Filtering criteria.
     * @param string[]|null $orderBy  Sorting.
     * @param ?integer      $limit    Result limits.
     * @param ?integer      $offset   Result start offset.
     * @param ?array        $groupBy  Grouping.
     *
     * @return array[] The objects.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): array
    {
        $query = $this->queryBy($criteria, $orderBy, $limit, $offset, $groupBy);
        $resp = $this->hydrateQuery($query);

        return $resp;
    }

    /**
     * @inheritDoc
     */
    public function hydrateQuery(QueryBuilder $query): array
    {
        $rawResult = $query->execute()->fetchAllAssociative();

        $resp = [];
        $rawResult = $this->unFlatRawResults($rawResult);
        // $rawResult never contains multi valued field data.
        foreach ($rawResult as $raw) {
            $record = $this->createRecord([]);
            $fields = $this->getFieldDefinitions();
            $record->id = $raw['id'];
            foreach ($fields as $fieldName => $field) {
                $record->setFieldDefinition($fieldName, $this->getFieldDefinition($fieldName));
                $isMultivalued = isset($field['settings']['multivalued']) ? $field['settings']['multivalued'] : false;
                if ($isMultivalued) {
                    $record->{$fieldName} = null;
                } else {
                    $record->{$fieldName} = $raw[$fieldName];
                }
            }
            $record->setLoaded();
            $resp[] = $record;
        }

        return $resp;
    }

    /**
     * Hydrate a FormRecord with flat data.
     *
     * @param FormRecord $record
     * @param array      $data
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function hydrateRecord(FormRecord $record, array $data = [])
    {
        if (!isset($data['id'])) {
            $data['id'] = Ulid::fromString('00000000000000000000000000');
        }
        $fields = $this->getFieldDefinitions();
        $record->id = $data['id'];
        // Set field definitions.
        foreach ($fields as $fieldName => $field) {
            $record->setFieldDefinition($fieldName, $this->getFieldDefinition($fieldName));
        }
        // Transfer data to record.
        /**
         * @var string $fieldName
         * @var array  $field
         */
        foreach ($fields as $fieldName => $field) {
            if ('id' === $fieldName) {
                continue;
            }
            $fieldDefinition = $this->getFieldDefinition($fieldName);
            $record->setFieldDefinition($fieldName, $fieldDefinition);
            $isMultivalued = isset($field['settings']['multivalued']) ? $field['settings']['multivalued'] : false;
            if ($isMultivalued) {
                $record->{$fieldName} = null;
            } else {
                if (!isset($data[$fieldName])) {
                    $data[$fieldName] = $fieldDefinition->getDefaultValue();
                }
                $record->set($fieldName, $data[$fieldName]);
            }
        }
        $record->setLoaded();
    }

    /**
     * @inheritDoc
     */
    public function queryBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): QueryBuilder
    {
        $query = $this->connection->createQueryBuilder()
            ->select(['t.*'])
            ->from($this->getTableName(), 't');

        if ($groupBy) {
            $aux = [];
            foreach ($groupBy as $group) {
                if (!str_starts_with($group, 't.')) {
                    $aux[] = 't.'.$group;
                } else {
                    $aux[] = $group;
                }
            }
            $query->groupBy($aux);
        }

        if ($orderBy) {
            // Is $orderBy indexed?
            $orderIndexed = (array_values($orderBy) !== $orderBy);
            // Generate a order by each user selection.
            foreach ($orderBy as $fieldName => $order) {
                if (!$orderIndexed) {
                    $order = 'ASC';
                }
                $name = $fieldName;
                $field = $this->getFieldDefinition($name);
                $name = $field->getSortOrFilterTableColumn();
                $query->orderBy($name, $order);
            }
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        if ($offset) {
            $query->setFirstResult($offset);
        }

        return $this->parseCriteria($criteria, $query);
    }

    /**
     * Count objects by a set of criteria.
     *
     * @param array $criteria Filtering criteria.
     *
     * @return int
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function countBy(array $criteria): int
    {
        $query = $this->connection->createQueryBuilder()
            ->select('count(t.id) as counter')
            ->from($this->getTableName(), 't');

        $query = self::parseCriteria($criteria, $query);
        $rawResult = $query->execute()->fetchAllAssociative();

        return intval($rawResult[0]["counter"]);
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return FormRecord|null The object.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function findOneBy(array $criteria): ?FormRecord
    {
        $resp = $this->findBy($criteria, null, 1);

        if (count($resp) > 0) {
            return current($resp);
        }

        return null;
    }

    /**
     * Clone a form record.
     *
     * @param string $id
     *
     * @return string
     */
    public function clone(string $id): string
    {
        $record = $this->find($id);
        if (!$record) {
            throw new \Exception(self::t('Record "@id" not found', ['@id' => $id]));
        }

        return $this->cloneRecord($record);
    }

    /**
     * Clone a form record.
     *
     * @param FormRecord $record
     *
     * @return string
     *
     * @throws \Exception
     */
    public function cloneRecord(FormRecord $record): string
    {
        // Create a new record.
        return $this->insert($this->getRecordInternalValues($record));
    }

    /**
     * Validate form record
     *
     * @param FormRecord $record
     *
     * @return bool
     */
    public function validateRecord(FormRecord $record): bool
    {
        // Data to validate.
        $data = $record->getCurrentRaw();
        unset($data['id']);

        return $this->isValidFieldValues($data, true, $record);
    }

    /**
     * Get last validation error messages.
     *
     * @return array
     */
    public function getLastValidationMessages(): array
    {
        return $this->lastErrors;
    }

    /**
     * Allow to a form manager to update a form record data array before serialization.
     *
     * @param array      $data
     * @param FormRecord $record
     *
     * @return array
     */
    public function updateFormRecordArray(array $data, FormRecord $record): array
    {
        return $data;
    }

    /**
     * Create a new record without saving it.
     *
     * Note: FormRecord doesn't need the complete field list to create or save it.
     * This is an auxiliary method.
     *
     * @param array $data Record data.
     *
     * @return FormRecord
     */
    public function createRecord(array $data): FormRecord
    {
        // Create record without data.
        $record = new FormRecord($this, [], $this->connection, $this->dispatcher);
        // And set each field to preprocess values.
        foreach ($data as $fieldName => $fieldValue) {
            $record->{$fieldName} = $fieldValue;
        }
        $record->setLoaded();

        return $record;
    }

    /**
     * Get a clean copy of record field values.
     *
     * @param FormRecord $record
     *
     * @return array
     */
    protected function getRecordInternalValues(FormRecord $record): array
    {
        // Get current values.
        $internals = $record->getCurrentRaw();
        unset($internals['id']);

        return $internals;
    }

    /**
     * Parse and inject query criteria to an QueryBuilder.
     *
     * @param array        $criteria Filter criteria.
     * @param QueryBuilder $query    The query builder.
     * @param bool         $isAnd
     *
     * @return QueryBuilder
     *
     * @throws \ReflectionException
     */
    protected function parseCriteria(array $criteria, QueryBuilder $query, bool $isAnd = true): QueryBuilder
    {
        if (count($criteria) > 0) {
            // Join required multivalued tables.
            $criteriaToUpdate = [];
            // We must not update $value if is not mandatory.
            foreach ($criteria as $fieldName => &$value) {
                switch (strtolower($fieldName)) {
                    case 'and':
                    case '&&':
                        $query = $this->parseCriteria($value, $query, true);
                        $criteriaToUpdate[$fieldName] = null;
                        break;

                    case 'or':
                    case '||':
                        $query = $this->parseCriteria($value, $query, false);
                        $criteriaToUpdate[$fieldName] = null;
                        break;

                    case 'unique':
                        $query->distinct();
                        $criteriaToUpdate[$fieldName] = null;
                        break;

                    case 'id':
                        // Nothing to do.
                        break;

                    case 'branch':
                        // This query require link to administrative division to filter by access.
                        // Add left join to the current query to filter by division branches.
                        foreach (current($criteria[$fieldName]) as $contraField => $contraCond) {
                            $query->leftJoin(
                                't',
                                'administrative_division',
                                'adbranch',
                                $contraField.' = adbranch.id'
                            );
                        }
                        break;

                    default:
                        $field = $this->getFieldDefinition($fieldName);
                        // If is an entity reference entity translate the entity ID.
                        if (!$field instanceof CountryReferenceEntity && $field instanceof EntityReferenceFieldType) {
                            // Translate the entity ID.
                            if (is_array($value)) {
                                foreach ($value as $operator => &$operands) {
                                    if (is_array($operands)) {
                                        foreach ($operands as &$operand) {
                                            $ulid = Ulid::fromString($operand);
                                            $operand = $ulid->toBinary();
                                        }
                                    } else {
                                        if (!in_array($operator, ['not_exists', 'exists'])) {
                                            if (stripos($operands, '..') !== false) {
                                                $parts = explode('..', $operands);
                                                $ulid0 = Ulid::fromString($parts[0]);
                                                $ulid1 = Ulid::fromString($parts[1]);
                                                $operands = $ulid0->toBinary().' .. '.$ulid1->toBinary();
                                            } else {
                                                $ulid = Ulid::fromString($operands);
                                                $operands = $ulid->toBinary();
                                            }
                                        }
                                    }
                                }
                            } else {
                                $ulid = Ulid::fromString($value);
                                $value = $ulid->toBinary();
                            }
                        }
                        // $fieldName can come with the expanded name instead the real field name.
                        if ($field && $field->isMultivalued()) {
                            // Join table.
                            $query->leftJoin(
                                't',
                                $field->getFieldTableName(),
                                $field->getFieldTableAlias(),
                                $field->getFieldTableAlias().'.record = t.id'
                            );
                            $criteriaToUpdate[$fieldName] = [
                                $field->getFieldTableAlias().'.'.$fieldName => $value,
                            ];
                        }
                        break;
                }
            }
            foreach ($criteriaToUpdate as $fieldName => $criteriaUpdate) {
                unset($criteria[$fieldName]);
                if (!is_null($criteriaUpdate)) {
                    $criteria += $criteriaUpdate;
                }
            }
            // Add filters
            $first = true;
            foreach ($criteria as $fieldName => $value1) {
                if (strtolower($fieldName) === 'id') {
                    $ulid = Ulid::fromString($value1);
                    $value1 = $ulid->toBinary();
                }
                $query = $this->parseOperatorValue($first, $query, $fieldName, $value1, $isAnd);
                $first = false;
            }
        }

        return $query;
    }

    /**
     * Validate record before save in data base.
     *
     * @param array       $data     Data to validate.
     * @param bool        $toUpdate Validate how data to update a record.
     * @param ?FormRecord $record   Optional record to get field definitions.
     *
     * @return bool
     *
     * @throws \Exception
     */
    protected function isValidFieldValues(array &$data, bool $toUpdate, FormRecord $record = null): bool
    {
        $this->lastErrors = [];
        if ($record) {
            $fieldDefinitions = $record->getFieldDefinitions();
        } else {
            $fieldDefinitions = $this->getFieldDefinitions();
        }
        // Don't allow unknown fields.
        $diff = array_diff(array_keys($data), array_keys($fieldDefinitions));
        if (count($diff) > 0) {
            $this->lastErrors[] = $this->t(
                'Invalid field in insert: @id.',
                [
                    '@id' => implode(', ', $diff),
                ]
            );

            return false;
        }
        // Validate each field.
        foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
            if ($record) {
                $field = $record->getFieldDefinition($fieldName);
            } else {
                if (!isset($fieldDefinition['type'])) {
                    continue;
                }
                $field = $this->fieldTypeManager->create($fieldDefinition['type'], $fieldDefinition);
                $field->setFormManager($this);
            }
            $field->setFieldName($fieldName);
            if ($record) {
                // The field definition can be initialized with a wrong record.
                $field->setFormRecord($fieldName, $record);
            }
            if (!$field->isValidContent($data[$fieldName], $data)) {
                // Copy error message from field to the form.
                foreach ($field->getLastValidationMessages() as $message) {
                    $this->lastErrors[] = $message;
                }
            }
            if ($field->isRequired() && !isset($data[$fieldName])) {
                if (!$toUpdate) {
                    $this->lastErrors[] = $this->t(
                        '[@type] Field "@id" is required.',
                        [
                            '@type' => $field->getFieldType(),
                            '@id' => $field->getLabelExtended(),
                        ]
                    );
                }
            }
        }
        if (count($this->lastErrors) > 0) {
            return false;
        }

        return true;
    }

    /**
     * Insert multivalued fields.
     *
     * @param array $multivalued Multivalued fields.
     * @param Ulid  $recordUlid  Owner record Ulid.
     *
     * @return void
     *
     * @throws Exception
     * @throws \ReflectionException
     */
    protected function insertMultiValuedToDatabase(array $multivalued, Ulid $recordUlid): void
    {
        foreach ($multivalued as $fieldName => $fieldValue) {
            $field = $this->getFieldDefinition($fieldName);
            foreach ($fieldValue as $item) {
                $subUlid = new Ulid();
                $subData = [
                    'id' => $subUlid->toBinary(),
                    'record' => $recordUlid->toBinary(),
                ];
                if (count($field->getProperties()) > 1) {
                    foreach ($field->getProperties() as $pKey => $pValue) {
                        $subData[$field->getId().'_'.$pKey] = $item[$field->getId().'_'.$pKey];
                    }
                } else {
                    $subData[$fieldName] = $item;
                }
                $this->connection->insert($field->getFieldTableName(), $subData);
            }
        }
    }

    /**
     * Prepare field data to be saved in data base.
     *
     * @param FieldTypeInterface $field       Current field.
     * @param array              $data        Record data.
     * @param array              $multivalued Data collector to do future inserts in multivalued fields.
     *
     * @return void
     */
    protected function prepareFieldToDatabase(FieldTypeInterface $field, array &$data, array &$multivalued): void
    {
        $fieldName = $field->getId();
        if ($field->isMultivalued()) {
            if (!is_null($data[$fieldName])) {
                foreach ($data[$fieldName] as &$content) {
                    $field->preInsert($content);
                }
                $multivalued[$fieldName] = $data[$fieldName];
            }
            unset($data[$fieldName]);
        } else {
            $field->preInsert($data[$fieldName]);
            if (count($field->getProperties()) > 1) {
                foreach ($data[$fieldName] as $pName => $pValue) {
                    $data[$pName] = $pValue;
                }
                unset($data[$fieldName]);
            }
        }
    }

    /**
     * Find and join multi property fields.
     *
     * @param array $rawResults Raw results.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function unFlatRawResults(array $rawResults): array
    {
        $fields = $this->getFields();
        foreach ($rawResults as &$rawResult) {
            /**
             * Current field.
             *
             * @var FieldTypeInterface $field
             */
            foreach ($fields as $field) {
                if (!$field->isMultivalued()) {
                    $props = $field->getProperties();
                    if (count($props) > 1) {
                        $rawResult[$field->getId()] = $props;
                        foreach ($props as $pName => $prop) {
                            if (isset($rawResult[$field->getId().'_'.$pName])) {
                                $rawResult[$field->getId()][$pName] = $rawResult[$field->getId().'_'.$pName];
                                unset($rawResult[$field->getId().'_'.$pName]);
                            }
                        }
                    }
                }
            }
        }

        return $rawResults;
    }

    /**
     * Parse operator and value from a criteria.
     *
     * @param bool         $first     Is the first where?
     * @param QueryBuilder $query
     * @param string       $fieldName
     * @param mixed        $value
     * @param bool         $isAnd
     *
     * @return QueryBuilder
     *
     * @throws \ReflectionException
     */
    protected function parseOperatorValue(bool &$first, QueryBuilder $query, string $fieldName, mixed &$value, bool $isAnd = true): QueryBuilder
    {
        $parameters = [];
        $operator = '=';
        $conjuntion = 'andWhere';
        if (!$isAnd) {
            $conjuntion = 'orWhere';
        }
        // Special operators can come how field name.
        if (is_array($value) && ('exists' === $fieldName || 'not_exists' === $fieldName)) {
            // The operator is the field name.
            $nFieldName = key($value);
            $value[$fieldName] = $value[$nFieldName];
            unset($value[$nFieldName]);
            $fieldName = $nFieldName;
        }
        // Fix multi-properties fields' names.
        $originalFieldName = 'id';
        // Virtual fields like 'id' or 'branch' don't enter here.
        // $fieldName can come prefixed with the table name.
        if ('id' !== $fieldName && 'branch' !== $fieldName) {
            // Field name can have a table alias name.
            $fieldTableParts = explode('.', $fieldName);
            if (count($fieldTableParts) === 2) {
                $fieldName = $fieldTableParts[1];
            }
            $originalFieldName = $fieldName;
            $field = $this->getFieldDefinition($fieldName);
            if (!$field) {
                throw new \Exception(
                    $this->t(
                        '[@formid] @class::parseOperatorValue, unknown query field "@name".',
                        [
                            '@class' => get_class($this),
                            '@name' => $fieldName,
                            '@formid' => $this->getId(),
                        ]
                    )
                );
            }
            $fieldName = $field->getSortOrFilterTableColumn();
        }
        if (is_array($value)) {
            // Process operations.
            foreach ($value as $oper => $cond) {
                $setDefaultParameter = true;
                switch (strtolower($oper)) {
                    case '<>':
                    case '!=':
                    case 'distinct':
                        $operator = '<>';
                        break;
                    case 'gt':
                    case '>':
                    case 'strictly_after':
                        $operator = '>';
                        break;
                    case 'lt':
                    case '<':
                    case 'strictly_before':
                        $operator = '<';
                        break;
                    case 'lte':
                    case '<=':
                    case 'before':
                        $operator = '<=';
                        break;
                    case 'gte':
                    case '>=':
                    case 'after':
                        $operator = '>=';
                        break;
                    case 'between':
                        $setDefaultParameter = false;
                        $parts = explode('..', $cond);
                        if (isset($parts[0])) {
                            $paramId = $this->getQueryParamId();
                            $parameters[':'.$fieldName.$paramId] = $parts[0];
                            $field = $this->getFieldDefinition($fieldName);
                            $sqlField = $fieldName;
                            if ($field) {
                                $sqlField = 't.'.$fieldName;
                            }
                            $query = $query->{$conjuntion}($sqlField.' > :'.$fieldName.$paramId);
                        }
                        if (isset($parts[1])) {
                            $paramId = $this->getQueryParamId();
                            $parameters[':'.$fieldName.$paramId] = $parts[1];
                            $field = $this->getFieldDefinition($fieldName);
                            $sqlField = $fieldName;
                            if ($field) {
                                $sqlField = 't.'.$fieldName;
                            }
                            $query = $query->{$conjuntion}($sqlField.' < :'.$fieldName.$paramId);
                        }
                        break;
                    case 'exists':
                        $setDefaultParameter = false;
                        $operator = 'is NULL';
                        $field = $this->getFieldDefinition($fieldName);
                        $sqlField = $fieldName;
                        if ($field) {
                            $sqlField = 't.'.$fieldName;
                        }
                        $query = $query->{$conjuntion}($sqlField.' '.$operator);
                        break;
                    case 'not_exists':
                        $setDefaultParameter = false;
                        $operator = 'is not NULL';
                        $field = $this->getFieldDefinition($fieldName);
                        $sqlField = $fieldName;
                        if ($field) {
                            $sqlField = 't.'.$fieldName;
                        }
                        $query = $query->{$conjuntion}($sqlField.' '.$operator);
                        break;
                    case 'like':
                        $operator = 'like';
                        $cond = "%$cond%";
                        break;
                    case 'like_start':
                        $operator = 'like';
                        $cond = "$cond%";
                        break;
                    case 'like_end':
                        $operator = 'like';
                        $cond = "%$cond";
                        break;
                    case 'alike_start':
                        // IMPORTANT!! Only valid to administrative_division.branch field.
                        $setDefaultParameter = false;
                        $operator = 'like';
                        $aDql = [];
                        foreach ($cond as $contraField => $condBody) {
                            foreach ($condBody as $conditionItem) {
                                $paramId = $this->getQueryParamId();
                                $parameters[':'.$fieldName.$paramId] = $conditionItem.'%';
                                $paramKeys[] = ':'.$fieldName.$paramId;
                                $aDql[] = 'adbranch.'.$fieldName.' '.$operator.' '.':'.$fieldName.$paramId;
                            }
                        }
                        $query = $query->{$conjuntion}('('.implode(' || ', $aDql).' || ISNULL(adbranch.'.$fieldName.'))');
                        break;
                    case 'alike_end':
                        $setDefaultParameter = false;
                        $operator = 'like';
                        $aDql = [];
                        foreach ($cond as $conditionItem) {
                            $paramId = $this->getQueryParamId();
                            $parameters[':'.$fieldName.$paramId] = '%'.$conditionItem;
                            $paramKeys[] = ':'.$fieldName.$paramId;
                            $field = $this->getFieldDefinition($fieldName);
                            $sqlField = $fieldName;
                            if ($field) {
                                $sqlField = 't.'.$fieldName;
                            }
                            $aDql[] = $sqlField.' '.$operator.' '.':'.$fieldName.$paramId;
                        }
                        $query = $query->{$conjuntion}('('.implode(' || ', $aDql).')');
                        break;
                    case 'in':
                        $setDefaultParameter = false;
                        $operator = 'in';
                        $paramKeys = [];
                        foreach ($cond as $conditionItem) {
                            $paramId = $this->getQueryParamId();
                            $parameters[':'.$fieldName.$paramId] = $conditionItem;
                            // $parameters could content other operator parameters.
                            $paramKeys[] = ':'.$fieldName.$paramId;
                        }
                        $field = $this->getFieldDefinition($fieldName);
                        $sqlField = $fieldName;
                        if ($field) {
                            $sqlField = 't.'.$fieldName;
                        }
                        $query = $query->{$conjuntion}($sqlField.' '.$operator.' ('.implode(',', $paramKeys).')');
                        break;
                    case 'notin':
                        $setDefaultParameter = false;
                        $operator = 'not in';
                        $paramKeys = [];
                        foreach ($cond as $conditionItem) {
                            $paramId = $this->getQueryParamId();
                            $parameters[':'.$fieldName.$paramId] = $conditionItem;
                            // $parameters could content other operator parameters.
                            $paramKeys[] = ':'.$fieldName.$paramId;
                        }
                        $field = $this->getFieldDefinition($fieldName);
                        $sqlField = $fieldName;
                        if ($field) {
                            $sqlField = 't.'.$fieldName;
                        }
                        $query = $query->{$conjuntion}($sqlField.' '.$operator.' ('.implode(',', $paramKeys).')');
                        break;

                    // We need alert the user about unknown query operator.
                    default:
                        throw new \Exception($this->t('Unknown query operator "@op".', ['@op' => $oper]));
                }
                if ($setDefaultParameter) {
                    $value = $cond;
                    $paramId = $this->getQueryParamId();
                    $parameters[':'.$fieldName.$paramId] = $value;
                    $field = $this->getFieldDefinition($fieldName);
                    $sqlField = $fieldName;
                    if ($field) {
                        $sqlField = 't.'.$fieldName;
                    }
                    $query = $query->{$conjuntion}($sqlField.' '.$operator.' :'.$fieldName.$paramId);
                }
            }
        } else {
            $paramId = $this->getQueryParamId();
            // Calculate the right table alias to this field.
            $fieldName = $originalFieldName;
            $field = $this->getFieldDefinition($originalFieldName);
            $tableAlias = null;
            $expandedName = '';
            // $fieldName can come with the expanded name instead the real field name.
            if ($field && 'id' !== $fieldName) {
                $tableAlias = $field->getFieldTableAlias();
                // This field requires expand him name?
                if (count($field->getProperties()) > 1) {
                    $expandedName = '_'.$field->getPrimaryProperty();
                }
            }
            if (!$tableAlias) {
                $tableAlias = 't';
            }
            // Add condition.
            $parameters[':'.$fieldName.$paramId] = $value;
            $query = $query->{$conjuntion}($tableAlias.'.'.$fieldName.$expandedName.' '.$operator.' :'.$fieldName.$paramId);
        }
        foreach ($parameters as $key => $parameter) {
            $query->setParameter($key, $parameter);
        }

        return $query;
    }

    /**
     * Get unique ID usable in query parameters.
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function getQueryParamId(): string
    {
        return random_int(10000, 99999);
    }

    /**
     * Add where conditions.
     *
     * @param bool         $first Is the first 'where'?
     * @param QueryBuilder $query
     * @param string       $where
     *
     * @return QueryBuilder
     */
    protected function addWhere(bool &$first, QueryBuilder $query, string $where): QueryBuilder
    {
        if ($first) {
            $query->where($where);
            $first = false;
        } else {
            $query->andWhere($where);
        }

        return $query;
    }
}
