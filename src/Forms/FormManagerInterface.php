<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Forms\FieldTypes\FieldTypeInterface;
use Doctrine\DBAL\Driver\Exception as Driver_Exception;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;

/**
 * Forms entity manager interface.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
interface FormManagerInterface
{

    /**
     * Get form Id.
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Get form manager class.
     *
     * @return string
     */
    public function getType(): string;

    /**
     * Get form title.
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Set form title.
     *
     * @param string $title New form title.
     *
     * @return void
     */
    public function setTitle(string $title): void;

    /**
     * Get form description.
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Set form description.
     *
     * @param string $description New form description.
     *
     * @return void
     */
    public function setDescription(string $description): void;

    /**
     * Get if this manager has endpoint.
     *
     * The class FormManager have constants defined about
     * valid end-points types. The constants start with:
     * "ENDPOINT_"
     *
     * @param string $type Any of FormManager::ENDPOINT_???.
     *
     * @return bool
     */
    public function haveEndpoint(string $type): bool;

    /**
     * Get alternative FormCollectionAction method if defined.
     *
     * @see FormManagerInterface::haveEndpoint()
     *
     * @param string $type
     *
     * @return string|null
     */
    public function getEndpointMethod(string $type): string|null;

    /**
     * Get raw endpoint setting.
     *
     * @see FormManagerInterface::haveEndpoint()
     *
     * @param string $type
     *
     * @return string
     */
    public function getEndpointRawValue(string $type): mixed;

    /**
     * Must this form ignore required fields?
     *
     * @return bool
     *   Return TRUE if the required field attribute is disabled.
     */
    public function mustIgnoreRequiredFields(): bool;

    /**
     * Change ignore required fields status.
     *
     * This configuration is stored in the current instance only.
     *
     * @param bool $value TRUE to ignore required fields, FALSE to honor it.
     *
     * @return $this
     */
    public function setIgnoreRequiredFields(bool $value): self;

    /**
     * Save form definition.
     *
     * @return void
     */
    public function save(): void;

    /**
     * Add a new field in the form.
     *
     * @param string $fieldTypeId Field type.
     * @param string $fieldName   Unique field name.
     * @param string $fieldLabel  Field interface label.
     * @param array  $options     Field settings.
     *
     * @return ?FieldTypeInterface
     */
    public function addField(string $fieldTypeId, string $fieldName, string $fieldLabel, array $options = []): ?FieldTypeInterface;

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void;

    /**
     * Save and flush definition.
     *
     * @return void
     */
    public function saveNow(): void;

    /**
     * Is this form configurable?
     *
     * @return bool
     */
    public function isConfigurable(): bool;

    /**
     * Get form data base table name.
     *
     * @return string
     */
    public function getTableName(): string;

    /**
     * Is the form installed?
     *
     * @return bool
     */
    public function isInstalled(): bool;

    /**
     * Get field definitions.
     *
     * @return array
     */
    public function getFieldDefinitions(): array;

    /**
     * Get fields.
     *
     * @return FieldTypeInterface[]
     *
     * @throws \ReflectionException
     */
    public function getFields(): array;

    /**
     * Get field definition.
     *
     * @param string $name Field name.
     *
     * @return FieldTypeInterface|null
     *
     * @throws \ReflectionException
     */
    public function getFieldDefinition(string $name): ?FieldTypeInterface;

    /**
     * Is a form field?
     *
     * @param string $name Field name.
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function isField(string $name): bool;

    /**
     * Get form metadata.
     *
     * @return array
     */
    public function getMeta(): array;

    /**
     * Set form metadata.
     *
     * @param array $meta New metadata.
     *
     * @return void
     */
    public function setMeta(array $meta): void;

    /**
     * Get fields marked how sortable.
     *
     * @return FieldTypeInterface[]
     */
    public function getSortFields(): array;

    /**
     * Get fields marked how filterable.
     *
     * @return FieldTypeInterface[]
     */
    public function getFilterFields(): array;

    /**
     * Install form in data base.
     *
     * @return void
     */
    public function install(): void;

    /**
     * Get SQL to create/alter the field.
     *
     * @param array  $fieldDefinition The field definition.
     * @param string $fieldName       The field name.
     * @param array  $requiredIndexes List of new indexes to add. This method add the new index if required.
     *
     * @return string
     *  Empty if the new field is multivalued, and not processed.
     *
     * @throws \ReflectionException
     */
    public function getSqlInstallField(array $fieldDefinition, string $fieldName, array &$requiredIndexes): string;

    /**
     * Uninstall form in data base.
     *
     * @return void
     */
    public function uninstall(): void;

    /**
     * Create a new record without save it.
     *
     * @param array $data Record data.
     *
     * @return FormRecord
     */
    // public function createRecord(array $data): FormRecord;

    /**
     * Insert an new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string;

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void;

    /**
     * Drop a record.
     *
     * @param string $id Record ID.
     *
     * @return void
     */
    public function drop(string $id): void;

    /**
     * Finds an object by its primary key / identifier.
     *
     * @param string $id The identifier.
     *
     * @return FormRecord|null The object.
     *
     * @psalm-return FormRecord|null
     */
    public function find(string $id): ?FormRecord;

    /**
     * Finds all objects in the repository.
     *
     * @return array<int, array> The objects.
     *
     * @psalm-return array[]
     */
    public function findAll(): array;

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array         $criteria Filtering criteria.
     * @param string[]|null $orderBy  Sorting.
     * @param ?integer      $limit    Result limits.
     * @param ?integer      $offset   Result start offset.
     * @param ?array        $groupBy  Grouping.
     *
     * @return array[] The objects.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): array;

    /**
     * Build query by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array         $criteria Filtering criteria.
     * @param string[]|null $orderBy  Sorting.
     * @param ?integer      $limit    Result limits.
     * @param ?integer      $offset   Result start offset.
     * @param ?array        $groupBy  Grouping.
     *
     * @return QueryBuilder.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function queryBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): QueryBuilder;

    /**
     * Build record list by query.
     *
     * Important: The query must be generated by static::queryBy.
     *
     * @param QueryBuilder $query
     *
     * @return array
     */
    public function hydrateQuery(QueryBuilder $query): array;

    /**
     * Count objects by a set of criteria.
     *
     * @param array $criteria Filtering criteria.
     *
     * @return int
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function countBy(array $criteria): int;

    /**
     * Finds a single object by a set of criteria.
     *
     * @param mixed[] $criteria The criteria.
     *
     * @return FormRecord|null The object.
     *
     * @psalm-return FormRecord|null
     */
    public function findOneBy(array $criteria): ?FormRecord;

    /**
     * Allow to a form manager to update a form record data array before serialization.
     *
     * @param array      $data
     * @param FormRecord $record
     *
     * @return array
     */
    public function updateFormRecordArray(array $data, FormRecord $record): array;

    /**
     * Clone a form record.
     *
     * @param string $id
     *
     * @return string
     */
    public function clone(string $id): string;

    /**
     * Clone a form record.
     *
     * @param FormRecord $record
     *
     * @return string
     *
     * @throws \Exception
     */
    public function cloneRecord(FormRecord $record): string;
}
