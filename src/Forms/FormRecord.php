<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Forms\Event\FormRecordFormatEvent;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\Types\FormRecordReferenceFieldType;
use App\Traits\FormReferenceStartingTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\DBAL\Connection;
use Iterator;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Forms record handler simplifier.
 *
 * Multi valued fields will be read only if is used.
 *
 * Note: Field types don't content the record data, only work with it or have
 * default values.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormRecord implements FormReferenceEntityInterface, Iterator
{
    use StringTranslationTrait;
    use FormReferenceStartingTrait;

    protected FormManagerInterface $form;
    protected array $data;
    /**
     * The original content after instantiate this record.
     *
     * @var array
     */
    protected array $originalData;
    protected bool $isModified;
    protected array $modifiedFields;
    protected Connection $connection;
    protected array $fields;
    // This information will not be saved.
    protected array $meta;
    protected EventDispatcherInterface $dispatcher;
    // Used to dump complete record data, with subforms.
    protected array $processedIds = [];

    /**
     * Form record constructor.
     *
     * @param FormManagerInterface     $form       Owner form manager.
     * @param array                    $data       Record data.
     * @param Connection               $connection Database connection.
     * @param EventDispatcherInterface $dispatcher Event dispatcher.
     */
    public function __construct(FormManagerInterface $form, array $data, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        $this->connection = $connection;
        $this->dispatcher = $dispatcher;
        $this->form = $form;
        $this->data = $data;
        $this->originalData = [];
        $this->isModified = false;
        $this->modifiedFields = [];
        $this->fields = [];
        $this->meta = [];
    }

    /**
     * Lock original data after the first record load.
     *
     * @return void
     */
    public function setLoaded(): void
    {
//        if (!$this->isModified) {
//            return;
//        }
        $this->originalData = $this->data;
        $this->modifiedFields = [];
        $this->isModified = false;
    }

    /**
     * Set field type to this record.
     *
     * @param string             $fieldName Field name.
     * @param FieldTypeInterface $field     Field type.
     *
     * @return void
     */
    public function setFieldDefinition(string $fieldName, FieldTypeInterface $field): void
    {
        $this->fields[$fieldName] = $field;
        $field->setFormRecord($fieldName, $this);
    }

    /**
     * Get data just after loading this record, the original data.
     *
     * Multivalued field will be null.
     *
     * @return array
     */
    public function getOriginal(): array
    {
        return $this->originalData;
    }

    /**
     * Get the current record content data.
     *
     * @return array
     */
    public function getCurrentRaw(): array
    {
        // Get form fields.
        $fields = $this->form->getFields();
        // If a field is multivalued, get value to force load it.
        foreach ($fields as $field) {
            if ($field->isMultivalued()) {
                $this->get($field->getId());
            }
        }
        // Return complete data.
        return $this->data;
    }

    /**
     * Os this record modified?
     *
     * @return bool
     */
    public function isModified(): bool
    {
        return $this->isModified;
    }

    /**
     * Get modified fields.
     *
     * @return array
     */
    public function getModifiedFields(): array
    {
        return $this->modifiedFields;
    }

    /**
     * Rewind the Iterator to the first field.
     *
     * @link https://php.net/manual/en/iterator.rewind.php
     *
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        return reset($this->data);
    }

    /**
     * Return the current field.
     *
     * @link https://php.net/manual/en/iterator.current.php
     *
     * @return mixed Can return any type.
     */
    public function current(): mixed
    {
        return current($this->data);
    }

    /**
     * Return the key of the current field.
     *
     * @link https://php.net/manual/en/iterator.key.php
     *
     * @return string|float|int|bool|null scalar on success, or null on failure.
     */
    public function key(): float|bool|int|string|null
    {
        return key($this->data);
    }

    /**
     * Move forward to next field.
     *
     * @link https://php.net/manual/en/iterator.next.php
     *
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        return next($this->data);
    }

    /**
     * Checks if current position is valid.
     *
     * @link https://php.net/manual/en/iterator.valid.php
     *
     * @return bool The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid(): bool
    {
        return key($this->data) !== null;
    }

    /**
     * Magic getter.
     *
     * Note: If multivalued field is not loaded then load it wrong, and fail.
     *
     * @param string $name Field name.
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public function __get(string $name): mixed
    {
        if ('id' === $name) {
            return (isset($this->data['id']) ? $this->data['id'] : null);
        }

        $field = $this->getFieldDefinition($name);
        if ($field) {
            if ($field instanceof FormRecordTargetInterface) {
                if ($field->isMultivalued()) {
                    // Before this, $this->data[$name] may be null,
                    // by that we force load field values.
                    $this->get($name);
                    // Get targets.
                    $response = [];
                    if (null !== $this->data[$name]) {
                        foreach ($this->data[$name] as $content) {
                            if (count($field->getProperties()) === 1) {
                                $response[] = $field->getTarget([key($field->getProperties()) => $content]);
                            } else {
                                $response[] = $field->getTarget($content);
                            }
                        }
                    }

                    return $response;
                } else {
                    return $field->getTarget($this->data[$name]);
                }
            }

            return $this->get($name);
        }

        return null;
    }

    /**
     * Get internal field properties.
     *
     * Note: If multivalued field is not loaded then load it.
     *
     * @param string $name Field name.
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public function get(string $name): mixed
    {
        if ('id' === $name) {
            return $this->id;
        }
        $field = $this->getFieldDefinition($name);
        if ($field) {
            if ($field->isMultivalued()) {
                if (!is_null($this->data[$name])) {
                    return $this->data[$name];
                }
                // Query auxiliary table.
                $query = $this->connection->createQueryBuilder()
                    ->select('*')
                    ->from($field->getFieldTableName(), 't');
                $query->where('t.record = :record');
                $ulid = Ulid::fromString($this->getId());
                $query->setParameter(':record', $ulid->toBinary());
                $rawResult = $query->execute()->fetchAllAssociative();
                // un-flat fields and set values.
                $props = $field->getProperties();
                foreach ($rawResult as $raw) {
                    if (count($props) > 1) {
                        $item = $props;
                        foreach ($props as $pName => $prop) {
                            $item[$pName] = $raw[$name.'_'.$pName];
                            unset($raw[$name.'_'.$pName]);
                        }
                        $this->data[$name][] = $item;
                    } else {
                        $this->data[$name][] = $raw[$name];
                    }
                }
                // Return values.
                return $this->data[$name];
            } else {
                $props = $field->getProperties();
                if (count($props) === 1) {
                    return $this->data[$name][key($props)];
                }

                return $this->data[$name];
            }
        }

        return null;
    }

    /**
     * Magic setter.
     *
     * @param string $name  Field name.
     * @param mixed  $value Field value.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function __set(string $name, mixed $value): void
    {
        if ('id' === $name) {
            $ulid = Ulid::fromString($value);
            if ($this->id !== null) {
                if ($this->id !== $ulid->toBase32()) {
                    throw new \Exception($this->t('You should not change a record ID by hand.'));
                }
            }
            $this->data['id'] = $ulid->toBase32();
        } else {
            $field = $this->getFieldDefinition($name);
            if ($field) {
                $props = $field->getProperties();
                if ($field->isMultivalued()) {
                    if (is_null($value)) {
                        $this->data[$name] = null;
                    } else {
                        $this->data[$name] = [];
                        foreach ($value as $valueData) {
                            $item = [];
                            if (count($props) === 1) {
                                $item[key($props)] = $field->formatToDatabase(key($props), $valueData, [key($props) => $valueData]);
                            } else {
                                $item = $valueData;
                                if (is_array($item) && in_array(key($props), array_keys($item))) {
                                    foreach ($item as $pKey => &$pValue) {
                                        $item[$pKey] = $field->formatToDatabase($pKey, $pValue, $item);
                                    }
                                }
                            }
                            if ($field instanceof FormRecordTargetInterface) {
                                $item = $field->setSource($item);
                            }
                            $this->data[$name][] = $item;
                        }
                    }
                } else {
                    if (count($props) === 1) {
                        $this->data[$name][key($props)] = $field->formatToDatabase(key($props), $value, [key($props) => $value]);
                    } else {
                        $this->data[$name] = $value;
                        if (!is_null($this->data[$name])) {
                            foreach ($this->data[$name] as $pKey => &$pValue) {
                                $this->data[$name][$pKey] = $field->formatToDatabase($pKey, $pValue, $this->data[$name]);
                            }
                        }
                    }
                    if ($field instanceof FormRecordTargetInterface) {
                        $this->data[$name] = $field->setSource($this->data[$name]);
                    }
                }
                $this->setModifiedField($name);
            }
        }
    }

    /**
     * Field raw setter.
     *
     * @param string $name  Field name.
     * @param mixed  $value Field value.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function set(string $name, mixed $value): void
    {
        $field = $this->getFieldDefinition($name);
        if ($field) {
            $this->data[$name] = $field->setRaw($value);
            $this->setModifiedField($name);
        }
    }

    /**
     * Get if field exist.
     *
     * @param string $name Field name.
     *
     * @return bool
     */
    public function __isset(string $name): bool
    {
        if ('id' === $name) {
            return true;
        }

        return isset($data[$name]);
    }

    /**
     * Have this form this field?
     *
     * @param string $name The field name.
     *
     * @return bool True if the form have the field.
     */
    public function haveField(string $name): bool
    {
        if (!isset($this->fields[$name])) {
            return false;
        }

        return true;
    }

    /**
     * Get field definition.
     *
     * @param string $name Field name.
     *
     * @return FieldTypes\FieldTypeInterface|null
     */
    public function getFieldDefinition(string $name): ?FieldTypes\FieldTypeInterface
    {
        if (!isset($this->fields[$name])) {
            throw new \Exception(
                $this->t(
                    'Field "@name" not defined in "@form".',
                    [
                        '@name' => $name,
                        '@form' => $this->getForm()->getId(),
                    ]
                )
            );
        }

        return $this->fields[$name];
    }

    /**
     * Get field definitions.
     *
     * @return array
     */
    public function getFieldDefinitions(): array
    {
        $resp = [];
        /**
         * Fields name and type.
         *
         * @var string             $name  Field name.
         * @var FieldTypeInterface $field Field type.
         */
        foreach ($this->fields as $name => $field) {
            $resp[$name] = $field->getSettings();
        }

        return $resp;
    }

    /**
     * Get fields.
     *
     * @return FieldTypeInterface[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Get owner form.
     *
     * @return FormManagerInterface|null
     */
    public function getForm(): ?FormManagerInterface
    {
        return $this->form;
    }

    /**
     * Id.
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Save this record.
     *
     * @return void
     */
    public function save(): void
    {
        // A record can clone himself in an update.
        $me = $this;
        $this->form->update($me);
        // This change the current ID if the update generates a clone.
        $this->data = $me->data;
        // After update in database:
        // Update the original data array.
        $this->originalData = $this->data;
        // The current data is the original data.
        $this->isModified = false;
        $this->modifiedFields = [];
        $this->setLoaded();
    }

    /**
     * Create a clone from this record.
     *
     * @return FormRecord
     */
    public function clone(): FormRecord
    {
        $cRid = $this->form->cloneRecord($this);

        return $this->form->find($cRid);
    }

    /**
     * Delete this record.
     *
     * @return void
     */
    public function delete(): void
    {
        $this->form->drop($this->id);
    }

    /**
     * Get record how array.
     *
     * @return array
     *
     * @throws \Exception
     */
    public function toArray(): array
    {
        $resp = [];
        foreach ($this as $fieldName => $value) {
            if ('id' !== $fieldName) {
                $field = $this->getFieldDefinition($fieldName);
            }
            // Get internal data forcing load multivalued data.
            if ('id' !== $fieldName && $field->isMultivalued()) {
                $this->{$fieldName};
            }
            $resp[$fieldName] = $this->getCurrentRaw()[$fieldName];
            // Format data.
            if ('id' !== $fieldName && $field->isMultivalued()) {
                if (is_array($resp[$fieldName])) {
                    foreach ($resp[$fieldName] as &$item) {
                        if (is_array($item) && count($item) === 1) {
                            // Simple values must be direct.
                            $item = current($item);
                        } else {
                            // Flat data.
                            $item = $field->formatToUse($item, true);
                            if (is_array($item)) {
                                $item['meta'] = $field->formatExtraJson($item, true);
                            }
                        }
                    }
                }
            } else {
                // Simple values must be direct.
                if (is_array($resp[$fieldName]) && count($resp[$fieldName]) === 1) {
                    $resp[$fieldName] = current($resp[$fieldName]);
                } else {
                    if ('id' !== $fieldName) {
                        // Flat data.
                        $resp[$fieldName] = $field->formatToUse($resp[$fieldName]);
                        $resp[$fieldName]['meta'] = $field->formatExtraJson($resp[$fieldName]);
                    }
                }
            }
        }
        $resp['form'] = $this->form->getId();

        return $this->form->updateFormRecordArray($resp, $this);
    }

    /**
     * Get record how a Json string.
     *
     * @return string
     *
     * @throws \Exception
     */
    public function toJson(): string
    {
        $resp = $this->toArray();

        return json_encode($resp, JSON_THROW_ON_ERROR);
    }

    /**
     * Get temporal metadata.
     *
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * Set temporal metadata.
     *
     * @param array $value Meta data.
     *
     * @return $this
     */
    public function setMeta(array $value): self
    {
        $this->meta = $value;

        return $this;
    }

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array
    {
        return $this->toArray() + ['type' => static::class];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        /** @var FormRecordFormatEvent $event */
        $event = $this->dispatcher->dispatch(new FormRecordFormatEvent($this, $forceString), 'formrecord.format.'.$this->form->getId());

        return $event->getFormat();
    }

    /**
     * Get all processed IDs by the dump() method.
     *
     * @return array Processed IDs.
     */
    public function getDumpedIds(): array
    {
        return $this->processedIds;
    }

    /**
     * Get form record dump.
     *
     * @param FormRecord|null $record Internal use only, we don't must set a value.
     *
     * @return array Record meta data and content.
     *
     * @throws \Exception
     */
    public function dump(?FormRecord $record = null): array
    {
        if (!$record) {
            $record = $this;
            $this->processedIds = [];
        }
        $resp = [
            'subforms' => [],
            'meta' => [
                'form' => $record->getForm()->getId(),
                'id' => $record->getId(),
            ],
            'data' => [],
        ];
        foreach ($record as $fieldName => $value) {
            if ('id' !== $fieldName) {
                $field = $record->getFieldDefinition($fieldName);
                if ($field instanceof FormRecordReferenceFieldType) {
                    if ($field->isMultivalued()) {
                        $values = $record->{$fieldName};
                        foreach ($values as $subValue) {
                            if ($subValue) {
                                if (!isset($this->processedIds[$subValue->getId()])) {
                                    $this->processedIds[$subValue->getId()] = $subValue->getId();
                                    $resp['subforms'][$subValue->getId()] = $this->dump($subValue);
                                }
                            }
                        }
                    } else {
                        $value = $record->{$fieldName};
                        if ($value && !isset($this->processedIds[$value->getId()])) {
                            $this->processedIds[$value->getId()] = $value->getId();
                            $resp['subforms'][$value->getId()] = $this->dump($value);
                        }
                    }
                }
            }
            // Get internal data forcing load multivalued data.
            if ('id' !== $fieldName && $field->isMultivalued()) {
                $record->{$fieldName};
            }
            $raw = $record->getCurrentRaw();
            $resp['data'][$fieldName] = $raw[$fieldName];
            // Format data.
            if ('id' !== $fieldName && $field->isMultivalued()) {
                if (is_array($resp['data'][$fieldName])) {
                    foreach ($resp['data'][$fieldName] as &$item) {
                        if (is_array($item) && count($item) === 1) {
                            // Simple values must be direct.
                            $item = current($item);
                        } else {
                            // Flat data.
                            $item = $field->formatToUse($item, true);
                        }
                    }
                }
            } else {
                // Simple values must be direct.
                if (is_array($resp['data'][$fieldName]) && count($resp['data'][$fieldName]) === 1) {
                    $resp['data'][$fieldName] = current($resp['data'][$fieldName]);
                } else {
                    if ('id' !== $fieldName) {
                        // Flat data.
                        $resp['data'][$fieldName] = $field->formatToUse($resp['data'][$fieldName]);
                    }
                }
            }
        }

        return $resp;
    }

    /**
     * Mark the record how modified.
     *
     * @param string $fieldName
     */
    protected function setModifiedField(string $fieldName): void
    {
        $this->isModified = true;
        if (!in_array($fieldName, $this->modifiedFields)) {
            $this->modifiedFields[] = $fieldName;
        }
    }
}
