<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

/**
 * Interface to add entity compatibility with entity reference field types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @property bool $starting
 */
interface FormReferenceEntityInterface
{
    /**
     * Id.
     *
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * Get a representative array.
     *
     * @return array
     */
    public function serializeToLog(): array;

    /**
     * Format data that it's derivative from the current field value.
     *
     * @param bool $forceString Is required a string response values?
     *
     * @return array
     */
    public function formatExtraJson(bool $forceString = false): array;

    /**
     * Is this instance a default parametric?
     *
     * @return bool
     */
    public function isDefaultParametric(): bool;

    /**
     * Set how default parametric.
     *
     * @param bool $value Is it default parametric?
     *
     * @return $this
     */
    public function setDefaultParametric(bool $value): self;

    /**
     * Is this instance working while starting the system?
     *
     * @return bool
     */
    public function isStarting(): bool;

    /**
     * Set starting mode.
     *
     * @param bool $value Is it starting?
     *
     * @return $this
     */
    public function setStarting(bool $value): self;
}
