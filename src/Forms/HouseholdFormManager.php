<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Entity\Configuration\ConfigurationReadInterface;
use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Entity\User;
use App\Forms\Event\InquiryFormManagerEvent;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Security\Handlers\HouseholdAccessHandler;
use App\Service\SessionService;
use App\Tools\InquiryFinishChecks;
use App\Traits\GetContainerTrait;
use App\Traits\InquiryFormLoggerTrait;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Forms entity manager.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class HouseholdFormManager extends ChangedFormManager
{
    use GetContainerTrait;
    use InquiryFormLoggerTrait;

    protected ?SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected ?EntityManager $entityManager;
    protected ?FormFactory $formFactory;
    protected HouseholdAccessHandler $haouseholdAccessHandler;

    /**
     * FormManager constructor.
     *
     * @param ConfigurationReadInterface $config           Form configuration.
     * @param FieldTypeManager           $fieldTypeManager Field type manager.
     * @param Connection                 $connection       Database connection.
     * @param EventDispatcherInterface   $dispatcher       Event dispatcher.
     */
    public function __construct(ConfigurationReadInterface $config, FieldTypeManager $fieldTypeManager, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($config, $fieldTypeManager, $connection, $dispatcher);
        $this->sessionService = static::getContainerInstance()->get('session_service');
        $accessManager = static::getContainerInstance()->get('access_manager');
        $this->haouseholdAccessHandler = new HouseholdAccessHandler($accessManager);
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->formFactory = static::getContainerInstance()->get('form_factory');
    }

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        parent::addBaseFields();

        $this->addField(
            'country_reference',
            'country',
            'Country',
            [
                'required' => true,
                'sort' => true,
                'filter' => true,
            ]
        )
            ->setDescription('Owner country.')
            ->setIndexable(true)
            ->setInternal(true);

        $this->addField(
            'household_process_reference',
            'household_process',
            'Household Process',
            [
                'required' => false,
                'country_field' => 'field_country',
            ]
        )
            ->setDescription('Related household process.')
            ->setMeta(['disabled' => true])
            ->setInternal(true);
    }

    /**
     * Get parent form ID.
     *
     * @return string
     */
    public function getParentForm(): string
    {
        if (isset($this->confValues['meta']['parent_form'])) {
            return $this->confValues['meta']['parent_form'];
        }

        return '';
    }

    /**
     * @inheritDoc
     */
    public function queryBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): QueryBuilder
    {
        /** @var User $user */
        $user = $this->getCurrentUser();
        $this->haouseholdAccessHandler->checkFormAccess($user, HouseholdAccessHandler::ACCESS_ACTION_READ, ['country' => $user->getCountry()]);

        return parent::queryBy($this->updateReadCriteria($criteria), $orderBy, $limit, $offset, $groupBy);
    }

    /**
     * Insert a new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        // Prepare log.
        $sourceData = $data;
        // We need the user to verify permissions.
        $user = $this->getCurrentUser();

        // Force country.
        if (!isset($data['field_country']) || empty($data['field_country'])) {
            if (!$user->getCountry()) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] This form require user with country set.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            // Set the user country how the record country.
            $data['field_country'] = $user->getCountry();
        }

        // Apply security management.
        $context = [
            'country' => '',
            'division' => null,
        ];
        if ($data['field_country'] instanceof Country) {
            $context['country'] = $data['field_country']->getId();
        } elseif (is_string($data['field_country'])) {
            $context['country'] = $data['field_country'];
            $data['field_country'] = $this->entityManager->getRepository(Country::class)->find($data['field_country']);
        } elseif (is_array($data['field_country']) && isset($data['field_country']['value'])) {
            $context['country'] = $data['field_country']['value'];
        } else {
            throw new \Exception(
                $this->t(
                    '[@class] I don\'t know how get the country code to check permissions.',
                    [
                        '@class' => self::class,
                    ]
                )
            );
        }
        $context['country'] = $this->entityManager->getRepository(Country::class)->find($context['country']);
        $this->haouseholdAccessHandler->checkFormAccess($user, HouseholdAccessHandler::ACCESS_ACTION_CREATE, $context);

        $this->setIgnoreRequiredFields(true);

        // Action.
        $id = parent::insert($data);
        // Logger.
//        $this->info('Insert household record', ['form_id' => $this->getId(), 'record_id' => $id, 'received' => $sourceData]);

        return $id;
    }

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update. If have a 'id' key it will be used instead the $id parameter.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$force) {
            /** @var HouseholdProcess $process */
            $processId = $record->get('field_household_process');
            $processRepo = $this->entityManager->getRepository($processId['class']);
            $process = $processRepo->find($processId['value']);

            if ($process && !$process->isOpen()) {
                throw new \Exception(self::t('The process is closed.'));
            }

            if (!$record->isModified()) {
                return;
            }
            $user = $this->getCurrentUser();

            // Apply security management.
            $context = [
                'country' => $record->{'field_country'},
                'division' => null,
            ];
            $this->haouseholdAccessHandler->checkFormAccess($user, HouseholdAccessHandler::ACCESS_ACTION_UPDATE, $context);

            if ($record->{'field_finished'}) {
                // Execute validation.
                $validator = new InquiryFinishChecks();
                $valid = $validator->checkInquiry($record);
                if (!$valid) {
                    $msg = $this->t("Validation at finish failed.");
                    $logs = $validator->getInquiryLogs($record);
                    foreach ($logs as $log) {
                        $msg .= "\n".$log->getMessage();
                    }
                    throw new \Exception($msg);
                }
            }

            $this->setIgnoreRequiredFields(!$record->{'field_finished'});

            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.subform.record.preupdate');
        }

        // Update.
        parent::update($record, $force);

        if (!$force) {
            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.subform.record.postupdate');
            // Logger.
//            $this->info('Update household record', ['form_id' => $this->getId(), 'record_id' => $record->{'id'}, 'updates' => $record->toArray()]);
        }
    }

    /**
     * Drop a record.
     *
     * @param string $id Record ID.
     *
     * @return void
     *
     * @throws Exception
     */
    public function drop(string $id): void
    {
        // Validate.
        $record = $this->find($id);
        if (!$record) {
            throw new \Exception(
                $this->t(
                    '[@class::drop] Record "@id" not found.',
                    [
                        '@class' => self::class,
                        '@id' => $id,
                    ]
                )
            );
        }

        // Apply security management.
        $user = $this->getCurrentUser();
        $context = [
            'country' => $record->{'field_country'},
            'division' => $record->{'field_community'},
        ];

        $this->haouseholdAccessHandler->checkFormAccess($user, HouseholdAccessHandler::ACCESS_ACTION_DELETE, $context);

        // We can't drop in closed household precess.
        $process = $record->{'field_household_process'};
        if ($process && !$process->isOpen()) {
            throw new \Exception(
                $this->t(
                    '[@class::drop] Record "@id" in a closed process.',
                    [
                        '@class' => self::class,
                        '@id' => $id,
                    ]
                )
            );
        }

        // Apply.
        parent::drop($id);
        // Logger.
//        $this->info('Deleted household record', ['form_id' => $record->getForm()->getId(), 'record_id' => $id]);
    }

    /**
     * Update filter criteria with security criteria.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function updateReadCriteria(array $criteria): array
    {
        /** @var User $user */
        $user = $this->getCurrentUser();
        // A user with 'all permissions' permission can read all.
        // A user with 'all countries' permission can read all.
        if ($this->haouseholdAccessHandler->hasPermission($user, 'all permissions', [$user->getCountry()]) || $this->haouseholdAccessHandler->hasPermission($user, 'all countries', [$user->getCountry()])) {
            return $criteria;
        }
        // A user only can read her country records.
        $criteria['field_country'] = $user->getCountry()->getCode();

        return $criteria;
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    protected function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@form_id] This form require user session.',
                    ['@form_id' => $this->getId()]
                )
            );
        }

        return $user;
    }
}
