<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Entity\AdministrativeDivision;
use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationReadInterface;
use App\Entity\Country;
use App\Forms\Event\InquiryFormManagerEvent;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use App\Repository\ConfigurationRepository;
use App\Security\Handlers\InquiryAccessHandler;
use App\Service\SessionService;
use App\Tools\InquiryFinishChecks;
use App\Tools\TranslatableMarkup;
use App\Traits\GetContainerTrait;
use App\Traits\InquiryFormLoggerTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception as Driver_Exception;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

/**
 * SIASAR inquiry form manager.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @todo     To check valid data we must call a external class
 * @todo     selected by a configuration property, to be
 * @todo     compatible with type checks.
 */
class InquiryFormManager extends ChangedFormManager
{
    use GetContainerTrait;
//    use InquiryFormLoggerTrait;

    protected ?EntityManager $entityManager;
    protected ?SessionService $sessionService;
    protected ?FormFactory $formFactory;
    protected InquiryAccessHandler $accessHandler;

    /**
     * Flag to allow external code to lock, or not, editors field update.
     *
     * Default true.
     *
     * @var bool
     */
    protected bool $allowUpdateEditorsField;
    protected LoggerInterface $inquiryFormLogger;

    /**
     * FormManager constructor.
     *
     * @param ConfigurationReadInterface $config           Form configuration.
     * @param FieldTypeManager           $fieldTypeManager Field type manager.
     * @param Connection                 $connection       Database connection.
     * @param EventDispatcherInterface   $dispatcher       Event dispatcher.
     */
    public function __construct(ConfigurationReadInterface $config, FieldTypeManager $fieldTypeManager, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($config, $fieldTypeManager, $connection, $dispatcher);
        $this->sessionService = $this->getContainerInstance()->get('session_service');
        $accessManager = $this->getContainerInstance()->get('access_manager');
        $this->accessHandler = new InquiryAccessHandler($accessManager);
        $this->entityManager = $this->getContainerInstance()->get('doctrine')->getManager();
        $this->allowUpdateEditorsField = true;
        $this->inquiryFormLogger = $this->getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->formFactory = static::getContainerInstance()->get('form_factory');

        if ($this->isConfigurable()) {
            // Update requirements.
            $fields = $this->getFields();
            $updated = false;
            /** @var FieldTypeInterface $field */
            foreach ($fields as $field) {
                if ('subform_reference' === $field->getFieldType()) {
                    // Get subform and update requirements.
                    $this->confValues['requires'][] = $field->getSubFormId();
                    $updated = true;
                }
            }
            if (isset($this->confValues['requires'])) {
                // Clean duplicate requirements.
                $aux = [];
                foreach ($this->confValues['requires'] as $required) {
                    $aux[$required] = true;
                }
                $this->confValues['requires'] = array_keys($aux);
            }

            if ($updated) {
                $this->saveNow();
            }
        }
    }

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        parent::addBaseFields();

        $this->addField(
            'country_reference',
            'country',
            'Country',
            [
                'required' => true,
                'sort' => true,
                'filter' => true,
            ]
        )
            ->setDescription('Owner country.')
            ->setIndexable(true)
            ->setInternal(true);

        // Add administrative division.
        $this->addField(
            'administrative_division_reference',
            'region',
            'Administrative division',
            [
                'required' => true,
                'sort' => true,
                'filter' => true,
                'country_field' => 'field_country',
            ]
        )
            ->setDescription('Owner administrative division.')
            ->setIndexable(true)
            ->setInternal(false);

        // Add digitizer user.
//        $this->addField('user_reference', 'digitizer', 'Digitizer', ['sort' => true, 'filter' => true])
//            ->setDescription('Assigned digitizer user.')
//            ->setIndexable(true)
//            ->setInternal(true);

        // Add creator user.
        $this->addField('user_reference', 'creator', 'Creator', ['sort' => true, 'filter' => true])
            ->setDescription('Author user.')
            ->setIndexable(true)
            ->setInternal(true);

        // Users that did change in this record.
        $this->addField('user_reference', 'editors', 'Editors', ['multivalued' => true])
            ->setDescription('Users that did change in this record.')
            ->setInternal(true);

        $this->addField('date', 'editors_update', 'Editors update dates', ['multivalued' => true])
            ->setDescription('When users that did change in this record.')
            ->setInternal(true);

        // User custom record ID.
        // todo detect if the user change de reference ID and/or it's not used before.
        $this->addField('short_text', 'reference', 'Reference ID', [
            'required' => true,
            'sort' => true,
            'filter' => true,
        ])
            ->setDescription('Management reference ID.')
            ->setIndexable(true)
            ->setInternal(false);

        $this->addField('ulid', 'ulid_reference', 'Internal reference ID', [
            'required' => true,
            'sort' => true,
            'filter' => true,
        ])
            ->setDescription('Internal management reference ID.')
            ->setIndexable(true)
            ->setInternal(true);

        $this->addField('boolean', 'validated', 'Validated mark')
            ->setDescription('This record had or have take validated state.')
            ->setInternal(true);

        $this->addField('boolean', 'deleted', 'Logical delete', [
            'sort' => true,
            'filter' => true,
        ])
            ->setDescription('This record was deleted.')
            ->setIndexable(true)
            ->setInternal(true);

        // Workflow status.
        $this->addField('inquiring_status', 'status', 'Inquiry status', [
            'sort' => true,
            'filter' => true,
        ])
            ->setDescription('Current inquiry status.')
            ->setIndexable(true)
            ->setInternal(true);
    }

    /**
     * Insert a new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        // Prepare log.
        $sourceData = $data;
        // We need the user to verify permissions.
        $user = $this->getCurrentUser();

        // Force country.
        if (!isset($data['field_country']) || empty($data['field_country'])) {
            if (!$user->getCountry()) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] This form require user with country set.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            // Set the user country how the record country.
            $data['field_country'] = $user->getCountry();
        }

        // Set current user how author.
        if (!isset($data['field_creator']) || empty($data['field_creator'])) {
            // Set the user country how the record country.
            $data['field_creator'] = $user;
        }
        $data['field_editors'] = [$user];
        $data['field_editors_update'] = [new \DateTime()];
        if (!isset($data['field_ulid_reference']) || empty($data['field_ulid_reference'])) {
            $data['field_ulid_reference'] = (new Ulid())->toBase32();
            $data['field_validated'] = false;
            $data['field_deleted'] = false;
        }
        if (!isset($data['field_reference']) || empty($data['field_reference'])) {
            $data['field_reference'] = uniqid();
        }

        // Apply security management.
        $context = [
            'country' => '',
            'division' => null,
        ];
        if ($data['field_country'] instanceof Country) {
            $context['country'] = $data['field_country']->getId();
        } elseif (is_string($data['field_country'])) {
            $context['country'] = $data['field_country'];
            $data['field_country'] = $this->entityManager->getRepository(Country::class)->find($data['field_country']);
        } elseif (is_array($data['field_country']) && isset($data['field_country']['value'])) {
            $context['country'] = $data['field_country']['value'];
        } else {
            throw new \Exception(
                $this->t(
                    '[@class] I don\'t know how get the country code to check permissions.',
                    [
                        '@class' => self::class,
                    ]
                )
            );
        }
        $context['country'] = $this->entityManager->getRepository(Country::class)->find($context['country']);
        $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_CREATE, $context);

        if (!isset($data['field_status'])) {
            $data['field_status'] = 'draft';
        }
        if (is_array($data['field_status'])) {
            $data['field_status'] = $data['field_status']['value'];
        }
        $this->allowEmptyRequiredInDraft($data['field_status']);
        // Dispatch preCreate event
        $event = new InquiryFormManagerEvent(
            $this,
            InquiryFormManagerEvent::ACTION_CREATE,
            $sourceData
        );
        $this->dispatcher->dispatch($event, 'form.inquiry.record.precreate');
        // Action.
        $id = parent::insert($data);
        // Dispatch postCreate event
        $sourceData['id'] = $id;
        $event = new InquiryFormManagerEvent(
            $this,
            InquiryFormManagerEvent::ACTION_CREATE,
            $sourceData
        );
        $this->dispatcher->dispatch($event, 'form.inquiry.record.postcreate');
        // Logger.
//        $this->info('Insert form record', ['form_id' => $this->getId(), 'record_id' => $id, 'received' => $sourceData]);

        return $id;
    }

    /**
     * Clone a form record.
     *
     * @param FormRecord $record
     *
     * @return string
     *
     * @throws \Exception
     */
    public function cloneRecord(FormRecord $record): string
    {
        if ('locked' !== $record->{'field_status'}) {
            $record->{'field_status'} = 'locked';
        }
        $record->save();

        // Create a new record.
        $newId = parent::cloneRecord($record);

        // Dispatch event after clone record
        $event = new InquiryFormManagerEvent(
            $this,
            InquiryFormManagerEvent::ACTION_CLONE,
            [
                'oldId' => $record->getId(),
                'newId' => $newId,
            ]
        );
        $this->dispatcher->dispatch($event, 'form.inquiry.record.postclone');

        return $newId;
    }

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update. If have a 'id' key it will be used instead the $id parameter.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$force) {
            if (!$record->isModified()) {
                return;
            }
            $this->stopLockedOrRemoved($record, 'updated');
            $user = $this->getCurrentUser();
            $modifiedFields = $record->getModifiedFields();

            // Don't allow change internal management ID.
            if ($record->{'field_ulid_reference'} !== $record->getOriginal()['field_ulid_reference']['value']) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] The field "field_ulid_reference" can\'t be updated.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            // Don't allow change internal country reference.
            if ($record->get('field_country')['value'] !== $record->getOriginal()['field_country']['value']) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] The field "field_country" can\'t be updated.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }

            // Don't allow change administrative division.
            $original = $record->getOriginal();
            if ('' !== $record->get('field_region')['value'] && '' === $original['field_region']['value']) {
                // First updating division.
                if (!$this->accessHandler->canUserUseDivision($user, $record->{'field_region'})) {
                    $fieldRegion = $this->getFieldDefinition('field_region');
                    throw new \Exception(
                        $this->t(
                            'You can\'t modify "@field". The value is outside of your administrative divisions.',
                            [
                                '@field' => $fieldRegion->getLabelExtended(),
                            ]
                        )
                    );
                }
            } elseif ('' === $record->get('field_region')['value'] && '' === $original['field_region']['value']) {
                // Saving without division, and not updating.
            } else {
                // Changing division.
                if ((is_null($record->{'field_region'}) xor is_null($original['field_region'])) || ($record->get('field_region')['value'] !== $original['field_region']['value'])) {
                    // The user is updating the region field. He must change to another own region, or have the update permission.
                    $divisionsRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
                    $division = $record->{'field_region'};
                    if (!$division || !$this->accessHandler->canUserUseDivision($user, $division)) {
                        // They can't use this region by limits, we must check permissions.
                        $division = null;
                        if (isset($original['field_region']['value'])) {
                            $division = $divisionsRepo->find($original['field_region']['value']);
                        }
                        $context = [
                            'country' => $this->entityManager->getRepository(Country::class)->find($original['field_country']['value']),
                            'division' => $division,
                        ];
                        $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_UPDATE_REGION, $context);
                    }
                }
            }

            // Apply security management.
            /** @var AdministrativeDivision $originalRegion */
            $originalRegion = $record->{'field_region'};
            $context = [
                'country' => $record->{'field_country'},
                'division' => $originalRegion ?? null,
            ];

            $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_UPDATE, $context);

            // Update editor users list.
            $user = $this->sessionService->getUser();
            if (!$user) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] This form require user session.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            $this->updateEditorsField($user, $record);

            // If it takes state validated, update the mark.
            if ('validated' === $record->{'field_status'}) {
                $record->{'field_validated'} = true;
                // Calculate indicator.
                if (!$this->isPointAssociated()) {
                    $class = $this->getIndicatorClass();
                    if (!empty($class)) {
                        $context = new SimpleIndicatorContext($record);
                        $indicator = new $class($context);
                        $indicator();
                    }
                }
            }

            // Status control and automatic changes.
            switch ($record->getOriginal()['field_status']['value']) {
                case 'draft':
                    // Check data before go to finished.
                    // Logic moved to src/EventListener/InquiryWorkflowSubscriber.php
                    // if (in_array('field_status', $modifiedFields)) {
                    //     if ('finished' === $record->{'field_status'}) {
                    //         // Execute validation.
                    //         $validator = new InquiryFinishChecks();
                    //         $valid = $validator->checkInquiry($record);
                    //         if (!$valid) {
                    //             // Don't throw exception to save inquirylogs during transaction
                    //             $msg = $this->t("Validation at finish failed.");

                    //             return;
                    //         }
                    //     }
                    // }
                    break;
                case 'finished':
                    // Move record to draft state.
                    $changeToDraft = false;
                    foreach ($modifiedFields as $fieldName) {
                        if ('field_status' !== $fieldName) {
                            $changeToDraft = true;
                        }
                    }
                    if ($changeToDraft) {
                        $record->{'field_status'} = 'draft';
                    }
                    break;
                case 'validated':
                    if ('validated' === $record->{'field_status'} && !$record->{'field_deleted'}) {
                        $record->{'field_status'} = 'draft';
//                    parent::update($record);
//                    $record = $record->clone();
                    }
                    break;
                case 'locked':
                case 'removed':
                    // Lock changes to these states.
                    $allowed = true;
                    foreach ($modifiedFields as $fieldName) {
                        $field = $this->getFieldDefinition($fieldName);
                        if (!$field->isInternal() && 'field_status' !== $fieldName) {
                            $allowed = false;
                            break;
                        }
                    }
                    if (!$allowed) {
                        $msg = $this->t("Records in locked, or removed states can't be updated.");
//                        $this->error($msg, $record->toArray());
                        throw new \Exception($msg);
                    }
                    break;
            }
            // Draft records ignore required attributes.
            $this->setIgnoreRequiredFields($record->{'field_status'} === 'draft');

//        try {
//            $this->entityManager->beginTransaction();
            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.inquiry.record.preupdate');
        }

        parent::update($record, $force);

        if (!$force) {
            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.inquiry.record.postupdate');
//            $this->entityManager->commit();
//        } catch (\Exception $e) {
//            $this->entityManager->rollback();
//            throw $e;
//        }

            // Logger.
//            $this->info('Update form record', ['form_id' => $this->getId(), 'record_id' => $record->{'id'}, 'updates' => $record->toArray()]);
        }
    }

    /**
     * Drop a record.
     *
     * @param string $id Record ID.
     *
     * @return void
     *
     * @throws Exception
     */
    public function drop(string $id): void
    {
        $record = $this->find($id);
        $form = $record->getForm();
        if (!$record) {
            throw new \Exception(
                $this->t(
                    '[@class::drop] Record "@id" not found.',
                    [
                        '@class' => self::class,
                        '@id' => $id,
                    ]
                )
            );
        }
        $this->stopLockedOrRemoved($record, 'deleted');
        $user = $this->getCurrentUser();

        // Apply security management.
        $context = [
            'country' => $record->{'field_country'},
            'division' => $record->{'field_region'},
        ];

        $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_DELETE, $context);

        // todo Draft or Finished: Creator or administrator can delete.
        // todo Validated: Validator, Sectorial or administrator => Creator can't delete.

        // Set logical deleted mark if it's have 'field_validated' mark.
        if ($record->{'field_validated'}) {
            // Logical drop.
            $record->{'field_deleted'} = true;
            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.inquiry.record.preupdate');

            $record->save();

            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.inquiry.record.postupdate');
            // Logger.
//            $this->info('Logical deleted form record', ['form_id' => $form->getId(), 'record_id' => $id]);
        } else {
//            try {
//                $this->entityManager->beginTransaction();
                // Dispatch event
                $event = new InquiryFormManagerEvent(
                    $this,
                    InquiryFormManagerEvent::ACTION_DELETE,
                    [],
                    $record
                );
                $this->dispatcher->dispatch($event, 'form.inquiry.record.predrop');
                // todo Drop subform records too.
                parent::drop($id);

                $event = new InquiryFormManagerEvent(
                    $this,
                    InquiryFormManagerEvent::ACTION_DELETE,
                    [],
                    $record
                );
                $this->dispatcher->dispatch($event, 'form.inquiry.record.postdrop');
//                $this->entityManager->commit();
//            } catch (\Exception $e) {
//                $this->entityManager->rollback();
//                throw $e;
//            }

            // Logger.
//            $this->info('Deleted form record', ['form_id' => $form->getId(), 'record_id' => $id]);
        }
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array         $criteria Filtering criteria.
     * @param string[]|null $orderBy  Sorting.
     * @param ?integer      $limit    Result limits.
     * @param ?integer      $offset   Result start offset.
     * @param ?array        $groupBy  Grouping.
     *
     * @return array[] The objects.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): array
    {
        $user = $this->getCurrentUser();
        $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_READ, ['country' => $user->getCountry()]);

        // Add security management criteria and filter output.
        return parent::findBy($this->updateReadCriteria($criteria), $orderBy, $limit, $offset, $groupBy);
    }

    /**
     * Count objects by a set of criteria.
     *
     * @param array $criteria Filtering criteria.
     *
     * @return int
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function countBy(array $criteria): int
    {
        // Add security management criteria and filter output.
        return parent::countBy($this->updateReadCriteria($criteria));
    }

    /**
     * Don't allow that the update record method adding current user to editors.
     */
    public function stopEditorsFieldUpdate(): void
    {
        $this->allowUpdateEditorsField = false;
    }

    /**
     * Allow that the update record method adding current user to editors.
     */
    public function allowEditorsFieldUpdate(): void
    {
        $this->allowUpdateEditorsField = true;
    }

    /**
     * Throw exception on locked or removed records.
     *
     * @param FormRecord $record Record to test.
     * @param string     $action Action label.
     */
    public static function stopLockedOrRemoved(FormRecord $record, string $action): void
    {
        switch ($record->getOriginal()['field_status']['value']) {
            case 'locked':
            case 'removed':
                throw new \Exception(
                    new TranslatableMarkup('Locked or removed records can\'t be @action.', ['@action' => $action])
                );
        }
    }

    /**
     * Update filter criteria with security criteria.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function updateReadCriteria(array $criteria): array
    {
        $user = $this->getCurrentUser();
        if (isset($criteria['field_region'])) {
            $ulid = Ulid::fromString($criteria['field_region']);
            $criteria['field_region'] = $ulid->toBinary();
        }
        // A user with 'all permissions' permission can read all.
        // A user with 'all countries' permission can read all.
        if ($this->accessHandler->hasPermission($user, 'all permissions', [$user->getCountry()]) || $this->accessHandler->hasPermission($user, 'all countries', [$user->getCountry()])) {
            return $criteria;
        }
        // A user only can read her country records.
        $criteria['field_country'] = $user->getCountry()->getCode();
        // A user only can read her administrative division records.
        // A user with 'all administrative divisions' permission, or without divisions,
        // can read all records in her country.
        if (!$this->accessHandler->hasPermission($user, 'all administrative divisions', [$user->getCountry()])) {
            $limits = $this->accessHandler->getUserDivisionsLimits($user);
            if (!empty($limits)) {
                // We need add multiple condition over the branch field.
                // 'branch' is a special field, like 'id'.
                // We need view unassigned divisions too.
                if (!isset($criteria['or'])) {
                    $criteria['or'] = [];
                    $criteria['or']['or'] = [];
                }

                $criteria['or']['branch'] = ['alike_start' => ['branch' => $limits]];
                $criteria['or']['or']['field_region'] = ['not_exists' => []];
                $criteria['or']['or']['branch'] = ['not_exists' => []];
            }
        }

        return $criteria;
    }

    /**
     * Is this form type associated by a SIASAR point.
     *
     * @return bool
     */
    public function isPointAssociated(): bool
    {
        if (!isset($this->confValues['meta']['point_associated'])) {
            return false;
        }

        return $this->confValues['meta']['point_associated'];
    }

    /**
     * Get indicator class to this form type, or empty.
     *
     * @return string
     */
    public function getIndicatorClass(): string
    {
        if (!isset($this->confValues['meta']['indicator_class'])) {
            return '';
        }

        return $this->confValues['meta']['indicator_class'];
    }

    /**
     * @inheritDoc
     */
    public function updateFormRecordArray(array $data, FormRecord $record): array
    {
        if ($this->isPointAssociated()) {
            /** @var PointFormManager $pointManager */
            $pointManager = $this->formFactory->find('form.point');
            switch ($record->getForm()->getId()) {
                case 'form.community':
                    $point = current($pointManager->findByCommunity($record->getId()));
                    break;
                case 'form.wsprovider':
                    $point = current($pointManager->findByWsp($record->getId()));
                    break;
                case 'form.wssystem':
                    $point = current($pointManager->findByWSystem($record->getId()));
                    break;
            }
            if ($point) {
                $data['point'] = $point->getId();
                $data['point_status'] = $point->{'field_status'};
            }
        }
        // Not required in a regular record conversion.
        if ('validated' === $data['field_status']) {
            if (!$this->isPointAssociated()) {
                $class = $this->getIndicatorClass();
                if (!empty($class)) {
                    $context = new SimpleIndicatorContext($record);
                    /** @var AbstractIndicator $indicator */
                    $indicator = new $class($context);
                    $data['indicator'] = $indicator->getValue();
                }
            }
        }

        return $data;
    }

    /**
     * Update editors list with a new user.
     *
     * @param UserInterface $user
     * @param FormRecord    $record
     */
    protected function updateEditorsField(UserInterface $user, FormRecord $record): void
    {
        if (!$this->allowUpdateEditorsField) {
            return;
        }

        $editors = $record->{'field_editors'};
        $editors[] = $user;
        $record->{'field_editors'} = $editors;

        $editors = $record->{'field_editors_update'};
        $editors[] = new \DateTime();
        $record->{'field_editors_update'} = $editors;
    }

    /**
     * If 'draft' status change not internal and required fields to
     * not required.
     *
     * @param string $status
     */
    protected function allowEmptyRequiredInDraft(string $status): void
    {
        if ('draft' !== $status) {
            $this->setIgnoreRequiredFields(false);

            return;
        }

        $this->setIgnoreRequiredFields(true);
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    protected function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@form_id] This form require user session.',
                    ['@form_id' => $this->getId()]
                )
            );
        }

        return $user;
    }

    /**
     * Get a clean copy of record field values.
     *
     * @param FormRecord $record
     *
     * @return array
     */
    protected function getRecordInternalValues(FormRecord $record): array
    {
        // Get current values.
        $internals = parent::getRecordInternalValues($record);
        unset($internals['field_creator']);
        unset($internals['field_editors']);
        unset($internals['field_editors_update']);
        unset($internals['field_validated']);
        unset($internals['field_deleted']);
        unset($internals['field_status']);
        // Call each field to clone content.
        foreach ($internals as $fieldName => $value) {
            $field = $record->getForm()->getFieldDefinition($fieldName);
            $internals[$fieldName] = $field->cloneContent();
        }

        return $internals;
    }
}
