<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Entity\Configuration\ConfigurationReadInterface;
use App\Entity\Country;
use App\Entity\User;
use App\Entity\UserReadMail;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Security\Handlers\BaseAccessHandler;
use App\Security\Handlers\MailAccessHandler;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception as Driver_Exception;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Internal mail system.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class MailFormManager extends ChangedFormManager
{
    use GetContainerTrait;
    protected ?SessionService $sessionService;
    protected ?EntityManager $entityManager;
    protected ?FormFactory $formFactory;
    protected ?MailAccessHandler $accessHandler;

    /**
     * FormManager constructor.
     *
     * @param ConfigurationReadInterface $config           Form configuration.
     * @param FieldTypeManager           $fieldTypeManager Field type manager.
     * @param Connection                 $connection       Database connection.
     * @param EventDispatcherInterface   $dispatcher       Event dispatcher.
     */
    public function __construct(ConfigurationReadInterface $config, FieldTypeManager $fieldTypeManager, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($config, $fieldTypeManager, $connection, $dispatcher);
        $this->sessionService = static::getContainerInstance()->get('session_service');
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->formFactory = static::getContainerInstance()->get('form_factory');
        $accessManager = $this->getContainerInstance()->get('access_manager');
        $this->accessHandler = new MailAccessHandler($accessManager);
    }

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        parent::addBaseFields();

        $this->addField(
            'country_reference',
            'country',
            'Country',
            [
                'required' => true,
                'sort' => true,
                'filter' => true,
            ]
        )
            ->setDescription('Owner country.')
            ->setIndexable(true)
            ->setInternal(true);

        $this->addField('user_reference', 'from', 'Remitter', [
            'required' => true,
            'multivalued' => false,
            'sort' => true,
            'filter' => true,
        ])
            ->setIndexable(true)
            ->setInternal(true);

        $this->addField('user_reference', 'to', 'Recipients', [
            'required' => false,
            'multivalued' => true,
            'sort' => false,
            'filter' => false,
        ])
            ->setIndexable(false)
            ->setInternal(false);

        $this->addField('short_text', 'subject', 'Subject', [
            'required' => true,
            'sort' => true,
            'filter' => true,
        ])
            ->setIndexable(true)
            ->setInternal(false);

        $this->addField('long_text', 'body', 'Body', [
            'required' => true,
            'sort' => true,
            'filter' => true,
        ])
            ->setDescription('Allow Markdown format.')
            ->setIndexable(true)
            ->setInternal(false);

        $this->addField('boolean', 'is_read', 'Read')
            ->setDescription('This record was read.')
            ->setInternal(false);

        $this->addField('boolean', 'is_child', 'Child', [
            'filter' => true,
        ])
            ->setDescription('This record is thread child.')
            ->setIndexable(true)
            ->setInternal(false);

        $this->addField('boolean', 'global', 'Global')
            ->setDescription('This record is a global message, without recipients')
            ->setInternal(true);

        $this->addField('form_record_reference', 'thread', 'Thread', [
            'required' => false,
            'multivalued' => false,
            'sort' => false,
            'filter' => false,
        ])
            ->setIndexable(true)
            ->setInternal(true);

        $this->addField('file_reference', 'attachments', 'Attachments', [
            'required' => false,
            'multivalued' => true,
            'sort' => false,
            'filter' => false,
        ])
            ->setIndexable(false)
            ->setInternal(false);
    }

    /**
     * @inheritDoc
     */
    public function queryBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): QueryBuilder
    {
        /** @var User $user */
        $user = $this->getCurrentUser();
        $this->accessHandler->checkUse($user, ['country' => $user->getCountry()]);

        return parent::queryBy($this->updateReadCriteria($criteria), $orderBy, $limit, $offset, $groupBy);
    }

    /**
     * Count objects by a set of criteria.
     *
     * @param array $criteria Filtering criteria.
     *
     * @return int
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function countBy(array $criteria): int
    {
        return parent::countBy($this->updateReadCriteria($criteria));
    }

    /**
     * Insert a new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        // Prepare log.
        $sourceData = $data;
        unset($data['field_created']);
        unset($data['field_changed']);
        // We need the user to verify permissions.
        $user = $this->getCurrentUser();

        // Force country.
        if (!isset($data['field_country']) || empty($data['field_country']) || empty($data['field_country']['value'])) {
            if (!$user->getCountry()) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] This form require user with country set.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            // Set the user country how the record country.
            $data['field_country'] = $user->getCountry();
        }

        // Apply security management.
        $context = [
            'country' => '',
            'division' => null,
        ];
        if ($data['field_country'] instanceof Country) {
            $context['country'] = $data['field_country']->getId();
        } elseif (is_string($data['field_country'])) {
            $context['country'] = $data['field_country'];
            $data['field_country'] = $this->entityManager->getRepository(Country::class)->find($data['field_country']);
        } elseif (is_array($data['field_country']) && isset($data['field_country']['value'])) {
            $context['country'] = $data['field_country']['value'];
        } else {
            throw new \Exception(
                $this->t(
                    '[@class] I don\'t know how get the country code to check permissions.',
                    [
                        '@class' => self::class,
                    ]
                )
            );
        }
        $context['country'] = $this->entityManager->getRepository(Country::class)->find($context['country']);
        $this->accessHandler->checkUse($user, $context);

        if (!isset($data['field_is_child']) || $data['field_is_child']['value'] !== '1') {
            if ((!isset($data['field_global']) || $data['field_global']['value'] === '0') && count($data['field_to']) <= 0) {
                throw new \Exception($this->t('Recipients are required.'));
            }
        }
        $this->setIgnoreRequiredFields(false);

        // Action.
        $id = parent::insert($data);
        // Add read mark by default to sender.
        $record = $this->find($id);
        $mark = new UserReadMail();
        $mark->setMail($record->getId());
        $mark->setUser($user);
        $mark->setIsChild($record->{'field_is_child'});
        $readMarkRepo = $this->entityManager->getRepository(UserReadMail::class);
        $readMarkRepo->saveNow($mark);

        return $id;
    }

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update. If have a 'id' key it will be used instead the $id parameter.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$force) {
            if (!$record->isModified()) {
                return;
            }
            $user = $this->getCurrentUser();

            // Apply security management.
            $context = [
                'country' => $record->{'field_country'},
                'division' => null,
            ];
            $this->accessHandler->checkUse($user, $context);
            // $this->haouseholdAccessHandler->checkFormAccess($user, HouseholdAccessHandler::ACCESS_ACTION_UPDATE, $context);

            if (in_array('field_thread', $record->getModifiedFields())) {
                // Remove read mark to this message, a new thread is added.
                $readMarkRepo = $this->entityManager->getRepository(UserReadMail::class);
                $marks = $readMarkRepo->findBy([
                    'mail' => $record->getId(),
                ]);
                $threads = $record->{'field_thread'};
                foreach ($threads as $thread) {
                    $marks = array_merge($marks, $readMarkRepo->findBy([
                        'mail' => $thread->getId(),
                    ]));
                }
                foreach ($marks as $mark) {
                    $readMarkRepo->remove($mark);
                }
                $this->entityManager->flush();
            }

            $this->setIgnoreRequiredFields(false);
        }

        // Update.
        parent::update($record, $force);

        if (!$force) {
            // Post-update actions go here.
        }
    }

    /**
     * Drop a record.
     *
     * @param string $id Record ID.
     *
     * @return void
     *
     * @throws Exception
     */
    public function drop(string $id): void
    {
        // Validate.
        $record = $this->find($id);
        if (!$record) {
            throw new \Exception(
                $this->t(
                    '[@class::drop] Record "@id" not found.',
                    [
                        '@class' => self::class,
                        '@id' => $id,
                    ]
                )
            );
        }

        // Apply security management.
        $user = $this->getCurrentUser();
        $context = [
            'country' => $record->{'field_country'},
            'division' => null,
        ];
        $this->accessHandler->checkUse($user, $context);
        // $this->haouseholdAccessHandler->checkFormAccess($user, HouseholdAccessHandler::ACCESS_ACTION_DELETE, $context);

        // Apply.
        parent::drop($id);
    }

    /**
     * Update filter criteria with security criteria.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function updateReadCriteria(array $criteria): array
    {
        /** @var User $user */
        $user = $this->getCurrentUser();
        $criteriaOriginal = $criteria;
        // A user with 'all permissions' permission can read all.
        // A user with 'all countries' permission can read all.
//        if ($this->haouseholdAccessHandler->hasPermission($user, 'all permissions', [$user->getCountry()]) || $this->haouseholdAccessHandler->hasPermission($user, 'all countries', [$user->getCountry()])) {
//            return $criteria;
//        }
        // A user only can read her country records.
        $criteria['field_country'] = $user->getCountry()->getCode();
        $criteria['unique'] = true;

        // A user only can see their own messages, sent to him or global messages.
        // Client query conditions:
        // One message: 'id' => <record id>
        if (1 === count($criteriaOriginal) && isset($criteriaOriginal['id'])) {
            $ulid = Ulid::fromString($criteriaOriginal['id']);
            $criteria['id'] = $ulid->toBinary();
            $criteria['or'] = [
                'field_to' => $user->getId(),
                'field_from' => $user->getId(),
                'field_is_child' => '1',
                'field_global' => '1',
            ];
        } elseif (1 === count($criteriaOriginal) && isset($criteriaOriginal['field_is_child']) && "0" === $criteriaOriginal['field_is_child']) {
            // All case: field_is_child = "0"
            $criteria['or'] = [
                'field_to' => $user->getId(),
                'field_from' => $user->getId(),
                'field_global' => '1',
            ];
            $criteria['field_is_child'] = 0;
        } elseif (2 === count($criteriaOriginal) &&
            isset($criteriaOriginal['field_is_child']) &&
            "0" === $criteriaOriginal['field_is_child'] &&
            isset($criteriaOriginal['field_to']) &&
            $user->getId() === $criteriaOriginal['field_to']) {
            // Send to me case: 'field_to' => <me>, field_is_child = "0"
            $criteria['or'] = [
                'field_to' => $user->getId(),
                'field_from' => $user->getId(),
                'field_global' => '1',
            ];
            $criteria['field_is_child'] = 0;
        }
        // Send by me case: 'field_from' => <me>, field_is_child = "0"

        return $criteria;
    }

    /**
     * @inheritDoc
     */
    protected function parseCriteria(array $criteria, QueryBuilder $query, bool $isAnd = true): QueryBuilder
    {
        if ($isAnd && isset($criteria['field_to'])) {
            $fieldTo = $criteria['field_to'];
            unset($criteria['field_to']);
            $criteria['or'] = [
                'field_to' => $fieldTo,
                'field_global' => '1',
            ];
        }

        return parent::parseCriteria($criteria, $query, $isAnd);
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    protected function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@form_id] This form require user session.',
                    ['@form_id' => $this->getId()]
                )
            );
        }
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->find($user->getId());

        return $user;
    }
}
