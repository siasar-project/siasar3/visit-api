<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use ApiPlatform\Core\Bridge\Symfony\Routing\IriConverter;
use App\Entity\Configuration\ConfigurationReadInterface;
use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Security\Handlers\PointAccessHandler;
use App\Service\SessionService;
use App\Tools\SiasarPointWrapper;
use App\Traits\GetContainerTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

/**
 * SIASAR Point form manager.
 */
class PointFormManager extends ChangedFormManager
{
    use GetContainerTrait;

    protected FormFactory $formFactory;
    protected IriConverter $iriConverter;
    protected PointAccessHandler $accessHandler;
    protected ?SessionService $sessionService;
    protected ?EntityManager $entityManager;

    /**
     * FormManager constructor.
     *
     * @param ConfigurationReadInterface $config           Form configuration.
     * @param FieldTypeManager           $fieldTypeManager Field type manager.
     * @param Connection                 $connection       Database connection.
     * @param EventDispatcherInterface   $dispatcher       Event dispatcher.
     */
    public function __construct(ConfigurationReadInterface $config, FieldTypeManager $fieldTypeManager, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($config, $fieldTypeManager, $connection, $dispatcher);
        $this->formFactory = $this->getContainerInstance()->get('form_factory');
        $this->sessionService = $this->getContainerInstance()->get('session_service');
        $this->entityManager = $this->getContainerInstance()->get('doctrine')->getManager();
        $accessManager = $this->getContainerInstance()->get('access_manager');
        $this->accessHandler = new PointAccessHandler($accessManager);
    }

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        parent::addBaseFields();

        $this->addField('country_reference', 'country', 'Country', [
            'required' => true,
        ])
            ->setInternal(true);

        $this->addField('community_reference', 'communities', 'Community inquiries', [
            'multivalued' => true,
            'required' => true,
            'country_field' => 'field_country',
        ])
            ->setInternal(true);

        $this->addField('wsprovider_reference', 'wsps', 'W.S.P.s inquiries', [
            'multivalued' => true,
            'required' => false,
            'country_field' => 'field_country',
        ])
            ->setInternal(true);

        $this->addField('wsystem_reference', 'wsystems', 'Water Systems inquiries', [
            'multivalued' => true,
            'required' => false,
            'country_field' => 'field_country',
        ])
            ->setInternal(true);

        $this->addField('user_reference', 'equip', 'Human equip', [
            'multivalued' => true,
        ])
            ->setInternal(true);

        $this->addField('pointing_status', 'status', 'Phase')
            ->setInternal(true);

        // This is a lazy version note. It's referred to other SIASAR Point from the first community
        // referred.
        // Unused, currently the version is the creation date.
        $this->addField('integer', 'version', 'Version')
            ->setInternal(true);
    }

    /**
     * Insert a new Point.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        /** @var User $user */
        $user = $this->getCurrentUser();

        // Force country.
        if (!isset($data['field_country']) || empty($data['field_country'])) {
            if (!$user->getCountry()) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] This form require user with country set.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            // Set the user country how the record country.
            $data['field_country'] = $user->getCountry();
        }

        if ($data['field_country'] instanceof Country) {
            $context['country'] = $data['field_country']->getId();
        } elseif (is_string($data['field_country'])) {
            $context['country'] = $data['field_country'];
            $data['field_country'] = $this->entityManager->getRepository(Country::class)->find($data['field_country']);
        } elseif (is_array($data['field_country']) && isset($data['field_country']['value'])) {
            $context['country'] = $data['field_country']['value'];
        } else {
            throw new \Exception(
                $this->t(
                    '[@class] I don\'t know how get the country code to check permissions.',
                    [
                        '@class' => self::class,
                    ]
                )
            );
        }

        // Apply security management.
        $context['country'] = $this->entityManager->getRepository(Country::class)->find($context['country']);
        $context['division'] = null;
        $this->accessHandler->checkFormAccess($user, PointAccessHandler::ACCESS_ACTION_CREATE, $context);

        // Action.
//        $this->connection->beginTransaction();
//        try {
            $id = parent::insert($data);
//            $this->connection->commit();
//        } catch (\Exception $e) {
//            $this->connection->rollback();
//            throw $e;
//        }

        return $id;
    }

    /**
     * Update Siasar Point.
     *
     * @param FormRecord $record Data to update.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$force) {
            if (!$record->isModified()) {
                return;
            }

            // Check access
            $user = $this->getCurrentUser();
            $context = [
                'country' => $record->{'field_country'},
                'division' => null,
            ];
            $this->accessHandler->checkFormAccess($user, PointAccessHandler::ACCESS_ACTION_UPDATE, $context);

            $modifiedFields = $record->getModifiedFields();

            // Status control
            switch ($record->getOriginal()['field_status']['value']) {
                case 'planning':
                    // Only is allowed to modify the SP if field_chk_to_plan is checked
                    foreach ($modifiedFields as $key => $fieldName) {
                        if ('field_chk_to_plan' === $fieldName) {
                            unset($modifiedFields[$key]);
                        }
                    }
                    if (count($modifiedFields) > 0 && !$record->{'field_chk_to_plan'}) {
                        throw new \Exception($this->t("Only is allowed to modify checks while planning."));
                    }
                    break;
            }
        }

        parent::update($record, $force);
    }

    /**
     * Drop a Siasar Point.
     *
     * @param string $id Siasar Point ID.
     *
     * @return void
     *
     * @throws Exception
     */
    public function drop(string $id): void
    {
        $point = $this->find($id);
        if (!$point) {
            throw new \Exception(
                $this->t(
                    '[@class::drop] Record "@id" not found.',
                    [
                        '@class' => self::class,
                        '@id' => $id,
                    ]
                )
            );
        }

        // Apply security management.
        $user = $this->getCurrentUser();
        $context = [
            'country' => $point->{'field_country'},
            'division' => null,
        ];
        $this->accessHandler->checkFormAccess($user, PointAccessHandler::ACCESS_ACTION_DELETE, $context);

        if ('calculating' === $point->{'field_status'} || 'calculated' === $point->{'field_status'}) {
            throw new \Exception($this->t("Point cannot be removed."), 422);
        }

        $wspForm = $this->formFactory->find('form.wsprovider');
        $wsystemForm = $this->formFactory->find('form.wssystem');
        $communityForm = $this->formFactory->find('form.community');

//        $this->connection->beginTransaction();
//        try {
        foreach ($point->{'field_wsps'} as $wsp) {
            /** @var FormRecord $wsp */
            $wspForm->drop($wsp->getId());
        }

        foreach ($point->{'field_wsystems'} as $wsystem) {
            /** @var FormRecord $wsystem */
            $wsystemForm->drop($wsystem->getId());
        }

        foreach ($point->{'field_communities'} as $community) {
            /** @var FormRecord $community */
            $communityForm->drop($community->getId());
        }

            parent::drop($point->getId());
//            $this->connection->commit();
//        } catch (\Exception $e) {
//            $this->connection->rollback();
//            throw $e;
//        }
    }

    /**
     * Find point by a WSSystem record.
     *
     * @param string $id
     * @param bool   $singleResult True to limit by 1
     *
     * @return array
     */
    public function findByWSystem(string $id, bool $singleResult = true): array
    {
        /** @var Ulid $ulid */
        $ulid = Ulid::fromString($id);

        return $this->findBy(
            [
                'field_wsystems' => $ulid->toBinary(),
            ],
            ['field_changed' => 'DESC'],
            $singleResult ? 1 : null
        );
    }

    /**
     * Find point by a community record.
     *
     * @param string $id
     * @param bool   $singleResult True to limit by 1
     *
     * @return array
     */
    public function findByCommunity(string $id, bool $singleResult = true): array
    {
        $ulid = Ulid::fromString($id);

        return $this->findBy(
            [
                'field_communities' => $ulid->toBinary(),
            ],
            ['field_changed' => 'DESC'],
            $singleResult ? 1 : null
        );
    }

    /**
     * Find point by a WSP record.
     *
     * @param string $id
     * @param bool   $singleResult True to limit by 1
     *
     * @return array
     */
    public function findByWsp(string $id, bool $singleResult = true): array
    {
        $ulid = Ulid::fromString($id);

        return $this->findBy(
            [
                'field_wsps' => $ulid->toBinary(),
            ],
            ['field_changed' => 'DESC'],
            $singleResult ? 1 : null
        );
    }

    /**
     * Get a list of Points with the same WSP associated (search by 'field_ulid_reference').
     *
     * Note: The list contains all versions
     *
     * @param string $id          Point ID
     * @param bool   $alivePoints
     *
     * @return array
     */
    public function findByWspRef(string $id, bool $alivePoints = true): array
    {
        $ulidRef = Ulid::fromString($id);
        /** @var QueryBuilder $query */
        $query = $this->connection->createQueryBuilder()
            ->select('p.*')
            ->from($this->getTableName(), 'p')
            ->leftJoin(
                'p',
                $this->getTableName().'__field_wsps',
                'fpfp',
                'p.id = fpfp.record',
            )
            ->leftJoin(
                'fpfp',
                'form_wsprovider',
                'wsp',
                'fpfp.field_wsps_value = wsp.id',
            );
        // $query->andWhere($query->expr()->eq('wsp.field_ulid_reference', '"'.$ulid->toBinary().'"'));
        $query->andWhere($query->expr()->eq('HEX(wsp.field_ulid_reference)', '"'.strtoupper(bin2hex($ulidRef->toBinary())).'"'));

        if ($alivePoints) {
            $query->andWhere($query->expr()->notIn('p.field_status', ['"calculating"', '"calculated"']));
        }
        $query->orderBy('p.field_changed_value', 'DESC');

        $resp = $this->hydrateQuery($query);

        return $resp;
    }

    /**
     * Build base query to get the versions of a Point
     *
     * @param FormRecord $point Point Record
     *
     * @return QueryBuilder
     */
    public function buildBaseQueryVersions(FormRecord $point): QueryBuilder
    {
        $communityRefs = [];
        foreach ($point->{'field_communities'} as $community) {
            $ulidRef = Ulid::fromString($community->{'field_ulid_reference'});
            $communityRefs[] = '"'.strtoupper(bin2hex($ulidRef->toBinary())).'"';
        }

        $query = $this->connection->createQueryBuilder()
            ->select(
                'ad.id as ad_id',
                'fp.id as point',
                'fp.field_status'
            )
            ->from($this->getTableName(), 'fp');
        // Join form_point__field_communities.
        $query->leftJoin('fp', 'form_point__field_communities', 'fpfc', 'fp.id = fpfc.record');
        // Join form community.
        $query->leftJoin('fpfc', 'form_community', 'fc', "fc.id = fpfc.field_communities_value");
        // Join form_point.
        $query->leftJoin('fc', 'administrative_division', 'ad', "ad.id = fc.field_region_value && fc.field_deleted = '0'");
        // Where clause
        $query->andWhere($query->expr()->in('HEX(fc.field_ulid_reference)', $communityRefs));

        return $query;
    }

    /**
     * Get the last version of a Point
     *
     * @param FormRecord $point
     *
     * @return FormRecord
     */
    public function getLastVersion(FormRecord $point): FormRecord
    {
        /** @var QueryBuilder $query */
        $query = $this->buildBaseQueryVersions($point);
        $query->addOrderBy('fp.field_changed_value', 'DESC');

        $resp = $query->execute()->fetchAssociative();

        $pointId = Ulid::fromBinary($resp['point'])->toBase32();
        $point = $this->find($pointId);

        return $point;
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array         $criteria Filtering criteria.
     * @param string[]|null $orderBy  Sorting.
     * @param ?integer      $limit    Result limits.
     * @param ?integer      $offset   Result start offset.
     * @param ?array        $groupBy  Grouping.
     *
     * @return array[] The objects.
     *
     * @throws Driver_Exception
     * @throws Exception
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null, ?array $groupBy = null): array
    {
        /** @var User $user */
        $user = $this->getCurrentUser();
        $this->accessHandler->checkFormAccess($user, PointAccessHandler::ACCESS_ACTION_READ, ['country' => $user->getCountry()]);

        // Add security management criteria and filter output.
        return parent::findBy($this->updateReadCriteria($criteria), $orderBy, $limit, $offset, $groupBy);
    }

    /**
     * Update filter criteria with security criteria.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function updateReadCriteria(array $criteria): array
    {
        /** @var User $user */
        $user = $this->getCurrentUser();
        // A user with 'all permissions' permission can read all.
        // A user with 'all countries' permission can read all.
        if ($this->accessHandler->hasPermission($user, 'all permissions', [$user->getCountry()]) || $this->accessHandler->hasPermission($user, 'all countries', [$user->getCountry()])) {
            return $criteria;
        }
        // A user only can read her country records.
        $criteria['field_country'] = $user->getCountry()->getCode();

        return $criteria;
    }

    /**
     * Recalculate Point
     *
     * @param FormRecord $point
     */
    public function recalculate(FormRecord $point): void
    {
        if ('calculated' !== $point->{'field_status'}) {
            return;
        }

        $point->{'field_status'} = 'calculating';
        $this->update($point, true);
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    protected function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@form_id] This form require user session.',
                    ['@form_id' => $this->getId()]
                )
            );
        }

        return $user;
    }

    /**
     * Get a clean copy of record field values.
     *
     * @param FormRecord $record
     *
     * @return array
     */
    protected function getRecordInternalValues(FormRecord $record): array
    {
        // Get current values.
        $internals = parent::getRecordInternalValues($record);
        unset($internals['field_status']);
        unset($internals['field_chk_to_plan']);

        // Call each field to clone content.
        foreach ($internals as $fieldName => $value) {
            $field = $record->getForm()->getFieldDefinition($fieldName);
            $internals[$fieldName] = $field->cloneContent();
        }

        return $internals;
    }
}
