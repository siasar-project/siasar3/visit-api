<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Forms;

use App\Entity\Configuration\ConfigurationReadInterface;
use App\Entity\Country;
use App\Forms\Event\InquiryFormManagerEvent;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Security\Handlers\InquiryAccessHandler;
use App\Service\SessionService;
use App\Tools\Tools;
use App\Traits\GetContainerTrait;
use App\Traits\InquiryFormLoggerTrait;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Forms entity manager.
 *
 * This form type will be used how subform by other form.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class SubformFormManager extends ChangedFormManager
{
    use GetContainerTrait;
    use InquiryFormLoggerTrait;

    protected ?SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected ?EntityManager $entityManager;
    protected ?FormFactory $formFactory;

    /**
     * FormManager constructor.
     *
     * @param ConfigurationReadInterface $config           Form configuration.
     * @param FieldTypeManager           $fieldTypeManager Field type manager.
     * @param Connection                 $connection       Database connection.
     * @param EventDispatcherInterface   $dispatcher       Event dispatcher.
     */
    public function __construct(ConfigurationReadInterface $config, FieldTypeManager $fieldTypeManager, Connection $connection, EventDispatcherInterface $dispatcher)
    {
        parent::__construct($config, $fieldTypeManager, $connection, $dispatcher);
        $this->sessionService = static::getContainerInstance()->get('session_service');
        $accessManager = static::getContainerInstance()->get('access_manager');
        $this->accessHandler = new InquiryAccessHandler($accessManager);
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->formFactory = static::getContainerInstance()->get('form_factory');
    }

    /**
     * Add base fields while creating the form.
     *
     * @return void
     */
    public function addBaseFields(): void
    {
        parent::addBaseFields();

        $this->addField(
            'country_reference',
            'country',
            'Country',
            [
                'required' => true,
                'sort' => true,
                'filter' => true,
            ]
        )
            ->setDescription('Owner country.')
            ->setIndexable(true)
            ->setInternal(true);
    }

    /**
     * Get parent form ID.
     *
     * @return string
     */
    public function getParentForm(): string
    {
        if (isset($this->confValues['meta']['parent_form'])) {
            return $this->confValues['meta']['parent_form'];
        }

        return '';
    }

    /**
     * Insert a new record.
     *
     * @param array $data Data to insert.
     *
     * @return string The last inserted ID.
     *
     * @throws \Exception
     */
    public function insert(array $data): string
    {
        // Prepare log.
        $sourceData = $data;
        // We need the user to verify permissions.
        $user = $this->getCurrentUser();

        // Force country.
        if (!isset($data['field_country']) || empty($data['field_country'])) {
            if (!$user->getCountry()) {
                throw new \Exception(
                    $this->t(
                        '[@form_id] This form require user with country set.',
                        ['@form_id' => $this->getId()]
                    )
                );
            }
            // Set the user country how the record country.
            $data['field_country'] = $user->getCountry();
        }

        // Apply security management.
        $context = [
            'country' => '',
            'division' => null,
        ];
        if ($data['field_country'] instanceof Country) {
            $context['country'] = $data['field_country']->getId();
        } elseif (is_string($data['field_country'])) {
            $context['country'] = $data['field_country'];
            $data['field_country'] = $this->entityManager->getRepository(Country::class)->find($data['field_country']);
        } elseif (is_array($data['field_country']) && isset($data['field_country']['value'])) {
            $context['country'] = $data['field_country']['value'];
        } else {
            throw new \Exception(
                $this->t(
                    '[@class] I don\'t know how get the country code to check permissions.',
                    [
                        '@class' => self::class,
                    ]
                )
            );
        }
        $context['country'] = $this->entityManager->getRepository(Country::class)->find($context['country']);
        $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_CREATE, $context);

        $this->setIgnoreRequiredFields(true);

        // Action.
        $id = parent::insert($data);
        // Logger.
//        $this->info('Insert sub-form record', ['form_id' => $this->getId(), 'record_id' => $id, 'received' => $sourceData]);

        return $id;
    }

    /**
     * Update a record.
     *
     * @param FormRecord $record Data to update. If have a 'id' key it will be used instead the $id parameter.
     * @param bool       $force  Force the save without validations.
     *
     * @return void
     */
    public function update(FormRecord &$record, bool $force = false): void
    {
        if (!$force) {
            if (!$record->isModified()) {
                return;
            }
            $user = $this->getCurrentUser();
            $modifiedFields = $record->getModifiedFields();

            // Apply security management.
            $context = [
                'country' => $record->{'field_country'},
                'division' => null,
            ];
            $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_UPDATE, $context);

            // If I've parent do a update control.
            $parentRecord = $this->getParentRecord($record);
            if (!$parentRecord) {
                // It doesn't have parent record yet, it must ignore required fields
                $this->setIgnoreRequiredFields(true);
            } else {
                switch ($parentRecord->{'field_status'}) {
                    case 'draft':
                        $this->setIgnoreRequiredFields(true);
                        break;
                    case 'finished':
                        // Move record to draft state.
                        $changeToDraft = false;
                        foreach ($modifiedFields as $fieldName) {
                            $field = $this->getFieldDefinition($fieldName);
                            if (!$field->isInternal()) {
                                $changeToDraft = true;
                            }
                        }
                        if ($changeToDraft) {
                            $parentRecord->{'field_status'} = 'draft';
                            $parentRecord->save();
                        }
                        break;
                    case 'validated':
                    case 'locked':
                    case 'removed':
                        $msg = $this->t("Records in validated, locked, or removed states can't be updated.");
                        $context = $parentRecord->toArray();
                        $context['form_id'] = $this->getId();
                        $context['record_id'] = $record->{'id'};
                        $this->error($msg, $context);
                        throw new \Exception($msg);
                }
            }

            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.subform.record.preupdate');
        }

        // Update.
        parent::update($record, $force);

        if (!$force) {
            // Dispatch event
            $event = new InquiryFormManagerEvent(
                $this,
                InquiryFormManagerEvent::ACTION_UPDATE,
                [],
                $record
            );
            $this->dispatcher->dispatch($event, 'form.subform.record.postupdate');
            // Logger.
//            $this->info('Update sub-form record', ['form_id' => $this->getId(), 'record_id' => $record->{'id'}, 'updates' => $record->toArray()]);
        }
    }

    /**
     * Drop a record.
     *
     * @param string $id Record ID.
     *
     * @return void
     *
     * @throws Exception
     */
    public function drop(string $id): void
    {
        // Validate.
        $record = $this->find($id);
        if (!$record) {
            return;
        }
        $parentRecord = $this->getParentRecord($record);
        if ($parentRecord && $parentRecord->getForm()->getType() === InquiryFormManager::class) {
            InquiryFormManager::stopLockedOrRemoved($parentRecord, 'deleted');
            $user = $this->getCurrentUser();

            // Apply security management.
            $context = [
                'country' => $parentRecord->{'field_country'},
                'division' => $parentRecord->{'field_region'},
            ];

            $this->accessHandler->checkFormAccess($user, InquiryAccessHandler::ACCESS_ACTION_UPDATE, $context);

            // Status control and automatic changes.
            switch ($parentRecord->{'field_status'}) {
                case 'draft':
                case 'finished':
                    // Do nothing.
                    break;
                case 'validated':
                case 'locked':
                case 'removed':
                    $msg = $this->t("Records in validated, locked, or removed states can't be updated.");
                    $context = $record->toArray();
                    $context['form_id'] = $this->getId();
                    $context['record_id'] = $record->{'id'};
                    $this->error($msg, $context);
                    throw new \Exception($msg);
            }
        }

        // Apply.
        parent::drop($id);
        // Logger.
//        $this->info('Deleted sub-form record', ['form_id' => $record->getForm()->getId(), 'record_id' => $id]);
    }

    /**
     * Get the parent record from a subform record.
     *
     * This method return an InquiryFormManager record or null.
     *
     * @param FormRecord $record
     *
     * @return FormRecord|null
     *
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \ReflectionException
     */
    public function getParentRecord(FormRecord $record): FormRecord | null
    {
        if (!method_exists($record->getForm(), 'getParentForm')) {
            return null;
        }
        // If I've parent do a update control.
        $parentForm = $this->formFactory->find($record->getForm()->getParentForm());
        /** @var FormRecord $parentRecord */
        $parentRecord = null;
        if ($parentForm) {
            // Find parent fields that reference this subform.
            $fields = $parentForm->getFields();
            /** @var FieldTypeInterface $field */
            foreach ($fields as $field) {
                if ('subform_reference' === $field->getFieldType() && $record->getForm()->getId() === FormFactory::formatId($field->getSubFormId())) {
                    $ulid = new Ulid($record->getId());
                    $parentRecord = current($parentForm->findBy([$field->getId() => $ulid->toBinary()]));
                    if ($parentRecord) {
                        break;
                    }
                }
            }
        }

        if ($parentRecord && InquiryFormManager::class !== $parentRecord->getForm()->getType()) {
            $parentRecord = $this->getParentRecord($parentRecord);
        }

        return $parentRecord ? $parentRecord : null;
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    protected function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@form_id] This form require user session.',
                    ['@form_id' => $this->getId()]
                )
            );
        }

        return $user;
    }
}
