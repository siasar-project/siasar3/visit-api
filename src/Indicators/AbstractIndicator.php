<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Entity\AdministrativeDivision;
use App\Entity\Indicator;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Indicators\wsp\HHaccijIndicator;
use App\Indicators\wsp\WaterSystemNote;
use App\Indicators\wsp\WSLCONsisIndicator;
use App\Indicators\wsp\WSLQUAsisIndicator;
use App\Indicators\wsp\WSLSEAsisIndicator;
use App\Repository\IndicatorRepository;
use App\Tools\ContextLogNormalizer;
use App\Tools\Json;
use App\Traits\GetContainerTrait;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;
use Symfony\Component\Uid\Ulid;

/**
 * Base indicator.
 */
abstract class AbstractIndicator implements IndicatorInterface
{
    use GetContainerTrait;

    public const VALUE_NOT_APPLICABLE = -1.0;
    public const VALUE_ERROR = -2.0;
    public const VALUE_NOT_CALCULATED = -3.0;
    public const VALUE_USE_SYS_A12_3I = -4.0;

    /**
     * If true this indicator must recalculate always.
     */
    protected $forceCalculate = false;

    protected string $indicatorCode;
    protected IndicatorContextInterface $context;
    protected FrozenParameterBag $weight;
    protected IndicatorRepository $indicatorRepo;
    protected ManagerRegistry $entityManager;
    protected LoggerInterface $inquiryFormLogger;
    protected bool $main;
    protected FormFactory $formFactory;

    /**
     * Constructor.
     *
     * @param string                    $indicatorCode
     * @param IndicatorContextInterface $context
     * @param bool                      $main
     *
     * @throws Exception
     */
    public function __construct(string $indicatorCode, IndicatorContextInterface $context, bool $main = false)
    {
        $this->indicatorCode = $indicatorCode;
        $this->main = $main;
        $this->context = $context;
        $this->entityManager = static::getContainerInstance()->get('doctrine');
        $this->indicatorRepo = $this->entityManager->getRepository(Indicator::class);
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->formFactory = static::getContainerInstance()->get('form_factory');

        if ($this->getContextFormId() !== $this->context->getRecord()->getForm()->getId()) {
            throw new Exception(
                sprintf(
                    '{%s} Bad indicator context: wrong form id "%s", require "%s".',
                    $this->indicatorCode,
                    $this->context->getRecord()->getForm()->getId(),
                    $this->getContextFormId()
                )
            );
        }

        $this->setWeights();
    }

    /**
     * Get required form ID in the context.
     *
     * @return string
     */
    abstract public function getContextFormId(): string;

    /**
     * Return $true if $conditional is true.
     *
     * @param bool  $conditional
     * @param mixed $true
     * @param mixed $false
     *
     * @return mixed
     */
    public function ternary(bool $conditional, mixed $true, mixed $false)
    {
        return $conditional ? $true : $false;
    }

    /**
     * Get indicator value.
     *
     * @param bool $readonly
     *
     * @return float
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getValue(bool $readonly = false): float
    {
        if (!$this->forceCalculate) {
            $indicator = $this->indicatorRepo->findOneBy([
                'record' => $this->getRecord()->getId(),
                'name' => $this->indicatorCode,
            ]);
            if ($indicator) {
                $value = $indicator->getValue();
                if ($value >= 0 || $readonly) {
                    return $value;
                }
                // If we don't use the cache, we must reset the indicator.
                $this->indicatorRepo->removeNow($indicator);
            }

            if ($readonly) {
                return static::VALUE_NOT_CALCULATED;
            }
        }

        $currentException = null;
        try {
            $value = $this->resolve();
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case -1:
                    $value = 0;
                    break;
                default:
                    $value = static::VALUE_ERROR;
                    // Add inquiry form log with error message, and return inquiry to draft.
                    $context['user'] = 'INDICATORS';
                    $context['user_id'] = '';

                    $context['form_id'] = $this->getRecord()->getForm()->getId();
                    $context['record_id'] = $this->getRecord()->getId();

                    // todo Binary IDs are missed in this conversion. If we do not do this conversion the log fails by JSON encoding.
                    $nContext = ContextLogNormalizer::normalize($context);
                    $nContext = Json::decode(Json::encode($nContext));
                    $this->inquiryFormLogger->error($e->getMessage(), $nContext);

                    $point = $this->getPoint();
                    if ($point) {
                        $point->{'field_status'} = 'digitizing';
                        $point->getForm()->update($point, true);
                    }

                    if ($this->isMainIndicator()) {
                        $record = $this->getRecord();
                        $record->{'field_status'} = 'draft';
                        $record->getForm()->update($record, true);
                    }
                    break;
            }
            $currentException = $e;
            // Allow save error state.
        }
        $ulid = Ulid::fromString($this->getRecord()->getId());
        $indicator = new Indicator($ulid, $this->indicatorCode);
        if (is_nan($value)) {
            $value = 0.0;
        }
        $indicator->setValue($value);
        $indicator->setRecordType($this->getRecord()->getForm()->getId());
        if ($currentException) {
            $indicator->setMessage(sprintf('[%s] %s', $currentException->getCode(), $currentException->getMessage()));
        }

        if (!$this->forceCalculate) {
            $this->indicatorRepo->saveNow($indicator);
        }

        if ($currentException && !$this->isMainIndicator()) {
            switch ($currentException->getCode()) {
                case -1:
                    // Continue calculate.
                    break;
                default:
                    // When error persisted how value, throw it again.
                    throw new Exception(sprintf('{%s} %s', $this->indicatorCode, $currentException->getMessage()), $currentException->getCode(), $currentException);
            }
        }

        return $value;
    }

     /**
     * Get indicator value.
     *
     * @return Indicator[]
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getAllValues(): array
    {
        $indicators = $this->indicatorRepo->findBy([
            'record' => $this->getRecord()->getId(),
        ]);

        return $indicators;
    }

    /**
     * Get indicator label.
     *
     * @param bool $readonly
     *
     * @return string
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getLabel(bool $readonly = false): string
    {
        if (!$this->isMainIndicator()) {
            return 'ERROR, NOT INDICATOR';
        }

        $value = $this->getValue($readonly);
        if (static::VALUE_ERROR === $value) {
            return 'ERROR';
        }
        if (static::VALUE_NOT_CALCULATED === $value) {
            return 'NOT CALCULATED';
        }
        if (static::VALUE_USE_SYS_A12_3I === $value) {
            return 'USE SYS A12.3i';
        }
        if ($value < 0) {
            return '';
        }
        if ($value >= 0.0 && $value <= 0.39) {
            return 'D';
        }
        if ($value >= 0.4 && $value <= 0.69) {
            return 'C';
        }
        if ($value >= 0.7 && $value <= 0.89) {
            return 'B';
        }
        if ($value >= 0.9 && $value <= 1.0) {
            return 'A';
        }

        return 'OUT OF RANGE';
    }

    /**
     * Allow use this instance how a function.
     *
     * @return float
     */
    public function __invoke(): float
    {
        return $this->getValue();
    }

    /**
     * Remove the persistent value to allow for a recalculation of this indicator again.
     *
     * @return void
     */
    public function reset(): void
    {
        // Find all indicators to this record.
        $indicators = $this->indicatorRepo->findBy([
            'record' => $this->getRecord()->getId(),
        ]);
        // Mark each indicator to delete.
        foreach ($indicators as $indicator) {
            $this->indicatorRepo->remove($indicator);
        }
        // Apply database changes.
        $this->entityManager->getManager()->flush();

        // Find and remove sub-indicators.
        $dump = $this->getRecord()->dump();
        $this->removeSubformIndicators($dump["subforms"]);
    }

    /**
     * Get a constant.
     *
     * @param string $name
     *
     * @return array|bool|float|int|mixed|string|null
     */
    public function phi(string $name): mixed
    {
        return $this->weight->get($name);
    }

    /**
     * Get context form record.
     *
     * @return FormRecord
     */
    public function getRecord(): FormRecord
    {
        return $this->context->getRecord();
    }

    /**
     * Get context point.
     *
     * @return ?FormRecord
     */
    public function getPoint(): ?FormRecord
    {
        return $this->context->getPoint();
    }

    /**
     * Get a constant.
     *
     * @param string $name
     *
     * @return array|bool|float|int|mixed|string|null
     */
    public function getWeight(string $name): mixed
    {
        return $this->phi($name);
    }

    /**
     * Get indicator code.
     *
     * @return string
     */
    public function getIndicatorCode(): string
    {
        return $this->indicatorCode;
    }

    /**
     * Is this a main indicator?
     *
     * False if is a sub-indicator.
     *
     * @return bool
     */
    public function isMainIndicator(): bool
    {
        return $this->main;
    }

    /**
     * Get sub-indicator value.
     *
     * @param string                         $class
     * @param IndicatorContextInterface|null $context
     * @param array|null                     $additionalData
     *
     * @return mixed
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getSubindicatorValue(string $class, IndicatorContextInterface $context = null, $additionalData = null): mixed
    {
        if (!$context) {
            $context = $this->context;
        }
        /** @var AbstractIndicator $subIndicator */
        $subIndicator = new $class($context, $additionalData);
        $value = $subIndicator->getValue();

        if ($value < 0) {
//            throw new Exception(sprintf('Sub-indicator stop in "%s".', $this->getIndicatorCode()), $value);
            return 0.0;
        }

        return $value;
    }

    /**
     * Resolve indicator.
     *
     * @return mixed
     */
    abstract protected function resolve();

    /**
     * Remove subforms indicators.
     *
     * @param array $subforms Dumped subforms to remove indicators.
     *
     * @throws ORMException
     */
    protected function removeSubformIndicators(array $subforms)
    {
        foreach ($subforms as $id => $subform) {
            $indicators = $this->indicatorRepo->findBy([
                'record' => $id,
            ]);
            // Mark each indicator to delete.
            foreach ($indicators as $indicator) {
                $this->indicatorRepo->remove($indicator);
            }
            // Apply database changes.
            $this->entityManager->getManager()->flush();
            $this->removeSubformIndicators($subform["subforms"]);
        }
    }

    /**
     * Init weights.
     *
     * @return void
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag();
    }

    /**
     * Get last wssystem distribution.
     *
     * @return FormRecord|null
     */
    protected function getLastDistribution(): ?FormRecord
    {
        $distributions = $this->getRecord()->{'field_distribution_infrastructure'};
        $younger = null;
        foreach ($distributions as $distribution) {
            if (!$younger) {
                $younger = $distribution;
            } else {
                $youngerDate = $younger->{'field_changed'};
                $distributionDate = $distribution->{'field_changed'};
                // @see https://stackoverflow.com/a/49084782/6385708
                if (($distributionDate <=> $youngerDate) === 1) {
                    $younger = $distribution;
                }
            }
        }

        return $younger;
    }

    /**
     * Get wssystem distribution by administrative division.
     *
     * @param AdministrativeDivision|null $adm
     *
     * @return FormRecord|null
     */
    protected function getDistribution(AdministrativeDivision $adm): ?FormRecord
    {
        $distributions = $this->getRecord()->{'field_distribution_infrastructure'};
        $dist = null;
        foreach ($distributions as $distribution) {
            $region = $distribution->{'field_communities_serviced_network'};
            if ($region === $adm) {
                return $distribution;
            }
        }
        if (null === $dist) {
            $dist = $this->getLastDistribution();
        }

        return $dist;
    }

    /**
     * Get water service systems that serve to this community.
     *
     * @return WaterSystemNote[]
     *
     * @throws Exception
     */
    protected function getCommunityIn1d12(): array
    {
        $response = [];
        // Get all point water systems.
        $point = $this->getPoint();
        $community = $this->getRecord();
        /** @var AdministrativeDivision $region */
        $region = $community->{'field_region'};
        $waterSystems = $point->{'field_wsystems'};
        foreach ($waterSystems as $waterSystem) {
            $servedCommunities = $waterSystem->{'field_served_communities'};
            foreach ($servedCommunities as $servedCommunity) {
                /** @var AdministrativeDivision $division */
                $division = $servedCommunity->{'field_community'};
                if ($region->getId() === $division->getId()) {
                    $a1d12 = $servedCommunity->{'field_households'};
                    $response[] = new WaterSystemNote(
                        // form.wssystem
                        $waterSystem,
                        // form.wssystem.communities
                        $servedCommunity,
                        $this->getSubindicatorValue(HHaccijIndicator::class, new SimpleIndicatorContext($waterSystem), ['a1d12' => $a1d12, 'comRegion' => $region]),
                        $this->getSubindicatorValue(WSLCONsisIndicator::class, new SimpleIndicatorContext($waterSystem)),
                        $this->getSubindicatorValue(WSLSEAsisIndicator::class, new SimpleIndicatorContext($waterSystem)),
                        $this->getSubindicatorValue(WSLQUAsisIndicator::class, new SimpleIndicatorContext($waterSystem))
                    );
                }
            }
        }

        return $response;
    }
}
