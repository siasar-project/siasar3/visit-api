<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Indicators\hcc\HCCHSHIndicator;
use App\Indicators\hcc\HCCHWAIndicator;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * HCC indicator.
 */
class HCCIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('HCC', $context, true);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.health.care';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // H C C = {φ} rsub {HWA} ∙ H C CHWA + {φ} rsub {HS H} ∙ H C CHSH
        $hccHwa = new HCCHWAIndicator($this->context);
        $hccHsh = new HCCHSHIndicator($this->context);

        return $this->getWeight('hwa') * $hccHwa() +
            $this->getWeight('hsh') * $hccHsh();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'hwa' => 1/2,
                'hsh' => 1/2,
            ]
        );
    }
}
