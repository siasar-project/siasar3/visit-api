<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use Exception;

/**
 * Base indicator context.
 */
class IndicatorContext implements IndicatorContextInterface
{
    protected ?FormRecord $point;
    protected FormRecord $form;

    /**
     * Base constructor.
     *
     * @param FormRecord  $record
     * @param ?FormRecord $point
     */
    public function __construct(FormRecord $record, FormRecord $point = null)
    {
        // Validate context.
        if ('form.point' === $record->getForm()->getId()) {
            throw new Exception('Bad indicator context: form can\'t be a form.point record.');
        }
        if (SubformFormManager::class !== $record->getForm()->getType() && InquiryFormManager::class !== $record->getForm()->getType()) {
            throw new Exception(
                sprintf(
                    'Bad indicator context: form must be under %s form manager.',
                    InquiryFormManager::class
                )
            );
        }
        if ($point) {
            if ('form.point' !== $point->getForm()->getId()) {
                throw new Exception('Bad indicator context: form must be a form.point record.');
            }
        }
        // save context.
        $this->form = $record;
        $this->point = $point;
    }

    /**
     * @return FormRecord|null
     */
    public function getPoint(): ?FormRecord
    {
        return $this->point;
    }

    /**
     * @return FormRecord
     */
    public function getRecord(): FormRecord
    {
        return $this->form;
    }
}
