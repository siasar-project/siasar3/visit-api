<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Entity\AdministrativeDivision;
use App\Entity\Indicator;
use App\Forms\FormRecord;
use App\Indicators\wsp\HHaccijIndicator;
use App\Indicators\wsp\WaterSystemNote;
use App\Indicators\wsp\WSLCONsisIndicator;
use App\Indicators\wsp\WSLQUAsisIndicator;
use App\Indicators\wsp\WSLSEAsisIndicator;
use App\Repository\IndicatorRepository;
use App\Tools\ContextLogNormalizer;
use App\Tools\Json;
use App\Traits\GetContainerTrait;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;
use Symfony\Component\Uid\Ulid;

/**
 * Indicator interface.
 */
interface IndicatorInterface
{
    /**
     * Get required form ID in the context.
     *
     * @return string
     */
    public function getContextFormId(): string;

    /**
     * Return $true if $conditional is true.
     *
     * @param bool  $conditional
     * @param mixed $true
     * @param mixed $false
     *
     * @return mixed
     */
    public function ternary(bool $conditional, mixed $true, mixed $false);

    /**
     * Get indicator value.
     *
     * @param bool $readonly
     *
     * @return float
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getValue(bool $readonly = false): float;

    /**
     * Get indicator value.
     *
     * @return Indicator[]
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getAllValues(): array;

    /**
     * Get indicator label.
     *
     * @param bool $readonly
     *
     * @return string
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getLabel(bool $readonly = false): string;

    /**
     * Allow use this instance how a function.
     *
     * @return float
     */
    public function __invoke(): float;

    /**
     * Remove the persistent value to allow for a recalculation of this indicator again.
     *
     * @return void
     */
    public function reset(): void;

    /**
     * Get a constant.
     *
     * @param string $name
     *
     * @return array|bool|float|int|mixed|string|null
     */
    public function phi(string $name): mixed;

    /**
     * Get context form record.
     *
     * @return FormRecord
     */
    public function getRecord(): FormRecord;

    /**
     * Get context point.
     *
     * @return ?FormRecord
     */
    public function getPoint(): ?FormRecord;

    /**
     * Get a constant.
     *
     * @param string $name
     *
     * @return array|bool|float|int|mixed|string|null
     */
    public function getWeight(string $name): mixed;

    /**
     * Get indicator code.
     *
     * @return string
     */
    public function getIndicatorCode(): string;

    /**
     * Is this a main indicator?
     *
     * False if is a sub-indicator.
     *
     * @return bool
     */
    public function isMainIndicator(): bool;

    /**
     * Get sub-indicator value.
     *
     * @param string                         $class
     * @param IndicatorContextInterface|null $context
     *
     * @return mixed
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getSubindicatorValue(string $class, IndicatorContextInterface $context = null): mixed;
}
