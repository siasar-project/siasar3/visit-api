<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Indicators\pat\PATCAPIndicator;
use App\Indicators\pat\PATCOBIndicator;
use App\Indicators\pat\PATINTIndicator;
use App\Indicators\pat\PATSINIndicator;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * PAT indicator.
 */
class PATIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('PAT', $context, true);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // TAP = {φ} rsub {ICT} ∙PATSIN+ {φ} rsub {INS} ∙PATCAP+ {φ} rsub {CO V} ∙PATCOB+ {φ} rsub {IN S} ∙PATINT
        $patSin = new PATSINIndicator($this->context);
        $patCap = new PATCAPIndicator($this->context);
        $patCob = new PATCOBIndicator($this->context);
        $patInt = new PATINTIndicator($this->context);

        return $this->getWeight('ict') * $patSin() +
            $this->getWeight('ins') * $patCap() +
            $this->getWeight('cov') * $patCob() +
            $this->getWeight('ts') * $patInt();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'ict' => 1/4,
                'ins' => 1/4,
                'cov' => 1/4,
                'ts' => 1/4,
            ]
        );
    }
}
