<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Indicators\sep\SEPECOIndicator;
use App\Indicators\sep\SEPENVIndicator;
use App\Indicators\sep\SEPOPMIndicator;
use App\Indicators\sep\SEPORGIndicator;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * SEP indicator.
 *
 * Alias: PSE
 */
class SEPIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('SEP', $context, true);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $sEPORG = new SEPORGIndicator($this->context);
        $sEPOPM = new SEPOPMIndicator($this->context);
        $sEPECO = new SEPECOIndicator($this->context);
        $sEPENV = new SEPENVIndicator($this->context);

        // SEP = {φ} rsub {ORG} ∙ SEPORG + {φ} rsub {O P M} ∙ SEPOPM + {φ} rsub {ECO} ∙ SEPECO + {φ} rsub {ENV} ∙ SEPENV
        return $this->getWeight('org') * $sEPORG() +
            $this->getWeight('opm') * $sEPOPM() +
            $this->getWeight('eco') * $sEPECO() +
            $this->getWeight('env') * $sEPENV();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'org' => 1/4,
                'opm' => 1/4,
                'eco' => 1/4,
                'env' => 1/4,
            ]
        );
    }
}
