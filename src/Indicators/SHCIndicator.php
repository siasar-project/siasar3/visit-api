<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Indicators\shc\SHCSSHIndicator;
use App\Indicators\shc\SHCSWAIndicator;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * SHC indicator.
 */
class SHCIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('SHC', $context, true);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.school';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // SHC = {φ} rsub {SWA} ∙SHCSWA+ {φ} rsub {S S H} ∙SHCSSH
        $shcSwa = new SHCSWAIndicator($this->context);
        $shcSsh = new SHCSSHIndicator($this->context);

        return $this->getWeight('swa') * $shcSwa() +
            $this->getWeight('ssh') * $shcSsh();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'swa' => 1/2,
                'ssh' => 1/2,
            ]
        );
    }
}
