<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Indicators\wsi\WSIAUTIndicator;
use App\Indicators\wsi\WSIINFIndicator;
use App\Indicators\wsi\WSIPROIndicator;
use App\Indicators\wsi\WSITREIndicator;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * WSI indicator.
 *
 * Alias: EIA
 */
class WSIIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSI', $context, true);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $a7 = $this->getRecord()->{'field_type_system'};
        $wsiaut = new WSIAUTIndicator($this->context);
        $wsiinf = new WSIINFIndicator($this->context);
        $wsipro = new WSIPROIndicator($this->context);
        $wsitre = new WSITREIndicator($this->context);

        $wsiAut = $wsiaut();
        if (in_array($a7, ['3', '5'])) {
            return (4/3) *
                (
                    $this->getWeight('inf') * $wsiinf() +
                    $this->getWeight('pro') * $wsipro() +
                    $this->getWeight('tre') * $wsitre()
                );
        }

        return $this->getWeight('aut') * $wsiAut +
            $this->getWeight('inf') * $wsiinf() +
            $this->getWeight('pro') * $wsipro() +
            $this->getWeight('tre') * $wsitre();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'aut' => 1/4,
                'inf' => 1/4,
                'pro' => 1/4,
                'tre' => 1/4,
            ]
        );
    }
}
