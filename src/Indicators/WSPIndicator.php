<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators;

use App\Entity\AdministrativeDivision;
use App\Indicators\wsp\SHLIndicator;
use App\Indicators\wsp\SHLSSLIndicator;
use App\Indicators\wsp\WaterSystemNote;
use App\Indicators\wsp\WSLIndicator;
use App\Indicators\wsi\WSIComIndicator;
use App\Indicators\sep\SEPComIndicator;
use Exception;

/**
 * WSP indicator.
 *
 * Alias: IAS
 */
class WSPIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSP', $context, true);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $a5 = $this->getRecord()->{'field_total_households'};
        if (0.0 === $a5) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }

        $b1 = $this->getRecord()->{'field_households_without_water_supply_system'};
        $cWater = 1 - ($b1 / $a5);

        $servedComms = $this->getServedRecordsByCommunity();
        $sumWSI = 0;
        $sumSEP = 0;
        $sumA12d3 = 0;

        foreach ($servedComms as $servedComm) {
            $wsi = new WSIIndicator(new PointIndicatorContext($servedComm->system, $this->getPoint()));
            $sep = new SEPIndicator(new PointIndicatorContext($servedComm->servedCommunity->{'field_provider'}, $this->getPoint()));
            $a12d3 = $servedComm->servedCommunity->{'field_households'};

            $sumWSI += $wsi() * $a12d3;
            $sumSEP += $sep() * $a12d3;
            $sumA12d3 += $a12d3;
        }

        $wsiCom = new WSIComIndicator($this->context, $cWater, $sumWSI, $sumA12d3);
        $sepCom = new SEPComIndicator($this->context, $cWater, $sumSEP, $sumA12d3);

        $wsl = new WSLIndicator($this->context);
        $shl = new SHLIndicator($this->context);

        $wslV = $wsl();
        $shlV = $shl();
        $base = $wslV * $shlV * $wsiCom() * $sepCom();
        if ($base < 0) {
            throw new Exception(sprintf('Negative square root in "%s".', $this->getIndicatorCode()));
        }

        return $base**(1/4);
    }

    /**
     * Get Service providers by this community.
     *
     * @return WaterSystemNote[]
     */
    protected function getServedRecordsByCommunity(): array
    {
        $resp = [];
        /** @var AdministrativeDivision $refDiv */
        $refDiv = $this->getRecord()->{'field_region'};
        $systems = $this->getPoint()->{'field_wsystems'};
        foreach ($systems as $system) {
            $comsServed = $system->{'field_served_communities'};
            foreach ($comsServed as $comServed) {
                /** @var AdministrativeDivision $com */
                $com = $comServed->{'field_community'};
                if ($refDiv->getId() === $com->getId()) {
                    $resp[] = new WaterSystemNote($system, $comServed, 0, 0, 0, 0);
                }
            }
        }

        return $resp;
    }
}
