<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\hcc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FsanPMas indicator.
 */
class FsanPMasIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FsanPMas', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.health.care';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var Bool $c2d1 */
        $c3d2d2 = $this->getRecord()->{'field_usable_toilet_men'};
        $c3d3d2 = $this->getRecord()->{'field_usable_toilet_neutral'};
        $a6d2 = $this->getRecord()->{'field_male_patients_per_day'};

        if (0 === $a6d2) {
            return 0.0;
        }

        // {F} rsub {sanPFem} = {H C {C} rsub {C 3.1 .2} + {H C {C} rsub {C 3.3 .2}} over {2}} over {H C {C} rsub {A 6.1}}
        return (($c3d3d2 / 2) + $c3d2d2) / $a6d2;
    }
}
