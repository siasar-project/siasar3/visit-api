<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\hcc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * HCCHWA indicator.
 *
 * Tabla 38: matriz de cálculo del indicador de agua en centros de salud (HCCHWA)
 * | # | HCC_B1                             | HCC_B3 | HCCHWA |
 * | 1 | 1. Sí, compartido con la comunidad | A      | 1,00   |
 * | 2 | 2. Sí, exclusivo para el centro    | A      | 1,00   |
 * | 3 | 1. Sí, compartido con la comunidad | B      | 0,70   |
 * | 4 | 2. Sí, exclusivo para el centro    | B      | 0,70   |
 * | 5 | 1. Sí, compartido con la comunidad | C      | 0,40   |
 * | 6 | 2. Sí, exclusivo para el centro    | C      | 0,40   |
 * | 7 | 1. Sí, compartido con la comunidad | D      | 0,00   |
 * | 8 | 2. Sí, exclusivo para el centro    | D      | 0,00   |
 * | 9 | 3. No                              | -      | 0,00   |
 */
class HCCHWAIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('HCCHWA', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.health.care';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // H C CHWA = f left (H C {C} rsub {B 1} , H C {C} rsub {B 3} right )
        $b1 = $this->getRecord()->{'field_have_water_supply'};
        $b3 = $this->getRecord()->{'field_functioning_water_system'};

        if (is_array($b1) && in_array('3', $b1)) {
            return 0.0;
        }

        switch ($b3) {
            case 'A':
                if (is_array($b1) && (in_array('1', $b1) || in_array('2', $b1))) {
                    return 1.0;
                }
                break;
            case 'B':
                if (is_array($b1) && (in_array('1', $b1) || in_array('2', $b1))) {
                    return 0.7;
                }
                break;
            case 'C':
                if (is_array($b1) && (in_array('1', $b1) || in_array('2', $b1))) {
                    return 0.4;
                }
                break;
            case 'D':
            default:
                return 0.0;
        }

        return 0.0;
    }
}
