<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\hcc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * HccHyg indicator.
 *
 * Tabla 42: matriz de cálculo de la higiene en el centro de salud (HccHyg)
 * | # | HCC_D1 | HCC_D2              | HccHyg |
 * | 1 | 1. Sí  | 1. Sí, agua y jabón | 1,00   |
 * | 2 | 1. Sí  | 2. Solo agua        | 0,40   |
 * | 3 | 1. Sí  | 3. Solo jabón       | 0,40   |
 * | 4 | 1. Sí  | 4. Ni agua ni jabón | 0,00   |
 * | 5 | 2. No  | -                   | 0,00   |
 */
class HccHygIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('HccHyg', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.health.care';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $d1 = $this->getRecord()->{'field_functional_hand_hygiene'};
        $d2 = $this->getRecord()->{'field_are_soap_and_water'};

        if ($d1) {
            switch ($d2) {
                case '1':
                    return 1.0;
                case '2':
                case '3':
                    return 0.4;
                case '4':
                    return 0.0;
            }
        }

        // Case 5
        return 0.0;
    }
}
