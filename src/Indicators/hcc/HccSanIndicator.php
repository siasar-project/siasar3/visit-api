<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\hcc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * HccSan indicator.
 *
 * Tabla 39: matriz de cálculo para saneamiento en centros de salud mixtos (HccSan)
 * | # | HCC_C1 | HCC_C2   | FsanPFem   | FsanPMas   | HCC_C5     | HCC_C6     | HccSan |
 * | 1 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | ≥ 1:100    | 1. Sí      | 1. Sí      | 1,00   |
 * | 2 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | ≥ 1:100    | 2. No      | 1. Sí      | 0,70   |
 * | 3 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | ≥ 1:100    | 1. Sí      | 2. No      | 0,70   |
 * | 4 | 1. Sí  | 1,2 o 3  | < 1:100    | ≥ 1:100    | Cualquiera | Cualquiera | 0,40   |
 * | 5 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | < 1:100    | Cualquiera | Cualquiera | 0,40   |
 * | 6 | 1. Sí  | 1,2 o 3  | < 1:100    | < 1:100    | Cualquiera | Cualquiera | 0,40   |
 * | 7 | 1. Sí  | 1,2 o 3  | Cualquiera | Cualquiera | 2. No      | 2. No      | 0,40   |
 * | 8 | 1. Sí  | 4, 5 o 6 | Cualquiera | Cualquiera | Cualquiera | Cualquiera | 0,00   |
 * | 9 | 2. No  | -        | -          | -          | -          | -          | 0,00   |
 *
 * Tabla 40: matriz de cálculo para saneamiento en centros de salud que atienden solo a mujeres (HccSan)
 * | # | HCC_C1 | HCC_C2   | FsanPFem   | HCC_C5     | HCC_C6     | HccSan |
 * | 1 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | 1. Sí      | 1. Sí      | 1,00   |
 * | 2 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | 2. No      | 1. Sí      | 0,70   |
 * | 3 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | 1. Sí      | 2. No      | 0,70   |
 * | 4 | 1. Sí  | 1,2 o 3  | < 1:100    | Cualquiera | Cualquiera | 0,40   |
 * | 5 | 1. Sí  | 1,2 o 3  | Cualquiera | 2. No      | 2. No      | 0,40   |
 * | 6 | 1. Sí  | 4, 5 o 6 | Cualquiera | Cualquiera | Cualquiera | 0,00   |
 * | 7 | 2. No  | -        | -          | -          | -          | 0,00   |
 *
 * Tabla 41: matriz de cálculo para saneamiento en centros de salud que atienen solo a hombres (HccSan)
 * | # | HCC_C1 | HCC_C2   | FsanPMas   | HCC_C6     | HccSan |
 * | 1 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | 1. Sí      | 1,00   |
 * | 2 | 1. Sí  | 1,2 o 3  | ≥ 1:100    | 2. No      | 0,70   |
 * | 3 | 1. Sí  | 1,2 o 3  | < 1:100    | Cualquiera | 0,40   |
 * | 4 | 1. Sí  | 4, 5 o 6 | Cualquiera | Cualquiera | 0,00   |
 * | 6 | 2. No  | -        | -          | -          | 0,00   |
 */
class HccSanIndicator extends AbstractIndicator
{
    private const COC1_100 = 1/100;

    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('HccSan', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.health.care';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // H c c San = f left (H C {C} rsub {C 1} , H C {C} rsub {C 2} , H C {C} rsub {C 5} , H C {C} rsub {C 6} , FsanPFem , FsanPMas right )

        //    • Si la suma de HCC_A6.1 y HCC_A6.2 es cero (no hay pacientes), entonces HCCHSH no se puede calcular.
        //      En esos casos, se recomienda marcar el indicador con un “no calculado” o “nulo” para distinguir que
        //      es un caso diferente a que el indicador pudiera valer cero. En todo caso, para los cálculos derivados
        //      que requieran un valor de HCCHSH, se puede usar el cero cuando HCCHSH no haya podido ser calculado
        //      (por  ejemplo, para el cálculo del índice HCC).
        $a6d1 = $this->getRecord()->{'field_female_patients_per_day'};
        $a6d2 = $this->getRecord()->{'field_male_patients_per_day'};
        if (($a6d1 + $a6d2) === 0) {
            return AbstractIndicator::VALUE_NOT_APPLICABLE;
        }

        $c1 = $this->getRecord()->{'field_have_toilets'};
        $c2 = $this->getRecord()->{'field_type_toilets'};
        $fSanPFem = $this->getSubindicatorValue(FsanPFemIndicator::class);
        $fSanPMas = $this->getSubindicatorValue(FsanPMasIndicator::class);
        $c5 = $this->getRecord()->{'field_toilets_menstrual_hygiene_facilities'};
        $c6 = $this->getRecord()->{'field_toilets_limited_mobility'};

        //    • El cálculo de la variable HccHyg es diferente según si el centro es mixto o no, por ello se presenta
        //      una tabla diferente para cada circunstancia:
        //        ◦ Si es un centro mixto (que atiene tanto a mujeres como a hombres) se usa la tabla correspondiente
        //          a centros de salud mixtos (Tabla 39).
        if (0 !== $a6d1 && 0 !== $a6d2) {
            if ($c1) {
                if (in_array($c2, ['1', '2', '3'])) {
                    // Case 1, 2 y 3
                    if ($fSanPFem >= static::COC1_100 && $fSanPMas >= static::COC1_100) {
                        if ($c5 && $c6) {
                            return 1.0;
                        }
                        if (!$c5 && $c6) {
                            return 0.7;
                        }
                        if ($c5 && !$c6) {
                            return 0.7;
                        }
                    }
                    // Case 4
                    if ($fSanPFem < static::COC1_100 && $fSanPMas >= static::COC1_100) {
                        return 0.4;
                    }
                    // Case 5
                    if ($fSanPFem >= static::COC1_100 && $fSanPMas < static::COC1_100) {
                        return 0.4;
                    }
                    // Case 6
                    if ($fSanPFem < static::COC1_100 && $fSanPMas < static::COC1_100) {
                        return 0.4;
                    }
                    // Case 7
                    if (!$c5 && !$c6) {
                        return 0.4;
                    }
                }
                // Case 8
                if (in_array($c2, ['4', '5', '6'])) {
                    return 0.0;
                }
            }

            return 0.0;
        }
        //        ◦ Si no hay pacientes femeninas (HCC_A6.1 = 0), entonces FsanPFem no se calcula y se aplica
        //          la tabla específica para centros de salud que atienen solo a hombres (Tabla 40).
        if (0 === $a6d1) {
            if ($c1) {
                // Cases 1-5
                if (in_array($c2, ['1', '2', '3'])) {
                    // Cases 1-3
                    if ($fSanPFem >= static::COC1_100) {
                        // Case 1
                        if ($c5 && $c6) {
                            return 1.0;
                        }
                        // Case 2
                        if (!$c5 && $c6) {
                            return 0.7;
                        }
                        // Case 3
                        if ($c5 && !$c6) {
                            return 0.7;
                        }
                    }
                    // Case 4
                    if ($fSanPFem < static::COC1_100) {
                        return 0.4;
                    }
                    // Case 5
                    if (!$c5 && !$c6) {
                        return 0.4;
                    }
                }
                // Case 6
                if (in_array($c2, ['4', '5', '6'])) {
                    return 0.0;
                }
            }

            // Case 7
            return 0.0;
        }
        //        ◦ Si no hay pacientes masculinos (HCC_A6.2 = 0), entonces FsanPMas no se calcula y se aplica
        //          la tabla específica para centros de salud que atienden solo a mujeres (Tabla 41).
        if (0 === $a6d2) {
            // Case 1-4
            if ($c1) {
                // Case 1-3
                if (in_array($c2, ['1', '2', '3'])) {
                    // Case 1 y 2
                    if ($fSanPMas >= static::COC1_100) {
                        // Case 1
                        if ($c6) {
                            return 1.0;
                        }
                        // Case 2
                        return 0.7;
                    }
                    // Case 3
                    if ($fSanPMas < static::COC1_100) {
                        return 0.4;
                    }
                }
                // Case 4
                if (in_array($c2, ['4', '5', '6'])) {
                    return 0.0;
                }
            }

            // Case 6
            return 0.0;
        }

        return 0.0;
    }
}
