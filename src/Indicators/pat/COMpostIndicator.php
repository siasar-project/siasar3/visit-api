<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * COMpost indicator.
 */
class COMpostIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('COMpost', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {

        /** @var Bool $c2d1 */
        $c2d1 = $this->getRecord()->{'field_providing_ta_system'};

        /** @var Bool $c2d2 */
        $c2d2 = $this->getRecord()->{'field_providing_ta_disinfection'};

        /** @var Bool $c2d3 */
        $c2d3 = $this->getRecord()->{'field_providing_ta_preventive'};

        /** @var Bool $c2d4 */
        $c2d4 = $this->getRecord()->{'field_providing_ta_administrative'};

        /** @var Bool $c2d5 */
        $c2d5 = $this->getRecord()->{'field_providing_ta_social'};

        /** @var Bool $c2d6 */
        $c2d6 = $this->getRecord()->{'field_providing_ta_evaluation'};

        /** @var Bool $c2d7 */
        $c2d7 = $this->getRecord()->{'field_providing_ta_innovation'};

        // {COM} rsub {post} =
        //      SI(PAT_C2.1.0 = 1 ; 1 ;0 ) +
        //      SI(PAT_C2.2.0 = 1 ; 1 ;0 ) +
        //      SI(PAT_C2.3.0 = 1 ; 1 ;0 ) +
        //      SI(PAT_C2.4.0 = 1 ; 1 ;0 ) +
        //      SI(PAT_C2.5.0 = 1 ; 1 ;0 ) +
        //      SI(PAT_C2.6.0 = 1 ; 1 ;0 ) +
        //      SI(PAT_C2.7.0 = 1 ; 1 ;0 )
        return $this->ternary($c2d1, 1, 0) +
            $this->ternary($c2d2, 1, 0) +
            $this->ternary($c2d3, 1, 0) +
            $this->ternary($c2d4, 1, 0) +
            $this->ternary($c2d5, 1, 0) +
            $this->ternary($c2d6, 1, 0) +
            $this->ternary($c2d7, 1, 0);
    }
}
