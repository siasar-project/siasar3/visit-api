<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * COMpre indicator.
 */
class COMpreIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('COMpre', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var Bool $c1d1 */
        $c1d1 = $this->getRecord()->{'field_assistance_provided_planning'};
        /** @var Bool $c1d2 */
        $c1d2 = $this->getRecord()->{'field_assistance_provided_supervision'};
        /** @var Bool $c1d3 */
        $c1d3 = $this->getRecord()->{'field_assistance_provided_creation'};

        // {COM} rsub {pre} =
        //      SI( TAP_ C1.1.0 = 1 ; 1 ;0 ) +
        //      SI( TAP_ C1.2.0 = 1 ; 1 ;0 ) +
        //      SI( TAP_ C1.3.0 = 1 ; 1 ;0 )
        return $this->ternary($c1d1, 1, 0) +
            $this->ternary($c1d2, 1, 0) +
            $this->ternary($c1d3, 1, 0);
    }
}
