<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FeqCalidad indicator.
 *
 * Alias: FeqQuality
 */
class FeqCalidadIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FeqCalidad', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b6d2 = $this->getRecord()->{'field_measurement_equipment'};
        $b6d2d1 = $this->getRecord()->{'field_measurement_equipment_quantity'};
        $b6d2d2 = $this->getRecord()->{'field_measurement_equipment_condition'};
        $a7 = $this->getRecord()->{'field_number_tap_should_assist'};

        if (0.0 === $a7) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }

        $rate = $b6d2d1 / $a7;

        return $this->ternary(
            !$b6d2 || '3' === $b6d2d2,
            0,
            $this->ternary(
                $rate >= (1/50),
                1,
                50 * $rate
            )
        );
    }
}
