<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FeqInfo indicator.
 *
 * Alias: FcompEq
 *
 * Tabla 43: matriz de cálculo del factor de equipos informáticos del PAT (FeqInfo)
 * | # | PAT_B1 | PAT_B6.3.0 | PAT_B6.3.2 | (PAT_B6.3.1 / PATpersonal) | FeqInfo |
 * | 1 | 1. Sí  | 1. Sí      | 1. Bien    | ≥ 0.50                     | 1,00    |
 * | 2 | 1. Sí  | 1. Sí      | 2. Regular | ≥ 0.50                     | 1,00    |
 * | 3 | 1. Sí  | 1. Sí      | 1. Bien    | < 0.50                     | {PAT_B6.3.1} over {PATpersonal} |
 * | 4 | 1. Sí  | 1. Sí      | 2. Regular | < 0.50                     | {PAT_B6.3.1} over {{TAP} rsub {PATpersonal}} |
 * | 5 | 1. Sí  | 1. Sí      | 3. Malo    | Cualquiera                 | 0,00    |
 * | 6 | 1. Sí  | 2. No      | -          | -                          | 0,00    |
 * | 7 | 2. No  | Cualquiera | Cualquiera | -                          | 0,00    |
 */
class FeqInfoIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FeqInfo', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b1 = $this->getRecord()->{'field_have_personnel_provide_assistance'};
        $b6d3d0 = $this->getRecord()->{'field_it_equipment'};
        $patPersonal = $this->getSubindicatorValue(PATpersonalIndicator::class);
        $b6d3d1 = $this->getRecord()->{'field_it_equipment_quantity'};
        $b6d3d2 = $this->getRecord()->{'field_it_equipment_condition'};

        if ($b1) {
            if ($b6d3d0) {
                switch ($b6d3d2) {
                    case 1:
                        // Good
                    case 2:
                        // Fair
                        if (0.0 === $patPersonal) {
                            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
                        }
                        $rate = $b6d3d1 / $patPersonal;
                        if ($rate >= 0.5) {
                            return 1;
                        }

                        return $rate;

                    case 3:
                        // Poor
                        // Default response.
                        break;
                }
            }
        }

        return 0;
    }
}
