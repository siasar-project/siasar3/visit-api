<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FInternet indicator.
 *
 * Tabla 44: matriz de cálculo del factor de servicio de Internet del PAT (FInternet)
 * | # | PAT_B7.2.0 | PAT_B7.2.1 | FInternet |
 * | 1 | 1. Sí      | 1. Bueno   | 1,00      |
 * | 2 | 1. Sí      | 2. Regular | 0,70      |
 * | 3 | 1. Sí      | 3. Malo    | 0,40      |
 * | 4 | 2. No      | -          | 0,00      |
 */
class FinternetIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FInternet', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b7d2d0 = $this->getRecord()->{'field_facilities_and_services_internet'};
        $b7d2d1 = $this->getRecord()->{'field_facilities_and_services_internet_state'};
        if ($b7d2d0) {
            switch ($b7d2d1) {
                case 1:
                    // Good
                    return 1;
                case 2:
                    // Fair
                    return 0.7;
                case 3:
                    // Poor
                    return 0.4;
            }
        }

        return 0;
    }
}
