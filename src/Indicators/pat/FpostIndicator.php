<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * Fpost indicator.
 *
 * Tabla 48: matriz de cálculo del factor de diversidad del PAT en fase de post construcción (Fpost)
 * | # | COMpostR / COMpos  | Fpost |
 * | 1 | ≥ 0,75             | 1,00  |
 * | 2 | ≥ 0,50 y < 0,75    | 0,70  |
 * | 3 | > 0 y < 0,50       | 0,40  |
 * | 4 | 0                  | 0,00  |
 */
class FpostIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('Fpost', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {F} rsub {post} =f( {COM} rsub {post} ;  {COM} rsub {postR} )
        $comPostR = $this->getSubindicatorValue(COMpostRIndicator::class);
        $comPost = $this->getSubindicatorValue(COMpostIndicator::class);

        if (0.0 === $comPost) {
            return 0;
        }

        $rate = $comPostR / $comPost;

        if ($rate >= 0.75) {
            return 1;
        }
        if ($rate >= 0.5 && $rate < 0.75) {
            return 0.7;
        }
        if ($rate > 0.0 && $rate < 0.5) {
            return 0.4;
        }

        return 0;
    }
}
