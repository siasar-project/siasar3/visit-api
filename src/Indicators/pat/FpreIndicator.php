<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * Fpre indicator.
 *
 * Tabla 47: matriz de cálculo del factor de diversidad del PAT en fase de planificación, diseño y ejecución (Fpre)
 * | # | COMpreR / COMpre | Fpre |
 * | 1 | 1                | 1,00 |
 * | 2 | ≥ 0,50 y < 1     | 0,70 |
 * | 3 | > 0 y < 0,50     | 0,40 |
 * | 4 | 0                | 0,00 |
 */
class FpreIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('Fpre', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $comPreR = $this->getSubindicatorValue(COMpreRIndicator::class);
        $comPre = $this->getSubindicatorValue(COMpreIndicator::class);

        if (0.0 === $comPre) {
            return 0;
        }

        $rate = $comPreR / $comPre;

        if (1 === $rate) {
            return 1;
        }
        if ($rate >= 0.5 && $rate < 1) {
            return 0.7;
        }
        if ($rate > 0.0 && $rate < 0.5) {
            return 0.4;
        }

        return 0;
    }
}
