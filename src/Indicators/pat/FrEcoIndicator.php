<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FrEco indicator.
 *
 * Tabla 46: matriz de cálculo del factor de recursos económicos del PAT (FrEco )
 * | # | PAT_B5.1 | PAT_B5.2 | FrEco |
 * | 1 | 1. Sí    | 1. Sí    | 1,00  |
 * | 2 | 1. Sí    | 2. No    | 0,40  |
 * | 3 | 2. No    | -        | 0,00  |
 */
class FrEcoIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FrEco', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b5d1 = $this->getRecord()->{'field_annual_operational_budget'};
        /** @var bool $b5d2 2.5.2.1 */
        $b5d2 = $this->getRecord()->{'field_annual_operational_amount_exists'};

        if ($b5d1) {
            if ($b5d2) {
                return 1;
            }

            return 0.4;
        }

        return 0;
    }
}
