<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\pat;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * PATINT indicator.
 *
 * Alias: TAPINT
 */
class PATINTIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('PATINT', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.tap';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $a5 = $this->getRecord()->{'field_what_phase_provide_technical_assistance'};
        if ($a5) {
            $fPre = $this->getSubindicatorValue(FpreIndicator::class);

            if (is_array($a5) && in_array('1', $a5) && in_array('2', $a5)) {
                $fPost = $this->getSubindicatorValue(FpostIndicator::class);

                return ($fPre + $fPost) / 2;
            }
            if (is_array($a5) && in_array('1', $a5)) {
                return $fPre;
            }
            if (is_array($a5) && in_array('2', $a5)) {
                $fPost = $this->getSubindicatorValue(FpostIndicator::class);

                return $fPost;
            }
        }

        throw new Exception('Field form.tap\'s "1.5 - field_what_phase_provide_technical_assistance" is required.');
    }
}
