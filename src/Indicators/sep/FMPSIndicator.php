<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FMPS indicator.
 *
 * | # | SEP_A4             | SEP_B2.1         | FMPS |
 * | 1 | Tipo 3             | -                | 1,00 |
 * | 2 | Tipos 1, 2, 4 y 99 | 1. Legalizado    | 1,00 |
 * | 3 | Tipos 1, 2, 4 y 99 | 2. En proceso    | 0,70 |
 * | 4 | Tipos 1, 2, 4 y 99 | 3. No legalizado | 0,40 |
 */
class FMPSIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FMPS', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a4 */
        $a4 = $this->getRecord()->{'field_provider_type'};

        if ('3' === $a4) {
            return 1.0;
        }

        if ('5' === $a4) {
            throw new Exception('Field form.wsprovider\'s "1.4 - field_provider_type" with value "5" can\'t get here!.');
        }

        /** @var string $b2d1 */
        // Note: In indicators documentation the field is wrong. The valid field es 2.1.2.
        $b2d1 = $this->getRecord()->{'field_provider_legal_status'};

        switch ($b2d1) {
            case '1':
                // 1. Legalizado
                return 1.0;
            case '2':
                // 2. En proceso
                return 0.7;
            case '3':
                // 3. No legalizado
                return 0.4;
            default:
                return 0.0;
        }
    }
}
