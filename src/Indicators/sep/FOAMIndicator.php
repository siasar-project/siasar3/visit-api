<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FOAM indicator.
 *
 * Take each SYS_1.12 that reference this provider.
 */
class FOAMIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FOAM', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
//        $systems = $this->getPoint()->{'field_wsystems'};
//        $subforms1d12 = [];
//
//        foreach ($systems as $system) {
//            $served = $system->{'field_served_communities'};
//            foreach ($served as $subRecord) {
//                /** @var FormRecord $provider */
//                $provider = $subRecord->{'field_provider'};
//                if ($provider->getId() === $this->getRecord()->getId()) {
//                    $subforms1d12[] = new FOAMNote($system, $subRecord);
//                }
//            }
//        }
//
//        $sumFormj = 0;
//        $sumHH = 0;
//        foreach ($subforms1d12 as $item) {
//            $foamInkj = new FOAMintkijIndicator(new PointIndicatorContext($item->system, $this->getPoint()));
//            $foamInkP = new FOAMintkPjIndicator(new PointIndicatorContext($item->system, $this->getPoint()));
//            $foamTranj = new FOAMtranjIndicator(new PointIndicatorContext($item->system, $this->getPoint()));
//            $foamTratj = new FOAMtratjIndicator(new PointIndicatorContext($item->system, $this->getPoint()));
//            $foamStorj = new FOAMstorjIndicator(new PointIndicatorContext($item->system, $this->getPoint()));
//            $foamDistj = new FOAMdistjIndicator(new PointIndicatorContext($item->system, $this->getPoint()));
//
//            $a12d3 = $item->servedCommunity->{'field_households'};
//            $sumHH += $a12d3;
//
//            $sumFormj += (($foamInkj() + $foamInkP() + $foamTranj() + $foamTratj() + $foamStorj() + $foamDistj()) * $a12d3) / $a12d3;
//        }
//
//        return $sumFormj / $sumHH;
        $foamInkj = new FOAMintkijIndicator(new SimpleIndicatorContext($this->getRecord()));
        $foamInkP = new FOAMintkPjIndicator(new SimpleIndicatorContext($this->getRecord()));
        $foamTranj = new FOAMtranjIndicator(new SimpleIndicatorContext($this->getRecord()));
        $foamTratj = new FOAMtratjIndicator(new SimpleIndicatorContext($this->getRecord()));
        $foamStorj = new FOAMstorjIndicator(new SimpleIndicatorContext($this->getRecord()));
        $foamDistj = new FOAMdistjIndicator(new SimpleIndicatorContext($this->getRecord()));

        $nInfra = new NinfrajIndicator(new SimpleIndicatorContext($this->getRecord()));
        if (0.0 === $nInfra()) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }

        return ($foamInkj() + $foamInkP() + $foamTranj() + $foamTratj() + $foamStorj() + $foamDistj()) / $nInfra();
    }
}
