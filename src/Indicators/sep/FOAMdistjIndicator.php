<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;

/**
 * FOAMdistj indicator.
 *
 * Sum each value of wssystem.
 */
class FOAMdistjIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FOAMdistj', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $distributions = $this->getRecord()->{'field_distribution_infrastructure'};
        if (count($distributions) <= 0) {
            return 0.0;
        }

        $value = 0;
        foreach ($distributions as $distribution) {
            $indDistri = new FOAMdistijIndicator(new SimpleIndicatorContext($distribution));

            $value += $indDistri();
        }

        return $value / count($distributions);
    }
}
