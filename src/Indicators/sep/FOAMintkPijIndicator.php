<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FOAMintkPij indicator.
 *
 * Note: SYS_B14 (solo si hay bombeo, tal y como se expone en la nota aclaratoria del algoritmo)
 *
 * | # | SYS_B14 | SYS_B15.2 | FOAMintkPij |
 * | 1 | 1. Sí   | 2 o más   | 1,00        |
 * | 2 | 1. Sí   | 1         | 0,70        |
 * | 3 | 1. Sí   | 0         | 0,00        |
 * | 3 | 2. No   | -         | 0,00        |
 */
class FOAMintkPijIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FOAMintkPij', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.sourceintake';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var bool $b14 */
        $b14 = $this->getRecord()->{'field_pump_maintenance_conducted_last_year'};
        /** @var string $b15d2 */
        // select: 1, 2, 99
        $b15d2 = $this->getRecord()->{'field_maintenance_electrically'};
        if (!$b15d2) {
            $b15d2 = [];
        }

        if ($b14) {
            $nOpers = count($b15d2);
            if ($nOpers >= 2) {
                return 1.0;
            }
            if (1 === $nOpers) {
                return 0.7;
            }
        }

        return 0.0;
    }
}
