<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FOAMintkij indicator.
 *
 * | # | SYS_B14 | SYS_B15.1 | FOAMintkij |
 * | 1 | 1. Sí   | 4 o más   | 1,00       |
 * | 2 | 1. Sí   | 2 o 3     | 0,70       |
 * | 3 | 1. Sí   | 1         | 0,40       |
 * | 4 | 1. Sí   | 0         | 0,00       |
 * | 5 | 2. No   | -         | 0,00       |
 */
class FOAMintkijIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FOAMintkij', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // form.wssystem.sourceintake
        $subforms = $this->getRecord()->{'field_water_source_intake'};
        if (count($subforms) === 0) {
            return 0.0;
        }

        $sum = 0;
        foreach ($subforms as $subform) {
            /** @var bool $b14 */
            $b14 = $subform->{'field_pump_maintenance_conducted_last_year'};
            /** @var string $b15d1 */
            // select: 1, 2, 3, 4, 99
            $b15d1 = $subform->{'field_maintenance_all_types'};
            if (!$b15d1) {
                $b15d1 = [];
            }

            if ($b14) {
                $nOpers = count($b15d1);
                if ($nOpers >= 4) {
                    $sum += 1.0;
                    continue;
                }
                if (2 === $nOpers || 3 === $nOpers) {
                    $sum += 0.7;
                    continue;
                }
                if (1 === $nOpers) {
                    $sum += 0.4;
                    continue;
                }
            }

            $sum += 0.0;
        }

        return $sum / count($subforms);
    }
}
