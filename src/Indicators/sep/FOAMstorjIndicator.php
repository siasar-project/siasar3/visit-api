<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;

/**
 * FOAMstorj indicator.
 */
class FOAMstorjIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FOAMstorj', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $infrasts = $this->getRecord()->{'field_storage_infrastructure'};
        if (count($infrasts) <= 0) {
            return 0.0;
        }

        $value = 0;
        foreach ($infrasts as $infrast) {
            $subind = new FOAMstorijIndicator(new SimpleIndicatorContext($infrast));

            $value += $subind();
        }

        return $value / count($infrasts);
    }
}
