<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FOAMtranij indicator.
 *
 * | # | SYS_C7 | SYS_C8  | FOAMtranij |
 * | 1 | 1. Sí  | 3 o más | 1,00       |
 * | 2 | 1. Sí  | 2       | 0,70       |
 * | 3 | 1. Sí  | 1       | 0,40       |
 * | 4 | 1. Sí  | 0       | 0,00       |
 * | 5 | 2. No  | -       | 0,00       |
 */
class FOAMtranijIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FOAMtranij', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.transmisionline';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var bool $c7 */
        $c7 = $this->getRecord()->{'field_have_maintenance_conducted_last_year'};
        /** @var string $c8 */
        // select: 1, 2, 3, 99
        $c8 = $this->getRecord()->{'field_maintenance_last_year'};
        if (!$c8) {
            $c8 = [];
        }

        if ($c7) {
            $nOpers = count($c8);
            if ($nOpers >= 3) {
                return 1.0;
            }
            if (2 === $nOpers) {
                return 0.7;
            }
            if (1 === $nOpers) {
                return 0.4;
            }
        }

        return 0.0;
    }
}
