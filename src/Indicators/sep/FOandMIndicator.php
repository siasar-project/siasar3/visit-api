<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FO&M indicator.
 *
 * | #  | SEP_B4 | SEP_F1                                      | SEP_F3 | FO&M |
 * | 1  | 1. Sí  | 3. Sí, mantenimiento preventivo y colectivo | 1. Sí  | 1,00 |
 * | 2  | 1. Sí  | 1. Sí, mantenimiento preventivo             | 1. Sí  | 0,70 |
 * | 3  | 1. Sí  | 2. Sí, mantenimiento correctivo             | 1. Sí  | 0,70 |
 * | 4  | 1. Sí  | 3. Sí, mantenimiento preventivo y colectivo | 2. No  | 0,70 |
 * | 5  | 1. Sí  | 1. Sí, mantenimiento preventivo             | 2. No  | 0,70 |
 * | 6  | 1. Sí  | 2. Sí, mantenimiento correctivo             | 2. No  | 0,70 |
 * | 7  | 2. No  | 3. Sí, mantenimiento preventivo y colectivo | 2. No  | 0,40 |
 * | 8  | 2. No  | 3. Sí, mantenimiento preventivo y colectivo | 1. Sí  | 0,40 |
 * | 9  | 2. No  | 1. Sí, mantenimiento preventivo             | 2. No  | 0,40 |
 * | 10 | 2. No  | 3. Sí, mantenimiento preventivo y colectivo | 1. Sí  | 0,40 |
 * | 11 | 2. No  | 2. Sí, mantenimiento correctivo             | 2. No  | 0,40 |
 * | 12 | 2. No  | 3. Sí, mantenimiento preventivo y colectivo | 1. Sí  | 0,40 |
 * | 13 | 1. Sí  | 4. No                                       | 1. Sí  | 0,00 |
 * | 14 | 2. No  | 4. No                                       | 1. Sí  | 0,00 |
 * | 15 | 1. Sí  | 4. No                                       | 2. No  | 0,00 |
 * | 16 | 2. No  | 4. No                                       | 2. No  | 0,00 |
 */
class FOandMIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FO&M', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var bool $b4 */
        $b4 = $this->getRecord()->{'field_have_personnel_to_rural_service'};
        /** @var string $f1 */
        // Options: 1, 2, 3, 4
        $f1 = $this->getRecord()->{'field_carryout_om_last_year'};
        /** @var bool $f3 */
        $f3 = $this->getRecord()->{'field_om_adequate_material_resources'};

        // case 1
        if ($b4 && '3' === $f1 && $f3) {
            return 1.0;
        }
        // case 2
        if ($b4 && '1' === $f1 && $f3) {
            return 0.7;
        }
        // case 3
        if ($b4 && '2' === $f1 && $f3) {
            return 0.7;
        }
        // case 4
        if ($b4 && '3' === $f1 && !$f3) {
            return 0.7;
        }
        // case 5
        if ($b4 && '1' === $f1 && !$f3) {
            return 0.7;
        }
        // case 6
        if ($b4 && '2' === $f1 && !$f3) {
            return 0.7;
        }
        // case 7
        if (!$b4 && '3' === $f1 && !$f3) {
            return 0.4;
        }
        // case 8
        if (!$b4 && '3' === $f1 && $f3) {
            return 0.4;
        }
        // case 9
        if (!$b4 && '1' === $f1 && !$f3) {
            return 0.4;
        }
        // case 10
        if (!$b4 && '3' === $f1 && $f3) {
            return 0.4;
        }
        // case 11
        if (!$b4 && '2' === $f1 && !$f3) {
            return 0.4;
        }
        // case 12
        if (!$b4 && '3' === $f1 && $f3) {
            return 0.4;
        }

        // cases 13 to 16
        return 0.0;
    }
}
