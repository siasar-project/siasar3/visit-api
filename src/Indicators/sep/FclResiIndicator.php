<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use App\Tools\Measurements\ConcentrationMeasurement;

/**
 * FclRes indicator.
 */
class FclResiIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FclRes', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var bool $g8 */
        $g8 = $this->getRecord()->{'field_monitoring_residual_disinfectant'};
        /** @var bool $g11 */
        $g11 = $this->getRecord()->{'field_resultsresidual_disinfectant_available'};
        /** @var FormRecord $lastResidualTest */
        $lastResidualTest = $this->getLastResidualTests();
        if (!$lastResidualTest) {
            return 0.0;
        }
        /** @var ConcentrationMeasurement $g12d2 */
        // Unit: mg/l
        $g12d2 = $lastResidualTest->{'field_concentration'};
        if (!$g12d2) {
            return 0.0;
        }
        $g12d2->setUnit('milligrams per liter (water at 5 C)');
        $g12d2 = $g12d2->getValue();

        if ($g8 && $g11) {
            if ($g12d2 >= 0.3 && $g12d2 <= 1.0) {
                return 1.0;
            }
            if ($g12d2 > 1.0 && $g12d2 <= 5.0) {
                return 0.7;
            }
            if ($g12d2 < 0.3 && $g12d2 > 5) {
                return 0.4;
            }
        }

        return 0.0;
    }

    /**
     * Get last wssystem residual tests.
     *
     * @return FormRecord|null
     */
    protected function getLastResidualTests(): ?FormRecord
    {
        /** @var FormRecord $residualTests */
        $residualTests = $this->getRecord()->{'field_residual_disinfectant_test_results'};
        $younger = null;
        foreach ($residualTests as $residualTest) {
            if (!$younger) {
                $younger = $residualTest;
            } else {
                $youngerDate = $younger->{'field_changed'};
                $residualTestDate = $residualTest->{'field_changed'};
                // @see https://stackoverflow.com/a/49084782/6385708
                if (($residualTestDate <=> $youngerDate) === 1) {
                    $younger = $residualTest;
                }
            }
        }

        return $younger;
    }
}
