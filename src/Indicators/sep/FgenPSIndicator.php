<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FgenPS indicator.
 */
class FgenPSIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FgenPS', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // F g enPS = SI left ({SEP} rsub {B 2.4} = 0 ; 0 ; {1} over {2} · left (
        //      SI left ({{SEP} rsub {B 2.5}} over {{SEP} rsub {B 2.4}} > 0,40 ; 1 ;
        //      {{SEP} rsub {B 2.5}} over {{SEP} rsub {B 2.4}} right ) +
        //      SI left (B 4 = 1 ; SI left ({{SEP} rsub {B 5.1}} over
        //      {{SEP} rsub {B 5.1} + {SEP} rsub {B 5.2}} > 0,40 ; 1 ;
        //      {{SEP} rsub {B 5.1}} over {{SEP} rsub {B 5.1} +
        //      {SEP} rsub {B 5.2}} right ) ; 0 right ) right ) right )
        /** @var int $b2d4 */
        $b2d4 = $this->getRecord()->{'field_committee_members'};
        /** @var int $b2d5 */
        $b2d5 = $this->getRecord()->{'field_women_committee_members'};
        /** @var bool $b4 */
        $b4 = $this->getRecord()->{'field_have_personnel_to_rural_service'};
        /** @var int $b5d1 */
        $b5d1 = $this->getRecord()->{'field_personnel_women'};
        /** @var int $b5d2 */
        $b5d2 = $this->getRecord()->{'field_personnel_men'};

        if (0 === $b2d4) {
            return 0.0;
        }

        $part1 = $this->ternary(
            ($b2d5 / $b2d4 > 0.4),
            1,
            $b2d5 / $b2d4
        );
        $part2 = 0;
        if (($b5d1 + $b5d2) === 0) {
            $part2 = 0;
        } else {
            if ($b4 && ($b5d1 + $b5d2) > 0) {
                if (0.4 < $b5d1 / ($b5d1 + $b5d2)) {
                    $part2 = 1;
                } elseif (0 !== ($b5d1 + $b5d2) && 0.4 >= $b5d1 / ($b5d1 + $b5d2)) {
                    $part2 = $b5d1 / ($b5d1 + $b5d2);
                }
            }
        }

        return (1/2) * ($part1 + $part2);
    }
}
