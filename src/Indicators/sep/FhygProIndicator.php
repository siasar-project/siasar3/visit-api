<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FhygPro indicator.
 *
 * SEP_F5.1 => El PSA desarrolla acciones de higiene
 * SEP_F5.2 => N.º de acciones desarrolladas
 * FhygPro => Factor de promoción de la higiene
 *
 * | # | SEP_F5.1 | SEP_F5.2 | FhygPro |
 * | 1 | 1. Sí    | ≥ 3      | 1,00    |
 * | 2 | 1. Sí    | 2        | 0,70    |
 * | 3 | 1. Sí    | 1        | 0,40    |
 * | 4 | 1. Sí    | 0        | 0,00    |
 * | 5 | 2. No    | -        | 0,00    |
 */
class FhygProIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FhygPro', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var bool $f5d1 */
        $f5d1 = $this->getRecord()->{'field_promote_hygiene_practice'};
        /** @var string[] $f5d2 */
        $f5d2 = $this->getRecord()->{'field_types_hygiene_practice'};
        $actions = 0;
        if ($f5d2) {
            $actions = count($f5d2);
        }

        if (!$f5d1) {
            return 0.0;
        }

        if ($actions >= 3) {
            return 1.0;
        }

        if (2 === $actions) {
            return 0.7;
        }

        if (1 === $actions) {
            return 0.4;
        }

        return 0.0;
    }
}
