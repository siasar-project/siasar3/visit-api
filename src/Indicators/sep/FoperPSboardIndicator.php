<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * FoperPSboard indicator.
 *
 * | # | SEP_A4                       | SEP_00.1 – SEP_B2.1                              | SEP_B2.2 | FoperPSboard |
 * | 1 | Tipo 3                       | -                                                | -        | 1,00         |
 * | 4 | Tipos 1, 2, 4 y 99           | ≤ 2 años                                         | 1. Sí    | 1,00         |
 * | 3 | Tipos 1, 2, 4 y 99           | > 2 años y ≤ 5 años                              | 1. Sí    | 0,70         |
 * | 2 | Tipos 1, 2, 4 y 99           | > 5 años                                         | 1. Sí    | 0,40         |
 * | 5 | Tipos 1, 2, 4 y 99           | Cualquier fecha y tiempo de vigencia en el cargo | 2. No    | 0,00         |
 */
class FoperPSboardIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FoperPSboard', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a4 */
        $a4 = $this->getRecord()->{'field_provider_type'};
        /** @var \DateTime $z0d1 */
        $z0d1 = $this->getRecord()->{'field_questionnaire_date'};
        $z0d1 = $z0d1->setTimezone(new DateTimeZone('UTC'));
        /** @var array $b2d1 */
        // type: month_year
        $b2d1 = $this->getRecord()->{'field_last_election_committee_date'};
        /** @var bool $b2d2 */
        $b2d2 = $this->getRecord()->{'field_all_committee_inplace'};

        if ('3' === $a4) {
            return 1.0;
        }

        if ('5' === $a4) {
            throw new Exception('Field form.wsprovider\'s "1.4 - field_provider_type" with value "5" can\'t get here!.');
        }

        // $a4 in 1, 2, 4 y 99
        $testDate = DateTime::createFromFormat(
            'Y-m-d',
            $b2d1['year'].'-'.$b2d1['month'].'-1',
            new DateTimeZone('UTC')
        );
        /** @var DateInterval $age */
        $age = date_diff($testDate, $z0d1);

        if ($age->y <= 2 && $b2d2) {
            return 1.0;
        }
        if ($age->y > 2 && $age->y <= 5 && $b2d2) {
            return 0.7;
        }
        if ($age->y > 5 && $b2d2) {
            return 0.4;
        }

        return 0.0;
    }
}
