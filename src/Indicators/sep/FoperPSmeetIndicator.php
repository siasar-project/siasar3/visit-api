<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FoperPSmeet indicator.
 *
 * | # | SEP_A4             | SEP_B2.3 | FoperPSmeet |
 * | 1 | Tipo 3             | -        | 1,00        |
 * | 2 | Tipos 1, 2, 4 y 99 | ≥ 3      | 1,00        |
 * | 3 | Tipos 1, 2, 4 y 99 | 2        | 0,70        |
 * | 4 | Tipos 1, 2, 4 y 99 | 1        | 0,40        |
 * | 5 | Tipos 1, 2, 4 y 99 | 0        | 0,00        |
 */
class FoperPSmeetIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FoperPSmeet', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a4 */
        $a4 = $this->getRecord()->{'field_provider_type'};
        /** @var int $b3d2 */
        $b3d2 = $this->getRecord()->{'field_board_meets_last_year'};

        if ('3' === $a4) {
            return 1.0;
        }

        if ('5' === $a4) {
            throw new Exception('Field form.wsprovider\'s "1.4 - field_provider_type" with value "5" can\'t get here!.');
        }

        // $a4 in 1, 2, 4 y 99
        if ($b3d2 >= 3) {
            return 1.0;
        }
        if (2 === $b3d2) {
            return 0.7;
        }
        if (1 === $b3d2) {
            return 0.4;
        }

        return 0.0;
    }
}
