<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FoperPSregl indicator.
 *
 * | # | SEP_A8                                 | FoperPSregl |
 * | 1 | 1. Sí, y se aplica plenamente          | 1,00        |
 * | 2 | 2. Sí, pero de aplica de forma parcial | 0,70        |
 * | 3 | 3. Sí, pero no se aplica               | 0,40        |
 * | 4 | 4. No                                  | 0,00        |
 */
class FoperPSreglIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FoperPSregl', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a8 */
        $a8 = $this->getRecord()->{'field_provider_have_rules_for_service'};

        switch ($a8) {
            case '1':
                return 1.0;
            case '2':
                return 0.7;
            case '3':
                return 0.4;
            case '4':
            default:
                return 0.0;
        }
    }
}
