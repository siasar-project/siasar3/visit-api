<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use App\Tools\CurrencyAmount;

/**
 * Frec indicator.
 */
class FrecIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('Frec', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // FREC = SI(
        //  SEP_C8.1 = 0 o SEP_C8.1 = 98 o SEP_C8.2 = 98 ;
        //  0;
        //  SI(
        //      SEP_C8.3 = 0 o SEP_C8.3 = 98 o SEP_C8.4 = 98
        //      MINIMO (SEP_C8.2 / SEP_C8.1 , 1);
        //      MINIMO (SEP_C8.4 / SEP_C8.3 , 1)
        //  )
        // )

        /** @var bool $c8d1d1 */
        $c8d1d1 = $this->getRecord()->{'field_users_required_pay_exists'};
        /** @var int $c8d1d2 */
        $c8d1d2 = $this->getRecord()->{'field_users_required_pay'};
        /** @var bool $c8d2d1 */
        $c8d2d1 = $this->getRecord()->{'field_users_uptodate_payment_exists'};
        /** @var int $c8d2d2 */
        $c8d2d2 = $this->getRecord()->{'field_users_uptodate_payment'};

        if (0 === $c8d1d2 || !$c8d1d1 || !$c8d2d2) {
            return 0;
        }

        /** @var bool $c8d3d1 */
        $c8d3d1 = $this->getRecord()->{'field_monthly_amount_billed_exists'};
        /** @var CurrencyAmount $c8d3d2 */
        $c8d3d2 = $this->getRecord()->{'field_monthly_amount_billed'};
        /** @var bool $c8d4d1 */
        $c8d4d1 = $this->getRecord()->{'field_monthly_amount_revenue_exists'};
        /** @var CurrencyAmount $c8d4d2 */
        $c8d4d2 = $this->getRecord()->{'field_monthly_amount_revenue'};

        if (!$c8d3d1 || $c8d3d2->amount === 0 || !$c8d4d1) {
            return min(($c8d2d2 / $c8d1d2), 1);
        }

        return min(($c8d4d2->amount / $c8d3d2->amount), 1);
    }
}
