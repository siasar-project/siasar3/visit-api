<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use App\Tools\CurrencyAmount;
use Exception;

/**
 * Frent indicator.
 */
class FrentIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('Frent', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // FRENT = SI (
        //  SEP_D1.12.1 = 0 ;
        //      1 ;
        //      MINIMO ((12∙SEP_C8.4 + Inextra) / 12·SEP_D1.12.1 , 1)
        //  )

        // FRENT = SI (
        //  SEP_D1.12.1 = 98 o SEP_C8.4 = 98 ;
        //  0;
        //  SI(
        //      SEP_D1.12.1 = 0;
        //      1;
        //      MINIMO ((12∙SEP_C8.4 + Inextra) / 12·SEP_D1.12.1 , 1)
        //  )
        // )

        /** @var bool $d1d12d1 */
        $d1d12d1 = $this->getRecord()->{'field_total_expenses_exists'};
        /** @var bool $c8d4d1 */
        $c8d4d1 = $this->getRecord()->{'field_monthly_amount_revenue_exists'};

        if (!$d1d12d1 || !$c8d4d1) {
            return 0;
        }

        /** @var CurrencyAmount $d1d12d2 */
        $d1d12d2 = $this->getRecord()->{'field_total_expenses'};

        if ($d1d12d2->amount === 0) {
            return 1;
        }

        /** @var CurrencyAmount $c8d4d2 */
        $c8d4d2 = $this->getRecord()->{'field_monthly_amount_revenue'};
        $inExtra = new InextraIndicator(new SimpleIndicatorContext($this->getRecord()));

        return min(
            (12 * $c8d4d2->amount + $inExtra()) / (12 * $d1d12d2->amount),
            1
        );
    }
}
