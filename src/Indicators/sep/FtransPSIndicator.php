<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * FtransPS indicator.
 *
 * | # | SEP_E1 | SEP_E9                     | FtransPS |
 * | 1 | 1. Sí  | 1. Sí, al menos anualmente | 1,00     |
 * | 2 | 1. Sí  | 2. Sí, pero no anualmente  | 0,70     |
 * | 3 | 1. Sí  | 3. No                      | 0,40     |
 * | 4 | 2. No  | Cualquier respuesta        | 0,00     |
 */
class FtransPSIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FtransPS', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var Bool $e1 */
        $e1 = $this->getRecord()->{'field_uptodate_records_revenues_expenses'};
        /** @var string $e9 */
        $e9 = $this->getRecord()->{'field_disclosed_financial_information'};

        if ($e1) {
            switch ($e9) {
                case '1':
                    // 1. Sí, al menos anualmente
                    return 1.0;
                case '2':
                    // 2. Sí, pero no anualmente
                    return 0.7;
                case '3':
                    // 3. No
                    return 0.4;
            }
        }

        return 0.0;
    }
}
