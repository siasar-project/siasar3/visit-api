<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use App\Tools\CurrencyAmount;

/**
 * Inextra indicator.
 */
class InextraIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('Inextra', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // In rsub {extra} =
        //      SI left ({SEP} rsub {C 10.1 .1} = 1 ; {SEP} rsub {C 10.1 .2} ; 0 right ) +
        //      SI left ({SEP} rsub {C 10.2 .1} = 1 ; {SEP} rsub {C 10.2 .2} ; 0 right ) +
        //      SI left ({SEP} rsub {C 10.3 .1} = 1 ; {SEP} rsub {C 10.3 .2} ; 0 right ) +
        //      SI left ({SEP} rsub {C 10.4 .1} = 1 ; {SEP} rsub {C 10.4 .2} ; 0 right )
        /** @var bool $c10d1d1 */
        $c10d1d1 = $this->getRecord()->{'field_additional_revenue_sources_new_connections_exists'};
        /** @var CurrencyAmount $c10d1d2 */
        $c10d1d2 = $this->getRecord()->{'field_additional_revenue_sources_new_connections'};
        /** @var bool $c10d2d1 */
        $c10d2d1 = $this->getRecord()->{'field_additional_revenue_sources_subsidies_exists'};
        /** @var CurrencyAmount $c10d2d2 */
        $c10d2d2 = $this->getRecord()->{'field_additional_revenue_sources_subsidies'};
        /** @var bool $c10d3d1 */
        $c10d3d1 = $this->getRecord()->{'field_additional_revenue_sources_uncommon_exists'};
        /** @var CurrencyAmount $c10d3d2 */
        $c10d3d2 = $this->getRecord()->{'field_additional_revenue_sources_uncommon'};
        /** @var bool $c10d4d1 */
        $c10d4d1 = $this->getRecord()->{'field_additional_revenue_sources_contributions_exists'};
        /** @var CurrencyAmount $c10d4d2 */
        $c10d4d2 = $this->getRecord()->{'field_additional_revenue_sources_contributions'};

        return $this->ternary($c10d1d1, ($c10d1d2 ? $c10d1d2->amount : null), 0) +
            $this->ternary($c10d2d1, ($c10d2d2 ? $c10d2d2->amount: null), 0) +
            $this->ternary($c10d3d1, ($c10d3d2 ? $c10d3d2->amount: null), 0) +
            $this->ternary($c10d4d1, ($c10d4d2 ? $c10d4d2->amount: null), 0);
    }
}
