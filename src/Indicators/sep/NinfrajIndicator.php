<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * Ninfraj indicator.
 */
class NinfrajIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('Ninfraj', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {{N} rsub {infra}} rsub {j} =
        //      SI left ({{n} rsub {intk}} rsub {j} > 0 ; 1 ; 0 right ) +
        //      SI left ({{n} rsub {intkP}} rsub {j} > 0 ; 1 ; 0 right ) +
        //      SI left ({{n} rsub {tran}} rsub {j} > 0 ; 1 ; 0 right ) +
        //      SI left ({{n} rsub {tra t}} rsub {j} > 0 ; 1 ; 0 right ) +
        //      SI left ({{n} rsub {stor}} rsub {j} > 0 ; 1 ; 0 right ) +
        //      SI left ({{n} rsub {dis t}} rsub {j} > 0 ; 1 ; 0 right )
        $distributions = $this->getRecord()->{'field_distribution_infrastructure'};
        $intakes = $this->getRecord()->{'field_water_source_intake'};
        $infrasts = $this->getRecord()->{'field_storage_infrastructure'};
        $transmisions = $this->getRecord()->{'field_water_transmision_line'};
        $points = $this->getRecord()->{'field_treatment_points'};

        $a7 = $this->getRecord()->{'field_type_system'};
        $a8 = $this->getRecord()->{'field_pumps_location'};
        $intakesP = 0;

        if ("2" === $a7 && in_array($a8, ["1", "3"])) {
            $intakesP = 1;
        }
        $subforms = $this->getRecord()->{'field_water_source_intake'};
        foreach ($subforms as $subform) {
            if (0 === $intakesP) {
                $b4d2d1 = $subform->{'field_source_type'};
                $b4d2d2 = $subform->{'field_source_catchment_type_groundwater'};
                if (in_array($a7, ["4", "6"]) && ("1" === $b4d2d1 || in_array($b4d2d2, ["2", "3"]))) {
                    $intakesP = 1;
                }
                $b15d2 = $subform->{'field_maintenance_electrically'};
                if (0 === $intakesP && $b15d2 && 0 < count($b15d2)) {
                    $intakesP = 1;
                }
            }
        }

        return $this->ternary(count($distributions) > 0, 1, 0) +
            $this->ternary(count($intakes) > 0, 1, 0) +
            $this->ternary($intakesP > 0, 1, 0) +
            $this->ternary(count($infrasts) > 0, 1, 0) +
            $this->ternary(count($transmisions) > 0, 1, 0) +
            $this->ternary(count($points) > 0, 1, 0);
    }
}
