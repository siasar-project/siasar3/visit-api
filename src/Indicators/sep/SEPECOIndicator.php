<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * SEPECO indicator.
 *
 * Alias: PSEGEF
 */
class SEPECOIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('SEPECO', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a4 */
        $a4 = $this->getRecord()->{'field_provider_type'};

        if ('5' === $a4) {
            return 0.0;
        }

        // Estos casos especiales en los que SEPECO no puede ser calculado, son:
        //  SI SEP_C8.1 = 98, entonces SEPECO no se puede calcular
        //  SI SEP_C8.2 = 98, entonces SEPECO no se puede calcular
        //  SI SEP_C8.4 = 98, entonces SEPECO no se puede calcular
        //  SI SEP_D1.12.1 = 98, entonces SEPECO no se puede calcular
        $c8d1 = $this->getRecord()->{'field_users_required_pay_exists'};
        $c8d2 = $this->getRecord()->{'field_users_uptodate_payment_exists'};
        $c8d4 = $this->getRecord()->{'field_monthly_amount_revenue_exists'};
        $d1d12d1 = $this->getRecord()->{'field_total_expenses_exists'};

        // SEPECO = SI (SEP_C1 ≠ 1 ; 0; (FREC + FRENT) / 2)
        /** @var string $c1 */
        $c1 = $this->getRecord()->{'field_water_tariff_inplace_inuse'};
        if ('1' !== $c1) {
            return 0.0;
        }

        $fRect = new FrecIndicator(new SimpleIndicatorContext($this->getRecord()));
        $fRent = new FrentIndicator(new SimpleIndicatorContext($this->getRecord()));

        return ($fRect() + $fRent()) / 2;
    }
}
