<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * SEPOPM indicator.
 *
 * Alias: PSEGOM
 */
class SEPOPMIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('SEPOPM', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a4 */
        $a4 = $this->getRecord()->{'field_provider_type'};

        if ('5' === $a4) {
            return 0.0;
        }

        $FOandM = new FOandMIndicator(new SimpleIndicatorContext($this->getRecord()));
        $sum1 = $FOandM() / 3;

        $systems = $this->getPoint()->{'field_wsystems'};
        $sum2Dividendo = 0;
        $sum2Divisor = 0;
        foreach ($systems as $system) {
            $served = $system->{'field_served_communities'};
            foreach ($served as $subRecord) {
                /** @var FormRecord $provider */
                $provider = $subRecord->{'field_provider'};
                if ($provider->getId() === $this->getRecord()->getId()) {
                    $a12d3 = $subRecord->{'field_households'};
                    $sum2Divisor += $a12d3;
                    $a7 = $system->{'field_type_system'};
                    $foam = new FOAMIndicator(new SimpleIndicatorContext($system));
                    if (in_array($a7, ['3', '5'])) {
                        $sum2Dividendo += (($foam() * 2) / 3) * $a12d3;
                    } else {
                        $fclResj = new FclResiIndicator(new SimpleIndicatorContext($system));
                        $sum2Dividendo += (($foam() + $fclResj()) / 3) * $a12d3;
                    }
                }
            }
        }

        if (0 === $sum2Divisor) {
            return 0.0;
        }

        return $sum1 + ($sum2Dividendo / $sum2Divisor);
    }
}
