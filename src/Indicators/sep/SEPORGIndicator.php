<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\sep;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\SimpleIndicatorContext;

/**
 * SEPORG indicator.
 *
 * Alias: PSEGOR
 */
class SEPORGIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('SEPORG', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wsprovider';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var string $a4 */
        $a4 = $this->getRecord()->{'field_provider_type'};

        if ('5' === $a4) {
            return 0.0;
        }

        $fmps = new FMPSIndicator(new SimpleIndicatorContext($this->getRecord()));
        $fOperPS = new FoperPSIndicator(new SimpleIndicatorContext($this->getRecord()));
        $fOpenPS = new FgenPSIndicator(new SimpleIndicatorContext($this->getRecord()));
        $fTransPS = new FtransPSIndicator(new SimpleIndicatorContext($this->getRecord()));

        return ($fmps() + $fOperPS() + $fOpenPS() + $fTransPS()) / 4;
    }
}
