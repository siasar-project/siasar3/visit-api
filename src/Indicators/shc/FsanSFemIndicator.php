<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\shc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * FsanSFem indicator.
 */
class FsanSFemIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('FsanSFem', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.school';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {F} rsub {san S Fem} = {SHC_ C3.1.2 + {SHC_ C3.3.2} over {2}} over {SHC_ A6.1}
        $c3d1d2 = $this->getRecord()->{'field_girls_only_toilets_usable'};
        $c3d3d2 = $this->getRecord()->{'field_common_use_toilets_usable'};
        $a6d1 = $this->getRecord()->{'field_student_total_number_of_female'};

        if (0.0 === $a6d1) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }

        return (($c3d3d2 / 2) + $c3d1d2) / $a6d1;
    }
}
