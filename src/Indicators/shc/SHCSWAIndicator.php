<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\shc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * SHCSWA indicator.
 *
 * Tabla 33: matriz de cálculo del indicador de agua en centros educativos (SHCSWA)
 * | # | SHC_B1                             | SHC_B3 | SHCSWA |
 * | 1 | 1. Sí, compartido con la comunidad | A      | 1,00   |
 * | 2 | 2. Sí, exclusivo para el centro    | A      | 1,00   |
 * | 3 | 1. Sí, compartido con la comunidad | B      | 0,70   |
 * | 4 | 2. Sí, exclusivo para el centro    | B      | 0,70   |
 * | 5 | 1. Sí, compartido con la comunidad | C      | 0,40   |
 * | 6 | 2. Sí, exclusivo para el centro    | C      | 0,40   |
 * | 7 | 1. Sí, compartido con la comunidad | D      | 0,00   |
 * | 8 | 2. Sí, exclusivo para el centro    | D      | 0,00   |
 * | 9 | 3. No                              | -      | 0,00   |
 */
class SHCSWAIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('SHCSWA', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.school';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b1 = $this->getRecord()->{'field_have_water_supply_system'};
        $b3 = $this->getRecord()->{'field_functioning_of_water_system'};

        if (is_array($b1) && (in_array('1', $b1) || in_array('2', $b1))) {
            switch ($b3) {
                case 'A':
                    return 1.0;
                case 'B':
                    return 0.7;
                case 'C':
                    return 0.4;
                case 'D':
                    return 0.0;
            }
        }

        return 0.0;
    }
}
