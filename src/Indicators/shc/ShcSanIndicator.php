<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\shc;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;

/**
 * ShcSan indicator.
 *
 * Tabla 34: matriz de cálculo para saneamiento en centros educativos mixtos (ShcSan)
 * | #  | SHC_A4      | SHC_C1 | SHC_C2   | FsanSFem   | FsanSMas   | ECS_C5     | SHC_C6     | ShcSan |
 * | 1  | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:50     | ≥ 1:50     | 1. Sí      | 1. Sí      | 1,00   |
 * | 2  | Tipos 1 y 2 | 1. Sí  | 1,2 o 3  | ≥ 1:50     | ≥ 1:50     | 2. No      | 1. Sí      | 1,00   |
 * | 3  | Tipo 3      | 1. Sí  | 1,2 o 3  | ≥ 1:50     | ≥ 1:50     | 2. No      | 1. Sí      | 0,70   |
 * | 4  | Cualquiera  | 1. Sí  | 1,2 o 3  | 1:50 >     | ≥ 1:50     | 1. Sí      | 1. Sí      | 0,70   |
 * | 5  | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:50     | 1:50 >     | 1. Sí      | 1. Sí      | 0,70   |
 * | 6  | Cualquiera  | 1. Sí  | 1,2 o 3  | 1:50 >     | 1:50 >     | 1. Sí      | 1. Sí      | 0,70   |
 * | 7  | Tipo 3      | 1. Sí  | 1,2 o 3  | ≥ 1:75     | ≥ 1:75     | 1. Sí      | 2. No      | 0,70   |
 * | 8  | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:75     | ≥ 1:75     | 2. No      | 1. Sí      | 0,70   |
 * | 9  | Tipos 1 y 2 | 1. Sí  | 1,2 o 3  | ≥ 1:75     | ≥ 1:75     | 1. Sí      | 2. No      | 0,40   |
 * | 10 | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:75     | ≥ 1:75     | 2. No      | 2. No      | 0,40   |
 * | 11 | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:75     | < 1:75     | Cualquiera | Cualquiera | 0,40   |
 * | 12 | Cualquiera  | 1. Sí  | 1,2 o 3  | < 1:75     | ≥ 1:75     | Cualquiera | Cualquiera | 0,40   |
 * | 13 | Cualquiera  | 1. Sí  | 1,2 o 3  | < 1:75     | < 1:75     | Cualquiera | Cualquiera | 0,40   |
 * | 14 | Cualquiera  | 1. Sí  | 4, 5 o 6 | Cualquiera | Cualquiera | Cualquiera | Cualquiera | 0,00   |
 * | 15 | Cualquiera  | 2. No  | -        | -          | -          | -          | -          | 0,00   |
 *
 * Tabla 35: matriz de cálculo para saneamiento en centros educativos femeninos (ShcSan)
 * | # | SHC_A4      | SHC_C1 | SHC_C2   | FsanSFem        | SHC_C5     | SHC_C6     | ShcSan |
 * | 1 | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:50          | 1. Sí      | 1. Sí      | 1,00   |
 * | 2 | Tipos 1 y 2 | 1. Sí  | 1,2 o 3  | ≥ 1:50          | 2. No      | 1. Sí      | 1,00   |
 * | 3 | Tipo 3      | 1. Sí  | 1,2 o 3  | ≥ 1:50          | 2. No      | 1. Sí      | 0,70   |
 * | 4 | Cualquiera  | 1. Sí  | 1,2 o 3  | 1:50 > y ≥ 1:75 | 1. Sí      | 1. Sí      | 0,70   |
 * | 5 | Tipo 3      | 1. Sí  | 1,2 o 3  | ≥ 1:75          | 1. Sí      | 2. No      | 0,70   |
 * | 6 | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:75          | 2. No      | 1. Sí      | 0,70   |
 * | 7 | Tipos 1 y 2 | 1. Sí  | 1,2 o 3  | ≥ 1:75          | 1. Sí      | 2. No      | 0,40   |
 * | 8 | Cualquiera  | 1. Sí  | 1,2 o 3  | ≥ 1:75          | 2. No      | 2. No      | 0,40   |
 * | 9 | Cualquiera  | 1. Sí  | 1,2 o 3  | < 1:75          | Cualquiera | Cualquiera | 0,40   |
 * | 10 | Cualquiera | 1. Sí  | 4, 5 o 6 | Cualquiera      | Cualquiera | Cualquiera | 0,00   |
 * | 11 | Cualquiera | 2. No  | -        | -               | -          | -          | 0,00   |
 *
 * Tabla 36: matriz de cálculo para saneamiento en centros educativos masculinos (ShcSan)
 * | # | SHC_A4     | SHC_C1 | SHC_C2   | FsanSMas        | SHC_C6     | ShcSan |
 * | 1 | Cualquiera | 1. Sí  | 1,2 o 3  | ≥ 1:50          | 1. Sí      | 1,00   |
 * | 2 | Cualquiera | 1. Sí  | 1,2 o 3  | 1:50 > y ≥ 1:75 | 1. Sí      | 0,70   |
 * | 3 | Cualquiera | 1. Sí  | 1,2 o 3  | ≥ 1:75          | 2. No      | 0,40   |
 * | 4 | Cualquiera | 1. Sí  | 1,2 o 3  | < 1:75          | Cualquiera | 0,40   |
 * | 5 | Cualquiera | 1. Sí  | 4, 5 o 6 | Cualquiera      | Cualquiera | 0,00   |
 * | 6 | Cualquiera | 2. No  | -        | -               | -          | 0,00   |
 */
class ShcSanIndicator extends AbstractIndicator
{
    private const COC1_50 = 1/50;
    private const COC1_75 = 1/75;

    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('ShcSan', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.school';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // Si la suma de SHC_A6.1 y SHC_A6.2 es cero (no hay alumnado), entonces SHCSSH no se puede calcular.
        // En esos casos, se recomienda marcar el indicador con un “no calculado” o “nulo” para distinguir que
        // es un caso diferente a que el indicador pudiera valer cero.
        // En todo caso, para los cálculos derivados que requieran un valor de SHCSSH, se puede usar el cero
        // cuando SHCSSH no haya podido ser calculado (por ejemplo, para el cálculo del índice SHC).
        $a6d1 = $this->getRecord()->{'field_student_total_number_of_female'};
        $a6d2 = $this->getRecord()->{'field_student_total_number_of_male'};
        if (($a6d1 + $a6d2) === 0) {
            return AbstractIndicator::VALUE_NOT_APPLICABLE;
        }

        $a4 = $this->getRecord()->{'field_school_type'};
        $c1 = $this->getRecord()->{'field_school_have_toilets'};
        $c2 = $this->getRecord()->{'field_type_toilets'};
        $c5 = $this->getRecord()->{'field_toilets_with_menstrual_hygiene_facilities'};
        $c6 = $this->getRecord()->{'field_limited_mobility_vision_toilet'};

        // • El cálculo de la variable ShcHyg es diferente según si la escuela es mixta o no, por ello se presenta
        // una tabla diferente para cada circunstancia:
        //  ◦ Si es un centro mixto (con alumnas y alumnos) se usa la tabla correspondiente a
        //      escuelas mixtas (Tabla 34).
        if (0 !== $a6d1 && 0 !== $a6d2) {
            $fSanSFem = $this->getSubindicatorValue(FsanSFemIndicator::class);
            $fSanSMas = $this->getSubindicatorValue(FsanSMasIndicator::class);
            // case 1.
            if ($c1 && in_array($c2, ['1', '2', '3']) && $fSanSFem >= static::COC1_50 && $fSanSMas >= static::COC1_50 && $c5 && $c6) {
                return 1;
            }
            // case 2.
            if (is_array($a4) && in_array('1', $a4) && in_array('2', $a4) &&
                $c1 &&
                in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_50 && $fSanSMas >= static::COC1_50 &&
                !$c5 && $c6
            ) {
                return 1;
            }
            // case 3.
            if (is_array($a4) && in_array('3', $a4) &&
                $c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_50 && $fSanSMas >= static::COC1_50 &&
                !$c5 && $c6
            ) {
                return 0.7;
            }
            // case 4.
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem < static::COC1_50 && $fSanSMas >= static::COC1_50 &&
                $c5 && $c6
            ) {
                return 0.7;
            }
            // case 5.
            if ($c1 &&  in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_50 && $fSanSMas < static::COC1_50 &&
                $c5 && $c6
            ) {
                return 0.7;
            }
            // case 6.
            if ($c1 &&  in_array($c2, ['1', '2', '3']) &&
                $fSanSFem < static::COC1_50 && $fSanSMas < static::COC1_50 &&
                $c5 && $c6
            ) {
                return 0.7;
            }
            // case 7.
            if (is_array($a4) && in_array('3', $a4) &&
                $c1 &&  in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $fSanSMas >= static::COC1_75 &&
                $c5 && !$c6
            ) {
                return 0.7;
            }
            // case 8.
            if ($c1 &&  in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $fSanSMas >= static::COC1_75 &&
                !$c5 && $c6
            ) {
                return 0.7;
            }
            // case 9.
            if (is_array($a4) && in_array('1', $a4) && in_array('2', $a4) &&
                $c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $fSanSMas >= static::COC1_75 &&
                $c5 && !$c6
            ) {
                return 0.4;
            }
            // case 10.
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $fSanSMas >= static::COC1_75 &&
                !$c5 && !$c6
            ) {
                return 0.4;
            }
            // case 11.
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $fSanSMas < static::COC1_75
            ) {
                return 0.4;
            }
            // case 12.
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem < static::COC1_75 && $fSanSMas >= static::COC1_75
            ) {
                return 0.4;
            }
            // case 13.
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem < static::COC1_75 && $fSanSMas < static::COC1_75
            ) {
                return 0.4;
            }
            // case 14.
            if ($c1 && in_array($c2, ['4', '5', '6'])) {
                return 0.0;
            }

            // case 15.
            return 0.0;
        }
        //  ◦ Si no hay alumnas (SHC_A6.1 = 0), entonces FsanSFem no se calcula y se aplica la
        //       tabla específica para escuelas masculinas (Tabla 36).
        if (0 === $a6d1) {
            $fSanSMas = $this->getSubindicatorValue(FsanSMasIndicator::class);
            // case 1
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSMas >= static::COC1_50 && $c6
            ) {
                return 1.0;
            }
            // case 2
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                static::COC1_50 > $fSanSMas && $fSanSMas >= static::COC1_75 &&
                $c6
            ) {
                return 0.7;
            }
            // case 3
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSMas >= static::COC1_75 && !$c6
            ) {
                return 0.4;
            }
            // case 4
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSMas < static::COC1_75
            ) {
                return 0.4;
            }
            // case 5
            if ($c1 && in_array($c2, ['4', '5', '6'])) {
                return 0.0;
            }
            // case 6
            return 0.0;
        }
        //  ◦ Si no hay alumnos (SHC_A6.2 = 0), entonces FsanSMas no se calcula y se aplica la tabla
        //       específica para escuelas femeninas (Tabla 35).
        if (0 === $a6d2) {
            $fSanSFem = $this->getSubindicatorValue(FsanSFemIndicator::class);
            // case 1
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_50 && $c5 && $c6
            ) {
                return 1.0;
            }
            // case 2
            if (is_array($a4) && in_array('1', $a4) && in_array('2', $a4) &&
                $c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_50 && !$c5 && $c6
            ) {
                return 1.0;
            }
            // case 3
            if (is_array($a4) && in_array('3', $a4) &&
                $c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_50 && !$c5 && $c6
            ) {
                return 0.7;
            }
            // case 4
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                static::COC1_50 > $fSanSFem && $fSanSFem >= static::COC1_75 &&
                $c5 && $c6
            ) {
                return 0.7;
            }
            // case 5
            if (is_array($a4) && in_array('3', $a4) &&
                $c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $c5 && !$c6
            ) {
                return 0.7;
            }
            // case 6
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && !$c5 && $c6
            ) {
                return 0.7;
            }
            // case 7
            if (is_array($a4) && in_array('1', $a4) && in_array('2', $a4) &&
                $c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && $c5 && !$c6
            ) {
                return 0.4;
            }
            // case 8
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem >= static::COC1_75 && !$c5 && !$c6
            ) {
                return 0.4;
            }
            // case 9
            if ($c1 && in_array($c2, ['1', '2', '3']) &&
                $fSanSFem < static::COC1_75
            ) {
                return 0.4;
            }
            // case 10
            if ($c1 && in_array($c2, ['4', '5', '6'])) {
                return 0.0;
            }
            // case 11
            return 0.0;
        }

        return 0.0;
    }
}
