<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * Csto indicator.
 *
 * | # | HHsyst        | Csto |
 * | 1 | ≤ 200         | 1,00 |
 * | 2 | 200 < y ≤ 500 | 0,50 |
 * | 3 | > 500         | 0,25 |
 */
class CstoIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Csto', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $hHsyst = new HHsystIndicator($this->context);
        $hHsystValue = $hHsyst();

        if ($hHsystValue <= 200) {
            return 1.0;
        }
        if ($hHsystValue > 200 && $hHsystValue <= 500) {
            return 0.5;
        }

        // $hHsystValue > 500
        return 0.25;
    }
}
