<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * Fintki indicator.
 *
 * | # | SYS_B16 | Fintki |
 * | 1 | A       | 1,00   |
 * | 2 | B       | 0,70   |
 * | 3 | C       | 0,40   |
 * | 4 | D       | 0,00   |
 */
class FintkiIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Fintki', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.sourceintake';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b16 = $this->getRecord()->{'field_physical_state_intake'};

        switch ($b16) {
            case "A":
                return 1.0;
            case "B":
                return 0.7;
            case "C":
                return 0.4;
            case "D":
            default:
                return 0.0;
        }
    }
}
