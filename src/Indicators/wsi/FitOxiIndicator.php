<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Entity\AdministrativeDivision;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\wsp\WaterSystemNote;
use Exception;

/**
 * FitOxi indicator.
 *
 * | # | SYS_D3.5.1 | SYS_D3.5.2 | SYS_D3.5.4 | FitOxij |
 * | 1 | 1. Sí      | 1. Sí      | A          | 1,00    |
 * | 2 | 1. Sí      | 1. Sí      | B          | 0,70    |
 * | 3 | 1. Sí      | 1. Sí      | C          | 0,40    |
 * | 4 | 1. Sí      | 1. Sí      | D          | 0,00    |
 * | 5 | 1. Sí      | 2. No      | Cualquiera | 0,00    |
 * | 6 | 2. No      | -          | -          | 0,00    |
 */
class FitOxiIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('FitOxi', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.treatmentpoints';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var Bool $d3d5d1 */
        $d3d5d1 = $this->getRecord()->{'field_aeration_oxid'};
        /** @var Bool $d3d5d2 */
        $d3d5d2 = $this->getRecord()->{'field_aeration_oxid_functioning'};
        /** @var string $d3d5d4 */
        // Options: A, B, C, D
        $d3d5d4 = $this->getRecord()->{'field_aeration_oxid_state'};

        if ($d3d5d1) {
            if ($d3d5d2) {
                switch ($d3d5d4) {
                    case 'A':
                        return 1.0;
                    case 'B':
                        return 0.7;
                    case 'C':
                        return 0.4;
                }
            }
        }

        return 0.0;
    }
}
