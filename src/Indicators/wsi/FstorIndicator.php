<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * Fstor indicator.
 */
class FstorIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Fstor', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {F} rsub {stor} = {sum from {i = 1} to {{n} rsub {stor}} {{F stor} rsub {i}}} over {{n} rsub {stor}}
        $storages = $this->getRecord()->{'field_storage_infrastructure'};
        if (!$storages || count($storages) === 0) {
            return 0.0;
        }

        $value = 0.0;
        foreach ($storages as $storage) {
            $fStori = new FstoriIndicator(new PointIndicatorContext($storage, $this->getPoint()));
            $value += $fStori();
        }

        return $value / count($storages);
    }
}
