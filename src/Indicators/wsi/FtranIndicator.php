<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * Ftran indicator.
 */
class FtranIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Ftran', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {F} rsub {tran} = {sum from {i = 1} to {{n} rsub {tran}} {{F tran} rsub {i}}} over {{n} rsub {tran}}
        $transmissions = $this->getRecord()->{'field_water_transmision_line'};
        if (!$transmissions || count($transmissions) === 0) {
            return 0.0;
        }

        $value = 0.0;
        foreach ($transmissions as $transmission) {
            $fTrani = new FtraniIndicator(new PointIndicatorContext($transmission, $this->getPoint()));
            $value += $fTrani();
        }

        return $value / count($transmissions);
    }
}
