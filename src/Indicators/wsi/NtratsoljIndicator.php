<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * Ntratsolj indicator.
 */
class NtratsoljIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Ntratsolj', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.treatmentpoints';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {{n} rsub {tratsol}} rsub {j} =
        //      SI ({SYS} rsub {D3.1.1} = 1 ; 1 ; 0 ) +
        //      SI ({SYS} rsub {D3.2.1} = 1 ; 1 ; 0 ) +
        //      SI ({SYS} rsub {D3.3.1} = 1 ; 1 ; 0 ) +
        //      SI ({SYS} rsub {D3.4.1} = 1 ; 1 ; 0 ) +
        //      SI ({SYS} rsub {D3.5.1} = 1 ; 1 ; 0 )
        /** @var Bool $d3d1d1 */
        $d3d1d1 = $this->getRecord()->{'field_filtration'};
        $d3d2d1 = $this->getRecord()->{'field_coagu_floccu'};
        $d3d3d1 = $this->getRecord()->{'field_sedimentation'};
        $d3d4d1 = $this->getRecord()->{'field_desalination'};
        $d3d5d1 = $this->getRecord()->{'field_aeration_oxid'};

        return $this->ternary($d3d1d1, 1, 0) +
            $this->ternary($d3d2d1, 1, 0) +
            $this->ternary($d3d3d1, 1, 0) +
            $this->ternary($d3d4d1, 1, 0) +
            $this->ternary($d3d5d1, 1, 0);
    }
}
