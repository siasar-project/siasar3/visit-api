<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Entity\AdministrativeDivision;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Indicators\wsp\WaterSystemNote;
use Exception;

/**
 * Qdemand indicator.
 */
class QdemandIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Qdemand', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var WaterSystemDemandNote[] $notes */
        $notes = $this->getCommunitiesByThisWSSystem();
        $prov = 80;
        $aggregation = 0;

        foreach ($notes as $note) {
            $a4 = $note->community->{'field_total_population'};
            $a12d3 = $note->servedCommunity->{'field_households'};
            $a5 = $note->community->{'field_total_households'};
            if (0.0 === $a5) {
                throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
            }
            $aggregation += ($a4 * $a12d3) / $a5;
        }

        return $prov * $aggregation;
    }

    /**
     * Get communities server by this WSSystem.
     *
     * @return WaterSystemNote[]
     */
    protected function getCommunitiesByThisWSSystem(): array
    {
        $resp = [];
        $a1d12 = $this->getRecord()->{'field_served_communities'};
        $communities = $this->getPoint()->{'field_communities'};

        if ($a1d12) {
            foreach ($a1d12 as $item) {
                foreach ($communities as $community) {
                    /** @var AdministrativeDivision $sysDiv */
                    $sysDiv = $item->{'field_community'};
                    /** @var AdministrativeDivision $comDiv */
                    $comDiv = $community->{'field_region'};
                    if ($sysDiv->getId() === $comDiv->getId()) {
                        $resp[] = new WaterSystemDemandNote($this->getRecord(), $item, $community);
                    }
                }
            }
        }

        return $resp;
    }
}
