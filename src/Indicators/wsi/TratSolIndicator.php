<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * TratSol indicator.
 */
class TratSolIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('TratSol', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.treatmentpoints';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {TratSol} rsub {j} = SI left (
        //      {{n} rsub {tratsol}} rsub {j} = 0 ;
        //      0 ;
        //      {{{Fit} rsub {fil}} rsub {j} + {{Fit} rsub {coa}} rsub {j} +
        //          {{Fit} rsub {sed}} rsub {j} + {{Fit} rsub {sal}} rsub {j} +
        //          {{Fit} rsub {oxi}} rsub {j}} over {{{n} rsub {tratsol}} rsub {j}} right )
        $nTratsol = new NtratsoljIndicator($this->context);
        $fitFil = new FitFilIndicator($this->context);
        $fitCoa = new FitCoaIndicator($this->context);
        $fitSed = new FitSedIndicator($this->context);
        $fitSal = new FitSalIndicator($this->context);
        $fitOxi = new FitOxiIndicator($this->context);

        if (0.0 === $nTratsol()) {
            return 0.0;
        }

        return $this->ternary(
            0 === $nTratsol(),
            0,
            ($fitFil() + $fitCoa() + $fitSed() + $fitSal() + $fitOxi()) / $nTratsol()
        );
    }
}
