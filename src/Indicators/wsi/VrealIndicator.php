<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * Vreal indicator.
 */
class VrealIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Vreal', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // form.wssystem.storage
        $sub5s = $this->getRecord()->{'field_storage_infrastructure'};
        if ($sub5s && count($sub5s) > 0) {
            $resp = 0;
            foreach ($sub5s as $sub5) {
                // E4.1 => 5.4
                /** @var VolumeMeasurement $e4d1 */
                $e4d1 = $sub5->{'field_storage_capacity'};
                if ($e4d1) {
                    $e4d1->setUnit('litre');
                    $resp += $e4d1->getValue();
                }
            }

            return $resp;
        }

        return 0.0;
    }
}
