<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * WSICom indicator.
 */
class WSIComIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context, $cWater, $sumWSI, $sumA12d3)
    {
        parent::__construct('WSICom', $context);
        $this->cWater = $cWater;
        $this->sumWSI = $sumWSI;
        $this->sumA12d3 = $sumA12d3;
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {WSI} rsub {com} =
        //      {c} rsub {water} ∙
        //      {sum from {i = 1} to {n} {{WSI} rsub {i} ∙ {S Y {S} rsub {A 12.3}} rsub {i}}} over {sum from {i = 1} to {n} {{S Y {S} rsub {A 12.3}} rsub {i}}}
        if (0 === $this->sumA12d3) {
            return 0.0;
        }

        return $this->cWater * ($this->sumWSI / $this->sumA12d3);
    }
}
