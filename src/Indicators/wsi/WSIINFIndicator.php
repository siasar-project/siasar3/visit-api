<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * WSIINF indicator.
 *
 * Alias: EIAINF
 */
class WSIINFIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSIINF', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $f1d7 = $this->getRecord()->{'field_type_system'};
        if (in_array($f1d7, ['1', '2'])) {
            $f1d6 = $this->getRecord()->{'field_system_derived_urban'};
            if ($f1d6) {
                $fDist = new FdistIndicator($this->context);

                return $fDist();
            }
        }

        $sources = $this->getRecord()->{'field_water_source_intake'};
        $transmissions = $this->getRecord()->{'field_water_transmision_line'};

        $a7 = $this->getRecord()->{'field_type_system'};

        $fInk = new FintkIndicator($this->context);
        $fTran = new FtranIndicator($this->context);
        $fStor = new FstorIndicator($this->context);
        $fDist = new FdistIndicator($this->context);

        if (in_array($a7, ['3', '5'])) {
            // WSIINF = nroot {2} {{F} rsub {intk} ∙ {F} rsub {d is t}}
            return sqrt($fInk() * $fDist());
        }

        // WSIINF = nroot {4} {{F} rsub {intk} ∙ {F} rsub {tran} ∙ {F} rsub {stor} ∙ {F} rsub {d is t}}
        return ($fInk() * $fTran() * $fStor() * $fDist()) ** (1/4);
    }
}
