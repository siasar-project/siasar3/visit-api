<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * WSIPRO indicator.
 *
 * Alias: EIAZPA
 */
class WSIPROIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSIPRO', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $f1d7 = $this->getRecord()->{'field_type_system'};
        if (in_array($f1d7, ['1', '2'])) {
            $f1d6 = $this->getRecord()->{'field_system_derived_urban'};
            if ($f1d6) {
                return 1.0;
            }
        }

        $sources = $this->getRecord()->{'field_water_source_intake'};
        if (!$sources || count($sources) === 0) {
            return self::VALUE_ERROR;
        }

        $value = 0.0;
        foreach ($sources as $source) {
            $wsiproi = new WSIPROiIndicator(new PointIndicatorContext($source, $this->getPoint()));
            $value += $wsiproi();
        }

        return $value / count($sources);
    }
}
