<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * WSIPROi indicator.
 */
class WSIPROiIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSIPROi', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem.sourceintake';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $b4d2d1 = $this->getRecord()->{'field_source_catchment_type_surface'};
        $b9d3 = $this->getRecord()->{'field_risk_household_contamination'};
        $b9d4 = $this->getRecord()->{'field_risk_chemical_contamination'};

        if ('1' === $b4d2d1) {
            $pos = $this->ternary(!$b9d3, 1, 0) + $this->ternary(!$b9d4, 1, 0);
            $apl = 2;
        } else {
            $b8 = $this->getRecord()->{'field_water_protected'};
            $b9d1 = $this->getRecord()->{'field_presence_vegetation'};
            $b9d2 = $this->getRecord()->{'field_erosion'};

            $pos = $this->ternary(!$b9d3, 1, 0) +
                $this->ternary(!$b9d4, 1, 0) +
                $this->ternary('1' === $b8, 1, 0) +
                $this->ternary('1' === $b9d1, 1, 0) +
                $this->ternary('2' === $b9d2, 1, 0);

            $apl = $this->ternary('97' === $b8, 0, 1) +
                $this->ternary('97' === $b9d1, 0, 1) +
                $this->ternary('97' === $b9d2, 0, 1) +
                2;
        }

        return $pos / $apl;
    }
}
