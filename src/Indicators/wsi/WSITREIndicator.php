<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;

/**
 * WSITRE indicator.
 */
class WSITREIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSITRE', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $f1d7 = $this->getRecord()->{'field_type_system'};
        if (in_array($f1d7, ['1', '2'])) {
            $f1d6 = $this->getRecord()->{'field_system_derived_urban'};
            if ($f1d6) {
                return 1.0;
            }
        }

        $a7 = $this->getRecord()->{'field_type_system'};
        if (in_array($a7, ['3', '5'])) {
            return 0.0;
        }

        $d = $this->getRecord()->{'field_treatment_points'};
        if (!$d || count($d) <= 0) {
            return 0.0;
        }

        $value = 0.0;
        foreach ($d as $item) {
            $wsitrej = new WSITREjIndicator(new PointIndicatorContext($item, $this->getPoint()));
            $value += $wsitrej();
        }

        return $value / count($d);
    }
}
