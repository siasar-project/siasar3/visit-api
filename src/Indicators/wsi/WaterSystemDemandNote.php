<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsi;

use App\Forms\FormRecord;

/**
 * Utility class to QdemandIndicator.
 */
class WaterSystemDemandNote
{
    // 'form.wssystem' => $waterSystem,
    public FormRecord $system;

    // 'form.wssystem' => $waterSystem,
    public FormRecord $servedCommunity;

    // 'form.community' => $waterSystem,
    public FormRecord $community;

    /**
     * @param FormRecord $system
     * @param FormRecord $servedCommunity
     * @param FormRecord $community
     */
    public function __construct(FormRecord $system, FormRecord $servedCommunity, FormRecord $community)
    {
        $this->system = $system;
        $this->community = $community;
        $this->servedCommunity = $servedCommunity;
    }
}
