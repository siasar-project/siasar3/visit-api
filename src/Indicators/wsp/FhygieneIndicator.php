<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * Fhygiene indicator.
 */
class FhygieneIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('Fhygiene', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // 4.2.2
        $d2 = $this->getRecord()->{'field_with_water_and_soap'};
        $a5 = $this->getRecord()->{'field_total_households'};
        if (0.0 === $a5) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }

        return $d2 / $a5;
    }
}
