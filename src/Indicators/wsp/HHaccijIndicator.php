<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * HHaccij indicator.
 *
 * | # | SYS_A7                 | SYS_F5   | SYS_F8                | Faccij     |
 * | 1 | Tipos 1 y 2            | 1. Sí    | -                     | SYS_A12.3i |
 * | 2 | Tipos 1 y 2            | 2. No    | 1. 30 minutos o menos | SYS_A12.3i |
 * | 3 | Tipos 1 y 2            | 2. No    | 2. Más de 30 minutos  | 0,00       |
 * | 4 | Tipos 1 y 2            | 3. Mixta | 1. 30 minutos o menos | SYS_A12.3i |
 * | 5 | Tipos 1 y 2            | 3. Mixta | 2. Más de 30 minutos  | SYS_F6     |
 * | 6 | Tipos 3, 4, 5, 6 y 99  | -        | 1. 30 minutos o menos | SYS_A12.3i |
 * | 7 | Tipos 3, 4, 5, 6 y 99  | -        | 2. Más de 30 minutos  | 0,00       |
 */
class HHaccijIndicator extends AbstractIndicator
{
    protected $forceCalculate = true;

    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context, array $addlData = [])
    {
        parent::__construct('HHaccij', $context);
        $this->addlData = $addlData;
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // form.wssystem
        // {HH} rsub {accij} = f left (
        //      S Y {S} rsub {A 7} ,
        //      S Y {S} rsub {F 5} ,
        //      S Y {S} rsub {F 6} ,
        //      S Y {S} rsub {F 8}
        // right )
        $a7 = $this->getRecord()->{'field_type_system'};
        // form.wssystem.distribution
        $comRegion = $this->getRecord()->{'field_region'};
        if (array_key_exists('comRegion', $this->addlData)) {
            $comRegion = $this->addlData['comRegion'];
        }
        $distribution = $this->getDistribution($comRegion);
        $f5 = $distribution->{'field_serve_household'};
        $f6 = $distribution->{'field_households_connection'};
        $f8 = $distribution->{'field_accessibility_public_standpost'};

        $a1d12 = null;
        if (array_key_exists('a1d12', $this->addlData)) {
            $a1d12 = $this->addlData['a1d12'];
        }

        if (in_array($a7, ['1', '2'])) {
            switch ($f5) {
                // Si
                case '1':
                    return $a1d12;
                // No
                case '2':
                    if ('1' === $f8) {
                        return $a1d12;
                    }
                    // $f8 === 2
                    return 0.0;
                // Mixta
                case '3':
                    if ('1' === $f8) {
                        return $a1d12;
                    }
                    // $f8 === 2
                    return $f6;
            }
        }

        // $a7 in [3, 4, 5, 6, 99]
        if ('1' === $f8) {
            return $a1d12;
        }
        // $f8 === 2
        return 0.0;
    }
}
