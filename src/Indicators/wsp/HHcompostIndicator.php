<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * HHcompost indicator.
 */
class HHcompostIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('HHcompost', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {HH} rsub {c ompost} =
        //      left ({COM} rsub {C 1.8 .1} − {COM} rsub {C 1.8 .1 .2} right ) +
        //      left ({COM} rsub {C 1.9 .1} − {COM} rsub {C 1.9 .1 .2} right ) +
        //      left ({COM} rsub {C 1.10 .1} − {COM} rsub {C 1.10 .1 .2} right )
        $c1d8d1 = $this->getRecord()->{'field_twinpit_with_slab_number'};
        $c1d8d1d2 = $this->getRecord()->{'field_n_hh_dpit_compost_slab_no_members'};
        $c1d9d1 = $this->getRecord()->{'field_twinpit_without_slab_number'};
        $c1d9d1d2 = $this->getRecord()->{'field_n_hh_dpit_compost_nslab_nmembers'};
        $c1d10d2 = $this->getRecord()->{'field_other_composting_toilet_number'};
        $c1d10d1d2 = $this->getRecord()->{'field_n_hh_other_compost_nmembers'};

        return ($c1d8d1 - $c1d8d1d2) +
            ($c1d9d1 - $c1d9d1d2) +
            ($c1d10d2 - $c1d10d1d2);
    }
}
