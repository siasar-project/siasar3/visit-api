<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * HHdryPit indicator.
 */
class HHdryPitIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('HHdryPit', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {HH} rsub {dryPit} =
        //      left ({COM} rsub {C 1.6 .1} − {COM} rsub {C 1.6 .1 .2} right ) +
        //      left ({COM} rsub {C 1.7 .1} − {COM} rsub {C 1.7 .2} right )
        $c1d6d1 = $this->getRecord()->{'field_pit_latrine_with_slab_number'};
        $c1d6d1d2 = $this->getRecord()->{'field_n_hh_pit_slab_share_no_members'};
        $c1d7d1 = $this->getRecord()->{'field_pit_latrine_without_slab_number'};
        $c1d7d1d2 = $this->getRecord()->{'field_n_hh_pit_noslab_share_no_members'};

        return ($c1d6d1 - $c1d6d1d2) +
            ($c1d7d1 - $c1d7d1d2);
    }
}
