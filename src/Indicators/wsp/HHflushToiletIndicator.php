<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * HHflushToilet indicator.
 */
class HHflushToiletIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('HHflushToilet', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // {HH} rsub {flushToilet} =
        //  left ({COM} rsub {C 1.1 .1} − {COM} rsub {C 1.1 .1 .2} right ) +
        //  left ({COM} rsub {C 1.2 .1} − {COM} rsub {C 1.2 .1 .2} right ) +
        //  left ({COM} rsub {C 1.3 .1} − {COM} rsub {C 1.3 .1 .2} right ) +
        //  left ({COM} rsub {C 1.4 .1} − {COM} rsub {C 1.4 .1 .2} right ) +
        //  left ({COM} rsub {C 1.5 .1} − {COM} rsub {C 1.5 .1 .2} right )
        $c1d1d1 = $this->getRecord()->{'field_sewer_connection_number'};
        $c1d1d1d2 = $this->getRecord()->{'field_n_hh_siphon_other_people'};
        $c1d2d1 = $this->getRecord()->{'field_septic_tank_number'};
        $c1d2d1d2 = $this->getRecord()->{'field_n_hh_tank_other_people'};
        $c1d3d1 = $this->getRecord()->{'field_pit_latrine_number'};
        $c1d3d1d2 = $this->getRecord()->{'field_n_hh_flushing_other_people'};
        $c1d4d1 = $this->getRecord()->{'field_without_containment_number'};
        $c1d4d1d2 = $this->getRecord()->{'field_n_hh_open_drain_other_people'};
        $c1d5d1 = $this->getRecord()->{'field_unknown_number'};
        $c1d5d1d2 = $this->getRecord()->{'field_n_hh_unknown_dest_other_people'};

        return ($c1d1d1 - $c1d1d1d2) +
            ($c1d2d1 - $c1d2d1d2) +
            ($c1d3d1 - $c1d3d1d2) +
            ($c1d4d1 - $c1d4d1d2) +
            ($c1d5d1 - $c1d5d1d2);
    }
}
