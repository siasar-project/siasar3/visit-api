<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * SHL indicator.
 *
 * Alias: NSH
 */
class SHLIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('SHL', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $sHLSSL = new SHLSSLIndicator($this->context);
        $sHLODL = new SHLODLIndicator($this->context);
        $sHLSSP = new SHLSSPIndicator($this->context);
        $sHLHHP = new SHLHHPIndicator($this->context);

        return $this->getWeight('ssl') * $sHLSSL() +
            $this->getWeight('odl') * $sHLODL() +
            $this->getWeight('ssp') * $sHLSSP() +
            $this->getWeight('hhp') * $sHLHHP();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'ssl' => 1/4,
                'odl' => 1/4,
                'ssp' => 1/4,
                'hhp' => 1/4,
            ]
        );
    }
}
