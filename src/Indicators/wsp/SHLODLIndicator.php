<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * SHLODL indicator.
 *
 * Alias: NSHHPE
 *
 * | # | Fdef            | SHLODL |
 * | 1 | < 0,01          | 1,00 |
 * | 2 | 0,03 > y ≥ 0,01 | 0,70 |
 * | 3 | 0,05 > y ≥ 0,03 | 0,40 |
 * | 4 | ≥ 0,05          | 0,00 |
 *
 * Fdef => Factor de defecación al aire libre
 * SHLODL => Indicador de nivel de defecación al aire libre
 */
class SHLODLIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('SHLODL', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $a5 = $this->getRecord()->{'field_total_households'};
        if (0.0 === $a5) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }
        $c1d12d1 = $this->getRecord()->{'field_no_facility_number'};
        $factor = $c1d12d1 / $a5;

        if ($factor < 0.01) {
            return 1.0;
        }
        if ($factor < 0.03 && $factor >= 0.01) {
            return 0.7;
        }
        if ($factor < 0.05 && $factor >= 0.03) {
            return 0.4;
        }

        return 0.0;
    }
}
