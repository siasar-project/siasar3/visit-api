<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * WSLACC indicator.
 *
 * Alias: NSAACC
 */
class WSLACCIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSLACC', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $wsystemNotes = $this->getCommunityIn1d12();
        $a5 = $this->getRecord()->{'field_total_households'};
        if (0.0 === $a5) {
            throw new Exception(sprintf('Division by zero in "%s".', $this->getIndicatorCode()));
        }

        if (count($wsystemNotes) <= 0) {
            // WSLACC = SI left ({COM} rsub {B 3.2 .1} > 0 ; {{COM} rsub {B 3.2 .1}} over {{COM} rsub {A 5}} ; 0 right )
            $b3d2d1 = $this->getRecord()->{'field_households_with_individual_rainwater_collection'};

            return $this->ternary(
                $b3d2d1 > 0,
                $b3d2d1 / $a5,
                0
            );
        }

        // WSLACC = {C} rsub {water} ∙ {F} rsub {acc}
        $fAccSum1 = 0;
        $fAccSum2 = 0;
        foreach ($wsystemNotes as $wsystemNote) {
            $hHaccij = $wsystemNote->hHaccij;
            $fAccSum1 += $hHaccij;
            $fAccSum2 += $wsystemNote->servedCommunity->{'field_households'};
        }
        $fAcc = $fAccSum1 / $fAccSum2;

        return $this->getSubindicatorValue(CWaterIndicator::class) * $fAcc;
    }
}
