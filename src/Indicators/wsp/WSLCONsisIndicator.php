<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * WSLCONsis indicator.
 *
 * | # | SYS_F10.1  | SYS_F10.2  | WSLCONsisij |
 * | 1 | ≥ 5        | ≥ 12       | 1,00        |
 * | 2 | 4          | ≥ 12       | 0,70        |
 * | 3 | ≥ 4        | ≥ 6 y < 12 | 0,40        |
 * | 4 | 3          | ≥ 6        | 0,40        |
 * | 5 | Cualquiera | < 6        | 0,00        |
 * | 6 | < 3        | Cualquiera | 0,00        |
 */
class WSLCONsisIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('WSLCONsis', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // form.wssystem.distribution
        $distribution = $this->getLastDistribution();
        if (!$distribution) {
            throw new Exception(
                'Field form.wssystem\'s "6 - field_distribution_infrastructure" is required.'
            );
        }
        $f10d1 = $distribution->{'field_service_week_days'};
        $f10d2 = $distribution->{'field_service_week_hours'};

        // Row 1
        if ($f10d1 >= 5 && $f10d2 >= 12) {
            return 1.0;
        }
        // Row 2
        if (4 === $f10d1 && $f10d2 >= 12) {
            return 0.7;
        }
        // Row 3
        if ($f10d1 >= 4 && ($f10d2 >= 6 && $f10d2 < 12)) {
            return 0.4;
        }
        // Row 4
        if (3 === $f10d1 && $f10d2 >= 6) {
            return 0.4;
        }

        // Rows 5 & 6.
        return 0.0;
    }
}
