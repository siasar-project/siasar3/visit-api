<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * WSL indicator.
 *
 * Alias: IAS
 */
class WSLIndicator extends AbstractIndicator
{
    /**
     * @param PointIndicatorContext $context
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSL', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $wSLACC = new WSLACCIndicator($this->context);
        $wSLCON = new WSLCONIndicator($this->context);
        $wSLSEA = new WSLSEAIndicator($this->context);
        $wSLQUA = new WSLQUAIndicator($this->context);

        return $this->getWeight('acc') * $wSLACC() +
            $this->getWeight('con') * $wSLCON() +
            $this->getWeight('sea') * $wSLSEA() +
            $this->getWeight('qua') * $wSLQUA();
    }

    /**
     * @inheritDoc
     */
    protected function setWeights(): void
    {
        $this->weight = new FrozenParameterBag(
            [
                'acc' => 1/4,
                'con' => 1/4,
                'sea' => 1/4,
                'qua' => 1/4,
            ]
        );
    }
}
