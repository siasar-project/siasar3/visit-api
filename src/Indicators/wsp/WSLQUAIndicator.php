<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Entity\AdministrativeDivision;
use App\Indicators\AbstractIndicator;
use App\Indicators\PointIndicatorContext;
use Exception;

/**
 * WSLQUA indicator.
 *
 * Alias: NSACAL
 */
class WSLQUAIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(PointIndicatorContext $context)
    {
        parent::__construct('WSLQUA', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.community';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        $wsystemNotes = $this->getCommunityIn1d12();
        if (count($wsystemNotes) <= 0) {
            return 0.0;
        }

        $sum1 = 0;
        $sum2 = 0;
        foreach ($wsystemNotes as $wsystemNote) {
            $g1 = $wsystemNote->system->{'field_water_quality_monitored'};
            $g5 = $wsystemNote->system->{'field_quality_results_available'};
            $households = $wsystemNote->servedCommunity->{'field_households'};
            $sum1 += $households * $wsystemNote->wSLQUAsis;
            $sum2 += $households;
        }
        if (0 === $sum2) {
            return 0.0;
        }
        $acc = $sum1 / $sum2;

        return $this->getSubindicatorValue(CWaterIndicator::class) * $acc;
    }
}
