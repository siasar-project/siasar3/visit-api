<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use DateInterval;
use DateTime;
use DateTimeZone;

/**
 * WSLQUAsis indicator.
 *
 * | # | SYS_G6.1 Y           | SYS_G7.1 Y           | SYS_G6.1 = “pasa" Y  | SYS_G7.1 = “pasa” Y  | WSLQUAsisi |
 * | # | Y SYS_G6.2 ≤ 12      | YSYS_G7.2 ≤ 12       | Y SYS_G6.2 ≤ 12      | Y SYS_G7.2 ≤ 12      | WSLQUAsisi |
 * | # | meses desde SYS_00.1 | meses desde SYS_00.1 | meses desde SYS_00.1 | meses desde SYS_00.1 | WSLQUAsisi |
 * | 1 | ≥ 2                  | ≥ 2                  | ≥ 2                  | ≥ 1                  | 1,00       |
 * | 2 | ≥ 2                  | ≥ 2                  | 1                    | 1                    | 0,70       |
 * | 3 | ≥ 2                  | 1                    | ≥ 1                  | 1                    | 0,70       |
 * | 4 | 1                    | 1                    | 1                    | 1                    | 0,70       |
 * | 5 | ≥ 1                  | ≥ 1                  | ≥ 1                  | 0                    | 0,40       |
 * | 6 | ≥ 1                  | ≥ 1                  | 0                    | -                    | 0,40       |
 * | 7 | ≥ 1                  | 0                    | ≥ 1                  | ≥ 0                  | 0,00       |
 * | 8 | 0                    | ≥ 0                  | -                    | ≥ 0                  | 0,00       |
 *
 * SYS_G6.1 Y SYS_G6.2 ≤ 12 meses desde SYS_00.1 => Número de análisis bacteriológicos en los últimos 12 meses
 * SYS_G7.1 SYS_G7.2 ≤ 12 meses desde SYS_00.1 => Número de análisis fisicoquímicos en los últimos 12 meses
 * SYS_G6.1 = “pasa" Y SYS_G6.2 ≤ 12 meses desde SYS_00.1 => Número de análisis bacteriológicos en los últimos 12 meses que pasan el test
 * SYS_G7.1 = “pasa” Y SYS_G7.2 ≤ 12 meses desde SYS_00.1 => Número de análisis fisicoquímicos en los últimos 12 meses que pasan el test
 * WSLQUAsisi => Indicador WSLQUA en el sistema
 */
class WSLQUAsisIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('WSLQUAsis', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        // field_quality_results_bacteriological   7.6       form.wssystem
        /** @var FormRecord[] $g6 */
        $bacteriologicals = $this->getRecord()->{'field_quality_results_bacteriological'};
        // field_quality_results_physiochemical   7.7       form.wssystem
        /** @var FormRecord[] $g7 */
        $physiochemicals = $this->getRecord()->{'field_quality_results_physiochemical'};
        /** @var \DateTime $f0d1 */
        $f0d1 = $this->getRecord()->{'field_questionnaire_date'};
        $f0d1 = $f0d1->setTimezone(new DateTimeZone('UTC'));

        // Count valid bacteriological tests
        $bacteriologicalCnt = 0;
        $bacteriologicalOkCnt = 0;
        foreach ($bacteriologicals as $bacteriological) {
            // type: select
            //  1: 'Meets standards'
            //  2: 'Does not meet standards'
            $g6d1 = $bacteriological->{'field_result'};
            // type: month_year
            $g6d2 = $bacteriological->{'field_test_date'};
            $testDate = DateTime::createFromFormat(
                'Y-m-d',
                $g6d2['year'].'-'.$g6d2['month'].'-1',
                new DateTimeZone('UTC')
            );
            /** @var DateInterval $age */
            $age = date_diff($testDate, $f0d1);
            $monthsDiff = ($age->y * 12) + $age->m;
            if ($monthsDiff <= 12) {
                $bacteriologicalCnt += 1;
                if ('1' === $g6d1) {
                    $bacteriologicalOkCnt += 1;
                }
            }
        }

        // Count valid physiochemical tests
        $physiochemicalCnt = 0;
        $physiochemicalOkCnt = 0;
        foreach ($physiochemicals as $physiochemical) {
            // type: select
            //  1: 'Meets standards'
            //  2: 'Does not meet standards'
            $g7d1 = $physiochemical->{'field_result'};
            // type: month_year
            $g7d2 = $physiochemical->{'field_test_date'};
            $testDate = DateTime::createFromFormat(
                'Y-m-d',
                $g7d2['year'].'-'.$g7d2['month'].'-1',
                new DateTimeZone('UTC')
            );
            /** @var DateInterval $age */
            $age = date_diff($testDate, $f0d1);
            $monthsDiff = ($age->y * 12) + $age->m;
            if ($monthsDiff <= 12) {
                $physiochemicalCnt += 1;
                if ('1' === $g7d1) {
                    $physiochemicalOkCnt += 1;
                }
            }
        }

        // Row 1
        if ($bacteriologicalCnt >= 2 && $physiochemicalCnt >= 2 && $bacteriologicalOkCnt >= 2 && $physiochemicalOkCnt >= 1) {
            return 1.0;
        }
        // Row 2
        if ($bacteriologicalCnt >= 2 && $physiochemicalCnt >= 2 && 1 === $bacteriologicalOkCnt && 1 === $physiochemicalOkCnt) {
            return 0.7;
        }
        // Row 3
        if ($bacteriologicalCnt >= 2 && 1 === $physiochemicalCnt && $bacteriologicalOkCnt >= 1 && 1 === $physiochemicalOkCnt) {
            return 0.7;
        }
        // Row 4
        if (1 === $bacteriologicalCnt && 1 === $physiochemicalCnt && 1 === $bacteriologicalOkCnt && 1 === $physiochemicalOkCnt) {
            return 0.7;
        }
        // Row 5
        if ($bacteriologicalCnt >= 1 && $physiochemicalCnt >= 1 && $bacteriologicalOkCnt >= 1 && 0 === $physiochemicalOkCnt) {
            return 0.4;
        }
        // Row 6
        if ($bacteriologicalCnt >= 1 && $physiochemicalCnt >= 1 && 0 === $bacteriologicalOkCnt) {
            return 0.4;
        }

        // Row 7 & 8
        return 0;
    }
}
