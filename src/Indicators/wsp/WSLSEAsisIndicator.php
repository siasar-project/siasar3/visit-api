<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Indicators\AbstractIndicator;
use App\Indicators\SimpleIndicatorContext;
use Exception;

/**
 * WSLSEAsis indicator.
 *
 * | # | SYS_A9.1 | SYS_A9.2 | WSLSEAsisi |
 * | 1 | 1. Sí    | 1. Sí    | 1,00       |
 * | 2 | 1. Sí    | 2. No    | 0,70       |
 * | 3 | 2. No    | 1. Sí    | 0,70       |
 * | 4 | 2. No    | 2. No    | 0,00       |
 */
class WSLSEAsisIndicator extends AbstractIndicator
{
    /**
     * @inheritDoc
     */
    public function __construct(SimpleIndicatorContext $context)
    {
        parent::__construct('WSLSEAsis', $context);
    }

    /**
     * @inheritDoc
     */
    public function getContextFormId(): string
    {
        return 'form.wssystem';
    }

    /**
     * @inheritDoc
     */
    protected function resolve()
    {
        /** @var Bool $a9d1 */
        $a9d1 = $this->getRecord()->{'field_dry_season'};
        /** @var Bool $a9d1 */
        $a9d2 = $this->getRecord()->{'field_rainy_season'};

        if ($a9d1 && $a9d2) {
            return 1.0;
        }
        if ($a9d1 && !$a9d2) {
            return 0.7;
        }
        if (!$a9d1 && $a9d2) {
            return 0.7;
        }

        // !$a9d1 && !$a9d2
        return 0;
    }
}
