<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Indicators\wsp;

use App\Forms\FormRecord;

/**
 * Utility class to WSLACCIndicator.
 */
class WaterSystemNote
{
    // 'form.wssystem' => $waterSystem,
    public FormRecord $system;

    // 'form.wssystem.communities' => $servedCommunity,
    public FormRecord $servedCommunity;

    /**
     * HHaccij indicator
     *
     * @var float
     */
    public $hHaccij;

    /**
     * WSLCONsis indicator.
     *
     * @var float
     */
    public $wSLCONsis;

    /**
     * WSLSEAsis indicator.
     *
     * @var float
     */
    public $wSLSEAsis;

    /**
     * WSLQUAsis indicator.
     *
     * @var float
     */
    public $wSLQUAsis;

    /**
     * @param FormRecord $system
     * @param FormRecord $community
     * @param float      $hHaccij
     * @param float      $wSLCONsis
     * @param float      $wSLSEAsis
     * @param float      $wSLQUAsis
     */
    public function __construct(FormRecord $system, FormRecord $community, float $hHaccij, float $wSLCONsis, float $wSLSEAsis, float $wSLQUAsis)
    {
        $this->system = $system;
        $this->servedCommunity = $community;
        $this->hHaccij = $hHaccij;
        $this->wSLCONsis = $wSLCONsis;
        $this->wSLSEAsis = $wSLSEAsis;
        $this->wSLQUAsis = $wSLQUAsis;
    }
}
