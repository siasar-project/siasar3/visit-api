<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App;

use App\Tools\PublicServicesGroupPass;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Twistor\FlysystemStreamWrapper;

/**
 * Application kernel.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class Kernel extends BaseKernel implements CompilerPassInterface
{
    use MicroKernelTrait;

    /**
     * Kernel constructor.
     *
     * @param string $environment Environment ID.
     * @param bool   $debug       Debug mode.
     */
    public function __construct(string $environment, bool $debug)
    {
        date_default_timezone_set('UTC');
        parent::__construct($environment, $debug);

        // Set stream wrappers.
        // '/files'
        $filesystem = new Filesystem(new Local($this->getPublicDir()));
        FlysystemStreamWrapper::register('files', $filesystem);
        // '/assets'
        $filesystem = new Filesystem(new Local($this->getProjectDir().'/assets'));
        FlysystemStreamWrapper::register('assets', $filesystem);
    }

    /**
     * Get public files' path.
     *
     * @return string
     */
    public function getPublicDir(): string
    {
        $dir = $this->getProjectDir();
        $resp = $dir.'/'.(isset($_ENV['APP_CONFIG_PUBLIC_FILES']) ? $_ENV['APP_CONFIG_PUBLIC_FILES'] : 'files');

        if (!is_dir($resp)) {
            @mkdir($resp, 0777, true);
        }

        return $resp;
    }

    /**
     * Fix private services that can't be injected.
     *
     * @param ContainerBuilder $container Container builder.
     *
     * @return void
     *
     * @see https://github.com/yokai-php/sonata-workflow/issues/1
     */
    public function process(ContainerBuilder $container): void
    {
        $container->getDefinition('workflow.registry')->setPublic(true);
        $container->getDefinition('security.user_password_hasher')->setPublic(true);
        $container->getDefinition('security.untracked_token_storage')->setPublic(true);
        $container->getDefinition('monolog.logger')->setPublic(true);
        $container->getDefinition('api_platform.iri_converter')->setPublic(true);
        $container->getDefinition('gravatar.api')->setPublic(true);
        $container->getDefinition('parameter_bag')->setPublic(true);
        $container->getDefinition('annotations.reader')->setPublic(true);
    }

    /**
     * Init compiler extra pass.
     *
     * @param ContainerBuilder $container
     *
     * @return void
     */
    protected function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new PublicServicesGroupPass());
    }

    /**
     * Init container.
     *
     * @param ContainerConfigurator $container Container.
     *
     * @return void
     */
    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import('../config/{packages}/*.yaml');
        $container->import('../config/{packages}/'.$this->environment.'/*.yaml');

        if (is_file(\dirname(__DIR__).'/config/services.yaml')) {
            $container->import('../config/services.yaml');
            $container->import('../config/{services}_'.$this->environment.'.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/services.php')) {
            // phpcs:ignore
            (require $path)($container->withPath($path), $this);
        }
    }

    /**
     * Init router.
     *
     * @param RoutingConfigurator $routes Router settings.
     *
     * @return void
     */
    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../config/{routes}/'.$this->environment.'/*.yaml');
        $routes->import('../config/{routes}/*.yaml');

        if (is_file(\dirname(__DIR__).'/config/routes.yaml')) {
            $routes->import('../config/routes.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/routes.php')) {
            // phpcs:ignore
            (require $path)($routes->withPath($path), $this);
        }
    }
}
