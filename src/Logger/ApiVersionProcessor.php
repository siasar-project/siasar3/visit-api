<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Logger;

use App\Service\SessionService;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Add API version and current environment settings to logs.
 */
class ApiVersionProcessor
{
    protected ContainerBagInterface $bag;
    protected TokenStorageInterface $tokenStorage;

    /**
     * @param ContainerBagInterface $bag
     * @param TokenStorageInterface $storage
     */
    public function __construct(ContainerBagInterface $bag, TokenStorageInterface $storage)
    {
        $this->bag = $bag;
        $this->tokenStorage = $storage;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function __invoke(array $record)
    {
        $record['extra']['api_version'] = $this->bag->get('api_platform.version');
        $record['extra']['environment'] = isset($_ENV['APP_ENV']) ? $_ENV['APP_ENV'] : '???';
        $record['extra']['host_id'] = $_ENV['ROCKETCHAT_THIS_INSTANCE_ID'];

        if ('@hostname' === $record['extra']['host_id']) {
            $record['extra']['host_id'] = isset($_ENV["HTTP_HOST"]) ? $_ENV["HTTP_HOST"] : gethostname();
        }

        $user = $this->getCurrentUser();
        $record['extra']['username'] = 'ANONYMOUS';
        $record['extra']['user_id'] = '';
        if ($user) {
            $record['extra']['username'] = $user->getUsername();
            $record['extra']['user_id'] = $user->getId();
        }

        return $record;
    }

    /**
     * Get current user by Token Storage.
     *
     * @return UserInterface|null
     */
    protected function getCurrentUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface) {
            return $token->getUser();
        }

        $user = SessionService::getUser();
        if ($user) {
            return $user;
        }

        return null;
    }
}
