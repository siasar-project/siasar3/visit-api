<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

declare(strict_types=1);

namespace App\OpenApi\GraphQl\Action;

use ApiPlatform\Core\GraphQl\Action\GraphiQlAction;
use ApiPlatform\Core\GraphQl\Action\GraphQlPlaygroundAction;
use ApiPlatform\Core\GraphQl\Error\ErrorHandlerInterface;
use ApiPlatform\Core\GraphQl\ExecutorInterface;
use ApiPlatform\Core\GraphQl\Type\SchemaBuilderInterface;
use App\Forms\FormFactory;
use GraphQL\Error\Debug;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\Error;
use GraphQL\Executor\ExecutionResult;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Decorate the GraphQL API entrypoint.
 *
 * @experimental
 *
 * @category Tools
 *
 * @author   Alan Poulain <contact@alanpoulain.eu>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class EntrypointAction
{
    protected ?SchemaBuilderInterface $schemaBuilder;
    protected ?ExecutorInterface $executor;
    protected ?GraphiQlAction $graphiQlAction;
    protected ?GraphQlPlaygroundAction $graphQlPlaygroundAction;
    protected ?NormalizerInterface $normalizer;
    protected ?ErrorHandlerInterface $errorHandler;
    protected ?FormFactory $formFactory;
    protected string|int|false $debug;
    protected bool $graphiqlEnabled;
    protected bool $graphQlPlaygroundEnabled;
    protected mixed $defaultIde;

    /**
     * Entry point Action constructor.
     *
     * @param SchemaBuilderInterface  $schemaBuilder            Schema Builder.
     * @param ExecutorInterface       $executor                 Executor.
     * @param GraphiQlAction          $graphiQlAction           GraphiQl Action.
     * @param GraphQlPlaygroundAction $graphQlPlaygroundAction  GraphQl Playground Action.
     * @param NormalizerInterface     $normalizer               Normalizer.
     * @param ErrorHandlerInterface   $errorHandler             Error Handler.
     * @param FormFactory             $formFactory              Form Factory.
     * @param bool                    $debug                    Debug.
     * @param bool                    $graphiqlEnabled          Graphiql Enabled.
     * @param bool                    $graphQlPlaygroundEnabled GraphQl Playground Enabled.
     * @param false                   $defaultIde               Default Ide.
     */
    public function __construct(SchemaBuilderInterface $schemaBuilder, ExecutorInterface $executor, GraphiQlAction $graphiQlAction, GraphQlPlaygroundAction $graphQlPlaygroundAction, NormalizerInterface $normalizer, ErrorHandlerInterface $errorHandler, FormFactory $formFactory, bool $debug = false, bool $graphiqlEnabled = false, bool $graphQlPlaygroundEnabled = false, $defaultIde = false)
    {
        $this->schemaBuilder = $schemaBuilder;
        $this->executor = $executor;
        $this->graphiQlAction = $graphiQlAction;
        $this->graphQlPlaygroundAction = $graphQlPlaygroundAction;
        $this->normalizer = $normalizer;
        $this->errorHandler = $errorHandler;
        if (class_exists(Debug::class)) {
            $this->debug = $debug ? Debug::INCLUDE_DEBUG_MESSAGE | Debug::INCLUDE_TRACE : false;
        } else {
            $this->debug = $debug ? DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE : DebugFlag::NONE;
        }
        $this->graphiqlEnabled = $graphiqlEnabled;
        $this->graphQlPlaygroundEnabled = $graphQlPlaygroundEnabled;
        $this->defaultIde = $defaultIde;
        $this->formFactory = $formFactory;
    }

    /**
     * Service invocation.
     *
     * @param Request $request The request.
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        try {
            if ($request->isMethod('GET') && 'html' === $request->getRequestFormat()) {
                if ('graphiql' === $this->defaultIde && $this->graphiqlEnabled) {
                    return ($this->graphiQlAction)($request);
                }

                if ('graphql-playground' === $this->defaultIde && $this->graphQlPlaygroundEnabled) {
                    return ($this->graphQlPlaygroundAction)($request);
                }
            }

            [$query, $operationName, $variables] = $this->parseRequest($request);
            if (null === $query) {
                throw new BadRequestHttpException('GraphQL query is not valid.');
            }

            $executionResult = $this->executor
                ->executeQuery($this->schemaBuilder->getSchema(), $query, null, null, $variables, $operationName)
                ->setErrorsHandler($this->errorHandler)
                ->setErrorFormatter([$this->normalizer, 'normalize']);
        } catch (\Exception $exception) {
            $executionResult = (new ExecutionResult(null, [new Error($exception->getMessage(), null, null, [], null, $exception)]))
                ->setErrorsHandler($this->errorHandler)
                ->setErrorFormatter([$this->normalizer, 'normalize']);
        }

        return new JsonResponse($executionResult->toArray($this->debug));
    }

    /**
     * Parse request.
     *
     * @param Request $request The request.
     *
     * @return array
     */
    protected function parseRequest(Request $request): array
    {
        $queryParameters = $request->query->all();
        $query = $queryParameters['query'] ?? null;
        $operationName = $queryParameters['operationName'] ?? null;
        if ($variables = $queryParameters['variables'] ?? []) {
            $variables = $this->decodeVariables($variables);
        }

        if (!$request->isMethod('POST')) {
            return [$query, $operationName, $variables];
        }

        if ('json' === $request->getContentType()) {
            return $this->parseData($query, $operationName, $variables, $request->getContent());
        }

        if ('graphql' === $request->getContentType()) {
            $query = $request->getContent();
        }

        if ('multipart' === $request->getContentType()) {
            return $this->parseMultipartRequest($query, $operationName, $variables, $request->request->all(), $request->files->all());
        }

        return [$query, $operationName, $variables];
    }

    /**
     * Parse data.
     *
     * @param string|null $query         Query.
     * @param string|null $operationName Operation Name.
     * @param array       $variables     Variables.
     * @param string      $jsonContent   Json Content.
     *
     * @return array
     */
    protected function parseData(?string $query, ?string $operationName, array $variables, string $jsonContent): array
    {
        if (!\is_array($data = json_decode($jsonContent, true))) {
            throw new BadRequestHttpException('GraphQL data is not valid JSON.');
        }

        if (isset($data['query'])) {
            $query = $data['query'];
        }

        if (isset($data['variables'])) {
            $variables = \is_array($data['variables']) ? $data['variables'] : $this->decodeVariables($data['variables']);
        }

        if (isset($data['operationName'])) {
            $operationName = $data['operationName'];
        }

        return [$query, $operationName, $variables];
    }

    /**
     * Parse Multipart Request.
     *
     * @param string|null $query          Query.
     * @param string|null $operationName  Operation Name.
     * @param array       $variables      Variables.
     * @param array       $bodyParameters Body Parameters.
     * @param array       $files          Files.
     *
     * @return array
     */
    protected function parseMultipartRequest(?string $query, ?string $operationName, array $variables, array $bodyParameters, array $files): array
    {
        if ((null === $operations = $bodyParameters['operations'] ?? null) || (null === $map = $bodyParameters['map'] ?? null)) {
            throw new BadRequestHttpException('GraphQL multipart request does not respect the specification.');
        }

        [$query, $operationName, $variables] = $this->parseData($query, $operationName, $variables, $operations);

        /** @var string $map */
        if (!\is_array($decodedMap = json_decode($map, true))) {
            throw new BadRequestHttpException('GraphQL multipart request map is not valid JSON.');
        }

        $variables = $this->applyMapToVariables($decodedMap, $variables, $files);

        return [$query, $operationName, $variables];
    }

    /**
     * Apply map to variables.
     *
     * @param array $map       Map.
     * @param array $variables Variables.
     * @param array $files     Files.
     *
     * @return array
     */
    protected function applyMapToVariables(array $map, array $variables, array $files): array
    {
        foreach ($map as $key => $value) {
            if (null === $file = $files[$key] ?? null) {
                throw new BadRequestHttpException('GraphQL multipart request file has not been sent correctly.');
            }

            foreach ($map[$key] as $mapValue) {
                $path = explode('.', $mapValue);

                if ('variables' !== $path[0]) {
                    throw new BadRequestHttpException('GraphQL multipart request path in map is invalid.');
                }

                unset($path[0]);

                $mapPathExistsInVariables = array_reduce($path, static function (array $inVariables, string $pathElement) {
                    return \array_key_exists($pathElement, $inVariables) ? $inVariables[$pathElement] : false;
                }, $variables);

                if (false === $mapPathExistsInVariables) {
                    throw new BadRequestHttpException('GraphQL multipart request path in map does not match the variables.');
                }

                $variableFileValue = &$variables;
                foreach ($path as $pathValue) {
                    $variableFileValue = &$variableFileValue[$pathValue];
                }
                $variableFileValue = $file;
            }
        }

        return $variables;
    }

    /**
     * Decode variables.
     *
     * @param string $variables Variables.
     *
     * @return array
     */
    protected function decodeVariables(string $variables): array
    {
        if (!\is_array($variables = json_decode($variables, true))) {
            throw new BadRequestHttpException('GraphQL variables are not valid JSON.');
        }

        return $variables;
    }
}
