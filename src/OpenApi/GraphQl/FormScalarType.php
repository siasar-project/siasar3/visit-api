<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\OpenApi\GraphQl;

use App\Forms\FieldTypes\FieldTypeInterface;
use GraphQL\Language\AST\Node;
use GraphQL\Type\Definition\ScalarType;

/**
 * Builds the GraphQL schema.
 *
 * @experimental
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormScalarType extends ScalarType
{
    protected FieldTypeInterface $fieldType;

    /**
     * Form scalar type constructor.
     *
     * @param FieldTypeInterface $fieldType Source form field type.
     * @param mixed[]            $config    Scalar settings.
     */
    public function __construct(FieldTypeInterface $fieldType, array $config = [])
    {
        parent::__construct($config);
        $this->fieldType = $fieldType;
    }

    /**
     * @inheritDoc
     */
    public function serialize($value): mixed
    {
        // TODO: Implement serialize() method.
    }

    /**
     * @inheritDoc
     */
    public function parseValue($value): mixed
    {
        // TODO: Implement parseValue() method.
    }

    /**
     * @inheritDoc
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null): mixed
    {
        // TODO: Implement parseLiteral() method.
    }
}
