<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

declare(strict_types=1);

namespace App\OpenApi\GraphQl;

use ApiPlatform\Core\GraphQl\Type\FieldsBuilderInterface;
use ApiPlatform\Core\GraphQl\Type\SchemaBuilder;
use ApiPlatform\Core\GraphQl\Type\SchemaBuilderInterface;
use ApiPlatform\Core\GraphQl\Type\TypesContainerInterface;
use ApiPlatform\Core\GraphQl\Type\TypesFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface;
use ApiPlatform\Core\Util\Inflector;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\OpenApi\GraphQl\Stage\SecurityStage;
use App\Resolver\FormQueryResolver;
use App\Resolver\UserMutationResolver;
use App\Traits\StringTranslationTrait;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;

/**
 * Builds the GraphQL schema.
 *
 * @experimental
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @see \ApiPlatform\Core\GraphQl\Type\SchemaBuilder
 * @see https://relay.dev/graphql/connections.htm
 * @see GraphQL security => https://api-platform.com/docs/core/graphql/#workflow-of-the-resolvers
 */
final class GraphQlSchemaBuilder implements SchemaBuilderInterface
{
    use StringTranslationTrait;

    protected ?ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory;
    protected ?ResourceMetadataFactoryInterface $resourceMetadataFactory;
    protected ?TypesFactoryInterface $typesFactory;
    protected ?TypesContainerInterface $typesContainer;
    protected ?FieldsBuilderInterface $fieldsBuilder;
    protected ?FormFactory $formFactory;
    protected ?FieldTypeManager $fieldTypeManager;
    protected SchemaBuilderInterface $schemaBuilder;

    /**
     * GraphQl Schema Builder constructor.
     *
     * @param ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory Resource Name Collection Factory.
     * @param ResourceMetadataFactoryInterface       $resourceMetadataFactory       Resource Metadata Factory.
     * @param TypesFactoryInterface                  $typesFactory                  Types Factory.
     * @param TypesContainerInterface                $typesContainer                Types Container.
     * @param FieldsBuilderInterface                 $fieldsBuilder                 Fields Builder.
     * @param FormFactory                            $formFactory                   Form Factory.
     * @param FieldTypeManager                       $fieldTypeManager              Field Type Manager.
     */
    public function __construct(ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory, ResourceMetadataFactoryInterface $resourceMetadataFactory, TypesFactoryInterface $typesFactory, TypesContainerInterface $typesContainer, FieldsBuilderInterface $fieldsBuilder, FormFactory $formFactory, FieldTypeManager $fieldTypeManager)
    {
        $this->resourceNameCollectionFactory = $resourceNameCollectionFactory;
        $this->resourceMetadataFactory = $resourceMetadataFactory;
        $this->typesFactory = $typesFactory;
        $this->typesContainer = $typesContainer;
        $this->fieldsBuilder = $fieldsBuilder;
        $this->formFactory = $formFactory;
        $this->fieldTypeManager = $fieldTypeManager;
        // Instance old schema builder.
        $this->schemaBuilder = new SchemaBuilder(
            $resourceNameCollectionFactory,
            $resourceMetadataFactory,
            $typesFactory,
            $typesContainer,
            $fieldsBuilder
        );
    }

    /**
     * Get data schema.
     *
     * @return Schema
     *
     * @throws \ApiPlatform\Core\Exception\ResourceClassNotFoundException
     * @throws \ReflectionException
     */
    public function getSchema(): Schema
    {
        // Do a wrapper over the original class instead replicate it.
        $schema = $this->schemaBuilder->getSchema();
        $schemaConfig = $schema->getConfig();

        // Base types.
        $types = $this->typesFactory->getTypes();
        foreach ($types as $typeId => $type) {
            $this->typesContainer->set($typeId, $type);
        }

        // Add field types.
        $fTypes = $this->fieldTypeManager->getFieldTypes();
        foreach ($fTypes as $fTypeId => $fType) {
            $fieldType = $this->fieldTypeManager->create($fTypeId);
            if (count($fieldType->getProperties()) === 1) {
                $gqlEq = $this->getFormFieldTypeHowStandardScalarType($fType["annotation"]->getId());
                if (!$gqlEq && 'select' !== $fType["annotation"]->getId()) {
                    $this->typesContainer->set(
                        'forms_'.$fTypeId,
                        new FormScalarType($fieldType, [
                            'name' => 'forms_'.$fTypeId,
                            'description' => $fType["annotation"]->getLabel(),
                        ])
                    );
                }
            } else {
                $settings = [
                    'name' => 'forms_'.$fTypeId,
                    'description' => $fType["annotation"]->getLabel(),
                    'resolveField' => function ($value, $args, $context, ResolveInfo $info) {
                        return (new FormQueryResolver())->fieldTypeResolveField($value, $args, $context, $info);
                    },
                    'fields' => [],
                ];
                foreach ($fieldType->getProperties() as $pId => $pValue) {
                    $settings['fields'][$pId] = Type::string();
                }
                if ('date' === $fTypeId) {
                    // Add extra sub-fields.
                    $settings['fields']['utc_value'] = Type::string();
                    $settings['fields']['timestamp'] = Type::int();
                }
                // Read.
                $obj = new ObjectType($settings);
                $this->typesContainer->set('forms_'.$fTypeId, $obj);
            }
        }

        // Node interface.
        $queryFields = ['node' => $this->fieldsBuilder->getNodeQueryFields()];
        $mutationFields = [];
        $subscriptionFields = [];
        // Forms integration.
        $formsTypes = $this->formFactory->findAll();
        /** @var FormManager $formInstance */
        foreach ($formsTypes as $formInstance) {
            $queryFields += $this->addFormItemQuery($formInstance);
            $queryFields += $this->addFormCollectionQuery($formInstance);
            $mutationFields += $this->addFormDeleteMutation($formInstance);
            $mutationFields += $this->addFormUpdateMutation($formInstance);
//                    $subscriptionFields += $this->fieldsBuilder->getSubscriptionFields(
//                        $this->formatFormName($formInstance->getId()),
//                        $resourceMetadata,
//                        $operationName
//                    );
            $mutationFields += $this->addFormCreateMutation($formInstance);
        }

        $schemaConfig->query->config['fields'] += $queryFields;
        $schemaConfig->mutation->config['fields'] += $mutationFields;

//        if ($subscriptionFields) {
//            $schema['subscription'] = new ObjectType([
//                'name' => 'Subscription',
//                'fields' => $subscriptionFields,
//            ]);
//        }

        // Login mutation.
        $mutation = [
            'login' => [
                'type' => Type::string(),
                'description' => $this->t('Get login token.')->render(),
                'resolve' => static function ($rootValue, $args, $contextValue, $info): string {
                    if (is_null($contextValue)) {
                        $contextValue = SecurityStage::getDefaultContext($contextValue, $args, $info);
                    }
                    $contextValue = array_merge(
                        $contextValue,
                        [
                            'args' => $args,
                            'info' => $info,
                            'is_mutation' => true,
                            // This property is not defined, or used, by API platform.
                            'is_custom' => true,
                        ]
                    );

                    return (new UserMutationResolver())($rootValue, $contextValue);
                },
                'write' => false,
                'validate' => false,
                'args' => [
                    'username' => ['type' => Type::nonNull(Type::string()), 'description' => $this->t('Name of the user')->render()],
                    'password' => ['type' => Type::nonNull(Type::string()), 'description' => $this->t('Password of the user')->render()],
                ],
            ],
        ];
        $schemaConfig->mutation->config['fields'] += $mutation;

        return new Schema($schemaConfig);
    }

    /**
     * Format a form ID as data type name.
     *
     * @param string $id The form ID.
     *
     * @return string
     */
    public static function formatFormName(string $id): string
    {
        return str_replace('.', '__', $id);
    }

    /**
     * Add form item query.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return array|array[]
     *
     * @throws \ReflectionException
     */
    protected function addFormItemQuery(FormManagerInterface $form): array
    {
        $resourceMetadata = [
            $this->formatFormName($form->getId()) => [
                'type' => null,
                'description' => $form->getDescription(),
                'args' => ['id' => Type::id()],
                // Resolver item.
                'resolve' => function (mixed $object, $args) use ($form) {
                    return (new FormQueryResolver())->findResolve($object, $args, $form);
                },
                'deprecationReason' => null,
            ],
        ];
        $typeDefinitionObjectSettings = [
            'name' => $this->formatFormName($form->getId()),
            'description' => $form->getDescription(),
            'interfaces' => [$this->typesContainer->get('Node')],
            'fields' => [
                'id' => [
                    'type' => Type::nonNull(Type::id()),
                    'resolve' => function (FormRecord $record, $args) {
                        return (new FormQueryResolver())->recordIriIdResolve($record, $args);
                    },
                ],
                '_id' => [
                    'type' => Type::string(),
                    'resolve' => function (FormRecord $record, $args) {
                        return (new FormQueryResolver())->recordIdResolve($record, $args);
                    },
                ],
            ],
        ];
        /** @var FieldTypeInterface $formFieldDefinition */
        foreach ($form->getFieldDefinitions() as $formFieldDefinition) {
            $field = $form->getFieldDefinition($formFieldDefinition['id']);
            $glType = $this->getFormFieldTypeHowStandardScalarType(
                $formFieldDefinition["type"],
                $formFieldDefinition["settings"]["required"]
            );
            if (!$glType) {
                $typeName = 'forms_'.$formFieldDefinition["type"];
                if ('select' === $formFieldDefinition["type"]) {
                    // Each select field must have own data type.
                    // We'll use it how enum.
                    $typeName = 'forms_'.$formFieldDefinition["id"].'_enum';
                    $options = [];
                    foreach ($formFieldDefinition["settings"]["options"] as $value => $label) {
                        $key = preg_replace('/[^a-zA-Z0-9]/', '', (string) $label);
                        $options['v'.$value] = [
                            'value' => $value,
                            'description' => $label,
                        ];
                    }
                    $this->typesContainer->set(
                        $typeName,
                        new EnumType([
                            'name' => $typeName,
                            'description' => $formFieldDefinition["label"],
                            'values' => $options,
                        ])
                    );
                }
                $glType = $this->typesContainer->get($typeName);
            }
            $typeDefinitionObjectSettings['fields'] += [
                $formFieldDefinition["id"] => [
                    'type' => $formFieldDefinition["settings"]["multivalued"] ?
                        Type::listOf($glType) :
                        $glType,
                    'description' => (
                        isset($formFieldDefinition['label']) ?
                            $formFieldDefinition['label'] :
                            ''
                        ).(
                        isset($formFieldDefinition['description']) ?
                            ': '.$formFieldDefinition['description'] :
                            ''
                        ),
                    // Resolve field to field values.
                    'resolve' => function (FormRecord $record, $args) use ($field) {
                        return (new FormQueryResolver())->formFieldResolve($record, $args, $field);
                    },
                ],
            ];
            if ($field->isDeprecated()) {
                $typeDefinitionObjectSettings['fields'][$formFieldDefinition["id"]]['isDeprecated'] = true;
                $typeDefinitionObjectSettings['fields'][$formFieldDefinition["id"]]['deprecationReason'] = $field->getDeprecationReason();
            }
        }
        $resourceMetadata[$this->formatFormName($form->getId())]['type'] = new ObjectType($typeDefinitionObjectSettings);
        $this->typesContainer->set(
            $this->formatFormName($this->formatFormName($form->getId())),
            $resourceMetadata[$this->formatFormName($form->getId())]['type']
        );

        return $resourceMetadata;
    }

    /**
     * Get the form collection query.
     *
     * @param FormManagerInterface $formInstance The form.
     *
     * @return array[]
     */
    protected function addFormCollectionQuery(FormManagerInterface $formInstance): array
    {
        // Build collection query.
        // We need an Edge, PageInfo and Connection.
        $formName = $this->formatFormName($formInstance->getId());
        $formPluralName = Inflector::pluralize($formName);
        $edge = [
            'name' => $formName.'Edge',
            'description' => $this->t('Edge of @form.', ['@form' => $formName])->render(),
            'fields' => [
                'node' => $this->typesContainer->get($formName),
                'cursor' => ['type' => Type::nonNull(Type::string())],
                'int_cursor' => Type::nonNull(Type::int()),
            ],
        ];
        $oEdge = new ObjectType($edge);
        $this->typesContainer->set($formName.'Edge', $oEdge);
        $pageInfo = [
            'name' => $formName.'PageInfo',
            'description' => $this->t('Information about the current page.')->render(),
            'fields' => [
                'endCursor' => Type::string(),
                'startCursor' => Type::string(),
                'hasNextPage' => Type::nonNull(Type::boolean()),
                'hasPreviousPage' => Type::nonNull(Type::boolean()),
            ],
        ];
        $oPageInfo = Type::nonNull(new ObjectType($pageInfo));
        $this->typesContainer->set($formName.'PageInfo', $oPageInfo);
        $connection = [
            'name' => $formName.'Connection',
            'description' => $this->t('Connection for @form.', ['@form' => $formName])->render(),
            'fields' => [
                'edges' => Type::listOf($oEdge),
                'pageInfo' => $oPageInfo,
                'totalCount' => Type::nonNull(Type::int()),
            ],
        ];
        $oConnections = new ObjectType($connection);
        $this->typesContainer->set($formName.'Connection', $oConnections);
        $query = [
            'name' => $formPluralName,
            'type' => $this->typesContainer->get($formName.'Connection'),
            'args' => [
                'first' => [
                    'type' => Type::int(),
                    'description' => $this->t('Returns the first n elements from the list.')->render(),
                ],
                'last' => [
                    'type' => Type::int(),
                    'description' => $this->t('Returns the last n elements from the list.')->render(),
                ],
                'before' => [
                    'type' => Type::string(),
                    'description' => $this->t('Returns the elements in the list that come before the specified cursor.')->render(),
                ],
                'after' => [
                    'type' => Type::string(),
                    'description' => $this->t('Returns the elements in the list that come after the specified cursor.')->render(),
                ],
                // TODO Add order and filters arguments.
//                            'order' => Type::listOf(new ObjectType([
//                                'name' => $formName.'Filter_order',
//                                'fields' => [
//                                    'id' => Type::string(),
//                                ],
//                            ])),
            ],
            'resolve' => function ($rootValue, $args, $contextValue, ResolveInfo $info) use ($formInstance) {
                return (new FormQueryResolver())->collectionResolve($rootValue, $args, $contextValue, $info, $formInstance);
            },
            'fields' => [

            ],
        ];

        return [$formPluralName => $query];
    }

    /**
     * Get the form create mutation.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return array[]
     *
     * @throws \ReflectionException
     */
    protected function addFormCreateMutation(FormManagerInterface $form): array
    {
        $formName = $this->formatFormName($form->getId());
        $inputFields = $this->getInputFields($form);
        $this->typesContainer->set(sprintf('create%sInput', ucfirst($formName)), new InputObjectType([
            'name' => sprintf('create%sInput', ucfirst($formName)),
            'description' => $form->getDescription(),
            'fields' => $inputFields + [
                    'clientMutationId' => ['type' => Type::string()],
                ],
            'interfaces' => [$this->typesContainer->get('Node')],
        ]));
        $this->typesContainer->set(sprintf('create%sPayload', ucfirst($formName)), new ObjectType([
            'name' => sprintf('create%sPayload', ucfirst($formName)),
            'description' => $form->getDescription(),
            'fields' => [
                $formName => $this->typesContainer->get($formName),
                'clientMutationId' => Type::string(),
            ],
            'resolveField' => function (mixed $rootValue, ?array $args, ?array $contextValue, ResolveInfo $info) {
                return (new FormQueryResolver())->createPayloadResolveField($rootValue, $args, $contextValue, $info);
            },
            'config' => [
                'name' => sprintf('create%sPayload', ucfirst($formName)),
                'description' => $form->getDescription(),
            ],
        ]));

        return [
            'create'.ucfirst($formName) => [
                'name' => 'create'.ucfirst($formName),
                'type' => $this->typesContainer->get(sprintf('create%sPayload', ucfirst($formName))),
                'description' => $this->t('Create an @form', ['@form' => $formName])->render(),
                'args' => [
                    'input' => [
                        'type' => Type::nonNull($this->typesContainer->get(sprintf('create%sInput', ucfirst($formName)))),
                    ],
                ],
                'resolve' => function ($source, $args, $context, $info) use ($form) {
                    return (new FormQueryResolver())->createMutationResolve($source, $args, $context, $info, $form);
                },
            ],
        ];
    }

    /**
     * Get the form update mutation.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return array[]
     *
     * @throws \ReflectionException
     */
    protected function addFormUpdateMutation(FormManagerInterface $form): array
    {
        $formName = $this->formatFormName($form->getId());
        $inputFields = $this->getInputFields($form);
        // Add ID! to input fields.
        $inputFields += [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'resolve' => function (FormRecord $record, $args) {
                    return (new FormQueryResolver())->recordIriIdResolve($record, $args);
                },
            ],
        ];
        $this->typesContainer->set(sprintf('update%sInput', ucfirst($formName)), new InputObjectType([
            'name' => sprintf('update%sInput', ucfirst($formName)),
            'description' => $form->getDescription(),
            'fields' => $inputFields + [
                    'clientMutationId' => ['type' => Type::string()],
                ],
            'interfaces' => [$this->typesContainer->get('Node')],
        ]));
        $this->typesContainer->set(sprintf('update%sPayload', ucfirst($formName)), new ObjectType([
            'name' => sprintf('update%sPayload', ucfirst($formName)),
            'description' => $form->getDescription(),
            'fields' => [
                $formName => $this->typesContainer->get($formName),
                'clientMutationId' => Type::string(),
            ],
            'resolveField' => function (mixed $rootValue, ?array $args, ?array $contextValue, ResolveInfo $info) {
                return (new FormQueryResolver())->createPayloadResolveField($rootValue, $args, $contextValue, $info);
            },
            'config' => [
                'name' => sprintf('update%sPayload', ucfirst($formName)),
                'description' => $form->getDescription(),
            ],
        ]));

        return [
            'update'.ucfirst($formName) => [
                'name' => 'update'.ucfirst($formName),
                'type' => $this->typesContainer->get(sprintf('update%sPayload', ucfirst($formName))),
                'description' => $this->t('Update an @form record', ['@form' => $formName])->render(),
                'args' => [
                    'input' => [
                        'type' => Type::nonNull($this->typesContainer->get(sprintf('update%sInput', ucfirst($formName)))),
                    ],
                ],
                'resolve' => function ($source, $args, $context, $info) use ($form) {
                    return (new FormQueryResolver())->updateMutationResolver($source, $args, $context, $info, $form);
                },
            ],
        ];
    }

    /**
     * Get the form delete mutation.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return array[]
     *
     * @throws \ReflectionException
     */
    protected function addFormDeleteMutation(FormManagerInterface $form): array
    {
        $formName = $this->formatFormName($form->getId());
        $inputFields = [];
        // Add ID! to input fields.
        $inputFields += [
            'id' => [
                'type' => Type::nonNull(Type::id()),
                'resolve' => function (FormRecord $record, $args) {
                    return (new FormQueryResolver())->recordIriIdResolve($record, $args);
                },
            ],
        ];
        $this->typesContainer->set(sprintf('delete%sInput', ucfirst($formName)), new InputObjectType([
            'name' => sprintf('delete%sInput', ucfirst($formName)),
            'description' => $form->getDescription(),
            'fields' => $inputFields + [
                    'clientMutationId' => ['type' => Type::string()],
                ],
            'interfaces' => [$this->typesContainer->get('Node')],
        ]));
        $this->typesContainer->set(sprintf('delete%sPayload', ucfirst($formName)), new ObjectType([
            'name' => sprintf('delete%sPayload', ucfirst($formName)),
            'description' => $form->getDescription(),
            'fields' => [
                $formName => $this->typesContainer->get($formName),
                'clientMutationId' => Type::string(),
            ],
            'resolveField' => function (mixed $rootValue, ?array $args, ?array $contextValue, ResolveInfo $info) {
                return (new FormQueryResolver())->createPayloadResolveField($rootValue, $args, $contextValue, $info);
            },
            'config' => [
                'name' => sprintf('delete%sPayload', ucfirst($formName)),
                'description' => $form->getDescription(),
            ],
        ]));

        return [
            'delete'.ucfirst($formName) => [
                'name' => 'delete'.ucfirst($formName),
                'type' => $this->typesContainer->get(sprintf('delete%sPayload', ucfirst($formName))),
                'description' => $this->t('Delete an @form record', ['@form' => $formName])->render(),
                'args' => [
                    'input' => [
                        'type' => Type::nonNull($this->typesContainer->get(sprintf('delete%sInput', ucfirst($formName)))),
                    ],
                ],
                'resolve' => function ($source, $args, $context, $info) use ($form) {
                    return (new FormQueryResolver())->deleteMutationResolver($source, $args, $context, $info, $form);
                },
            ],
        ];
    }

    /**
     * Generate Input fields settings array to mutations.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function getInputFields(FormManagerInterface $form): array
    {
        $inputFields = [];
        /** @var FieldTypeInterface $field */
        foreach ($form->getFields() as $field) {
            // Is a single property field?
            if (count($field->getProperties()) === 1) {
                // Yes, is a select field? Selects use ENUMs.
                if ('select' !== $field->getFieldType()) {
                    // No, use a standard type.
                    $gqlEq = $this->getFormFieldTypeHowStandardScalarType($field->getFieldType());
                    // Have a standard type?
                    if (!$gqlEq) {
                        // No, use a scalar type created to form fields.
                        $inputFields[$field->getId()] = [
                            'name' => $field->getId(),
                            'type' => new FormScalarType($field, [
                                'type' => $this->typesContainer->get('forms_'.$field->getFieldType()),
                                'description' => $field->getLabel(),
                            ]),
                        ];
                    } else {
                        // Yes, use the standard scalar type.
                        $inputFields[$field->getId()] = [
                            'name' => $field->getId(),
                            'type' => $gqlEq,
                        ];
                    }
                } else {
                    // Yes, is a select field. Use the ENUM type.
                    $inputFields[$field->getId()] = [
                        'name' => $field->getId(),
                        'type' => $this->typesContainer->get('forms_'.$field->getId().'_enum'),
                    ];
                }
            } else {
                // No, use a subtype how input.
                $inputFieldTypeName = sprintf('Input%s', ucfirst($field->getFieldType()));
                if (!$this->typesContainer->has($inputFieldTypeName)) {
                    $settings = [
                        'name' => sprintf('Input%s', ucfirst($field->getFieldType())),
                        'description' => $field->getLabel(),
                        'fields' => [],
                    ];
                    foreach ($field->getProperties() as $pId => $pValue) {
                        $settings['fields'][$pId] = Type::string();
                    }
                    $this->typesContainer->set($settings['name'], new InputObjectType($settings));
                }
                $gqlType = $this->typesContainer->get($inputFieldTypeName);
                $inputFields[$field->getId()] = ['type' => $gqlType];
            }
        }

        return $inputFields;
    }

    /**
     * Get Graphql field type equivalent.
     *
     * @param string $id         Field type Id.
     * @param bool   $isRequired Is a required field?
     *
     * @return ScalarType|NonNull|null null if haven't equivalent.
     */
    protected function getFormFieldTypeHowStandardScalarType(string $id, bool $isRequired = false): ScalarType|NonNull|null
    {
        // TODO Must each form field type know the equivalent GraphQl field type?
        $field = $this->fieldTypeManager->create($id);
        if (count($field->getProperties()) > 1) {
            return null;
        }

        $resp = null;
        switch ($id) {
            case 'boolean':
            case 'radio_boolean':
                $resp = Type::boolean();
                break;
            case 'decimal':
                $resp = Type::float();
                break;
            case 'integer':
                $resp = Type::int();
                break;
            case 'select':
                // Ignore this type. We'll declare a type for each field.
                break;
            case 'long_text':
            case 'short_text':
            case 'phone':
            case 'mail':
            case 'publishing_status':
            default:
                $resp = Type::string();
                break;
        }
        if ($resp) {
            if ($isRequired) {
                return Type::nonNull($resp);
            }

            return $resp;
        }

        return null;
    }

    /**
     * Recursive ArrayObjecto to array transformer.
     *
     * @param \ArrayObject $object Subject.
     *
     * @return array
     */
    protected function arrayObjectToArray(\ArrayObject $object): array
    {
        $resp = (array) $object;
        foreach ($resp as $key => $value) {
            if ($value instanceof \ArrayObject) {
                $resp[$key] = $this->arrayObjectToArray($value);
            } elseif (is_array($value)) {
                foreach ($value as $vKey => $vValue) {
                    if ($vValue instanceof \ArrayObject) {
                        $resp[$key][$vKey] = $this->arrayObjectToArray($vValue);
                    } else {
                        $resp[$key][$vKey] = $vValue;
                    }
                }
            } else {
                $resp[$key] = $value;
            }
        }

        return $resp;
    }
}
