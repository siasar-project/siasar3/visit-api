<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\OpenApi\GraphQl\Stage;

use ApiPlatform\Core\GraphQl\Resolver\Stage\ReadStageInterface;

/**
 * Decorate read stage to add security.
 *
 * @experimental
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ReadStage implements ReadStageInterface
{
    protected ReadStageInterface $readStage;

    /**
     * ReadStage constructor.
     *
     * @param ReadStageInterface $readStage Decorated read stage.
     */
    public function __construct(ReadStageInterface $readStage)
    {
        $this->readStage = $readStage;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(?string $resourceClass, ?string $rootClass, string $operationName, array $context): object|iterable|null
    {
        // You can add pre-read code here.
        // TODO Add security logic.

        // Call the decorated read stage (this syntax calls the __invoke method).
        $item = ($this->readStage)($resourceClass, $rootClass, $operationName, $context);

        // You can add post-read code here.

        return $item;
    }
}
