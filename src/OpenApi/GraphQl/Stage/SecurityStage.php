<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\OpenApi\GraphQl\Stage;

use ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use App\Service\SessionService;
use App\Traits\StringTranslationTrait;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Decorate security stage to add security.
 *
 * @experimental
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class SecurityStage implements SecurityStageInterface
{

    use StringTranslationTrait;

    protected SecurityStageInterface $securityStage;
    protected SessionService $sessionService;
    protected ResourceMetadataFactoryInterface $resourceMetadataFactory;

    /**
     * SecurityStage constructor.
     *
     * @param ResourceMetadataFactoryInterface $resourceMetadataFactory Resource metadata.
     * @param SecurityStageInterface           $securityStage           Decorated service.
     * @param SessionService                   $sessionService          Session service.
     */
    public function __construct(ResourceMetadataFactoryInterface $resourceMetadataFactory, SecurityStageInterface $securityStage, SessionService $sessionService)
    {
        $this->securityStage = $securityStage;
        $this->sessionService = $sessionService;
        $this->resourceMetadataFactory = $resourceMetadataFactory;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(string $resourceClass, string $operationName, array $context): void
    {
        $context = array_merge(
            [
                'is_collection' => false,
                'is_mutation' => false,
                'is_subscription' => false,
                // This property is not defined, or used, by API platform.
                'is_custom' => false,
            ],
            $context
        );
        // You can add pre-security code here.
        // TODO Add security logic.
        if ($context["is_collection"]) {
            $this->requireAdminSession($resourceClass, $operationName, $context);
        } elseif ($context["is_mutation"]) {
            if ('login' !== $operationName) {
                $this->requireAdminSession($resourceClass, $operationName, $context);
            }
        } elseif ($context["is_subscription"]) {
            $this->requireAdminSession($resourceClass, $operationName, $context);
        } else {
            $this->requireAdminSession($resourceClass, $operationName, $context);
        }

        // Call the decorated security stage (this syntax calls the __invoke method).
        if (!$context["is_custom"]) {
            ($this->securityStage)($resourceClass, $operationName, $context);
        }

        // You can add post-security code here.
    }

    /**
     * Get default context array.
     *
     * @param array|null       $context Original context.
     * @param array            $args    Operation arguments.
     * @param ResolveInfo|null $info    Resolve information.
     *
     * @return array
     */
    public static function getDefaultContext(?array $context, array $args, ?ResolveInfo $info): array
    {
        if (is_null($context)) {
            $context = [];
        }
        $context += [
            'args' => $args,
            'info' => $info,
            'is_collection' => false,
            'is_mutation' => false,
            'is_subscription' => false,
            // This property is not defined, or used, by API platform.
            'is_custom' => false,
        ];

        return $context;
    }

    /**
     * Control user access.
     *
     * @param string $resourceClass Resource class.
     * @param string $operationName Operation name.
     * @param array  $context       Context.
     *
     * @throws \ApiPlatform\Core\Exception\ResourceClassNotFoundException
     */
    protected function requireAdminSession(string $resourceClass, string $operationName, array $context)
    {
        $message = $this->t('Access Denied.');
        if (!empty($resourceClass)) {
            $resourceMetadata = $this->resourceMetadataFactory->create($resourceClass);
            $message = $resourceMetadata->getGraphqlAttribute($operationName, 'security_message', $message);
        }
        $user = $this->sessionService->getUser();
        if (!$user || !in_array('ROLE_ADMIN', $user->getRoles())) {
            throw new AccessDeniedHttpException($message);
        }
    }
}
