<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\OpenApi\GraphQl\Stage;

use ApiPlatform\Core\GraphQl\Resolver\Stage\WriteStageInterface;

/**
 * Decorate write stage to add security.
 *
 * @experimental
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class WriteStage implements WriteStageInterface
{
    protected WriteStageInterface $writeStage;

    /**
     * WriteStage constructor.
     *
     * @param WriteStageInterface $writeStage Decorated write stage.
     */
    public function __construct(WriteStageInterface $writeStage)
    {
        $this->writeStage = $writeStage;
    }

    /**
     * @inheritDoc
     */
    public function __invoke($data, string $resourceClass, string $operationName, array $context): ?object
    {
        // You can add pre-write code here.
        // TODO Add security logic.

        // Call the decorated write stage (this syntax calls the __invoke method).
        $writtenObject = ($this->writeStage)($data, $resourceClass, $operationName, $context);

        // You can add post-write code here.

        return $writtenObject;
    }
}
