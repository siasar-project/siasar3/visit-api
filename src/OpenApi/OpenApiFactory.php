<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\OpenApi;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\DataProvider\PaginationOptions;
use ApiPlatform\Core\OpenApi\Model\ExternalDocumentation;
use ApiPlatform\Core\OpenApi\Model\MediaType;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\Parameter;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\Model\Response;
use ApiPlatform\Core\OpenApi\Model\Server;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model;
use App\DataProvider\Extension\InquiryFormLogCollectionExtensionInterface;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Tools\TemplateGenerator;
use App\Traits\GetContainerTrait;
use App\Traits\OpenApiSchemaTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Decorate the OpenApiFactory.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class OpenApiFactory implements OpenApiFactoryInterface
{
    use GetContainerTrait;
    use StringTranslationTrait;
    use OpenApiSchemaTrait;

    protected OpenApiFactoryInterface $decorated;
    protected array $annotations;
    protected ?FormFactory $formFactory;
    protected ?FieldTypeManager $fieldTypeManager;
    protected ParameterBagInterface $systemSettings;
    protected TemplateGenerator $templates;

    /**
     * OpenApiFactory constructor.
     *
     * @param OpenApiFactoryInterface $decorated        Decorated instance.
     * @param TemplateGenerator       $templates
     * @param FormFactory             $formFactory      Form factory.
     * @param FieldTypeManager        $fieldTypeManager Form field type manager.
     */
    public function __construct(OpenApiFactoryInterface $decorated, TemplateGenerator $templates, FormFactory $formFactory, FieldTypeManager $fieldTypeManager)
    {
        $this->decorated = $decorated;
        $this->annotations = [];
        $this->formFactory = $formFactory;
        $this->fieldTypeManager = $fieldTypeManager;
        $this->systemSettings = $this->getContainerInstance()->getParameterBag();
        $this->templates = $templates;
    }

    /**
     * Decorate an OpenApi class.
     *
     * @param array $context Context.
     *
     * @return OpenApi
     *
     * @throws \ReflectionException
     */
    public function __invoke(array $context = []): OpenApi
    {
        // Get default factory response.
        $openApi = $this->decorated->__invoke($context);
        /**
         * A path item.
         *
         * @var PathItem $pathItem
         */
        foreach ($openApi->getPaths()->getPaths() as $path => &$pathItem) {
            $post = $this->updateOperation($pathItem->getPost());
            if ($post) {
                $openApi->getPaths()->addPath($path, $pathItem->withPost($post));
            }

            $get = $this->updateOperation($pathItem->getGet());
            if ($get) {
                $openApi->getPaths()->addPath($path, $pathItem->withGet($get));
            }

            $delete = $this->updateOperation($pathItem->getDelete());
            if ($delete) {
                $openApi->getPaths()->addPath($path, $pathItem->withDelete($delete));
            }

            $head = $this->updateOperation($pathItem->getHead());
            if ($head) {
                $openApi->getPaths()->addPath($path, $pathItem->withHead($head));
            }

            $patch = $this->updateOperation($pathItem->getPatch());
            if ($patch) {
                $openApi->getPaths()->addPath($path, $pathItem->withPatch($patch));
            }

            $put = $this->updateOperation($pathItem->getPut());
            if ($put) {
                $openApi->getPaths()->addPath($path, $pathItem->withPut($put));
            }
        }
        // API Platform path customizations.
        // Remove plural filters in point_logs collections.
        $paths = $openApi->getPaths();
        $pointLogGetCollectionPath = $paths->getPath('/api/v1/point_logs');
        $getOperation = $pointLogGetCollectionPath->getGet();
        $params = $getOperation->getParameters();
        $newParams = [];
        /** @var Parameter $param */
        foreach ($params as $param) {
            if ('point[]' !== $param->getName() && 'country[]' !== $param->getName()) {
                $newParams[] = $param;
            }
        }
        $getOperation = $getOperation->withParameters($newParams);
        $pointLogGetCollectionPath = $pointLogGetCollectionPath->withGet($getOperation);
        $paths->addPath('/api/v1/point_logs', $pointLogGetCollectionPath);

        // Forms.
        $forms = $this->formFactory->findAll();
        // Create schemas for each form.
        $this->createFieldTypesSchemas($openApi);
        $this->createFormErrorSchema($openApi);
        $this->createFormStructureSchema($openApi);
        /**
         * @var string $formId
         * @var FormManager $form
         */
        foreach ($forms as $formId => $form) {
            $this->createFormSchema('GET', $openApi, $formId, $form);
            $this->createFormSchema('POST', $openApi, $formId, $form);
            $this->createFormSchema('PUT', $openApi, $formId, $form);
        }
        // Inquiries form schema.
        $oApifields = [
            'type' => 'object',
            'description' => $this->t('Inquiry forms record.')->render(),
            'properties' => [
                'id' => ['readOnly' => true, 'description' => $this->t('Record ID')->render(), 'type' => 'string', 'nullable' => false],
                'type' => ['readOnly' => true, 'description' => $this->t('Record type')->render(), 'type' => 'string', 'nullable' => false],
                'type_title' => ['readOnly' => true, 'description' => $this->t('Record type label')->render(), 'type' => 'string', 'nullable' => false],
                'field_reference' => ['readOnly' => true, 'description' => $this->t('Record user reference')->render(), 'type' => 'string', 'nullable' => false],
                'field_ulid_reference' => ['readOnly' => true, 'description' => $this->t('Record internal reference')->render(), 'type' => 'string', 'nullable' => false],
                'field_validated' => ['readOnly' => true, 'description' => $this->t('Record was validated')->render(), 'type' => 'boolean', 'nullable' => false],
                'field_deleted' => ['readOnly' => true, 'description' => $this->t('Record deleted')->render(), 'type' => 'boolean', 'nullable' => false],
                'field_status' => ['readOnly' => true, 'description' => $this->t('Record status')->render(), 'type' => 'string', 'nullable' => false],
                'field_country' => ['readOnly' => true, 'description' => $this->t('Record country')->render(), 'type' => 'object', 'nullable' => false],
                'field_region' => ['readOnly' => true, 'description' => $this->t('Record administrative division')->render(), 'type' => 'object', 'nullable' => false],
                'field_creator' => ['readOnly' => true, 'description' => $this->t('Record user creator')->render(), 'type' => 'object', 'nullable' => false],
                'field_editors' => ['readOnly' => true, 'description' => $this->t('Record user editors')->render(), 'type' => 'array', 'items' => ['type' => 'object'], 'nullable' => false],
                'field_editors_update' => ['readOnly' => true, 'description' => $this->t('Record user editors updates')->render(), 'type' => 'array', 'items' => ['type' => 'object'], 'nullable' => false],
                'field_image' => ['readOnly' => true, 'description' => $this->t('Record image')->render(), 'type' => 'string', 'nullable' => true],
                'field_title' => ['readOnly' => true, 'description' => $this->t('Record title')->render(), 'type' => 'string', 'nullable' => true],
            ],
            'required' => [],
        ];
        self::addComponentSchema($openApi, 'inquiries_join-GET', $oApifields);
        // Form end-points.
        $formIdParam = new Model\Parameter(
            'fid',
            'path',
            $this->t('Form ID')->render(),
            true,
            false,
            false,
            ["type" => "string"],
            'simple'
        );
        $idParam = new Model\Parameter(
            'id',
            'path',
            $this->t('Record ID')->render(),
            true,
            false,
            false,
            ["type" => "string"],
            'simple'
        );
        $pageParam = new Model\Parameter(
            $this->systemSettings->get('api_platform.collection.pagination.page_parameter_name'),
            'query',
            $this->t(
                'The collection page number. We will get @cnt items per page.',
                ['@cnt' => $this->systemSettings->get('api_platform.collection.pagination.items_per_page')]
            )->render(),
            false,
            false,
            false,
            [
                "type" => "integer",
                "default" => 1,
            ],
            'simple'
        );
        $inquiryFormsKeys = [];
        foreach ($forms as $formId => $form) {
            if ($form->getType() === InquiryFormManager::class) {
                $inquiryFormsKeys[] = $formId;
            }
        }
        /**
         * @var string $formId
         * @var FormManager $form
         */
        foreach ($forms as $formId => $form) {
            if (!$form) {
                continue;
            }
            // Form data end-points.
            // Item paths.
            if ($form->haveEndpoint(FormManager::ENDPOINT_ITEM_GET)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_GET);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $getOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $getOp = new Operation(
                        'getFormRecordItem',
                        [$form->getTitle()],
                        [
                            '200' => $this->getResponseOk($form),
                            '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                        ],
                        $this->t('Get @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [$idParam],
                        null,
                        null,
                        false,
                        null,
                        null
                    );
                }

                $this->addDefaultResponseCodes($getOp);
            } else {
                $getOp = null;
            }
            if ($form->haveEndpoint(FormManager::ENDPOINT_ITEM_PUT)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_PUT);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $putOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $putOp = new Operation(
                        'putFormRecordItem',
                        [$form->getTitle()],
                        [
                            '200' => $this->getResponseOk($form),
                            '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                            '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                            '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
                        ],
                        $this->t('Update @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [$idParam],
                        $this->getPutRequestBody($form),
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($putOp);
            } else {
                $putOp = null;
            }
            if ($form->haveEndpoint(FormManager::ENDPOINT_ITEM_PATCH)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_PATCH);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $patchOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $patchOp = new Operation(
                        'patchFormRecordItem',
                        [$form->getTitle()],
                        [
                            '200' => $this->getResponseOk($form),
                            '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                            '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                            '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
                        ],
                        $this->t('Update @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [$idParam],
                        $this->getPutRequestBody($form),
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($patchOp);
            } else {
                $patchOp = null;
            }
            if ($form->haveEndpoint(FormManager::ENDPOINT_ITEM_DELETE)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_DELETE);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $deleteOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $deleteOp = new Operation(
                        'deleteFormRecordItem',
                        [$form->getTitle()],
                        [
                            '204' => $this->getResponseFail($this->t('@form resource deleted', ['@form' => $form->getId()])->render()),
                            '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                        ],
                        $this->t('Delete @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [$idParam],
                        null,
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($deleteOp);
            } else {
                $deleteOp = null;
            }
            if ($form->haveEndpoint(FormManager::ENDPOINT_ITEM_CLONE)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_CLONE);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $cloneOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $cloneOp = new Operation(
                        'cloneFormRecordItem',
                        [$form->getTitle()],
                        [
                            '200' => $this->getResponseOk($form),
                            '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                            '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                            '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
                        ],
                        $this->t('Clone @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [$idParam],
                        null,
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($cloneOp);
            } else {
                $cloneOp = null;
            }
            if ($form->haveEndpoint(FormManager::ENDPOINT_ITEM_HISTORY)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_ITEM_HISTORY);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $historyOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $historyOp = new Operation(
                        'historyFormRecordItem',
                        [$form->getTitle()],
                        [
                            '200' => $this->getMultipleResponseOk($form),
                            '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                            '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                            '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
                        ],
                        $this->t('Get history @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [$idParam],
                        null,
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($historyOp);
            } else {
                $historyOp = null;
            }
            // Get, put and delete.
            $pItem = new PathItem(
                null,
                $form->getTitle(),
                $form->getDescription(),
                $getOp,
                $putOp,
                null,
                $deleteOp,
                null,
                null,
                $patchOp,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/form/data/'.$formId.'s/{id}', $pItem);
            // Clone.
            $pItem = new PathItem(
                null,
                $form->getTitle(),
                $form->getDescription(),
                null,
                $cloneOp,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/form/data/'.$formId.'s/{id}/clone', $pItem);
            // History.
            $pItem = new PathItem(
                null,
                $form->getTitle(),
                $form->getDescription(),
                $historyOp,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/form/data/'.$formId.'s/{id}/history', $pItem);
            if ($form->haveEndpoint(FormManager::ENDPOINT_COLLECTION_GET)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_COLLECTION_GET);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $getOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    // Root paths. Default form controller.
                    $parameters = [$pageParam];
                    // Sort parameters.
                    $sorts = $form->getSortFields();
                    /** @var FieldTypeInterface $sort */
                    foreach ($sorts as $sort) {
                        $paramName = $sort->getId();
                        $parameters[] = new Model\Parameter(
                            "order[$paramName]",
                            'query',
                            $this->t(
                                'Sort collection by "@fieldName".',
                                ['@fieldName' => $sort->getLabel()->render()]
                            )->render(),
                            false,
                            false,
                            true,
                            [
                                "type" => "string",
                                "enum" => ['asc', 'desc'],
                            ],
                            'simple'
                        );
                    }
                    // Filter parameters.
                    $filters = $form->getFilterFields();
                    /** @var FieldTypeInterface $filter */
                    foreach ($filters as $filter) {
                        $paramName = $filter->getId();
                        $parameters[] = new Model\Parameter(
                            $paramName,
                            'query',
                            $this->t(
                                'Filter collection by "@fieldName".',
                                ['@fieldName' => $filter->getLabel()->render()]
                            )->render(),
                            false,
                            false,
                            true,
                            $this->getParamSchemaByField($filter),
                            'simple'
                        );
                    }
                    $parameters[] = new Model\Parameter(
                        'unique',
                        'query',
                        $this->t('Filter collection by unique records.')->render(),
                        false,
                        false,
                        true,
                        [
                            "type" => "boolean",
                        ],
                        'simple'
                    );
                    $getOp = new Operation(
                        'getCollectionFormRecordItem',
                        [$form->getTitle()],
                        [
                            '200' => $this->getResponseCollectionOk($form),
                        ],
                        $this->t('Get @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        $parameters,
                        null,
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($getOp);
            } else {
                // End-point disabled.
                $getOp = null;
            }
            // Post.
            if ($form->haveEndpoint(FormManager::ENDPOINT_COLLECTION_POST)) {
                $external = $form->getEndpointRawValue(FormManager::ENDPOINT_COLLECTION_POST);
                if (is_array($external)) {
                    // The form defines his own response definition.
                    $postOp = call_user_func($external['openapi_operation'], $form, $openApi);
                } else {
                    $postOp = new Operation(
                        'postCollectionFormRecordItem',
                        [$form->getTitle()],
                        [
                            '201' => $this->getResponseOk($form),
                            '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                            '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
                        ],
                        $this->t('Add @desc', ['@desc' => $form->getDescription()])->render(),
                        '',
                        null,
                        [],
                        $this->getPostRequestBody($form),
                        null,
                        false,
                        null,
                        null
                    );
                }
                $this->addDefaultResponseCodes($postOp);
            } else {
                $postOp = null;
            }
            // Path.
            $pItem = new PathItem(
                null,
                $form->getTitle(),
                $form->getDescription(),
                $getOp,
                null,
                $postOp,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/form/data/'.$formId.'s', $pItem);
        }
        // Inquiries get collection.
        $parameters = [$pageParam];
        $fields = [
            'id' => ['type' => 'string'],
            'field_reference' => ['type' => 'string'],
            'field_ulid_reference' => ['type' => 'string'],
            'field_validated' => ['type' => 'boolean'],
            'field_deleted' => ['type' => 'boolean'],
            'field_status' => ['type' => 'array', 'items' => ['type' => 'string']],
            'field_region' => ['type' => 'string'],
            'field_creator' => ['type' => 'string'],
            'field_created' => ['type' => 'string'],
            'field_changed' => ['type' => 'string'],
        ];
        // Inquiries field orders.
        foreach ($fields as $field => $fType) {
            $parameters[] = new Model\Parameter(
                "order[$field]",
                'query',
                $this->t(
                    'Sort collection by "@fieldName".',
                    ['@fieldName' => $field]
                )->render(),
                false,
                false,
                true,
                [
                    "type" => 'string',
                    "enum" => ['asc', 'desc'],
                ],
                'simple'
            );
        }
        // Inquiries field filters.
        foreach ($fields as $field => $fType) {
            $parameters[] = new Model\Parameter(
                $field,
                'query',
                $this->t('Filter collection by "@fieldName".', ['@fieldName' => $field])->render(),
                false,
                false,
                true,
                $fType,
                'simple'
            );
        }
        // Filter by record type.
        $parameters[] = new Model\Parameter(
            'record_type',
            'query',
            $this->t('Filter collection by "Record type". Allowed types: @types', ['@types' => implode(', ', $inquiryFormsKeys)])->render(),
            false,
            false,
            true,
            ['type' => 'array', 'item' => ['type' => 'string', 'enum' => $inquiryFormsKeys]],
            'simple'
        );
        // Add operation.
        $getOp = new Operation(
            'getCollectionFormInquiryRecordItem',
            ['Inquiries'],
            [
                '200' => $this->getResponseInquiryCollectionOk(),
            ],
            $this->t('Get @desc', ['@desc' => $this->t('List of all inquiry forms records. If not filtered by type only show records from forms with getting collection end-points.')])->render(),
            '',
            null,
            $parameters,
            null,
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($getOp);
        // Multiple form records and images upload, in ZIP format, and import.
        $postOp = new Operation(
            'inquiriesUpload',
            ['Inquiries'],
            [
                '201' => $this->getResponseUploadOk(),
                '400' => $this->getResponseFail($this->t('Invalid input')->render(), false),
                '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
            ],
            self::t('Upload ZIP to import images and form records inside it.')->render(),
            self::t('Upload ZIP to import images and form records inside it.')->render(),
            null,
            [],
            $this->getPostZIPRequestBody(),
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($postOp);

        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Inquiries collection')->render(),
            $this->t('List all of inquiry forms records.')->render(),
            $getOp,
            null,
            $postOp,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/inquiries', $pItem);

        // Endpoints of Point to manage Community, WSP and WSSystem forms.
        $pointForm = $this->formFactory->find('form.point');
        $wspForm = $this->formFactory->find('form.wsprovider');
        $wssytemForm = $this->formFactory->find('form.wssystem');
        $communityForm = $this->formFactory->find('form.community');

        // Point add WSP form.
        $pointIdParam = new Model\Parameter(
            'pid',
            'path',
            $this->t('Point ID')->render(),
            true,
            false,
            false,
            ["type" => "string"],
            'simple'
        );

        $inquiriesPoint = [
            'post' => [
                'form.wsprovider' => 'postPointWSPFormRecordItem',
                'form.wssystem' => 'postPointWSSystemFormRecordItem',
            ],
            'delete' => [
                'form.wssystem' => 'deletePointWSSystemFormRecordItem',
                'form.community' => 'deletePointCommunityFormRecordItem',
                'form.wsprovider' => 'deletePointWSPFormRecordItem',
            ],
        ];
        foreach ($inquiriesPoint as $method => $forms) {
            switch ($method) {
                case 'post':
                    foreach ($forms as $formId => $action) {
                        $form = $this->formFactory->find($formId);
                        $postOp = new Operation(
                            $action,
                            [$pointForm->getTitle()],
                            [
                                '201' => $this->getResponseOk($form),
                                '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                                '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
                            ],
                            $this->t('Add @desc', ['@desc' => $form->getDescription()])->render(),
                            '',
                            null,
                            [$idParam],
                            $this->getPostRequestBody($form),
                            null,
                            false,
                            null,
                            null
                        );
                        $this->addDefaultResponseCodes($postOp);
                        // Path.
                        $pItem = new PathItem(
                            null,
                            $this->t('Add @title to Siasar Point', ['@title' => $form->getTitle()])->render(),
                            $this->t('Add @title record to specific Siasar Point.', ['@title' => $form->getTitle()])->render(),
                            null,
                            null,
                            $postOp,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            []
                        );
                        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{id}/'.$form->getId(), $pItem);
                    }
                    break;
                case 'delete':
                    foreach ($forms as $formId => $action) {
                        $form = $this->formFactory->find($formId);
                        $deleteOp = new Operation(
                            $action,
                            [$pointForm->getTitle()],
                            [
                                '204' => $this->getResponseFail($this->t('@form resource deleted', ['@form' => $form->getId()])->render()),
                                '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                                '422' => $this->getResponseFail($this->t('Unprocessable entity')->render()),
                            ],
                            $this->t('Delete @desc', ['@desc' => $form->getDescription()])->render(),
                            '',
                            null,
                            [$pointIdParam, $idParam],
                            null,
                            null,
                            false,
                            null,
                            null
                        );
                        $this->addDefaultResponseCodes($deleteOp);
                        $pItem = new PathItem(
                            null,
                            $this->t('Delete @title to Siasar Point', ['@title' => $form->getTitle()])->render(),
                            $this->t('Delete @title record to specific Siasar Point.', ['@title' => $form->getTitle()])->render(),
                            null,
                            null,
                            null,
                            $deleteOp,
                            null,
                            null,
                            null,
                            null,
                            null,
                            []
                        );
                        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{pid}/'.$form->getId().'/{id}', $pItem);
                        if ('form.community' === $formId) {
                            // Add delete households from community.
                            $deleteOp = new Operation(
                                'deletePointCommunityHouseHoldsItem',
                                [$pointForm->getTitle()],
                                [
                                    '204' => $this->getResponseFail($this->t('@form households deleted', ['@form' => $form->getId()])->render()),
                                    '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                                    '422' => $this->getResponseFail($this->t('Unprocessable entity')->render()),
                                ],
                                $this->t('Delete @desc', ['@desc' => $form->getDescription()])->render(),
                                '',
                                null,
                                [$pointIdParam, $idParam],
                                null,
                                null,
                                false,
                                null,
                                null
                            );
                            $this->addDefaultResponseCodes($deleteOp);
                            $pItem = new PathItem(
                                null,
                                $this->t('Delete households from community')->render(),
                                $this->t('Delete households records from community and set it how without households.')->render(),
                                null,
                                null,
                                null,
                                $deleteOp,
                                null,
                                null,
                                null,
                                null,
                                null,
                                []
                            );
                            $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{pid}/'.$form->getId().'/{id}/households', $pItem);
                        }
                    }
                    break;
            }
        }
        // Point add Water supply system form.
        // Add operation.
        $postOp = new Operation(
            'postPointWSSystemFormRecordItem',
            [$pointForm->getTitle()],
            [
                '201' => $this->getResponseOk($wssytemForm),
                '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
            ],
            $this->t('Add @desc', ['@desc' => $wssytemForm->getDescription()])->render(),
            '',
            null,
            [$idParam],
            $this->getPostRequestBody($wssytemForm),
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($postOp);
        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Add Water Supply System to Siasar Point')->render(),
            $this->t('Add Water Supply System record to specific Siasar Point.')->render(),
            null,
            null,
            $postOp,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{id}/form.wssystem', $pItem);

        // Point list forms.
        // Add operation.
        $getOp = new Operation(
            'getPointInquiries',
            [$pointForm->getTitle()],
            [
                '200' => $this->getResponseInquiryCollectionOk(),
                '404' => $this->getResponseFail($this->t('Resource not found')->render()),
            ],
            $this->t('Get forms in point')->render(),
            '',
            null,
            [$idParam],
            null,
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($getOp);
        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Get forms in point')->render(),
            $this->t('Get forms in Siasar Point.')->render(),
            $getOp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{id}/inquiries', $pItem);

        // Clone Point.
        // Add operation
        $cloneOp = new Operation(
            'cloneFormRecordItem',
            [$pointForm->getTitle()],
            [
                '200' => $this->getResponseOk($pointForm),
                '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
            ],
            $this->t('Clone Siasar Point')->render(),
            '',
            null,
            [$idParam],
            null,
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($cloneOp);
        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Clone Siasar Point')->render(),
            $this->t('Clone Siasar Point.')->render(),
            null,
            $cloneOp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{id}/clone', $pItem);
        // Replan Point.
        // Add operation
        $replanOp = new Operation(
            'replanFormRecordItem',
            [$pointForm->getTitle()],
            [
                '201' => $this->getResponseOk($pointForm),
                '400' => $this->getResponseFail($this->t('Invalid input')->render(), true),
                '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                '422' => $this->getResponseFail($this->t('Unprocessable entity')->render(), true),
            ],
            $this->t('Replan Siasar Point. Unlink forms from the indicated point to a new point')->render(),
            '',
            null,
            [$idParam],
            $this->replanPointRequestBody($pointForm),
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($replanOp);
        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Replan Point')->render(),
            $this->t('Unlink forms from the indicated point to a new point')->render(),
            null,
            $replanOp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{id}/replan', $pItem);

        // History point.
        // Add operation
        $parameters = [$idParam, $pageParam];
        $fields = [
            'field_created' => ['type' => 'string'],
            'field_changed' => ['type' => 'string'],
        ];
        // Inquiries field orders.
        foreach ($fields as $field => $fType) {
            $parameters[] = new Model\Parameter(
                "order[$field]",
                'query',
                $this->t(
                    'Sort collection by "@fieldName".',
                    ['@fieldName' => $field]
                )->render(),
                false,
                false,
                true,
                [
                    "type" => 'string',
                    "enum" => ['asc', 'desc'],
                ],
                'simple'
            );
        }
        $getHistoryOp = new Operation(
            'getPointHistory',
            [$pointForm->getTitle()],
            [
                '200' => $this->getResponseOk($pointForm),
                '404' => $this->getResponseFail($this->t('Resource not found')->render()),
            ],
            $this->t('Get the history of a Siasar Point')->render(),
            '',
            null,
            $parameters,
            null,
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($getHistoryOp);
        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Get the history of a Siasar Point')->render(),
            $this->t('Get the history of a Siasar Point.')->render(),
            $getHistoryOp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/'.$pointForm->getId().'s/{id}/history', $pItem);
        // Form structure space.
        if (count($forms) > 0) {
            // Form structure.
            $getOp = new Operation(
                'getFormStructureItem',
                [$this->t('Form structure')->render()],
                [
                    '200' => $this->getResponseStructureOk(),
                    '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                ],
                $this->t('Get form structure.')->render(),
                '',
                null,
                [$formIdParam],
                null,
                null,
                false,
                null,
                null
            );
            $this->addDefaultResponseCodes($getOp);
            $pItem = new PathItem(
                null,
                $this->t('@form structure.', ['@form' => $form->getTitle()])->render(),
                $form->getDescription(),
                $getOp,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/form/structure/{fid}', $pItem);
            // Form empty template.
            $getOp = new Operation(
                'getFormStructureEmptyItem',
                [$this->t('Form structure')->render()],
                [
                    '200' => $this->getResponseUndefinedOk(),
                    '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                ],
                $this->t('Get empty record template.')->render(),
                'Get an empty record, useful to emulate API responses in offline mode clients.',
                null,
                [$formIdParam],
                null,
                null,
                false,
                null,
                null
            );
            $this->addDefaultResponseCodes($getOp);
            $pItem = new PathItem(
                null,
                $this->t('@form empty record template.', ['@form' => $form->getTitle()])->render(),
                $form->getDescription(),
                $getOp,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/form/structure/empty/{fid}', $pItem);
        }
        // Form structure list end-points.
        $typeFilterParam = new Model\Parameter(
            'type',
            'query',
            $this->t('Filter by form type.')->render().' '.$this->t('Use a double slash, "\\\\\\\\", to filter. e.g. "App\\\\\\\\Forms\\\\\\\\InquiryFormManager".'),
            false,
            false,
            false,
            [
                "type" => "string",
                "default" => '',
            ],
            'simple'
        );
        $getOp = new Operation(
            'getCollectionFormRecordItem',
            [$this->t('Form structure')->render()],
            [
                '200' => $this->getResponseStructureCollectionOk(),
            ],
            $this->t('Get forms list')->render(),
            '',
            null,
            [$pageParam, $typeFilterParam],
            null,
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($getOp);
        $pItem = new PathItem(
            null,
            $this->t('Forms list')->render(),
            '',
            $getOp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/structure', $pItem);
        // Add parametric download template links.
        $parametrics = $this->templates->getParamtricEntities();
        foreach ($parametrics as $parametric) {
            $className = TemplateGenerator::getClassnameOnly($parametric->getName());
            $comment = $this->templates->getParametricComment($parametric->getName());
            // Parametric CSV template download.
            $getOp = new Operation(
                'getParametricDownloadTemplate'.$className,
                ['Configuration'],
                [
                    '200' => $this->getFileResponseOk(),
                ],
                $comment,
                'Download template from '.$comment,
                null,
                [],
                null,
                null,
                false,
                null,
                null
            );
            $this->addDefaultResponseCodes($getOp);
            $pItem = new PathItem(
                null,
                $comment,
                'Download template from '.$comment,
                $getOp,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/configuration/templates/'.$className, $pItem);
            // Parametric CSV upload and import.
            $postOp = new Operation(
                'getParametricUpload'.$className,
                ['Configuration'],
                [
                    '201' => $this->getResponseUploadOk(),
                    '404' => $this->getResponseFail($this->t('Resource not found')->render()),
                ],
                $comment,
                self::t('Upload CSV, and add a new queue job to import @class', ['@class' => $comment])->render(),
                null,
                [],
                $this->getPostCSVRequestBody($className),
                null,
                false,
                null,
                null
            );
            $this->addDefaultResponseCodes($postOp);
            $pItem = new PathItem(
                null,
                $comment,
                self::t('Upload CSV, and add a new queue job to import @class', ['@class' => $comment])->render(),
                null,
                null,
                $postOp,
                null,
                null,
                null,
                null,
                null,
                null,
                []
            );
            $openApi->getPaths()->addPath('/api/v1/configuration/import/'.$className, $pItem);
        }
        // Count unread mails.
        // Add operation
        $mailForm = $this->formFactory->find('form.mail');
        $countMailOp = new Operation(
            'unreadMailsCount',
            [$mailForm->getTitle()],
            [
                '200' => $this->getMailResponseOk($mailForm),
            ],
            $this->t('Count unread mails.')->render(),
            '',
            null,
            [],
            null,
            null,
            false,
            null,
            null
        );
        $this->addDefaultResponseCodes($countMailOp);
        // Path.
        $pItem = new PathItem(
            null,
            $this->t('Count unread mails')->render(),
            $this->t('Count unread mails.')->render(),
            $countMailOp,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            []
        );
        $openApi->getPaths()->addPath('/api/v1/form/data/'.$mailForm->getId().'s/count_unread', $pItem);
        // Add server list.
        if (!isset($_ENV['APP_CONFIG_OTHER_SERVERS'])) {
            $_ENV['APP_CONFIG_OTHER_SERVERS'] = '';
        }
        $servers = explode(',', $_ENV['APP_CONFIG_OTHER_SERVERS']);
        $serverList = [new Server('/', $this->t('This server'))];
        foreach ($servers as $server) {
            $server = explode('|', trim($server));
            $description = '';
            if (2 === count($server)) {
                $description = trim($server[1]);
            }
            $server = trim($server[0]);
            if ('/' !== $server && !empty($server)) {
                $serverList[] = new Server($server, $description);
            }
        }
        $openApi = $openApi->withServers($serverList);

        return $openApi;
    }

    /**
     * Get POST request body.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return RequestBody
     */
    protected function getPostRequestBody(FormManagerInterface $form): Model\RequestBody
    {
        return new Model\RequestBody(
            $this->t('The new @form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-POST',
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            ),
            true
        );
    }

    /**
     * Get PUT request body.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return RequestBody
     */
    protected function getPutRequestBody(FormManagerInterface $form): Model\RequestBody
    {
        return new Model\RequestBody(
            $this->t('The @form resource updates', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-POST',
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            ),
            true
        );
    }

    /**
     * Get inquiry collection OK response.
     *
     * @return Response
     */
    protected function getResponseInquiryCollectionOk(): Response
    {
        return new Response(
            $this->t('Retrieves the collection of inquiry resources')->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'array',
                                'items' => [
                                    '$ref' => '#/components/schemas/inquiries_join-GET',
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get collection OK response.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return Response
     */
    protected function getResponseCollectionOk(FormManagerInterface $form): Response
    {
        return new Response(
            $this->t('Retrieves the collection of @form resources', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'array',
                                'items' => [
                                    '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-GET',
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get forms list OK response.
     *
     * @return Response
     */
    protected function getResponseStructureCollectionOk(): Response
    {
        return new Response(
            $this->t('Retrieves the collection of forms resources')->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'array',
                                'items' => [
                                    'type' => 'object',
                                    'properties' => [
                                        'id' => [
                                            'readOnly' => true,
                                            'description' => $this->t('Form ID')->render(),
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                        'path' => [
                                            'readOnly' => true,
                                            'description' => $this->t('Form structure path')->render(),
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                        'type' => [
                                            'readOnly' => true,
                                            'description' => $this->t('Form manager type')->render(),
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                        'title' => [
                                            'readOnly' => true,
                                            'description' => $this->t('Form title')->render(),
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                        'description' => [
                                            'readOnly' => true,
                                            'description' => $this->t('Form description')->render(),
                                            'type' => 'string',
                                            'nullable' => false,
                                        ],
                                        'sdg' => [
                                            'readOnly' => true,
                                            'description' => $this->t('Form allow SDG status')->render(),
                                            'type' => 'boolean',
                                            'nullable' => false,
                                        ],
                                    ],
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get OK response.
     *
     * @return Response
     */
    protected function getResponseStructureOk(): Response
    {
        return new Response(
            $this->t('Form structure')->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/form_structure',
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get OK response.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return Response
     */
    protected function getResponseOk(FormManagerInterface $form): Response
    {
        return new Response(
            $this->t('@form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-GET',
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get multiple OK response.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return Response
     */
    protected function getMultipleResponseOk(FormManagerInterface $form): Response
    {
        return new Response(
            $this->t('@form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'array',
                                'items' => new \ArrayObject(
                                    [
                                        '$ref' => '#/components/schemas/'.str_replace('.', '_', $form->getId()).'-GET',
                                    ]
                                ),
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get OK response.
     *
     * @return Response
     */
    protected function getResponseUndefinedOk(): Response
    {
        return new Response(
            $this->t('Undefined resource')->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject([
                                'type' => 'object',
                        ]),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get upload OK response.
     *
     * @return Response
     */
    protected function getResponseUploadOk(): Response
    {
        return new Response(
            $this->t('Upload OK response.')->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'object',
                                'properties' => [
                                    'message' => [
                                        'readOnly' => true,
                                        'description' => self::t('Status response')->render(),
                                        'type' => 'string',
                                        'nullable' => false,
                                    ],
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Generated body definition to import parametric end-point.
     *
     * @param string $class
     * @param bool   $required
     *
     * @return RequestBody
     */
    protected function getPostCSVRequestBody(string $class, bool $required = true)
    {
        // Generated in part by https://chat.openai.com/chat/9857f989-d877-4f3b-97e2-85c8efef4bc1 (GPT-3)
        // completed with custom code and https://stackoverflow.com/questions/65082784/how-do-i-annotate-an-endpoint-in-nestjs-for-openapi-that-takes-multipart-form-da
        return new RequestBody(
            self::t('File to upload.')->render(),
            new \ArrayObject([
                'multipart/form-data' => [
                    'schema' => [
                        'type' => 'object',
                        'properties' => [
                            'file' => [
                                'type' => 'string',
                                'format' => 'binary',
                                'description' => self::t(
                                    'The CSV file to upload. See "@path" to get the template file.',
                                    ['@path' => '/configuration/templates/'.$class]
                                )->render(),
                            ],
                        ],
                        'required' => ['file'],
                    ],
                ],
            ])
        );
    }

    /**
     * Generated body definition to inquiries upload end-point.
     *
     * @return RequestBody
     */
    protected function getPostZIPRequestBody()
    {
        // Generated in part by https://chat.openai.com/chat/9857f989-d877-4f3b-97e2-85c8efef4bc1 (GPT-3)
        // completed with custom code and https://stackoverflow.com/questions/65082784/how-do-i-annotate-an-endpoint-in-nestjs-for-openapi-that-takes-multipart-form-da
        return new RequestBody(
            self::t('File to upload.')->render(),
            new \ArrayObject([
                'multipart/form-data' => [
                    'schema' => [
                        'type' => 'object',
                        'properties' => [
                            'file' => [
                                'type' => 'string',
                                'format' => 'binary',
                                'description' => self::t('The ZIP file to upload.')->render(),
                            ],
                            'username' => [
                                "type" => "string",
                            ],
                            "password" => [
                                "type" => "password",
                                "format" => "password",
                            ],
                        ],
                        'required' => ['file'],
                    ],
                ],
            ])
        );
    }

    /**
     * Get OK response.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return Response
     */
    protected function getMailResponseOk(FormManagerInterface $form): Response
    {
        return new Response(
            $this->t('@form resource', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'object',
                                'properties' => [
                                    'count' => [
                                        'readOnly' => true,
                                        'description' => $this->t('Number of unread mails')->render(),
                                        'type' => 'integer',
                                        'nullable' => false,
                                    ],
                                ],
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            )
        );
    }

    /**
     * Get OK binary response.
     *
     * @return Response
     */
    protected function getFileResponseOk(): Response
    {
        return new Response(
            $this->t('Binary resource')->render(),
            new \ArrayObject(
                [
                    'application/octet-stream' => new MediaType(
                        new \ArrayObject(
                            [
                                "type" => "string",
                                "format" => "binary",
                            ]
                        ),
                        null,
                        null,
                        new Model\Encoding('application/octet-stream')
                    ),
                ],
            )
        );
    }

    /**
     * Update a operation with annotations.
     *
     * @param Operation|null $post The operation to update.
     *
     * @return ?Operation
     *
     * @throws \ReflectionException
     */
    protected function updateOperation(?Operation $post): ?Operation
    {
        if ($post) {
            $className = $post->getTags()[0];
            $completeClassName = 'App\Entity\\'.$className;
            if (class_exists($completeClassName)) {
                // Get entity annotations.
                $apiResource = $this->getAnnotationApiResource($completeClassName);
                $operationId = str_replace($className.'Collection', '', $post->getOperationId());
                if (isset($apiResource->collectionOperations[$operationId])) {
                    $newOperation = $apiResource->collectionOperations[$operationId];
                    if (isset($newOperation["openapi_context"]["parameters"])) {
                        // Add custom parameters.
                        $post = $post->withParameters($newOperation["openapi_context"]["parameters"]);
                    }
                    if (isset($newOperation["openapi_context"]["responses"])) {
                        $post = $post->withResponses([]);
                        foreach ($newOperation["openapi_context"]["responses"] as $status => $newResponses) {
                            $createdResponse = new Response(
                                $newResponses['description'] ?? '',
                                isset($newResponses['content']) ? new \ArrayObject($newResponses['content']) : null,
                                isset($newResponses['headers']) ? new \ArrayObject($newResponses['headers']) : null,
                                isset($newResponses['links']) ? new \ArrayObject($newResponses['links']) : null
                            );
                            $post->addResponse($createdResponse, $status);
                        }
                    }
                }
            }
            $this->addDefaultResponseCodes($post);
        }

        return $post;
    }

    /**
     * Get APIResource annotation if existed.
     *
     * @param string $classPath Class name with namespace.
     *
     * @return object|null
     *
     * @throws \ReflectionException
     */
    protected function getAnnotationApiResource(string $classPath): ?object
    {
        if (isset($this->annotations[$classPath])) {
            return $this->annotations[$classPath];
        }
        // Deprecated and will be removed in 2.0 but currently needed
        AnnotationRegistry::registerLoader('class_exists');
        $reader = new AnnotationReader();
        $reflectionClass = new ReflectionClass($classPath);
        $this->annotations[$classPath] = $reader->getClassAnnotation($reflectionClass, ApiResource::class);

        return $this->annotations[$classPath];
    }

    /**
     * Create the form schema.
     *
     * @param string               $method  Schema method.
     * @param OpenApi              $openApi Open API definition.
     * @param string               $formId  The form ID.
     * @param FormManagerInterface $form    The form manager.
     */
    protected function createFormSchema(string $method, OpenApi $openApi, string $formId, FormManagerInterface $form): void
    {
        $oApifields = [
            'type' => 'object',
            'description' => $form->getDescription(),
            'properties' => [],
            'required' => [],
        ];
        if ('GET' === $method) {
            $oApifields['properties']['id'] = [
                'readOnly' => true,
                'description' => $this->t('Record ID')->render(),
                'type' => 'string',
                'nullable' => false,
            ];
        }
        $fields = $form->getFields();
        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            $oApifields['properties'][$field->getId()] = [];
            if ($field->isMultivalued()) {
                $fieldSchema = $this->getFormFieldTypeSchemaName($field->getFieldType());
                $fieldSchemaArray = [
                    'type' => 'array',
                    'items' => $fieldSchema,
                ];
                $oApifields['properties'][$field->getId()] += $fieldSchemaArray;
            } else {
                $oApifields['properties'][$field->getId()] += ['description' => $field->getLabel()->render().(!empty($field->getDescription()) ? ': '.$field->getDescription() : '')];
                $oApifields['properties'][$field->getId()] += $this->getFormFieldTypeSchemaName($field->getFieldType());
            }
            if ($field->isRequired()) {
                $oApifields['required'][] = $field->getId();
            }
            if ($field->isDeprecated()) {
                // Mark the field how deprecated instead hide it.
                $oApifields['properties'][$field->getId()] += ['deprecated' => true];
                $oApifields['properties'][$field->getId()]['description'] .= ' Deprecated: '.$field->getDeprecationReason();
            }
        }
        self::addComponentSchema($openApi, str_replace('.', '_', $formId).'-'.$method, $oApifields);
    }

    /**
     * Create field types definition schemas.
     *
     * @param OpenApi $openApi Open API definition.
     */
    protected function createFieldTypesSchemas(OpenApi $openApi): void
    {
        $fieldTypeDefinitions = $this->fieldTypeManager->getFieldTypes();
        foreach ($fieldTypeDefinitions as $fieldTypeId => $fieldTypeDefinition) {
            $fieldType = $this->fieldTypeManager->create($fieldTypeId);
            if (count($fieldType->getProperties()) === 1) {
                continue;
            }
            $oApifields = [
                'type' => 'object',
                'description' => $fieldType->getDescription(),
                'properties' => [],
                'required' => [],
            ];
            foreach ($fieldType->getProperties() as $propId => $propDefault) {
                $oApifields['properties'][$propId] = [
                    'description' => '',
                    'type' => 'string',
                ];
                $oApifields['required'][] = $propId;
            }
            self::addComponentSchema($openApi, 'form_type__'.$fieldTypeId, $oApifields);
        }
    }

    /**
     * Add error message schema.
     *
     * @param OpenApi $openApi The Open API definition.
     */
    protected function createFormErrorSchema(OpenApi $openApi): void
    {
        $oApifields = [
            'type' => 'object',
            'description' => $this->t('Form error')->render(),
            'properties' => [
                'message' => [
                    'type' => 'string',
                ],
            ],
            'required' => [],
        ];
        self::addComponentSchema($openApi, 'form_error', $oApifields);
    }

    /**
     * Create field types definition schemas.
     *
     * @param OpenApi $openApi Open API definition.
     */
    protected function createFormStructureSchema(OpenApi $openApi): void
    {
        $oApifields = [
            'type' => 'object',
            'description' => $this->t('Form structure')->render(),
            // Some properties are optional.
            'properties' => [
                'id' => ['type' => 'string'],
                'type' => ['type' => 'string'],
                'title' => ['type' => 'string'],
                'description' => ['type' => 'string'],
                'meta' => ['type' => 'object'],
                'fields' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        // Some properties are optional.
                        'properties' => [
                            'type' => ['type' => 'string'],
                            'id' => ['type' => 'string'],
                            'label' => ['type' => 'string'],
                            'description' => ['type' => 'string'],
                            'indexable' => ['type' => 'boolean'],
                            'deprecated' => ['type' => 'boolean'],
                            'settings' => [
                                'type' => 'object',
                            ],
                        ],
                    ],
                ],
            ],
            'required' => [],
        ];
        self::addComponentSchema($openApi, 'form_structure', $oApifields);
    }

    /**
     * Get Open API field type equivalent.
     *
     * @param string $id Field type Id.
     *
     * @return array
     */
    protected function getFormFieldTypeSchemaName(string $id): array
    {
        $resp = [];
        switch ($id) {
            case 'integer':
                $resp['format'] = 'int64';
                // Integer and boolean are standard data types, but integer contains format.
            case 'boolean':
                $resp['type'] = $id;
                break;
            case 'decimal':
                $resp['type'] = 'number';
                break;
            case 'long_text':
            case 'short_text':
            case 'phone':
            case 'mail':
            case 'select':
                $resp['type'] = 'string';
                break;
        }
        if ($resp) {
            return $resp;
        }

        /** @var FieldTypeInterface $field */
        $field = $this->fieldTypeManager->create($id);
        if ($field) {
            if (1 === count($field->getProperties())) {
                return ['type' => 'string'];
            }
        }

        return ['$ref' => '#/components/schemas/form_type__'.$id];
    }

    /**
     * Get PUT request body.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return RequestBody
     */
    protected function replanPointRequestBody(FormManagerInterface $form): Model\RequestBody
    {
        return new Model\RequestBody(
            self::t('The replanning action of a @form', ['@form' => $form->getId()])->render(),
            new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                'type' => 'object',
                                'description' => '',
                                'properties' => [
                                    'form.community' => [
                                        'type' => 'array',
                                        'items' => [
                                            'type' => 'string',
                                            ],
                                        ],
                                    'form.wssystem' => [
                                        'type' => 'array',
                                        'items' => [
                                            'type' => 'string',
                                            ],
                                        ],
                                    'form.wsprovider' => [
                                        'type' => 'array',
                                        'items' => [
                                            'type' => 'string',
                                            ],
                                        ],
                                ],
                                'required' => ['form.community'],
                            ],
                        ),
                        null,
                        null,
                        new Model\Encoding('application/json')
                    ),
                ],
            ),
            true
        );
    }
}
