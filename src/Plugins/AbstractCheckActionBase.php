<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins;

use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Tools\CurrencyAmount;
use App\Tools\InquiryConditionalResolver;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\Reader;
use Exception;
use Psr\Log\LoggerInterface;

/**
 * Check Action base class.
 */
abstract class AbstractCheckActionBase
{
    use StringTranslationTrait;

    protected SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected FormRecord $inquiry;
    protected Reader $annotationReader;

    protected string $currencyMessageError = '';
    protected string $currency = '';
    // Only log currency unit error one time.
    protected bool $currencyErrorDetected = false;

    /**
     * Base constructor.
     *
     * @param Reader          $annotationReader
     * @param SessionService  $sessionService
     * @param LoggerInterface $inquiryFormLogger
     * @param FormRecord      $inquiry
     */
    public function __construct(Reader $annotationReader, SessionService $sessionService, LoggerInterface $inquiryFormLogger, FormRecord $inquiry)
    {
        $this->inquiry = $inquiry;
        $this->sessionService = $sessionService;
        $this->inquiryFormLogger = $inquiryFormLogger;
        $this->annotationReader = $annotationReader;
    }

    /**
     * Add action to activity log.
     *
     * @param array $context
     *
     * @throws \Exception
     */
    abstract protected function logResult(array $context = []): void;

    /**
     * Log message by diferency currencies.
     *
     * @param array $context
     *
     * @return void
     *
     * @throws \Exception
     */
    abstract protected function logByCurrency(array $context = []): void;

    /**
     * Try to read the Currency field and get the value.
     *
     * If the field don't have value return 0.0.
     * Validate that all queried fields have the same currency unit.
     *
     * @param CurrencyAmount|null $value
     *
     * @return float|bool
     */
    protected function getCurrencyAmount(?CurrencyAmount $value): float|bool
    {
        if (is_null($value)) {
            return 0.0;
        }

        if (empty($this->currency)) {
            $this->currency = $value->getId();
        } else {
            // Validate currency.
            if ($this->currency !== $value->getId()) {
                $this->logByCurrency();

                return false;
            }
        }

        return $value->amount;
    }

    /**
     * Log by currency message
     *
     * @param string $message
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function logByCurrencyMessage(string $message)
    {
        $this->currencyMessageError = $message;
    }

    /**
     * Is the field visible?
     *
     * @param FormRecord $form            Form where search the field.
     * @param string     $fieldName       Field to get visibility.
     * @param string     $parentFieldName If is a subform field, the parent field name.
     *
     * @return bool True if the field is visible.
     *
     * @throws \ReflectionException
     */
    protected function isVisibleField(FormRecord $form, string $fieldName, string $parentFieldName = ''): bool
    {
        $visibleResolver = new InquiryConditionalResolver($form);

        return $visibleResolver->isVisible($fieldName, $parentFieldName);
    }
}
