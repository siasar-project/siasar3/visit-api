<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;

/**
 * Base household base class.
 */
abstract class AbstractHouseholdCalcBase
{
    use StringTranslationTrait;

    protected SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected FormRecord $inquiry;
    protected Reader $annotationReader;
    protected int $householdCount;

    /**
     * Base constructor.
     *
     * @param Reader          $annotationReader
     * @param SessionService  $sessionService
     * @param LoggerInterface $inquiryFormLogger
     * @param FormRecord      $inquiry
     */
    public function __construct(Reader $annotationReader, SessionService $sessionService, LoggerInterface $inquiryFormLogger, FormRecord $inquiry)
    {
        $this->inquiry = $inquiry;
        $this->sessionService = $sessionService;
        $this->inquiryFormLogger = $inquiryFormLogger;
        $this->annotationReader = $annotationReader;
        $this->householdCount = 0;
    }

    /**
     * Execute this calc step.
     *
     * @param FormRecord $householdRecord
     */
    public function step(FormRecord $householdRecord)
    {
        // Count the total of households processed.
        $this->householdCount++;
    }

    /**
     * Final calc step.
     *
     * Required to operations like "medium"
     */
    public function finish()
    {
        // Normalize total to community size.
        $communitySize = $this->inquiry->{'field_total_households'};
        $acc = $this->inquiry->{$this->getAnnotation()->getCommunityField()};
        // $acc ------> $this->householdCount
        // x    ------> $communitySize
        $normalizedValue = ($acc * $communitySize) / $this->householdCount;
        $this->inquiry->{$this->getAnnotation()->getCommunityField()} = round($normalizedValue);
    }

    /**
     * Set community form field to zero to start the calc process.
     */
    public function init()
    {
        $this->inquiry->{$this->getAnnotation()->getCommunityField()} = 0;
    }

    /**
     * Get this plugin annotation.
     *
     * @return HouseholdCalc
     */
    public function getAnnotation(): HouseholdCalc
    {
        return $this->annotationReader
            ->getClassAnnotation(new \ReflectionClass(static::class), 'App\Annotations\HouseholdCalc');
    }
}
