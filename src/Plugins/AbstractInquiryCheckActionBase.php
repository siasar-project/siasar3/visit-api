<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins;

use App\Tools\ContextLogNormalizer;
use App\Tools\Json;
use Exception;

/**
 * Inquiry Check Action class.
 */
abstract class AbstractInquiryCheckActionBase extends AbstractCheckActionBase
{
    /**
     * Execute this checking.
     *
     * @return bool True if it has no errors or warnings.
     */
    abstract public function check(): bool;

    /**
     * @inheritdoc
     */
    protected function logResult(array $context = []): void
    {
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        $context['form_id'] = $this->inquiry->getForm()->getId();
        $context['record_id'] = $this->inquiry->getId();

        $annotation = $this->annotationReader
            ->getClassAnnotation(new \ReflectionClass(static::class), 'App\Annotations\InquiryCheckAction');

        // todo Binary IDs are missed in this conversion. If we do not do this conversion the log fails by JSON encoding.
        $nContext = ContextLogNormalizer::normalize($context);
        $nContext = Json::decode(Json::encode($nContext));
        switch (strtolower($annotation->getLevel())) {
            case 'error':
                $this->inquiryFormLogger->error(sprintf('[%s] %s', $annotation->getId(), self::t($annotation->getMessage())), $nContext);
                break;

            case 'warning':
                $this->inquiryFormLogger->warning(sprintf('[%s] %s', $annotation->getId(), self::t($annotation->getMessage())), $nContext);
                break;

            default:
                $this->inquiryFormLogger->info(sprintf('[%s] %s', $annotation->getId(), self::t($annotation->getMessage())), $nContext);
                break;
        }
    }

    /**
     * @inheritdoc
     */
    protected function logByCurrency(array $context = []): void
    {
        if ($this->currencyErrorDetected) {
            return;
        }
        $this->currencyErrorDetected = true;

        if (empty($this->currencyMessageError)) {
            throw new Exception('Class %s need define warning message with logByCurrencyMessage()', __CLASS__);
        }

        $message = $this->currencyMessageError;
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        $context['form_id'] = $this->inquiry->getForm()->getId();
        $context['record_id'] = $this->inquiry->getId();

        $annotation = $this->annotationReader
            ->getClassAnnotation(new \ReflectionClass(static::class), 'App\Annotations\InquiryCheckAction');

        // todo Binary IDs are missed in this conversion. If we do not do this conversion the log fails by JSON encoding.
        $nContext = ContextLogNormalizer::normalize($context);
        $nContext = Json::decode(Json::encode($nContext));
        switch (strtolower($annotation->getLevel())) {
            case 'error':
                $this->inquiryFormLogger->error(sprintf('[%s] %s', $annotation->getId(), self::t($message)), $nContext);
                break;

            case 'warning':
                $this->inquiryFormLogger->warning(sprintf('[%s] %s', $annotation->getId(), self::t($message)), $nContext);
                break;

            default:
                $this->inquiryFormLogger->info(sprintf('[%s] %s', $annotation->getId(), self::t($annotation->getMessage())), $nContext);
                break;
        }
    }

    /**
     * If $controlField get $dataField value, else get zero.
     *
     * @param string $controlField
     * @param string $dataField
     *
     * @return int
     */
    protected function getValueIfEnabled(string $controlField, string $dataField): int
    {
        if (empty($controlField)) {
            // Without control field.
            return $this->inquiry->{$dataField};
        }

        /** @var bool $control */
        $control = $this->inquiry->{$controlField};
        if ($control) {
            return $this->inquiry->{$dataField};
        }

        return 0;
    }

    /**
     * If $controlField get $dataField value, else get false.
     *
     * @param string $controlField
     * @param string $dataField
     *
     * @return bool
     */
    protected function getBoolIfEnabled(string $controlField, string $dataField): bool
    {
        if (empty($controlField)) {
            // Without control field.
            return $this->inquiry->{$dataField};
        }

        /** @var bool $control */
        $control = $this->inquiry->{$controlField};
        if ($control) {
            return $this->inquiry->{$dataField};
        }

        return false;
    }
}
