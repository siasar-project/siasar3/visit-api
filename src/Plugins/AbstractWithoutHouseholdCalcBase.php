<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins;

use App\Annotations\WithoutHouseholdCalc;
use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;

/**
 * Base without household base class.
 */
abstract class AbstractWithoutHouseholdCalcBase
{
    use StringTranslationTrait;

    protected SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected FormRecord $inquiry;
    protected Reader $annotationReader;

    /**
     * Base constructor.
     *
     * @param Reader          $annotationReader
     * @param SessionService  $sessionService
     * @param LoggerInterface $inquiryFormLogger
     * @param FormRecord      $inquiry
     */
    public function __construct(Reader $annotationReader, SessionService $sessionService, LoggerInterface $inquiryFormLogger, FormRecord $inquiry)
    {
        $this->inquiry = $inquiry;
        $this->sessionService = $sessionService;
        $this->inquiryFormLogger = $inquiryFormLogger;
        $this->annotationReader = $annotationReader;
    }

    /**
     * Set community form field to zero to start the calc process.
     */
    public function init()
    {
        $this->inquiry->{$this->getAnnotation()->getCommunityField()} = 0;
    }

    /**
     * Execute the calc.
     *
     * @return mixed
     */
    abstract public function execute();

    /**
     * Get this plugin annotation.
     *
     * @return WithoutHouseholdCalc
     */
    public function getAnnotation(): WithoutHouseholdCalc
    {
        return $this->annotationReader
            ->getClassAnnotation(new \ReflectionClass(static::class), 'App\Annotations\WithoutHouseholdCalc');
    }
}
