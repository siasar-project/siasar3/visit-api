<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 2.11 <=> field_n_hh_from_comm_ws_drink [(Suma respuestas HA2=1)* "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "B11",
 *     active = true,
 *     communityField = "field_n_hh_from_comm_ws_drink",
 * )
 */
class HouseholdCalcB11 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 2.11 <=> field_n_hh_from_comm_ws_drink [(Suma respuestas HA2=1)* "A5" / "0.4b"]
        if ($householdRecord->{'field_use_water_from_community_supply_drinking'}) {
            $this->inquiry->{'field_n_hh_from_comm_ws_drink'} += 1;
        }
    }
}
