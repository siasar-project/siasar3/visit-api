<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 2.13 <=> field_n_hh_from_comm_ws_not_by_problem [(Suma respuestas HA3=1)* "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "B13",
 *     active = true,
 *     communityField = "field_n_hh_from_comm_ws_not_by_problem",
 * )
 */
class HouseholdCalcB13 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 2.13 <=> field_n_hh_from_comm_ws_not_by_problem [(Suma respuestas HA3=1)* "A5" / "0.4b"]
        $opcs = $householdRecord->{'field_main_reasons_not_using'};
        if (is_array($opcs) && in_array('1', $opcs)) {
            $this->inquiry->{'field_n_hh_from_comm_ws_not_by_problem'} += 1;
        }
    }
}
