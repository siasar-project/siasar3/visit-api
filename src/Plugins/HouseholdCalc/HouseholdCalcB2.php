<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 2.20 <=> field_n_hh_from_comm_ws_not_carry [(Suma respuestas HA4=2 - False)* "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "B2",
 *     active = true,
 *     communityField = "field_households_store_drinking_water",
 * )
 */
class HouseholdCalcB2 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 2.2 <=> field_households_store_drinking_water [(Suma respuestas HA5=1 - True)* "A5" / "0.4b"]
        if ($householdRecord->{'field_drinking_water_safely_stored'}) {
            $this->inquiry->{'field_households_store_drinking_water'} += 1;
        }
    }
}
