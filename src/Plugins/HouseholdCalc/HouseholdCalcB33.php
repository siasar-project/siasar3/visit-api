<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 2.33 <=> field_n_hh_from_comm_ws_tanker_truck [(Suma respuestas HA7=11)* "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "B33",
 *     active = true,
 *     communityField = "field_n_hh_from_comm_ws_tanker_truck",
 * )
 */
class HouseholdCalcB33 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 2.33 <=> field_n_hh_from_comm_ws_tanker_truck [(Suma respuestas HA7=11)* "A5" / "0.4b"]
        if ('11' === $householdRecord->{'field_main_source_drinking_water'}) {
            $this->inquiry->{'field_n_hh_from_comm_ws_tanker_truck'} += 1;
        }
    }
}
