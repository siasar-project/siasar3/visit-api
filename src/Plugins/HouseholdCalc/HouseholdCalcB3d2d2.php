<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * 2.3.2.2 <=> field_avg_volume_rainwater_collection_tanks [Mediana de respuestas de HA6.2]
 *
 * @HouseholdCalc(
 *     id = "B3.2.2",
 *     active = true,
 *     communityField = "field_avg_volume_rainwater_collection_tanks",
 * )
 */
class HouseholdCalcB3d2d2 extends AbstractHouseholdCalcBase
{
    protected static array $values = [];

    /**
     * @inheritDoc
     */
    public function init()
    {
        $value = new VolumeMeasurement(0, 'cubic metre');
        $this->inquiry->{$this->getAnnotation()->getCommunityField()} = $value;
    }

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        // 2.3.2.2 <=> field_avg_volume_rainwater_collection_tanks [Mediana de respuestas de HA6.2]
        /** @var VolumeMeasurement $value */
        $value = $householdRecord->{'field_volume_rainwater_collection'};
        if ($value) {
            $value->setUnit('cubic metre');
            static::$values[] = $value->getValue();
            $this->inquiry->{'field_avg_volume_rainwater_collection_tanks_exists'} = true;
            $this->inquiry->{'field_households_with_rainwater_collection'} = true;
        }
    }

    /**
     * @inheritDoc
     */
    public function finish()
    {
        // Optional final step.
        if (count(static::$values) === 0) {
            $value = new VolumeMeasurement(0, 'cubic metre');
            $this->inquiry->{'field_avg_volume_rainwater_collection_tanks'} = $value;

            return;
        }
        if (count(static::$values) === 1) {
            $value = new VolumeMeasurement(static::$values[0], 'cubic metre');
            $this->inquiry->{'field_avg_volume_rainwater_collection_tanks'} = $value;

            return;
        }

        sort(static::$values, SORT_NUMERIC);
        // @see https://www.disfrutalasmatematicas.com/datos/mediana.html
        if ((count(static::$values) % 2) === 0) {
            // odd values count.
            $midle = round(count(static::$values) / 2, 0, PHP_ROUND_HALF_UP);
            $middleIndex = $midle - 1;
            $medium = (static::$values[$middleIndex] + static::$values[$middleIndex + 1]) / 2;
            $value = new VolumeMeasurement($medium, 'cubic metre');
            $this->inquiry->{'field_avg_volume_rainwater_collection_tanks'} = $value;
        } else {
            // even values count.
            $middleIndex = round(count(static::$values) / 2, 0, PHP_ROUND_HALF_UP) - 1;
            $value = new VolumeMeasurement(static::$values[$middleIndex], 'cubic metre');
            $this->inquiry->{'field_avg_volume_rainwater_collection_tanks'} = $value;
        }
    }
}
