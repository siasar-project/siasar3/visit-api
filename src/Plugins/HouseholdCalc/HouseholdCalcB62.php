<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 2.62 <=> field_n_hh_average_min_collection [Mediana de los valores respondidos de HA10.2b (1.10.1)]
 *
 * @HouseholdCalc(
 *     id = "B62",
 *     active = true,
 *     communityField = "field_n_hh_average_min_collection",
 * )
 */
class HouseholdCalcB62 extends AbstractHouseholdCalcBase
{
    protected static array $values = [];

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->inquiry->{$this->getAnnotation()->getCommunityField()} = 0;
    }

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        // 2.62 <=> field_n_hh_average_min_collection [Mediana de los valores respondidos de HA10.2b (1.10.1)]
        /** @var int $value */
        $value = $householdRecord->{'field_time_take_get_water_minutes'};
        if (!empty($value)) {
            static::$values[] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function finish()
    {
        // Optional final step.
        if (count(static::$values) === 0) {
            $this->inquiry->{'field_n_hh_average_min_collection'} = 0;

            return;
        }
        if (count(static::$values) === 1) {
            $this->inquiry->{'field_n_hh_average_min_collection'} = static::$values[0];

            return;
        }

        sort(static::$values, SORT_NUMERIC);
        // @see https://www.disfrutalasmatematicas.com/datos/mediana.html
        if ((count(static::$values) % 2) === 0) {
            // odd values count.
            $midle = round(count(static::$values) / 2, 0, PHP_ROUND_HALF_UP);
            $middleIndex = $midle - 1;
            $medium = (static::$values[$middleIndex] + static::$values[$middleIndex + 1]) / 2;
            $this->inquiry->{'field_n_hh_average_min_collection'} = $medium;
        } else {
            // even values count.
            $middleIndex = round(count(static::$values) / 2, 0, PHP_ROUND_HALF_UP) - 1;
            $this->inquiry->{'field_n_hh_average_min_collection'} = static::$values[$middleIndex];
        }
    }
}
