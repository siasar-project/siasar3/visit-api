<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 3.1.1.5 <=> field_unknown_number [(Suma respuestas HS1=5)* "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "C1.1.5",
 *     active = true,
 *     communityField = "field_unknown_number",
 * )
 */
class HouseholdCalcC1d1d5 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 3.1.1.5 <=> field_unknown_number [(Suma respuestas HS1=5)* "A5" / "0.4b"]
        if ('5' === $householdRecord->{'field_toilet_facility'}) {
            $this->inquiry->{'field_unknown_number'} += 1;
            $this->inquiry->{'field_flush_toilets'} = true;
        }
    }
}
