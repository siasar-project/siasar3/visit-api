<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 3.1.3.2 <=> field_twinpit_without_slab_number [(Suma respuestas HS1=9)* "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "C1.3.2",
 *     active = true,
 *     communityField = "field_twinpit_without_slab_number",
 * )
 */
class HouseholdCalcC1d3d2 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 3.1.3.2 <=> field_twinpit_without_slab_number [(Suma respuestas HS1=9)* "A5" / "0.4b"]
        if ('9' === $householdRecord->{'field_toilet_facility'}) {
            $this->inquiry->{'field_twinpit_without_slab_number'} += 1;
            $this->inquiry->{'field_composting_toilets'} = true;
        }
    }
}
