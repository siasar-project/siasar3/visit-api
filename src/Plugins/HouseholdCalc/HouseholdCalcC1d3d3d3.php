<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 3.1.3.3.3 <=> field_n_hh_other_compost_members [Si HS1=10; (Suma respuestas HS3=1) * "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "C1.3.3.3",
 *     active = true,
 *     communityField = "field_n_hh_other_compost_members",
 * )
 */
class HouseholdCalcC1d3d3d3 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 3.1.3.3.3 <=> field_n_hh_other_compost_members [Si HS1=10; (Suma respuestas HS3=1) * "A5" / "0.4b"]
        if ('10' === $householdRecord->{'field_toilet_facility'}) {
            if ($householdRecord->{'field_members_use_sanitation_facility'}) {
                $this->inquiry->{'field_n_hh_other_compost_members'} += 1;
                $this->inquiry->{'field_composting_toilets'} = true;
            }
        }
    }
}
