<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 3.52 <=> field_n_hh_provider_emptied_pit_treatment [Si HS1=4; (Suma respuestas HS4=1) * "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "C52",
 *     active = true,
 *     communityField = "field_n_hh_provider_emptied_pit_treatment",
 * )
 */
class HouseholdCalcC52 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 3.52 <=> field_n_hh_provider_emptied_pit_treatment [(Suma respuestas HS6=1) * "A5" / "0.4b"]
        if ('1' === $householdRecord->{'field_were_contents_emptied'}) {
            $this->inquiry->{'field_n_hh_provider_emptied_pit_treatment'} += 1;
        }
    }
}
