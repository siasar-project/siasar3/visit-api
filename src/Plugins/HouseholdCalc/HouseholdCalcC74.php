<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 3.74 <=> field_n_hh_member_no_use_by_other [Si HS1=4; (Suma respuestas HS4=1) * "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "C74",
 *     active = true,
 *     communityField = "field_n_hh_member_no_use_by_other",
 * )
 */
class HouseholdCalcC74 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 3.74 <=> field_n_hh_member_no_use_by_other [(Suma respuestas HS8=99) * "A5" / "0.4b"]
        $opcs = $householdRecord->{'field_why_unable_access_toilet'};
        if (is_array($opcs) && in_array('99', $opcs)) {
            $this->inquiry->{'field_n_hh_member_no_use_by_other'} += 1;
        }
    }
}
