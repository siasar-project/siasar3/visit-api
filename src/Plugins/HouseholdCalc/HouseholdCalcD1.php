<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 4.1 <=> field_with_basic_handwashing [ HH1.1 + HH1.2 + HH1.3]
 *
 * @HouseholdCalc(
 *     id = "D1",
 *     active = true,
 *     communityField = "field_with_basic_handwashing",
 * )
 */
class HouseholdCalcD1 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 4.1 <=> field_with_basic_handwashing [ HH1.1 + HH1.2 + HH1.3]
        $fieldShowWashHands = $householdRecord->{'field_show_wash_hands'};
        if (in_array($fieldShowWashHands, ['1', '2', '3'])) {
            $this->inquiry->{'field_with_basic_handwashing'} += 1;
        }
    }
}
