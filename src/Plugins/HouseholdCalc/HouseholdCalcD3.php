<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 4.3 <=> field_with_water [Suma del número de encuestas que cumplen la siguiente combinación (HH1= 1, 2 o 3 Y HH2=1 y HH3=2)]
 *
 * @HouseholdCalc(
 *     id = "D3",
 *     active = true,
 *     communityField = "field_with_water",
 * )
 */
class HouseholdCalcD3 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 4.3 <=> field_with_water [Suma del número de encuestas que cumplen la
        // siguiente combinación (HH1= 1, 2 o 3 Y HH2=1 y HH3=2)]
        $hh1 = $householdRecord->{'field_show_wash_hands'};
        $hh2 = $householdRecord->{'field_observed_water_at_place'};
        $hh3 = $householdRecord->{'field_observed_soap_at_place'};
        if (in_array($hh1, ['1', '2', '3']) &&
            '1' === $hh2 &&
            '2' === $hh3
        ) {
            $this->inquiry->{'field_with_water'} += 1;
            $this->inquiry->{'field_with_water_exists'} = true;
        }
    }
}
