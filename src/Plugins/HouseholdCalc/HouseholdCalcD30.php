<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\HouseholdCalc;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;

/**
 * 4.30 <=> field_n_hh_menstrual_other [(Suma respuestas HH6=99) * "A5" / "0.4b"]
 *
 * @HouseholdCalc(
 *     id = "D30",
 *     active = true,
 *     communityField = "field_n_hh_menstrual_other",
 * )
 */
class HouseholdCalcD30 extends AbstractHouseholdCalcBase
{

    /**
     * @inheritDoc
     */
    public function step(FormRecord $householdRecord)
    {
        parent::step($householdRecord);

        // 4.30 <=> field_n_hh_menstrual_other [(Suma respuestas HH6=99) * "A5" / "0.4b"]
        if ('99' === $householdRecord->{'field_last_period_hygiene_material_mainly'}) {
            $this->inquiry->{'field_n_hh_menstrual_other'} += 1;
        }
    }
}
