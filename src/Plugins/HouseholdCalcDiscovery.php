<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins;

use App\Annotations\HouseholdCalc;
use App\Forms\FormRecord;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Household calc operations discovery service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class HouseholdCalcDiscovery
{
    protected string $namespace;
    protected string $directory;
    protected Reader $annotationReader;
    /**
     * The Kernel root directory.
     */
    protected string $rootDir;
    protected array $householdCalcs = [];

    /**
     * Household calc operations discovery constructor.
     *
     * @param string $namespace        Annotation name space.
     * @param string $directory        Household calc operation definitions folder.
     * @param string $rootDir          Application root folder.
     * @param Reader $annotationReader Annotation reader.
     */
    public function __construct(string $namespace, string $directory, string $rootDir, Reader $annotationReader)
    {
        $this->namespace = $namespace;
        $this->annotationReader = $annotationReader;
        $this->directory = $directory;
        $this->rootDir = $rootDir;
    }

    /**
     * Returns all the workers
     *
     * @return array [['class' => string, 'annotation' => HouseholdCalc]]
     *
     * @throws \ReflectionException
     */
    public function getHouseholdCalcs(): array
    {
        if (!$this->householdCalcs) {
            $this->discoverHouseholdCalcs();
        }

        $resp = [];
        foreach ($this->householdCalcs as $key => $action) {
            $resp[$key] = $action;
        }

        return $resp;
    }

    /**
     * Discovers Household calc operations.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    protected function discoverHouseholdCalcs(): void
    {
        // Household calc operations are in the types folder in this discoverer.
        $path = __DIR__.'/HouseholdCalc/';
        $finder = new Finder();
        $finder->files()->in($path);

        /**
         * HouseholdCalc definition file.
         *
         * @var SplFileInfo $file
         */
        foreach ($finder as $file) {
            $class = $this->namespace.'\\'.$file->getBasename('.php');
            $annotation = $this->annotationReader
                ->getClassAnnotation(new \ReflectionClass($class), 'App\Annotations\HouseholdCalc');
            if (!$annotation) {
                continue;
            }

            if ($annotation->isActive()) {
                /**
                 * Annotation setting.
                 *
                 * @var HouseholdCalc $annotation
                 */
                $this->householdCalcs[$annotation->getId()] = [
                    'class' => $class,
                    'annotation' => $annotation,
                ];
            }
        }
    }
}
