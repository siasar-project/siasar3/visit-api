<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ACOMA04",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "Although there are areas at elevations below sea level, it is rare to find areas below sea level.",
 *     message = "The altitude is less than 0 meters. Check if this value is correct.",
 * )
 */
class InquiryCheckACOMA04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //COM_A1.3 < 0
        /** @var LengthMeasurement $condition */
        $condition = $this->inquiry->{'field_location_alt'};
        $condition->setUnit('metre');
        if (0 > $condition->getValue()) {
            $this->logResult();
        }

        return true;
    }
}
