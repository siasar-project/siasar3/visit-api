<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMA07",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "The number of dwellings is high for the rural average.",
 *     message = "The value is high for the usual average in the rural context. Check if it is correct.",
 * )
 */
class InquiryCheckACOMA07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //COM_A5 > 2000
        $condition = $this->inquiry->{'field_total_households'};
        if (2000 < $condition) {
            $this->logResult();
        }

        return true;
    }
}
