<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMA10",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "The ratio between population and dwellings is anomalous.",
 *     message = "According to the digitized data, less than 2 persons per dwelling live in the dwellings of this community. Check if this value is correct.",
 * )
 */
class InquiryCheckACOMA10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //COM_A4 / COM_A5 < 2
        $conditionAd4 = $this->inquiry->{'field_total_population'};
        $conditionAd5 = $this->inquiry->{'field_total_households'};
        if (0 < $conditionAd4 && 0 < $conditionAd5) {
            if (2 > ($conditionAd4/$conditionAd5)) {
                $this->logResult();
            }
        }

        return true;
    }
}
