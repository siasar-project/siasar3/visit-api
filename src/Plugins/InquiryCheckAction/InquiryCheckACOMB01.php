<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMB01",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "The volume is somewhat high to be a home tank",
 *     message = "The average volume of household rainwater tanks is somewhat high. Check if this value is correct.",
 * )
 */
class InquiryCheckACOMB01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //Nota: litros o equivalente en cualquier otra unidad
        // COM_B3.2.2 => 2.3.2.2.2 - field_avg_volume_rainwater_collection_tanks
        // COM_B3.2.2 > 10000 l
        /** @var VolumeMeasurement $volume */
        $volume = $this->inquiry->{'field_avg_volume_rainwater_collection_tanks'};
        $volume->setUnit('litre');
        if ($volume->getValue() > 10000) {
            $this->logResult();
        }

        return true;
    }
}
