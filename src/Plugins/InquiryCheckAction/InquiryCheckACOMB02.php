<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "ACOMB02",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "It is rare that the rainwater tank is not used at least one month a year.",
 *     message = "Rainwater tanks are not being used any month of the year. Check if this data is correct.",
 * )
 */
class InquiryCheckACOMB02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // comprobar primero si Existen tanques de agua lluvia en la comunidad:
        // 2.3.1 ¿Existen hogares con tanques de agua lluvia particular?
        // Si la respuesta es SI entonces se aplica la regla sino no.
        /** @var bool $b3d1 */
        $b3d1 = $this->inquiry->{'field_households_with_rainwater_collection'};

        if ($b3d1) {
            // COM_B3.2.3 => 2.3.2.3.2 - field_house_water_source_months_from_rainwater
            // COM_B3.2.3 = 0
            $b3d2d3d2 = $this->inquiry->{'field_house_water_source_months_from_rainwater'};

            // Is the field visible?
            $visibleResolver = new InquiryConditionalResolver($this->inquiry);
            $visible3d2d2d2 = $visibleResolver->isVisible('field_house_water_source_months_from_rainwater', '');

            if ($visible3d2d2d2 && 0 === $b3d2d3d2) {
                $this->logResult();
            }
        }

        return true;
    }
}
