<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMC04",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "If sanitation facilities are shared, some dwellings should not have sanitation.",
 *     message = "It has been indicated that there are dwellings that share sanitation facilities, however, all dwellings have sanitation facilities so there would be no need to share. Check if the data in group 3.1 are correct.",
 * )
 */
class InquiryCheckACOMC04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Código sectorial:
        // COM_C1.11.2 > 0 y COM_C1.12.1 = 0
        //
        // Codito IT:
        // COM 3.1.4.1.2 > 0 y COM 3.1.5.1 = 0
        $f3d1d4d1d2 = $this->inquiry->{'field_n_hh_dwellings_nmembers'};
        if ($f3d1d4d1d2 > 0) {
            $f3d1d5d1 = $this->getValueIfEnabled('field_no_facility', 'field_no_facility_number');
            if (0 === $f3d1d5d1) {
                $this->logResult();
            }
        }

        return true;
    }
}
