<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMC06",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "There is no water system, but there is sanitation with hydraulically flushed toilets.",
 *     message = "It has been indicated that the community does not have a water system, but does have sanitation facilities that require hydraulic hauling.",
 * )
 */
class InquiryCheckACOMC06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Código sectorial:
        // (COM_B1 = COM_A5) y
        // COM_C1.1.1 > 0 o COM_C1.2.1 > 0 o COM_C1.3.1 > 0 o COM_C1.4.1 > 0 o COM_C1.5.1 > 0
        //
        // Código IT:
        // (COM 2.1 = COM 1.5) y
        // COM 3.1.1.1 > 0 o COM 3.1.1.2 > 0 o COM 3.1.1.3 > 0 o COM 3.1.1.4 > 0 o COM 3.1.1.5 > 0
        $f2d1 = $this->inquiry->{'field_households_without_water_supply_system'};
        $f1d5 = $this->inquiry->{'field_total_households'};
        if ($f2d1 === $f1d5) {
            $f3d1d1d1 = $this->getValueIfEnabled('field_flush_toilets', 'field_sewer_connection_number');
            $f3d1d1d2 = $this->getValueIfEnabled('field_flush_toilets', 'field_septic_tank_number');
            $f3d1d1d3 = $this->getValueIfEnabled('field_flush_toilets', 'field_pit_latrine_number');
            $f3d1d1d4 = $this->getValueIfEnabled('field_flush_toilets', 'field_without_containment_number');
            $f3d1d1d5 = $this->getValueIfEnabled('field_flush_toilets', 'field_unknown_number');
            if ($f3d1d1d1 > 0 || $f3d1d1d2 > 0 || $f3d1d1d3 > 0 || $f3d1d1d4 > 0 || $f3d1d1d5 > 0) {
                $this->logResult();
            }
        }

        return true;
    }
}
