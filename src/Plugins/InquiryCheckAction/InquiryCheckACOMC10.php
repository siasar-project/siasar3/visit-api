<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMC10",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "This type of sanitation does not usually require a service provider.",
 *     message = "The type of sanitation facilities in the community usually do not have a sanitation service provider associated with them. Review this data.",
 * )
 */
class InquiryCheckACOMC10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // (COM_C1.7.1 = COM_A5) y C2 ≠ 4
        $c1d2d2 = $this->inquiry->{'field_pit_latrine_without_slab_number'};
        $a5 = $this->inquiry->{'field_total_households'};
        $c2 = $this->inquiry->{'field_formal_service_provider_offers'};

        if ($c1d2d2 === $a5 && '4' !== $c2) {
            $this->logResult();
        }

        return true;
    }
}
