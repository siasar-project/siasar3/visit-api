<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMC11",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "If sanitation facilities are shared, some household should not have sanitation.",
 *     message = "It has been indicated that there are homes that share sanitation facilities, however, all the homes have a sanitation facility, so they would not need to share. Check if the data in group 3.1 is correct.",
 * )
 */
class InquiryCheckACOMC11 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Código sectorial:
        // COM_C1.13.2 > 0 y COM_C1.12.1 = 0
        //
        // Codito IT:
        // COM 3.1.1.6 o COM 3.1.2.3 o COM 3.1.3.4 o COM 3.1.4.2 y COM 3.1.5.1 = 0
        $f3d1d5d1 = $this->getValueIfEnabled('field_no_facility', 'field_no_facility_number');
        if (0 === $f3d1d5d1) {
            $f3d1d1d6 = $this->getBoolIfEnabled('field_flush_toilets', 'field_flush_share_facilities_multi_households');
            $f3d1d2d3 = $this->getBoolIfEnabled('field_drypit_latrines', 'field_drypit_share_facilities_multi_households');
            $f3d1d3d4 = $this->getBoolIfEnabled('field_composting_toilets', 'field_composting_share_facilities_multi_households');
            $f3d1d4d2 = $this->getBoolIfEnabled('field_bucket', 'field_bucket_share_facilities_multi_households');
            if ($f3d1d1d6 || $f3d1d2d3 || $f3d1d3d4 || $f3d1d4d2) {
                $this->logResult();
            }
        }

        return true;
    }
}
