<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMD01",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "If there is no sanitation facility in the community it is unusual to have hand washing facilities.",
 *     message = "It has been indicated that there is no sanitation facility in the community, but there are hand washing facilities. Check if this is correct.",
 * )
 */
class InquiryCheckACOMD01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // COM_D1 > 0 y (COM_C1.12.1 = COM_A5)
        $d1 = $this->inquiry->{'field_with_basic_handwashing'};
        $a5 = $this->inquiry->{'field_total_households'};
        // COM_C1.12.1 --> 3.1.5.1
        $c1d5d1 = $this->inquiry->{'field_no_facility_number'};

        if (0 < $d1 && $c1d5d1 === $a5) {
            $this->logResult();
        }

        return true;
    }
}
