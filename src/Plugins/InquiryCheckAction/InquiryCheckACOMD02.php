<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACOMD02",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "There is no water system in the community, but there are hand-washing facilities with water.",
 *     message = "It has been indicated that there is no water system serving the community, but there are hand washing facilities with water.",
 * )
 */
class InquiryCheckACOMD02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // (COM_B1 = COM_A5) y COM_D2 > 0 o COM_D3 > 0
        $a5 = $this->inquiry->{'field_total_households'};
        $b1 = $this->inquiry->{'field_households_without_water_supply_system'};
        $d2d2 = $this->inquiry->{'field_with_water_and_soap'};
        $d3d2 = $this->inquiry->{'field_with_water'};

        if ($b1 === $a5) {
            if (0 < $d2d2 || 0 < $d3d2) {
                $this->logResult();
            }
        }

        return true;
    }
}
