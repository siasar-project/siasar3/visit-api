<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACSAA01",
 *     active = true,
 *     level = "warning",
 *     form = "form.health.care",
 *     observation = "If the coordinate is zero, the point is badly located on the map.",
 *     message = "The coordinate is zero, check if this position is correct.",
 * )
 */
class InquiryCheckACSAA01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // CSA_A1.1 = 0
        if (0.0 === $this->inquiry->{'field_location_lat'}) {
            $this->logResult();
        }

        return true;
    }
}
