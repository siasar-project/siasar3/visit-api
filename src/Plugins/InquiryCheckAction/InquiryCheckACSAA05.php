<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastián Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACSAA05",
 *     active = true,
 *     level = "warning",
 *     form = "form.health.care",
 *     observation = "It is rare that there are no female personnel.",
 *     message = "No female staff has been indicated at this center. Check if this is correct.",
 * )
 */
class InquiryCheckACSAA05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_A5.1 = 0
        //CSA_A5.1 = field_female_employees

        $femaleEmployees = $this->inquiry->{'field_female_employees'};
        if (0 === $femaleEmployees) {
            $this->logResult();
        }

        return true;
    }
}
