<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACSAC05",
 *     active = true,
 *     level = "warning",
 *     form = "form.health.care",
 *     observation = "There are no facilities for female students.",
 *     message = "The facility has female patients, but has not been indicated as having any sanitation facilities reserved for women. Check if this is correct.",
 * )
 */
class InquiryCheckACSAC05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // CSA_A6.1 > 0 y CSA_C3.1.1 = 0
        $a6d1 = $this->inquiry->{'field_female_patients_per_day'};
        $c3d1d1 = $this->inquiry->{'field_total_toilet_women'};

        if (0 < $a6d1 && 0 === $c3d1d1) {
            $this->logResult();
        }

        return true;
    }
}
