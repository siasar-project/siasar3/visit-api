<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ACSAC06",
 *     active = true,
 *     level = "warning",
 *     form = "form.health.care",
 *     observation = "There are no facilities for male students.",
 *     message = "The facility has male patients, but has not indicated that it has any sanitation facilities reserved for men. Check if this is correct.",
 * )
 */
class InquiryCheckACSAC06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // CSA_A6.2 > 0 y CSA_C3.2.1 = 0
        $a6d2 = $this->inquiry->{'field_male_patients_per_day'};
        $c3d2d1 = $this->inquiry->{'field_total_toilet_men'};

        if (0 < $a6d2 && 0 === $c3d2d1) {
            $this->logResult();
        }

        return true;
    }
}
