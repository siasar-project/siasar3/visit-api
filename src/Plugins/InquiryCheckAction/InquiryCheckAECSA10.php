<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "AECSA10",
 *     active = true,
 *     level = "warning",
 *     form = "form.school",
 *     observation = "The number of staff seems excessive.",
 *     message = "The number of male staff is high (more than 50 people). Check if this is correct.",
 * )
 */
class InquiryCheckAECSA10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // ECS_A5.2 > 50
        if (50 < $this->inquiry->{'field_staff_total_number_of_men'}) {
            $this->logResult();
        }

        return true;
    }
}
