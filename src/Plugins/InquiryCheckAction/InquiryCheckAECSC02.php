<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "AECSC02",
 *     active = true,
 *     level = "warning",
 *     form = "form.school",
 *     observation = "The number of facilities is excessive.",
 *     message = "The number of installations of this type is quite high. Check if this number is correct.",
 * )
 */
class InquiryCheckAECSC02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // ECS_C3.2.1 > 25
        if (25 < $this->inquiry->{'field_boys_only_toilets_total'}) {
            $this->logResult();
        }

        return true;
    }
}
