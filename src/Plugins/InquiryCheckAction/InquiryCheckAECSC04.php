<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastián Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "AECSC04",
 *     active = true,
 *     level = "warning",
 *     form = "form.school",
 *     observation = "It is unusual to have flush toilets in a facility where there is no water system.",
 *     message = "It has been indicated that the center has sanitation facilities with hydraulic flushing, but however the center does not have a water system. Check if this is correct.",
 * )
 */
class InquiryCheckAECSC04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //  ECS_A6.1 > 0 and ECS_C3.1.1 = 0

        $havewaterSupplySystem = $this->inquiry->{'field_have_water_supply_system'};
        $typeToilets = $this->inquiry->{'field_type_toilets'};

        if ("3" === $havewaterSupplySystem && "1" === $typeToilets) {
            $this->logResult();
        }

        return true;
    }
}
