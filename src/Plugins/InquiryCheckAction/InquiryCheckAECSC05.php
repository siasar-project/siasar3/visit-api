<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastián Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "AECSC05",
 *     active = true,
 *     level = "warning",
 *     form = "form.school",
 *     observation = "No facilities for female students.",
 *     message = "The center has female students, but has not indicated that it has any sanitation facilities reserved for female students. Check if this is correct.",
 * )
 */
class InquiryCheckAECSC05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //  ECS_A6.1 > 0 y  ECS_C3.1.1 = 0
        //A6.1 = field_student_total_number_of_female INT
        //C3.1.1 = field_girls_only_toilets_total INT
        $studentTotalNumberOfFemale = $this->inquiry->{'field_student_total_number_of_female'};
        $girlsOnlyToiletsTotal = $this->inquiry->{'field_girls_only_toilets_total'};

        if (0 < $studentTotalNumberOfFemale && 0 === $girlsOnlyToiletsTotal) {
            $this->logResult();
        }

        return true;
    }
}
