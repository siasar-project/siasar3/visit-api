<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "AECSC07",
 *     active = true,
 *     level = "warning",
 *     form = "form.school",
 *     observation = "It is strange that it has been indicated that the facilities are clean, but instead there are none usable.",
 *     message = "It has been indicated that there is no usable sanitation facility, but nevertheless the facilities are clean. Check if this is correct.",
 * )
 */
class InquiryCheckAECSC07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $c1 = $this->inquiry->{'field_school_have_toilets'};

        if ($c1) {
            // ECS_C7 ≠ 3 y (ECS_C3.1.2 + ECS_C3.2.2 + ECS_C3.3.2) = 0
            $c7 = $this->inquiry->{'field_how_clean_student_toilets'};
            $c3d1d2 = $this->inquiry->{'field_girls_only_toilets_usable'};
            $c3d2d2 = $this->inquiry->{'field_boys_only_toilets_usable'};
            $c3d3d2 = $this->inquiry->{'field_common_use_toilets_usable'};

            if ('3' !== $c7 && 0 === ($c3d1d2 + $c3d2d2 + $c3d3d2)) {
                $this->logResult();
            }
        }

        return true;
    }
}
