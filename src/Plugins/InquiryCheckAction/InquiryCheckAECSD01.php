<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "AECSD01",
 *     active = true,
 *     level = "warning",
 *     form = "form.school",
 *     observation = "It is unusual to have water for hand washing in a facility where there is no water system.",
 *     message = "It has been indicated that the hand washing facilities have water, yet the center does not have a water system. Check if this is correct.",
 * )
 */
class InquiryCheckAECSD01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // ECS_B1 = 3 y (ECS_D2 = 1 o ECS_D2 = 2)
        $b1 = $this->inquiry->{'field_have_water_supply_system'};
        $d2 = $this->inquiry->{'field_handwashing_facilities_soap_water'};

        if ('3' === $b1 && ('1' === $d2 || '2' === $d2)) {
            $this->logResult();
        }

        return true;
    }
}
