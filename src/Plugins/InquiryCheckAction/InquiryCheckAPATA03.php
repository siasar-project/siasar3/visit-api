<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "APATA03",
 *     active = true,
 *     level = "warning",
 *     form = "form.tap",
 *     observation = "Only in Andean areas can there be altitudes of more than 5000 meters, and even then, there are very few points at those altitudes.",
 *     message = "The altitude is higher than 5000 meters. Check if this value is correct.",
 * )
 */
class InquiryCheckAPATA03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PAT_A1.3 > 5000
        /** @var LengthMeasurement $tap1d1d3 */
        $tap1d1d3 = $this->inquiry->{'field_location_alt'};
        $tap1d1d3->setUnit('metre');

        if (5000 < $tap1d1d3->getValue()) {
            $this->logResult();
        }

        return true;
    }
}
