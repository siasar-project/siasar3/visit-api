<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Entity\GeographicalScope;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APATA05",
 *     active = true,
 *     level = "warning",
 *     form = "form.tap",
 *     observation = "The number of providers served is high and the TAP is not national in scope.",
 *     message = "The TAP is serving a high number of service providers (over 250). Review if this is correct.",
 * )
 */
class InquiryCheckAPATA05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PAT_A6 ≠ 1 y PAT_A7 > 250
        /** @var GeographicalScope $tap1d6 */
        $tap1d6 = $this->inquiry->{'field_geographical_intervention_scope'};
        $tap1d7 = $this->inquiry->{'field_number_tap_should_assist'};

        if (!$tap1d6) {
            return true;
        }

        if ('1' !== $tap1d6->getKeyIndex() && 250 < $tap1d7) {
            $this->logResult();
        }

        return true;
    }
}
