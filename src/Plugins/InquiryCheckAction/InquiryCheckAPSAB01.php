<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "APSAB01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "There should be almost no community providers over 50 years old.",
 *     message = "The provider has been incorporated for more than 50 years. Although this is possible, check if this value is correct.",
 * )
 */
class InquiryCheckAPSAB01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_B1.1 < 1972
        $condition = $this->inquiry->{'field_constitution_date'};
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visibleB1d1 = $visibleResolver->isVisible('field_constitution_date', '');
        if ($visibleB1d1) {
            if (1972 > $condition['year']) {
                $this->logResult();
            }
        }

        return true;
    }
}
