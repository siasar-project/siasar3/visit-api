<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAB02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that a private provider is not legalized.",
 *     message = "The provider is type 4 (private operator) but is not legalized. Check if this is correct.",
 * )
 */
class InquiryCheckAPSAB02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_B1.2 ≠ 1 y PSA_A4 = 4
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        $wsp2d1d2 = $this->inquiry->{'field_provider_legal_status'};

        if ('4' === $wsp1d4 && '1' !== $wsp2d1d2) {
            $this->logResult();
        }

        return true;
    }
}
