<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;
use DateTime;

/**
 * @InquiryCheckAction(
 *     id = "APSAB03",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The date of the last election of board members is more than 5 years ago.",
 *     message = "The date of election of board members is more than 5 years ago. Check if this is correct or if a more current date is available.",
 * )
 */
class InquiryCheckAPSAB03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_0.1 – PSA_B2.1 > 5 años
        /** @var \DateTime $date */
        $date = $this->inquiry->{'field_questionnaire_date'};
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visibleB2d1 = $visibleResolver->isVisible('field_last_election_committee_date', '');
        if ($visibleB2d1) {
            $b2d1 = $this->inquiry->{'field_last_election_committee_date'};
            $electionDate = \DateTime::createFromFormat('Y-m-d', $b2d1['year'].'-'.$b2d1['month'].'-'.'1');
            if ($electionDate) {
                $interval = $electionDate->diff($date);
                $yearsDiff = $interval->y;
                $monthsDiff = $interval->m;
                $daysDiff = $interval->d;
                $totalMonthsDiff = ($yearsDiff * 12) + $monthsDiff;

                if (60 < $totalMonthsDiff || (60 === $totalMonthsDiff && 0 < $daysDiff)) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
