<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "APSAB04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It seems rare for the WSP to hold more than one meeting a week.",
 *     message = "The number of board meetings in the last year is more than one per week. Check if this value is correct.",
 * )
 */
class InquiryCheckAPSAB04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_B2.3 > 54
        $wsp2d2d3 = $this->inquiry->{'field_board_meets_last_year'};
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visible2d2d3 = $visibleResolver->isVisible('field_board_meets_last_year', '');
        if (!$visible2d2d3) {
            return true;
        }
        if (54 < $wsp2d2d3) {
            $this->logResult();
        }

        return true;
    }
}
