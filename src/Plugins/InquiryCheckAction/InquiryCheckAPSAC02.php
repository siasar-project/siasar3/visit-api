<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "APSAC02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The amount of water produced is excessive.",
 *     message = "The volume of water produced seems very high (exceeds 10 l/s every day of the month). Check if this value is correct.",
 * )
 */
class InquiryCheckAPSAC02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: el valor son 25000 metros cúbicos o equivalente en cualquier otra unidad
        //  PSA_C6 > 25000 metros cúbicos
        $wsp3d6d1 = $this->inquiry->{'field_m3_monthly_produced_water_exists'};
        if ($wsp3d6d1) {
            /** @var VolumeMeasurement $wsp3d6d2 */
            $wsp3d6d2 = $this->inquiry->{'field_m3_monthly_produced_water'};
            $wsp3d6d2->setUnit($wsp3d6d2->getReferenceUnit());
            if (25000 < $wsp3d6d2->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
