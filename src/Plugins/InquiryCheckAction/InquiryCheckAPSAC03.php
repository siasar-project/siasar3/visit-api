<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC03",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The number of users is excessive.",
 *     message = "The number of users seems very high for rural providers. Check if this value is correct.",
 * )
 */
class InquiryCheckAPSAC03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // (PSA_A4 ≠ 3 o PSA_A4 ≠ 4) y
        //  PSA_C8.1 > 10000
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        $wsp3d8d1d2 = $this->inquiry->{'field_users_required_pay'};
        if (('3' !== $wsp1d4 || '4' !== $wsp1d4) && 10000 < $wsp3d8d1d2) {
            $this->logResult();
        }

        return true;
    }
}
