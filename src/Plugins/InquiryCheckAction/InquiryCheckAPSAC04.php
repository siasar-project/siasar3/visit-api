<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The monthly billing is 10% higher than that resulting from multiplying the rate by the number of registered users.",
 *     message = "The average monthly billing is slightly higher than what would result from multiplying the average rate by the number of registered users. Check if this value is correct.",
 * )
 */
class InquiryCheckAPSAC04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 3.8.1.2 and 3.8.3.2 must have the same currency unit.');

        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C8.3 > 1,1 x PSA_C4 x PSA_C8.1
        $wsp3d4d1 = $this->inquiry->{'field_average_monthly_tariff_exists'};
        $wsp3d8d1d1 = $this->inquiry->{'field_users_required_pay_exists'};
        $wsp3d8d1d2 = $this->inquiry->{'field_users_required_pay'};
        $wsp3d8d3d1 = $this->inquiry->{'field_monthly_amount_billed_exists'};
        // Check if all values are known
        if ($wsp3d4d1 && $wsp3d8d1d1 && $wsp3d8d3d1) {
            $wsp3d4d2 = $this->getCurrencyAmount($this->inquiry->{'field_average_monthly_tariff'});
            $wsp3d8d3d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_billed'});

            if (false === $wsp3d4d2 || false === $wsp3d8d3d2) {
                $this->logByCurrency();

                return true;
            }

            if ($wsp3d8d3d2 > (1.1 * $wsp3d4d2 * $wsp3d8d1d2)) {
                $this->logResult();
            }
        }

        return true;
    }
}
