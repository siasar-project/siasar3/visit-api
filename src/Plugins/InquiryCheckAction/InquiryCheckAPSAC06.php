<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC06",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that only 25% of the users are up to date.",
 *     message = "The number of users per day is less than 25% of the total. Check if this value is correct.",
 * )
 */
class InquiryCheckAPSAC06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C8.2 < 0,25 x PSA_C8.1
        $wsp3d8d1d1 = $this->inquiry->{'field_users_required_pay_exists'};
        $wsp3d8d2d1 = $this->inquiry->{'field_users_uptodate_payment_exists'};
        // Check if all values are known
        if ($wsp3d8d1d1 && $wsp3d8d2d1) {
            $wsp3d8d1d2 = $this->inquiry->{'field_users_required_pay'};
            $wsp3d8d2d2 = $this->inquiry->{'field_users_uptodate_payment'};

            if ($wsp3d8d2d2 < (0.25 * $wsp3d8d1d2)) {
                $this->logResult();
            }
        }

        return true;
    }
}
