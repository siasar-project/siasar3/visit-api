<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC10",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The revenue-to-billing ratio is more than 25% different from the people-per-day ratio.",
 *     message = "The proportion of people up to date on payments varies by more than 25% of the ratio of income to turnover. Check if these numbers are correct.",
 * )
 */
class InquiryCheckAPSAC10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 3.8.3.2 and 3.8.4.2 must have the same currency unit.');

        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // (PSA_C8.2 / PSA_C8.1) > 1,25 x (PSA_C8.4 / PSA_C8.3)
        // O bien
        // (PSA_C8.2 / PSA_C8.1) < 0,75 x (PSA_C8.4 / PSA_C8.3)

        $wsp3d8d1d1 = $this->inquiry->{'field_users_required_pay_exists'};
        $wsp3d8d2d1 = $this->inquiry->{'field_users_uptodate_payment_exists'};
        $wsp3d8d3d1 = $this->inquiry->{'field_monthly_amount_billed_exists'};
        $wsp3d8d4d1 = $this->inquiry->{'field_monthly_amount_revenue_exists'};

        if ($wsp3d8d1d1 && $wsp3d8d2d1 && $wsp3d8d3d1 && $wsp3d8d4d1) {
            $wsp3d8d1d2 = $this->inquiry->{'field_users_required_pay'}; // integer
            $wsp3d8d2d2 = $this->inquiry->{'field_users_uptodate_payment'}; // integer
            $wsp3d8d3d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_billed'}); // currency
            $wsp3d8d4d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_revenue'}); // currency

            if (false === $wsp3d8d3d2 || false === $wsp3d8d4d2) {
                $this->logByCurrency();

                return true;
            }

            if (($wsp3d8d2d2 / $wsp3d8d1d2) > (1.25 * ($wsp3d8d4d2 / $wsp3d8d3d2))) {
                $this->logResult();
            }

            if (($wsp3d8d2d2 / $wsp3d8d1d2) < (0.75 * ($wsp3d8d4d2 / $wsp3d8d3d2))) {
                $this->logResult();
            }
        }

        return true;
    }
}
