<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC11",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "No monthly billing.",
 *     message = "There is no monthly billing at the lender. Check if this is really the case.",
 * )
 */
class InquiryCheckAPSAC11 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C8.2 = 0
        $wsp3d8d1d1 = $this->inquiry->{'field_users_required_pay_exists'};
        if ($wsp3d8d1d1) {
            $wsp3d8d2d2 = $this->inquiry->{'field_users_uptodate_payment'};

            if (0.0 === $wsp3d8d2d2) {
                $this->logResult();
            }
        }

        return true;
    }
}
