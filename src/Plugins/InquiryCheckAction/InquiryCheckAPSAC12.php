<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC12",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "No monthly income.",
 *     message = "The service provider has no monthly income. Check if this is really the case.",
 * )
 */
class InquiryCheckAPSAC12 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C8.4 = 0
        $wsp3d8d4d1 = $this->inquiry->{'field_monthly_amount_revenue_exists'};
        if ($wsp3d8d4d1) {
            $wsp3d8d4d2 = $this->inquiry->{'field_monthly_amount_revenue'};
            if (0.0 === $wsp3d8d4d2->amount) {
                $this->logResult();
            }
        }

        return true;
    }
}
