<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAC13",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "Annual extraordinary income exceeds ordinary income, which is unusual.",
 *     message = "Extraordinary annual income from new connections, spills or fines is higher than the total ordinary income from tariffs for the whole year. Review whether this is really the case.",
 * )
 */
class InquiryCheckAPSAC13 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 3.8.4.2 and 3.10.1.2 must have the same currency unit.');

        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C10.1.2 > 12 x PSA_C8.4
        $wsp3d8d4d1 = $this->inquiry->{'field_monthly_amount_revenue_exists'};
        $wsp3d10d1d1 = $this->inquiry->{'field_additional_revenue_sources_new_connections_exists'};

        if ($wsp3d8d4d1 && $wsp3d10d1d1) {
            $wsp3d8d4d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_revenue'});
            $wsp3d10d1d2 = $this->getCurrencyAmount($this->inquiry->{'field_additional_revenue_sources_new_connections'});

            if (false === $wsp3d8d4d2 || false === $wsp3d10d1d2) {
                $this->logByCurrency();

                return true;
            }

            if ($wsp3d10d1d2 > (12 * $wsp3d8d4d2)) {
                $this->logResult();
            }
        }

        return true;
    }
}
