<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "APSAC18",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "If there is more invoicing than income, field 3.9 should be answered to indicate the reasons for non-payment.",
 *     message = "It has been indicated that there is more turnover than income, i.e. there is a certain rate of non-payment. This being the case, it is suggested to answer in field 3.9 what are the main reasons for non-payment.",
 * )
 */
class InquiryCheckAPSAC18 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        /** @var Country $country */
        $country = $this->inquiry->{'field_country'};
        $levels = $country->getFormLevel();
        $formLevel = 1;
        if (isset($levels["form.wsprovider"]["level"])) {
            $formLevel = $levels["form.wsprovider"]["level"];
        }
        if (1 === $formLevel) {
            return true;
        }
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }

        // Is the field visible?
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visible = $visibleResolver->isVisible('field_reasons_nonpayment', '');
        if (!$visible) {
            return true;
        }

        // (PSA_C8.3 > PSA_C8.4) y PSA_C9 NO esta respondida
        $amountBilled = $this->inquiry->{'field_monthly_amount_billed'};
        $amountRevenue = $this->inquiry->{'field_monthly_amount_revenue'};
        $condition = $this->inquiry->{'field_reasons_nonpayment'};

        if (!$amountBilled || !$amountRevenue) {
            return true;
        }

        if (($amountBilled->getId() === $amountRevenue->getId()) && ($amountBilled->amount > $amountRevenue->amount)) {
            if (!$condition) {
                $this->logResult();
            }
        }

        return true;
    }
}
