<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAD01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The sum of all partial expenses is less than the total expense. This is not necessarily a failure because some expenses may simply not have been discretized.",
 *     message = "The sum of the different expenses is less than the total monthly expense figure. This may be caused by the fact that some partial expenses are not known, but it is still suggested to review the expense block.",
 * )
 */
class InquiryCheckAPSAD01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_D1.12.2 > SUMA (D1.X.2)
        $this->logByCurrencyMessage('All fields in 4.1 Expenses must have the same currency unit.');

        // Subtotal group 4.1.4.2
        $fieldExpensesManagementSubtotal = $this->getCurrencyAmount($this->inquiry->{'field_expenses_management_subtotal'});

        // Subtotal group 4.1.9.2
        $fieldExpensesOperationsSubtotal = $this->getCurrencyAmount($this->inquiry->{'field_expenses_operations_subtotal'});

        // Without subtotal group.
        $fieldExpensesMinorRepairs = $this->getCurrencyAmount($this->inquiry->{'field_expenses_minor_repairs'});
        $fieldExpensesMaintenanceUpkeep = $this->getCurrencyAmount($this->inquiry->{'field_expenses_maintenance_upkeep'});
        // Total
        $fieldTotalExpenses = $this->getCurrencyAmount($this->inquiry->{'field_total_expenses'});
        $knowFieldTotalExpenses = $this->inquiry->{'field_total_expenses_exists'};

        if (false === $fieldExpensesMinorRepairs ||
            false === $fieldExpensesMaintenanceUpkeep ||
            false === $fieldTotalExpenses
        ) {
            $this->logByCurrency();

            return true;
        }

        if ($knowFieldTotalExpenses) {
            if ($fieldTotalExpenses > ($fieldExpensesManagementSubtotal + $fieldExpensesOperationsSubtotal + $fieldExpensesMinorRepairs + $fieldExpensesMaintenanceUpkeep)) {
                $this->logResult();

                return true;
            }
        }

        return true;
    }
}
