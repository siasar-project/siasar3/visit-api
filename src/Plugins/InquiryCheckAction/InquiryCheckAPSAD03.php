<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAD03",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "Expenditures are less than half of monthly income.",
 *     message = "Monthly income is more than double the monthly expenses. Check if this high difference is correct.",
 * )
 */
class InquiryCheckAPSAD03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 3.8.4.2 and 4.1.12.2 must have the same currency unit.');

        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_D1.12.2 < 0,5 x PSA_C8.4
        // D1.12.2 = field_total_expenses
        // C8.4 = field_monthly_amount_revenue
        $totalExpenses = $this->getCurrencyAmount($this->inquiry->{'field_total_expenses'});
        $monthlyAmountRevenue = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_revenue'});

        if (false === $totalExpenses || false === $monthlyAmountRevenue) {
            $this->logByCurrency();

            return true;
        }

        if ($totalExpenses < (0.5 * $monthlyAmountRevenue)) {
            $this->logResult();
        }

        return true;
    }
}
