<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAD04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The sum of all partial administration expenses is less than the total expense. This is not necessarily a failure because some expenses may simply not have been discretized.",
 *     message = "The sum of the different administration expenses is less than the total monthly expense figure. This may be caused because some partial expenses are not known, but it is also suggested to review the administration expenses block.",
 * )
 */
class InquiryCheckAPSAD04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_D1.12.2 > SUMA (D1.X.2)
        $this->logByCurrencyMessage('All fields in 4.1 Expenses must have the same currency unit.');

        // Group 4.1.4.2
        $fieldExpensesSalariesAdministrativePersonnel = $this->getCurrencyAmount($this->inquiry->{'field_expenses_salaries_administrative_personnel'});
        $fieldExpensesOfficeSupplies = $this->getCurrencyAmount($this->inquiry->{'field_expenses_office_supplies'});
        $fieldExpensesFinancialLegal = $this->getCurrencyAmount($this->inquiry->{'field_expenses_financial_legal'});
        // Subtotal group 4.1.4.2
        $fieldExpensesManagementSubtotal = $this->getCurrencyAmount($this->inquiry->{'field_expenses_management_subtotal'});
        $knowFieldExpensesManagementSubtotal = $this->inquiry->{'field_expenses_management_subtotal_exists'};

        if (false === $fieldExpensesSalariesAdministrativePersonnel ||
            false === $fieldExpensesOfficeSupplies  ||
            false === $fieldExpensesFinancialLegal ||
            false === $fieldExpensesManagementSubtotal
        ) {
            $this->logByCurrency();

            return true;
        }

        if ($knowFieldExpensesManagementSubtotal) {
            if ($fieldExpensesManagementSubtotal > ($fieldExpensesSalariesAdministrativePersonnel + $fieldExpensesOfficeSupplies + $fieldExpensesFinancialLegal)) {
                $this->logResult();

                return true;
            }
        }

        return true;
    }
}
