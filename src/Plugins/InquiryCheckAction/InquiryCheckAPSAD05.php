<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAD05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The sum of all partial operating expenses is less than the total expense. This is not necessarily a failure because some expenses may simply not have been discretized.",
 *     message = "The sum of the different operating expenses is less than the total monthly expense figure. This may be caused because some partial expenses are not known, but it is also suggested to review the operating expenses block.",
 * )
 */
class InquiryCheckAPSAD05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_D1.12.2 > SUMA (D1.X.2)
        $this->logByCurrencyMessage('All fields in 4.1 Expenses must have the same currency unit.');

        // Group 4.1.9.2
        $fieldExpensesSalariesTechnicalPersonnel = $this->getCurrencyAmount($this->inquiry->{'field_expenses_salaries_technical_personnel'});
        $fieldExpensesEnergyCosts = $this->getCurrencyAmount($this->inquiry->{'field_expenses_energy_costs'});
        $fieldExpensesWaterTreatmentCosts = $this->getCurrencyAmount($this->inquiry->{'field_expenses_water_treatment_costs'});
        $fieldExpensesOtherCosts = $this->getCurrencyAmount($this->inquiry->{'field_expenses_other_costs'});
        // Subtotal group 4.1.9.2
        $fieldExpensesOperationsSubtotal = $this->getCurrencyAmount($this->inquiry->{'field_expenses_operations_subtotal'});
        $knowFieldExpensesOperationsSubtotal = $this->inquiry->{'field_expenses_operations_subtotal_exists'};

        if (false === $fieldExpensesSalariesTechnicalPersonnel  ||
            false === $fieldExpensesEnergyCosts ||
            false === $fieldExpensesWaterTreatmentCosts ||
            false === $fieldExpensesOtherCosts ||
            false === $fieldExpensesOperationsSubtotal
        ) {
            $this->logByCurrency();

            return true;
        }

        if ($knowFieldExpensesOperationsSubtotal) {
            if ($fieldExpensesOperationsSubtotal > ($fieldExpensesSalariesTechnicalPersonnel + $fieldExpensesEnergyCosts + $fieldExpensesWaterTreatmentCosts + $fieldExpensesOtherCosts)) {
                $this->logResult();

                return true;
            }
        }

        return true;
    }
}
