<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that the lender has a current account book, but has not reported billing and/or billing is zero.",
 *     message = "The lender has the ledger up to date, but however the monthly billing information has not been collected and/or the billing is zero. Check if this is correct.",
 * )
 */
class InquiryCheckAPSAE01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E1 = 1 y (PSA_C8.3 =0 o PSA_C8.3 = 98)
        // E1 = field_uptodate_records_revenues_expenses
        // C8.3 = field_monthly_amount_billed

        $monthlyAmountBilledExists = $this->inquiry->{'field_monthly_amount_billed_exists'};
        $uptodateRecordsRevenuesExpenses = $this->inquiry->{'field_uptodate_records_revenues_expenses'};


        if ($monthlyAmountBilledExists && $uptodateRecordsRevenuesExpenses) {
            $monthlyAmountBilled = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_billed'});
            if (0.0 === $monthlyAmountBilled) {
                if (!$uptodateRecordsRevenuesExpenses) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
