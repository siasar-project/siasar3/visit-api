<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE03",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that the lender has a current account book, but has not reported billing and/or billing is zero.",
 *     message = "The provider has the account book up to date, but nevertheless the information on the monthly expenses has not been collected and/or there are no expenses. Check if this is correct",
 * )
 */
class InquiryCheckAPSAE03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E1 = 1 y (PSA_D1.12.2  = 0 o PSA_C1.12.2 field_total_expense = 98)
        // E1 = field_uptodate_records_revenues_expenses
        // D1.12.2  = field_total_expenses
        $uptodateRecordsRevenuesExpenses = $this->inquiry->{'field_uptodate_records_revenues_expenses'};
        $totalExpenses = $this->inquiry->{'field_total_expenses'};

        if (!$totalExpenses && !$uptodateRecordsRevenuesExpenses) {
            $this->logResult();
        }

        return true;
    }
}
