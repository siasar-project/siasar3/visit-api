<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that there is no income if there is a ledger.",
 *     message = "The lender has an up-to-date book of accounts, but the total amount of annual income is zero. Check if this is correct or if this value has not really been reported.",
 * )
 */
class InquiryCheckAPSAE04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E2.2 = 0
        // E2.2 = field_total_amount_revenue_latest_year
        $totalAmountRevenueLatestYearExist = $this->inquiry->{'field_total_amount_revenue_latest_year_exists'};

        if ($totalAmountRevenueLatestYearExist) {
            $totalAmountRevenueLatestYear = $this->getCurrencyAmount($this->inquiry->{'field_total_amount_revenue_latest_year'});
            if (0.0 === $totalAmountRevenueLatestYear) {
                $this->logResult();
            }
        }

        return true;
    }
}
