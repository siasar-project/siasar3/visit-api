<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that there are no expenses if there is a ledger.",
 *     message = "The lender has an up to date book of accounts, but the total amount of annual expenses is zero. Check if this is correct or if this value has not been reported.",
 * )
 */
class InquiryCheckAPSAE05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E3.2 = 0
        // E3.2 = field_total_amount_expenses_latest_year
        $this->logByCurrencyMessage('Field 5.3.2 must have the same currency unit.');

        $wsp5d3d1 = $this->inquiry->{'field_total_amount_expenses_latest_year_exists'};

        if ($wsp5d3d1) {
            $wsp5d3d2 = $this->getCurrencyAmount($this->inquiry->{'field_total_amount_expenses_latest_year'});
            if (false === $wsp5d3d2) {
                $this->logByCurrency();

                return true;
            }
            if (0.0 === $wsp5d3d2) {
                $this->logResult();
            }
        }

        return true;
    }
}
