<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE06",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "Annual income exceeds the sum of monthly income and all extraordinary income by 25%.",
 *     message = "The income recorded in the account book is more than 25% higher than the sum of all monthly income and extraordinary income. This may be correct (they are different analysis annuities), but it is worth checking.",
 * )
 */
class InquiryCheckAPSAE06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E2 > 1,25 x ((12 x PSA_C8.4) + PSA_C10.1.2 + PSA_C10.2.2 + PSA_C10.3.2 + PSA_C10.4.2 + PSA_C10.5.2)
        $this->logByCurrencyMessage('Fields 5.2, 3.8.4.2 and group 3.10 must have the same currency unit.');

        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        $wsp3d8d4d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_revenue'});
        $wsp3d10d1d2 = $this->getCurrencyAmount($this->inquiry->{'field_additional_revenue_sources_new_connections'});
        $wsp3d10d2d2 = $this->getCurrencyAmount($this->inquiry->{'field_additional_revenue_sources_subsidies'});
        $wsp3d10d3d2 = $this->getCurrencyAmount($this->inquiry->{'field_additional_revenue_sources_uncommon'});
        $wsp3d10d4d2 = $this->getCurrencyAmount($this->inquiry->{'field_additional_revenue_sources_contributions'});
        $wsp3d10d5d3 = $this->getCurrencyAmount($this->inquiry->{'field_additional_revenue_sources_other'});
        $wsp5d2d2 = $this->getCurrencyAmount($this->inquiry->{'field_total_amount_revenue_latest_year'});

        if (false === $wsp3d8d4d2 ||
            false === $wsp3d10d1d2 ||
            false === $wsp3d10d2d2 ||
            false === $wsp3d10d3d2 ||
            false === $wsp3d10d4d2 ||
            false === $wsp3d10d5d3 ||
            false === $wsp5d2d2
        ) {
            $this->logByCurrency();

            return true;
        }

        if ($wsp5d2d2 > (1.25 * ((12 * $wsp3d8d4d2) + $wsp3d10d1d2 + $wsp3d10d2d2 + $wsp3d10d3d2 + $wsp3d10d4d2 + $wsp3d10d5d3))) {
            $this->logResult();
        }

        return true;
    }
}
