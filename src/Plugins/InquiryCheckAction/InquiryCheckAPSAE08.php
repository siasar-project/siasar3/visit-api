<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE08",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "Annual expenses exceed the sum of monthly expenses by 25%.",
 *     message = "The expenses recorded in the ledger are more than 25% higher than the sum of all monthly expenses. This may be correct (they are different analysis annuities), but it is worth checking.",
 * )
 */
class InquiryCheckAPSAE08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 5.3.2 and 4.1.12.2 must have the same currency unit.');

        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_E03 > 1,25 x (12 x PSA_D1.12.2)
        $wsp5d3d1 = $this->inquiry->{'field_total_amount_expenses_latest_year_exists'};
        $wsp4d1d12d1 = $this->inquiry->{'field_total_expenses_exists'};
        if ($wsp5d3d1 && $wsp4d1d12d1) {
            $wsp5d3d2 = $this->getCurrencyAmount($this->inquiry->{'field_total_amount_expenses_latest_year'});
            $wsp4d1d12d2 = $this->getCurrencyAmount($this->inquiry->{'field_total_expenses'});

            if (false === $wsp5d3d2 || false === $wsp4d1d12d2) {
                $this->logByCurrency();

                return true;
            }

            if ($wsp5d3d2 > (1.25 * (12 * $wsp4d1d12d2))) {
                $this->logResult();
            }
        }


        return true;
    }
}
