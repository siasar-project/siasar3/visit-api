<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE10",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "It is strange that there is a bank account but no funds.",
 *     message = "The service provider has a bank account, however, no funds are available. Check if this is correct.",
 * )
 */
class InquiryCheckAPSAE10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E4 = 1 y PSA_E5 = 2
        $wsp5d4 = $this->inquiry->{'field_association_have_bank_account'};
        $wsp5d5 = $this->inquiry->{'field_association_have_available_funds'};

        if ($wsp5d4 && !$wsp5d5) {
            $this->logResult();
        }

        return true;
    }
}
