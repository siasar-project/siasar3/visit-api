<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE11",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "If you have no funds available you probably have no reserve fund (unless you have it set aside).",
 *     message = "The service provider has no funds available, but does have a reserve fund. Check if this is correct.",
 * )
 */
class InquiryCheckAPSAE11 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_E5 = 2 y PSA_E7 = 1
        $wsp5d5 = $this->inquiry->{'field_association_have_available_funds'};
        $wsp5d7 = $this->inquiry->{'field_are_savings_for_future'};

        if (!$wsp5d5 && $wsp5d7) {
            $this->logResult();
        }

        return true;
    }
}
