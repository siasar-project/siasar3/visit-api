<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAE16",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "There are more complaints than twelve times the number of users.",
 *     message = "The provider is type 3 (public institution) or type 4 (private body) but does not have a balance sheet. Check if this is correct",
 * )
 */
class InquiryCheckAPSAE16 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        /** @var Country $country */
        $country = $this->inquiry->{'field_country'};
        $levels = $country->getFormLevel();
        $formLevel = 1;
        if (isset($levels["form.wsprovider"]["level"])) {
            $formLevel = $levels["form.wsprovider"]["level"];
        }
        if (2 >= $formLevel) {
            return true;
        }
        // PSA_E15 = 2 y (PSA_A4 = 3 o PSA = 4)
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        $wsp5d15 = $this->inquiry->{'field_available_balance_sheet'};

        if (!$wsp5d15 && ('3' === $wsp1d4 || '4' === $wsp1d4)) {
            $this->logResult();
        }

        return true;
    }
}
