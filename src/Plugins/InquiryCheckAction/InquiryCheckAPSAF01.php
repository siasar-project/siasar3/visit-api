<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAF01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "An institutional or private service provider should be doing maintenance.",
 *     message = "The provider is type 3 (public institution) or type 4 (private organization) but is not performing maintenance operations on the system. Check if this is correct.",
 * )
 */
class InquiryCheckAPSAF01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_F1 = 4 y (PSA_A4 = 3 o PSA = 4)
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        $wsp6d1 = $this->inquiry->{'field_carryout_om_last_year'};

        if ('4' === $wsp6d1 && ('3' === $wsp1d4 || '4' === $wsp1d4)) {
            $this->logResult();
        }

        return true;
    }
}
