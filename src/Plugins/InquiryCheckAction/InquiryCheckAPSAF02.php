<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAF02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "An institutional or private lender should be performing maintenance",
 *     message = "The lender performs maintenance work, but has no staff (2.4), so it is done on a communal or voluntary basis. Check if this is correct.",
 * )
 */
class InquiryCheckAPSAF02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_F1 ≠ 4 y PSA_B4 = 2
        // PSA_F1 =  field_carryout_om_last_year
        //  PSA_B4 = field_have_personnel_to_rural_service
        $wsp6d1 = $this->inquiry->{'field_carryout_om_last_year'};
        $wsp2d4 = $this->inquiry->{'field_have_personnel_to_rural_service'};

        if ('4' !== $wsp6d1 && '2' === $wsp2d4) {
            $this->logResult();
        }

        return true;
    }
}
