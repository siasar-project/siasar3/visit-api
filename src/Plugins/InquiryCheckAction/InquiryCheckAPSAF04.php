<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "APSAF04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The lender performs maintenance operations, but does not have any tools.",
 *     message = "The provider performs maintenance work, but does not have the material resources to do so (6.3). Check if this is correct.",
 * )
 */
class InquiryCheckAPSAF04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_F1 ≠ 4 y PSA_F3 = 2
        // PSA_F1 =  field_carryout_om_last_year
        // PSA_F3 = field_om_adequate_material_resources
        $wsp6d1 = $this->inquiry->{'field_carryout_om_last_year'};
        $wsp6d3 = $this->inquiry->{'field_om_adequate_material_resources'};

        if ('4' !== $wsp6d1 && '2' === $wsp6d3) {
            $this->logResult();
        }

        return true;
    }
}
