<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISA05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "There should be almost no system registered in SIASAR that is more than 50 years old.",
 *     message = "The system has been built more than 50 years ago. Although this is possible, check if this value is correct.",
 * )
 */
class InquiryCheckASISA05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_A4 < 1972
        $condition = $this->inquiry->{'field_year_build'};
        if (!empty($condition)) {
            $refYear = intval(date('Y')) - 50;
            if ($refYear > $condition) {
                $this->logResult();
            }
        }

        return true;
    }
}
