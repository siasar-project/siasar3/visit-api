<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISA06",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Systems that are part of a larger urban system will be very few in SIASAR, so it is better to alert, in case it is an error.",
 *     message = "It has been reported that the system is part of a larger one. Since this is unusual, review that indeed this system is actually a derivation of a large urban system.",
 * )
 */
class InquiryCheckASISA06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_A6 = 1
        $condition = ($this->inquiry->{'field_have_distribution_network'});
        if ($condition) {
            /** @var bool $value */
            $value = $this->inquiry->{'field_system_derived_urban'};
            if ($value) {
                $this->logResult();
            }
        }

        return true;
    }
}
