<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISA07",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is unusual for there to be no homes served.",
 *     message = "The number of dwellings in some of the communities is zero. Check if this is correct.",
 * )
 */
class InquiryCheckASISA07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //A12.3 = 0
        $subformRecords = $this->inquiry->{'field_served_communities'};
        foreach ($subformRecords as $record) {
            $condition = $record->{'field_households'};

            if (0 === $condition) {
                $this->logResult();
            }
        }

        return true;
    }
}
