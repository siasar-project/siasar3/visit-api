<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is strange that there is a pumped catchment from a river or stream type source.",
 *     message = "The source is surface type 1 (equivalent to river or stream), but the system is type 2 (pumped) with a pump in the catchment. Check if this is correct because usually the catchment from rivers does not require electric pumping.",
 * )
 */
class InquiryCheckASISB05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {

        //2.4.1.1 = 1 y
        //1.7 = 2 y
        //1.8 ≠ 2
        $typeSystem = $this->inquiry->{'field_type_system'};
        $pumpsLocation = $this->inquiry->{'field_pumps_location'};

        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            $sourceTypeSurface = $record->{'field_source_type_surface'};

            if ("1" === $sourceTypeSurface && "2" === $typeSystem && "2" !== $pumpsLocation) {
                $this->logResult();
            }
        }

        return true;
    }
}
