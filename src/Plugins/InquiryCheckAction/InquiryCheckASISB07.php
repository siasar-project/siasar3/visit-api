<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB07",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is very rare to withdraw water from an aquifer without a well.",
 *     message = "It has been indicated that it is a type 1 (gravity) system but the source is subway type 1 (aquifer). Check if this is correct because drawing water from an aquifer usually requires a well or equivalent.",
 * )
 */
class InquiryCheckASISB07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //2.4.1.2 = 1 y
        //1.7 = 1
        $typeSystem = $this->inquiry->{'field_type_system'};
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            $sourceTypeGroundWater = $record->{'field_source_type_groundwater'};

            if ("1" === $sourceTypeGroundWater && "1" === $typeSystem) {
                $this->logResult();
            }
        }

        return true;
    }
}
