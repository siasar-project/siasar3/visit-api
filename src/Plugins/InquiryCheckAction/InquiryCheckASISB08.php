<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB08",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is rare to have a pumped catchment from a spring, spring or spring source.",
 *     message = "The source is subway type 2 (spring or spring), but the system is type 2 (pumped) with a pump at the catchment. Check if this is correct because usually the catchment from springs or springs requires electric pumping.",
 * )
 */
class InquiryCheckASISB08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //2.4.1.2 = 2 y
        //1.7 = 2 y
        //1.8 ≠ 2

        $typeSystem = $this->inquiry->{'field_type_system'};
        $pumpsLocation = $this->inquiry->{'field_pumps_location'};
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            $sourceTypeGroundWater = $record->{'field_source_type_groundwater'};

            if ("2" === $sourceTypeGroundWater && "2" === $typeSystem && "2" !== $pumpsLocation) {
                $this->logResult();
            }
        }

        return true;
    }
}
