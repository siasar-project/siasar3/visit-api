<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB09",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Floating catchments in rivers or streams are not common.",
 *     message = "Check if the combination of source and catchment is correct.",
 * )
 */
class InquiryCheckASISB09 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B4.1.1 = 1 y SIS_B4.2.1 = 1
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $sys2d4d1d1 = $subformRecord->{'field_source_type_surface'};
            $sys2d4d2d1 = $subformRecord->{'field_source_catchment_type_surface'};

            if ('1' === $sys2d4d1d1 && '1' === $sys2d4d2d1) {
                $this->logResult();
            }
        }

        return true;
    }
}
