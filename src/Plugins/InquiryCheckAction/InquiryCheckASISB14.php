<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISB14",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "If the source flow rate was measured, it should not be zero.",
 *     message = "The flow rate measured at the source is zero. Check if this is correct.",
 * )
 */
class InquiryCheckASISB14 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: si hay medición de caudal
        //  SIS_B5.2 = 0
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $subrecordsB5 = $subformRecord->{'field_source_flow'};
            if ($subrecordsB5) {
                foreach ($subrecordsB5 as $subrecordB5) {
                    /** @var FlowMeasurement */
                    $b5d2 = $subrecordB5->{'field_flow'};
                    $b5d2->setUnit('liter/second');
                    if (0 === $b5d2->getValue()) {
                        $this->logResult();
                    }
                }
            }
        }

        return true;
    }
}
