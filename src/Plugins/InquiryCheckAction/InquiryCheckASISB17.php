<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISB17",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is strange that a measurement in the dry season is higher than in the rainy season.",
 *     message = "There is at least one dry season flow measurement greater than at least one wet season flow measurement. Check.",
 * )
 */
class InquiryCheckASISB17 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esta alerta solo aplica si hay dos mediciones (a y b) o más:
        //  SIS_B6.2 en (medición(a) y SIS_B6.1 = 2) > SIS_B6.2 en (medición(b) y SIS_B6.1 = 3)
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $accDry = 0;
            $accRainy = 0;
            $subrecordsB6 = $subformRecord->{'field_intake_flow'};
            if (count($subrecordsB6) > 1) {
                foreach ($subrecordsB6 as $subrecordB6) {
                    $b6d1 = $subrecordB6->{'field_season'};
                    /** @var FlowMeasurement */
                    $b6d2 = $subrecordB6->{'field_flow'};
                    $b6d2->setUnit('liter/second');
                    // Dry season
                    if (in_array('2', $b6d1)) {
                        $accDry += $b6d2->getValue();
                    }
                    // Rainy season
                    if (in_array('3', $b6d1)) {
                        $accRainy += $b6d2->getValue();
                    }
                }
                if ($accDry > $accRainy) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
