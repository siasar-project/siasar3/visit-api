<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;
use DateTime;

/**
 * @InquiryCheckAction(
 *     id = "ASISB20",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The date of the flow measurement is more than 5 years old.",
 *     message = "The measurement is more than 5 years old. Review if this is correct or if a more current measurement is available.",
 * )
 */
class InquiryCheckASISB20 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_0.1 - SIS_B6.4 > 5 años
        /** @var \DateTime $date */
        $date = $this->inquiry->{'field_questionnaire_date'};
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $subrecordsB6 = $subformRecord->{'field_intake_flow'};
            if ($subrecordsB6) {
                foreach ($subrecordsB6 as $subrecordB6) {
                    $b6d4 = $subrecordB6->{'field_measured'};
                    $measuredDate = \DateTime::createFromFormat('Y-m-d', $b6d4['year'].'-'.$b6d4['month'].'-'.'1');
                    if ($measuredDate) {
                        $interval = $measuredDate->diff($date);
                        $yearsDiff = $interval->y;
                        $monthsDiff = $interval->m;
                        $daysDiff = $interval->d;
                        $totalMonthsDiff = ($yearsDiff * 12) + $monthsDiff;

                        if (60 < $totalMonthsDiff || (60 === $totalMonthsDiff && 0 < $daysDiff)) {
                            $this->logResult();
                        }
                    }
                }
            }
        }

        return true;
    }
}
