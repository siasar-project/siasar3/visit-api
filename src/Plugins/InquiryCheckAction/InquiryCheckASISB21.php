<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB21",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "A grating catchment usually does not require surface protection.",
 *     message = "Type 2 surface catchment (grating) does not always require protection and, therefore, this question is usually 'not applicable'.",
 * )
 */
class InquiryCheckASISB21 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B8 ≠ 97 y SIS_B4.2.1 = 2
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $sys2d4d2d1 = $subformRecord->{'field_source_catchment_type_surface'};
            $sys2d8 = $subformRecord->{'field_water_protected'};

            if ('97' !== $sys2d8 && '2' === $sys2d4d2d1) {
                $this->logResult();
            }
        }

        return true;
    }
}
