<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB23",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "In a grate catchment, erosion is not usually relevant.",
 *     message = "For type 2 surface catchment (grid) erosion is usually not relevant. Check if the answer to this question could be 'not applicable'.",
 * )
 */
class InquiryCheckASISB23 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B9.2 ≠ 97 y SIS_B4.2.1 = 2
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $sys2d4d2d1 = $subformRecord->{'field_source_catchment_type_surface'};
            $sys2d9d2 = $subformRecord->{'field_erosion'};

            if ('97' !== $sys2d9d2 && '2' === $sys2d4d2d1) {
                $this->logResult();
            }
        }

        return true;
    }
}
