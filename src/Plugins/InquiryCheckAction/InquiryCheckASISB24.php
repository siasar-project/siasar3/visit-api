<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISB24",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Most rural water wells are less than 100 meters deep.",
 *     message = "Check if the well actually has a depth of more than 100 meters (or equivalent unit)."
 * )
 */
class InquiryCheckASISB24 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: metros o equivalente en cualquier otra unidad
        //  SIS_B10 > 100 metros
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            /** @var LengthMeasurement $sys2d10d2 */
            $sys2d10d2 = $subformRecord->{'field_well_depth'};
            $sys2d10d2->setUnit('metre');

            if (100 < $sys2d10d2->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
