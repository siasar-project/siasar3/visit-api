<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISB26",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The depth of the well must be greater than the depth of the aquifer in order to actually capture water.",
 *     message = "Check if the depth of the well really does not reach the level of the aquifer (because, if it does, it could not be currently capturing water)."
 * )
 */
class InquiryCheckASISB26 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B11 > SIS_B10
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $sys2d10d1 = $subformRecord->{'field_well_depth_exists'};
            /** @var LengthMeasurement $sys2d10d2 */
            $sys2d10d2 = $subformRecord->{'field_well_depth'};
            $sys2d10d2->setUnit('metre');

            $sys2d11d1 = $subformRecord->{'field_static_water_level_exists'};
            /** @var LengthMeasurement $sys2d11d2 */
            $sys2d11d2 = $subformRecord->{'field_static_water_level'};
            $sys2d11d2->setUnit('metre');

            if ($sys2d10d1 && $sys2d11d1) {
                if ($sys2d11d2 > $sys2d10d2) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
