<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISB29",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It seems strange that the infrastructure is in D, despite the fact that maintenance work has been done in the last year.",
 *     message = "The condition of the infrastructure is down (D) despite the fact that maintenance operations have been carried out in the last year. Check if this is really the case."
 * )
 */
class InquiryCheckASISB29 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B16 = D y SIS_B14 = 1
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $subformRecord) {
            $sys2d14 = $subformRecord->{'field_pump_maintenance_conducted_last_year'};
            $sys2d16 = $subformRecord->{'field_physical_state_intake'};

            if ('D' === $sys2d16 && $sys2d14) {
                $this->logResult();
            }
        }

        return true;
    }
}
