<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISC01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "There should be very few pipelines that exceed 10 km.",
 *     message = "The pipeline is longer than 10 km (or equivalent unit). Check if this is really the case.",
 * )
 */
class InquiryCheckASISC01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: metros o equivalente en cualquier otra unidad
        //  SIS_C2 > 10000 metros
        $subformRecords = $this->inquiry->{'field_water_transmision_line'}; // 3
        foreach ($subformRecords as $subformRecord) {
            if ($this->isVisibleField($subformRecord, 'field_length', 'field_water_transmision_line')) {
                /** @var LengthMeasurement $sys3d2 */
                $sys3d2 = $subformRecord->{'field_length'}; // 3.2
                $sys3d2->setUnit('metre');
                if (10000 < $sys3d2->getValue()) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
