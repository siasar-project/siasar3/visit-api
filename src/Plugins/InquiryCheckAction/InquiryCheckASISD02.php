<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISD02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is unusual for a protected spring to have a treatment facility.",
 *     message = "A type 5 system (spring with improved catchment) is very unlikely to have treatment and/or disinfection facilities. Check if this is correct.",
 * )
 */
class InquiryCheckASISD02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_A7 = 5 y SIS_D está cubierto
        $sys1d7 = $this->inquiry->{'field_type_system'};
        $sys4 = $this->inquiry->{'field_treatment_points'};
        if ('5' === $sys1d7 && $sys4) {
            $this->logResult();
        }

        return true;
    }
}
