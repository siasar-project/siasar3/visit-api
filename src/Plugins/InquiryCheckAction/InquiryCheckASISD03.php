<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISD03",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "If the coordinate is zero, the point is wrongly located on the map.",
 *     message = "The coordinate is zero, check if this position is correct.",
 * )
 */
class InquiryCheckASISD03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $haveDistributionNetwork = $this->inquiry->{'field_have_distribution_network'};
        if (!$haveDistributionNetwork) {
            // When the systems questionnaire is selected "without distribution" this rule does NOT apply.
            return true;
        }

        // SIS_D1.1 = 0
        $subrecords = $this->inquiry->{'field_treatment_points'};
        foreach ($subrecords as $sys4) {
            $sys4d1d1 = $sys4->{'field_infrastructure_latitude'};
            if (0.0 === $sys4d1d1) {
                $this->logResult();
            }
        }

        return true;
    }
}
