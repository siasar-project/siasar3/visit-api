<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISD06",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Although there are areas at elevations below sea level, it is unusual.",
 *     message = "The altitude is less than 0 meters. Check if this value is correct.",
 * )
 */
class InquiryCheckASISD06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_D1.3 < 0
        $subrecords = $this->inquiry->{'field_treatment_points'};
        foreach ($subrecords as $sys4) {
            /** @var LengthMeasurement */
            $sys4d1d3 = $sys4->{'field_infrastructure_altitude'};
            $sys4d1d3->setUnit('metre');

            if (0 > $sys4d1d3->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
