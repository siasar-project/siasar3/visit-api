<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISD07",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is unusual to have disinfection without a tank in which to dilute the disinfectant.",
 *     message = "It has been indicated that there is a disinfection facility despite the fact that the system does not have a storage tank. Check if this is correct.",
 * )
 */
class InquiryCheckASISD07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_D3.6 = 1 y SIS_E no está cubierto
        $subrecords = $this->inquiry->{'field_treatment_points'};
        $haveDisinfection = false;
        foreach ($subrecords as $sys4) {
            $sys4d3d6d1 = $sys4->{'field_disinfection'};
            if ($sys4d3d6d1) {
                $haveDisinfection = true;
                break;
            }
        }
        $sys5 = $this->inquiry->{'field_storage_infrastructure'};
        if ($haveDisinfection && !$sys5) {
            $this->logResult();
        }

        return true;
    }
}
