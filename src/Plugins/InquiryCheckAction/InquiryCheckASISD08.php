<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISD08",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It seems strange that the infrastructure is in A, despite the fact that no maintenance work has been done.",
 *     message = "The condition of the infrastructure is good (A) despite the fact that no maintenance operations have been performed. Check if this is really the case.",
 * )
 */
class InquiryCheckASISD08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esto se aplica para todas las X filas de tipos de instalación de D3
        //  SIS_D3.4.X = A y
        //  SIS_D6 = 2
        $subrecords = $this->inquiry->{'field_treatment_points'};
        foreach ($subrecords as $sys4) {
            $sys4d3d1d4 = $sys4->{'field_filtration_state'};
            $sys4d3d2d4 = $sys4->{'field_coagu_floccu_state'};
            $sys4d3d3d4 = $sys4->{'field_sedimentation_state'};
            $sys4d3d4d4 = $sys4->{'field_desalination_state'};
            $sys4d3d5d4 = $sys4->{'field_aeration_oxid_state'};
            $sys4d3d6d4 = $sys4->{'field_disinfection_state'};

            if ('A' === $sys4d3d1d4 || 'A' === $sys4d3d2d4 || 'A' === $sys4d3d3d4 || 'A' === $sys4d3d4d4 || 'A' === $sys4d3d5d4 || 'A' === $sys4d3d6d4) {
                $sys4d6 = $sys4->{'field_treatment_maintenance_conducted_last_year'};
                if (!$sys4d6) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
