<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISE01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is not at all common for a well with a hand pump to have a storage facility.",
 *     message = "A type 3 system (hand pumped well) is very unlikely to have storage facilities. Check if this is correct"
 * )
 */
class InquiryCheckASISE01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //SIS_A7 = 3 y SIS_E está cubierto

        $sys1d7 = $this->inquiry->{'field_type_system'};
        $sys5 = $this->inquiry->{'field_storage_infrastructure'};
        if ('3' === $sys1d7 && $sys5) {
            $this->logResult();
        }

        return true;
    }
}
