<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "ASISE04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "If the coordinate is zero, the point is badly located on the map.",
 *     message = "The coordinate is zero, check if this position is correct."
 * )
 */
class InquiryCheckASISE04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //SIS_E1.2 = 0

        // Is the field visible?
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visible = $visibleResolver->isVisible('field_storage_infrastructure', '');
        if (!$visible) {
            return true;
        }

        $subRecords = $this->inquiry->{'field_storage_infrastructure'};
        foreach ($subRecords as $subRecord) {
            // Is the field visible?
            $visibleResolver = new InquiryConditionalResolver($subRecord);
            $visible = $visibleResolver->isVisible('field_storage_longitude', 'field_storage_infrastructure');
            if ($visible) {
                $currentField = $subRecord->{'field_storage_longitude'};
                if (0.0 === $currentField) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
