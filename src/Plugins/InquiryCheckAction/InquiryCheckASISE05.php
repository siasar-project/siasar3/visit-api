<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISE05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Only in Andean areas can there be altitudes of more than 5000 meters, and even then, there are very few points at those altitudes.",
 *     message = "The altitude is higher than 5000 meters. Check if this value is correct"
 * )
 */
class InquiryCheckASISE05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //SIS_E1.3 > 5000

        $subRecords = $this->inquiry->{'field_storage_infrastructure'};
        foreach ($subRecords as $subRecord) {
            /** @var \App\Tools\Measurements\LengthMeasurement $currentField */
            $currentField = $subRecord->{'field_storage_altitude'};
            $currentField->setUnit('meter');
            if (5000 < $currentField->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
