<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISE07",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "A tank of more than 1,000 cubic meters is not common in rural environments.",
 *     message = "The tank is larger than 1000 cubic meters (or equivalent unit), higher than average in rural communities. Check if this value is correct."
 * )
 */
class InquiryCheckASISE07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //Nota: metros cúbicos o equivalente en cualquier otra unidad
        //SIS_E4 > 1000 metros cúbicos
        //E4 => field_storage_capacity

        $subformRecords = $this->inquiry->{'field_storage_infrastructure'};
        foreach ($subformRecords as $record) {
            /** @var VolumeMeasurement $volume */
            $volume = $record->{'field_storage_capacity'};
            $volume->setUnit('cubic metre');

            if (1000 < $volume->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
