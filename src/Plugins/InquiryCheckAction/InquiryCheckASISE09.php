<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISE09",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It seems strange that the infrastructure is in D, despite the fact that maintenance work has been done in the last year.",
 *     message = "The condition of the infrastructure is down (D) despite the fact that maintenance operations have been carried out in the last year. Check if this is really the case."
 * )
 */
class InquiryCheckASISE09 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //SIS_E7 = D y SIS_E6 = 1
        //E7 = field_physical_state_storage
        //E6 = field_storage_maintenance_last_year

        $subRecords = $this->inquiry->{'field_storage_infrastructure'};
        foreach ($subRecords as $subRecord) {
            $physicalStateStorage = $subRecord->{'field_physical_state_storage'};
            $storageMaintenanceLastYear = $subRecord->{'field_storage_maintenance_last_year'};
            if ("D" === $physicalStateStorage && "1" === $storageMaintenanceLastYear) {
                $this->logResult();
            }
        }

        return true;
    }
}
