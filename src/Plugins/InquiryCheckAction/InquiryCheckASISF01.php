<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISF01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "There should be very few distributions exceeding 10 km.",
 *     message = "The distribution has a length greater than 10 km (or equivalent unit). Check if this is really the case",
 * )
 */
class InquiryCheckASISF01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //Nota: metros o equivalente en cualquier otra unidad
        //SIS_F3 > 10000 metros
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subformRecords as $record) {
            /** @var LengthMeasurement $length */
            $length = $record->{'field_network_length'};
            $length->setUnit('metre');

            if (10000 < $length->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
