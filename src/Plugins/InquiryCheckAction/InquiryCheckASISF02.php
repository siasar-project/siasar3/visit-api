<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISF02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It seems strange that the number of public fountains is one for every two dwellings (6.7 greater than 50% of the dwellings in field 1.12.3)",
 *     message = "It has been indicated that there is more than one public fountain for every two dwellings. This appears to be a high housing/fountain ratio. Check if this value is correct.",
 * )
 */
class InquiryCheckASISF02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esta comprobación se hace contra la comunidad (i) servida por esa distribución específica
        //  SIS_F7 > 0,5 x SIS_A.12.3i
        $subrecords1d12 = $this->inquiry->{'field_served_communities'};
        $subrecords6 = $this->inquiry->{'field_distribution_infrastructure'};

        foreach ($subrecords6 as $sys6) {
            $sys6d2 = $sys6->{'field_communities_serviced_network'}; // administrative_division_reference
            if (!$sys6d2) {
                return true;
            }
            foreach ($subrecords1d12 as $sys1d12) {
                $sys1d12d1 = $sys1d12->{'field_community'}; // administrative_division_reference
                if ($sys1d12d1->getId() === $sys6d2->getId()) {
                    $sys1d12d3 = $sys1d12->{'field_households'};
                    $sys6d7 = $sys6->{'field_without_households_connection'};

                    if ($sys6d7 > (0.5 * $sys1d12d3)) {
                        $this->logResult();
                    }
                }
            }
        }

        return true;
    }
}
