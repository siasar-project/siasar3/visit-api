<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISF06",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "A strikingly high flow rate value.",
 *     message = "The value of the distributed flow rate is greater than 10 l/s (or equivalent) during the whole day. Check if this value is correct.",
 * )
 */
class InquiryCheckASISF06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: el valor son 864000 l/día o equivalente en cualquier otra unidad
        //  SIS_F11 > 864000 l/día
        $subrecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subrecords as $sys6) {
            /** @var FlowMeasurement $sys6d11d2 */
            $sys6d11d2 = $sys6->{'field_service_flow'};
            $sys6d11d2->setUnit('liter/day');

            if (864000 < $sys6d11d2->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
