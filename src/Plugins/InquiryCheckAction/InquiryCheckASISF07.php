<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISF07",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Zero flow means that there is no water in the system, so check to see if this is OK.",
 *     message = "It has been indicated that the distribution flow rate is zero, i.e. no water is being distributed. Check if this is correct.",
 * )
 */
class InquiryCheckASISF07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_F11 = 0 y
        //  (SIS_A9.1 = 1 o SIS_A9.2 = 1)
        $sys1d9d1 = $this->inquiry->{'field_dry_season'};
        $sys1d9d2 = $this->inquiry->{'field_rainy_season'};
        $subrecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subrecords as $sys6) {
            /** @var FlowMeasurement $sys6d11d2 */
            $sys6d11d2 = $sys6->{'field_service_flow'};

            if (0.0 === $sys6d11d2->getValue()) {
                if ($sys1d9d1 || $sys1d9d2) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
