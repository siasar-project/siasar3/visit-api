<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISF09",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The indicated flow rate would give an estimate of the number of liters per person per day of less than 25 liters, which is possible, but somewhat low.",
 *     message = "According to the system flow rate recorded in 6.11.2, the provision in the communities served, for an average of 5 persons in each dwelling, is less than 25 liters per person per day. Check if this value is correct.",
 * )
 */
class InquiryCheckASISF09 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota1 : esta comprobación se hace contra la comunidad (i) servida por esa distribución específica
        // Nota 2: las unidades para este algoritmo deben ser litros/día
        // SIS_F11 / ( 5 x SIS_A.12.3i) < 25
        $subrecords1d12 = $this->inquiry->{'field_served_communities'};
        $subrecords6 = $this->inquiry->{'field_distribution_infrastructure'};

        foreach ($subrecords6 as $sys6) {
            $sys6d2 = $sys6->{'field_communities_serviced_network'}; // administrative_division_reference
            if (!$sys6d2) {
                return true;
            }
            foreach ($subrecords1d12 as $sys1d12) {
                $sys1d12d1 = $sys1d12->{'field_community'}; // administrative_division_reference
                if ($sys1d12d1->getId() === $sys6d2->getId()) {
                    $sys1d12d3 = $sys1d12->{'field_households'};
                    /** @var FlowMeasurement $sys6d11d2 */
                    $sys6d11d2 = $sys6->{'field_service_flow'};
                    $sys6d11d2->setUnit('liter/day');

                    if (25 > ($sys6d11d2->getValue() / (5 * $sys1d12d3))) {
                        $this->logResult();
                    }
                }
            }
        }

        return true;
    }
}
