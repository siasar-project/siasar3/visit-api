<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISF10",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It doesn't make much sense that there is enough water, but then in the flow field, it is zero.",
 *     message = "It has been indicated that the flow rate of the system is zero, however, the water is sufficient to meet the demand of the community (1.9). Check if this is correct.",
 * )
 */
class InquiryCheckASISF10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_F11 = 0
        $subrecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subrecords as $sys6) {
            /** @var FlowMeasurement $sys6d11d2 */
            $sys6d11d2 = $sys6->{'field_service_flow'};

            if (0.0 === $sys6d11d2->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
