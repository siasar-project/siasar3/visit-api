<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISF11",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It seems strange that the infrastructure is in A, despite the fact that no maintenance work has been done.",
 *     message = "The condition of the infrastructure is good (A) despite the fact that no maintenance operations have been performed. Check if this is really the case.",
 * )
 */
class InquiryCheckASISF11 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_F16 = A y SIS_F14 = 2
        $subrecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subrecords as $sys6) {
            $sys6d14 = $sys6->{'field_service_maintenance_conducted_last_year'};
            $sys6d16 = $sys6->{'field_distribution_state'};

            if ('A' === $sys6d16 && !$sys6d14) {
                $this->logResult();
            }
        }

        return true;
    }
}
