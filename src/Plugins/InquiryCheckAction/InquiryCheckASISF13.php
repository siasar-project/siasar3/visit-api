<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISF13",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The flow rate served (6.11) should not be greater than the sum of all the flow rate collected (2.6).",
 *     message = "The flow served cannot be greater than the sum of all the catchment flows.",
 * )
 */
class InquiryCheckASISF13 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota 1: esta comprobación debe hacerse en las mismas unidades
        // Nota 2: esta comprobación debe sumar todas las captaciones del sistema (i) y todas las distribuciones (j)
        // Nota 3: la comprobación se hará siempre con las últimas mediciones de caudal en B6
        //  SUMA(SIS_F11i) l/s > SUMA(B6.2j) l/s

        $subrecords2 = $this->inquiry->{'field_water_source_intake'};
        $subrecords6 = $this->inquiry->{'field_distribution_infrastructure'};
        $acc2d6d2 = 0;
        $acc6d11 = 0;

        // SIS_B
        foreach ($subrecords2 as $sys2) {
            $sysSubforms2d6 = $sys2->{'field_intake_flow'};
            if (!$sysSubforms2d6) {
                continue;
            }
            $sys2d6 = end($sysSubforms2d6);
            /** @var FlowMeasurement $sys2d6d2 */
            $sys2d6d2 = $sys2d6->{'field_flow'};
            $sys2d6d2->setUnit('liter/second');
            $acc2d6d2 += $sys2d6d2->getValue();
        }
        // SIS_F
        foreach ($subrecords6 as $sys6) {
            /** @var FlowMeasurement $sys6d11d2 */
            $sys6d11d2 = $sys6->{'field_service_flow'};
            $sys6d11d2->setUnit('liter/second');
            $acc6d11 += $sys6d11d2->getValue();
        }

        if ($acc6d11 > $acc2d6d2) {
            $this->logResult();
        }

        return true;
    }
}
