<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use DateTime;

/**
 * @InquiryCheckAction(
 *     id = "ASISG02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The water quality measurement date is more than 5 years old.",
 *     message = "The measurement is more than 5 years old. Check if this is correct or if a more updated measurement is available.",
 * )
 */
class InquiryCheckASISG02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_0.1 – SIS_G6.2 > 5 años
        /** @var \DateTime $date */
        $date = $this->inquiry->{'field_questionnaire_date'};
        $subformRecords = $this->inquiry->{'field_quality_results_physiochemical'};
        foreach ($subformRecords as $sys7d7) {
            $sys7d7d2 = $sys7d7->{'field_test_date'};
            $testDate = \DateTime::createFromFormat('Y-m-d', $sys7d7d2['year'].'-'.$sys7d7d2['month'].'-'.'1');
            if ($testDate) {
                $interval = $testDate->diff($date);
                $yearsDiff = $interval->y;
                $monthsDiff = $interval->m;
                $daysDiff = $interval->d;
                $totalMonthsDiff = ($yearsDiff * 12) + $monthsDiff;

                if (60 < $totalMonthsDiff || (60 === $totalMonthsDiff && 0 < $daysDiff)) {
                    $this->logResult();
                }
            }
        }

        return true;
    }
}
