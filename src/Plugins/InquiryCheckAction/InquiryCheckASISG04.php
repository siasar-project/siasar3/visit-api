<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ASISG04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is not at all common for a spring to have disinfectant monitoring.",
 *     message = "A type 5 system (spring with improved catchment) is very unlikely to have disinfectant monitoring. Check if this is correct.",
 * )
 */
class InquiryCheckASISG04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_A7 = 5 y SIS_G8 = 1
        $sys1d7 = $this->inquiry->{'field_type_system'};
        $sys7d8 = $this->inquiry->{'field_monitoring_residual_disinfectant'};
        if ('5' === $sys1d7 && $sys7d8) {
            $this->logResult();
        }

        return true;
    }
}
