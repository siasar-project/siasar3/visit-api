<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\ConcentrationMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ASISG05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "Concentration is very high.",
 *     message = "The concentration of the disinfectant is too high. Check if the value is correct.",
 * )
 */
class InquiryCheckASISG05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: el valor son 864000 l/día o equivalente en cualquier otra unidad
        //  SIS_G12.2 > 10 ppm
        $subforms = $this->inquiry->{'field_residual_disinfectant_test_results'};
        foreach ($subforms as $sys12d7) {
            /** @var ConcentrationMeasurement $sys12d7d2 */
            $sys12d7d2 = $sys12d7->{'field_concentration'};
            $sys12d7d2->setUnit('particles per million');

            if (10 < $sys12d7d2->getValue()) {
                $this->logResult();
            }
        }

        return true;
    }
}
