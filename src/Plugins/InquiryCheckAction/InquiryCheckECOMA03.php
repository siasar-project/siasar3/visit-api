<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMA03",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "The ratio between population and dwellings is anomalous.",
 *     message = "According to the digitized data, more than 20 people live in the dwellings of this community per dwelling, which is too high as an average (remember that a dwelling is not the same as a building. There can be multiple dwellings in a building).",
 * )
 */
class InquiryCheckECOMA03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // COM_A4 (field_total_population) / COM_A5 (field_total_households) > 20
        $a4 = $this->inquiry->{'field_total_population'};
        $a5 = $this->inquiry->{'field_total_households'};
        if (0 === $a5) {
            return true;
        }
        if (20 < ($a4 / $a5)) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
