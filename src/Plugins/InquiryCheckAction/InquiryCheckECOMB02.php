<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMB02",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "There cannot be more dwellings safely storing water than the total number of dwellings.",
 *     message = "The number of dwellings safely storing water must be equal to or less than the total number of dwellings in the community.",
 * )
 */
class InquiryCheckECOMB02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // COM_B2 > COM_A5
        $householdsStoreDrinkingWater = $this->inquiry->{'field_households_store_drinking_water'};
        $totalHouseHolds = $this->inquiry->{'field_total_households'};
        if ($householdsStoreDrinkingWater > $totalHouseHolds) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
