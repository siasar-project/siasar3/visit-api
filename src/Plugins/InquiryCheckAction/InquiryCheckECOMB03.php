<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMB03",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "There cannot be more dwellings with rainwater tanks than the total number of dwellings.",
 *     message = "The number of houses with rainwater tanks must be equal to or less than the total number of houses in the community.",
 * )
 */
class InquiryCheckECOMB03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // COM_B3.2.1 > COM_A5
        $householdsWithIndividualRainwaterCollection = $this->inquiry->{'field_households_with_individual_rainwater_collection'};
        $totalHouseHolds = $this->inquiry->{'field_total_households'};
        if ($householdsWithIndividualRainwaterCollection > $totalHouseHolds) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
