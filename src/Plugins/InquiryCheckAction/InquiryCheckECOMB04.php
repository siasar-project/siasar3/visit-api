<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ECOMB04",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "I know of no household rainwater tanks larger than 25 cubic meters in rural communities.",
 *     message = "The average volume of domiciliary rainwater tanks is very high. It is suggested to check if they are really domiciliary or if there is an error in the volume data.",
 * )
 */
class InquiryCheckECOMB04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //Nota: litros o equivalente en cualquier otra unidad
        //COM_B3.2.2 > 25000 l
        //COM_B3.2.2 --> field_avg_volume_rainwater_collection_tanks
        /** @var VolumeMeasurement $volume */
        $avgVolumeRainwaterCollectionTanks = $this->inquiry->{'field_avg_volume_rainwater_collection_tanks'};
        $avgVolumeRainwaterCollectionTanks->setUnit('litre');

        if (25000 < $avgVolumeRainwaterCollectionTanks->getValue()) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
