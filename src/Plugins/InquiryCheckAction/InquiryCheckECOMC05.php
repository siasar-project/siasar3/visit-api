<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMC05",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "There cannot be more dwellings with installation than the number of dwellings in the community.",
 *     message = "The number of dwellings with this type of sanitation installation must be equal to or less than the number of dwellings in the community. (3.1.1.5 > 1.5)",
 * )
 */
class InquiryCheckECOMC05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // COM_C1.1.5 > COM_A5
        $c1d1d5 = $this->inquiry->{'field_no_facility_number'};
        $totalHouseHolds = $this->inquiry->{'field_total_households'};

        if ($c1d1d5 > $totalHouseHolds) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
