<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMC13",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "The sum of all dwellings with or without sanitation in table 3.1 must equal the total number of dwellings in the community.",
 *     message = "The number of dwellings with sanitation installation added to the number of dwellings without installation must be equal to the number of dwellings in the community.",
 * )
 */
class InquiryCheckECOMC13 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: X de 1 a 12
        //
        //Código sectorial:
        //SUMA (COM_C1.X.1 ) ≠ COM_A5
        //
        //Código IT:
        //Suma (COM 3.1.1.X + COM 3.1.2.X + COM 3.1.3.X + COM 3.1.4.1 + COM 3.1.5.1 + COM 3.1.6) > COM 1.5
        $totalHouseholds = $this->inquiry->{'field_total_households'};
        $amountField =
                    // 3.1.1.1
                    $this->getValueIfEnabled('field_flush_toilets', 'field_sewer_connection_number') +
                    // 3.1.1.2
                    $this->getValueIfEnabled('field_flush_toilets', 'field_septic_tank_number') +
                    // 3.1.1.3
                    $this->getValueIfEnabled('field_flush_toilets', 'field_pit_latrine_number') +
                    // 3.1.1.4
                    $this->getValueIfEnabled('field_flush_toilets', 'field_without_containment_number') +
                    // 3.1.1.5
                    $this->getValueIfEnabled('field_flush_toilets', 'field_unknown_number') +

                    // 3.1.2.1
                    $this->getValueIfEnabled('field_drypit_latrines', 'field_pit_latrine_with_slab_number') +
                    // 3.1.2.2
                    $this->getValueIfEnabled('field_drypit_latrines', 'field_pit_latrine_without_slab_number') +

                    // 3.1.3.1
                    $this->getValueIfEnabled('field_composting_toilets', 'field_twinpit_with_slab_number') +
                    // 3.1.3.2
                    $this->getValueIfEnabled('field_composting_toilets', 'field_twinpit_without_slab_number') +
                    // 3.1.3.3
                    $this->getValueIfEnabled('field_composting_toilets', 'field_other_composting_toilet_number') +

                    // 3.1.4.1
                    $this->getValueIfEnabled('field_bucket', 'field_bucket_number') +

                    // 3.1.6
                    $this->getValueIfEnabled('', 'field_n_hh_other_sanit_type') +

                    // 3.1.5.1
                    $this->getValueIfEnabled('field_no_facility', 'field_no_facility_number');

        $range = $totalHouseholds * 0.05;
        if ($amountField < ($totalHouseholds - $range) || $amountField > ($totalHouseholds + $range)) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
