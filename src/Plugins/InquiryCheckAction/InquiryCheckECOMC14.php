<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMC14",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "If there is no sanitation, there should be no sanitation service provider.",
 *     message = "If there is no sanitation in the community, there can be no sanitation service provision, field 3.2.",
 * )
 */
class InquiryCheckECOMC14 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        /* C2 visible si:
         * - field_flush_toilets: true
         * - field_drypit_latrines: true
         * - field_composting_toilets: true
         * - field_bucket: true
         */
        $flush = $this->inquiry->{'field_flush_toilets'};
        $latrines = $this->inquiry->{'field_drypit_latrines'};
        $composting = $this->inquiry->{'field_composting_toilets'};
        $bucket = $this->inquiry->{'field_bucket'};
        if (!$flush && !$latrines && !$composting && !$bucket) {
            return true;
        }
        // (COM_C1.12.1 = COM_A5) y C2 ≠ 4
        $c1d12d1 = $this->inquiry->{'field_no_facility_number'};
        $a5 = $this->inquiry->{'field_total_households'};
        $c2 = $this->inquiry->{'field_formal_service_provider_offers'};
        if ("4" !== $c2) {
            if ($c1d12d1 === $a5) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
