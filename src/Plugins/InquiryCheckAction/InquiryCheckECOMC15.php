<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMC15",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "There cannot be more homes with installation than the number of homes in the community.",
 *     message = "The number of homes with this type of sanitation installation must be equal to or less than the number of homes in the community.",
 * )
 */
class InquiryCheckECOMC15 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Código sectorial:
        // COM_C1.13.1 > COM_A5
        //
        // Código IT:
        // COM 3.1.6 > COM 1.5
        $f3d1d6 = $this->inquiry->{'field_n_hh_other_sanit_type'};
        $f1d5 = $this->inquiry->{'field_total_households'};
        if ($f3d1d6 > $f1d5) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
