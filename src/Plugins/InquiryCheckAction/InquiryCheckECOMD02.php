<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECOMD02",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "There cannot be more dwellings with soap and water installation than the number of dwellings with installation in the community.",
 *     message = "The number of homes with soap and water installation must be equal to or less than the number of homes with community facilities.",
 * )
 */
class InquiryCheckECOMD02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // COM_D2 > COM_D1
        $com4d2 = $this->inquiry->{'field_with_water_and_soap'};
        $com4d1 = $this->inquiry->{'field_with_basic_handwashing'};

        if ($com4d2 > $com4d1) {
            $this->logResult();

            return false;
        }


        return true;
    }
}
