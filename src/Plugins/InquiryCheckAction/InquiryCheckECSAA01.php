<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAA01",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "There must be at least one person in the personnel category.",
 *     message = "The center must have at least one person on the health and administrative staff.",
 * )
 */
class InquiryCheckECSAA01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_A5.1 + CSA_A5.2 = 0
        //CSA_A5.1 => field_female_employees
        //CSA_A5.2 => field_male_employees

        $femaleEmployees = $this->inquiry->{'field_female_employees'};
        $maleEmployees = $this->inquiry->{'field_male_employees'};
        $sumFields = $femaleEmployees + $maleEmployees;
        if (0 === $sumFields) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
