<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAA02",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "There must be at least one patient",
 *     message = "The center cannot have no patients. In case the center is currently closed, it is suggested to consider not including it in SIASAR or to put here the number of patients it could receive.",
 * )
 */
class InquiryCheckECSAA02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_A6.1 + ECS_A6.2 = 0
        //ECS_A6.1 => field_female_patients_per_day
        //ECS_A6.2 => field_male_patients_per_day

        $femalePatientsPerDay = $this->inquiry->{'field_female_patients_per_day'};
        $malePatientsPerDay = $this->inquiry->{'field_male_patients_per_day'};
        $sumFields = $femalePatientsPerDay + $malePatientsPerDay;
        if (0 === $sumFields) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
