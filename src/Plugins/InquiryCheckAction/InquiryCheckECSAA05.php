<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAA05",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "The number of patients seems excessive",
 *     message = "The average number of female patients is too high (more than 10,000 patients per day).",
 * )
 */
class InquiryCheckECSAA05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_A6.1 > 10000
        //CSA_A6.1 => field_female_patients_per_day
        $femalePatientsPerDay = $this->inquiry->{'field_female_patients_per_day'};
        if (10000 < $femalePatientsPerDay) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
