<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAB01",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "If you have a water supply, you cannot select that you do not have a water supply.",
 *     message = "If water supply is available, you cannot mark 2.1 as not available at the same time.",
 * )
 */
class InquiryCheckECSAB01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // (ECS_B1 = 1 y/o ECS_B1 = 2) y ECS_B1 = 3
        $b1 = $this->inquiry->{'field_have_water_supply'};
        if ($b1) {
            if (in_array('3', $b1) && (in_array('1', $b1) || in_array('2', $b1))) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
