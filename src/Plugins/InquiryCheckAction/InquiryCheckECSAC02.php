<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAC02",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "There cannot be more usable facilities than number of facilities",
 *     message = "The number of usable sanitation facilities cannot be greater than the number of usable facilities. (3.3.2.2 > 3.3.2.1)",
 * )
 */
class InquiryCheckECSAC02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_C3.2.2 > CSA_C3.2.1
        //CSA_C3.2.2 => field_usable_toilet_men
        //CSA_C3.2.1 => field_total_toilet_men
        $usableToiletMen = $this->inquiry->{'field_usable_toilet_men'};
        $totalToiletMen = $this->inquiry->{'field_total_toilet_men'};
        if ($usableToiletMen > $totalToiletMen) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
