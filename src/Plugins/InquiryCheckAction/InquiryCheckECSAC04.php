<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAC04",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "If there are installations, the total number of installations cannot be zero.",
 *     message = "There must be one or more sanitation facilities in the center.",
 * )
 */
class InquiryCheckECSAC04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_C3.1.1 + CSA_C3.2.1 + CSA_C3.3.1 = 0
        //CSA_C3.1.1 => field_total_toilet_women
        //CSA_C3.2.1 => field_total_toilet_men
        //CSA_C3.3.1 => field_total_toilet_neutral
        $haveToilets = $this->inquiry->{'field_have_toilets'};
        if ($haveToilets) {
            $totalToiletWomen = $this->inquiry->{'field_total_toilet_women'};
            $totalToiletMen = $this->inquiry->{'field_total_toilet_men'};
            $totalToiletNeutral = $this->inquiry->{'field_total_toilet_neutral'};
            $sumfields = $totalToiletWomen + $totalToiletMen + $totalToiletNeutral;
            if (0 === $sumfields) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
