<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAC07",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "The number of facilities is excessive",
 *     message = "The number of installations of this type is too high (3.3.3.1 > 250)",
 * )
 */
class InquiryCheckECSAC07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_C3.3.1 > 250
        //CSA_C3.3.1 => field_total_toilet_neutral
        $totalToiletNeutral = $this->inquiry->{'field_total_toilet_neutral'};
        if (250 < $totalToiletNeutral) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
