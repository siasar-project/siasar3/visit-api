<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAC08",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "If there is a feminine hygiene facility in 3.5, then at least one female or mixed facility must have been provided.",
 *     message = "If there is a facility for feminine hygiene, then in 3.3 there must be at least one facility usable for women or for common use.",
 * )
 */
class InquiryCheckECSAC08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_C5 = 1 y
        //(CSA_C3.1.2 + CSA_C3.3.2) = 0
        //CSA_C5 => field_toilets_menstrual_hygiene_facilities
        //CSA_C3.1.2 => field_usable_toilet_women
        //CSA_C3.3.2 => field_usable_toilet_neutral

        $toiletsMenstrualHygieneFacilities = $this->inquiry->{'field_toilets_menstrual_hygiene_facilities'};
        $usableToiletWomen = $this->inquiry->{'field_usable_toilet_women'};
        $usableToiletNeutral = $this->inquiry->{'field_usable_toilet_neutral'};
        $sumFields = $usableToiletWomen + $usableToiletNeutral;
        if (1 === $toiletsMenstrualHygieneFacilities && 0 === $sumFields) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
