<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ECSAC09",
 *     active = true,
 *     level = "error",
 *     form = "form.health.care",
 *     observation = "If there is a facility for persons with reduced mobility in 3.6, then at least one usable facility must have been placed in 3.3.",
 *     message = "If there is a facility for people with reduced mobility or vision, then in 3.3 there must be at least one usable facility.",
 * )
 */
class InquiryCheckECSAC09 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //CSA_C6 = 1 y
        //(CSA_C3.1.2 + CSA_C3.2.2 + CSA_C3.3.2) = 0
        //CSA_C6 => field_toilets_limited_mobility
        //CSA_C3.1.2 => field_usable_toilet_women
        //CSA_C3.2.2 => field_usable_toilet_men
        //CSA_C3.3.2 => field_usable_toilet_neutral

        $toiletsLimitedMobility = $this->inquiry->{'field_toilets_limited_mobility'};
        $usableToiletWomen = $this->inquiry->{'field_usable_toilet_women'};
        $usableToiletMen = $this->inquiry->{'field_usable_toilet_men'};
        $usableToiletNeutral = $this->inquiry->{'field_usable_toilet_neutral'};
        $sumFields = $usableToiletWomen + $usableToiletMen + $usableToiletNeutral;
        if (1 === $toiletsLimitedMobility && 0 === $sumFields) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
