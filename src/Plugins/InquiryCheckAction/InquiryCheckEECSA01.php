<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSA01",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "There must be at least one person in the personnel category.",
 *     message = "The center must have at least one person on the teaching and administrative staff.",
 * )
 */
class InquiryCheckEECSA01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // ECS_A5.1 + ECS_A5.2 = 0

        $amountField = $this->inquiry->{'field_staff_total_number_of_women'} + $this->inquiry->{'field_staff_total_number_of_men'};
        if (0 === $amountField) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
