<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSA02",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "There must be at least one student",
 *     message = "The center cannot have no students. In the case that the center is currently closed, it is suggested to consider not including it in SIASAR or to put here the number of students that could be accommodated.",
 * )
 */
class InquiryCheckEECSA02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // ECS_A6.1 + ECS_A6.2 = 0
        if (isset($this->inquiry->{'field_student_total_number_of_female'}) && isset($this->inquiry->{'field_student_total_number_of_male'})) {
            $studentTotalNumberOfFemale = $this->inquiry->{'field_student_total_number_of_female'};
            $studentTotalNumberOfMale = $this->inquiry->{'field_student_total_number_of_male'};
            $sumStudentsMaleYFemale = $studentTotalNumberOfFemale + $studentTotalNumberOfMale;
            if (0 === $sumStudentsMaleYFemale) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
