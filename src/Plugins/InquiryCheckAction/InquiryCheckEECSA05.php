<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSA05",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "The number of students seems excessive",
 *     message = "The number of female students is too high (more than 10,000 female students).",
 * )
 */
class InquiryCheckEECSA05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // ECS_A6.1 > 10000
        $studentTotalNumberOfFemale = $this->inquiry->{'field_student_total_number_of_female'};
        if (10000 < $studentTotalNumberOfFemale) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
