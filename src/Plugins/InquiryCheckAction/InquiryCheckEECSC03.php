<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSC03",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "There cannot be more usable facilities than number of facilities",
 *     message = "The number of usable sanitation facilities may not be greater than the number of facilities (3.3.3.2 > 3.3.3.1)",
 * )
 */
class InquiryCheckEECSC03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_C3.3.2 > ECS_C3.3.1
        $commonUseToiletsUsable = $this->inquiry->{'field_common_use_toilets_usable'};
        $commonUseToiletsTotal = $this->inquiry->{'field_common_use_toilets_total'};

        if ($commonUseToiletsUsable > $commonUseToiletsTotal) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
