<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSC04",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "If there are installations, the total number of installations cannot be zero.",
 *     message = "There must be one or more sanitation facilities in the center.",
 * )
 */
class InquiryCheckEECSC04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_C3.1.1  + ECS_C3.2.1 + ECS_C3.3.1 = 0
        //C3.1.1 => field_girls_only_toilets_total
        //C3.2.1 => field_boys_only_toilets_total
        //C3.3.1 => field_common_use_toilets_total
        $haveToilets = $this->inquiry->{'field_school_have_toilets'};
        if ($haveToilets) {
            $girlsOnlyToiletsTotal = $this->inquiry->{'field_girls_only_toilets_total'};
            $boysOnlyToiletsTotal = $this->inquiry->{'field_boys_only_toilets_total'};
            $commonUseToiletsTotal = $this->inquiry->{'field_common_use_toilets_total'};
            $sumTotalToilets =  $girlsOnlyToiletsTotal + $boysOnlyToiletsTotal + $commonUseToiletsTotal;
            if (0 === $sumTotalToilets) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
