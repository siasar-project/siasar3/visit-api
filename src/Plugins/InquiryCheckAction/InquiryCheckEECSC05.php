<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSC05",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "The number of facilities is excessive",
 *     message = "The number of installations of this type is too high (3.3.1.1)",
 * )
 */
class InquiryCheckEECSC05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_C3.1.1 > 250
        //C3.1.1 => field_girls_only_toilets_total

        $girlsOnlyToiletsTotal = $this->inquiry->{'field_girls_only_toilets_total'};
        if (250 < $girlsOnlyToiletsTotal) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
