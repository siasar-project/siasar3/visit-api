<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSC08",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "If there is a feminine hygiene facility in 3.5, then at least one girl's or mixed facility must have been put in place.",
 *     message = "If there is a facility for feminine hygiene, then in 3.3 there must be at least one usable facility for girls or common use.",
 * )
 */
class InquiryCheckEECSC08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_C5  = 1 y
        //(ECS_C3.1.2  + ECS_C3.3.2) = 0
        //ECS_C5 => field_toilets_with_menstrual_hygiene_facilities
        //ECS_C3.1.2 => field_girls_only_toilets_usable
        //ECS_C3.3.2 => field_common_use_toilets_usable

        $toiletsWithMenstrualhygieneFacilities = $this->inquiry->{'field_toilets_with_menstrual_hygiene_facilities'};
        $sumFields =  $this->inquiry->{'field_girls_only_toilets_usable'}  + $this->inquiry->{'field_common_use_toilets_usable'};
        if (0 === $toiletsWithMenstrualhygieneFacilities && 0 === $sumFields) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
