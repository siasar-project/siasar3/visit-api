<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSD01",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "If facilities do not have soap, menstrual hygiene facilities cannot have soap.",
 *     message = "If no hand-washing facilities have soap, those reserved for menstrual hygiene cannot have soap either.",
 * )
 */
class InquiryCheckEECSD01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_D2 = 2 y
        // (ECS_D3 = 1 o ECS_D3 = 3)
        //ECS_D2 => field_handwashing_facilities_soap_water
        //ECS_D3 => field_menstrual_facilities_soap_water
        $handwashingFacilitiesSoapWater = $this->inquiry->{'field_handwashing_facilities_soap_water'};
        $menstrualFacilitiesSoapWater = $this->inquiry->{'field_menstrual_facilities_soap_water'};
        if (2 === $handwashingFacilitiesSoapWater) {
            switch ($menstrualFacilitiesSoapWater) {
                case 1:
                    $this->logResult();

                    return false;
                case 3:
                    $this->logResult();

                    return false;
            }
        }

        return true;
    }
}
