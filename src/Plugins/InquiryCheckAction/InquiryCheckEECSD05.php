<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EECSD05",
 *     active = true,
 *     level = "error",
 *     form = "form.school",
 *     observation = "4.4 = 1 requires toilets to be installed",
 *     message = "If 'girls toilets have containers reserved for menstrual hygiene waste, flush toilets should be provided as the majority installation in 3.2.",
 * )
 */
class InquiryCheckEECSD05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //ECS_D4 = 1
        //ECS_C2 ≠ 1
        //ECS_D4 => field_menstrual_covered_bins
        //ECS_C2 => field_type_toilets

        $menstrualCoveredBins = $this->inquiry->{'field_menstrual_covered_bins'};
        $typeToilets = $this->inquiry->{'field_type_toilets'};

        if (1 === $menstrualCoveredBins && 1 !== $typeToilets) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
