<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "EHOGA01",
 *     active = true,
 *     level = "error",
 *     form = "form.community.household",
 *     observation = "I do not know of household rainwater tanks of more than 25 cubic meters in the rural context",
 *     message = "The volume of the domestic rainwater tank is very high. It is suggested to check if they really are domicile or if there is an error in the volume data.",
 * )
 */
class InquiryCheckEHOGA01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: litros o equivalente en cualquier otra unidad
        // HOG_HA6.2 > 25000 l
        // 1.6.2 => 1.6.2.2 => field_volume_rainwater_collection

        /** @var VolumeMeasurement $volume */
        $volume = $this->inquiry->{'field_volume_rainwater_collection'};
        if ($volume) {
            $volume->setUnit('litre');
            if (25000 < $volume->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
