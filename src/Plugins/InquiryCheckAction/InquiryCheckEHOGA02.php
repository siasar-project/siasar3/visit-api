<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EHOGA02",
 *     active = true,
 *     level = "error",
 *     form = "form.community.household",
 *     observation = "Excessive water collection time.",
 *     message = "The water collection time is more than twelve hours.",
 * )
 */
class InquiryCheckEHOGA02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // HOG_HA10.2 > 720
        if (720 < $this->inquiry->{'field_time_take_get_water_minutes'}) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
