<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "EHOGA21",
 *     active = false,
 *     level = "error",
 *     form = "form.community.household",
 *     observation = "Field 2.1 is required, 'What kind of toilet facility do members of your household usually use?'",
 *     message = "Field 2.1 is required, 'What kind of toilet facility do members of your household usually use?'",
 * )
 */
class InquiryCheckEHOGA21 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $value = $this->inquiry->{'field_toilet_facility'};
        if (!$value) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
