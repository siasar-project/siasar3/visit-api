<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "EHOGA33",
 *     active = true,
 *     level = "error",
 *     form = "form.community.household",
 *     observation = "Field 3.3 is required, 'Observe availability of soap or detergent at the place for handwashing'",
 *     message = "Field 3.3 is required, 'Observe availability of soap or detergent at the place for handwashing'",
 * )
 */
class InquiryCheckEHOGA33 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visible = $visibleResolver->isVisible('field_observed_soap_at_place', '');
        if ($visible) {
            $value = $this->inquiry->{'field_observed_soap_at_place'};
            if (!$value) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
