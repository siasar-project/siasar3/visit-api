<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPATA01",
 *     active = true,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "There cannot be more WSPs served than WSPs in the care area.",
 *     message = "There cannot be more service providers served than the number of service providers the TAP must serve.",
 * )
 */
class InquiryCheckEPATA01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PAT_A8 > PAT_A7
        // 1.8 => field_number_tap_supported_last_12_months
        // 1.7 => field_number_tap_should_assist

        if ($this->inquiry->{'field_number_tap_supported_last_12_months'} > $this->inquiry->{'field_number_tap_should_assist'}) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
