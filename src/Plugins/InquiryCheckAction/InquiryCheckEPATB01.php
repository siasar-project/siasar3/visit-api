<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPATB01",
 *     active = true,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "If there are contracted personnel, then at 2.2 there must be at least one person.",
 *     message = "The number of hired personnel has been indicated, but the sum of male and female personnel (2.2) is zero.",
 * )
 */
class InquiryCheckEPATB01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //PAT_B1 = 1 y
        //PAT_B2.1 + PAT_B2.2 = 0

        if (1 === $this->inquiry->{'field_have_personnel_provide_assistance'}) {
            $amountField = $this->inquiry->{'field_number_women'} + $this->inquiry->{'field_number_men'};

            if (0 === $amountField) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
