<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Tools\InquiryConditionalResolver;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPATC10",
 *     active = true,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "The number of WSPs served by this action cannot be greater than the number of WSPs served.",
 *     message = "The number of service providers served by this specific function, 3.2.7.2, cannot exceed the total number of providers served (1.8).",
 * )
 */
class InquiryCheckEPATC10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PAT_C2.7.2 > PAT_A8
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visible3d2d7d2 = $visibleResolver->isVisible('field_providing_ta_innovation_assisted', '');
        if (!$visible3d2d7d2) {
            return true;
        }
        $tap3d2d7d2 = $this->inquiry->{'field_providing_ta_innovation_assisted'};
        $tap1d8 = $this->inquiry->{'field_number_tap_supported_last_12_months'};
        if ($tap3d2d7d2 > $tap1d8) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
