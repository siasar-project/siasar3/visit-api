<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPATC11",
 *     active = true,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "If the TAP has competencies in the design phase, it must have some function marked in 3.1",
 *     message = "It has been indicated that the TAP has functions in the planning and design phase, but no competence or function has been indicated in said phase",
 * )
 */
class InquiryCheckEPATC11 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PAT_A5 = 1 y
        // PAT_C1.1.0 = 2 y
        // PAT_C1.2.0 = 2 y
        // PAT_C1.3.0 = 2
        $a5 = $this->inquiry->{'field_what_phase_provide_technical_assistance'};
        $c1d1 = $this->inquiry->{'field_assistance_provided_planning'};
        $c1d2 = $this->inquiry->{'field_assistance_provided_supervision'};
        $c1d3 = $this->inquiry->{'field_assistance_provided_creation'};

        if (is_array($a5) && in_array('1', $a5) && !$c1d1 && !$c1d2 && !$c1d3) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
