<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPATC12",
 *     active = true,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "If the TAP has competences in the post construction phase, it must have some function marked in 3.1",
 *     message = "It has been indicated that the TAP has functions in the post-construction phase, but no competence or function has been indicated in said phase.",
 * )
 */
class InquiryCheckEPATC12 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PAT_A5 = 2 y
        // PAT_C2.1.0 = 2 y
        // PAT_C2.2.0 = 2 y
        // PAT_C2.3.0 = 2 y
        // PAT_C2.4.0 = 2 y
        // PAT_C2.5.0 = 2 y
        // PAT_C2.6.0 = 2 y
        // PAT_C2.7.0 = 2
        $a5 = $this->inquiry->{'field_what_phase_provide_technical_assistance'};
        $c2d1 = $this->inquiry->{'field_providing_ta_system'};
        $c2d2 = $this->inquiry->{'field_providing_ta_disinfection'};
        $c2d3 = $this->inquiry->{'field_providing_ta_preventive'};
        $c2d4 = $this->inquiry->{'field_providing_ta_administrative'};
        $c2d5 = $this->inquiry->{'field_providing_ta_social'};
        $c2d6 = $this->inquiry->{'field_providing_ta_evaluation'};
        $c2d7 = $this->inquiry->{'field_providing_ta_innovation'};

        if (is_array($a5) && in_array('2', $a5) && !$c2d1 && !$c2d2 && !$c2d3 && !$c2d4 && !$c2d5 && !$c2d6 && !$c2d7) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
