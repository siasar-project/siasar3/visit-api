<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Entity\Currency;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FieldTypes\Types\SubformFieldType;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\CurrencyAmount;

/**
 * @InquiryCheckAction(
 *     id = "EPSAA01",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "All currency fields must use the same currency",
 *     message = "All currency fields must use the same currency.",
 * )
 */
class InquiryCheckEPSAA01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $currencyFields = $this->getCurrencyFields($this->inquiry->getForm());

        if (!$this->isOk($currencyFields, $this->inquiry)) {
            $this->logResult();

            return false ;
        }

        return true;
    }

    /**
     * Have the record all currency fields with the same currency?
     *
     * @param array         $currencyFields
     * @param FormRecord    $record
     * @param Currency|null $refCurrency
     *
     * @return bool
     */
    protected function isOk(array $currencyFields, FormRecord $record, Currency $refCurrency = null): bool
    {
        // Validate current record.
        /** @var FieldTypeInterface $field */
        foreach ($currencyFields[$record->getForm()->getId()]["currency"] as $field) {
            /** @var CurrencyAmount $value */
            $value = $record->{$field->getId()};
            if ($value) {
                if (!$refCurrency) {
                    $refCurrency = $value->currency;
                } else {
                    // This condition "INT(0) !== FLOAT(0.0)" is TRUE, we need that FLOAT(0.0) will be equal to INT(0).
                    // phpcs:ignore
                    if (0 != $value->amount && $refCurrency->getName() !== $value->currency->getName()) {
                        return false;
                    }
                }
            }
        }
        // Validate sub-records.

        return true;
    }

    /**
     * Get all form currency, and subforms, fields.
     *
     * [
     *  'form.wsprovider' => [
     *      'currency' => [<field>, ...],
     *      'subform' => [
     *          'field' => ??,
     *          'form.wsprovider.??' => [
     *              'currency' => [<field>, ...],
     *              'subform' => [
     *                  ...
     *              ],
     *          ],
     *      ],
     *  ],
     * ]
     *
     * @param FormManagerInterface $form
     * @param bool                 $isSubForm
     *
     * @return FieldTypeInterface
     *
     * @throws \ReflectionException
     */
    protected function getCurrencyFields(FormManagerInterface $form, bool $isSubForm = false): array
    {
        $fields = $form->getFields();
        if (!$isSubForm) {
            $currencyFields[$form->getId()] = [
                'currency' => [],
                'subform' => [],
            ];
        } else {
            $currencyFields = [];
        }
        foreach ($fields as $field) {
            switch ($field->getFieldType()) {
                case 'currency':
                    $currencyFields[$form->getId()]['currency'][] = $field;
                    break;
                case 'subform_reference':
                    /** @var SubformFieldType $field */
                    $currencyFields[$form->getId()]['subform'][] = [
                        'field' => $field,
                        $field->getSubFormId() => $this->getCurrencyFields($field->getSubformManager(), true),
                    ];
                    break;
            }
        }

        return $currencyFields;
    }
}
