<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAB01",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "The date of election of board members cannot be greater than the date of creation of the lender.",
 *     message = "The date of the last election of board members is older than the date of the lender's creation.",
 * )
 */
class InquiryCheckEPSAB01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //PSA_B2.1 < PSA_B1.1
        //B2.1 => field_last_election_committee_date
        //B1.1 =>field_constitution_date
        $lastElectionCommiteeDate = $this->inquiry->{'field_last_election_committee_date'};
        $constitutionDate = $this->inquiry->{'field_constitution_date'};

        if ($lastElectionCommiteeDate && $constitutionDate) {
            if ($lastElectionCommiteeDate['year'] < $constitutionDate['year']) {
                $this->logResult();

                return false;
            }

            if ($lastElectionCommiteeDate['year'] === $constitutionDate['year']) {
                if ($lastElectionCommiteeDate['month'] < $constitutionDate['month']) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
