<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAB02",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There can be no more lender meetings than days per year.",
 *     message = "The number of meetings of the lender's board of directors is excessive (more than one per day in the last year).",
 * )
 */
class InquiryCheckEPSAB02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //PSA_B2.3 > 365
        $boardMeetsLastYear = $this->inquiry->{'field_board_meets_last_year'};

        if ($boardMeetsLastYear > 365) {
            $this->logResult();

            return false ;
        }

        return true;
    }
}
