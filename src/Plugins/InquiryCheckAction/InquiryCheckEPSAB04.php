<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAB04",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There cannot be more women on the board than positions on the board.",
 *     message = "The number of women on the board of directors exceeds the number of board positions.",
 * )
 */
class InquiryCheckEPSAB04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {

        //PSA_B2.5 > PSA_B2.4
        $womenCommitteeMembers = $this->inquiry->{'field_women_committee_members'};
        $committeeMembers = $this->inquiry->{'field_committee_members'};

        if ($womenCommitteeMembers > $committeeMembers) {
            $this->logResult();

            return false ;
        }

        return true;
    }
}
