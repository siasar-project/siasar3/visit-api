<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAB06",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There cannot be more persons from minorities on the board than positions on the board.",
 *     message = "The number of people from vulnerable, minority or indigenous groups on the board of directors exceeds the number of board positions.",
 * )
 */
class InquiryCheckEPSAB06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_B2.7 > PSA_B2.4
        $wsp2d2d7 = $this->inquiry->{'field_excluded_people_committee_members'};
        $wsp2d2d4 = $this->inquiry->{'field_committee_members'};
        if ($wsp2d2d7 > $wsp2d2d4) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
