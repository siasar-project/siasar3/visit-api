<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAB07",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "If there are hired personnel, then in 2.5 there must be at least one person.",
 *     message = "The number of hired personnel has been indicated, but the sum of male and female personnel (2.5) is zero.",
 * )
 */
class InquiryCheckEPSAB07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //PSA_B4 = 1 y PSA_B5.1 + PSA_B5.2 = 0
        $havePersonnelToRuralService = $this->inquiry->{'field_have_personnel_to_rural_service'};
        $personnelWomen = $this->inquiry->{'field_personnel_women'};
        $personnelMen = $this->inquiry->{'field_personnel_men'};
        $sum = $personnelWomen + $personnelMen;
        if (true === $havePersonnelToRuralService  &&  0 === $sum) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
