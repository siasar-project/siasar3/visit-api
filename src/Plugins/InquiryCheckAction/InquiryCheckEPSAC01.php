<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAC01",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There should be no regulated billing periods longer than one year.",
 *     message = "The billing period of the tariff is higher than one year. There should not be such high periods, so it is suggested to evaluate whether this payment is really a formal fee.",
 * )
 */
class InquiryCheckEPSAC01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C5 > 365
        $wsp3d5 = $this->inquiry->{'field_days_average_billing_cycle'};
        if ($wsp3d5 > 365) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
