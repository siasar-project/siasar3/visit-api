<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "EPSAC03",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "The value of produced water is excessive.",
 *     message = "The volume of water produced is excessive.",
 * )
 */
class InquiryCheckEPSAC03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // Nota: el valor son 25000 metros cúbicos o equivalente en cualquier otra unidad
        // PSA_C6 > 2500000 metros cúbicos
        /** @var VolumeMeasurement $volume */
        $volume = $this->inquiry->{'field_m3_monthly_produced_water'};
        $volume->setUnit('cubic metre');

        if (2500000 < $volume->getValue()) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
