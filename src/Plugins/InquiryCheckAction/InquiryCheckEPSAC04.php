<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAC04",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "The number of users is excessive (100,000, equivalent to a city of half a million inhabitants) and the WSP is neither a large institution nor a private organization.",
 *     message = "The number of users is excessive.",
 * )
 */
class InquiryCheckEPSAC04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // (PSA_A4 ≠ 3 o PSA_A4 ≠ 4) y
        // PSA_C8.1 > 100000

        if ('3' !== $this->inquiry->{'field_provider_type'} || '4' !== $this->inquiry->{'field_provider_type'}) {
            if ($this->inquiry->{'field_users_required_pay_exists'}) {
                if ($this->inquiry->{'field_users_required_pay'} > 100000) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
