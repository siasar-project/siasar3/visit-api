<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAC05",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There cannot be more users per day than number of users.",
 *     message = "The number of users per day must be equal to or less than the number of users.",
 * )
 */
class InquiryCheckEPSAC05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // PSA_C8.2 > PSA_C8.1
        if ($this->inquiry->{'field_users_uptodate_payment'} > $this->inquiry->{'field_users_required_pay'}) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
