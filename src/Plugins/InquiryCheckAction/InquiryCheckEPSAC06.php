<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAC06",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "On average, there can be no more monthly income than billing.",
 *     message = "On average, the monthly ordinary income must be equal to or less than the billing.",
 * )
 */
class InquiryCheckEPSAC06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 3.8.3.2 and 3.8.4.2 must have the same currency unit.');
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // PSA_C8.4 > PSA_C8.3

        $wsp3d8d4d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_revenue'});
        $wsp3d8d3d2 = $this->getCurrencyAmount($this->inquiry->{'field_monthly_amount_billed'});

        if (false === $wsp3d8d4d2 || false === $wsp3d8d3d2) {
            $this->logByCurrency();

            return false;
        }

        if ($wsp3d8d4d2 > $wsp3d8d3d2) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
