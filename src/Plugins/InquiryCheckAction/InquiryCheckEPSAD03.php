<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAD03",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There should not be a partial expense less than the sum of all expenses of this type.",
 *     message = "The sum of the partial operating expenses must be equal to or less than the total operating expenses figure.",
 * )
 */
class InquiryCheckEPSAD03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('All fields in 4.1 Expenses must have the same currency unit.');
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }

        $blocks = [
            'Operacion' => [
                'fragments' => [
                    [
                        // Field names.
                        'in_use' => 'field_chk_expenses_salaries_technical_personnel', // 4.1.5
                        'know' => 'field_expenses_salaries_technical_personnel_exists', // 4.1.5.1
                        'amount' => 'field_expenses_salaries_technical_personnel', // 4.1.5.2
                    ],
                    [
                        // Field names.
                        'in_use' => 'field_chk_expenses_energy_costs', // 4.1.6
                        'know' => 'field_expenses_energy_costs_exists', // 4.1.6.1
                        'amount' => 'field_expenses_energy_costs', // 4.1.6.2
                    ],
                    [
                        // Field names.
                        'in_use' => 'field_chk_expenses_water_treatment_costs', // 4.1.7
                        'know' => 'field_expenses_water_treatment_costs_exists', // 4.1.7.1
                        'amount' => 'field_expenses_water_treatment_costs', // 4.1.7.2
                    ],
                    [
                        // Field names.
                        'in_use' => 'field_chk_expenses_other_costs', // 4.1.8
                        'know' => 'field_expenses_other_costs_exists', // 4.1.8.1
                        'amount' => 'field_expenses_other_costs', // 4.1.8.2
                    ],
                ],
                'total' => [
                    // Field names.
                    'in_use' => 'field_chk_expenses_operations_subtotal', // 4.1.9
                    'know' => 'field_expenses_operations_subtotal_exists', // 4.1.9.1
                    'amount' => 'field_expenses_operations_subtotal', // 4.1.9.2
                ],
            ],
        ];

        // Validate in each block that fragments amounts are equal o less that total amount in block.
        foreach ($blocks as $block) {
            if ($this->isVisibleField($this->inquiry, $block['total']['amount'])) {
                // Sum fragment fields.
                $fragmentsAmount = 0;
                foreach ($block['fragments'] as $item) {
                    if ($this->isVisibleField($this->inquiry, $item['amount'])) {
                        $amount = $this->getCurrencyAmount($this->inquiry->{$item['amount']});
                        if (false === $amount) {
                            $this->logByCurrency();

                            return false;
                        }
                        $fragmentsAmount += $amount;
                    }
                }
                $total = $this->getCurrencyAmount($this->inquiry->{$block['total']['amount']});
                if (false === $total) {
                    $this->logByCurrency();

                    return false;
                }
                if ($fragmentsAmount > $total) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
