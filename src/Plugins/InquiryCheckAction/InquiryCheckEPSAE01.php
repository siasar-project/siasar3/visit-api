<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Entity\Country;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAE01",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "If the periodicity of accountability is not annual, it must be 'other' in 5.10.",
 *     message = "Accountability is greater than one year, so in 5.10 the periodicity can only be 'other'.",
 * )
 */
class InquiryCheckEPSAE01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        /** @var Country $country */
        $country = $this->inquiry->{'field_country'};
        $levels = $country->getFormLevel();
        $formLevel = 1;
        if (isset($levels["form.wsprovider"]["level"])) {
            $formLevel = $levels["form.wsprovider"]["level"];
        }
        if ($formLevel < 2) {
            // Field field_how_financial_disclosed is level 2, not usable in level 1.
            return true;
        }
        //PSA_E9 = 2 y PSA_E10 ≠ 5
        $disclosedFinancialInformation = $this->inquiry->{'field_disclosed_financial_information'};
        $howFinancialDisclosed = $this->inquiry->{'field_how_financial_disclosed'};
        if ('2' === $disclosedFinancialInformation  &&  '5' !== $howFinancialDisclosed) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
