<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAE02",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "There cannot be more complaints resolved than complaints raised.",
 *     message = "The number of claims resolved cannot be greater than the number of claims received.",
 * )
 */
class InquiryCheckEPSAE02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //PSA_E14 > PSA_E13
        $addressedGrievancesTotalNumber = $this->inquiry->{'field_addressed_grievances_total_number'};
        $grievancesTotalNumber = $this->inquiry->{'field_grievances_total_number'};
        if ($addressedGrievancesTotalNumber > $grievancesTotalNumber) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
