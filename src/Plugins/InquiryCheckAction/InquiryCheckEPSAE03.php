<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "EPSAE03",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "Assets and liabilities must add up to the same amount",
 *     message = "In the balance sheet, assets and liabilities should add up to the same amount.",
 * )
 */
class InquiryCheckEPSAE03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $this->logByCurrencyMessage('Fields 5.16.2, 5.17.2, 5.18.2, and 5.19.2 must have the same currency unit.');

        //(PSA_E16 + PSA_E17) ≠ (PSA_E18 + PSA_E19)
        $ed16d2 = $this->getCurrencyAmount($this->inquiry->{'field_current_assets_value'});
        $ed17d2 = $this->getCurrencyAmount($this->inquiry->{'field_notcurrent_assets_value'});
        $ed18d2 = $this->getCurrencyAmount($this->inquiry->{'field_current_liabilities_value'});
        $ed19d2 = $this->getCurrencyAmount($this->inquiry->{'field_notcurrent_liabilities_value'});

        if (false === $ed16d2 ||
            false === $ed17d2 ||
            false === $ed18d2 ||
            false === $ed19d2) {
            $this->logByCurrency();

            return true;
        }

        if (($ed16d2 + $ed17d2) !== ($ed18d2 + $ed19d2)) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
