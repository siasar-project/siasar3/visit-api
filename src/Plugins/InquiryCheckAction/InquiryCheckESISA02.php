<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISA02",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If the system type is a well, the source type cannot be surface water.",
 *     message = "It has been indicated that the system is type 4 (well), so the source cannot be surface. Check source type.",
 * )
 */
class InquiryCheckESISA02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_A7 = 4 y
        // (SIS_B4.1 = 1 o SIS_B4.1 = 3)
        if ('4' === $this->inquiry->{'field_type_system'}) {
            $subformRecords = $this->inquiry->{'field_water_source_intake'};

            foreach ($subformRecords as $record) {
                if ('1' === $record->{'field_source_type'} || '3' === $record->{'field_source_type'}) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
