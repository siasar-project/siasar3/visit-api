<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISA07",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "[field_year_build] Field 1.4 'Year Built' - The construction year must be prior to or the same as the survey year.",
 *     message = "[field_year_build] Field 1.4 'Year Built' - The construction year must be prior to or the same as the survey year.",
 * )
 */
class InquiryCheckESISA07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $questionnaireDate = $this->inquiry->{'field_questionnaire_date'};
        $yearBuild = $this->inquiry->{'field_year_build'};
        if (!empty($yearBuild)) {
            if ($yearBuild > (int) $questionnaireDate->format('Y')) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
