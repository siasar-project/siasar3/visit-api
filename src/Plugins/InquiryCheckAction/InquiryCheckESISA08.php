<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISA08",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "Systems without distribution network in SIASAR can only be linked to one community. Please check the number of served communities. If there is more than one, then consider that the system should be of the 'with distribution' type.",
 *     message = "Systems without distribution network in SIASAR can only be linked to one community. Please check the number of served communities. If there is more than one, then consider that the system should be of the 'with distribution' type.",
 * )
 */
class InquiryCheckESISA08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $subrecords1d12 = $this->inquiry->{'field_served_communities'};
        $haveDistributionNetwork = $this->inquiry->{'field_have_distribution_network'};

        if (!$haveDistributionNetwork) {
            $prueba = count($subrecords1d12);
            if (count($subrecords1d12) >= 2) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
