<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Entity\AdministrativeDivision;
use App\Forms\FormRecord;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISA09",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "",
 *     message = "Distributed communities, 6.2, must be the same as are served communities, 1.12.1.",
 * )
 */
class InquiryCheckESISA09 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        /** @var bool $f1d0 */
        $f1d0 = $this->inquiry->{'field_have_distribution_network'};
        if (!$f1d0) {
            // When the systems questionnaire is selected "without distribution" this rule does NOT apply.
            return true;
        }
        $subRecord = $this->inquiry->{'field_distribution_infrastructure'};
        if (0 < count($subRecord)) {
            /** @var FormRecord[] $f1d12 */
            $f1d12 = $this->inquiry->{'field_served_communities'};
            /** @var FormRecord[] $f6 */
            $f6 = $this->inquiry->{'field_distribution_infrastructure'};

            // We must compare the number of keys, it can be duplicated.
            $f1d12Keys = [];
            $f6Keys = [];
            foreach ($f1d12 as $f1d12Item) {
                $f1d12Keys[$f1d12Item->get('field_community')['value']] = true;
            }
            foreach ($f6 as $f6Item) {
                $f6Keys[$f6Item->get('field_communities_serviced_network')['value']] = true;
            }
            if (count($f1d12Keys) !== count($f6Keys)) {
                $this->logResult();

                return false;
            }

            $served = [];
            // Campo comunidad de 1.12: field_community
            foreach ($f1d12 as $record1d12) {
                /** @var AdministrativeDivision $comm */
                $comm = $record1d12->{'field_community'};
                $served[$comm->getId()] = true;
            }
            // Campo comunidad de 6: field_communities_serviced_network
            foreach ($f6 as $record6) {
                /** @var AdministrativeDivision $comm */
                $comm = $record6->{'field_communities_serviced_network'};
                if (!$comm || !isset($served[$comm->getId()])) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
