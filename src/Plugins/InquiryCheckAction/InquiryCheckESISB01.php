<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISB01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "Water cannot be abstracted from an aquifer with a catchment box.",
 *     message = "The combination of source and catchment is not correct. Water cannot be abstracted from a type 1 groundwater source (aquifer) with a type 1 abstraction (catchment box).",
 * )
 */
class InquiryCheckESISB01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B4.1.2 = 1 y
        //  SIS_B4.2.2 = 1
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            if ('1' === $record->{'field_source_type_groundwater'} && '1' === $record->{'field_source_catchment_type_groundwater'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
