<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISB02",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "You cannot collect water from a spring with a well.",
 *     message = "The combination of source and catchment is not correct. Water cannot be taken from a type 2 groundwater source (spring or spring hole) with a type 2 or type 3 catchment (wells).",
 * )
 */
class InquiryCheckESISB02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B4.1.2 = 2 y
        //  (SIS_B4.2.2 = 2 o SIS_B4.2.2 = 3)
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            if ('2' === $record->{'field_source_type_groundwater'}) {
                if ('2' === $record->{'field_source_catchment_type_groundwater'} || '3' === $record->{'field_source_catchment_type_groundwater'}) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
