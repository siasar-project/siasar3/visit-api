<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * Check ESISB03
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @InquiryCheckAction(
 *     id = "ESISB03",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "Very high flow rate value.",
 *     message = "The flow rate value is too high.",
 * )
 */
class InquiryCheckESISB03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: el valor son 1000 l/s o equivalente en cualquier otra unidad
        //  SIS_B5.2 > 1000 l/s
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            foreach ($record->{'field_source_flow'} as $sourceFlow) {
                /** @var FlowMeasurement $flow */
                $flow = $sourceFlow->{'field_flow'};
                $flow->setUnit('liter/second');

                if (1000 < $flow->getValue()) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
