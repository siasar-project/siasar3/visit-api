<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ESISB05",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "The flow captured by a catchment cannot be greater than that of its source.",
 *     message = "The collected flow cannot be greater than that of the corresponding source.",
 * )
 */
class InquiryCheckESISB05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota 1: esta comprobación debe hacerse en las mismas unidades
        // Nota 2: la comprobación se hará siempre con las últimas mediciones de caudal en B5 y B6
        // Nota 3: esta comprobación no aplica si no existe valor alguno en B5.2
        // B6.2 l/s > B5.2 l/s
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            $sourceFlow = $record->{'field_source_flow'};
            if (!is_null($sourceFlow)) {
                $sourceFlow = end($sourceFlow);
            }
            $intakeFlow = $record->{'field_intake_flow'};
            if (!is_null($intakeFlow)) {
                $intakeFlow = end($intakeFlow);
            }

            if (!$sourceFlow || !$intakeFlow) {
                return true;
            }

            /** @var FlowMeasurement $fieldSource */
            $fieldSource = $sourceFlow->{'field_flow'};
            $fieldSource->setUnit('liter/second');
            /** @var FlowMeasurement $fieldIntake */
            $fieldIntake = $intakeFlow->{'field_flow'};
            $fieldIntake->setUnit('liter/second');

            if ($fieldIntake->getValue() > $fieldSource->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
