<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ESISB06",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "I know of no drinking water well in a community deeper than 250 metres, although the limit has been set at 500 metres just in case.",
 *     message = "The depth of the well is excessive (greater than 500 metres or equivalent unit).",
 * )
 */
class InquiryCheckESISB06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: metros o equivalente en cualquier otra unidad
        //  SIS_B10 > 500 metros
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            /** @var LengthMeasurement $length */
            $length = $record->{'field_well_depth'};
            $length->setUnit('metre');

            if (500 < $length->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
