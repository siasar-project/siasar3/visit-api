<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * Check ESISB07
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @InquiryCheckAction(
 *     id = "ESISB07",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "The pump must be located inside the well.",
 *     message = "The depth at which the pump is located must not be greater than the depth of the well itself. Check both values.",
 * )
 */
class InquiryCheckESISB07 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B12 > SIS_B10
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            if ($record->{'field_well_depth_exists'} && $record->{'field_pump_depth_exists'}) {
                /** @var LengthMeasurement $wellLength */
                $wellLength = $record->{'field_well_depth'};
                $wellLength->setUnit('metre');
                /** @var LengthMeasurement $pumpLength */
                $pumpLength = $record->{'field_pump_depth'};
                $pumpLength->setUnit('metre');

                if ($pumpLength->getValue() > $wellLength->getValue()) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
