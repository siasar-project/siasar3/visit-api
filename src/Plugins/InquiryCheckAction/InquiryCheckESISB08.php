<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "ESISB08",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If there is maintenance (2.14 = 'Yes') then at least one activity must be performed among those listed in fields 2.15.1 and 2.15.2. It would be sufficient to tick one activity in either field.",
 *     message = "If there is maintenance (2.14 = 'Yes') then you must tick at least one activity performed between fields 2.15.1 and 2.15.2.",
 * )
 */
class InquiryCheckESISB08 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_B14 = 1 y
        //  En SIS_B15.1 y/o en SIS_B15.2 no se marca respuesta alguna
        $subformRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subformRecords as $record) {
            $sysB14 = $record->{'field_pump_maintenance_conducted_last_year'};
            if ($sysB14) {
                $visibleResolver = new InquiryConditionalResolver($record);
                $visibleB15d1 = $visibleResolver->isVisible('field_maintenance_all_types', 'field_water_source_intake');
                $visibleB15d2 = $visibleResolver->isVisible('field_maintenance_electrically', 'field_water_source_intake');

                if (($visibleB15d1 && !$record->{'field_maintenance_all_types'}) && ($visibleB15d2 && !$record->{'field_maintenance_electrically'})) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
