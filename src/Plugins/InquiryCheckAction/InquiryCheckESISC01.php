<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * Check ESISC01
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @InquiryCheckAction(
 *     id = "ESISC01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "Driving more than 100 km seems excessive in rural environments.",
 *     message = "The length of the pipeline is excessive.",
 * )
 */
class InquiryCheckESISC01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: metros o equivalente en cualquier otra unidad
        //  SIS_C2 > 100000 metros
        $subformRecords = $this->inquiry->{'field_water_transmision_line'};
        foreach ($subformRecords as $record) {
            /** @var LengthMeasurement $length */
            $length = $record->{'field_length'};
            $length->setUnit('metre');

            if (100000 < $length->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
