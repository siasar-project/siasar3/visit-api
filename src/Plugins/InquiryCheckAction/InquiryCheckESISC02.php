<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ESISC02",
 *     active = false,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "A drive of more than 120 inches in diameter seems excessive in rural environments.",
 *     message = "The diameter of the piping is excessive.",
 * )
 */
class InquiryCheckESISC02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: pulgadas o equivalente en cualquier otra unidad
        //  SIS_C4 > 120 pulgadas
        $subformRecords = $this->inquiry->{'field_water_transmision_line'};
        foreach ($subformRecords as $record) {
            /** @var LengthMeasurement $def */
            $def = $record->{'field_average_diameter'};
            $def->setUnit('inch');

            if (120 < $def->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
