<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISC03",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If there is maintenance (3.7 = 'Yes') then at least one activity must be performed among those listed in field 3.8.",
 *     message = "If there is maintenance (3.7 = 'Yes') then you must mark at least one activity performed in field 3.8.",
 * )
 */
class InquiryCheckESISC03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_C7= 1 y
        //  En SIS_C8 no se marca respuesta alguna
        $subformRecords = $this->inquiry->{'field_water_transmision_line'};
        foreach ($subformRecords as $record) {
            if ($record->{'field_have_maintenance_conducted_last_year'} && !$record->{'field_maintenance_last_year'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
