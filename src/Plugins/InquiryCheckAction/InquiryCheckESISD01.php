<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Sebastian Vargas <jhoan.vargas@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISD01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If one treatment point has the same coordinates as another, then it should be grouped into a single treatment point for the purposes of the SIASAR questionnaire.",
 *     message = "There are two treatment blocks with the same coordinates, so it is the same treatment point with multiple facilities. Please adjust to digitize it as a single block in the questionnaire.",
 * )
 */
class InquiryCheckESISD01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // "Nota: este error solo se produce si hay varios bloques D llenos, es decir, si hay
        // varios puntos de tratamiento levantados (a, b, etc…)
        // SIS_D1a = SIS_D1b"
        $subs = $this->inquiry->{'field_treatment_points'};
        $arrCoordinates = [];
        if (count($subs) > 1) {
            /** @var FormRecord $sub */
            foreach ($subs as $sub) {
                if (!$arrCoordinates) {
                    $arrCoordinates[$sub->getId()]['latitude'] = $sub->{'field_infrastructure_latitude'};
                    $arrCoordinates[$sub->getId()]['longitude'] = $sub->{'field_infrastructure_longitude'};
                }

                foreach ($arrCoordinates as $key => $coordinates) {
                    if ($key === $sub->getId()) {
                        continue;
                    }
                    if ($sub->{'field_infrastructure_latitude'} === $coordinates['latitude'] && $sub->{'field_infrastructure_longitude'} === $coordinates['longitude']) {
                        $this->logResult();

                        return false;
                    }
                    $arrCoordinates[$sub->getId()]['latitude'] = $sub->{'field_infrastructure_latitude'};
                    $arrCoordinates[$sub->getId()]['longitude'] = $sub->{'field_infrastructure_longitude'};
                }
            }
        }

        return true;
    }
}
