<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISD02",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "Disinfectant cannot be used in the tank if there is no tank.",
 *     message = "It has been indicated that the disinfectant is applied in the tank, but no tank has been digitized in the system.",
 * )
 */
class InquiryCheckESISD02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_D4 = 5 y
        // SIS_E no está cubierto
        $subformRecords = $this->inquiry->{'field_treatment_points'};

        foreach ($subformRecords as $record) {
            if ('5' === $record->{'field_typology_chlorination'} && !$this->inquiry->{'field_storage_infrastructure'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
