<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ESISD04",
 *     active = false,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "A tank of more than 100,000 cubic meters only makes sense in very large cities; it does not seem logical in a rural context.",
 *     message = "Storage tank volume is excessive.",
 * )
 */
class InquiryCheckESISD04 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: metros cúbicos o equivalente en cualquier otra unidad
        // SIS_D4 > 100000 metros cúbicos
        $subformRecords = $this->inquiry->{'field_treatment_points'};
        foreach ($subformRecords as $record) {
            /** @var LengthMeasurement $length */
            $length = $record->{'field_length'};
            $length->setUnit('cubic meters');

            if (100000 < $length->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
