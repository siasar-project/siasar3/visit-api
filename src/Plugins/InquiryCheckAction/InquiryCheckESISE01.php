<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISE01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If there is maintenance (5.6 = 'Yes') then at least one activity must be performed among those listed in field 5.7.",
 *     message = "If there is maintenance (5.6 = 'Yes') then you must mark at least one activity performed in field 5.7.",
 * )
 */
class InquiryCheckESISE01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_E5= 1 y
        // En SIS_E6 no se marca respuesta alguna
        //E5 => field_storage_maintenance_conducted_last_year
        //E6 => field_storage_maintenance_last_year

        $subformRecords = $this->inquiry->{'field_storage_infrastructure'};
        foreach ($subformRecords as $record) {
            if (1 === $record->{'field_storage_maintenance_conducted_last_year'}) {
                if (!$record->{'field_storage_maintenance_last_year'}) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
