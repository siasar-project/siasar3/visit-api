<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISF01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "In the SIASAR 3 model, each distribution block is linked to one and only one community, and viceversa.",
 *     message = "Two distributions have been joined to the same community. In SIASAR, each community is served by a single distribution block that groups together all the pipes or facilities that supply water to that community.",
 * )
 */
class InquiryCheckESISF01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: este error solo se produce si hay varios bloques F llenos, es decir, si hay varias distribuciones levantadas (a, b, etc…)
        //  SIS_F2a = SIS_F2b
        $condition = ($this->inquiry->{'field_have_distribution_network'});
        if (!$condition) {
            return true;
        }
        $communities = [];
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        if (1 < count($subformRecords)) {
            foreach ($subformRecords as $record) {
                $community = $record->{'field_communities_serviced_network'};
                if ($community && !in_array($community->getId(), $communities)) {
                    $communities[] = $community->getId();
                } else {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
