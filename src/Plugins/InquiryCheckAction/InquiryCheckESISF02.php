<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\Measurements\LengthMeasurement;

/**
 * @InquiryCheckAction(
 *     id = "ESISF02",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "A distribution of more than 100 km seems excessive in rural environments.",
 *     message = "The length of the distribution is too long.",
 * )
 */
class InquiryCheckESISF02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: metros o equivalente en cualquier otra unidad
        // SIS_F3 > 100000 metros
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subformRecords as $record) {
            /** @var LengthMeasurement $length */
            $length = $record->{'field_network_length'};
            $length->setUnit('metre');

            if (100000 < $length->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
