<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISF03",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If the distribution is not 100% domiciliary, then there must be some housing without domiciliary distribution.",
 *     message = "It has been indicated in 6.5 that the distribution is not fully domiciliary, so the number of dwellings with domiciliary connection (6.6) should be less than the number of dwellings served.",
 * )
 */
class InquiryCheckESISF03 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esta comprobación se hace contra la comunidad (i) servida por esa distribución específica
        // SIS_F5 = 3 y
        // SIS_F6 ≥ SIS_A.12.3i
        $subrecords1d12 = $this->inquiry->{'field_served_communities'};
        $subrecords6 = $this->inquiry->{'field_distribution_infrastructure'};

        foreach ($subrecords6 as $sys6) {
            $sys6d5 = $sys6->{'field_serve_household'};
            if ('3' === $sys6d5) {
                $sys6d2 = $sys6->{'field_communities_serviced_network'}; // administrative_division_reference
                if (!$sys6d2) {
                    return true;
                }
                foreach ($subrecords1d12 as $sys1d12) {
                    $sys1d12d1 = $sys1d12->{'field_community'}; // administrative_division_reference
                    if ($sys1d12d1->getId() === $sys6d2->getId()) {
                        $sys1d12d3 = $sys1d12->{'field_households'};
                        $sys6d6 = $sys6->{'field_households_connection'};

                        if ($sys6d6 >= $sys1d12d3) {
                            $this->logResult();

                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}
