<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISF05",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "There cannot be more homes with micro-metering than homes served.",
 *     message = "The number of homes with micro-metering must be equal to or less than the number of homes served.",
 * )
 */
class InquiryCheckESISF05 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        //Nota: esta comprobación se hace contra la comunidad (i) servida por esa distribución específica
        //SIS_F9.1 > SIS_F6
        //F9.1 => field_how_many_micrometers
        //F6 => field_households_connection
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subformRecords as $record) {
            if ($record->{'field_how_many_micrometers'} > $record->{'field_households_connection'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
