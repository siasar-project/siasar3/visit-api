<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISF06",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "There cannot be more homes with a working micro-meter than homes with a micro-meter.",
 *     message = "The number of dwellings with operational micro-metering must be equal to or less than the number of dwellings with micro-metering.",
 * )
 */
class InquiryCheckESISF06 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_F9.2 > SIS_F9.1
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subformRecords as $record) {
            if ($record->{'field_how_many_micrometers_billing'} > $record->{'field_how_many_micrometers'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
