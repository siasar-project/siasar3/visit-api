<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISF09",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If there is maintenance (6.14 = 'Yes') then at least one activity must be performed among those listed in field 6.15.",
 *     message = "If there is maintenance (6.14 = 'Yes') then you must mark at least one activity performed in field 6.15.",
 * )
 */
class InquiryCheckESISF09 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_F14= 1 y
        // En SIS_F15 no se marca respuesta alguna
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subformRecords as $record) {
            if ($record->{'field_maintenance_activities'} && !$record->{'field_service_maintenance_conducted_last_year'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
