<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISF10",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If the distribution is 100% residential, then the number of dwellings with residential distribution must be exactly the same number of dwellings served",
 *     message = "It has been indicated in 6.5 that the distribution is totally residential, so the number of dwellings with household connection (6.6) must be equal to the number of dwellings served",
 * )
 */
class InquiryCheckESISF10 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esta comprobación se hace contra la comunidad (i) servida por esa distribución específica

        // SIS_F5 (field_serve_household) = 1 y
        // SIS_F6 (field_households_connection) ≠ SIS_A.12.3i (field_households)
        $subrecords1d12 = $this->inquiry->{'field_served_communities'};
        $subrecords6 = $this->inquiry->{'field_distribution_infrastructure'};

        foreach ($subrecords6 as $sys6) {
            $sys6d2 = $sys6->{'field_communities_serviced_network'}; // administrative_division_reference
            $sys6d5 = $sys6->{'field_serve_household'};
            $sys6d6 = $sys6->{'field_households_connection'};
            if (!$sys6d2) {
                return true;
            }
            if ('1' === $sys6d5) {
                foreach ($subrecords1d12 as $sys1d12) {
                    $sys1d12d1 = $sys1d12->{'field_community'}; // administrative_division_reference
                    if ($sys1d12d1->getId() === $sys6d2->getId()) {
                        $sys1d12d3 = $sys1d12->{'field_households'};
                        if ($sys6d6 !== $sys1d12d3) {
                            $this->logResult();

                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}
