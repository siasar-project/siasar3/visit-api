<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISG01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If there is monitoring of the disinfectant substance it is because you have to do a disinfection installation or activity.",
 *     message = "It has been indicated that the disinfectant substance is monitored but no facility has been included to supply disinfectant in the system.",
 * )
 */
class InquiryCheckESISG01 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // SIS_G8 = 1 y
        // SIS_E no está cubierto
        if (1 === $this->inquiry->{'field_monitoring_residual_disinfectant'} && !$this->inquiry->{'field_storage_infrastructure'}) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
