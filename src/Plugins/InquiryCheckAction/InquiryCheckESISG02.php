<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "ESISG02",
 *     active = false,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "The disinfectant substance monitored must be the same as that used in the system.",
 *     message = "The monitored disinfected substance does not match the substance applied in the system (in field 4.5).",
 * )
 */
class InquiryCheckESISG02 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // NOTE: Disabled because it isn't possible to compare this fields.

        // SIS_G12.1 ≠ SIS_D5
        $subformRecords = $this->inquiry->{'field_residual_disinfectant_test_results'};
        $subFormTreatmentPoints = $this->inquiry->{'field_treatment_points'};

        foreach ($subformRecords as $record) {
            if ($record->{'field_type'} !== $subFormTreatmentPoints->{'field_disinfecting_substance'}) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
