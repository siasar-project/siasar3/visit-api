<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FC001",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "Number of households without communal water systems is required if is visible.",
 *     message = "Field '2.1 Number of households without communal water systems' is required.",
 * )
 */
class InquiryCheckFC001 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $haveHouseholds = $this->inquiry->{'field_have_households'};
        if (!$haveHouseholds) {
            $field = $this->inquiry->getFieldDefinition('field_households_without_water_supply_system');
            if ($field->isEmpty()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
