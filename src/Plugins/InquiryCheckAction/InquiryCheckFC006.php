<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FC006",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "Is there at least one type of formal service provider that offers collection, transport and/or treatment services of excreta/wastewater to the community? is required if is visible.",
 *     message = "Field '3.2 Is there at least one type of formal service provider that offers collection, transport and/or treatment services of excreta/wastewater to the community?' is required.",
 * )
 */
class InquiryCheckFC006 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = (
            $this->inquiry->{'field_flush_toilets'} ||
            $this->inquiry->{'field_drypit_latrines'} ||
            $this->inquiry->{'field_composting_toilets'} ||
            $this->inquiry->{'field_bucket'}
        );

        $ad5 = $this->inquiry->{'field_total_households'};
        $c1d5 = $this->inquiry->{'field_no_facility'};
        $c1d5d1 = $this->inquiry->{'field_no_facility_number'};
        $isVisibleCd2 = $this->isVisibleField($this->inquiry, 'field_formal_service_provider_offers');

        if (($condition && !$c1d5) || ($isVisibleCd2 && ($c1d5 && ($c1d5d1 < $ad5)))) {
            $c1d5 = $this->inquiry->{'field_formal_service_provider_offers'};
            if (empty($c1d5)) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
