<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @InquiryCheckAction(
 *     id = "FT006",
 *     active = false,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "'Number of service providers assisted in the previous 12 months' is required if is visible.",
 *     message = "Field '3.1.2.2 Number of service providers assisted in the previous 12 months' is required.",
 * )
 */
class InquiryCheckFT006 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $visibleResolver = new InquiryConditionalResolver($this->inquiry);
        $visible3d1d2d2 = $visibleResolver->isVisible('field_assistance_provided_supervision_assisted', '');
        if (!$visible3d1d2d2) {
            return true;
        }
        $fieldtype = $this->inquiry->getFieldDefinition('field_assistance_provided_supervision_assisted');
        if ($fieldtype->isEmpty()) {
            $this->logResult();

            return false;
        }

        return true;
    }
}
