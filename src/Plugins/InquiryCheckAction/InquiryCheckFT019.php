<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FT019",
 *     active = false,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "'Was this role performed within the previous 12 months?' is required if is visible.",
 *     message = "Field '3.2.5.1 Was this role performed within the previous 12 months?' is required.",
 * )
 */
class InquiryCheckFT019 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = ($this->inquiry->{'field_providing_ta_social'});
        if ($condition) {
            $currentField = $this->inquiry->{'field_providing_ta_social_performed'};
            if (empty($currentField)) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
