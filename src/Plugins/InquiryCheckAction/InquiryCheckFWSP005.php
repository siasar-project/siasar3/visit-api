<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSP005",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "'Type of tariff?' is required if is visible.",
 *     message = "Field '3.3 Type of tariff?' is required.",
 * )
 */
class InquiryCheckFWSP005 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = (
            '1' === $this->inquiry->{'field_water_tariff_inplace_inuse'} ||
            '2' === $this->inquiry->{'field_water_tariff_inplace_inuse'}
        );
        $providerType = $this->inquiry->{'field_provider_type'};
        if ('5' !== $providerType && $condition) {
            $value = $this->inquiry->{'field_tariff_type'};
            if (empty($value)) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
