<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSP007",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "'What type of environmental protection practices are being promoted?' is required if is visible.",
 *     message = "Field '6.6.2 What type of environmental protection practices are being promoted?' is required.",
 * )
 */
class InquiryCheckFWSP007 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = ($this->inquiry->{'field_promote_environmental_protection'});
        if ($condition) {
            $value = $this->inquiry->{'field_types_environmental_protection'};
            if (empty($value)) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
