<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSP008",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "'An unregularized real state can't be a regularized at the same time.",
 *     message = "Field '1.9 Land regulation'. An unregularized real state can't be a regularized at the same time.",
 * )
 */
class InquiryCheckFWSP008 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // La respuesta 3 no es compatible con la 1 y la 2; la 1 y 2 pueden estar marcadas a la vez
        $fieldValue = $this->inquiry->{'field_land_regulation'};

        if ($fieldValue) {
            if (in_array('3', $fieldValue) && (in_array('1', $fieldValue) || in_array('2', $fieldValue))) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
