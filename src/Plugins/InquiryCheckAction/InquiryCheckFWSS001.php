<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSS001",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "'If it’s a pumped supply system, where is the pump(s) located?' is required if is visible.",
 *     message = "Field '1.8 If it’s a pumped supply system, where is the pump(s) located?' is required.",
 * )
 */
class InquiryCheckFWSS001 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = ('2' === $this->inquiry->{'field_type_system'});
        if ($condition) {
            $pumpsLocation = $this->inquiry->{'field_pumps_location'};
            if (empty($pumpsLocation)) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
