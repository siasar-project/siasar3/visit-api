<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSS002",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "Remember that without a distribution network, you can only have one 'water source and/or water intake' (Field 2).",
 *     message = "Remember that without a distribution network, you can only have one 'water source and/or water intake' (Field 2).",
 * )
 */
class InquiryCheckFWSS002 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Field 2 'field_water_source_intake' of form.wssystem
        if (!$this->inquiry->{'field_have_distribution_network'}) {
            $subforms = $this->inquiry->{'field_water_source_intake'};
            if (count($subforms) > 1) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
