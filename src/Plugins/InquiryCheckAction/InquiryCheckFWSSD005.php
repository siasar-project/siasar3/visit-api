<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSSD005",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "'Accessibility of the public standpost' is required if is visible.",
 *     message = "Field '6.8 Accessibility of the public standpost' is required.",
 * )
 */
class InquiryCheckFWSSD005 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $subRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subRecords as $subRecord) {
            $condition = (!$this->inquiry->{'field_have_distribution_network'});
            $condition1 = ('2' === $subRecord->{'field_serve_household'} || '3' === $subRecord->{'field_serve_household'});
            $condition = $condition || ($this->inquiry->{'field_have_distribution_network'} && $condition1);
            if ($condition) {
                $currentField = $subRecord->{'field_accessibility_public_standpost'};
                if (empty($currentField)) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
