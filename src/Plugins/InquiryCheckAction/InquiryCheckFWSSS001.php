<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSSS001",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "'Latitude' is required if is visible.",
 *     message = "Field '5.1.1 Latitude' is required.",
 * )
 */
class InquiryCheckFWSSS001 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = ($this->inquiry->{'field_have_distribution_network'});
        $condition = $condition && (!$this->inquiry->{'field_system_derived_urban'});
        if ($condition) {
            $subRecords = $this->inquiry->{'field_storage_infrastructure'};
            foreach ($subRecords as $subRecord) {
                $currentField = $subRecord->{'field_storage_latitude'};
                if (empty($currentField)) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
