<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSSSI003",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "'Altitude' is required if is visible.",
 *     message = "Field '2.1.3 Altitude' is required.",
 * )
 */
class InquiryCheckFWSSSI003 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $condition = $this->inquiry->{'field_have_distribution_network'};
        if ($condition) {
            $subRecords = $this->inquiry->{'field_water_source_intake'};
            foreach ($subRecords as $subRecord) {
                $currentField = $subRecord->{'field_source_altitude'};
                if (empty($currentField)) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
