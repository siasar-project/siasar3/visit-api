<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "FWSSSI007",
 *     active = false,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "'Maintenance operations in the last year on electrically pumped intakes' is required if is visible.",
 *     message = "Field '2.15.2 Maintenance operations in the last year on electrically pumped intakes' is required.",
 * )
 */
class InquiryCheckFWSSSI007 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $withDistribution = $this->inquiry->{'field_have_distribution_network'};
        $systemType = $this->inquiry->{'field_type_system'};
        $pumpsLocation = $this->inquiry->{'field_pumps_location'};
        $subRecords = $this->inquiry->{'field_water_source_intake'};
        foreach ($subRecords as $subRecord) {
            $condition = $subRecord->{'field_pump_maintenance_conducted_last_year'};
            if ($condition) {
                if ($withDistribution) {
                    $condition = ('4' === $systemType) || ('6' === $systemType);
                } else {
                    $condition = ('1' === $pumpsLocation) || ('3' === $pumpsLocation);
                }
                if ($condition) {
                    $currentField = $subRecord->{'field_maintenance_electrically'};
                    if (empty($currentField)) {
                        $this->logResult();

                        return false;
                    }
                }
            }
        }

        return true;
    }
}
