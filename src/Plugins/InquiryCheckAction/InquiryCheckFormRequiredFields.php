<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Forms\FormRecord;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Tools\ContextLogNormalizer;
use App\Tools\Json;

/**
 * @InquiryCheckAction(
 *     id = "RequiredFields",
 *     active = true,
 *     level = "error",
 *     form = "*",
 *     observation = "Validate required fields",
 *     message = "Validate required fields.",
 *     multiMessage = true,
 * )
 */
class InquiryCheckFormRequiredFields extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $resp = true;
        $fields = $this->inquiry->getFields();

        foreach ($fields as $field) {
            switch ($field->getFieldType()) {
                case 'subform_reference':
                    /** @var FormRecord $subRecord */
                    $subRecord = $this->inquiry->{$field->getId()};
                    $nombre = $field->getFieldName();
                    if ($field->isMultivalued()) {
                        if ($field->isRequired(true) && 0 === count($subRecord)) {
                            $resp = false;
                            $this->logResult([
                                '#message' => $this->t(
                                    '[@type] Field "@id" is required.',
                                    [
                                        '@type' => $field->getFieldType(),
                                        '@id' => $field->getLabelExtended(),
                                    ]
                                ),
                            ]);
                        }
                        foreach ($subRecord as $record) {
                            $subform = $record->getForm();
                            $valid = $subform->validateRecord($record);

                            if (!$valid) {
                                $resp = false;

                                $this->logResult([
                                    '#message' => implode("\n", $subform->getLastValidationMessages()),
                                ]);
                            }
                        }
                    } else {
                        $subform = $subRecord->getForm();
                        $valid = $subform->validateRecord($subRecord);

                        if (!$valid) {
                            $resp = false;

                            $this->logResult([
                                '#message' => implode("\n", $subform->getLastValidationMessages()),
                            ]);
                        }
                    }
                    break;
                default:
                    if ($field->isRequired(true) && $field->isEmpty()) {
                        $resp = false;
                        $this->logResult([
                            '#message' => $this->t(
                                '[@type] Field "@id" is required.',
                                [
                                    '@type' => $field->getFieldType(),
                                    '@id' => $field->getLabelExtended(),
                                ]
                            ),
                        ]);
                    }
                    break;
            }
        }

        return $resp;
    }

    /**
     * Add action to activity log.
     *
     * @param array $context
     *
     * @throws \Exception
     */
    protected function logResult(array $context = []): void
    {
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        $context['form_id'] = $this->inquiry->getForm()->getId();
        $context['record_id'] = $this->inquiry->getId();

        $annotation = $this->annotationReader
            ->getClassAnnotation(new \ReflectionClass(static::class), 'App\Annotations\InquiryCheckAction');

        if (!isset($context['#message'])) {
            $message = $annotation->getMessage();
        } else {
            $message = $context['#message'];
            unset($context['#message']);
        }
        // todo Binary IDs are missed in this conversion. If we do not do this conversion the log fails by JSON encoding.
        $nContext = ContextLogNormalizer::normalize($context);
        $nContext = Json::decode(Json::encode($nContext));
        switch (strtolower($annotation->getLevel())) {
            case 'error':
                $this->inquiryFormLogger->error(sprintf('[%s] %s', $annotation->getId(), $message), $nContext);
                break;

            case 'warning':
                $this->inquiryFormLogger->warning(sprintf('[%s] %s', $annotation->getId(), $message), $nContext);
                break;

            default:
                $this->inquiryFormLogger->info(sprintf('[%s] %s', $annotation->getId(), $message), $nContext);
                break;
        }
    }
}
