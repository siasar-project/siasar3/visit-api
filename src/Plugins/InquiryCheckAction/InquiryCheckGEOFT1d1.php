<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Forms\FormRecord;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Service\CountryGeometryService;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;

/**
 * @InquiryCheckAction(
 *     id = "GEOFT1d1",
 *     active = true,
 *     level = "error",
 *     form = "form.tap",
 *     observation = "If have a geometry point, it must be inside the country.",
 *     message = "Field 1.1 - Location must be inside the country.",
 * )
 */
class InquiryCheckGEOFT1d1 extends AbstractInquiryCheckActionBase
{
    use GetContainerTrait;

    private CountryGeometryService $countryGeometry;

    /**
     * Base constructor.
     *
     * @param Reader          $annotationReader
     * @param SessionService  $sessionService
     * @param LoggerInterface $inquiryFormLogger
     * @param FormRecord      $inquiry
     */
    public function __construct(Reader $annotationReader, SessionService $sessionService, LoggerInterface $inquiryFormLogger, FormRecord $inquiry)
    {
        parent::__construct($annotationReader, $sessionService, $inquiryFormLogger, $inquiry);

        $this->countryGeometry = static::getContainerInstance()->get('country_geometry_service');
    }

    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $country = $this->inquiry->{'field_country'};
        if ($this->countryGeometry->exist($country->getId())) {
            $lat = $this->inquiry->{'field_location_lat'};
            $lon = $this->inquiry->{'field_location_lon'};
            if (0 !== $lat || 0 !== $lon) {
                if (!$this->countryGeometry->validatePoint($country, $lat, $lon)) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
