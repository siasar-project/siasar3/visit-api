<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Forms\FormRecord;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Service\CountryGeometryService;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;

/**
 * @InquiryCheckAction(
 *     id = "GEONoDef",
 *     active = true,
 *     level = "warning",
 *     form = "*",
 *     observation = "This country not define a geometry.",
 *     message = "This country can't validate geo-locations.",
 * )
 */
class InquiryCheckGEONoDef extends AbstractInquiryCheckActionBase
{
    use GetContainerTrait;

    private CountryGeometryService $countryGeometry;

    /**
     * Base constructor.
     *
     * @param Reader          $annotationReader
     * @param SessionService  $sessionService
     * @param LoggerInterface $inquiryFormLogger
     * @param FormRecord      $inquiry
     */
    public function __construct(Reader $annotationReader, SessionService $sessionService, LoggerInterface $inquiryFormLogger, FormRecord $inquiry)
    {
        parent::__construct($annotationReader, $sessionService, $inquiryFormLogger, $inquiry);

        $this->countryGeometry = static::getContainerInstance()->get('country_geometry_service');
    }

    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $country = $this->inquiry->{'field_country'};
        if (!$this->countryGeometry->exist($country->getId())) {
            $this->logResult();

            return true;
        }

        return true;
    }
}
