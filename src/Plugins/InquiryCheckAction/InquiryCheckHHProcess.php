<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Entity\HouseholdProcess;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "HHProcess",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "If this inquiry has a household process, it must be closed before finalizing.",
 *     message = "If this inquiry has a household process, it must be closed before finalizing.",
 * )
 */
class InquiryCheckHHProcess extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        /** @var bool $withHouseholds */
        $withHouseholds = $this->inquiry->{'field_have_households'};
        if ($withHouseholds) {
            /** @var HouseholdProcess|null $process */
            $process = $this->inquiry->{'field_household_process'};
            if (!$process || $process->isOpen()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
