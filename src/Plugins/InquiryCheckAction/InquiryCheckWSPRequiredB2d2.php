<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "WSPRequiredB2d2",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "Field 2.1.2 is required, 'Provider's legal status'.",
 *     message = "Field 2.1.2 is required, 'Provider's legal status'.",
 * )
 */
class InquiryCheckWSPRequiredB2d2 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ('3' !== $wsp1d4 && '5' !== $wsp1d4) {
            $wsp2d1d2 = $this->inquiry->getFieldDefinition('field_provider_legal_status');
            if ($wsp2d1d2->isEmpty()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
