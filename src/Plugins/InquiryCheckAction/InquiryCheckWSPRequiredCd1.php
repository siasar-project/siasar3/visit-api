<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "WSPRequiredCd1",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "[field_water_tariff_inplace_inuse] Field '3.1 - Is there a water tariff in place and in use?' is required.",
 *     message = "[field_water_tariff_inplace_inuse] Field '3.1 - Is there a water tariff in place and in use?' is required.",
 * )
 */
class InquiryCheckWSPRequiredCd1 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // field_water_tariff_inplace_inuse Cd1
        $providerType = $this->inquiry->{'field_provider_type'};
        if ('5' !== $providerType) {
            $waterTariffInplace = $this->inquiry->getFieldDefinition('field_water_tariff_inplace_inuse');
            if ($waterTariffInplace->isEmpty()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
