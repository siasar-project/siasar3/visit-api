<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "WSPRequiredEd1",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "[field_uptodate_records_revenues_expenses] Field '5.1 - Does the provider have up-to-date records of revenues and expenses?' is required.",
 *     message = "[field_uptodate_records_revenues_expenses] Field '5.1 - Does the provider have up-to-date records of revenues and expenses?' is required.",
 * )
 */
class InquiryCheckWSPRequiredEd1 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // field_uptodate_records_revenues_expenses Ed1
        $providerType = $this->inquiry->{'field_provider_type'};
        if ('5' !== $providerType) {
            $uptodateRecords = $this->inquiry->getFieldDefinition('field_uptodate_records_revenues_expenses');
            if ($uptodateRecords->isEmpty()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
