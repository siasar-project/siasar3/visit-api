<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\InquiryCheckAction;

use App\Annotations\InquiryCheckAction;
use App\Plugins\AbstractInquiryCheckActionBase;

/**
 * @InquiryCheckAction(
 *     id = "WSPRequiredFd6d1",
 *     active = true,
 *     level = "error",
 *     form = "form.wsprovider",
 *     observation = "[field_promote_environmental_protection] Field '6.6.1 - Does the service provider promote environmental protection near the system?' is required.",
 *     message = "[field_promote_environmental_protection] Field '6.6.1 - Does the service provider promote environmental protection near the system?' is required.",
 * )
 */
class InquiryCheckWSPRequiredFd6d1 extends AbstractInquiryCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $providerType = $this->inquiry->{'field_provider_type'};
        if ('5' !== $providerType) {
            $promoteEnviromentalProtection = $this->inquiry->getFieldDefinition('field_promote_environmental_protection');
            if ($promoteEnviromentalProtection->isEmpty()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
