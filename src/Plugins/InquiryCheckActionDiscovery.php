<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins;

use App\Annotations\InquiryCheckAction;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Inquiry checking operations discovery service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class InquiryCheckActionDiscovery
{
    protected string $namespace;
    protected string $directory;
    protected Reader $annotationReader;
    /**
     * The Kernel root directory.
     */
    protected string $rootDir;
    protected array $inquiryCheckActions = [];

    /**
     * Inquiry checking operations discovery constructor.
     *
     * @param string $namespace        Annotation name space.
     * @param string $directory        Inquiry checking operation definitions folder.
     * @param string $rootDir          Application root folder.
     * @param Reader $annotationReader Annotation reader.
     */
    public function __construct(string $namespace, string $directory, string $rootDir, Reader $annotationReader)
    {
        $this->namespace = $namespace;
        $this->annotationReader = $annotationReader;
        $this->directory = $directory;
        $this->rootDir = $rootDir;
    }

    /**
     * Returns all the workers
     *
     * @param string|null $formId Filter by form ID. Use '*' to get all.
     *
     * @return array [['class' => string, 'annotation' => InquiryCheckAction]]
     *
     * @throws \ReflectionException
     */
    public function getInquiryCheckActions(?string $formId = '*'): array
    {
        if (!$this->inquiryCheckActions) {
            $this->discoverInquiryCheckActions();
        }

        if ('*' === $formId) {
            return $this->inquiryCheckActions;
        }

        $resp = [];
        foreach ($this->inquiryCheckActions as $key => $action) {
            if ('*' === $action['annotation']->getForm() || $formId === $action['annotation']->getForm()) {
                $resp[$key] = $action;
            }
        }

        return $resp;
    }

    /**
     * Discovers Inquiry checking operations.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    protected function discoverInquiryCheckActions(): void
    {
        // Inquiry checking operations are in the types folder in this discoverer.
        $path = __DIR__.'/InquiryCheckAction/';
        $finder = new Finder();
        $finder->files()->in($path);

        /**
         * InquiryCheckAction definition file.
         *
         * @var SplFileInfo $file
         */
        foreach ($finder as $file) {
            $class = $this->namespace.'\\'.$file->getBasename('.php');
            $annotation = $this->annotationReader
                ->getClassAnnotation(new \ReflectionClass($class), 'App\Annotations\InquiryCheckAction');
            if (!$annotation) {
                continue;
            }

            if ($annotation->isActive()) {
                /**
                 * Annotation setting.
                 *
                 * @var InquiryCheckAction $annotation
                 */
                $this->inquiryCheckActions[$annotation->getId()] = [
                    'class' => $class,
                    'annotation' => $annotation,
                ];
            }
        }
    }
}
