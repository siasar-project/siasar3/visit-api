<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ACOMZ01",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "If the number of households served by the systems exceeds the community total, then there are households with two systems.",
 *     message = "The number of households served by the systems associated with this community exceeds the total number of households in the community. This implies that some households have two systems, which may be valid, but it is still recommended to review.",
 * )
 */
class PointCheckACOMZ01 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota 1: esta alerta solo aplica para comunidades con 2 o más sistemas
        // Nota 2: se aplica la suma de los i sistemas unidos a esa comunidad
        //  SUMA(SIS_A12.3i) > COM_A5
        $com1d5 = $this->inquiry->{'field_total_households'};

        // Get the systems
        $pointSystems = $this->point->{'field_wsystems'};
        $servedCommunities = [];
        $relatedSystems = [];
        $sumSys1d12d3 = 0;
        foreach ($pointSystems as $pointSystem) {
            $servedCommunities = $pointSystem->{'field_served_communities'};
            foreach ($servedCommunities as $subRecord) {
                if ($this->inquiry->{'field_region'}->getId() === $subRecord->{'field_community'}->getId()) {
                    $relatedSystems[$pointSystem->getId()] = $pointSystem;
                    $sumSys1d12d3 += $subRecord->{'field_households'};
                }
            }
        }

        if (count($relatedSystems) > 1) {
            if ($sumSys1d12d3 > $com1d5) {
                $this->logResult();
            }
        }

        return true;
    }
}
