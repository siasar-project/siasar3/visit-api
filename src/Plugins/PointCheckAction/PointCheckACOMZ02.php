<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ACOMZ02",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "It is unusual for a community without a gravity or pumped system to have hydraulically flushed sanitation.",
 *     message = "It has been indicated that the community has households with sanitation facilities with hydraulic discharge. However, the community does not have a water system with a distribution network. Check if this is correct.",
 * )
 */
class PointCheckACOMZ02 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esta alerta de aplica si todos los sistemas asociados a la comunidad cumplen la condición
        //  (COM_C1.1.0 = 1 o COM_C1.2.0 = 1 o COM_C1.3.0 = 1 o COM_C1.4.0 = 1 o COM_C1.5.0 = 1) y
        //  (SIS_A7 = 3 o SIS_A7 = 4 o SIS_A7 = 5 o SIS_A7 = 6)
        $throwWarning = false;

        $com3d1d1d1 = $this->inquiry->{'field_sewer_connection_number'};
        $com3d1d1d2 = $this->inquiry->{'field_septic_tank_number'};
        $com3d1d1d3 = $this->inquiry->{'field_pit_latrine_number'};
        $com3d1d1d4 = $this->inquiry->{'field_without_containment_number'};
        $com3d1d1d5 = $this->inquiry->{'field_unknown_number'};

        if ($com3d1d1d1 > 0 ||
            $com3d1d1d2 > 0 ||
            $com3d1d1d3 > 0 ||
            $com3d1d1d4 > 0 ||
            $com3d1d1d5 > 0
        ) {
            // Get the systems
            $pointSystems = $this->point->{'field_wsystems'};
            $servedCommunities = [];
            foreach ($pointSystems as $pointSystem) {
                $servedCommunities = $pointSystem->{'field_served_communities'};
                foreach ($servedCommunities as $subRecord) {
                    if ($this->inquiry->{'field_region'}->getId() === $subRecord->{'field_community'}->getId()) {
                        $sys1d7 = $pointSystem->{'field_type_system'};
                        if ('3' !== $sys1d7 ||
                            '4' !== $sys1d7 ||
                            '5' !== $sys1d7 ||
                            '6' !== $sys1d7
                        ) {
                            return true;
                        }

                        $throwWarning = true;
                    }
                }
            }
        }

        if ($throwWarning) {
            $this->logResult();
        }

        return true;
    }
}
