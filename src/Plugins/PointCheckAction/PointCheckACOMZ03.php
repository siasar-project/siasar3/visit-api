<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ACOMZ03",
 *     active = true,
 *     level = "warning",
 *     form = "form.community",
 *     observation = "The community has sanitation with hydraulic discharge, but there are households that do not have a house connection.",
 *     message = "It has been indicated that the community has households with sanitation facilities with hydraulic discharge. However, part or all of the community lacks home distribution. Check if this is correct.",
 * )
 */
class PointCheckACOMZ03 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: esta alerta de aplica si todos los sistemas asociados a la comunidad cumplen la condición
        // Nota 2: el campo F5 debe ser el de las distribuciones asociadas a esa comunidad
        // (COM_C1.1.0 = 1 o COM_C1.2.0 = 1 o COM_C1.3.0 = 1 o COM_C1.4.0 = 1 o COM_C1.5.0 = 1) y
        // (SIS_A7 = 1 o SIS_A7 = 2) y
        // SIS_F5 ≠ 1
        $throwWarning = false;

        $com3d1d1d1 = $this->inquiry->{'field_sewer_connection_number'};
        $com3d1d1d2 = $this->inquiry->{'field_septic_tank_number'};
        $com3d1d1d3 = $this->inquiry->{'field_pit_latrine_number'};
        $com3d1d1d4 = $this->inquiry->{'field_without_containment_number'};
        $com3d1d1d5 = $this->inquiry->{'field_unknown_number'};

        if ($com3d1d1d1 > 0 ||
            $com3d1d1d2 > 0 ||
            $com3d1d1d3 > 0 ||
            $com3d1d1d4 > 0 ||
            $com3d1d1d5 > 0
        ) {
            // Get the systems
            $pointSystems = $this->point->{'field_wsystems'};
            $servedCommunities = [];
            foreach ($pointSystems as $pointSystem) {
                $servedCommunities = $pointSystem->{'field_served_communities'};
                foreach ($servedCommunities as $sys1d12) {
                    if ($this->inquiry->{'field_region'}->getId() === $sys1d12->{'field_community'}->getId()) {
                        $sys1d7 = $pointSystem->{'field_type_system'};
                        if ('1' !== $sys1d7 ||
                            '2' !== $sys1d7
                        ) {
                            return true;
                        }
                        $subrecords6 = $pointSystem->{'field_distribution_infrastructure'};
                        foreach ($subrecords6 as $sys6) {
                            $sys6d2 = $sys6->{'field_communities_serviced_network'}; // administrative_division_reference
                            $sys6d5 = $sys6->{'field_serve_household'};
                            if ('1' !== $sys6d5) {
                                if (!$sys6d2) {
                                    break;
                                }
                                if ($sys6d2->getId() === $sys1d12->{'field_community'}->getId()) {
                                    $throwWarning = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($throwWarning) {
            $this->logResult();
        }

        return true;
    }
}
