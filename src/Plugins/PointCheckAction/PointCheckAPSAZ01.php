<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "APSAZ01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "If this WSP is staffed, review why its linked systems are not being maintained.",
 *     message = "If the service provider is staffed (WSP 2.4) then review whether maintenance is actually being performed on the associated systems.",
 * )
 */
class PointCheckAPSAZ01 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // Nota: se muestra si y solo si la alerta se da en todos los sistemas vinculados
        //  (SIS_B14 = 2 y SIS_C7 = 2 y SIS_D6 = 2 y SIS_E5 = 2 y SIS_F14 = 2) y
        //  PSA_B4 = 1
        $wsp2d4 = $this->inquiry->{'field_have_personnel_to_rural_service'};
        $relatedSystems = [];

        if (!$wsp2d4) {
            return true;
        }

        $pointSystems = $this->point->{'field_wsystems'};
        $servedCommunities = [];
        foreach ($pointSystems as $pointSystem) {
            $servedCommunities = $pointSystem->{'field_served_communities'};
            foreach ($servedCommunities as $subRecord) {
                if ($this->inquiry->getId() === $subRecord->{'field_provider'}->getId()) {
                    $relatedSystems[$pointSystem->getId()] = $pointSystem;
                }
            }
        }

        foreach ($relatedSystems as $record) {
            $sysB = $record->{'field_water_source_intake'};
            if (is_array($sysB)) {
                foreach ($sysB as $subrecord) {
                    $sysB14 = $subrecord->{'field_pump_maintenance_conducted_last_year'};
                    if ($sysB14) {
                        return true;
                    }
                }
            }
            $sysC = $record->{'field_water_transmision_line'};
            if (is_array($sysC)) {
                foreach ($sysC as $subrecord) {
                    $sysC7 = $subrecord->{'field_have_maintenance_conducted_last_year'};
                    if ($sysC7) {
                        return true;
                    }
                }
            }
            $sysD = $record->{'field_treatment_points'};
            if (is_array($sysD)) {
                foreach ($sysD as $subrecord) {
                    $sysD6 = $subrecord->{'field_treatment_maintenance_conducted_last_year'};
                    if ($sysD6) {
                        return true;
                    }
                }
            }
            $sysE = $record->{'field_storage_infrastructure'};
            if (is_array($sysE)) {
                foreach ($sysE as $subrecord) {
                    $sysE5 = $subrecord->{'field_storage_maintenance_conducted_last_year'};
                    if ($sysE5) {
                        return true;
                    }
                }
            }
            $sysF = $record->{'field_distribution_infrastructure'};
            if (is_array($sysF)) {
                foreach ($sysF as $subrecord) {
                    $sysF14 = $subrecord->{'field_service_maintenance_conducted_last_year'};
                    if ($sysF14) {
                        return true;
                    }
                }
            }
        }

        $this->logResult();

        return true;
    }
}
