<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @PointCheckAction(
 *     id = "APSAZ04",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "If there is no flow measurement at the catchment, it seems difficult to have measurement of the water billed.",
 *     message = "The systems associated with the provider do not have flow measurement at the catchment (SYS 2.7) but, nevertheless, the provider reports having monthly billed water measurement for its systems. Check if this is correct.",
 * )
 */
class PointCheckAPSAZ04 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $wsp1d4 = $this->inquiry->{'field_provider_type'};
        if ("5" === $wsp1d4) {
            return true;
        }
        // Nota: se muestra si y solo si la alerta se da en todos los sistemas vinculados
        //  SIS_B7 = 3 y PSA_C7 ≠ 98
        $throwWarning = false;
        $wsp3d7d1 = $this->inquiry->{'field_m3_monthly_billed_water_exists'};
        if (!$wsp3d7d1) {
            return true;
        }

        $pointSystems = $this->point->{'field_wsystems'};
        $servedCommunities = [];
        $relatedSystems = [];
        foreach ($pointSystems as $pointSystem) {
            $servedCommunities = $pointSystem->{'field_served_communities'};
            foreach ($servedCommunities as $subRecord) {
                if ($this->inquiry->getId() === $subRecord->{'field_provider'}->getId()) {
                    $relatedSystems[$pointSystem->getId()] = $pointSystem;
                }
            }
        }

        foreach ($relatedSystems as $record) {
            $sysB = $record->{'field_water_source_intake'};
            if (is_array($sysB)) {
                foreach ($sysB as $subrecord) {
                    $visibleResolver = new InquiryConditionalResolver($subrecord);
                    $visibleB7 = $visibleResolver->isVisible('field_systematically_measure_flow', 'field_water_source_intake');
                    if (!$visibleB7) {
                        return true;
                    }
                    $sysB7 = $subrecord->{'field_systematically_measure_flow'};
                    if ('3' !== $sysB7) {
                        return true;
                    }
                    $throwWarning = true;
                }
            }
        }

        if ($throwWarning) {
            $this->logResult();
        }

        return true;
    }
}
