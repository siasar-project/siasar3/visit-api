<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;
use App\Tools\Measurements\VolumeMeasurement;

/**
 * @PointCheckAction(
 *     id = "APSAZ05",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The sum of the flow rates of the systems is 10% greater than the monthly billed water figure of the provider that is operating them (and the service provider only serves those systems).",
 *     message = "The sum of the flows distributed by the systems associated with the service provider is 10% higher than the monthly volume produced declared by the service provider. Check if this is correct.",
 * )
 */
class PointCheckAPSAZ05 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota 1: se suman los datos de los i sistemas servidos por el prestador
        // Nota 2: la unidad de trabajo será l/s en SIS_F11.2
        // Nota 3: esta validación solo se aplica para sistemas servidos por un único prestador
        // Nota 4: el PSA no da servicio a ningún sistema con más de un PSA asociado

        // (30 x SUMA (SIS_F11.2i) >
        // 1,1 x 1000 x PSA_C7) y
        // PSA_C7 ≠ 98

        $wsp3d7d1 = $this->inquiry->{'field_m3_monthly_billed_water_exists'};
        if (!$wsp3d7d1) {
            return true;
        }
        /** @var VolumeMeasurement $wsp3d7d2 */
        $wsp3d7d2 = $this->inquiry->{'field_m3_monthly_billed_water'}; // volume
        $wsp3d7d2->setUnit('cubic metre');

        // Get the systems served by the service provider
        $pointSystems = $this->point->{'field_wsystems'};
        $servedCommunities = [];
        $relatedSystems = [];
        foreach ($pointSystems as $pointSystem) {
            $servedCommunities = $pointSystem->{'field_served_communities'};
            foreach ($servedCommunities as $sys1d12) {
                if (in_array($pointSystem->getId(), $relatedSystems) && $this->inquiry->getId() !== $sys1d12->{'field_provider'}->getId()) {
                    unset($relatedSystems[$pointSystem->getId()]);
                }
                if ($this->inquiry->getId() === $sys1d12->{'field_provider'}->getId()) {
                    $relatedSystems[$pointSystem->getId()] = $pointSystem;
                }
            }
        }

        $sumSysF11 = 0;
        foreach ($relatedSystems as $system) {
            $sysF = $system->{'field_distribution_infrastructure'};
            if (is_array($sysF)) {
                foreach ($sysF as $subrecord) {
                    /** @var FlowMeasurement $sysF11d2 */
                    $sysF11d2 = $subrecord->{'field_service_flow'};
                    $sysF11d2->setUnit('liter/second');
                    $sumSysF11 += $sysF11d2->getValue();
                }
            }
        }

        if ((30 * $sumSysF11) > (1.1 * (1000 * $wsp3d7d2->getValue()))) {
            $this->logResult();
        }

        return true;
    }
}
