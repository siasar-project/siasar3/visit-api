<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "APSAZ07",
 *     active = true,
 *     level = "warning",
 *     form = "form.wsprovider",
 *     observation = "The number of homes served by all systems served by the service provider is less than the number of registered users.",
 *     message = "The number of households served by all the systems served by the service provider is less than the number of users registered with the provider, which may be valid, but should be reviewed.",
 * )
 */
class PointCheckAPSAZ07 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: se aplica la suma de los i sistemas unidos a ese prestador
        //  SUMA(SIS_A12.3i) < PSA_C8.1

        $wsp3d8d1d1 = $this->inquiry->{'field_users_required_pay_exists'};
        if (!$wsp3d8d1d1) {
            return true;
        }
        $wsp3d8d1d2 = $this->inquiry->{'field_users_required_pay'};

        // Get the systems
        $pointSystems = $this->point->{'field_wsystems'};
        $servedCommunities = [];
        $relatedSystems = [];
        $sumSys1d12d3 = 0;
        foreach ($pointSystems as $pointSystem) {
            $servedCommunities = $pointSystem->{'field_served_communities'};
            foreach ($servedCommunities as $subRecord) {
                if ($this->inquiry->getId() === $subRecord->{'field_provider'}->getId()) {
                    $relatedSystems[$pointSystem->getId()] = $pointSystem;
                    $sumSys1d12d3 += $subRecord->{'field_households'};
                }
            }
        }

        if ($sumSys1d12d3 < $wsp3d8d1d2) {
            $this->logResult();
        }

        return true;
    }
}
