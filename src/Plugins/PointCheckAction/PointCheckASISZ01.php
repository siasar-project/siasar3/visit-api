<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;
use App\Tools\InquiryConditionalResolver;

/**
 * @PointCheckAction(
 *     id = "ASISZ01",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "It is unusual for the service provider to be incorporated when the system has been in operation for more than 5 years.",
 *     message = "It has been indicated that the service provider was created more than 5 years (WSP 2.1.1) after the system was built (SYS 1.4). Check if this is correct.",
 * )
 */
class PointCheckASISZ01 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: se muestra si y solo si la alerta se da en todos los PSA vinculados
        //  PSA_B1.1 - SIS_A4 > 5 años
        $sys1d4 = $this->inquiry->{'field_year_build'};
        $throwWarning = false;

        // Search related providers (Field 1.12.2)
        $servedCommunities = $this->inquiry->{'field_served_communities'};
        foreach ($servedCommunities as $sys1d12) {
            $sys1d12d2 = $sys1d12->{'field_provider'};
            $wsp2d1d1 = $sys1d12d2->{'field_constitution_date'};
            $visibleResolver = new InquiryConditionalResolver($sys1d12d2);
            $visible2d1d1 = $visibleResolver->isVisible('field_constitution_date', '');
            if ($visible2d1d1) {
                if (($wsp2d1d1['year'] - $sys1d4) > 5) {
                    $throwWarning = true;
                }
            }
        }

        if ($throwWarning) {
            $this->logResult();
        }

        return true;
    }
}
