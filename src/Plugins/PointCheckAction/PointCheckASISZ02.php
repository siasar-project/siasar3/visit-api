<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ASISZ02",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The system is electrically pumped but the lender has no recorded expenses.",
 *     message = "Based on the type of system, it appears to have an electrical pumping system that could involve some type of expense. The lender's questionnaire has not indicated any expense in WSP_4.1. Check if this is correct.",
 * )
 */
class PointCheckASISZ02 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: se muestra si y solo si la alerta se da en todos los PSA vinculados
        // (SIS_A7 = 2 o SIS_A7 = 4 o SIS_A7 = 6)
        // y PSA_D1.12.1 = 0
        $sys1d7 = $this->inquiry->{'field_type_system'};
        $throwWarning = false;

        if ('2' === $sys1d7 || '4' === $sys1d7 || '6' === $sys1d7) {
            // Search related providers (Field 1.12.2)
            $servedCommunities = $this->inquiry->{'field_served_communities'};
            foreach ($servedCommunities as $sys1d12) {
                $provider = $sys1d12->{'field_provider'};
                $wsp1d4 = $provider->{'field_provider_type'};
                if ("5" === $wsp1d4) {
                    return true;
                }
                $wsp4d1d12d2 = $this->getCurrencyAmount($provider->{'field_total_expenses'});

                if (0 < $wsp4d1d12d2) {
                    return true;
                }

                if (0.0 === $wsp4d1d12d2) {
                    $throwWarning = true;
                }
            }
        }

        if ($throwWarning) {
            $this->logResult();
        }

        return true;
    }
}
