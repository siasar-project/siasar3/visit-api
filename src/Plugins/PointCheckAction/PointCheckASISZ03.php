<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ASISZ03",
 *     active = true,
 *     level = "warning",
 *     form = "form.wssystem",
 *     observation = "The system has disinfection and is in operation, but there are no recorded expenditures for disinfectant product.",
 *     message = "The system has a disinfection facility in use, but the provider has no disinfectant expenditure recorded in WSP_4.1. Check if this is correct.",
 * )
 */
class PointCheckASISZ03 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: se muestra si y solo si la alerta se da en todos los PSA vinculados
        //  SIS_D3.6.2 = 1 y
        //  PSA_D1.12.1 = 0
        $throwWarning = false;
        $sys4 = $this->inquiry->{'field_treatment_points'};
        if (is_array($sys4)) {
            foreach ($sys4 as $subrecord) {
                $sys4d3d6d2 = $subrecord->{'field_disinfection_functioning'};
                if (!$sys4d3d6d2) {
                    return true;
                }
            }
        }

        // Search related providers (Field 1.12.2)
        $servedCommunities = $this->inquiry->{'field_served_communities'};
        foreach ($servedCommunities as $subRecord) {
            $provider = $subRecord->{'field_provider'};
            $wsp1d4 = $provider->{'field_provider_type'};
            if ("5" === $wsp1d4) {
                return true;
            }
            $wsp4d1d12d2 = $this->getCurrencyAmount($provider->{'field_total_expenses'});

            if (0 < $wsp4d1d12d2) {
                return true;
            }

            if (0.0 === $wsp4d1d12d2) {
                $throwWarning = true;
            }
        }

        if ($throwWarning) {
            $this->logResult();
        }

        return true;
    }
}
