<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ECOMB05",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "If the community does not have a system registered in SIASAR, then all the households in the community should be considered as 'without a system'.",
 *     message = "If the community does not have any water system, then the number of households without a community water system should equal the number of households in the community.",
 * )
 */
class PointCheckECOMB05 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        $pointSystems = $this->point->{'field_wsystems'};
        $pointCommunities = $this->point->{'field_communities'};
        $pointProviders = $this->point->{'field_wsps'};

        if (0 === count($pointSystems) && 0 === count($pointProviders) && 1 === count($pointCommunities)) {
            $bd1 = $this->inquiry->{'field_households_without_water_supply_system'};
            $ad5 = $this->inquiry->{'field_total_households'};

            if ($bd1 < $ad5) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
