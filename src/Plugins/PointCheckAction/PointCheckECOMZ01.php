<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ECOMZ01",
 *     active = true,
 *     level = "error",
 *     form = "form.community",
 *     observation = "If the community only has one associated system, the sum of households with and without a system must be the total number of households",
 *     message = "If the community has only one associated system, the number of households served by the system plus the number of households without water service must equal the total number of households in the community.",
 * )
 */
class PointCheckECOMZ01 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota 2: el error solo aplica a comunidad con un único i sistema asociado
        //  SIS_A12.3i +COM_B1 ≠ COM_A5 y COMi solo tiene un único sistema
        $com1d5 = $this->inquiry->{'field_total_households'};
        $com2d1 = $this->inquiry->{'field_households_without_water_supply_system'};

        // Get the systems
        $pointSystems = $this->point->{'field_wsystems'};
        $servedCommunities = [];
        $relatedSystems = [];
        $sumSys1d12d3 = 0;

        foreach ($pointSystems as $pointSystem) {
            $servedCommunities = $pointSystem->{'field_served_communities'};
            foreach ($servedCommunities as $subRecord) {
                if ($this->inquiry->{'field_region'}->getId() === $subRecord->{'field_community'}->getId()) {
                    $relatedSystems[$pointSystem->getId()] = $pointSystem;
                    $sumSys1d12d3 += $subRecord->{'field_households'};
                }
            }
        }

        if (1 === count($relatedSystems)) {
            if (($sumSys1d12d3 + $com2d1) !== $com1d5) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
