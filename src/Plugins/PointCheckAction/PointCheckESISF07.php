<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;
use App\Tools\Measurements\FlowMeasurement;

/**
 * Point Check ESISF07
 *
 * @category SIASAR_3
 *
 * @author   Carmen Gordo <carmen.gordo@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @PointCheckAction(
 *     id = "ESISF07",
 *     active = false,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "The flow rate served (6.11) should not be greater than the sum of all the collected flow rate (2.6).",
 *     message = "The flow served cannot be greater than the sum of all catchment flows.",
 * )
 */
class PointCheckESISF07 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota 1: esta comprobación debe hacerse en las mismas unidades
        // Nota 2: esta comprobación debe sumar todas las captaciones del sistema (i) y todas las distribuciones (j)
        // Nota 3: la comprobación se hará siempre con las últimas mediciones de caudal en B6
        // SUMA(SIS_F11i) l/s > SUMA(B6.2j) l/s

        // SIS_F => 'field_distribution_infrastructure'
        $subformRecords = $this->inquiry->{'field_distribution_infrastructure'};
        foreach ($subformRecords as $record) {
            $amountInfrastructure = $record->{'field_source_flow'} ? all($record->{'field_source_flow'}) : null;

            $intakeFlow = $record->{'field_intake_flow'} ? end($record->{'field_intake_flow'}) : null;

            /** @var FlowMeasurement $fieldSource */
            $fieldInfrastructure = $amountInfrastructure->{'field_flow'};
            $fieldSource->setUnit('liter/second');
            /** @var FlowMeasurement $fieldIntake */
            $fieldIntake = $intakeFlow->{'field_flow'};
            $fieldIntake->setUnit('liter/second');

            if ($fieldInfrastructure->getValue() > $fieldSource->getValue()) {
                $this->logResult();

                return false;
            }
        }

        return true;
    }
}
