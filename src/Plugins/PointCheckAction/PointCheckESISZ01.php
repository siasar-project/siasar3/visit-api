<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Annotations\PointCheckAction;
use App\Plugins\AbstractPointCheckActionBase;

/**
 * @PointCheckAction(
 *     id = "ESISZ01",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "If WSPs do not perform maintenance, then no maintenance action should be flagged in the system.",
 *     message = "If the service providers do not perform maintenance (WSP 6.1) then the associated system cannot indicate that maintenance is being performed on any of their infrastructures.",
 * )
 */
class PointCheckESISZ01 extends AbstractPointCheckActionBase
{
    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: se muestra si y solo si la alerta se da en todos los PSA vinculados
        // (SIS_B14 = 1 o SIS_C7 = 1 o SIS_D6 = 1 o SIS_E5 = 1 o SIS_F14 = 1) y
        // PSA_F1 = 4
        $sysB14Flag = false;
        $sysC7Flag = false;
        $sysD6Flag = false;
        $sysE5Flag = false;
        $sysF14Flag = false;

        $sysB = $this->inquiry->{'field_water_source_intake'};
        if (is_array($sysB)) {
            foreach ($sysB as $subrecord) {
                $sysB14 = $subrecord->{'field_pump_maintenance_conducted_last_year'};
                if ($sysB14) {
                    $sysB14Flag = true;
                    break;
                }
            }
        }
        $sysC = $this->inquiry->{'field_water_transmision_line'};
        if (is_array($sysC)) {
            foreach ($sysC as $subrecord) {
                $sysC7 = $subrecord->{'field_have_maintenance_conducted_last_year'};
                if ($sysC7) {
                    $sysC7Flag = true;
                    break;
                }
            }
        }
        $sysD = $this->inquiry->{'field_treatment_points'};
        if (is_array($sysD)) {
            foreach ($sysD as $subrecord) {
                $sysD6 = $subrecord->{'field_treatment_maintenance_conducted_last_year'};
                if ($sysD6) {
                    $sysD6Flag = true;
                    break;
                }
            }
        }
        $sysE = $this->inquiry->{'field_storage_infrastructure'};
        if (is_array($sysE)) {
            foreach ($sysE as $subrecord) {
                $sysE5 = $subrecord->{'field_storage_maintenance_conducted_last_year'};
                if ($sysE5) {
                    $sysE5Flag = true;
                    break;
                }
            }
        }
        $sysF = $this->inquiry->{'field_distribution_infrastructure'};
        if (is_array($sysF)) {
            foreach ($sysF as $subrecord) {
                $sysF14 = $subrecord->{'field_service_maintenance_conducted_last_year'};
                if ($sysF14) {
                    $sysF14Flag = true;
                    break;
                }
            }
        }

        // Search related providers (Field 1.12.2)
        $servedCommunities = $this->inquiry->{'field_served_communities'};
        foreach ($servedCommunities as $subRecord) {
            $provider = $subRecord->{'field_provider'};
            if ('4' === $provider->{'field_carryout_om_last_year'}) {
                if ($sysB14Flag || $sysC7Flag || $sysD6Flag || $sysE5Flag || $sysF14Flag) {
                    $this->logResult();

                    return false;
                }
            }
        }

        return true;
    }
}
