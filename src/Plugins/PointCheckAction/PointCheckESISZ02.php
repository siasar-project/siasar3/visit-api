<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\PointCheckAction;

use App\Forms\FormRecord;
use Psr\Log\LoggerInterface;
use App\Service\SessionService;
use App\Annotations\PointCheckAction;
use App\Forms\FormFactory;
use Doctrine\Common\Annotations\Reader;
use App\Plugins\AbstractPointCheckActionBase;
use App\Traits\GetContainerTrait;

/**
 * @PointCheckAction(
 *     id = "ESISZ02",
 *     active = true,
 *     level = "error",
 *     form = "form.wssystem",
 *     observation = "The number of households served cannot be greater than the total number of households in the community.",
 *     message = "The number of households served in a community cannot be greater than the total number of households in that community.",
 * )
 */
class PointCheckESISZ02 extends AbstractPointCheckActionBase
{
    use GetContainerTrait;

    protected FormFactory $formFactory;

    /**
     * Base constructor.
     *
     * @param Reader          $annotationReader
     * @param SessionService  $sessionService
     * @param LoggerInterface $inquiryFormLogger
     * @param FormRecord      $inquiry
     * @param FormRecord|null $point
     */
    public function __construct(Reader $annotationReader, SessionService $sessionService, LoggerInterface $inquiryFormLogger, FormRecord $inquiry, ?FormRecord $point)
    {
        parent::__construct($annotationReader, $sessionService, $inquiryFormLogger, $inquiry, $point);
        $this->formFactory = static::getContainerInstance()->get('form_factory');
    }

    /**
     * @inheritDoc
     */
    public function check(): bool
    {
        // Nota: este error se comprueba para cada comunidad i asociada al sistema en A12
        //  SIS_A12.3i > COM_A5i
        $servedCommunities = $this->inquiry->{'field_served_communities'};
        $communityManager = $this->formFactory->find('form.community');

        foreach ($servedCommunities as $sys1d12) {
            $fieldCommunity = $sys1d12->{'field_community'};
            $households = $sys1d12->{'field_households'};
            $community = current($communityManager->findBy(
                [
                    'field_region' => $fieldCommunity->getId(),
                    'field_deleted' => '0',
                ],
                ['field_changed' => 'DESC'],
            ));
            if (!$community) {
                continue;
            }
            $commHouseholds = $community->{'field_total_households'};
            if ($households > $commHouseholds) {
                $this->logResult();

                return false;
            }

            return true;
        }
    }
}
