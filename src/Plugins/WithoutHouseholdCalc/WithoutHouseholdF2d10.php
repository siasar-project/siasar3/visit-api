<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\WithoutHouseholdCalc;

use App\Annotations\WithoutHouseholdCalc;
use App\Plugins\AbstractWithoutHouseholdCalcBase;

/**
 * 2.10 = A5 - B1
 *
 * @WithoutHouseholdCalc(
 *     id = "2d10",
 *     active = true,
 *     communityField = "field_n_hh_communal_ws",
 * )
 */
class WithoutHouseholdF2d10 extends AbstractWithoutHouseholdCalcBase
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        // 2.10 = A5 - B1
        $f1d5 = $this->inquiry->{'field_total_households'};
        $f2d1 = $this->inquiry->{'field_households_without_water_supply_system'};

        $this->inquiry->{'field_n_hh_communal_ws'} = $f1d5 - $f2d1;
    }
}
