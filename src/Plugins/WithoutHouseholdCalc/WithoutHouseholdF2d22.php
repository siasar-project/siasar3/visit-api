<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\WithoutHouseholdCalc;

use App\Annotations\WithoutHouseholdCalc;
use App\Plugins\AbstractWithoutHouseholdCalcBase;

/**
 * 2.22 = A5 - B3.2.1
 *
 * @WithoutHouseholdCalc(
 *     id = "2d22",
 *     active = true,
 *     communityField = "field_n_hh_from_comm_ws_not_rain_collection",
 * )
 */
class WithoutHouseholdF2d22 extends AbstractWithoutHouseholdCalcBase
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        // 2.22 = A5 - B3.2.1
        $f1d5 = $this->inquiry->{'field_total_households'};
        $f2d3d2d1 = $this->inquiry->{'field_households_with_individual_rainwater_collection'};

        $this->inquiry->{'field_n_hh_from_comm_ws_not_rain_collection'} = $f1d5 - $f2d3d2d1;
    }
}
