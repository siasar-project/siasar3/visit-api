<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\WithoutHouseholdCalc;

use App\Annotations\WithoutHouseholdCalc;
use App\Plugins\AbstractWithoutHouseholdCalcBase;

/**
 * 3.1.1.5.2 = Si C1.12345.2; C1.5.1
 *
 * @WithoutHouseholdCalc(
 *     id = "3d1d1d5d2",
 *     active = true,
 *     communityField = "field_n_hh_unknown_dest_other_people",
 * )
 */
class WithoutHouseholdF3d1d1d5d2 extends AbstractWithoutHouseholdCalcBase
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        // 3.1.1.5.2 = Si C1.12345.2; C1.5.1
        /** @var bool $known */
        $known = $this->inquiry->{'field_flush_share_facilities_multi_households'};
        if ($known) {
            $value = $this->inquiry->{'field_unknown_number'};
            $this->inquiry->{'field_n_hh_unknown_dest_other_people'} = $value;
        }
    }
}
