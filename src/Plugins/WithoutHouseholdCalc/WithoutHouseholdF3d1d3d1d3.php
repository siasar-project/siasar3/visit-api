<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\WithoutHouseholdCalc;

use App\Annotations\WithoutHouseholdCalc;
use App\Plugins\AbstractWithoutHouseholdCalcBase;

/**
 * 3.1.3.1.3 = Si C1.8910.3; C1.8.1
 *
 * @WithoutHouseholdCalc(
 *     id = "3d1d3d1d3",
 *     active = true,
 *     communityField = "field_n_hh_dpit_slab_members",
 * )
 */
class WithoutHouseholdF3d1d3d1d3 extends AbstractWithoutHouseholdCalcBase
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        // 3.1.3.1.3 = Si C1.8910.3; C1.8.1
        /** @var bool $known */
        $known = $this->inquiry->{'field_composting_all_members_use_toilet'};
        if ($known) {
            $value = $this->inquiry->{'field_twinpit_with_slab_number'};
            $this->inquiry->{'field_n_hh_dpit_slab_members'} = $value;
        }
    }
}
