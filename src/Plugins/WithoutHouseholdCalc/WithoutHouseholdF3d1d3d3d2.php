<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Plugins\WithoutHouseholdCalc;

use App\Annotations\WithoutHouseholdCalc;
use App\Plugins\AbstractWithoutHouseholdCalcBase;

/**
 * 3.1.3.3.2 = Si C1.8910.2; C1.10.1
 *
 * @WithoutHouseholdCalc(
 *     id = "3d1d3d3d2",
 *     active = true,
 *     communityField = "field_n_hh_other_compost_nmembers",
 * )
 */
class WithoutHouseholdF3d1d3d3d2 extends AbstractWithoutHouseholdCalcBase
{
    /**
     * @inheritDoc
     */
    public function execute()
    {
        // 3.1.3.3.2 = Si C1.8910.2; C1.10.1
        /** @var bool $known */
        $known = $this->inquiry->{'field_composting_share_facilities_multi_households'};
        if ($known) {
            $value = $this->inquiry->{'field_other_composting_toilet_number'};
            $this->inquiry->{'field_n_hh_other_compost_nmembers'} = $value;
        }
    }
}
