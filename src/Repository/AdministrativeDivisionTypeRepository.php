<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Traits\ServiceEntityRepositoryTrait;
use App\Entity\AdministrativeDivisionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdministrativeDivisionType|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdministrativeDivisionType|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdministrativeDivisionType[]    findAll()
 * @method AdministrativeDivisionType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdministrativeDivisionTypeRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    /**
     *   AdministrativeDivisionTypeRepository constructor
     *   @param ManagerRegistry $registry entity manager for Material
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdministrativeDivisionType::class);
    }

    /**
     * create a adt
     *
     * @param string  $typeName
     * @param integer $level
     *
     * @return AdministrativeDivisionType
     */
    public function create(string $typeName, $level)
    {
        $administrativeDivisionType = new AdministrativeDivisionType();
        $administrativeDivisionType->setName($typeName)
                                   ->setLevel($level);

        return $administrativeDivisionType;
    }
}
