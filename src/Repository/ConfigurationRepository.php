<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationRead;
use App\Entity\Configuration\ConfigurationWrite;
use App\Entity\User;
use App\Traits\ServiceEntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Configuration repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    /**
     * ConfigurationRepository constructor.
     *
     * @param ManagerRegistry $registry Manager.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Configuration::class);
    }

    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param mixed    $id          The identifier.
     * @param int|null $lockMode    Unused.
     * @param int|null $lockVersion Unused.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function find($id, $lockMode = null, $lockVersion = null): ?object
    {
        $response = $this->findBy(['id' => $id]);
        if (count($response) > 0) {
            return current($response);
        }

        return null;
    }

    /**
     * Exist this configuration?
     *
     * @param string $id Configuration Id.
     *
     * @return bool
     */
    public function exist(string $id): bool
    {
        return ($this->find($id) !== null);
    }

    /**
     * Finds an entity by its primary key / identifier in user space.
     *
     * @param User     $user        The user.
     * @param mixed    $id          The identifier.
     * @param int|null $lockMode    Unused.
     * @param int|null $lockVersion Unused.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function findInUser(User $user, $id, $lockMode = null, $lockVersion = null): ?object
    {
        return $this->find($this->getUserKeyRoot($user).$id, $lockMode, $lockVersion);
    }

    /**
     * Finds an entity by its primary key / identifier.
     *
     * @param mixed    $id          The identifier.
     * @param int|null $lockMode    One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     *
     * @throws \Exception
     */
    public function findEditable($id, $lockMode = null, $lockVersion = null): ?object
    {
        $response = parent::find($id, $lockMode, $lockVersion);
        if (!$response) {
            return null;
        }

        return new ConfigurationWrite($this->_em, $id, $response);
    }

    /**
     * Finds an entity by its primary key / identifier in user space.
     *
     * @param user     $user        The user.
     * @param mixed    $id          The identifier.
     * @param int|null $lockMode    One of the \Doctrine\DBAL\LockMode::* constants
     *                              or NULL if no specific lock mode should be used
     *                              during the search.
     * @param int|null $lockVersion The lock version.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     *
     * @throws \Exception
     */
    public function findEditableInUser(User $user, $id, $lockMode = null, $lockVersion = null): ?object
    {
        return $this->findEditable($this->getUserKeyRoot($user).$id, $lockMode, $lockVersion);
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array      $criteria Find criteria.
     * @param array|null $orderBy  Response order.
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?object
    {
        $response = $this->findBy($criteria, $orderBy);
        if (count($response) > 0) {
            return current($response);
        }

        return null;
    }

    /**
     * Finds all entities in the repository.
     *
     * @return array
     */
    public function findAll(): array
    {
        return $this->findBy([]);
    }

    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria Find criteria.
     * @param array|null $orderBy  Response order.
     * @param null       $limit    Response limit.
     * @param null       $offset   Response offset.
     *
     * @return mixed[]|object[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $response = parent::findBy($criteria, $orderBy, $limit, $offset);
        foreach ($response as &$cfg) {
            $cfg = new ConfigurationRead($cfg->getId(), $cfg->getValue());
        }

        return $response;
    }

    /**
     * Fetches configurations with the key start with.
     *
     * @param string $start Prefix key.
     *
     * @return array
     *
     * @see https://stackoverflow.com/a/14788419
     */
    public function findByKeyStart(string $start): array
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $q  = $qb->select('p')
            ->from('App\Entity\Configuration', 'p')
            ->where("p.id LIKE :key ESCAPE '!'")
            ->setParameter('key', $start.'%')
            ->getQuery();

        return $q->getResult();
    }

    /**
     * Create a new configuration.
     *
     * @param string $id     Configuration Id.
     * @param array  $values Configuration values.
     *
     * @return ConfigurationWrite
     *
     * @throws \Exception
     */
    public function create(string $id, array $values): ConfigurationWrite
    {
        $cfg = new ConfigurationWrite($this->_em, $id);
        $cfg->setValue($values);

        return $cfg;
    }

    /**
     * Create a new virtual configuration.
     *
     * This configuration can't be saved.
     *
     * @param string $id     Configuration Id.
     * @param array  $values Configuration values.
     *
     * @return ConfigurationRead
     *
     * @throws \Exception
     */
    public function createVirtual(string $id, array $values): ConfigurationRead
    {
        return new ConfigurationRead($id, $values);
    }

    /**
     * Delete a configuration.
     *
     * @param string $id Configuration ID.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function delete(string $id): void
    {
        $conf = parent::find($id);
        if ($conf) {
            $this->removeNow($conf);
        }
    }

    /**
     * Create a user configuration.
     *
     * @param User   $user   The user.
     * @param string $id     The configuration Id.
     * @param array  $values Configuration values.
     *
     * @return ConfigurationWrite
     *
     * @throws \Exception
     */
    public function createInUser(User $user, string $id, array $values): ConfigurationWrite
    {
        return $this->create($this->getUserKeyRoot($user).$id, $values);
    }

    /**
     * Get configuration key root to users.
     *
     * @param User $user The user.
     *
     * @return string The root key ends with dot, ".".
     */
    public function getUserKeyRoot(User $user): string
    {
        return 'user.'.$user->getId().'.';
    }
}
