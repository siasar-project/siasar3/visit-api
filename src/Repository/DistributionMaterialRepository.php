<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Traits\ServiceEntityRepositoryTrait;
use App\Entity\DistributionMaterial;
use App\Traits\StringTranslationTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Distribution Material Repository
 * @method DistributionMaterial|null find($id, $lockMode = null, $lockVersion = null)
 * @method DistributionMaterial|null findOneBy(array $criteria, array $orderBy = null)
 * @method DistributionMaterial[]    findAll()
 * @method DistributionMaterial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DistributionMaterialRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;
    use StringTranslationTrait;

    /**
     *   DistributionMaterialRepository constructor
     *
     *   @param ManagerRegistry $registry entity manager for Distribution Material
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DistributionMaterial::class);
    }

    /**
     * @inheritDoc
     */
    public function remove(Object $entity): void
    {
        if (self::getParametricTool()->isDefaultParametric($entity)) {
            throw new \Exception(self::t("You can't remove this entity."));
        }
        $this->_em->remove($entity);
    }
}
