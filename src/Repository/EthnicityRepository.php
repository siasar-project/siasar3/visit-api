<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Traits\ServiceEntityRepositoryTrait;
use App\Entity\Ethnicity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ethnicity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ethnicity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ethnicity[]    findAll()
 * @method Ethnicity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EthnicityRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    /**
     *   EthnicityRepository constructor
     *   @param ManagerRegistry $registry entity manager for Ethnicity
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ethnicity::class);
    }
}
