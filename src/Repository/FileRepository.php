<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\Country;
use App\Entity\File;
use App\Service\FileSystem;
use App\Traits\ServiceEntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * File repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    protected FileSystem $fileSystem;

    /**
     * Construct repository.
     *
     * @param ManagerRegistry $registry
     * @param FileSystem      $fileSystem
     */
    public function __construct(ManagerRegistry $registry, FileSystem $fileSystem)
    {
        parent::__construct($registry, File::class);
        $this->fileSystem = $fileSystem;
    }

    /**
     * Create a new file.
     *
     * @param UploadedFile|string $source  Complete source path with filename or request uploaded file.
     * @param Country|null        $country Owner country.
     * @param bool                $move    Move or copy the file?
     *
     * @return File
     */
    public function create(UploadedFile|string $source, Country $country = null, bool $move = true): File
    {
        $uploaded = null;
        if ($source instanceof UploadedFile) {
            $uploaded = $source;
            $source = (string) $source;
            $move = true;
        }
        // Source is required.
        if (!is_file($source) || is_dir($source)) {
            throw new \Exception(sprintf('File not found: "%s"', $source));
        }

        // Prepare destination folder.
        $countryFolder = ($country) ? $country->getCode() : 'world';

        // Create file and move content.
        $fileInfo = array_merge(['dirname' => '', 'basename' => '', 'extension' => '', 'filename' => ''], pathinfo($source));
        $oldFileName = $fileInfo['basename'];
        if ($uploaded) {
            $oldFileName = $uploaded->getClientOriginalName();
        }

        $file = new File();

        $file->setFileName($oldFileName)
             ->setPath('.')
             ->setCountry($country);

        $this->saveNow($file);

        // The final ID is created after save the entity.
        $relativePath = $countryFolder.'/'.$file->getCreated()->format('Y-m');
        $destination = $this->fileSystem->getPublicFolder().'/'.$relativePath;
        if (!is_dir($destination)) {
            @mkdir($destination, 0777, true);
        }
        $destination .= '/'.$oldFileName.'._'.$file->getId();
        $relativePath .= '/'.$oldFileName.'._'.$file->getId();
        if ($move) {
            @rename($source, $destination);
        } else {
            @copy($source, $destination);
        }

        $file->setPath($relativePath);
        $this->saveNow($file);

        return $file;
    }
}
