<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Traits\ServiceEntityRepositoryTrait;
use App\Entity\HouseholdProcess;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Household Process Repository
 * @method HouseholdProcess|null find($id, $lockMode = null, $lockVersion = null)
 * @method HouseholdProcess|null findOneBy(array $criteria, array $orderBy = null)
 * @method HouseholdProcess[]    findAll()
 * @method HouseholdProcess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HouseholdProcessRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    protected FormFactory $formFactory;

    /**
     * HouseholdProcessRepository constructor
     *
     * @param ManagerRegistry $registry    entity manager for Household Process
     * @param FormFactory     $formFactory
     */
    public function __construct(ManagerRegistry $registry, FormFactory $formFactory)
    {
        parent::__construct($registry, HouseholdProcess::class);
        $this->formFactory = $formFactory;
    }

    /**
     * Close the HouseholdProcess
     *
     * @param HouseholdProcess $householdProcess
     */
    public function closeProcess(HouseholdProcess $householdProcess)
    {
//        $this->_em->beginTransaction();
//        try {
            $householdProcess->setOpen(false);
            $this->saveNow($householdProcess);
//            $this->_em->commit();
//        } catch (\Exception $e) {
//            $this->_em->rollback();
//            throw $e;
//        }
    }

    /**
     * Update the total of finished households.
     *
     * @param HouseholdProcess $householdProcess
     */
    public function updateFinishedCounter(HouseholdProcess $householdProcess)
    {
//        $this->_em->beginTransaction();
//        try {
            $finished = $householdProcess->getFinishedHousehold();
            $householdProcess->setFinishedHousehold($finished + 1);
            $this->saveNow($householdProcess);
//            $this->_em->commit();
//        } catch (\Exception $e) {
//            $this->_em->rollback();
//            throw $e;
//        }
    }

    /**
     * Remove entity.
     *
     * @param Object $entity
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(Object $entity): void
    {
        // Find all households of this Process
        $householdForm = $this->formFactory->find('form.community.household');
        $households = $householdForm->findBy(
            [
                'field_household_process' => $entity->getId(),
            ]
        );

//        $this->_em->beginTransaction();
//        try {
            /** @var FormRecord $household */
        foreach ($households as $household) {
            // Delete the households forms
            $household->delete();
        }

            // Remove the HouseholdProcess
            $this->_em->remove($entity);
//            $this->_em->commit();
//        } catch (\Exception $e) {
//            $this->_em->rollback();
//            throw $e;
//        }
    }
}
