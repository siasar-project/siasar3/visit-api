<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\InquiryFormLog;
use App\Traits\ServiceEntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Inquiry form log repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @method InquiryFormLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method InquiryFormLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method InquiryFormLog[]    findAll()
 * @method InquiryFormLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InquiryFormLogRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InquiryFormLog::class);
    }
}
