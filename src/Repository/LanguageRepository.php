<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\Language;
use App\Traits\ServiceEntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Language entity repository.
 *
 * @method Language|null find($id, $lockMode = null, $lockVersion = null)
 * @method Language|null findOneBy(array $criteria, array $orderBy = null)
 * @method Language[]    findAll()
 * @method Language[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @category Tools
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LanguageRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    /**
     * LanguageRepository constructor.
     *
     * @param ManagerRegistry $registry Manager registry.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Language::class);
    }

    /**
     * Create new language from a request.
     *
     * @param string $id      ID.
     * @param string $isoCode ISO code.
     * @param string $name    Name.
     *
     * @return Language
     */
    public function create(string $id, string $isoCode, string $name): Language
    {
        $lang = new Language($id);
        $lang->setName($name);
        $lang->setIsoCode($isoCode);

        $this->saveNow($lang);

        return $lang;
    }

    /**
     * Save a language entity now.
     *
     * @param Language $lang The language entity.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Language $lang): void
    {
        if (empty($lang->getId())) {
            throw new BadRequestException('ID required');
        }

        if (empty($lang->getName())) {
            throw new BadRequestException('Language name required');
        }

        $this->_em->persist($lang);
    }

    /**
     * Save a language entity now.
     *
     * Do a persist and a flush.
     *
     * @param Language $lang The language entity.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveNow(Language $lang): void
    {
        $this->save($lang);
        $this->_em->flush();
    }

    /**
     * Find a language by field.
     *
     * @param string $field The field name to find.
     * @param string $value The value to find.
     *
     * @return Language|null The user or null if not exist.
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByField(string $field, string $value): ?Language
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.'.$field.' = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Remove a language now.
     *
     * @param string $id ID Language to delete.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeNow(string $id): void
    {
        $lang = $this->findOneById($id);

        if (!$lang instanceof Language) {
            throw new BadRequestHttpException(sprintf('Language with id %s not found', $id));
        }

        $this->remove($lang);
        $this->_em->flush();
    }

    /**
     * Update a language.
     *
     * @param string $id   ID Language.
     * @param string $name Language name.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(string $id, string $name): void
    {
        $lang = $this->findOneById($id);

        if (!$lang instanceof Language) {
            throw new BadRequestHttpException(
                \sprintf('Language with id \'%s\' not found', $id)
            );
        }

        $lang->setName($name);

        if (empty($lang->getName())) {
            throw new BadRequestException('Language name required');
        }

        $this->_em->flush();
    }
}
