<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Traits\ServiceEntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * LocaleSource entity repository.
 *
 * @method LocaleSource|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocaleSource|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocaleSource[]    findAll()
 * @method LocaleSource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @category Tools
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LocaleSourceRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;

    /**
     * LocaleSourceRepository constructor.
     *
     * @param ManagerRegistry $registry Manager registry.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocaleSource::class);
    }

    /**
     * Create new source from a request.
     *
     * @param string  $message  Message source.
     * @param ?string $context  Context.
     * @param bool    $isPlural Is this source an plural?
     *
     * @return LocaleSource
     */
    public function create(string $message, string $context = null, bool $isPlural = false): LocaleSource
    {
        $localeSource = new LocaleSource();
        $localeSource->setMessage($message);
        $localeSource->setContext($context);
        $localeSource->setIsPlural($isPlural);

        $this->saveNow($localeSource);

        return $localeSource;
    }

    /**
     * Save a locale source now.
     *
     * Do a persist and a flush.
     *
     * @param LocaleSource $localeSource The language entity.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveNow(LocaleSource $localeSource): void
    {
        if (empty($localeSource->getMessage())) {
            throw new BadRequestException('Message required');
        }

        $this->save($localeSource);
        $this->_em->flush();
    }

    /**
     * Find a locale source by field.
     *
     * @param string $field The field name to find.
     * @param string $value The value to find.
     *
     * @return LocaleSource|null The user or null if not exist.
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByField(string $field, string $value): ?LocaleSource
    {
        return $this->createQueryBuilder('ls')
            ->andWhere('ls.'.$field.' = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get all translations from a source (all languages).
     *
     * @param LocaleSource $source The source
     *
     * @return array|null
     */
    public function getAllTranslationsBySource(LocaleSource $source): ?array
    {
        /**
         * LocaleTarget Repository.
         *
         * @var LocaleTargetRepository $targetRepository
         */
        $targetRepository = $this->_em->getRepository(LocaleTarget::class);
        $translations = $targetRepository->findByLocaleSource($source);

        return $translations;
    }

    /**
     * Get literals
     *
     * @param string $lang     Language ID
     * @param string $searchIn ENUM('all', 'untranslated', 'translated')
     * @param array  $filter
     * @param int    $limit
     * @param int    $offset
     *
     * @return array|null
     */
    public function getLiterals(string $lang, string $searchIn = 'all', array $filter = [], ?int $limit = null, ?int $offset = null)
    {
        $languages = [];
        $langLevel = explode('_', $lang);
        // Build language inheritance structure
        foreach ($langLevel as $key => $value) {
            if (count($languages) === 0) {
                $languages[] = $value;
            } else {
                $languages[] = $languages[$key-1].'_'.$value;
            }
        }

        $query = $this->createQueryBuilder('ls');
        $query->select('ls, LENGTH(IDENTITY(lt.language)) as HIDDEN id_len');
        $query->leftJoin(
            LocaleTarget::class,
            'lt',
            Join::WITH,
            $query->expr()->andX(
                $query->expr()->eq('lt.localeSource', 'ls.id'),
                $query->expr()->IN('lt.language', array_reverse($languages)),
            ),
        );

        if (isset($filter['context'])) {
            $query->andWhere('ls.context = :context')
                ->setParameter('context', $filter['context']);
        }

        // The string to search
        if (isset($filter['stringContains'])) {
            $query->andWhere($query->expr()->orX(
                "lt.translation LIKE :search",
                "ls.message LIKE :search"
            ))
            ->setParameter('search', '%'.$filter['stringContains'].'%');
        }

        switch ($searchIn) {
            case 'translated':
                $query->andWhere($query->expr()->isNotNull('lt.translation'));
                break;
            case 'untranslated':
                $query->andWhere($query->expr()->isNull('lt.translation'));
                break;
            default:
                // Both
                break;
        }

        $query->orderBy('ls.id', 'ASC');
        $query->orderBy('id_len', 'DESC');
        $query->groupBy('ls.id');

        // With pagination
        if ($limit) {
            $query->setMaxResults($limit);
            $query->setFirstResult($offset);
        }

        return $query->getQuery()->getResult();
    }
}
