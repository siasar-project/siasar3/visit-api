<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Service\SessionService;
use App\Tools\TranslatableMarkup;
use App\Traits\GetContainerTrait;
use App\Traits\ServiceEntityRepositoryTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Uid\Ulid;

/**
 * LocaleTarget entity repository.
 *
 * @method LocaleTarget|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocaleTarget|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocaleTarget[]    findAll()
 * @method LocaleTarget[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @category Tools
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LocaleTargetRepository extends ServiceEntityRepository
{
    use ServiceEntityRepositoryTrait;
    use GetContainerTrait;

    private ?SessionService $sessionService;

    /**
     * LocaleTargetRepository constructor.
     *
     * @param ManagerRegistry $registry Manager registry.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocaleTarget::class);
        $this->sessionService = $this->getContainerInstance()->get('session_service');
    }

    /**
     * Translate a string wrapper.
     *
     * @param TranslatableMarkup $translatedString The string wrapper.
     *
     * @return string
     */
    public function translateString(TranslatableMarkup $translatedString)
    {
        return $this->doTranslate($translatedString->getUntranslatedString(), $translatedString->getOptions());
    }

    /**
     * Check if has translation.
     *
     * @param TranslatableMarkup $translatedString The string wrapper.
     *
     * @return bool
     */
    public function hasTranslation(TranslatableMarkup $translatedString): bool
    {
        return $this->doTranslate($translatedString->getUntranslatedString(), $translatedString->getOptions(), true);
    }

    /**
     * Create new translation from a request.
     *
     * @param Language     $language Language ID.
     * @param LocaleSource $source   LocaleSource ID.
     * @param string       $message  Message translation.
     * @param ?string      $context  Context.
     *
     * @return LocaleTarget
     */
    public function create(Language $language, LocaleSource $source, string $message, ?string $context = null): ?LocaleTarget
    {
        $localeTarget = new LocaleTarget($language, $source);
        $localeTarget->setTranslation($message);
        $localeTarget->setContext($context);

        $this->saveNow($localeTarget);

        return $localeTarget;
    }

    /**
     * Save a locale source now.
     *
     * @param LocaleTarget $localeTarget The LocaleTarget entity.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(LocaleTarget $localeTarget): void
    {
        if (empty($localeTarget->getTranslation())) {
            throw new BadRequestException('Message required');
        }

        $this->_em->persist($localeTarget);
    }

    /**
     * Translates a string to the current language or to a given language.
     *
     * @param string $string
     *   A string containing the English text to translate.
     * @param array  $options
     *   An associative array of additional options, with the following elements:
     *   - 'langcode': The language code to translate to a language other than
     *      what is used to display the page.
     *   - 'context': The context the source string belongs to.
     * @param ?bool  $queryExists
     *   If true, it returns whether it has a translation or not. If false, it returns the translation.
     *
     * @return string
     *   The translated string.
     *
     * @source Drupal <drupal.org>
     */
    protected function doTranslate(string $string, array $options = [], ?bool $queryExists = false): string
    {
        // If a NULL langcode has been provided, unset it.
        if (!isset($options['langcode']) && array_key_exists('langcode', $options)) {
            unset($options['langcode']);
        }

        if (!isset($options['langcode'])) {
            // Set current user language.
            // Get user preferred language.
            $user = $this->sessionService->getUser();
            if ($user && $user->getLanguage()) {
                $options['langcode'] = $user->getLanguage()->getId();
            }
        }

        // Merge in options defaults.
        $options = $options + [
                'langcode' => 'ENGL',
                'context' => null,
            ];

        $localeSourceRepo = $this->_em->getRepository(LocaleSource::class);

        $criteria = [
            'message' => $string,
            'context' => $options['context'],
        ];
        if (is_null($criteria['context']) || empty($criteria['context'])) {
            unset($criteria['context']);
        }
        $source = $localeSourceRepo->findOneBy($criteria);
        if (!$source) {
            // Add the new string, and return it.
            $this->_em->getRepository(LocaleSource::class)->create($string, $options['context']);

            return $string;
        }
        // Query with inheritance.
        $inheritance = explode('_', $options['langcode']);
        $where = [];
        foreach ($inheritance as $key => $lid) {
            $id = [];
            for ($i = 0; $i <= $key; ++$i) {
                $id[] = $inheritance[$i];
            }
            $id = implode('_', $id);
            $where[] = "IDENTITY(q.language) = '$id'";
        }
        $where = '('.implode(' OR ', $where).')';
        $ulid = new Ulid($source->getId());
        /** @var QueryBuilder $query */
        $query = $this->_em->getRepository(LocaleTarget::class)->createQueryBuilder('q')
            ->select('q, LENGTH(IDENTITY(q.language)) as HIDDEN id_len')
            ->where('IDENTITY(q.localeSource) = :source')
            ->setParameter(':source', $ulid->toBinary())
            ->andWhere($where)
            ->orderBy('id_len', 'DESC')
            ->setMaxResults($queryExists ? 1 : 4)
            ->setMaxResults(4);
        $q = $query->getQuery();
        /** @var LocaleTarget $result */
        $result = current($q->execute());

        if (!$queryExists) {
            return $result ? $result->getTranslation() : $string;
        }

        return $result ? true : false;
    }
}
