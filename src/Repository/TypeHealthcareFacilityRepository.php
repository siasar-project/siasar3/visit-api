<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\TypeHealthcareFacility;
use App\Traits\ServiceEntityRepositoryTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeHealthcareFacility|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeHealthcareFacility|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeHealthcareFacility[]    findAll()
 * @method TypeHealthcareFacility[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeHealthcareFacilityRepository extends ServiceEntityRepository
{
    use StringTranslationTrait;
    use ServiceEntityRepositoryTrait;

    /**
     *   TypeHealthcareFacilityRepository constructor
     *   @param ManagerRegistry $registry entity manager for Material
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeHealthcareFacility::class);
    }

    /**
     * @inheritDoc
     */
    public function remove(Object $entity): void
    {
        if (self::getParametricTool()->isDefaultParametric($entity)) {
            throw new \Exception(self::t("You can't remove this entity."));
        }
        $this->_em->remove($entity);
    }
}
