<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Repository;

use App\Entity\Country;
use App\Entity\User;
use App\Service\Mailer\MailerService;
use App\Service\Password\EncoderService;
use App\Service\Request\RequestService;
use App\Templating\TwigTemplate;
use App\Traits\ServiceEntityRepositoryTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User entity repository.
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    use ServiceEntityRepositoryTrait;
    use StringTranslationTrait;

    protected EncoderService $encoderService;
    protected MailerService $mailer;

    /**
     * UserRepository constructor.
     *
     * @param ManagerRegistry $registry       Manager registry.
     * @param EncoderService  $encoderService Encoder service.
     * @param MailerService   $mailer         Mailer service.
     */
    public function __construct(ManagerRegistry $registry, EncoderService $encoderService, MailerService $mailer)
    {
        parent::__construct($registry, User::class);
        $this->encoderService = $encoderService;
        $this->mailer = $mailer;
    }

    /**
     * Resend activation email.
     *
     * @param Request $request Current request.
     *
     * @throws NonUniqueResultException
     *
     * @return void
     */
    public function resend(Request $request): void
    {
        $email = RequestService::getField($request, 'email');
        $user = $this->findOneByEmail($email);

        if (!$user) {
            throw new UnprocessableEntityHttpException($this->t('User email not found'));
        }

        if ($user->isActive()) {
            throw new BadRequestHttpException($this->t('User already active'));
        }

        $user->setToken(\sha1(\uniqid()));
        $this->saveNow($user);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     *
     * @param UserInterface $user               User.
     * @param string        $newEncodedPassword New password.
     *
     * @throws ORMException
     * @throws OptimisticLockException
     *
     * @return void
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException($this->t('Instances of "@class" are not supported.', ['@class' => \get_class($user)]));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * Create a new user from a request.
     *
     * @param string  $name     User name.
     * @param string  $email    User email.
     * @param string  $password User password.
     * @param Country $country  User country
     *
     * @return User
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(string $name, string $email, string $password, Country $country): User
    {
        $user = new User();
        $user->setUsername($name);
        $user->setEmail($email);
        $user->setPassword($this->encoderService->generateEncodedPassword($user, $password));
        $user->setCountry($country);
        if ((bool) $_ENV['APP_USER_AUTO_ACTIVATION']) {
            $user->setActive(true);
        }
        $this->saveNow($user);

        if (!(bool) $_ENV['APP_USER_AUTO_ACTIVATION']) {
            // Send activation mail.
            $payload = [
                'name' => $user->getUsername(),
                // todo Use Url generation by route.
                'url' => \sprintf(
                    '%s%s?token=%s&uid=%s',
                    '/',
                    'client route',
                    $user->getToken(),
                    $user->getId()
                ),
            ];
            $this->mailer->send($user->getEmail(), TwigTemplate::USER_REGISTER, $payload);
        }

        return $user;
    }

    /**
     * Save a user entity now.
     *
     * @param User $user The user entity.
     *
     * @throws ORMException
     * @throws OptimisticLockException
     *
     * @return void
     */
    public function save(User $user): void
    {
        // todo Validate in a single site.
        // todo This validations break updating users.
        if (empty($user->getEmail())) {
            throw new BadRequestHttpException('Email required');
        }
        if (empty($user->getEmail()) || $otherUser = $this->findOneByEmail($user->getEmail())) {
            if ($otherUser->getId() !== $user->getId()) {
                throw new UnprocessableEntityHttpException(
                    $this->t("User with email '@email' already exist", ['@email' => $user->getEmail()])
                );
            }
        }

        if (empty($user->getUsername())) {
            throw new BadRequestHttpException($this->t('User name required'));
        }
        if ($otherUser = $this->findOneByUserName($user->getUsername())) {
            if ($otherUser->getId() !== $user->getId()) {
                throw new UnprocessableEntityHttpException(
                    $this->t("User with user name '@name' already exist", ['@name' => $user->getUsername()])
                );
            }
        }

        $this->_em->persist($user);
    }

    /**
     * Find user by field.
     *
     * @param string $field The field name to find.
     * @param string $value The value to find.
     *
     * @return User|null The user or null if not exist.
     *
     * @throws NonUniqueResultException
     */
    public function findOneByField($field, $value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.'.$field.' = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find user by email.
     *
     * @param string $value The email to find.
     *
     * @return User|null The user or null if not exist.
     *
     * @throws NonUniqueResultException
     */
    public function findOneByEmail(string $value): ?User
    {
        return $this->findOneByField('email', $value);
    }

    /**
     * Find user by user name.
     *
     * @param string $value The user name to find.
     *
     * @return User|null The user or null if not exist.
     *
     * @throws NonUniqueResultException
     */
    public function findOneByUserName(string $value): ?User
    {
        return $this->findOneByField('username', $value);
    }

    /**
     * Find users by role.
     *
     * @param string[] $roles
     *
     * @return User[]
     *
     * @see https://stackoverflow.com/a/16692911
     */
    public function findByRole(array $roles): array
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u');

        foreach ($roles as &$role) {
            $role = strtoupper($role);
        }
        if (!in_array('ROLE_USER', $roles)) {
            $first = true;
            foreach ($roles as $role) {
                $id = uniqid();
                if ($first) {
                    $first = false;
                    $qb->where('u.roles LIKE :roles'.$id)
                        ->setParameter('roles'.$id, '%"'.$role.'"%');
                } else {
                    $qb->orWhere('u.roles LIKE :roles'.$id);
                    $qb->setParameter('roles'.$id, '%"'.$role.'"%');
                }
            }
        }

        return $qb->getQuery()->getResult();
    }
}
