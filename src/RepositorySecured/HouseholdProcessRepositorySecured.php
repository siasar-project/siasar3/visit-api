<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\RepositorySecured;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Forms\FormFactory;
use App\Repository\HouseholdProcessRepository;
use App\Security\Handlers\DoctrineAccessHandler;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Tools\PublicServiceInterface;
use App\Traits\RepositorySecuredTrait;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Household Process repository with security.
 *
 * @method void                  save(HouseholdProcess $entity)
 * @method void                  saveNow(HouseholdProcess $entity)
 * @method void                  remove(HouseholdProcess $entity)
 * @method void                  removeNow(HouseholdProcess $entity)
 * @method HouseholdProcess|null find($id, $lockMode = null, $lockVersion = null)
 * @method HouseholdProcess|null findOneBy(array $criteria, array $orderBy = null)
 * @method HouseholdProcess[]    findAll()
 *
 * @-method HouseholdProcess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @method UserInterface         getCurrentUser()
 */
class HouseholdProcessRepositorySecured extends HouseholdProcessRepository implements PublicServiceInterface
{
    use RepositorySecuredTrait;

    protected AccessManager $accessManager;

    /**
     * HouseholdProcessRepository constructor
     *
     * @param ManagerRegistry       $registry       entity manager
     * @param SessionService        $sessionService
     * @param IriConverterInterface $iriConverter
     * @param FormFactory           $formFactory
     */
    public function __construct(ManagerRegistry $registry, SessionService $sessionService, IriConverterInterface $iriConverter, FormFactory $formFactory)
    {
        parent::__construct($registry, $formFactory);

        $this->accessManager = $this->getContainerInstance()->get('access_manager');
        $this->accessHandler = new DoctrineAccessHandler(HouseholdProcess::class, $this->accessManager, $sessionService, $iriConverter);
    }

    /**
     * Close the HouseholdProcess
     *
     * @param HouseholdProcess $householdProcess
     */
    public function closeProcess(HouseholdProcess $householdProcess)
    {
        // Get user session.
        $user = $this->accessHandler->getCurrentUser();
        // Validate update permission.
        $this->accessHandler->checkEntityAccess(
            $user,
            DoctrineAccessHandler::ACCESS_ACTION_UPDATE,
            ['country' => $user->getCountry()]
        );
        // Close process.
        parent::closeProcess($householdProcess);
    }

    /**
     * {@inheritDoc}
     *
     * @see \Doctrine\ORM\EntityRepository::findBy
     */
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        // Current user.
        $user = SessionService::getUser();
        $ulid = Ulid::fromString($user->getId());
        $currentUserUlid = $ulid->toBinary();

        // Create DBAL query builder.
        $queryBuilder = $this->_em->getConnection()->createQueryBuilder();
        $queryBuilder
            ->select('hhp.id')
            ->from('household_process', 'hhp');

        if (!$this->accessManager->hasPermission($user, 'all permissions')) {
            // Filter by point teams.
            $queryBuilder->innerJoin(
                'hhp',
                'form_point__field_communities',
                'fpc',
                'hhp.community_reference = fpc.field_communities_value',
            );
            $queryBuilder->innerJoin(
                'fpc',
                'form_point',
                'fp',
                'fpc.record = fp.id'
            );
            $queryBuilder->innerJoin(
                'fp',
                'form_point__field_team',
                'fpt',
                'fp.id = fpt.record'
            );
            $queryBuilder->andWhere('fpt.field_team_value = :currentUserUlid');
            $queryBuilder->setParameter('currentUserUlid', $currentUserUlid);
        }

        // Get entity metadata to inspect the fields.
        $metadata = $this->_em->getClassMetadata(HouseholdProcess::class);
        foreach ($criteria as $field => $value) {
            // Rename entity fields with database column name.
            $field = $this->translateFieldToDBAL($field);
            // Apply filter.
            if ($metadata->hasField($field)) {
                // Get field type from definition.
                $fieldType = $metadata->getTypeOfField($field);

                // Performs the appropriate type conversion based on the field type
                // We ensure that the values that come as strings (for example,
                // from web forms or APIs) are correctly converted to the data types
                // necessary for the query.
                switch ($fieldType) {
                    case 'boolean':
                        // Converts strings 'true' or 'false' to a boolean value
                        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN) ? '1' : '0';
                        break;
                    case 'ulid':
                        if ($value instanceof HouseholdProcess) {
                            $value = $value->getId();
                        }
                        $ulid = Ulid::fromString($value);
                        $value = $ulid->toBinary();
                        break;
                    // Here more type conversions could be handled based on the entity definition.
                    // case 'datetime':
                    //     // Converts the string to a DateTime object
                    //     $value = new \DateTime($value);
                    //     break;
                    // ...other field types and conversions...
                }
            }
            // We add the where out of field validation to stop process with default MySQL error.
            $queryBuilder->andWhere("hhp.$field = :$field")
                ->setParameter($field, $value);
        }

        // Limit user by divisions.
        $userDivisions = $this->accessManager->getUserDivisionsLimits($user);
        if (!empty($userDivisions) || !$this->accessManager->hasPermission($user, 'all administrative divisions')) {
            $queryBuilder->innerJoin(
                'hhp',
                'administrative_division',
                'ad',
                'hhp.administrative_division_id = ad.id'
            );
            $divisionsExpr = [];
            foreach ($userDivisions as $divLimit) {
                $uniqParamId = uniqid('branch');
                $divisionsExpr[] = "ad.branch LIKE :$uniqParamId";
                $queryBuilder->setParameter($uniqParamId, $divLimit.'%');
            }
            if (!empty($divisionsExpr)) {
                $queryBuilder->andWhere(implode(' OR ', $divisionsExpr));
            }
        }

        // Pagination.
        if (null !== $limit) {
            $queryBuilder->setMaxResults($limit);
        }
        if (null !== $offset) {
            $queryBuilder->setFirstResult($offset);
        }

        // Get filtered household process IDs.
        $stmt = $queryBuilder->execute();
        $results = $stmt->fetchAll();

        // Hidrate each household process.
        $ids = array_column($results, 'id');
        $householdProcesses = parent::findBy(['id' => $ids], $orderBy);

        // Return entities.
        return $householdProcesses;
    }

    /**
     * Translate entity field name with database column name.
     *
     * @param string $field
     *
     * @return string
     */
    protected function translateFieldToDBAL(string $field): string
    {
        switch ($field) {
            case 'communityReference':
                $field = 'community_reference';
                break;
            case 'administrativeDivision':
                $field = 'administrative_division_id';
                break;
            case 'finishedHousehold':
                $field = 'finished_household';
                break;
//            case 'id':
//                $field = 'hhp.id';
//                break;
        }

        return $field;
    }
}
