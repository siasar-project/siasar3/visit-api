<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\RepositorySecured;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\TechnicalAssistanceProvider;
use App\Repository\TechnicalAssistanceProviderRepository;
use App\Security\Handlers\DoctrineAccessHandler;
use App\Service\SessionService;
use App\Tools\PublicServiceInterface;
use App\Traits\RepositorySecuredTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * TechnicalAssistanceProvider repository with security.
 *
 * @method void                             save(TechnicalAssistanceProvider $entity)
 * @method void                             saveNow(TechnicalAssistanceProvider $entity)
 * @method void                             remove(TechnicalAssistanceProvider $entity)
 * @method void                             removeNow(TechnicalAssistanceProvider $entity)
 * @method TechnicalAssistanceProvider|null find($id, $lockMode = null, $lockVersion = null)
 * @method TechnicalAssistanceProvider|null findOneBy(array $criteria, array $orderBy = null)
 * @method TechnicalAssistanceProvider[]    findAll()
 * @method TechnicalAssistanceProvider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method UserInterface                    getCurrentUser()
 */
class TechnicalAssistanceProviderRepositorySecured extends TechnicalAssistanceProviderRepository implements PublicServiceInterface
{
    use RepositorySecuredTrait;

    /**
     * TechnicalAssistanceProviderRepository constructor
     *
     * @param ManagerRegistry       $registry       entity manager
     * @param SessionService        $sessionService
     * @param IriConverterInterface $iriConverter
     */
    public function __construct(ManagerRegistry $registry, SessionService $sessionService, IriConverterInterface $iriConverter)
    {
        parent::__construct($registry);

        $accessManager = $this->getContainerInstance()->get('access_manager');
        $this->accessHandler = new DoctrineAccessHandler(TechnicalAssistanceProvider::class, $accessManager, $sessionService, $iriConverter);
    }
}
