<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\Country;

/**
 * GraphQL country create mutation resolver.
 *
 * Defined to allow get the country code how a input argument.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
final class CountryMutationResolver implements MutationResolverInterface
{
    /**
     * @param Country|null $item    The new item data.
     * @param array        $context Context.
     *
     * @return Country
     */
    public function __invoke($item, array $context)
    {
        // Mutation input arguments are in $context['args']['input'].
        // Do something with the country.
        // Or fetch the country if it has not been retrieved.
        // The returned item will pe persisted.
        return $item;
    }
}
