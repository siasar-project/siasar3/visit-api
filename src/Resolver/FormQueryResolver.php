<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\OpenApi\GraphQl\Stage\SecurityStage;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * GraphQL form resolvers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormQueryResolver
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected SecurityStageInterface $securityStage;

    /**
     * Form Query Resolver constructor.
     */
    public function __construct()
    {
        $this->securityStage = $this->getContainerInstance()->get('App\OpenApi\GraphQl\Stage\SecurityStage');
    }

    /**
     * Resolve query collection.
     *
     * @param mixed                $rootValue    Root value.
     * @param array|null           $args         Query arguments.
     * @param array|null           $contextValue Value context.
     * @param ResolveInfo          $info         Query context.
     * @param FormManagerInterface $formInstance The form to query.
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws \Exception
     */
    public function collectionResolve(mixed $rootValue, ?array $args, ?array $contextValue, ResolveInfo $info, FormManagerInterface $formInstance): array
    {
        // Security stage.
        $context = SecurityStage::getDefaultContext($contextValue, $args, $info);
        $context = array_merge(
            $context,
            [
                'is_collection' => true,
                'is_custom' => true,
                'form_manager' => $formInstance,
            ]
        );
        ($this->securityStage)('', 'read', $context);
        // Resolve.
        $count = $formInstance->countBy([]);

        $before = isset($args['before']) ? base64_decode($args['before']) - 1 : null;
        $after = isset($args['after']) ? base64_decode($args['after']) + 1 : null;
        $first = $args['first'] ?? null;
        $last = $args['last'] ?? null;

        $edgesToReturn = $this->edgesToReturn($count, $before, $after, $first, $last);

        $records = $formInstance->findBy([], null, $edgesToReturn->length(), $edgesToReturn->start);
        $edges = [];
        foreach ($records as $index => $record) {
            $cursor = $edgesToReturn->start + $index;
            $edges[] = [
                'node' => $record,
                'cursor' => base64_encode((string) $cursor),
                'int_cursor' => $cursor,
            ];
        }

        return [
            'edges' => $edges,
            'pageInfo' => [
                'endCursor' => base64_encode($edgesToReturn->end - 1),
                'startCursor' => base64_encode($edgesToReturn->start),
                'hasNextPage' => ($count > $edgesToReturn->end),
                'hasPreviousPage' => ($edgesToReturn->start > 0),
            ],
            'totalCount' => $count,
        ];
    }

    /**
     * Resolve field content.
     *
     * @param FormRecord         $record The record.
     * @param array|null         $args   Query arguments.
     * @param FieldTypeInterface $field  Form field type.
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public function formFieldResolve(FormRecord $record, ?array $args, FieldTypeInterface $field): mixed
    {
        $resp = null;
        if (count($field->getProperties()) === 1) {
            $resp = $record->{$field->getId()};
        } else {
            // Flat data.
            $resp = [];
            $fieldName = $field->getId();
            $fieldData = $record->get($fieldName);
            if ($field->isMultivalued()) {
                foreach ($fieldData as $index => $value) {
                    $resp[$index] = $field->formatToUse($value, true);
                }
            } else {
                $resp = $field->formatToUse($fieldData, true);
            }
        }

        return $resp;
    }

    /**
     * Get record Id.
     *
     * @param FormRecord $record The record.
     * @param array|null $args   Query arguments.
     *
     * @return string
     */
    public function recordIdResolve(FormRecord $record, ?array $args): string
    {
        return $record->getId();
    }

    /**
     * Get RestFul item query route.
     *
     * @param FormRecord $record The record.
     * @param array|null $args   Query arguments.
     *
     * @return string
     */
    public function recordIriIdResolve(FormRecord $record, ?array $args): string
    {
        return sprintf(
            '/api/v1/form/data/%ss/%s',
            $record->getForm()->getId(),
            $record->getId()
        );
    }

    /**
     * Find a record by Id.
     *
     * @param mixed                $object       Unknown.
     * @param array|null           $args         Query arguments.
     * @param FormManagerInterface $formInstance Form manager to use.
     *
     * @return mixed
     */
    public function findResolve(mixed $object, ?array $args, FormManagerInterface $formInstance): mixed
    {
        // Security stage.
        $context = SecurityStage::getDefaultContext([], $args, null);
        $context = array_merge(
            $context,
            [
                'is_custom' => true,
                'form_manager' => $formInstance,
            ]
        );
        ($this->securityStage)('', 'read', $context);
        // Resolve.
        if (!isset($args['id'])) {
            return null;
        }
        $id = parse_url($args['id'], PHP_URL_PATH);
        $id = explode('/', $id);
        $id = end($id);

        return $formInstance->find($id);
    }

    /**
     * Resolve a content field.
     *
     * @param ?array      $value   Base record.
     * @param ?array      $args    Query arguments.
     * @param ?array      $context Query context.
     * @param ResolveInfo $info    Resolve context.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function fieldTypeResolveField(?array $value, ?array $args, ?array $context, ResolveInfo $info): mixed
    {
        if ($info->parentType->name === 'forms_date') {
            switch ($info->fieldName) {
                case 'utc_value':
                    $date = new \DateTime($value['value'], new \DateTimeZone($value['timezone']));
                    $date->setTimezone(new \DateTimeZone('UTC'));

                    return $date->format('Y-m-d H:i:s');

                case 'timestamp':
                    $date = new \DateTime($value['value'], new \DateTimeZone($value['timezone']));

                    return $date->getTimestamp();
            }
        }
        if (isset($value[$info->fieldName])) {
            return $value[$info->fieldName];
        }

        return null;
    }

    /**
     * Resolve create payload fields.
     *
     * @param mixed       $rootValue    Base value.
     * @param array|null  $args         Arguments.
     * @param array|null  $contextValue Context.
     * @param ResolveInfo $info         Step information.
     *
     * @return mixed
     */
    public function createPayloadResolveField(mixed $rootValue, ?array $args, ?array $contextValue, ResolveInfo $info): mixed
    {
        // Is querying the client mutation id?
        if ('clientMutationId' === $info->fieldName) {
            // Yes, get it from the record meta data.
            $meta = $rootValue->getMeta();

            return $meta['clientMutationId'] ?? null;
        }

        return $rootValue;
    }

    /**
     * Resolve the delete mutation input.
     *
     * @param mixed                $source  Data source.
     * @param array|null           $args    Arguments.
     * @param array|null           $context Context.
     * @param ResolveInfo          $info    Step information.
     * @param FormManagerInterface $form    Referred form.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function deleteMutationResolver(mixed $source, ?array $args, ?array $context, ResolveInfo $info, FormManagerInterface $form): mixed
    {
        // Security stage.
        $context = SecurityStage::getDefaultContext($context, $args, null);
        $context = array_merge(
            $context,
            [
                'is_mutation' => true,
                'is_custom' => true,
                'form_manager' => $form,
            ]
        );
        ($this->securityStage)('', 'delete', $context);
        // Resolve.
        $clientMutationId = null;
        // Have the input a client mutation id?
        if (isset($args['input']['clientMutationId'])) {
            // Yes, get it to save later.
            $clientMutationId = $args['input']['clientMutationId'];
            unset($args['input']['clientMutationId']);
        }
        // Load data.
        /** @var FormRecord $record */
        $record = (new FormQueryResolver())->findResolve(
            null,
            ['id' => $args["input"]["id"]],
            $form
        );
        $record->setMeta(['clientMutationId' => $clientMutationId]);
        // Delete record.
        $record->delete();

        return $record;
    }

    /**
     * Resolve the update mutation input.
     *
     * @param mixed                $source  Data source.
     * @param array|null           $args    Arguments.
     * @param array|null           $context Context.
     * @param ResolveInfo          $info    Step information.
     * @param FormManagerInterface $form    Referred form.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function updateMutationResolver(mixed $source, ?array $args, ?array $context, ResolveInfo $info, FormManagerInterface $form): mixed
    {
        // Security stage.
        $context = SecurityStage::getDefaultContext($context, $args, null);
        $context = array_merge(
            $context,
            [
                'is_mutation' => true,
                'is_custom' => true,
                'form_manager' => $form,
            ]
        );
        ($this->securityStage)('', 'update', $context);
        // Resolve.
        $clientMutationId = null;
        // Have the input a client mutation id?
        if (isset($args['input']['clientMutationId'])) {
            // Yes, get it to save later.
            $clientMutationId = $args['input']['clientMutationId'];
            unset($args['input']['clientMutationId']);
        }
        // Load data.
        /** @var FormRecord $record */
        $record = (new FormQueryResolver())->findResolve(
            null,
            ['id' => $args["input"]["id"]],
            $form
        );
        $record->setMeta(['clientMutationId' => $clientMutationId]);
        unset($args['input']['id']);
        foreach ($args['input'] as $fieldName => $value) {
            $record->set($fieldName, $value);
        }
        $record->save();

        return $record;
    }

    /**
     * Resolve the create mutation input.
     *
     * @param mixed                $source  Data source.
     * @param array|null           $args    Arguments.
     * @param array|null           $context Context.
     * @param ResolveInfo          $info    Step information.
     * @param FormManagerInterface $form    Referred form.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function createMutationResolve(mixed $source, ?array $args, ?array $context, ResolveInfo $info, FormManagerInterface $form): mixed
    {
        // Security stage.
        $context = SecurityStage::getDefaultContext($context, $args, null);
        $context = array_merge(
            $context,
            [
                'is_mutation' => true,
                'is_custom' => true,
                'form_manager' => $form,
            ]
        );
        ($this->securityStage)('', 'create', $context);
        // Resolve.
        $clientMutationId = null;
        // Have the input a client mutation id?
        if (isset($args['input']['clientMutationId'])) {
            // Yes, get it to save later.
            $clientMutationId = $args['input']['clientMutationId'];
            unset($args['input']['clientMutationId']);
        }
        // Insert data.
        $id = $form->insert($args['input']);
        // Load record, and set the client mutation id in the record meta data.
        $record = $form->find($id);
        $record->setMeta(['clientMutationId' => $clientMutationId]);

        return $record;
    }

    /**
     * Recursive ArrayObjecto to array transformer.
     *
     * @param \ArrayObject $object Subject.
     *
     * @return array
     */
    protected function arrayObjectToArray(\ArrayObject $object): array
    {
        $resp = (array) $object;
        foreach ($resp as $key => $value) {
            if ($value instanceof \ArrayObject) {
                $resp[$key] = $this->arrayObjectToArray($value);
            } elseif (is_array($value)) {
                foreach ($value as $vKey => $vValue) {
                    if ($vValue instanceof \ArrayObject) {
                        $resp[$key][$vKey] = $this->arrayObjectToArray($vValue);
                    } else {
                        $resp[$key][$vKey] = $vValue;
                    }
                }
            } else {
                $resp[$key] = $value;
            }
        }

        return $resp;
    }

    /**
     * Edges to return.
     *
     * @param int         $allEdges Records total.
     * @param string|null $before   Backward pagination arguments: returning edges before this cursor.
     * @param string|null $after    Forward pagination arguments: returning edges after this cursor.
     * @param int|null    $first    Forward pagination arguments: returning at most first edges.
     * @param int|null    $last     Backward pagination arguments: returning at most last edges.
     *
     * @return EdgesSegment
     *
     * @throws \Exception
     */
    protected function edgesToReturn(int $allEdges, ?string $before, ?string $after, ?int $first, ?int $last): EdgesSegment
    {
        $resp = $this->applyCursorsToEdges($allEdges, $before, $after);
        // Let edges be the result of calling ApplyCursorsToEdges(allEdges, before, after).
        //  If first is set:
        if (!is_null($first)) {
            // If first is less than 0:
            if ($first < 0) {
                // Throw an error.
                throw new \Exception($this->t('First must be grater than or equal to zero.'));
            }
            // If edges has length greater than than first:
            if ($resp->length() > $first) {
                // Slice edges to be of length first by removing edges from the end of edges.
                $resp->end = $resp->start + $first;
            }
        }
        // If last is set:
        if (!is_null($last)) {
            // If last is less than 0:
            if ($last < 0) {
                // Throw an error.
                throw new \Exception($this->t('Last must be grater than or equal to zero.'));
            }
            // If edges has length greater than than last:
            if ($resp->length() > $last) {
                // Slice edges to be of length last by removing edges from the start of edges.
                $resp->start = $resp->end - $last;
            }
        }
        // Return edges.
        return $resp;
    }

    /**
     * Apply cursors to edges.
     *
     * @param int      $allEdges Records total.
     * @param int|null $before   Before edge.
     * @param int|null $after    After edge.
     *
     * @return EdgesSegment
     *
     * @see https://relay.dev/graphql/connections.htm#ApplyCursorsToEdges()
     */
    protected function applyCursorsToEdges(int $allEdges, ?int $before, ?int $after): EdgesSegment
    {
        $resp = new EdgesSegment();
        // Initialize edges to be allEdges.
        $resp->end = $allEdges;
        // If after is set:
        if (!is_null($after)) {
            //  Let afterEdge be the edge in edges whose cursor is equal to the after argument.
            //  If afterEdge exists:
            if ($this->existInEdges($resp, $after)) {
                // Remove all elements of edges before and including afterEdge.
                $resp->start = $after;
            }
        }
        // If before is set:
        if (!is_null($before)) {
            // Let beforeEdge be the edge in edges whose cursor is equal to the before argument.
            // If beforeEdge exists:
            if ($this->existInEdges($resp, $before)) {
                // Remove all elements of edges after and including beforeEdge.
                $resp->end = $before;
            }
        }
        // Return edges.
        return $resp;
    }

    /**
     * Exist $index in the segment?
     *
     * @param EdgesSegment $segment Segment.
     * @param int          $index   Index.
     *
     * @return bool
     */
    protected function existInEdges(EdgesSegment $segment, int $index): bool
    {
        return ($index >= $segment->start && $index <= $segment->end);
    }
}
