<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use ApiPlatform\Core\GraphQl\Resolver\Stage\SecurityStageInterface;
use App\Entity\User;
use App\Security\Core\user\UserProvider;
use App\Traits\GetContainerTrait;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

/**
 * User login token resolver.
 *
 * @experimental
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @see \ApiPlatform\Core\GraphQl\Type\SchemaBuilder
 */
final class UserMutationResolver implements MutationResolverInterface
{
    use GetContainerTrait;

    protected UserPasswordHasher $userPasswordEncoder;
    protected JWTTokenManagerInterface $JWTManager;
    protected UserProvider $userProvider;
    protected SecurityStageInterface $securityStage;

    /**
     * UserMutationResolver constructor.
     */
    public function __construct()
    {
        $this->userPasswordEncoder = $this->getContainerInstance()->get('security.user_password_hasher');
        $this->JWTManager = $this->getContainerInstance()->get('lexik_jwt_authentication.jwt_manager');
        $this->userProvider = $this->getContainerInstance()->get('App\Security\Core\user\UserProvider');
        $this->securityStage = $this->getContainerInstance()->get('App\OpenApi\GraphQl\Stage\SecurityStage');
    }

    /**
     * Execute the resolver.
     *
     * @param User|null $item    The user.
     * @param array     $context Context parameters.
     *
     * @return string
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke($item, $context): string
    {
        ($this->securityStage)('', 'login', $context);
        $token = '';
        try {
            $user = $this->userProvider->loadUserByUsername($context['args']['username']);
        } catch (\Exception $w) {
            return '';
        }

        if ($this->userPasswordEncoder->isPasswordValid($user, $context['args']['password'])) {
            $token = $this->JWTManager->create($user);
        }

        return $token;
    }
}
