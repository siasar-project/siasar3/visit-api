<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

declare(strict_types=1);

namespace App\Security\Core\user;

use App\Tools\Json;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\HttpFoundation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Yivoff\JwtRefreshBundle\Contracts\HasherInterface;
use Yivoff\JwtRefreshBundle\Contracts\RefreshTokenInterface;
use Yivoff\JwtRefreshBundle\Contracts\RefreshTokenProviderInterface;
use function explode;
use function str_contains;
use function time;

/**
 * Refresh token session authenticator.
 */
class RefreshTokenAuthenticator extends AbstractAuthenticator
{
    protected string $parameterName;

    /**
     * @param HasherInterface               $encoder
     * @param AuthenticationSuccessHandler  $successHandler
     * @param RefreshTokenProviderInterface $tokenProvider
     */
    public function __construct(private HasherInterface $encoder, private AuthenticationSuccessHandler $successHandler, private RefreshTokenProviderInterface $tokenProvider)
    {
        $this->parameterName = 'refresh_token';
    }

    /**
     * @param HttpFoundation\Request $request
     *
     * @return PassportInterface
     *
     * @throws \Exception
     */
    public function authenticate(HttpFoundation\Request $request): PassportInterface
    {
        $body = Json::decode($request->getContent());
        $credentials = (string) $body[$this->parameterName];

        if (!str_contains($credentials, ':')) {
            throw new AuthenticationException('Invalid Token Format');
        }

        [$tokenId, $userProvidedVerification] = explode(':', $credentials);

        $token = $this->tokenProvider->getTokenWithIdentifier($tokenId);

        if (!$token instanceof RefreshTokenInterface) {
            throw new AuthenticationException('Token Does Not Exist');
        }

        if ($token->getValidUntil() <= time()) {
            throw new AuthenticationException('Token expired');
        }

        if (!$this->encoder->verify($userProvidedVerification, $token->getVerifier())) {
            throw new AuthenticationException('Token verification failed');
        }

        return new SelfValidatingPassport(new UserBadge($token->getUsername()));
    }

    /**
     * @param HttpFoundation\Request $request
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function supports(HttpFoundation\Request $request): bool
    {
        $body = Json::decode($request->getContent());

        return isset($body[$this->parameterName]);
    }

    /**
     * @param HttpFoundation\Request  $request
     * @param AuthenticationException $exception
     *
     * @return HttpFoundation\Response
     */
    public function onAuthenticationFailure(HttpFoundation\Request $request, AuthenticationException $exception): HttpFoundation\Response
    {
        return new HttpFoundation\JsonResponse(['error' => $exception->getMessage()], HttpFoundation\Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param HttpFoundation\Request $request
     * @param TokenInterface         $token
     * @param string                 $firewallName
     *
     * @return HttpFoundation\Response|null
     */
    public function onAuthenticationSuccess(HttpFoundation\Request $request, TokenInterface $token, string $firewallName): ?HttpFoundation\Response
    {
        return $this->successHandler->onAuthenticationSuccess($request, $token);
    }
}
