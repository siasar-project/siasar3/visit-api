<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Core\user;

use App\Entity\RefreshToken;
use App\Repository\RefreshTokenRepository;
use DateTimeImmutable;
use Yivoff\JwtRefreshBundle\Contracts\PurgableRefreshTokenProviderInterface;
use Yivoff\JwtRefreshBundle\Contracts\RefreshTokenInterface;
use Yivoff\JwtRefreshBundle\Contracts\RefreshTokenProviderInterface;

/**
 * Refresh token provider.
 */
class RefreshTokenProvider implements RefreshTokenProviderInterface, PurgableRefreshTokenProviderInterface
{
    protected RefreshTokenRepository $refreshTokenRepository;

    /**
     * @param RefreshTokenRepository $refreshTokenRepository
     */
    public function __construct(RefreshTokenRepository $refreshTokenRepository)
    {
        $this->refreshTokenRepository = $refreshTokenRepository;
    }

    /**
     * Get token by identifier.
     *
     * @param string $identifier
     *
     * @return RefreshTokenInterface|null
     */
    public function getTokenWithIdentifier(string $identifier): ?RefreshTokenInterface
    {
        $resp = current($this->refreshTokenRepository->findBy(['identifier' => $identifier]));

        if (!$resp) {
            return null;
        }

        return $resp;
    }

    /**
     * Delete token by identifier.
     *
     * @param string $identifier
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteTokenWithIdentifier(string $identifier): void
    {
        $token = $this->getTokenWithIdentifier($identifier);
        if ($token) {
            $this->refreshTokenRepository->removeNow($token);
        }
    }

    /**
     * Persist new token.
     *
     * @param RefreshTokenInterface $refreshToken
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(RefreshTokenInterface $refreshToken): void
    {
        $token = new RefreshToken();
        $token->setIdentifier($refreshToken->getIdentifier());
        $token->setUsername($refreshToken->getUsername());
        $token->setValidUntil($refreshToken->getValidUntil());
        $token->setVerifier($refreshToken->getVerifier());

        $this->refreshTokenRepository->saveNow($token);
    }

    /**
     * Get token by username.
     *
     * @param string $username
     *
     * @return RefreshTokenInterface|null
     */
    public function getTokenForUsername(string $username): ?RefreshTokenInterface
    {
        return current($this->refreshTokenRepository->findBy(['username' => $username]));
    }

    /**
     * Delete old refresh tokens.
     *
     * @return void
     */
    public function purgeExpiredTokens(): void
    {
        $qd = $this->refreshTokenRepository->createQueryBuilder('t');
        $qd->delete();
        $qd->andWhere('t.validUntil < :time');
        $qd->setParameter('time', (new DateTimeImmutable())->getTimestamp());
        $query = $qd->getQuery();
        $query->getResult();
    }
}
