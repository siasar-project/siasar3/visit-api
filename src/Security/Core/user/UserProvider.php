<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Core\user;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Traits\StringTranslationTrait;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Security user provider..
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    use StringTranslationTrait;

    protected UserRepository $userRepository;
    protected TokenStorageInterface $usageTrackingTokenStorage;

    /**
     * UserProvider constructor.
     *
     * @param UserRepository        $userRepository
     * @param TokenStorageInterface $usageTrackingTokenStorage
     */
    public function __construct(UserRepository $userRepository, TokenStorageInterface $usageTrackingTokenStorage)
    {
        $this->userRepository = $userRepository;
        $this->usageTrackingTokenStorage = $usageTrackingTokenStorage;
    }

    /**
     * Load active user by username.
     *
     * @param string $identifier The user name.
     *
     * @return UserInterface
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        return $this->loadUserByUsername($identifier);
    }

    /**
     * Load active user by username.
     *
     * @param string $username The user name.
     *
     * @return UserInterface
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername(string $username): UserInterface
    {
        $user = null;
        try {
            $dbUser = $this->userRepository->findOneByUserName($username);
            if ($dbUser && $dbUser->isActive()) {
                $user = $dbUser;
            }
        } catch (\Exception $e) {
            $user = null;
        }
        if (!$user) {
            throw new UserNotFoundException($this->t('User @user not found', ['@user' => $username]));
        }

        return $user;
    }

    /**
     * Reload user.
     *
     * @param UserInterface $user The user entity.
     *
     * @return UserInterface
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException($this->t('Instances of @class are not supported', ['@class' => get_class($user)]));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Upgrades the encoded password of a user, typically for using a better hash algorithm.
     *
     * @param UserInterface $user               The user entity.
     * @param string        $newEncodedPassword The new encoded password.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        $user->setPassword($newEncodedPassword);
        $this->userRepository->saveNow($user);
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class The user class to test.
     *
     * @return bool
     */
    public function supportsClass(string $class): bool
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return ?UserInterface
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     */
    public function getUser(): ?UserInterface
    {
//        if (!$this->container->has('security.token_storage')) {
//            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
//        }

        if (null === $token = $this->usageTrackingTokenStorage->getToken()) {
            return null;
        }

        if (!\is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }
}
