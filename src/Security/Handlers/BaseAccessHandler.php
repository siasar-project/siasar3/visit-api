<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Handlers;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Service\AccessManager;
use App\Traits\StringTranslationTrait;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Access handler base.
 */
class BaseAccessHandler
{
    use StringTranslationTrait;

    public const ACCESS_ACTION_CREATE = 'create';
    public const ACCESS_ACTION_READ = 'read';
    public const ACCESS_ACTION_UPDATE = 'update';
    public const ACCESS_ACTION_UPDATE_REGION = 'region update';
    public const ACCESS_ACTION_DELETE = 'delete';

    protected AccessManager $accessManager;

    /**
     * BaseAccessHandler constructor.
     *
     * @param AccessManager $accessManager Access manager.
     */
    public function __construct(AccessManager $accessManager)
    {
        $this->accessManager = $accessManager;
    }

    /**
     * Check action user access.
     *
     * Throw exception if not access allowed.
     *
     * @param UserInterface $user    User that do the action.
     * @param string        $action  The action to do, see ACCESS_ACTION_* constants.
     * @param string        $key     The permission key to build the permission ID.
     * @param array         $context Country and administrative division context. ['country' => 'hn', 'division' => '...']
     */
    public function checkAccess(UserInterface $user, string $action, string $key, array $context = []): void
    {
        $permission = sprintf('%s %s', $action, $key);
        $allowed = $this->hasAccess($user, $action, $key, $context);
        if (!$allowed) {
            throw new \Exception(
                $this->t(
                    'The "@action" action require the permission "@permission".',
                    [
                        '@action' => $action,
                        '@permission' => $permission,
                    ]
                )
            );
        }
    }

    /**
     * Check action user access.
     *
     * @param UserInterface $user    User that do the action.
     * @param string        $action  The action to do, see ACCESS_ACTION_* constants.
     * @param string        $key     The permission key to build the permission ID.
     * @param array         $context Country and administrative division context. ['country' => 'hn', 'division' => '...']
     *
     * @return bool
     */
    public function hasAccess(UserInterface $user, string $action, string $key, array $context = []): bool
    {
        $permission = sprintf('%s %s', $action, $key);
        $context = array_merge(
            [
                'country' => null,
                'division' => null,
            ],
            $context
        );

        return $this->accessManager->hasPermission(
            $user,
            $permission,
            (!is_null($context['country']) ? [$context['country']] : []),
            $context['division']
        );
    }

    /**
     * Has user the permission?
     *
     * @param UserInterface           $user       User entity.
     * @param string                  $permission Permission key.
     * @param Country[]               $countries  Country context.
     * @param ?AdministrativeDivision $division   Administrative division context.
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function hasPermission(UserInterface $user, string $permission, array $countries = [], AdministrativeDivision $division = null): bool
    {
        return $this->accessManager->hasPermission($user, $permission, $countries, $division);
    }

    /**
     * Get user division limits.
     *
     * @param UserInterface $user
     *
     * @return array
     */
    public function getUserDivisionsLimits(UserInterface $user): array
    {
        return $this->accessManager->getUserDivisionsLimits($user);
    }

    /**
     * Can this user use this division?
     *
     * @param UserInterface               $user  User querying.
     * @param AdministrativeDivision|null $using Division to test.
     *
     * @return bool
     */
    public function canUserUseDivision(UserInterface $user, AdministrativeDivision $using = null): bool
    {
        if (!$using) {
            return true;
        }

        $limits = $this->getUserDivisionsLimits($user);
        if (count($limits) === 0) {
            return true;
        }

        $usingBranch = $using->getBranch();
        foreach ($limits as $limit) {
            if (str_starts_with($usingBranch, $limit)) {
                return true;
            }
        }

        return false;
    }
}
