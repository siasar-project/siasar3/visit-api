<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Handlers;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Country;
use App\Service\AccessManager;
use App\Service\SessionService;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Inflector\EnglishInflector;
use Symfony\Component\String\UnicodeString;
use Symfony\Component\Uid\Ulid;

/**
 * Inquiry form access handler.
 */
class DoctrineAccessHandler extends BaseAccessHandler
{
    protected SessionService $sessionService;
    protected string $className;
    protected string $class;
    protected Reader $annotationReader;
    protected array $classMetaData;
    private IriConverterInterface $iriConverter;

    /**
     * DoctrineAccessHandler constructor.
     *
     * @param string                $class          Entity class name to control.
     * @param AccessManager         $accessManager
     * @param SessionService        $sessionService
     * @param IriConverterInterface $iriConverter
     */
    public function __construct(string $class, AccessManager $accessManager, SessionService $sessionService, IriConverterInterface $iriConverter)
    {
        parent::__construct($accessManager);

        $this->sessionService = $sessionService;
        $this->class = $class;
        $this->classMetaData = [];
        $this->iriConverter = $iriConverter;
    }

    /**
     * Check action user access.
     *
     * Throw exception if not access allowed.
     *
     * @param UserInterface $user    User that do the action.
     * @param string        $action  The action to do, see ACCESS_ACTION_* constants.
     * @param array         $context Country and administrative division context. ['country' => 'hn', 'division' => '...']
     */
    public function checkEntityAccess(UserInterface $user, string $action, array $context = []): void
    {
        $entityName = $this->getClassShortName();
        $this->checkAccess($user, $action, sprintf('doctrine %s', strtolower($entityName)), $context);
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    public function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@class] This entity require user session.',
                    ['@class' => $this->class]
                )
            );
        }

        return $user;
    }

    /**
     * Update filter criteria with security criteria.
     *
     * @param array $criteria
     *
     * @return array
     */
    public function updateReadCriteria(array $criteria): array
    {
        $user = $this->getCurrentUser();

        // A user with 'all permissions' permission can read all.
        // A user with 'all countries' permission can read all.
        if ($this->hasPermission($user, 'all permissions', [$user->getCountry()]) || $this->hasPermission($user, 'all countries', [$user->getCountry()])) {
            return $criteria;
        }
        // A user only can read her country records.
        $criteria['country'] = $user->getCountry()->getCode();
        // A user only can read her administrative division records.
        // A user with 'all administrative divisions' permission can read all records in her country.
        if (!$this->hasPermission($user, 'all administrative divisions', [$user->getCountry()])) {
            $limits = $this->getUserDivisionsLimits($user);
            if (!empty($limits)) {
                // We need add multiple condition over the branch field.
                // 'branch' is a special field, like 'id'. 'field_region' here is the linked field into the form.
                // Important: 'field_region' reference the contrapart field name.
                $criteria['branch'] = ['alike_start' => ['field_region' => $limits]];
            }
        }

        return $criteria;
    }

    /**
     * Update filter criteria with security criteria.
     *
     * @param Criteria $criteria
     *
     * @return array
     */
    public function updateMatchingCriteria(Criteria $criteria): Criteria
    {
        $user = $this->getCurrentUser();

        // A user with 'all permissions' permission can read all.
        // A user with 'all countries' permission can read all.
        if ($this->hasPermission($user, 'all permissions', [$user->getCountry()]) || $this->hasPermission($user, 'all countries', [$user->getCountry()])) {
            return $criteria;
        }
        // A user only can read her country records.
        if ($this->class !== Country::class) {
            $criteria->andWhere(Criteria::expr()->eq('country', $user->getCountry()));
        } else {
            $criteria->andWhere(Criteria::expr()->eq('code', $user->getCountry()));
        }

        return $criteria;
    }

    /**
     * Get entity data.
     *
     * @param object $instance
     *
     * @return \stdClass
     */
    public function filterItem(object $instance): \stdClass
    {
        $resp = new \stdClass();

        $entityDescription = $this->getClassConverterProperties();
        foreach ($entityDescription as $propertyName => $options) {
            switch ($options["properties"]["processType"]) {
                case 'ManyToOne':
                    $resp->{$propertyName} = null;
                    if ($content = $instance->{$options["properties"]["getMethod"]}()) {
                        $resp->{$propertyName} = $this->iriConverter->getIriFromItem($content);
                    }
                    break;

                case 'OneToMany':
                case 'ManyToMany':
                    if ($options["properties"]["useHowCounter"]) {
                        $resp->{$propertyName} = count($instance->{$options["properties"]["getMethod"]}());
                    } else {
                        $resp->{$propertyName} = [];
                        $children = $instance->{$options["properties"]["getMethod"]}();
                        foreach ($children as $child) {
                            $resp->{$propertyName}[] = $this->filterItem($child);
                        }
                    }
                    break;

                case 'Column':
                    $resp->{$propertyName} = $instance->{$options["properties"]["getMethod"]}();
                    break;
            }
        }

        return $resp;
    }

    /**
     * Get protected class.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Get analyzed metadata about current managed class.
     *
     * todo Generalize to apply over any class and cache it in memory.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function getClassConverterProperties(): array
    {
        $resp = [];
        $metaProperties = $this->getClassMetaProperties();
        foreach ($metaProperties as $name => $metaProperty) {
            $annotPack = [
                'short_classes' => [],
                'classes' => [],
                'properties' => [
                    'processType' => '',
                    'useHowCounter' => false,
                    'fieldName' => $name,
                ],
            ];
            foreach ($metaProperty as $annotation) {
                $annotPack['classes'][] = get_class($annotation);
                $annotPack['short_classes'][] = $this->getClassShortName(get_class($annotation));
                switch (get_class($annotation)) {
                    case 'App\Annotations\UseHowCounter':
                        $annotPack['properties']['useHowCounter'] = true;
                        break;
                    case 'Symfony\Component\Serializer\Annotation\Groups':
                        $annotPack['properties']['processGroups'] = $annotation->getGroups();
                        break;
                    case 'Doctrine\ORM\Mapping\ManyToOne':
                        $annotPack['properties']['processType'] = 'ManyToOne';
                        $annotPack['properties']['type'] = $annotation->targetEntity;
                        $annotPack['properties']['getMethod'] = 'get'.ucfirst($name);
                        break;

                    case 'Doctrine\ORM\Mapping\OneToMany':
                    case 'Doctrine\ORM\Mapping\ManyToMany':
                        $annotPack['properties']['processType'] = $this->getClassShortName(get_class($annotation));
                        $annotPack['properties']['type'] = $annotation->targetEntity;
                        $annotPack['properties']['getMethod'] = 'get'.ucfirst($name);
                        break;

                    case 'Doctrine\ORM\Mapping\Column':
                        $annotPack['properties']['processType'] = 'Column';
                        $annotPack['properties']['type'] = $annotation->type;
                        $annotPack['properties']['getMethod'] = 'get'.ucfirst($name);
                        break 2;
                }
            }
            $resp[$name] = $annotPack;
        }

        return $resp;
    }

    /**
     * Get class short name.
     *
     * @param ?string $class
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    protected function getClassShortName(string $class = null): string
    {
        if (!$class) {
            $class = $this->class;
        }

        return (new \ReflectionClass($class))->getShortName();
    }

    /**
     * Get class ID property.
     *
     * @return string|null
     *
     * @throws \ReflectionException
     */
    protected function getClassMetaId(): ?string
    {
        $meta = $this->getClassMetaData();
        foreach ($meta["properties"] as $name => $property) {
            foreach ($property as $annotation) {
                if ($annotation instanceof Id) {
                    return $name;
                }
            }
        }

        return null;
    }

    /**
     * Get class properties.
     *
     * Include meta ID, and all information about properties.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function getClassMetaProperties(): array
    {
        $meta = $this->getClassMetaData();

        return $meta["properties"];
    }

    /**
     * Get entity metadata.
     *
     * @return ?object[]
     *
     * @throws \ReflectionException
     */
    protected function getClassMetaData(): ?array
    {
        if (count($this->classMetaData) <= 0) {
            // a full list of extractors is shown further below
            $phpDocExtractor = new PhpDocExtractor();
            $reflectionExtractor = new ReflectionExtractor();

            // list of PropertyListExtractorInterface (any iterable)
            $listExtractors = [$reflectionExtractor];

            // list of PropertyTypeExtractorInterface (any iterable)
            $typeExtractors = [$phpDocExtractor, $reflectionExtractor];

            // list of PropertyDescriptionExtractorInterface (any iterable)
            $descriptionExtractors = [$phpDocExtractor];

            // list of PropertyAccessExtractorInterface (any iterable)
            $accessExtractors = [$reflectionExtractor];

            // list of PropertyInitializableExtractorInterface (any iterable)
            $propertyInitializableExtractors = [$reflectionExtractor];

            $propertyInfo = new PropertyInfoExtractor(
                $listExtractors,
                $typeExtractors,
                $descriptionExtractors,
                $accessExtractors,
                $propertyInitializableExtractors
            );

            $reader = new AnnotationReader();
            $this->classMetaData = [
                'class' => $reader->getClassAnnotations(new \ReflectionClass($this->class)),
                'properties' => [],
            ];
            $properties = $propertyInfo->getProperties($this->class);
            foreach ($properties as $property) {
                $this->classMetaData['properties'][$property] = $reader->getPropertyAnnotations(new \ReflectionProperty($this->class, $property));
            }
        }

        return $this->classMetaData;
    }
}
