<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Handlers;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Household form access handler.
 */
class HouseholdAccessHandler extends BaseAccessHandler
{

    /**
     * Check action user access.
     *
     * Throw exception if not access allowed.
     *
     * @param UserInterface $user    User that do the action.
     * @param string        $action  The action to do, see ACCESS_ACTION_* constants.
     * @param array         $context Country and administrative division context. ['country' => 'hn', 'division' => '...']
     */
    public function checkFormAccess(UserInterface $user, string $action, array $context = []): void
    {
        $this->checkAccess($user, $action, 'household record', $context);
    }
}
