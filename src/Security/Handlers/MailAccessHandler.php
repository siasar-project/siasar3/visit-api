<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Handlers;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Inquiry form haccess handler.
 */
class MailAccessHandler extends BaseAccessHandler
{
//    public const ACCESS_ACTION_CREATE = 'create';

    /**
     * Check user access.
     *
     * Throw exception if not access allowed.
     *
     * @param UserInterface $user    User that do the action.
     * @param array         $context Country and administrative division context. ['country' => 'hn', 'division' => '...']
     *
     * @throws \Exception
     */
    public function checkUse(UserInterface $user, array $context = []): void
    {
        $permission = 'can use mailing';
        $action = 'use mailing';
        $allowed = $this->hasPermission($user, $permission, $context);
        if (!$allowed) {
            throw new \Exception(
                $this->t(
                    'The "@action" action require the permission "@permission".',
                    [
                        '@action' => $action,
                        '@permission' => $permission,
                    ]
                )
            );
        }
    }
    /**
     * Check user access.
     *
     * Throw exception if not access allowed.
     *
     * @param UserInterface $user    User that do the action.
     * @param array         $context Country and administrative division context. ['country' => 'hn', 'division' => '...']
     *
     * @throws \Exception
     */
    public function checkSendGlobal(UserInterface $user, array $context = []): void
    {
        $permission = 'can send global mail';
        $action = 'send global mail';
        $allowed = $this->hasPermission($user, $permission, $context);
        if (!$allowed) {
            throw new \Exception(
                $this->t(
                    'The "@action" action require the permission "@permission".',
                    [
                        '@action' => $action,
                        '@permission' => $permission,
                    ]
                )
            );
        }
    }
}
