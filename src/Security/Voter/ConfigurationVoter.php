<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Voter;

use App\Entity\Configuration;
use App\Entity\User;
use App\Service\AccessManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Configuration access voter.
 */
class ConfigurationVoter extends VoterBase
{
    const SUBJECT_ARRAY_KEY = 'configuration_key';

    /**
     * Voters constructor.
     *
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        parent::__construct($accessManager);
    }

    /**
     * Configurations allowed by access attribute, action.
     *
     * Only a few configurations may be public exposed.
     */
    protected const CONFIGURATION_BY_ATTRIBUTE = [
        self::READ => [
            'system.units.length',
            'system.units.flow',
            'system.units.volume',
            'system.units.concentration',
            'system.role.*',
            'system.permissions',
            'system.languages',
        ],
    ];

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed  $subject   The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $this->supportsAttributes())) {
            return false;
        }

        // If isn't array or configuration, return false.
        if (!(is_array($subject) && isset($subject[self::SUBJECT_ARRAY_KEY])) && !$subject instanceof Configuration) {
            return false;
        }

        // If is configuration, but it's not in the public list, return false.
        if ($subject instanceof Configuration && !in_array($subject->getId(), self::CONFIGURATION_BY_ATTRIBUTE)) {
            return false;
        }

        // If is array, but it's not in the public list, return false.
        if (is_array($subject) && isset($subject[self::SUBJECT_ARRAY_KEY]) && !in_array($subject[self::SUBJECT_ARRAY_KEY], self::CONFIGURATION_BY_ATTRIBUTE[$attribute])) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($subject instanceof Configuration) {
            $key = $subject->getId();
        } else {
            // It's array.
            $key = $subject[self::SUBJECT_ARRAY_KEY];
        }

        switch ($attribute) {
            case static::READ:
                return $this->canView($key, $user);
            case static::CREATE:
            case static::UPDATE:
            case static::DELETE:
                return false;
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * Can the user view the configuration?
     *
     * @param string $key
     * @param User   $user
     *
     * @return bool
     */
    protected function canView(string $key, User $user): bool
    {
        $allowed = $this->accessManager->hasPermission($user, 'read public configuration');

        return $allowed;
    }

    /**
     * Can the user edit the configuration?
     *
     * @param Configuration $configuration
     * @param User          $user
     *
     * @return bool
     */
    private function canEdit(Configuration $configuration, User $user): bool
    {
        return false;
    }
}
