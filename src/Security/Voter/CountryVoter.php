<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Voter;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Service\AccessManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Security logic.
 */
class CountryVoter extends VoterBase
{
    /**
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        parent::__construct($accessManager);

        $this->supportedType = Country::class;
    }

    /**
     * Verify access.
     *
     * @param string         $attribute Action to do.
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $permission = sprintf('%s doctrine country', strtolower($attribute));
        $user = $token->getUser();
        // If the user is anonymous, do not grant access.
        if (!$user instanceof UserInterface) {
            return false;
        }
        /** @var Country $subject */

        return $this->accessManager->hasPermission($user, $permission, [$subject]);
    }
}
