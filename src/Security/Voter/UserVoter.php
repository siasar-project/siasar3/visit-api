<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Voter;

use App\Entity\User;
use App\Service\AccessManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Security logic.
 */
class UserVoter extends VoterBase
{
    public const READ_OWN = 'read own';
    public const UPDATE_OWN = 'update own';

    /**
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        parent::__construct($accessManager);

        $this->supportedType = User::class;
    }

    /**
     * Get valid attributes in this voter.
     *
     * @return string[]
     */
    protected function supportsAttributes(): array
    {
        return parent::supportsAttributes() + [static::UPDATE_OWN, static::READ_OWN];
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $attribute = strtolower($attribute);
        /** @var User $user */
        $user = $token->getUser();
        if (self::UPDATE === $attribute || self::READ === $attribute) {
            // Self user action must be limited to current user.
            // Later we'll check if the user have the permission.
            /** @var User $subject */
            if ($user->getId() === $subject->getId()) {
                switch ($attribute) {
                    case self::UPDATE:
                        $attribute = self::UPDATE_OWN;
                        break;
                    case self::READ:
                        $attribute = self::READ_OWN;
                        break;
                }
            }
        }

        return parent::voteOnAttribute($attribute, $subject, $token);
    }
}
