<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Security\Voter;

use App\Service\AccessManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Access voter tool.
 */
class VoterBase extends Voter
{
    public const CREATE = 'create';
    public const READ   = 'read';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    protected AccessManager $accessManager;
    protected ?string $supportedType;

    /**
     * Voters constructor.
     *
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        $this->accessManager = $accessManager;
        $this->supportedType = null;
    }

    /**
     * Get valid attributes in this voter.
     *
     * @return string[]
     */
    protected function supportsAttributes(): array
    {
        return [static::CREATE, static::READ, static::UPDATE, static::DELETE];
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed  $subject   The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        if (!$this->supportedType) {
            return false;
        }

        // see https://symfony.com/doc/current/security/voters.html
        $attribute = strtolower($attribute);

        return in_array($attribute, $this->supportsAttributes()) && $subject instanceof $this->supportedType;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if (!$this->supportedType) {
            throw new \LogicException('This code should not be reached!');
        }

        $attribute = strtolower($attribute);
        $className = (new \ReflectionClass($this->supportedType))->getShortName();
        $permission = sprintf('%s doctrine %s', $attribute, strtolower($className));
        $user = $token->getUser();
        // If the user is anonymous, do not grant access.
        if (!$user instanceof UserInterface) {
            return false;
        }

        return $this->accessManager->hasPermission($user, $permission, [$subject->getCountry()]);
    }
}
