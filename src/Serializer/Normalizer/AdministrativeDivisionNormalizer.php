<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Serializer\Normalizer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Repository\AdministrativeDivisionRepository;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Administrative division normalizer.
 */
class AdministrativeDivisionNormalizer implements DenormalizerInterface, NormalizerInterface, CacheableSupportsMethodInterface
{

    protected DenormalizerInterface|ObjectNormalizer $normalizer;
    protected IriConverterInterface $iriConverter;
    protected AdministrativeDivisionRepository $divisionRepository;
    protected array $processed;

    /**
     * @param ObjectNormalizer                 $normalizer
     * @param IriConverterInterface            $iriConverter
     * @param AdministrativeDivisionRepository $divisionRepository
     */
    public function __construct(ObjectNormalizer $normalizer, IriConverterInterface $iriConverter, AdministrativeDivisionRepository $divisionRepository)
    {
        if (!$normalizer instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException('The normalizer must implement the DenormalizerInterface');
        }

        $this->normalizer = $normalizer;
        $this->iriConverter = $iriConverter;
        $this->divisionRepository = $divisionRepository;

        $this->processed = [];
    }

    /**
     * @param mixed  $data
     * @param string $class
     * @param null   $format
     * @param array  $context
     *
     * @return mixed|object
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $object = $this->normalizer->denormalize($data, $class, $format, $context);

        return $object;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        $internal = $this->normalizer->supportsDenormalization($data, $type, $format);

        return ($data instanceof AdministrativeDivision && $internal);
    }

    /**
     * @param AdministrativeDivision $object
     * @param ?string                $format
     * @param array                  $context
     *
     * @return array
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        // Break circle recursions, we must remember the IDs just processed or
        // that will be processed. While we process a item maybe that we
        // require load children or parent that can or can't to be loaded before.
        if (isset($this->processed[$object->getId()])) {
            return $this->processed[$object->getId()];
        }

        $this->processed[$object->getId()] = [];
        // Fix circular reference.
        $context[AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT] = 2;

        $data = $this->normalizer->normalize($object, $format, $context);

        // Here: add, edit, or delete some data.
        //$data['administrativeDivisions'] = $this->divisionRepository->count(['parent' => $object->getId()]);
        unset($data['administrativeDivisions']);
        $data['country'] = $this->iriConverter->getIriFromItem($object->getCountry());
        $data['parent'] = null;
        if ($object->getParent()) {
            $data['parent'] = $this->iriConverter->getIriFromItem($object->getParent());
        }
        $data['code'] = $object->getCode();

        $this->processed[$object->getId()] = $data;

        return $data;
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        $internal = $this->normalizer->supportsNormalization($data, $format);
        $resp = ($data instanceof AdministrativeDivision && ($internal || 'custom' === $format));

        if ($resp) {
            $this->processed = [];
        }

        return $resp;
    }

    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
