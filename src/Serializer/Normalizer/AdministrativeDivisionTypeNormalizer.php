<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Serializer\Normalizer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivisionType;
use App\Repository\AdministrativeDivisionTypeRepository;
use App\Traits\StringTranslationTrait;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Administrative division normalizer.
 */
class AdministrativeDivisionTypeNormalizer implements DenormalizerInterface, NormalizerInterface, CacheableSupportsMethodInterface
{
    use StringTranslationTrait;

    protected DenormalizerInterface|ObjectNormalizer $normalizer;
    protected IriConverterInterface $iriConverter;
    protected AdministrativeDivisionTypeRepository $divisionRepository;
    protected array $processed;

    /**
     * @param ObjectNormalizer                     $normalizer
     * @param IriConverterInterface                $iriConverter
     * @param AdministrativeDivisionTypeRepository $divisionRepository
     */
    public function __construct(ObjectNormalizer $normalizer, IriConverterInterface $iriConverter, AdministrativeDivisionTypeRepository $divisionRepository)
    {
        if (!$normalizer instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException('The normalizer must implement the DenormalizerInterface');
        }

        $this->normalizer = $normalizer;
        $this->iriConverter = $iriConverter;
        $this->divisionRepository = $divisionRepository;

        $this->processed = [];
    }

    /**
     * @param mixed  $data
     * @param string $class
     * @param null   $format
     * @param array  $context
     *
     * @return mixed|object
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $object = $this->normalizer->denormalize($data, $class, $format, $context);

        return $object;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        $internal = $this->normalizer->supportsDenormalization($data, $type, $format);

        return ($data instanceof AdministrativeDivisionType && $internal);
    }

    /**
     * @param AdministrativeDivisionType $object
     * @param ?string                    $format
     * @param array                      $context
     *
     * @return array
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        // Break circle recursions, we must remember the IDs just processed or
        // that will be processed. While we process a item maybe that we
        // require load children or parent that can or can't to be loaded before.
        if (isset($this->processed[$object->getId()])) {
            return $this->processed[$object->getId()];
        }

        $this->processed[$object->getId()] = [];
        // Fix circular reference.
        $context[AbstractNormalizer::CIRCULAR_REFERENCE_LIMIT] = 2;

        $data = $this->normalizer->normalize($object, $format, $context);

        // Here: add, edit, or delete some data.
        $data['country'] = $this->iriConverter->getIriFromItem($object->getCountry());
        $data['real_name'] = $data['name'];
        $data['name'] = self::t(
            $data['name'],
            [],
            [
                'context' => 'Administrative division type',
            ]
        );

        $this->processed[$object->getId()] = $data;

        return $data;
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        $internal = $this->normalizer->supportsNormalization($data, $format);
        $resp = ($data instanceof AdministrativeDivisionType && ($internal || 'custom' === $format));

        if ($resp) {
            $this->processed = [];
        }

        return $resp;
    }

    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
