<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Serializer\Normalizer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Annotations\IsCountryParametric;
use App\Annotations\SecuredRepository;
use App\Entity\Country;
use App\Forms\FieldTypes\Types\InquiringStatusFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Repository\CountryRepository;
use App\Tools\Json;
use App\Traits\GetContainerTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Expression;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Proxy\Proxy;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Country normalizer.
 */
class CountryNormalizer implements DenormalizerInterface, NormalizerInterface, CacheableSupportsMethodInterface
{
    use GetContainerTrait;

    protected DenormalizerInterface|ObjectNormalizer $normalizer;
    protected IriConverterInterface $iriConverter;
    protected CountryRepository $countryRepository;
    protected EntityManagerInterface $entityManager;
    protected FormFactory $formFactory;
    protected Reader $annotationReader;
    protected array $processed;

    /**
     * @param FormFactory            $formFactory
     * @param ObjectNormalizer       $normalizer
     * @param IriConverterInterface  $iriConverter
     * @param CountryRepository      $countryRepository
     * @param EntityManagerInterface $entityManager
     * @param Reader                 $annotationReader
     */
    public function __construct(FormFactory $formFactory, ObjectNormalizer $normalizer, IriConverterInterface $iriConverter, CountryRepository $countryRepository, EntityManagerInterface $entityManager, Reader $annotationReader)
    {
        if (!$normalizer instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException('The normalizer must implement the DenormalizerInterface');
        }

        $this->normalizer = $normalizer;
        $this->iriConverter = $iriConverter;
        $this->countryRepository = $countryRepository;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;

        $this->processed = [];
        $this->annotationReader = $annotationReader;
    }

    /**
     * @param mixed  $data
     * @param string $class
     * @param null   $format
     * @param array  $context
     *
     * @return mixed|object
     *
     * @throws ExceptionInterface
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $object = $this->normalizer->denormalize($data, $class, $format, $context);

        return $object;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        $internal = $this->normalizer->supportsDenormalization($data, $type, $format);

        return ($data instanceof Country && $internal);
    }

    /**
     * @param Country $object
     * @param ?string $format
     * @param array   $context
     *
     * @return array
     *
     * @throws ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        // Break circle recursions, we must remember the IDs just processed or
        // that will be processed. While we process a item maybe that we
        // require load children or parent that can or can't to be loaded before.
        if (isset($this->processed[$object->getId()])) {
            return $this->processed[$object->getId()];
        }

        $this->processed[$object->getId()] = [];
        // Proxy was deprecated since Doctrine 2.7.
//        if ($object instanceof Proxy) {
//            // Get real Country instance instead the proxy.
//            $this->entityManager->clear(Country::class);
//            $repo = $this->entityManager->getRepository(Country::class);
//            $object = $repo->find($object->getCode());
//        }
        $data = $this->normalizer->normalize($object, $format, []);

        // Filter using securized repository.
        $meta = $this->entityManager->getMetadataFactory()->getMetadataFor(Country::class);
        $relations = $meta->getAssociationMappings();
        foreach ($relations as $propertyName => $relation) {
            if (2 !== $relation["type"]) {
                if ($this->isParametric($relation["targetEntity"])) {
                    $repo = $this->getSecuredRepository($relation["targetEntity"]);
                    try {
                        // Trick to speed up.
                        // The secured repository throw error if the user don't have permission to read the entity.
                        $repo->findBy([$relation["mappedBy"] => $object->getId()], null, 1);
                    } catch (\Exception $e) {
                        // They don't have permission to read, then we can clear the output.
                        $data[$propertyName] = [];
                    }
                }
            }
        }

        // Here: add, edit, or delete some data.
        // Add form level data.
        $countryLevels = $object->getFormLevel();
        $inquiries = $this->formFactory->findByType(InquiryFormManager::class);
        $levels = [];
        /** @var FormManagerInterface $inquiry */
        foreach ($inquiries as $inquiry) {
            $fLev = isset($countryLevels[$inquiry->getId()]['level']) ? $countryLevels[$inquiry->getId()]['level'] : 1;
            $fSdg = isset($countryLevels[$inquiry->getId()]['sdg']) ? $countryLevels[$inquiry->getId()]['sdg'] : false;
            $levels[$inquiry->getId()] = [
                'level' => $fLev,
                'label' => Country::FORM_LEVEL_LABELS[$fLev],
                'sdg' => $fSdg,
            ];
        }
        $data['formLevel'] = $levels;

        // Clean proxy attributes.
        $clean = [];
        foreach ($data as $field => $value) {
            if (!str_starts_with($field, '__')) {
                $clean[$field] = $value;
            }
        }
        $data = $clean;

        $this->processed[$object->getId()] = $data;

        return $data;
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        $internal = $this->normalizer->supportsNormalization($data, $format);
        $resp = ($data instanceof Country && ($internal || 'custom' === $format));

        if ($resp) {
            $this->processed = [];
        }

        return $resp;
    }

    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }

    /**
     * Is this class a parametric?
     *
     * @param string $class
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    protected function isParametric(string $class): bool
    {
        $annotations = $this->annotationReader->getClassAnnotations(new \ReflectionClass($class));
        foreach ($annotations as $note) {
            if ($note instanceof IsCountryParametric) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the secured repository to the selected entity.
     *
     * @param string $class
     *
     * @return ServiceEntityRepository|null
     *
     * @throws \ReflectionException
     */
    protected function getSecuredRepository(string $class): ServiceEntityRepository|null
    {
        $annotations = $this->annotationReader->getClassAnnotations(new \ReflectionClass($class));
        foreach ($annotations as $note) {
            if ($note instanceof SecuredRepository) {
                $srClass = $note->class;

                return self::getContainerInstance()->get($srClass);
            }
        }

        return null;
    }
}
