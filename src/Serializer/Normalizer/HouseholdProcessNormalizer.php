<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Serializer\Normalizer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Annotations\IsCountryParametric;
use App\Annotations\SecuredRepository;
use App\Entity\File;
use App\Entity\HouseholdProcess;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Repository\HouseholdProcessRepository;
use App\Traits\GetContainerTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Uid\Ulid;

/**
 * HouseholdProcess normalizer.
 */
class HouseholdProcessNormalizer implements DenormalizerInterface, NormalizerInterface, CacheableSupportsMethodInterface
{
    use GetContainerTrait;

    protected DenormalizerInterface|ObjectNormalizer $normalizer;
    protected IriConverterInterface $iriConverter;
    protected HouseholdProcessRepository $householdProcessRepository;
    protected EntityManagerInterface $entityManager;
    protected FormFactory $formFactory;
    protected Reader $annotationReader;
    protected array $processed;

    /**
     * @param FormFactory                $formFactory
     * @param ObjectNormalizer           $normalizer
     * @param IriConverterInterface      $iriConverter
     * @param HouseholdProcessRepository $householdProcessRepository
     * @param EntityManagerInterface     $entityManager
     * @param Reader                     $annotationReader
     */
    public function __construct(FormFactory $formFactory, ObjectNormalizer $normalizer, IriConverterInterface $iriConverter, HouseholdProcessRepository $householdProcessRepository, EntityManagerInterface $entityManager, Reader $annotationReader)
    {
        if (!$normalizer instanceof DenormalizerInterface) {
            throw new \InvalidArgumentException('The normalizer must implement the DenormalizerInterface');
        }

        $this->normalizer = $normalizer;
        $this->iriConverter = $iriConverter;
        $this->householdProcessRepository = $householdProcessRepository;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;

        $this->processed = [];
        $this->annotationReader = $annotationReader;
    }

    /**
     * @param mixed  $data
     * @param string $class
     * @param null   $format
     * @param array  $context
     *
     * @return mixed|object
     *
     * @throws ExceptionInterface
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $object = $this->normalizer->denormalize($data, $class, $format, $context);

        return $object;
    }

    /**
     * @param mixed  $data
     * @param string $type
     * @param null   $format
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        $internal = $this->normalizer->supportsDenormalization($data, $type, $format);

        return ($data instanceof HouseholdProcess && $internal);
    }

    /**
     * @param HouseholdProcess $object
     * @param ?string          $format
     * @param array            $context
     *
     * @return array
     *
     * @throws ExceptionInterface
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        // Break circle recursions, we must remember the IDs just processed or
        // that will be processed. While we process a item maybe that we
        // require load children or parent that can or can't to be loaded before.
        if (isset($this->processed[$object->getId()])) {
            return $this->processed[$object->getId()];
        }

        $this->processed[$object->getId()] = [];
        $data = $this->normalizer->normalize($object, $format, []);

        $data['country'] = $this->iriConverter->getIriFromItem($object->getCountry());

        $data["administrativeDivision"]['meta'] = $object->getAdministrativeDivision()->formatExtraJson();

        $householdForm = $this->formFactory->find('form.community.household');
        $inThisProcess = $householdForm->countBy(
            [
                'field_household_process' => $object->getId(),
            ]
        );

        $data['totalHousehold'] = $inThisProcess;
        $data['draftHousehold'] = $inThisProcess - $data['finishedHousehold'];
        $data['communityHouseholds'] = 0;

        // Search a valid image from community form.
        $form = $this->formFactory->find('form.community');
        $communityRecord = $form->find($object->getCommunityReference());
        $data['image'] = '';
        if ($communityRecord) {
            $data['communityHouseholds'] = $communityRecord->{'field_total_households'};
            /** @var File[] $images */
            $images = $communityRecord->{'field_community_photos'};
            if (count($images) > 0 && isset($images[0])) {
                $extras = $images[0]->formatExtraJson();
                if (isset($extras)) {
                    $data['image'] = $extras['url'];
                }
            }
            // Get Point
            $pointForm = $this->formFactory->find('form.point');
            $point = current($pointForm->findByCommunity($communityRecord->getId()));
            if ($point) {
                $data['point'] = $point->getId();
            }
        }

        $this->processed[$object->getId()] = $data;

        return $data;
    }

    /**
     * @param mixed $data
     * @param null  $format
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        $internal = $this->normalizer->supportsNormalization($data, $format);
        $resp = ($data instanceof HouseholdProcess && ($internal || 'custom' === $format));

        if ($resp) {
            $this->processed = [];
        }

        return $resp;
    }

    /**
     * @return bool
     */
    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
