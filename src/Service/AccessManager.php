<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use App\Entity\AdministrativeDivision;
use App\Entity\Configuration;
use App\Entity\Country;
use App\Entity\User;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\ConfigurationRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Role and permissions access manager.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AccessManager
{
    protected ConfigurationRepository $configuration;
    protected AdapterInterface $cache;
    protected Security $security;

    /**
     * Prepare AccessManager service.
     *
     * @param Security  $security
     * @param ?Registry $entityManager Entity manager.
     */
    public function __construct(Security $security, Registry $entityManager = null)
    {
        $this->configuration = $entityManager->getRepository(Configuration::class);
        $this->security = $security;
    }

    /**
     * Core security layer.
     *
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * Get system permissions by group.
     *
     * @param string $group Filter by permission group.
     *
     * @return array
     */
    public function getSystemPermissions(string $group = ''): array
    {
        $cachedPermissions = $this->getCache()->getItem('system.permissions.'.$group);
        if (!$cachedPermissions->isHit()) {
            $permissionsConfig = $this->configuration->find('system.permissions');
            if (!$permissionsConfig) {
                return [];
            }

            $resp = $permissionsConfig->getValue();
            // Complete structure.
            foreach ($resp as $key => &$value) {
                $trustedValue = array_merge(
                    [
                        'label' => $key,
                        'description' => '',
                        'security_warning' => false,
                        'group' => 'ungrouped',
                    ],
                    $value
                );
                $resp[$key] = $trustedValue;
            }
            unset($value);

            // Filter if required.
            if (!empty($group)) {
                $aux = [];
                foreach ($resp as $key => $value) {
                    if ($group === $value['group']) {
                        $aux[$key] = $value;
                    }
                }
                $resp = $aux;
            }
            $cachedPermissions->set($resp);
            $this->getCache()->save($cachedPermissions);
        } else {
            $resp = $cachedPermissions->get();
        }

        return $resp;
    }

    /**
     * Is this permission defined in system?
     *
     * @param string $permission
     *
     * @return bool
     */
    public function isSystemPermission(string $permission): bool
    {
        $permissions = $this->getSystemPermissions();

        return in_array($permission, array_keys($permissions));
    }

    /**
     * Get all system permissions groups.
     *
     * @return array
     */
    public function getSystemPermissionsGroups(): array
    {
        $cachedPermissions = $this->getCache()->getItem('system.permissions.groups');
        if (!$cachedPermissions->isHit()) {
            $groups = [];
            $permissions = $this->getSystemPermissions();
            foreach ($permissions as $permission) {
                $groups[$permission['group']] = true;
            }
            $resp = array_keys($groups);
            $cachedPermissions->set($resp);
            $this->getCache()->save($cachedPermissions);
        } else {
            $resp = $cachedPermissions->get();
        }

        return $resp;
    }

    /**
     * Get system roles.
     *
     * @return array
     */
    public function getSystemRoles(): array
    {
        $cfgs = $this->configuration->findByKeyStart('system.role.');

        $resp = [];
        foreach ($cfgs as $config) {
            $resp += $config->getValue();
        }
        // Clean configuration requirements key.
        unset($resp['requires']);

        return $resp;
    }

    /**
     * Get permissions by role.
     *
     * @param array $roles Role list.
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getPermissionsByRoles(array $roles): array
    {
        $cachedPermissions = $this->getCache()->getItem('system.permissions_by_roles.'.implode('_', $roles));
        if (!$cachedPermissions->isHit()) {
            $permissions = [];
            $systemRoles = $this->getSystemRoles();
            // Find roles permissions.
            foreach ($roles as $role) {
                if (isset($systemRoles[$role])) {
                    if (isset($systemRoles[$role]['permissions'])) {
                        $permissions = array_merge($permissions, $systemRoles[$role]['permissions']);
                    }
                } else {
                    throw new \Exception(sprintf('Role "%s" not defined.', $role));
                }
            }
            // Validate system permissions.
            $systemPermissions = array_keys($this->getSystemPermissions());
            foreach ($permissions as $permission) {
                if (!in_array($permission, $systemPermissions)) {
                    throw new \Exception(
                        sprintf(
                            'Permission "%s" not defined in roles: %s.',
                            $permission,
                            implode(', ', $roles)
                        )
                    );
                }
            }

            $resp = $permissions;
            $cachedPermissions->set($resp);
            $this->getCache()->save($cachedPermissions);
        } else {
            $resp = $cachedPermissions->get();
        }

        return $resp;
    }

    /**
     * Get plain administrative divisions tree.
     *
     * @param AdministrativeDivision $division Division root.
     *
     * @return AdministrativeDivision[]
     */
    public function getAdministrativeDivisionsPlain(AdministrativeDivision $division): array
    {
        $resp = [$division];

        $children = $division->getAdministrativeDivisions()->toArray();
        foreach ($children as $child) {
            $resp = array_unique(array_merge($resp, $this->getAdministrativeDivisionsPlain($child)), SORT_REGULAR);
        }

        return $resp;
    }

    /**
     * Has user the permission?
     *
     * @param UserInterface           $user       User entity.
     * @param string                  $permission Permission key.
     * @param Country[]               $countries  Country context.
     * @param ?AdministrativeDivision $division   Administrative division context.
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function hasPermission(UserInterface $user, string $permission, array $countries = [], AdministrativeDivision $division = null): bool
    {
        if (!$user instanceof User) {
            // The instance is not a valid one.
            return false;
        }
        $permissions = $this->getPermissionsByRoles($user->getRoles());

        // Have the user 'all permissions' permission?
        if (in_array('all permissions', $permissions)) {
            return true;
        }

        // Can the user work in any country?
        if (!in_array('all countries', $permissions)) {
            // Not, verify the context country.
            if (!$user->getCountry()) {
                // The user haven't country context.
                return false;
            }
            if (!empty($countries)) {
                $validContext = false;
                foreach ($countries as $country) {
                    if ($user->getCountry()->getCode() === $country->getCode()) {
                        $validContext = true;
                        break;
                    }
                }
                if (!$validContext) {
                    // The user isn't in this country context.
                    return false;
                }

                // This context require administrative division or can the user work in any administrative division?
                if (!is_null($division) && !in_array('all administrative divisions', $permissions)) {
                    // Get user divisions and sub-divisions.
                    $userLimits = $this->getUserDivisionsLimits($user);
                    // Verify user divisions vs context.
                    $validContext = true;
                    if (count($userLimits) > 0) {
                        $validContext = false;
                        foreach ($userLimits as $limit) {
                            if (str_starts_with($division->getBranch(), $limit)) {
                                $validContext = true;
                                break;
                            }
                        }
                    }
                    if (!$validContext) {
                        // The user isn't in this division context.
                        return false;
                    }
                }
            }
        }

        return in_array($permission, $permissions);
    }

    /**
     * Get user division limits.
     *
     * @param UserInterface $user
     *
     * @return array
     */
    public function getUserDivisionsLimits(UserInterface $user): array
    {
        $resp = [];
        /** @var User $user */
        $userDivisions = $user->getAdministrativeDivisions();
        foreach ($userDivisions as $userDivision) {
            $resp[] = $userDivision->getBranch();
        }

        return $resp;
    }

    /**
     * Get cache adapter.
     *
     * @return AdapterInterface
     */
    protected function getCache(): AdapterInterface
    {
        if (!isset($this->cache)) {
            $this->cache = new ArrayAdapter();
        }

        return $this->cache;
    }
}
