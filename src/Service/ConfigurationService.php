<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationRead;
use App\Entity\Configuration\ConfigurationWrite;
use App\Repository\ConfigurationRepository;
use App\Update\SystemUpdateMethods;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Configuration tool service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationService
{
    protected ?ManagerRegistry $registry;

    /**
     * Constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Get configuration repository.
     *
     * @return ConfigurationRepository
     */
    public function getRepository(): ConfigurationRepository
    {
        return $this->registry->getRepository(Configuration::class);
    }

    /**
     * Validate unit key.
     *
     * @param string $unitType The unit configuration key.
     * @param string $unit     The unit to validate.
     *
     * @return bool
     */
    public function existSystemUnit(string $unitType, string $unit): bool
    {
        $units = $this->getRepository()->find($unitType);

        return isset($units->getValue()[$unit]);
    }

    /**
     * Is this server in maintenance mode?
     *
     * @return bool
     */
    public function inMaintenanceMode(): bool
    {
        $config = $this->getRepository()->find('mode.maintenance');

        return !!$config;
    }

    /**
     * Enable or disable maintenance mode.
     *
     * @param $value
     *
     * @return $this
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setMaintenanceMode($value = false): self
    {
        /** @var ConfigurationWrite $config */
        $config = $this->getRepository()->findEditable('mode.maintenance');

        if (!!$config) {
            if (!$value) {
                $config->delete();
            }
        } else {
            if ($value) {
                $config = $this->getRepository()->create(
                    'mode.maintenance',
                    [
                        'local_only' => true,
                    ]
                );
                $config->saveNow();
            }
        }

        return $this;
    }

    /**
     * Get editable update status.
     *
     * @return ConfigurationWrite
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getUpdateStatus(): ConfigurationWrite
    {
        /** @var ConfigurationWrite $config */
        $config = $this->getRepository()->findEditable('system.update');
        if (!$config) {
            $config = $this->getRepository()->create(
                'system.update',
                [
                    'local_only' => true,
                    'next' => 0,
                ]
            );
            $config->saveNow();
        }

        return $config;
    }

    /**
     * Set the next update version.
     *
     * @param int $next
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUpdateVersion(int $next): void
    {
        if ($next < 0) {
            $next = 0;
        }
        $config = $this->getUpdateStatus();
        $values = $config->getValue();
        $values['next'] = $next;
        $config->setValue($values);
        $config->saveNow();
    }

    /**
     * Execute all updates after the last executed.
     *
     * @param SymfonyStyle|null $io
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    public function runUpdate(SymfonyStyle $io = null): void
    {
        $config = $this->getUpdateStatus();
        $nextUpdate = $config->getValue()['next'];
        // $io->info(sprintf('Next update: %s', $nextUpdate));
        $methods = get_class_methods('App\Update\SystemUpdateMethods');
        $updates = [];
        $rows = [];
        foreach ($methods as $method) {
            if ('__construct' !== $method && 'SystemUpdateMethods' !== $method) {
                $version = str_replace('update', '', $method);
                if ($version < $nextUpdate) {
                    continue;
                }
                if (is_numeric($version)) {
                    $updates[$version] = $method;
                    // Get description.
                    $ref = new ReflectionMethod('App\Update\SystemUpdateMethods', $method);
                    $doc = $ref->getDocComment();
                    preg_match_all('#@(.*?)\n#s', $doc, $annotations);
                    foreach ($annotations[0] as $note) {
                        $doc = str_replace(trim($note), '', $doc);
                    }
                    $lines = explode("\n", $doc);
                    $doc = [];
                    foreach ($lines as $line) {
                        $line = trim($line);
                        if ('/**' !== $line && '*' !== $line && '*/' !== $line) {
                            $doc[] = trim(substr($line, 1));
                        }
                    }
                    $rows[$version] = [$version, implode(" ", $doc)];
                }
            }
        }
        if (count($rows) > 0) {
            ksort($rows);
            $io->table(['Update', 'Description'], array_values($rows));
            $continue = $io->confirm('Apply updates?');
            if ($continue) {
                // Apply updates.
                ksort($updates);
                $updater = new SystemUpdateMethods($this->registry->getConnection());
                $next = $config->getValue()['next'];
                foreach ($updates as $version => $update) {
                    $io->note(sprintf('Updating %s: %s', $version, $rows[$version][1]));
                    $updater->{$update}($io);
                    $next = $version + 1;
                }
                $this->setUpdateVersion($next);
            }
        }
    }
}
