<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use App\Entity\Country;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Country geometry tools.
 *
 * @see https://mariadb.com/kb/en/st-contains/
 */
class CountryGeometryService
{

    /**
     * Country geometry tools.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->_em = $em;
    }

    /**
     * Is the point inside the country?
     *
     * @param Country $country   Country where to search.
     * @param float   $latitude
     * @param float   $longitude
     *
     * @return bool
     */
    public function validatePoint(Country $country, $latitude, $longitude): bool
    {
        $connection = $this->_em->getConnection();
        $query = $connection->createQueryBuilder()
            ->select('*')
            ->from('country_geometry', 'c')
            ->where(
                'ST_INTERSECTS(c.polygon, '.
                sprintf('ST_BUFFER(ST_GEOMFROMTEXT(\'Point(%s %s)\'), 0.0455)', $longitude, $latitude).
                ')'
            )
            ->andWhere('c.id = \''.$country->getId().'\'');
        $inside = $query->execute()->rowCount();

        return $inside === 1;
    }

    /**
     * Exist country geometry
     *
     * @param string $id       Country code.
     * @param bool   $toInsert
     *
     * @return bool
     *
     * @throws Exception
     */
    public function exist(string $id, bool $toInsert = false): bool
    {
        /** @var Connection $connection */
        $connection = $this->_em->getConnection();
        $query = $connection->createQueryBuilder()
            ->select('*')
            ->from('country_geometry', 'c')
            ->where('c.id = :country');
        if (!$toInsert) {
            $query->andWhere('c.polygon IS NOT NULL');
        }
        $query->setParameter(':country', $id);
        $cnt = $query->execute()->rowCount();

        return $cnt !== 0;
    }

    /**
     * Update polygon property.
     *
     * @param Country $country Country entity
     * @param mixed   $geojson Geometry
     *
     * @return void
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateGeometry(Country $country, &$geojson)
    {
        /** @var Connection $connection */
        $connection = $this->_em->getConnection();
        $query = $connection->createQueryBuilder()
            ->update('country_geometry', 'c')
            ->set('polygon', 'ST_GeomFromGeoJSON(:geometry)')
            ->where('c.id = :country');
        $query->setParameter(':geometry', $geojson);
        $query->setParameter(':country', $country->getCode());
        $query->execute();
    }
}
