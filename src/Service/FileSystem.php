<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use App\Traits\GetContainerTrait;
use Symfony\Component\Console\Helper\Helper;

/**
 * OS file system tools service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FileSystem
{
    use GetContainerTrait;

    protected static array $mimeTypes = [];

    /**
     * +-------------------------------------------------------------------------+
     * | Revive Adserver                                                         |
     * | http://www.revive-adserver.com                                          |
     * |                                                                         |
     * | Copyright: See the COPYRIGHT.txt file.                                  |
     * | License: GPLv2 or later, see the LICENSE.txt file.                      |
     * +-------------------------------------------------------------------------+
     * Attempts to remove the file indicated by the $sFilename path from the
     * filesystem. If the $filename indicates non-empty directory the function
     * will remove it along with all its content.
     *
     * @param string $sFilename
     *
     * @return bool True if the operation is successful, Exception if there
     * was a failure.
     */
    public static function systemFileRemove(string $sFilename): bool
    {
        if (file_exists($sFilename)) {
            if (is_dir($sFilename)) {
                $directory = opendir($sFilename);
                if (false === $directory) {
                    throw new \Exception(sprintf("Can't open the directory: '%s'.", $sFilename));
                }
                while (($sChild = readdir($directory)) !== false) {
                    if ('.' === $sChild or '..' === $sChild or '.gitkeep' === $sChild) {
                        continue;
                    }
                    $result = @self::systemFileRemove($sFilename.'/'.$sChild);
                }
                closedir($directory);
                $result = rmdir($sFilename);
                if (false === $result) {
                    throw new \Exception(sprintf("Can't delete the directory: '%s'.", $sFilename));
                }
            } else {
                if (!unlink($sFilename)) {
                    throw new \Exception(sprintf("Can't remove the file: '%s'.", $sFilename));
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Get all files and folders from a folder.
     *
     * @param string $sFilename The folder to explore.
     *
     * @return array Response with ['files' => [], 'folders' => []].
     *
     * @throws \Exception
     */
    public static function systemGetFilesAndFolders(string $sFilename): array
    {
        $resp = ['files' => [], 'folders' => []];
        if (file_exists($sFilename)) {
            if (is_dir($sFilename)) {
                $resp['folders'][] = $sFilename;
                $directory = opendir($sFilename);
                if (false === $directory) {
                    throw new \Exception(sprintf("Can't open the directory: '%s'.", $sFilename));
                }
                while (($sChild = readdir($directory)) !== false) {
                    if ('.' === $sChild or '..' === $sChild) {
                        continue;
                    }
                    $result = @self::systemGetFilesAndFolders($sFilename.'/'.$sChild);
                    foreach ($result['files'] as $file) {
                        $resp['files'][] = $file;
                    }
                    foreach ($result['folders'] as $file) {
                        $resp['folders'][] = $file;
                    }
                }
                closedir($directory);
            } else {
                $resp['files'][] = $sFilename;
            }
        }

        return $resp;
    }

    /**
     * Get public files' path.
     *
     * @return string
     */
    public function getPublicFolder(): string
    {
        return $this->getKernelInstance()->getPublicDir();
    }

    /**
     * Get cache files' path.
     *
     * @return string
     */
    public function getCacheFolder(): string
    {
        return $this->getKernelInstance()->getCacheDir();
    }

    /**
     * Get and format the file size by path.
     *
     * @param string $path File or folder path.
     *
     * @return string
     */
    public static function formatFileSize(string $path): string
    {
        if (is_file($path)) {
            $size = filesize($path) ?: 0;
        } else {
            $size = 0;
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS | \RecursiveDirectoryIterator::FOLLOW_SYMLINKS)) as $file) {
                if ($file->isReadable()) {
                    $size += $file->getSize();
                }
            }
        }

        return Helper::formatMemory($size);
    }

    /**
     * Pharinix File Manager Copyright (C) 2016 Pedro Pelaez <aaaaa976@gmail.com>
     * Sources https://github.com/PSF1/pharinix_mod_file_manager
     *
     * GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007
     */

    /**
     * Download and prepare mime types list.
     *
     * @see http://php.net/manual/es/function.mime-content-type.php#107798
     */
    public function mimeTypesUpdate(): void
    {
        $url = 'http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types';
        @file_put_contents(self::getPublicFolder().'/mimetypes.txt', @file_get_contents($url));
        self::$mimeTypes = [];
    }

    /**
     * Get usable mime types list keyed by file extension.
     *
     * @return array
     */
    public function getMimeTypes(): array
    {
        if (count(self::$mimeTypes) === 0) {
            if (is_file($this->getPublicFolder().'/mimetypes.bin')) {
                self::$mimeTypes = unserialize(@file_get_contents($this->getPublicFolder().'/mimetypes.bin'));
            } else {
                self::$mimeTypes = [];
                if (!is_file($this->getPublicFolder().'/mimetypes.txt')) {
                    $this->mimeTypesUpdate();
                }
                $lines = @explode("\n", @file_get_contents($this->getPublicFolder().'/mimetypes.txt'));
                foreach ($lines as $line) {
                    if (isset($line[0]) && $line[0] !== '#' && preg_match_all('#([^\s]+)#', $line, $out) && isset($out[1]) && ($length = count($out[1])) > 1) {
                        for ($i = 1; $i < $length; $i++) {
                            self::$mimeTypes[$out[1][$i]] = $out[1][0];
                        }
                    }
                }
                @file_put_contents($this->getPublicFolder().'/mimetypes.bin', serialize(self::$mimeTypes));
            }
        }

        return self::$mimeTypes;
    }

    /**
     * Get mime type by file extension.
     *
     * @param string $name File name
     *
     * @return string Mime type
     */
    public function getMimeByExt(string $name): string
    {
        $types = $this->getMimeTypes();
        $fileExtension = $this->getFileExtension($name);
        if (isset($types[$fileExtension])) {
            return $types[$fileExtension];
        }

        return "application/octet-stream";
    }

    /**
     * Get file extension.
     *
     * @param string $name
     *
     * @return string
     */
    public function getFileExtension(string $name): string
    {
        return strtolower(pathinfo($name, PATHINFO_EXTENSION));
    }

    /**
     * Get file folder.
     *
     * @param string $name
     *
     * @return string
     */
    public function getFileDirname(string $name): string
    {
        return strtolower(pathinfo($name, PATHINFO_DIRNAME));
    }

    /**
     * Get file name without extension.
     *
     * @param string $name
     *
     * @return string
     */
    public function getFileName(string $name): string
    {
        return strtolower(pathinfo($name, PATHINFO_FILENAME));
    }

    /**
     * Get file name with extension.
     *
     * @param string $name
     *
     * @return string
     */
    public function getFileNameWithExtension(string $name): string
    {
        return pathinfo($name, PATHINFO_BASENAME);
    }
}
