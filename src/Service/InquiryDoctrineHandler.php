<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Kevin Hirczy <https://www.linkedin.com/in/kevin-hirczy-7a9377106/>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use App\Entity\Country;
use App\Entity\InquiryFormLog;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;

/**
 * Sends Inquiry forms logs to database.
 *
 * @author Kevin Hirczy <https://www.linkedin.com/in/kevin-hirczy-7a9377106/>
 *
 * @see    https://nehalist.io/logging-events-to-database-in-symfony/
 */
class InquiryDoctrineHandler extends AbstractProcessingHandler
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * MonologDBHandler constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * Called when writing to our database
     *
     * @param array $record
     */
    protected function write(array $record): void
    {
        $country = null;

        $user = SessionService::getUser();
        $userCountry = $user->getCountry();
        $country = null;
        $formId = null;
        $recordId = null;

        if (!is_null($userCountry)) {
            $country = $this->em->getRepository(Country::class)->find($userCountry->getCode());
        }

        if (isset($record['context']['form_id'])) {
            $formId = $record['context']['form_id'];
        }

        if (isset($record['context']['record_id'])) {
            $recordId = $record['context']['record_id'];
        }

        $logEntry = new InquiryFormLog();
        $logEntry->setMessage($record['message']);
        $logEntry->setLevel($record['level']);
        $logEntry->setLevelName($record['level_name']);
        $logEntry->setExtra($record['extra']);
        $logEntry->setContext($record['context']);
        $logEntry->setCountry($country);
        $logEntry->setFormId($formId);
        $logEntry->setRecordId($recordId);

        $this->em->getRepository(InquiryFormLog::class)->saveNow($logEntry);
    }
}
