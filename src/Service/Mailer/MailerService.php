<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service\Mailer;

use App\Templating\TwigTemplate;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

/**
 * Mailer service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class MailerService
{
    // Mail subjects by twig template.
    protected const TEMPLATE_SUBJECT_MAP = [
        TwigTemplate::USER_REGISTER => 'Welcome!',
    ];

    protected MailerInterface $mailer;
    protected Environment $engine;
    protected LoggerInterface $logger;
    protected string $mailerDefaultSender;

    /**
     * MailerService constructor.
     *
     * @param MailerInterface $mailer              Mailer service.
     * @param Environment     $engine              Twig engine.
     * @param LoggerInterface $logger              Logging service.
     * @param string          $mailerDefaultSender Default sender email.
     */
    public function __construct(MailerInterface $mailer, Environment $engine, LoggerInterface $logger, string $mailerDefaultSender)
    {
        $this->mailer = $mailer;
        $this->engine = $engine;
        $this->logger = $logger;
        $this->mailerDefaultSender = $mailerDefaultSender;
    }

    /**
     * Send email using a twig template.
     *
     * @param string $receiver Destination email.
     * @param string $template Twig template to use.
     * @param array  $payload  Twig context data.
     *
     * @return void
     */
    public function send(string $receiver, string $template, array $payload): void
    {
        $email = (new TemplatedEmail())
            ->from($this->mailerDefaultSender)
            ->to($receiver)
            ->subject(self::TEMPLATE_SUBJECT_MAP[$template])
            ->htmlTemplate($template)
            ->context($payload);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error(\sprintf('Error sending email: %s', $e->getMessage()));
        }
    }
}
