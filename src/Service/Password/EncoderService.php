<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service\Password;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Password encode service.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class EncoderService
{
    protected const MINIMUM_LENGTH = 6;
    protected PasswordHasherFactoryInterface $userPasswordEncoder;

    /**
     * EncoderService constructor.
     *
     * @param PasswordHasherFactoryInterface $hasherFactory User password encoder.
     */
    public function __construct(PasswordHasherFactoryInterface $hasherFactory)
    {
        $this->userPasswordEncoder = $hasherFactory;
    }

    /**
     * Generate encoded password.
     *
     * @param UserInterface $user     The user.
     * @param string        $password The password.
     *
     * @return string
     */
    public function generateEncodedPassword(UserInterface $user, string $password): string
    {
        if (self::MINIMUM_LENGTH > \strlen($password)) {
            throw new BadRequestHttpException(sprintf('Password must be at least %s characters', self::MINIMUM_LENGTH));
        }

        return $this->userPasswordEncoder->getPasswordHasher($user)->hash($password);
    }

    /**
     * Validate a user password.
     *
     * @param UserInterface $user     User.
     * @param string        $hash     Hashed password.
     * @param string        $password User password.
     *
     * @return bool
     */
    public function isValidPassword(UserInterface $user, string $hash, string $password): bool
    {
        return $this->userPasswordEncoder->getPasswordHasher($user)->verify($hash, $password);
    }
}
