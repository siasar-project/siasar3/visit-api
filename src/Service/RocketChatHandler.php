<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use GuzzleHttp\ClientInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Sends notifications through RocketChat Webhooks
 *
 * @author Dmitriy Kuts <me@exileed.com>
 * @author Pedro Pelaez <pedro@front.id>
 *
 * @see    https://docs.rocket.chat/guides/administrator-guides/integrations
 */
class RocketChatHandler extends AbstractProcessingHandler
{
    /**
     * Colors for a given log level.
     *
     * @var array
     */
    protected $levelColors = [
        Logger::DEBUG => '#9E9E9E',
        Logger::INFO => '#4CAF50',
        Logger::NOTICE => '#607D8B',
        Logger::WARNING => '#FFEB3B',
        Logger::ERROR => '#F44336',
        Logger::CRITICAL => '#F44336',
        Logger::ALERT => '#F44336',
        Logger::EMERGENCY => '#F44336',
    ];

    /**
     * GuzzleClient
     *
     * @var Client
     */
    protected $client;

    /**
     * RocketChat Webhook URLs
     *
     * @var array
     */
    protected $webHookUrls;

    /**
     * Disable guzzle exceptions
     * @var bool
     */
    protected $disableException;

    /**
     * RocketChat channel (ID)
     *
     * @var string
     */
    protected $channel;

    /**
     * The emoji name to use (or null)
     *
     * @var string|null
     */
    protected $iconEmoji;

    protected TokenStorageInterface $tokenStorage;

    /**
     * RocketChatHandler constructor.
     *
     * @param TokenStorageInterface $storage
     * @param array                 $webHookUrls
     * @param string                $channel
     * @param bool                  $disableException
     * @param string                $iconEmoji
     * @param int                   $level            The minimum logging level at which this handler will be triggered
     * @param bool                  $bubble           Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct(TokenStorageInterface $storage, array $webHookUrls, string $channel, bool $disableException, string $iconEmoji, $level = Logger::DEBUG, $bubble = true)
    {
        $this->webHookUrls      = $webHookUrls;
        $this->channel          = $channel;
        $this->disableException = $disableException;
        $this->client           = new Client();
        $this->iconEmoji        = $iconEmoji;
        parent::__construct($level, $bubble);
        $this->tokenStorage = $storage;
    }

    /**
     * RocketChat Webhook URLs
     *
     * @return array
     */
    public function getWebHookUrls(): array
    {
        return $this->webHookUrls;
    }

    /**
     * @inheritDoc
     */
    protected function write(array $record): void
    {
        $user = $this->getCurrentUser();
        $author = 'ANONYMOUS';
        if ($user) {
            $countryName = '';
            $countryCode = '';
            if ($user->getCountry()) {
                $countryName = $user->getCountry()->getName();
                $countryCode = $user->getCountry()->getId();
            }
            $author = sprintf(
                ':alien: %s: [%s] - :flag_%s: %s [%s]',
                $user->getUsername(),
                implode(', ', $user->getRoles()),
                $countryCode,
                $countryName,
                $countryCode
            );
        }
        $level = $record['level'] ?? $this->level;
        $hostname = $_ENV['ROCKETCHAT_THIS_INSTANCE_ID'];
        if ('@hostname' === $hostname) {
            $hostname = gethostname();
        }
        $text = "\n:computer: ".$hostname." / *{$_ENV['APP_ENV']}*\n";
        $text .= "```\n".$author."\n```\n";
        $content = [
            'text' => $this->iconEmoji.$record['message'] ?? '',
            'channel' => $this->channel,
            'attachments' => [
                [
                    'title' => '',
                    'text' => $text.$record["formatted"] ?? '',
                    'color' => $this->levelColors[$level],
                ],
            ],
        ];

        foreach ($this->webHookUrls as $url) {
            if (empty($url)) {
                continue;
            }
            try {
                $this->client->post(
                    $url,
                    [
                        'json' => $content,
                    ]
                );
            } catch (GuzzleException $e) {
                if ($this->disableException === false) {
                    throw $e;
                }
            }
        }
    }

    /**
     * Get current user by Token Storage.
     *
     * @return UserInterface|null
     */
    protected function getCurrentUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface) {
            /** @var UserInterface $user */
            $user = $token->getUser();

            return $user;
        }

        return null;
    }
}
