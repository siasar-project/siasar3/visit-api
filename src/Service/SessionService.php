<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Tools\HttpHeadersHelper;
use App\Traits\GetContainerTrait;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Current session service.
 *
 * This service use the Authorization header to get the current user.
 * JWT authorization must lock bad tokens to not allow use it.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class SessionService
{
    use GetContainerTrait;

    protected static array $decodedToken;
    protected static ?User $user = null;

    protected ?UserRepository $userRepository;
    protected JWTManager $JWTManager;
    protected EntityManagerInterface $entityManager;
    protected SessionInterface $session;

    /**
     * SessionService constructor.
     *
     * @param EntityManagerInterface   $entityManager Entity manager.
     * @param JWTTokenManagerInterface $JWTManager    JWT manager.
     * @param SessionInterface         $session       Session manager.
     */
    public function __construct(EntityManagerInterface $entityManager, JWTTokenManagerInterface $JWTManager, SessionInterface $session)
    {
        $this->userRepository = $entityManager->getRepository(User::class);
        $this->entityManager = $entityManager;
        $this->JWTManager = $JWTManager;

        $this->resetSession();
        $this->session = $session;
    }

    /**
     * Refresh user session data.
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function resetSession(): void
    {
        static::$decodedToken = [];
        static::$user = null;
        $headers = HttpHeadersHelper::getHeaders();
        // Test authorization compatibility, used to change user session too.
        if (isset($_ENV['TESTS_HEADER_HTTP_AUTHORIZATION'])) {
            $headers = ['Authorization' => $_ENV['TESTS_HEADER_HTTP_AUTHORIZATION']];
        } elseif (isset($headers['HEADER']['HTTP_AUTHORIZATION'])) {
            $headers = ['Authorization' => $headers['HEADER']['HTTP_AUTHORIZATION']];
        }
        // Capture request header authorization.
        if (isset($headers["Authorization"])) {
            $token = str_replace('Bearer ', '', $headers["Authorization"]);
            $token = str_replace('bearer ', '', $token);
            try {
                // Decode and validate the token.
                static::$decodedToken = $this->JWTManager->parse($token);
            } catch (\Exception $e) {
                // Do nothing.
            }

            if (isset(static::$decodedToken["username"])) {
                // Use the username to get the session user to avoid tests issues.
                static::$user = $this->userRepository->findOneByUserName(static::$decodedToken["username"]);
            } else {
                static::$decodedToken = [];
                static::$user = null;
            }
        }
    }

    /**
     * Get current user.
     *
     * @return User|null
     */
    public static function getUser(): ?User
    {
        return static::$user;
    }

    /**
     * Get current user ID.
     *
     * @return string|null
     */
    public static function getUserId(): ?string
    {
        if (static::$user) {
            return static::$user->getId();
        }

        return null;
    }

    /**
     * Get user session token.
     *
     * @param string $userName
     *
     * @return string
     */
    public function generateFakeUserToken(string $userName): string
    {
        $userRepository = $this->entityManager->getRepository(User::class);
        $user = $userRepository->findOneByUserName($userName);
        if (!$user) {
            return '';
        }

        return $this->JWTManager->create($user);
    }

    /**
     * Start user session.
     *
     * @param string $username
     */
    public function loginFakeUser(string $username): void
    {
        $token = $this->generateFakeUserToken($username);
        $_ENV['TESTS_HEADER_HTTP_AUTHORIZATION'] = 'Bearer '.$token;
        // Update session status.
        $this->session->clear();
        $this->session->invalidate();

        $this->resetSession();
    }
}
