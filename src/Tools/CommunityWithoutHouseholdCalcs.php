<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use ApiPlatform\Core\Bridge\Symfony\Routing\IriConverter;
use App\Entity\HouseholdProcess;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Plugins\AbstractHouseholdCalcBase;
use App\Plugins\AbstractWithoutHouseholdCalcBase;
use App\Plugins\WithoutHouseholdCalcDiscovery;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Common\Annotations\AnnotationReader;
use Monolog\Logger;
use Symfony\Component\Uid\Ulid;

/**
 * Execute calcs after household process closed.
 */
class CommunityWithoutHouseholdCalcs
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected WithoutHouseholdCalcDiscovery $householdCalcsDiscovery;
    protected $entityManager;
    protected null|SessionService $sessionManager;
    protected null|IriConverter $iriConverter;
    protected Logger|null $inquiryFormLogger;
    protected AnnotationReader|null $annotationReader;
    protected FormFactory $formFactory;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->householdCalcsDiscovery = static::getContainerInstance()->get('without_householdcalc_discovery');
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->iriConverter = static::getContainerInstance()->get('api_platform.iri_converter');
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->annotationReader = static::getContainerInstance()->get('annotations.reader');
        $this->formFactory = static::getContainerInstance()->get('form_factory');
    }

    /**
     * Initialize all affected community fields.
     *
     * @param FormRecord $inquiry Community record to update.
     *
     * @return void
     */
    public function executeInits(FormRecord $inquiry): void
    {
        $calcs = $this->householdCalcsDiscovery->getHouseholdCalcs();
        foreach ($calcs as $calc) {
            $class = $calc['class'];
            $annotation = $calc['annotation'];
            /** @var AbstractHouseholdCalcBase $instance */
            $instance = new $class(
                $this->annotationReader,
                $this->sessionManager,
                $this->inquiryFormLogger,
                $inquiry
            );
            $instance->init();
        }
    }

    /**
     * Execute community household calcs.
     *
     * This method save the inquiry at the end.
     *
     * @param FormRecord $inquiry Community record to update.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function executeCalcs(FormRecord $inquiry): void
    {
        $calcs = $this->householdCalcsDiscovery->getHouseholdCalcs();
        $withHouseholds = $inquiry->{'field_have_households'};
        if ($withHouseholds) {
            // This calcs only apply to without households.
            return;
        }
        foreach ($calcs as $calc) {
            $class = $calc['class'];
            $annotation = $calc['annotation'];
            /** @var AbstractWithoutHouseholdCalcBase $instance */
            $instance = new $class(
                $this->annotationReader,
                $this->sessionManager,
                $this->inquiryFormLogger,
                $inquiry
            );
            $instance->execute();
        }
        // Update without launch events or validations.
        $inquiry->getForm()->update($inquiry, true);
    }
}
