<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Forms\FormReferenceEntityInterface;

/**
 * Context log normalizer.
 */
class ContextLogNormalizer
{

    /**
     * Normalize context array to be serialized.
     *
     * @param array $context
     *
     * @return array
     */
    public static function normalize(array $context): array
    {
        $newContext = [];
        foreach ($context as $key => $value) {
            if (is_array($value)) {
                $newContext[$key] = static::normalize($value);
            } elseif ($value instanceof FormReferenceEntityInterface) {
                $newContext[$key] = $value->serializeToLog();
            } else {
                $newContext[$key] = $value;
            }
        }

        return $newContext;
    }
}
