<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

/**
 * Utility class for cryptographically-secure string handling routines.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 *
 * @ingroup utility
 */
class Crypt
{

    /**
     * Returns a string of highly randomized bytes (over the full 8-bit range).
     *
     * This function is better than simply calling mt_rand() or any other built-in
     * PHP function because it can return a long string of bytes (compared to < 4
     * bytes normally from mt_rand()) and uses the best available pseudo-random
     * source.
     *
     * In PHP 7 and up, this uses the built-in PHP function random_bytes().
     * In older PHP versions, this uses the random_bytes() function provided by
     * the random_compat library, or the fallback hash-based generator from Drupal
     * 7.x.
     *
     * @param int $count
     *   The number of characters (bytes) to return in the string.
     *
     * @return string
     *   A randomly generated string.
     *
     * @see https://www.drupal.org/node/3057191
     */
    public static function randomBytes(int $count): string
    {
        return random_bytes($count);
    }

    /**
     * Calculates a base-64 encoded, URL-safe sha-256 hmac.
     *
     * @param mixed $data
     *   Scalar value to be validated with the hmac.
     * @param mixed $key
     *   A secret key, this can be any scalar value.
     *
     * @return string
     *   A base-64 encoded sha-256 hmac, with + replaced with -, / with _ and
     *   any = padding characters removed.
     */
    public static function hmacBase64(mixed $data, mixed $key): string
    {
        // $data and $key being strings here is necessary to avoid empty string
        // results of the hash function if they are not scalar values. As this
        // function is used in security-critical contexts like token validation it
        // is important that it never returns an empty string.
        if (!is_scalar($data) || !is_scalar($key)) {
            throw new \InvalidArgumentException('Both parameters passed to \App\Tools\Crypt::hmacBase64 must be scalar values.');
        }

        $hmac = base64_encode(hash_hmac('sha256', $data, $key, true));
        // Modify the hmac so it's safe to use in URLs.
        return str_replace(['+', '/', '='], ['-', '_', ''], $hmac);
    }

    /**
     * Calculates a base-64 encoded, URL-safe sha-256 hash.
     *
     * @param string $data
     *   String to be hashed.
     *
     * @return string
     *   A base-64 encoded sha-256 hash, with + replaced with -, / with _ and
     *   any = padding characters removed.
     */
    public static function hashBase64(string $data): string
    {
        $hash = base64_encode(hash('sha256', $data, true));
        // Modify the hash so it's safe to use in URLs.
        return str_replace(['+', '/', '='], ['-', '_', ''], $hash);
    }

    /**
     * Compares strings in constant time.
     *
     * Use PHP's built-in hash_equals() function instead.
     *
     * @param string $knownString
     *   The expected string.
     * @param string $userString
     *   The user supplied string to check.
     *
     * @return bool
     *   Returns true when the two strings are equal, false otherwise.
     *
     * @see https://www.drupal.org/node/3054488
     */
    public static function hashEquals(string $knownString, string $userString): bool
    {
        return hash_equals($knownString, $userString);
    }

    /**
     * Returns a URL-safe, base64 encoded string of highly randomized bytes.
     *
     * @param int $count
     *   The number of random bytes to fetch and base64 encode.
     *
     * @return string
     *   A base-64 encoded string, with + replaced with -, / with _ and any =
     *   padding characters removed.
     *
     * @see \App\Tools\Crypt::randomBytes()
     */
    public static function randomBytesBase64(int $count = 32): string
    {
        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode(random_bytes($count)));
    }
}
