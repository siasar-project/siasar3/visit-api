<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Entity\Currency;
use App\Forms\FormReferenceEntityInterface;
use App\Traits\FormReferenceStartingTrait;

/**
 * Auxiliary currency reference form field type.
 */
class CurrencyAmount implements FormReferenceEntityInterface
{
    use FormReferenceStartingTrait;

    /**
     * @var float
     */
    public $amount;
    /** @var ?Currency  */
    public ?Currency $currency;

    /**
     * @param float    $amount
     * @param Currency $currency
     */
    public function __construct(float $amount = 0, Currency $currency = null)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @inheritDoc
     */
    public function getId(): ?string
    {
        return ($this->currency) ? $this->currency->getId() : '';
    }

    /**
     * @inheritDoc
     */
    public function serializeToLog(): array
    {
        return $this->toArray();
    }

    /**
     * Get reference how array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'value' => ($this->currency) ? $this->currency->getId() : '',
            'class' => Currency::class,
            'amount' => $this->amount,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function formatExtraJson(bool $forceString = false): array
    {
        return ($this->currency) ? $this->currency->formatExtraJson($forceString) : [];
    }
}
