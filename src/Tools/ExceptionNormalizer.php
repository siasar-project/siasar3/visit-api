<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Exception normalizer.
 */
class ExceptionNormalizer
{
    use GetContainerTrait;
    use StringTranslationTrait;

    /**
     * Transform exception to array.
     *
     * @param \Exception $e
     * @param bool       $safe todo Filter message in production environment.
     *
     * @return array
     */
    public static function normalize(\Exception $e, bool $safe = false): array
    {
        /** @var ParameterBagInterface $instance */
        $systemSettings = self::getContainerInstance()->get('parameter_bag');

        $body = ['message' => $e->getMessage()];

        if ('prod' !== $systemSettings->get('kernel.environment')) {
            $body['code'] = $e->getCode();
            $body['file'] = $e->getFile();
            $body['line'] = $e->getLine();
            $body['trace'] = $e->getTrace();
        }

        return $body;
    }
}
