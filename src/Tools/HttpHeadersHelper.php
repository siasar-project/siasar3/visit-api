<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

/**
 * HTTP Headers helper.
 *
 * apache_request_headers() not always is enabled, this class wrap it
 * to have an universal headers getter.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class HttpHeadersHelper
{

    /**
     * Fetches all HTTP request headers from the current request
     *
     * @link https://php.net/manual/en/function.apache-request-headers.php
     *
     * @return array|false
     *   An associative array of all the HTTP headers in the current request, or <b>FALSE</b> on failure.
     *
     * @see https://www.php.net/manual/en/function.apache-request-headers.php#70810
     */
    public static function getHeaders(): array|false
    {
        if (!function_exists('apache_request_headers')) {
            $arh = [];
            $rxHttp = '/\AHTTP_/';
            foreach ($_SERVER as $key => $val) {
                if (preg_match($rxHttp, $key)) {
                    $arhKey = preg_replace($rxHttp, '', $key);
                    // do some nasty string manipulations to restore the original letter case
                    // this should work in most cases
                    $rxMatches = explode('_', $arhKey);
                    if (count($rxMatches) > 0 and strlen($arhKey) > 2) {
                        foreach ($rxMatches as $akKey => $akVal) {
                            $rxMatches[$akKey] = ucfirst($akVal);
                        }
                        $arhKey = implode('-', $rxMatches);
                    }
                    $arh[$arhKey] = $val;
                }
            }

            return ($arh);
        }

        return apache_request_headers();
    }
}
