<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Service\FileSystem;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Symfony\Component\Finder\Finder;

/**
 * Optimize image size.
 */
class ImageOptimizer
{
    protected Imagine $imagine;
    protected FileSystem $fileSystem;

    /**
     * ImageOptimizer constructor.
     *
     * @param FileSystem $fileSystem
     */
    public function __construct(FileSystem $fileSystem)
    {
        $this->imagine = new Imagine();
        $this->fileSystem = $fileSystem;
    }

    /**
     * Calculate new image size.
     *
     * @param string $filename
     * @param int    $width
     * @param int    $height
     *
     * @return float[]|int[]
     */
    public function calcNewSize(string $filename, int $width, int $height): array
    {
        list($iwidth, $iheight) = getimagesize($this->fileSystem->getPublicFolder().'/'.$filename);
        $ratio = $iwidth / $iheight;
        if ($width / $height > $ratio) {
            $width = $height * $ratio;
        } else {
            $height = $width / $ratio;
        }

        if ($height > $iheight || $width > $iwidth) {
            return [$iwidth, $iheight];
        }

        return [$width, $height];
    }

    /**
     * Resize image in a new file and return the new file path.
     *
     * @param string $filename
     * @param int    $width
     * @param int    $height
     *
     * @return string
     */
    public function resize(string $filename, int $width, int $height): string
    {
        list($width, $height) = $this->calcNewSize($filename, $width, $height);

        $photo = $this->imagine->open($this->fileSystem->getPublicFolder().'/'.$filename);

        $filaNmeParts = $this->parseFileName($filename);

        $finalPath = $this->getTempPath().'/'.$filaNmeParts->name.'_'.$width.'x'.$height.'.'.$filaNmeParts->ext.'.'.$filaNmeParts->realExt;
        if (!is_file($finalPath)) {
            $photo->resize(new Box($width, $height))->save($finalPath);
        }

        return $finalPath;
    }

    /**
     * Remove all optimized versions.
     *
     * @param string $filename
     *
     * @return void
     */
    public function clearOptimizations(string $filename): void
    {
        $filaNmeParts = $this->parseFileName($filename);

        $pattern = $filaNmeParts->name.'_*x*.'.$filaNmeParts->ext.'.'.$filaNmeParts->realExt;
        $finder = new Finder();
        $finder->name($pattern);
        foreach ($finder->in($this->getTempPath()) as $file) {
            $relPath = $file->getPath().'/'.$file->getRelativePathname();
            @unlink($relPath);
        }
    }

    /**
     * Get file name parts.
     *
     * @param string $filename
     *
     * @return \stdClass
     */
    protected function parseFileName(string $filename): \stdClass
    {
        $resp = new \stdClass();
        $resp->ext = $this->fileSystem->getFileExtension($filename);
        $resp->name = $this->fileSystem->getFileName($filename);
        $resp->realExt = $this->fileSystem->getFileExtension($resp->name);
        $resp->name = $this->fileSystem->getFileName($resp->name);

        return $resp;
    }

    /**
     * Get optimized images path.
     *
     * @return string
     */
    protected function getTempPath(): string
    {
        $tmpPath = $this->fileSystem->getCacheFolder().'/images';
        @mkdir($tmpPath, 0775, true);

        return $tmpPath;
    }
}
