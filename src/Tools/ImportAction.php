<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

/**
 * Configuration import action.
 *
 * @category Tools
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ImportAction
{
    public const ACTION_UPDATE = 'update';
    public const ACTION_NOTHING = 'nothing';
    public const ACTION_CREATE = 'create';
    public const ACTION_DELETE = 'delete';

    protected string $action;
    protected string $id;
    protected mixed $object;
    protected array $requires;
    protected int $weight;

    /**
     * New action to do.
     *
     * @param string $action   Action type.
     * @param string $id       Configuration ID.
     * @param mixed  $object   Configuration content.
     * @param int    $weight   Action weight.
     * @param array  $requires Configuration dependencies.
     */
    public function __construct(string $action, string $id, mixed $object, int $weight = 0, array $requires = [])
    {
        $this->action = $action;
        $this->id = $id;
        $this->object = $object;
        $this->weight = $weight;
        $this->requires = $requires;
    }

    /**
     * Get action.
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Set action.
     *
     * @param string $action
     *
     * @return ImportAction
     */
    public function setAction(string $action): ImportAction
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get ID.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set ID.
     *
     * @param string $id
     *
     * @return ImportAction
     */
    public function setId(string $id): ImportAction
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get object.
     *
     * @return mixed
     */
    public function getObject(): mixed
    {
        return $this->object;
    }

    /**
     * Set object.
     *
     * @param mixed $object
     *
     * @return ImportAction
     */
    public function setObject(mixed $object): ImportAction
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get weight.
     *
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * Set weight.
     *
     * @param int $weight
     *
     * @return ImportAction
     */
    public function setWeight(int $weight): ImportAction
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Add weight.
     *
     * @param int $weight Value to add.
     *
     * @return ImportAction
     */
    public function addWeight(int $weight): ImportAction
    {
        $this->weight += $weight;

        return $this;
    }

    /**
     * Get dependencies.
     *
     * @return array
     */
    public function getRequires(): array
    {
        return $this->requires;
    }

    /**
     * Set dependencies.
     *
     * @param array $requires
     *
     * @return ImportAction
     */
    public function setRequires(array $requires): ImportAction
    {
        $this->requires = $requires;

        return $this;
    }
}
