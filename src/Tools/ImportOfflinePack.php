<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Entity\Country;
use App\Entity\File;
use App\Forms\FieldTypes\Types\EntityReferenceFieldType;
use App\Forms\FieldTypes\Types\FileReferenceEntity;
use App\Forms\FieldTypes\Types\FormRecordReferenceFieldType;
use App\Forms\FieldTypes\Types\PhoneFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormReferenceEntityInterface;
use App\Repository\FileRepository;
use App\Service\FileSystem;
use App\Service\SessionService;
use App\Traits\CallablePromptMessagesTrait;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Uid\Ulid;
use ZipArchive;

/**
 * Offline data pack importer
 *
 * @test console forms:offline:import imports/AdminlocalHn_2023-4-26.zip AdminlocalHn
 */
class ImportOfflinePack
{
    use StringTranslationTrait;
    use GetContainerTrait;
    use CallablePromptMessagesTrait;

    /**
     * Pack ULIDs used how index, and local ULIDs how value.
     *
     * Ex. ['01GYS45KCNV6VJ60FVTT1RFGWD' => '01GYS45XG3J76TJG8QCT4QK3WE']
     *
     * @var array
     */
    protected array $ulidDictionary;
    /**
     * Deleted images.
     *
     * Ex. ['01GYS45KCNV6VJ60FVTT1RFGWD', '01GYS45XG3J76TJG8QCT4QK3WE']
     *
     * @var array
     */
    protected array $deletedImages;
    /**
     * Offline pack content index.
     *
     * @var array
     */
    protected array $index;
    protected UploadedFile $file;
    protected FormFactory $formFactory;
    protected EntityManagerInterface|null $entityManager;

    /**
     * Constructor.
     *
     * @param UploadedFile           $file
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(UploadedFile $file, EntityManagerInterface $entityManager)
    {
        $this->ulidDictionary = [];
        $this->deletedImages = [];
        $this->file = $file;
        $this->formFactory = self::getContainerInstance()->get('form_factory');
        $this->entityManager = $entityManager;
    }

    /**
     * Import pack.
     *
     * @return JsonResponse|Response|void
     *
     * @throws \Exception
     */
    public function import()
    {
        $user = SessionService::getUser();
        $this->displayText(sprintf('Using <question>%s</question> session.', $user->getUsername()));
        if (!$user) {
            throw new \Exception(self::t('User session is required'), 400);
        }
        $zip = new ZipArchive();
        if ($zip->open($this->file->getPathname()) === true) {
            $this->displayText(sprintf('Importing ZIP file: <info>%s</info>', $this->file->getPathname()));
            // Iterate through the files inside the ZIP archive.
            // Get index to process the pack.
            /** @var resource $indexStream */
            $indexResource = $zip->getStream('index.json');
            $indexContent = stream_get_contents($indexResource);
            $this->index = Json::decode($indexContent);
            // Import images.
            /** @var FileRepository $fileRepo */
            $fileRepo = $this->entityManager->getRepository(File::class);
            $tmpFolder = $this->getTemporaryDirectory();
            $this->displayInfo('Importing images...');
            foreach ($this->index as $change) {
                switch ($change['type']) {
                    case 'binary_file':
                        // Images only can be new or old & deleted.
                        if ($change['deleted']) {
                            // Old deleted.
                            $this->displayText("DELETE IMAGE: {$change['id']}");
                            $file = $fileRepo->find($change['id']);
                            if ($file) {
                                $fileRepo->removeNow($file);
                            }
                            $this->deletedUlid[] = $change['id'];
                        } else {
                            // New image.
                            $content = $this->getZipedContentByUlid($zip, $change['id']);
                            $filename = $this->getZipedNameByUlid($zip, $change['id']);
                            file_put_contents($tmpFolder.'/'.$filename, $content);
                            $uploadedFile = new UploadedFile($tmpFolder.'/'.$filename, $filename);
                            $file = $fileRepo->create($uploadedFile, $user->getCountry());
                            $this->displayText("ADD IMAGE: {$change['id']} => {$file->getId()}");
                            $this->addPackUlid($change['id'], $file->getId(), $change);
                        }
                        break;
                    default:
                        // ignore forms
                        break;
                }
            }
            // Import records with references.
            $this->displayInfo('Import records with references...');
            $lastLong = 0;
            $stuckSteps = 0;
            while (count($this->index) > 0) {
                $change = array_shift($this->index);
                if (count($this->index) === $lastLong) {
                    ++$stuckSteps;
                    if (count($this->index) === $stuckSteps) {
                        throw new \Exception(
                            self::t('Infinity loop detected, stuck in @id.', ['@id' => $change['id']]),
                            400
                        );
                    }
                } else {
                    $stuckSteps = 0;
                }
                $lastLong = count($this->index);
                $this->displayText(
                    sprintf(
                        '<fg=yellow>Queue length: <info>%s</info>, current item is <info>%s</info></>',
                        count($this->index),
                        $change['type']
                    )
                );

                switch ($change['type']) {
                    case 'binary_file':
                        // Ignore images.
                        $this->displayText('Image ignored in this step');
                        continue 2;
                    case 'form.point':
                        // Updated point.
                        /** @var FormManagerInterface $form */
                        $form = $this->formFactory->find('form.point');
                        $point = $form->find($change['id']);
                        $content = Json::decode($this->getZipedContentByUlid($zip, $change['id']));
                        // If have unresolved references, we must to re-queue.
                        if ($this->pointHaveUnresolvedReferences($form, $content)) {
                            array_push($this->index, $change);
                            $this->displayText(sprintf('Re-queued point by unresolved references: <info>%s</info>', $change['id']));
                        } else {
                            // Else, update water systems and providers in point.
                            $wsystem = $content['wsystem'];
                            $point->{'field_wsystems'} = [];
                            $wsystemsForm = $this->formFactory->find('form.wssystem');
                            foreach ($wsystem as $recordId) {
                                // Search old ID, if not found.
                                $oldRecord = $wsystemsForm->find($recordId['id']);
                                $nid = $recordId['id'];
                                if (!$oldRecord) {
                                    $nid = $this->getPackUlid($recordId['id']);
                                }
                                $nvalue = $point->{'field_wsystems'};
                                $nvalue[] = [
                                    'value' => $nid,
                                    'form' => 'form.wssystem',
                                ];
                                $point->{'field_wsystems'} = $nvalue;
                            }
                            $wsp = $content['wsp'];
                            $wspForm = $this->formFactory->find('form.wsprovider');
                            $point->{'field_wsps'} = [];
                            foreach ($wsp as $recordId) {
                                // Search old ID, if not found.
                                $oldRecord = $wspForm->find($recordId['id']);
                                $nid = $recordId['id'];
                                if (!$oldRecord) {
                                    $nid = $this->getPackUlid($recordId['id']);
                                }
                                $nvalue = $point->{'field_wsps'};
                                $nvalue[] = [
                                    'value' => $nid,
                                    'form' => 'form.wsprovider',
                                ];
                                $point->{'field_wsps'} = $nvalue;
                            }
                            $form->update($point);
                            // todo Update each form to dispatch fusions.
                        }
                        break;
                    default:
                        /** @var FormManagerInterface $form */
                        $form = $this->formFactory->find($change['type']);
                        // New or updated record.
                        // Records can be deleted, new or updated.
                        if ($change['deleted']) {
                            // Old deleted.
                            $form->drop($change['id']);
                            $this->deletedUlid[] = $change['id'];
                            $this->displayText(sprintf('Deleted record: <info>%s</info>', $change['id']));
                        } else {
                            $oldRecord = $form->find($change['id']);
                            $content = Json::decode($this->getZipedContentByUlid($zip, $change['id']));
                            $content = $this->cleanContent($form, $content);
                            // If we have unresolved record references we need do a delayed insert.
                            if ($this->haveUnresolvedReferences($form, $content)) {
                                array_push($this->index, $change);
                                $this->displayText(
                                    sprintf(
                                        'Re-queued record by unresolved references: <info>%s - %s</info>',
                                        $change['type'],
                                        $change['id']
                                    )
                                );
                            } else {
                                $this->displayText(sprintf('Update references to <info>%s</info>', $change['id']));
                                $content = $this->updateReferences($form, $content);
                                if (!$oldRecord) {
                                    // Is a new record.
                                    $newId = $form->insert($content);
                                    $this->addPackUlid($change['id'], $newId, $change);
                                    $this->displayText(sprintf('Inserted new record <info>"%s" %s</info>, from %s', $change['type'], $newId, $change['id']));
                                } else {
                                    // Update field content ULIDs.
                                    $fieldDefinitions = $form->getFields();
                                    // We need update an old record.
                                    foreach ($fieldDefinitions as $fieldDefinition) {
                                        switch ($fieldDefinition->getFieldType()) {
                                            case 'ulid':
                                            case 'country_reference':
                                                continue 2;
                                            case 'boolean':
                                            case 'radio_boolean':
                                                if (isset($content[$fieldDefinition->getId()])) {
                                                    $content[$fieldDefinition->getId()] = ($content[$fieldDefinition->getId()] === true || $content[$fieldDefinition->getId()] === '1');
                                                }
                                                break;
                                        }
                                        if (isset($content[$fieldDefinition->getId()])) {
                                            $oldRecord->set($fieldDefinition->getId(), $content[$fieldDefinition->getId()]);
                                        }
                                    }
                                    $form->update($oldRecord);
                                    $this->displayText(sprintf('Updated old record <info>"%s" %s</info>', $oldRecord->getForm()->getId(), $oldRecord->getId()));
                                }
                            }
                        }
                        break;
                }
            }
            $zip->close();
            $this->displayText('Zip file closed, end of process.');
        } else {
            throw new \Exception(self::t('Failed to open ZIP file'), 400);
        }
    }

    /**
     * Remove virtual fields, and meta properties.
     *
     * @param FormManagerInterface $form
     * @param array                $content
     *
     * @return array
     */
    protected function cleanContent(FormManagerInterface $form, array $content): array
    {
        $remove = [];
        $user = SessionService::getUser();
        $form->setIgnoreRequiredFields(true);
        foreach ($content as $fieldName => &$value) {
            if (!str_starts_with($fieldName, 'field_')) {
                $remove[] = $fieldName;
            } else {
                $field = $form->getFieldDefinition($fieldName);
                // The country field may be empty from the client.
                if ('country_reference' === $field->getFieldType()) {
                    $value = [
                        'value' => $user->getCountry()->getId(),
                        'class' => Country::class,
                    ];
                }
                // If is not a valid field value, we must ignore the new value.
                if (!$field->isValidContent($value, $content)) {
                    if ($field instanceof PhoneFieldType) {
                        $lastError = $field->getLastValidationMessages();
                        if (count($lastError) > 0) {
                            $this->displayWarning(
                                sprintf(
                                    '[%s => %s] Error cleaning content field "%s": %s',
                                    $form->getId(),
                                    $content['id'],
                                    $fieldName,
                                    implode("\n", $lastError)
                                )
                            );
                            $remove[] = $fieldName;
                        }
                    }
                }
            }
            if (is_array($value)) {
                unset($value['meta']);
            }
        }
        foreach ($remove as $item) {
            unset($content[$item]);
        }

        return $content;
    }

    /**
     * Have this point unresolved references?
     *
     * @param FormManagerInterface $form
     * @param array                $content
     *
     * @return bool
     */
    protected function pointHaveUnresolvedReferences(FormManagerInterface $form, array $content):bool
    {
        // Water systems.
        foreach ($content['wsystem'] as $item) {
            // Verify each ULID to validate if is known or not.
            $formType = 'form.wssystem';
            $subform = $this->formFactory->find($formType);
            $subRecord = $subform->find($item['id']);
            if (!$subRecord) {
                if (!$this->isKnownPackUlid($item['id'])) {
                    return true;
                }
            }
        }
        // Providers.
        foreach ($content['wsp'] as $item) {
            // Verify each ULID to validate if is known or not.
            $formType = 'form.wssystem';
            $subform = $this->formFactory->find($formType);
            $subRecord = $subform->find($item['id']);
            if (!$subRecord) {
                if (!$this->isKnownPackUlid($item['id'])) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Update images and subform references.
     *
     * @param FormManagerInterface $form
     * @param array                $content
     *
     * @return array
     */
    protected function updateReferences(FormManagerInterface $form, array $content): array
    {
        $fieldDefinitions = $form->getFields();
        foreach ($fieldDefinitions as $fieldDefinition) {
            if ($fieldDefinition instanceof FormRecordReferenceFieldType ||
                $fieldDefinition instanceof FileReferenceEntity) {
                $fieldName = $fieldDefinition->getId();
                if (isset($content[$fieldName])) {
                    if ($fieldDefinition->isMultivalued()) {
                        if (count($content[$fieldName]) > 0) {
                            foreach ($content[$fieldName] as &$item) {
                                $ulid = $item[$fieldDefinition->getPrimaryProperty()];
                                if ($fieldDefinition instanceof FormRecordReferenceFieldType) {
                                    $vForm = $this->formFactory->find($item['form']);
                                    $vRecord = $vForm->find($ulid);
                                    if ($vRecord) {
                                        continue;
                                    }
                                }
                                if (!in_array($ulid, $this->deletedImages) && $this->isKnownPackUlid($ulid)) {
                                    $item[$fieldDefinition->getPrimaryProperty()] = $this->getPackUlid($ulid);
                                }
                            }
                        }
                    } else {
                        if (!empty($content[$fieldName][$fieldDefinition->getPrimaryProperty()])) {
                            $ulid = $content[$fieldName][$fieldDefinition->getPrimaryProperty()];
                            if ($fieldDefinition instanceof FormRecordReferenceFieldType) {
                                $vForm = $this->formFactory->find($content[$fieldName]['form']);
                                $vRecord = $vForm->find($ulid);
                                if ($vRecord) {
                                    continue;
                                }
                            }
                            if (!in_array($ulid, $this->deletedImages)) {
                                $content[$fieldName][$fieldDefinition->getPrimaryProperty()] = $this->getPackUlid($ulid);
                            }
                        }
                    }
                } else {
                    // The field now is empty.
                    if ($fieldDefinition->isMultivalued()) {
                        $content[$fieldName] = [];
                    } else {
                        $content[$fieldName] = $fieldDefinition->getDefaultValue();
                    }
                }
            }
        }

        return $content;
    }

    /**
     * Have this record unresolved references?
     *
     * @param FormManagerInterface $form
     * @param array                $content
     *
     * @return bool
     */
    protected function haveUnresolvedReferences(FormManagerInterface $form, array &$content): bool
    {
        $fieldDefinitions = $form->getFields();
        foreach ($fieldDefinitions as $fieldDefinition) {
            if ($fieldDefinition instanceof FormRecordReferenceFieldType) {
                if (isset($content[$fieldDefinition->getId()])) {
                    if ($fieldDefinition->isMultivalued()) {
                        if (count($content[$fieldDefinition->getId()]) > 0) {
                            // Verify each ULID to validate if is known or not.
                            foreach ($content[$fieldDefinition->getId()] as &$item) {
                                $formType = $item['form'];
                                $subform = $this->formFactory->find($formType);
                                $subRecord = $subform->find($item['value']);
                                if (!$subRecord) {
                                    // Try to update reference.
                                    if ($this->isKnownPackUlid($item['value'])) {
                                        $ulid = $this->getPackUlid($item['value']);
                                        $this->displayText(sprintf('Update references to <info>%s</info>', $ulid));
                                        $item['value'] = $ulid;
                                        continue;
                                    }
                                    // We can't update reference, alert.
                                    $this->displayText(
                                        sprintf(
                                            'Unresolved references: %s::%s => %s',
                                            $form->getId(),
                                            $fieldDefinition->getId(),
                                            print_r($item['value'], true)
                                        )
                                    );

                                    return true;
                                }
                            }
                        }
                    } else {
                        if (!empty($content[$fieldDefinition->getId()]['value'])) {
                            // Verify ULID to validate if is known or not.
                            $formType = $content[$fieldDefinition->getId()]['form'];
                            $subform = $this->formFactory->find($formType);
                            $subRecord = $subform->find($content[$fieldDefinition->getId()]['value']);
                            if (!$subRecord) {
                                // Try to update reference.
                                if ($this->isKnownPackUlid($content[$fieldDefinition->getId()]['value'])) {
                                    $ulid = $this->getPackUlid($content[$fieldDefinition->getId()]['value']);
                                    $this->displayText(sprintf('Update references to <info>%s</info>', $ulid));
                                    $content[$fieldDefinition->getId()]['value'] = $ulid;
                                    continue;
                                }
                                // We can't update reference, alert.
                                $this->displayText(
                                    sprintf(
                                        'Unresolved references: %s::%s => %s',
                                        $form->getId(),
                                        $fieldDefinition->getId(),
                                        print_r($content[$fieldDefinition->getId()], true)
                                    )
                                );

                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Validate ULID.
     *
     * @param string $ulid
     * @param bool   $required
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function validatePackUlid(string $ulid, bool $required = true): void
    {
        if ($required && empty($ulid)) {
            throw new \Exception(sprintf('%s: ULID parameter is required, but is empty', self::class));
        }

        Ulid::fromString($ulid);
    }

    /**
     * Add a new offline pack ULID.
     *
     * @param string $packUlid
     * @param string $value
     * @param array  $changeRecord
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function addPackUlid(string $packUlid, string $value, array $changeRecord)
    {
        $this->validatePackUlid($packUlid);
        $this->validatePackUlid($value, false);
        $this->ulidDictionary[$packUlid] = $changeRecord;
        $this->ulidDictionary[$packUlid]['value'] =  $value;
    }

    /**
     * Update offline pack ULID.
     *
     * @param string $packUlid
     * @param string $value
     * @param array  $changeRecord
     *
     * @return void
     */
    protected function updatePackUlid(string $packUlid, string $value, array $changeRecord)
    {
        $this->addPackUlid($packUlid, $value, $changeRecord);
    }

    /**
     * Get local ULID.
     *
     * @param string $packUlid
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function getPackUlid(string $packUlid): string
    {
        if (!isset($this->ulidDictionary[$packUlid])) {
            throw new \Exception(sprintf('Unknown offline pack ULID: %s', $packUlid));
        }

        return $this->ulidDictionary[$packUlid]['value'];
    }

    /**
     * Have we a replacement to this packed Ulid?
     *
     * @param string $packUlid
     *
     * @return bool
     */
    protected function isKnownPackUlid(string $packUlid): bool
    {
        return isset($this->ulidDictionary[$packUlid]);
    }

    /**
     * Is this ULID marked to remove?
     *
     * @param string $packUlid
     *
     * @return string
     *
     * @throws \Exception
     */
    protected function isDeletedInPack(string $packUlid): string
    {
        foreach ($this->index as $change) {
            if ($change['id'] === $packUlid) {
                return $change['deleted'];
            }
        }

        return false;
    }

    /**
     * Get temporal folder.
     *
     * @return string
     */
    protected function getTemporaryDirectory()
    {
        $tmpPath = self::getKernelInstance()->getProjectDir().'/var/cache/tmp';

        if (!is_dir($tmpPath)) {
            mkdir($tmpPath, 0777, true);
        }

        return $tmpPath;
    }

    /**
     * Get ZIPed file content by ULID.
     *
     * Notes: Records file names are {ulid}.json, and images are {ulid}_{file_name}
     *
     * @param ZipArchive $zip
     * @param string     $ulid
     *
     * @return false|string|null
     */
    protected function getZipedContentByUlid(ZipArchive $zip, string $ulid)
    {
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $fileName = $zip->getNameIndex($i);
            if (str_contains($fileName, $ulid)) {
                return $zip->getFromIndex($i);
            }
        }

        return null;
    }

    /**
     * Get ZIPed file name by ULID.
     *
     * Notes: Records file names are {ulid}.json, and images are {ulid}_{file_name}
     *
     * @param ZipArchive $zip
     * @param string     $ulid
     *
     * @return false|string|null
     */
    protected function getZipedNameByUlid(ZipArchive $zip, string $ulid)
    {
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $fileName = $zip->getNameIndex($i);
            if (str_contains($fileName, $ulid)) {
                $fileSystem = new FileSystem();

                return $fileSystem->getFileNameWithExtension($fileName);
            }
        }

        return null;
    }

    /**
     * Get all file names in ZIP folder.
     *
     * Note: This method must return sub-folder files too.
     *
     * @param ZipArchive $zip
     * @param string     $folder Folder path without initial '/', but with end '/'.
     *
     * @return array
     */
    protected function getZipFilesByFolder(ZipArchive $zip, string $folder): array
    {
        if (str_starts_with($folder, '/')) {
            $folder = substr($folder, 1);
        }
        if (!str_ends_with($folder, '/')) {
            $folder .= '/';
        }
        $resp = [];
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);
            if (str_starts_with($filename, $folder)) {
                $fileContents = $zip->getFromIndex($i);
                if (!empty($fileContents)) {
                    $resp[] = $filename;
                }
                unset($fileContents);
            }
        }

        return $resp;
    }
}
