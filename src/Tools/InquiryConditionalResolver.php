<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Entity\Country;
use App\Forms\FieldTypes\Types\SubformFieldType;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\SubformFormManager;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Symfony\Component\Uid\Ulid;

/**
 * Class to compute meta conditional about a record.
 */
class InquiryConditionalResolver
{
    use StringTranslationTrait;
    use GetContainerTrait;

    protected FormRecord $record;
    protected ?FormFactory $formFactory;

    /**
     * Resolver constructor.
     *
     * @param FormRecord $record
     */
    public function __construct(FormRecord $record)
    {
        $this->record = $record;
        $this->formFactory = static::getContainerInstance()->get('form_factory');
    }

    /**
     * Get if the field is visible in the current record state.
     *
     * @param string $fieldName
     * @param string $parentField The field name from a possible parent form.
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function isVisible(string $fieldName, string $parentField): bool
    {
        $field = $this->getForm()->getFieldDefinition($fieldName);
        $meta = $field->getMeta();

        /** @var Country $country */
        $country = $this->record->{'field_country'};
        $countryLevels = $country->getFormLevel();
        if (!isset($meta["level"])) {
            $meta["level"] = 1;
        }
        if (isset($countryLevels[$this->getForm()->getId()]) && $countryLevels[$this->getForm()->getId()] < $meta["level"]) {
            // The field level is higher than the country form level.
            return false;
        }

        if (!isset($meta['conditional']) || !isset($meta['conditional']['visible'])) {
            return true;
        }

        return $this->computeConditions($meta['conditional']['visible'], $parentField);
    }

    /**
     * Validate that the selected option is visible.
     *
     * @param string      $fieldName
     * @param string|null $currentValue
     * @param string      $parentField  The field name from a possible parent form.
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function isVisibleOption(string $fieldName, string|null $currentValue, string $parentField): bool
    {
        $field = $this->getForm()->getFieldDefinition($fieldName);
        $meta = $field->getMeta();
        if (!isset($meta['conditional']) || !isset($meta['conditional']['option']) || !$currentValue) {
            return true;
        }

        if (!isset($meta['conditional']['option'][$currentValue])) {
            return true;
        }

        return $this->computeConditions($meta['conditional']['option'][$currentValue], $parentField);
    }

    /**
     * Execute conditions.
     *
     * @param array  $conditions
     * @param string $parentField The field name from a possible parent form.
     * @param string $operator
     *
     * @return bool
     *
     * @throws \Exception
     */
    protected function computeConditions(array $conditions, string $parentField, string $operator = 'and'): bool
    {
        $result = ($operator === 'and');
        foreach ($conditions as $key => $condition) {
            $key = strtolower($key);
            $subOperator = in_array($key, ['or', 'and']) ? $key : $operator;
            if (is_array($condition)) {
                $internal = $this->computeConditions($condition, $parentField, $subOperator);
            } else {
                // Compute internal condition.
                switch ($key) {
                    case 'or':
                        $internal = $this->computeConditions($condition, $parentField, 'or');
                        break;
                    case 'and':
                        $internal = $this->computeConditions($condition, $parentField, 'and');
                        break;
                    default:
                        $currentValue = $this->getCurrentFieldValue($key, $parentField);
                        $internal = $this->isEqualTo($currentValue, $condition);
                        break;
                }
            }
            // Aggregate result.
            $result = match ($operator) {
                'or' => $result || $internal,
                'and' => $result && $internal,
                default => throw new \Exception($this->t('Unknown conditional operator: @op.', ['@op' => $operator])),
            };
        }

        return $result;
    }

    /**
     * Compare $value with $condition.
     *
     * @param $value
     * @param $condition
     *
     * @return bool
     */
    protected function isEqualTo($value, $condition): bool
    {
        if (is_array($value)) {
            return in_array($condition, $value);
        }

        return $value === $condition;
    }

    /**
     * Get current field value.
     *
     * Find value from fields in this record, or referenced values.
     *
     * @param string $key         Field name or external reference.
     * @param string $parentField The field name from a possible parent form.
     *
     * @return mixed
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws \ReflectionException
     */
    protected function getCurrentFieldValue(string $key, string $parentField): mixed
    {
        $parts = explode('.', $key);
        if (count($parts) === 1) {
            // Don't have separator.
            return $this->record->{$key};
        }

        if (count($parts) > 2) {
            // Not valid conditional key.
            throw new \Exception(
                $this->t(
                    "[@form] Conditional key it's Invalid, \"@key\". The can have only 1 separator point.",
                    [
                        '@form' => $this->record->getForm()->getId(),
                        '@key' => $key,
                    ]
                )
            );
        }

        /** @var FormManagerInterface $form */
        $form = $this->record->getForm();
        if ('parent' === $parts[0]) {
            // Reference to a parent form field.
            if ($this->record->getForm()->getType() !== SubformFormManager::class) {
                throw new \Exception(
                    $this->t(
                        "[@form] Parent conditional key only is valid in @subform, \"@key\".",
                        [
                            '@form' => $this->record->getForm()->getId(),
                            '@subform' => SubformFormManager::class,
                            '@key' => $key,
                        ]
                    )
                );
            }
            $formMeta = $form->getMeta();
            if (!$formMeta['parent_form']) {
                throw new \Exception(
                    $this->t(
                        '[@form] Parent meta property not found, key: "@key".',
                        [
                            '@form' => $this->record->getForm()->getId(),
                            '@key' => $key,
                        ]
                    )
                );
            }
            $parentForm = $this->formFactory->find($formMeta['parent_form']);
            if (!$parentForm) {
                throw new \Exception(
                    $this->t(
                        '[@form] Parent form not found, "@parent".',
                        [
                            '@form' => $this->record->getForm()->getId(),
                            '@parent' => $formMeta['parent_form'],
                        ]
                    )
                );
            }
            // Find this record in parent.
            $ulid = new Ulid($this->record->getId());
            $parentRecord = $parentForm->findBy(
                [
                    $parentField => $ulid->toBinary(),
                ]
            );
            // Return parent field value.
            return count($parentRecord) > 0 ? $parentRecord[0]->{$parts[1]} : null;
        }

        // Reference to a sub form field.
        /** @var SubformFieldType $referenceField */
        $referenceField = $form->getFieldDefinition($parts[0]);
        $subFormId = $referenceField->getSubFormId();
        $subForm = $this->formFactory->find($subFormId);
        if (!$subForm) {
            throw new \Exception(
                $this->t(
                    '[@form] Subform form not found, "@subform".',
                    [
                        '@form' => $this->record->getForm()->getId(),
                        '@subform' => $subFormId,
                    ]
                )
            );
        }
        // Find this record in subform.
        $subRecords = $this->record->{$parts[0]};
        $ids = [];
        /** @var FormRecord $subRecord */
        foreach ($subRecords as $subRecord) {
            $ulid = new Ulid($subRecord->getId());
            $ids[] = $ulid->toBinary();
        }
        $subRecord = $subForm->find(current($ids));
        // Return parent field value.
        return $subRecord ? $subRecord->{$parts[1]} : null;
    }

    /**
     * Get current form manager.
     *
     * @return FormManagerInterface
     */
    protected function getForm(): FormManagerInterface
    {
        return $this->record->getForm();
    }
}
