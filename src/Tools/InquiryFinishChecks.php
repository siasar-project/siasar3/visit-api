<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use ApiPlatform\Core\Bridge\Symfony\Routing\IriConverter;
use App\Entity\InquiryFormLog;
use App\Forms\FormRecord;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Plugins\InquiryCheckActionDiscovery;
use App\Repository\InquiryFormLogRepository;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Annotations\AnnotationReader;
use Monolog\Logger;

/**
 * Check inquiry before get finished.
 */
class InquiryFinishChecks
{
    use GetContainerTrait;

    protected InquiryCheckActionDiscovery $inquiryCheckActionDiscovery;
    protected $entityManager;
    protected null|SessionService $sessionManager;
    protected null|IriConverter $iriConverter;
    protected Logger|null $inquiryFormLogger;
    protected AnnotationReader|null $annotationReader;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->inquiryCheckActionDiscovery = static::getContainerInstance()->get('inquirycheckaction_discovery');
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->iriConverter = static::getContainerInstance()->get('api_platform.iri_converter');
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->annotationReader = static::getContainerInstance()->get('annotations.reader');
    }

    /**
     * Check all inquiry form before go to finished.
     *
     * @param FormRecord $inquiry
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function checkInquiry(FormRecord $inquiry): bool
    {
        $result = true;

        // Clear all logs.
        $inquiryLogRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $this->clearInquiryLogs($inquiryLogRepo, $inquiry);
        $this->entityManager->flush();

        // Checks.
        $actions = $this->inquiryCheckActionDiscovery->getInquiryCheckActions($inquiry->getForm()->getId());
        foreach ($actions as $action) {
            $class = $action['class'];
            /** @var AbstractInquiryCheckActionBase $action */
            $actionInstance = new $class(
                $this->annotationReader,
                $this->sessionManager,
                $this->inquiryFormLogger,
                $inquiry
            );
            $singleResult = $actionInstance->check();
            if (!$singleResult) {
                if ($inquiry->haveField('field_status')) {
                    $inquiry->{'field_status'} = 'draft';
                } elseif ($inquiry->haveField('field_finished')) {
                    $inquiry->{'field_finished'} = false;
                }
                $inquiry->save();
            }
            $result &= $singleResult;
        }

        return $result;
    }

    /**
     * Get all logs about this inquiry.
     *
     * @param FormRecord $inquiry The inquiry.
     *
     * @return InquiryFormLog[] The Log record list.
     */
    public function getInquiryLogs(FormRecord $inquiry): array
    {
        $inquiryLogRepo = $this->entityManager->getRepository(InquiryFormLog::class);

        return $inquiryLogRepo->findBy(['recordId' => $inquiry->getId()]);
    }

    /**
     * Clear old inquiry logs.
     *
     * This method don't call flush at the end.
     *
     * @param InquiryFormLogRepository $formLogRepository
     * @param FormRecord               $inquiry
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function clearInquiryLogs(InquiryFormLogRepository $formLogRepository, FormRecord $inquiry)
    {
        $logs = $formLogRepository->findBy(['recordId' => $inquiry->getId()]);
        foreach ($logs as $log) {
            $formLogRepository->remove($log);
        }
    }
}
