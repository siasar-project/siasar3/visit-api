<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Traits\StringTranslationTrait;

/**
 * Json tools.
 */
class Json
{
    use StringTranslationTrait;

    /**
     * Decode Json string or throw error.
     *
     * @param string $json
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public static function decode(string $json): mixed
    {
        $resp = json_decode($json, true);
        $lastJsonError = json_last_error_msg();
        if ('No error' !== $lastJsonError) {
            if (empty($json)) {
                $lastJsonError = 'The Json string is empty.';
            }
            throw new \Exception(self::t('Bad JSON format: @msg.', ['@msg' => $lastJsonError]));
        }

        return $resp;
    }

    /**
     * Encode Json string or throw error.
     *
     * @param mixed $data
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function encode(mixed $data): string
    {
        $resp = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_INVALID_UTF8_IGNORE);
        $lastJsonError = json_last_error_msg();
        if ('No error' !== $lastJsonError) {
            throw new \Exception(self::t('Bad JSON format: @msg.', ['@msg' => $lastJsonError]));
        }

        return $resp;
    }
}
