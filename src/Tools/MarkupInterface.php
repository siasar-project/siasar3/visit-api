<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

/**
 * Marks an object's __toString() method as returning markup.
 *
 * Objects that implement this interface will not be automatically XSS filtered
 * by the render system or automatically escaped by the theme engine.
 *
 * If there is any risk of the object's __toString() method returning
 * user-entered data that has not been filtered first, it must not be used. If
 * the object that implements this does not perform automatic escaping or
 * filtering itself, then it must be marked as "@internal". For example, Views
 * has the internal ViewsRenderPipelineMarkup object to provide a custom render
 * pipeline in order to render JSON and to fast render fields. By contrast,
 * FormattableMarkup and TranslatableMarkup always sanitize their output when
 * used correctly.
 *
 * If the object is going to be used directly in Twig templates it should
 * implement \Countable so it can be used in if statements.
 *
 * @see \Drupal\Component\Render\MarkupTrait
 * @see \Drupal\Core\Template\TwigExtension::escapeFilter()
 * @see \Drupal\Component\Render\FormattableMarkup
 * @see \Drupal\Core\StringTranslation\TranslatableMarkup
 * @see \Drupal\views\Render\ViewsRenderPipelineMarkup
 * @see twig_render_template()
 * @see sanitization
 * @see theme_render
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
interface MarkupInterface extends \JsonSerializable
{

    /**
     * Returns markup.
     *
     * @return string
     *   The markup.
     */
    public function __toString();
}
