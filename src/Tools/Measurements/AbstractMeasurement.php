<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools\Measurements;

use App\Entity\Configuration;
use App\Traits\GetContainerTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Base measurement handler.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
abstract class AbstractMeasurement
{
    use GetContainerTrait;

    protected array $units;
    protected string $referenceUnit;
    protected float $value;
    protected string $unit;

    /**
     * BaseMeasurement constructor.
     *
     * @param string $configId     Units configuration ID.
     * @param array  $defaultUnits Units configuration ID.
     * @param string $refUnit      Reference unit.
     * @param float  $value        Initial value.
     * @param string $unit         Initial unit.
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __construct(string $configId, array  $defaultUnits, string $refUnit, float $value = 0, string $unit = '')
    {
        $this->value = $value;
        $this->unit = $unit;
        $this->referenceUnit = $refUnit;
        // Init unit list.
        // TODO May be useful units cache?
        if (empty($this->units)) {
            try {
                /** @var Registry $entityManager */
                $entityManager = $this->getContainerInstance()->get('doctrine');
                $repository = $entityManager->getRepository(Configuration::class);
                $units = $repository->find($configId);
                $this->units = (array) $units?->getValue();
            } catch (\Exception $e) {
                $this->units = $defaultUnits;
            }
        }
    }

    /**
     * Get measurement value.
     *
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * Set measurement value.
     *
     * @param float $value The new value.
     *
     * @return $this
     */
    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get the reference unit.
     *
     * @return string
     */
    public function getReferenceUnit(): string
    {
        return $this->referenceUnit;
    }

    /**
     * Get measurement units.
     *
     * @return array
     */
    public function getUnits(): array
    {
        return $this->units;
    }

    /**
     * Get measurement unit.
     *
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * Change measurement unit.
     *
     * @param string $unit The new unit.
     *
     * @return $this
     */
    public function setUnit(string $unit): self
    {
        // Convert this to base unit.
        $value = $this->convertToReference($this->value, $this->unit);
        // Convert this to the new unit.
        $this->unit = $unit;
        $this->value = $this->convertFromReference($value, $unit);

        return $this;
    }

    /**
     * Get a clone from this measurement.
     *
     * @return self
     */
    public function clone(): self
    {
        return new static($this->value, $this->unit);
    }

    /**
     * Convert value to reference unit.
     *
     * @param float  $value Value.
     * @param string $unit  Value units.
     *
     * @return float
     */
    protected function convertToReference(float $value, string $unit): float
    {
        if (empty($unit) || !isset($this->units[$unit])) {
            return $value;
        }

        return $value * $this->units[$unit]['rate'];
    }

    /**
     * Convert value to units from reference units.
     *
     * @param float  $value Value.
     * @param string $unit  Value units.
     *
     * @return float
     */
    protected function convertFromReference(float $value, string $unit): float
    {
        if (empty($unit) || !isset($this->units[$unit])) {
            return $value;
        }

        return $value / $this->units[$unit]['rate'];
    }
}
