<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools\Measurements;

/**
 * Flow measurement handler.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FlowMeasurement extends AbstractMeasurement
{

    /**
     * Flow Measurement constructor.
     *
     * @param float  $value        Initial value.
     * @param string $unit         Initial unit.
     * @param bool   $useException
     *
     * @throws \Exception
     */
    public function __construct(float $value = 0, string $unit = 'liter/second', bool $useException = true)
    {
        parent::__construct(
            'system.units.flow',
            [
                'liter/second' => [
                    'symbol' => 'L/s',
                    'rate' => 1,
                    'rate1' => 1,
                    'rate2' => 1,
                ],
                'cubic meter/second' => [
                    'symbol' =>  'm^3/s',
                    'rate' => 0.001,
                    'rate1' => 1000,
                    'rate2' => 1,
                ],
                'cubic meter/day' => [
                    'symbol' => 'm^3/d',
                    'rate' => 86.4,
                    'rate1' => 1000,
                    'rate2' => 86400,
                ],
            ],
            'liter/second',
            $value,
            $unit,
            $useException
        );
    }

    /**
     * Convert value to reference unit.
     *
     * @param float  $value Value.
     * @param string $unit  Value units.
     *
     * @return float
     */
    protected function convertToReference(float $value, string $unit): float
    {
        if (empty($unit) || !isset($this->units[$unit])) {
            return $value;
        }

        $rate1 = $this->units[$unit]['rate1'];
        $rate2 = $this->units[$unit]['rate2'];

        return $value * ($rate1 / $rate2);
    }

    /**
     * Convert value to units from reference units.
     *
     * @param float  $value Value.
     * @param string $unit  Value units.
     *
     * @return float
     */
    protected function convertFromReference(float $value, string $unit): float
    {
        if (empty($unit) || !isset($this->units[$unit])) {
            return $value;
        }

        $rate1 = $this->units[$unit]['rate1'];
        $rate2 = $this->units[$unit]['rate2'];

        return $value / ($rate1 / $rate2);
    }
}
