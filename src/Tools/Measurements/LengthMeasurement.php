<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools\Measurements;

/**
 * Length measurement handler.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LengthMeasurement extends AbstractMeasurement
{
    /**
     * LengthMeasurement constructor.
     *
     * @param float  $value        Initial value.
     * @param string $unit         Initial unit.
     * @param bool   $useException
     *
     * @throws \Exception
     */
    public function __construct(float $value = 0, string $unit = 'metre', bool $useException = true)
    {
        parent::__construct(
            'system.units.length',
            [
                'metre' => [
                    'rate' => 1,
                    'symbol' => 'm',
                ],
                'kilometre' => [
                    'rate' => 1000,
                    'symbol' => 'km',
                ],
                'hectometre' => [
                    'rate' => 100,
                    'symbol' => 'hm',
                ],
            ],
            'metre',
            $value,
            $unit,
            $useException
        );
    }
}
