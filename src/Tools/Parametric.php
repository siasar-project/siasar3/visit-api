<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */


namespace App\Tools;

use App\Annotations\IsCountryParametric;
use App\Annotations\Label;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Id;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use Symfony\Component\Yaml\Yaml;

/**
 * Default parametric entities tool.
 */
class Parametric
{
    use GetContainerTrait;

    protected ManagerRegistry $entityManager;
    protected Reader $annotationReader;
    protected array $cacheDefaultParametric;
    protected array $cacheIsParametric;

    /**
     * Parametric constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param Reader          $annotationReader
     */
    public function __construct(ManagerRegistry $entityManager, Reader $annotationReader)
    {
        $this->entityManager = $entityManager;
        $this->annotationReader = $annotationReader;
        $this->cacheDefaultParametric = [];
        $this->cacheIsParametric = [];
    }

    /**
     * Get parametric entity classes.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getParametricClasses(): array
    {
        $resp = [];

        /** @var ClassMetadata[] $metas */
        $metas = $this->entityManager->getManager()->getMetadataFactory()->getAllMetadata();
        foreach ($metas as $meta) {
            $annotations = $this->annotationReader->getClassAnnotations(new ReflectionClass($meta->getName()));

            foreach ($annotations as $note) {
                if ($note instanceof IsCountryParametric) {
                    $resp[] = $meta->getName();
                }
            }
        }

        return $resp;
    }

    /**
     * Get field that work how label.
     *
     * Default use ID.
     *
     * @param string|object $class Class name or instance.
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    public function getParametricLabelField(string|object $class): string
    {
        $className = $class;
        if (is_object($class)) {
            $className = get_class($class);
        }
        // Find label property.
        /** @var ClassMetadata $metas */
        $metas = $this->entityManager->getManager()->getMetadataFactory()->getMetadataFor($className);
        foreach ($metas->getFieldNames() as $fieldName) {
            $annotations = $this->annotationReader->getPropertyAnnotations(
                new \ReflectionProperty($className, $fieldName)
            );
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Label) {
                    return $fieldName;
                }
            }
        }

        // Label not found or defined, find ID property.
        foreach ($metas->getFieldNames() as $fieldName) {
            $annotations = $this->annotationReader->getPropertyAnnotations(
                new \ReflectionProperty($className, $fieldName)
            );
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Id) {
                    return $fieldName;
                }
            }
        }

        // Not found.
        return '';
    }

    /**
     * Is this a parametric?
     *
     * @param string $class Fully-qualified class name.
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function isParametricClass(string $class): bool
    {
        if (!isset($this->cacheIsParametric[$class])) {
            $classes = $this->getParametricClasses();
            $this->cacheIsParametric[$class] = in_array($class, $classes);
        }

        return $this->cacheIsParametric[$class];
    }

    /**
     * Is this entity a default parametric value?
     *
     * @param mixed $entity
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function isDefaultParametric(mixed $entity): bool
    {
        if ($this->isParametricClass(get_class($entity))) {
            $defaults = $this->getDefaultEntities(get_class($entity));
            if (count($defaults) > 0) {
                $properties = array_keys(current($defaults));
                foreach ($defaults as $default) {
                    $allOk = true;
                    foreach ($properties as $property) {
                        $first = strtoupper(substr($property, 0, 1));
                        $getterName = 'get'.$first.substr($property, 1);
                        // Some properties can be numeric or alphanumeric.
                        if (call_user_func([$entity, $getterName]) !== (string) $default[$property]) {
                            $allOk = false;
                            break;
                        }
                    }
                    if ($allOk) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get parametric list.
     *
     * @param string $className Fully-qualified class name.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getDefaultEntities(string $className): array
    {
        $resp = [];
        if ($this->isParametricClass($className)) {
            $simpleClassName = $this->getClassNameOnly($className);
            if (!isset($this->cacheDefaultParametric[$simpleClassName])) {
                $fixturesDir = self::getKernelInstance()->getProjectDir().'/src/ParametricFixtures/';
                if (is_file($fixturesDir.$simpleClassName.'.yml')) {
                    $resp = Yaml::parseFile($fixturesDir.$simpleClassName.'.yml');
                    $this->cacheDefaultParametric[$simpleClassName] = $resp;
                }
            } else {
                $resp = $this->cacheDefaultParametric[$simpleClassName];
            }
        }

        return $resp;
    }

    /**
     * Get class name without namespace.
     *
     * @see https://coderwall.com/p/cpxxxw/php-get-class-name-without-namespace
     *
     * @param string $class
     *
     * @return string|null
     */
    protected function getClassNameOnly(string $class): ?string
    {
        $path = explode('\\', $class);

        return array_pop($path);
    }
}
