<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Entity\Country;
use App\Service\FileSystem;
use App\Traits\CallablePromptMessagesTrait;
use App\Traits\GetContainerTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Parametric entities importer.
 */
class ParametricImporter
{
    use GetContainerTrait;
    use StringTranslationTrait;
    use CallablePromptMessagesTrait;

    protected ManagerRegistry $entityManager;
    protected TemplateGenerator $templates;
    protected FileSystem $fileSystem;
    protected string $countryCode;
    protected string $className;
    protected string $file;
    protected bool $clearOld;

    /**
     * Constructor.
     *
     * @param string $countryCode
     * @param string $className
     * @param string $file
     * @param bool   $clearOld
     */
    public function __construct(string $countryCode, string $className, string $file, bool $clearOld = false)
    {
        $this->entityManager = self::getContainerInstance()->get('doctrine');
        $this->templates = self::getContainerInstance()->get('template_generator');
        $this->fileSystem = self::getContainerInstance()->get('os_file_system');

        $this->countryCode = $countryCode;
        $this->className = $className;
        $this->file = $file;
        $this->clearOld = $clearOld;
    }

    /**
     * Import data.
     *
     * @param bool $validateFileName
     * @param bool $notModify        Do not do any changes to the database.
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function import(bool $validateFileName = true, bool $notModify = false): bool
    {
        // Validate country.
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find($this->countryCode);
        if (!$country) {
            $this->displayError(self::t('Country "@country" not found.', ['@country' => $this->countryCode]));

            return false;
        }
        $this->displayInfo(self::t('Importing to "@name"', ['@name' => $country->getName()]));

        // Validate class name.
        $metaClass = $this->templates->getParametricByClass('App\\Entity\\'.$this->className);
        if (!$metaClass) {
            $this->displayError(self::t('Parametric "@name" not found.', ['@name' => $this->className]));

            return false;
        }
        $this->displayInfo(self::t('Importing "@name" parametric', ['@name' => $metaClass->getName()]));

        // validate file.
        if ($validateFileName && stripos($this->file, $this->className) === false) {
            $this->displayError(
                self::t(
                    'File "@file" does not contain the class name "@name". Is it the wrong file?',
                    [
                        '@file' => $this->file,
                        '@name' => $this->className,
                    ]
                )
            );

            return false;
        }
        if ($validateFileName && strtolower($this->fileSystem->getFileExtension($this->file)) !== 'csv') {
            $this->displayError(self::t('File "@file" is not CSV.', ['@file' => $this->file]));

            return false;
        }
        if (!is_file($this->file)) {
            $this->displayError(self::t('File "@file" not found.', ['@file' => $this->file]));

            return false;
        }
        if (filesize($this->file) <= 0) {
            $this->displayError(self::t('File "@file" is empty.', ['@file' => $this->file]));

            return false;
        }
        $this->displayInfo(self::t('Reading "@file" file', ['@file' => $metaClass->getName()]));

        // Clear old entities.
        $class = $metaClass->getName();
        $repo = $this->entityManager->getRepository($class);
        $items = $repo->findBy(['country' => $country]);
        if ($this->clearOld && !$notModify) {
            // Precess exceptions.
            switch ($this->className) {
                case 'OfficialLanguage':
                    $country->setMainOfficialLanguage(null);
                    break;
            }
            // Remove items.
            foreach ($items as $item) {
                try {
                    $repo->removeNow($item);
                } catch (\Exception $e) {
                    $this->displayWarning($e->getMessage());
                }
            }
        }
        $items = $repo->findBy(['country' => $country]);
        if ($this->clearOld && $notModify) {
            $items = [];
        }
        if (count($items) > 0) {
            $this->displayError(self::t('The parametric must be empty, @count found.', ['@count' => count($items)]));

            return false;
        }

        // Import content.
        $rowIndex = 1;
        $header = [];
        $errorMsg = [];
        $outputWidth = 0;
        $groupTime = -hrtime(true);
        $completeTime = -hrtime(true);
        if (($fh = fopen($this->file, "r")) !== false) {
            while (($data = fgetcsv($fh, 1000000, ",")) !== false) {
                $error = false;
                $cols = count($data);
                if (1 === $rowIndex) {
                    $header = $data;
                } else {
                    $item = new $class();
                    $item->setCountry($country);
                    for ($c = 0; $c < $cols; $c++) {
                        if ('parent' !== $header[$c]) {
                            if ('App\Entity\AdministrativeDivision' === $class && 'level' === $header[$c]) {
                                continue;
                            }
                            $method = "set".ucfirst($header[$c]);
                            $item->$method($data[$c]);
                            if ('App\Entity\AdministrativeDivision' === $class && 'code' === $header[$c]) {
                                $item->setOldid($data[$c]);
                            }
                        } else {
                            if ($notModify) {
                                $parent = null;
                            } else {
                                $parent = $repo->findOneBy(['code' => $data[$c]]);
                                if ($parent) {
                                    $item->setParent($parent);
                                }
                            }
                        }
                    }
                    try {
                        if (!$notModify) {
                            $repo->saveNow($item);
                        } else {
                            unset($item);
                        }
                    } catch (\Exception $e) {
                        $error = true;
                        $errorMsg[] = $e->getMessage();
                    }
                }
                if ($error) {
                    $this->displayText('E');
                } else {
                    $this->displayText('.');
                }
                $rowIndex++;
                $outputWidth++;
                if ($outputWidth >= 80) {
                    $groupTime += hrtime(true);
                    $outputWidth = 0;
                    $this->displayText(self::t(' @count [@time ms]', ['@count' => $rowIndex - 1, '@time' => $groupTime/1e+6]), true);
                    $groupTime = -hrtime(true);
                }
            }
            $groupTime += hrtime(true);
            $completeTime += hrtime(true);
            $this->displayText(self::t(' @count [@time ms]', ['@count' => $rowIndex - 1, '@time' => $groupTime/1e+6]), true);
            if (!$notModify) {
                $this->displayInfo(self::t('Imported in @time ms.', ['@time' => $completeTime/1e+6]));
            } else {
                $this->displayInfo(self::t('Validated in @time ms.', ['@time' => $completeTime/1e+6]));
            }
            fclose($fh);
        }

        if (count($errorMsg) > 0) {
            foreach ($errorMsg as $msg) {
                $this->displayError($msg);
            }

            return false;
        }

        return true;
    }
}
