<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Entity\Language;
use App\Repository\LanguageRepository;
use App\Repository\LocaleTargetRepository;

/**
 * A class to hold plural translatable markup.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class PluralTranslatableMarkup extends TranslatableMarkup
{

    /**
     * The delimiter used to split plural strings.
     *
     * This is the ETX (End of text) character and is used as a minimal means to
     * separate singular and plural variants in source and translation text. It
     * was found to be the most compatible delimiter for the supported databases.
     */
    const DELIMITER = "\03";

    /**
     * The item count to display.
     *
     * @var int
     */
    protected int $count;

    /**
     * The already translated string.
     *
     * @var string
     */
    protected string $translatedString;

    /**
     * Language repository.
     */
    protected ?LanguageRepository $langRepository;

    /**
     * Constructs a new PluralTranslatableMarkup object.
     *
     * Parses values passed into this class through the format_plural() function
     * in Drupal and handles an optional context for the string.
     *
     * @param int                     $count
     *   The item count to display.
     * @param string                  $singular
     *   The string for the singular case. Make sure it is clear this is singular,
     *   to ease translation (e.g. use "1 new comment" instead of "1 new"). Do not
     *   use @count in the singular string.
     * @param string                  $plural
     *   The string for the plural case. Make sure it is clear this is plural, to
     *   ease translation. Use @count in place of the item count, as in
     *   "@count new comments".
     * @param array                   $args
     *   (optional) An array with placeholder replacements, keyed by placeholder.
     *   See \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
     *   additional information about placeholders. Note that you do not need to
     *   include @count in this array; this replacement is done automatically
     *   for the plural cases.
     * @param array                   $options
     *   (optional) An associative array of additional options. See t() for
     *   allowed keys.
     * @param ?LocaleTargetRepository $stringTranslation
     *   (optional) The string translation service.
     *
     * @see \Drupal\Component\Render\FormattableMarkup::placeholderFormat()
     */
    public function __construct($count, $singular, $plural, array $args = [], array $options = [], ?LocaleTargetRepository $stringTranslation = null)
    {
        $this->count = $count;
        $translatableString = implode(static::DELIMITER, [$singular, $plural]);
        $this->translatedString = '';
        $this->langRepository = null;
        $options['is_plural'] = true;
        parent::__construct($translatableString, $args, $options, $stringTranslation);
    }

    /**
     * Constructs a new class instance from already translated markup.
     *
     * This method ensures that the string is pluralized correctly. As opposed
     * to the __construct() method, this method is designed to be invoked with
     * a string already translated (such as with configuration translation).
     *
     * @param int    $count
     *   The item count to display.
     * @param string $translatedString
     *   The already translated string.
     * @param array  $args
     *   An associative array of replacements to make after translation. Instances
     *   of any key in this array are replaced with the corresponding value.
     *   Based on the first character of the key, the value is escaped and/or
     *   themed. See \Drupal\Component\Render\FormattableMarkup. Note that you
     *   do not need to include @count in this array; this replacement is done
     *   automatically for the plural cases.
     * @param array  $options
     *   An associative array of additional options. See t() for allowed keys.
     *
     * @return static
     *   A PluralTranslatableMarkup object.
     */
    public static function createFromTranslatedString($count, $translatedString, array $args = [], array $options = [])
    {
        $plural = new static($count, '', '', $args, $options);
        $plural->translatedString = $translatedString;

        return $plural;
    }

    /**
     * Renders the object as a string.
     *
     * @return string
     *   The translated string.
     */
    public function render()
    {
        if (!$this->translatedString) {
            $this->translatedString = $this->getStringTranslation()->translateString($this);
        }
        if ($this->translatedString === '') {
            return '';
        }

        $arguments = $this->getArguments();
        $arguments['@count'] = $this->count;
        $translatedArray = explode(static::DELIMITER, $this->translatedString);

        if ($this->count === 1) {
            return $this->placeholderFormat($translatedArray[0], $arguments);
        }

        $index = $this->getPluralIndex();
        if (0 === $index) {
            // Singular form.
            $return = $translatedArray[0];
        } else {
            if (isset($translatedArray[$index])) {
                // N-th plural form.
                $return = $translatedArray[$index];
            } else {
                // If the index cannot be computed or there's no translation, use the
                // second plural form as a fallback (which allows for most flexibility
                // with the replaceable @count value).
                $return = $translatedArray[1];
            }
        }

        return $this->placeholderFormat($return, $arguments);
    }

    /**
     * Gets the plural index through the gettext formula.
     *
     * @return int
     */
    public function getPluralIndex(): int
    {
        return $this->localeGetPlural($this->count, $this->getOption('langcode'));
    }

    /**
     * Returns plural form index for a specific number.
     *
     * The index is computed from the formula of this language.
     *
     * @param int         $count
     *   Number to return plural for.
     * @param string|null $langcode
     *   (optional) Language code to translate to a language other than what is used
     *   to display the page, or null for current language. Defaults to null.
     *
     * @return int
     *   The numeric index of the plural variant to use for this $langcode and
     *   $count combination or -1 if the language was not found or does not have a
     *   plural formula.
     */
    public function localeGetPlural(int $count, ?string $langcode = null): int
    {
        // $language_interface = \Drupal::languageManager()->getCurrentLanguage();
        if (!$langcode) {
            throw new \Exception('Default language not implemented.');
        }

        // Used to store precomputed plural indexes corresponding to numbers
        // individually for each language.
        // TODO implement cache.
        //$plural_indexes = [];

        // TODO implement default language by context: $language_interface->getId().
        $langcode = $langcode ? $langcode : 'ENGL';

        // Retrieve and statically cache the plural formulas for all languages.
        /** @var Language $language */
        $language = $this->getLanguageRepository()->find($langcode);
        if ($language) {
            $po = new PoHeader();
            [$nplurals, $pluralFormulas] = $po->parsePluralForms(
                sprintf(
                    'nplurals=%s; plural=(%s);',
                    $language->getPluralNumber(),
                    $language->getPluralFormula()
                )
            );
        } else {
            $pluralFormulas = [];
        }

        // If there is a plural formula for the language, evaluate it for the given
        // $count and statically cache the result for the combination of language
        // and count, since the result will always be identical.
        if (!empty($pluralFormulas)) {
            // Plural formulas are stored as an array for 0-199. 100 is the highest
            // modulo used but storing 0-99 is not enough because below 100 we often
            // find exceptions (1, 2, etc).
            $index = $count > 199 ? 100 + ($count % 100) : $count;

            return isset($pluralFormulas[$index]) ? $pluralFormulas[$index] : $pluralFormulas['default'];
        }

        if ('ENGL' === $langcode) {
            // In case there is no plural formula for English (no imported translation
            // for English), use a default formula.
            return (int) ($count !== 1);
        }

        // Otherwise, return -1 (unknown).
        return -1;
    }

    /**
     * {@inheritdoc}
     */
    public function __sleep()
    {
        return array_merge(parent::__sleep(), ['count']);
    }

    /**
     * Gets the language repository service.
     *
     * @return LanguageRepository
     *   The language repository service.
     */
    protected function getLanguageRepository(): LanguageRepository
    {
        if (!$this->langRepository) {
            $doctrine = $this->getContainerInstance()->get('doctrine');
            $this->langRepository = $doctrine->getRepository(Language::class);
        }

        return $this->langRepository;
    }
}
