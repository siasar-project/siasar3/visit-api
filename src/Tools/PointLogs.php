<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Entity\Country;
use App\Entity\PointLog;
use App\Repository\CountryRepository;
use App\Repository\PointLogRepository;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Auxiliary SIASAR Point log collection.
 */
class PointLogs implements LoggerInterface
{
    protected SiasarPointWrapper $pointWrapper;
    protected PointLogRepository $pointLogRepository;
    protected CountryRepository $countryRepository;
    protected SessionService $sessionService;

    /**
     * Build a point log instance.
     *
     * @param SiasarPointWrapper     $pointWrapper
     * @param EntityManagerInterface $entityManager
     * @param SessionService         $sessionService
     */
    public function __construct(SiasarPointWrapper $pointWrapper, EntityManagerInterface $entityManager, SessionService $sessionService)
    {
        $this->pointWrapper = $pointWrapper;
        $this->pointLogRepository = $entityManager->getRepository(PointLog::class);
        $this->countryRepository = $entityManager->getRepository(Country::class);
        $this->sessionService = $sessionService;
    }

    /**
     * {@inheritDoc}
     */
    public function emergency($message, array $context = array())
    {
        $this->log(Logger::EMERGENCY, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function alert($message, array $context = array())
    {
        $this->log(Logger::ALERT, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function critical($message, array $context = array())
    {
        $this->log(Logger::CRITICAL, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function error($message, array $context = array())
    {
        $this->log(Logger::ERROR, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function warning($message, array $context = array())
    {
        $this->log(Logger::WARNING, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function notice($message, array $context = array())
    {
        $this->log(Logger::NOTICE, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function info($message, array $context = array())
    {
        $this->log(Logger::INFO, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function debug($message, array $context = array())
    {
        $this->log(Logger::DEBUG, $message, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function log($level, $message, array $context = array())
    {
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        $item = new PointLog();
        $item->setContext(ContextLogNormalizer::normalize($context));
        $item->setExtra([]);
        $item->setLevel($level);
        $item->setLevelName(Logger::getLevelName($level));
        $item->setMessage($message);
        $item->setPoint($this->pointWrapper->getId());

        $countryId = $this->pointWrapper->getRecord()->get('field_country')['value'];
        $country = $this->countryRepository->find($countryId);
        $item->setCountry($country);

        $this->pointLogRepository->saveNow($item);
    }

    /**
     * Get number of warnings and errors messages.
     *
     * @return array
     */
    public function getSummary(): array
    {
        $resp = [
            'warnings' => -1,
            'errors' => -1,
        ];

        if ($this->pointWrapper->isNew()) {
            return $resp;
        }

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('point', new Ulid($this->pointWrapper->getId())))
            ->andWhere(Criteria::expr()->lt('level', 400));
        $warnings = $this->pointLogRepository->matching($criteria)->count();

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('point', new Ulid($this->pointWrapper->getId())))
            ->andWhere(Criteria::expr()->gte('level', 400));
        $errors = $this->pointLogRepository->matching($criteria)->count();

        $resp['warnings'] = $warnings;
        $resp['errors'] = $errors;

        return $resp;
    }

    /**
     * Is this level an error message?
     *
     * @param $level
     *
     * @return bool
     */
    public static function isError($level): bool
    {
        return $level >= 400;
    }

    /**
     * Is this level a notification message?
     *
     * @param $level
     *
     * @return bool
     */
    public static function isNotification($level): bool
    {
        return $level < 400;
    }
}
