<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use stdClass;

/**
 * HTTP SIASAR 2 rest client.
 *
 * This class manage session cookies.
 */
class Siasar2Client
{
    public const END_POINT_LOGIN = '/rest/user/login';
    public const END_POINT_ADMINISTRATIVE_DIVISIONS = '/rest/vocab/division_administrativa';

    protected string $url;
    protected string $username;
    protected string $password;

    /**
     * API client.
     * @var Client
     */
    protected Client $client;

    /**
     * @param ?string $url
     * @param ?string $username
     * @param ?string $password
     */
    public function __construct(string $url = null, string $username = null, string $password = null)
    {
        if (!$url) {
            $url = $_ENV['SIASAR2_API_URL'];
        }
        if (!$username) {
            $username = $_ENV['SIASAR2_API_USERNAME'];
        }
        if (!$password) {
            $password = $_ENV['SIASAR2_API_PASSWORD'];
        }
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;

        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->url,
            'cookies' => true,
        ]);
    }

    /**
     * Init user session.
     *
     * @return ?ResponseInterface
     *
     * @throws GuzzleException
     */
    public function login(): ?ResponseInterface
    {
        if (!boolval($_ENV['SIASAR2_API_ACTIVE'])) {
            return null;
        }
        // Client use cookies to support session.
        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode(['name' => $this->username, 'pass' => $this->password]),
        ];

        return $this->client->post(self::END_POINT_LOGIN, $options);
    }

    /**
     * Get the user visible administrative divisions.
     *
     * @return array
     *
     * @throws GuzzleException
     */
    public function getUserAdministrativeDivisions(): array
    {
        if (!boolval($_ENV['SIASAR2_API_ACTIVE'])) {
            return [];
        }

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ];
        $resp = $this->client->get(self::END_POINT_ADMINISTRATIVE_DIVISIONS, $options);
        $body = Json::decode($resp->getBody()->getContents());

        $tree = [];
        // Create tree how a list.
        foreach ($body['allowed_values'] as $id => $value) {
            $tree[$id] = $this->getEmptyAdministrativeDivisionNode($value);
        }
        // Complete data.
        if (isset($body['field_codigo_division_admin'])) {
            foreach ($body['field_codigo_division_admin'] as $id => $value) {
                $tree[$id]->code = $value;
            }
        }
        // Move nodes to parents.
        foreach ($body['hierarchy'] as $child => $parent) {
            if (0 !== $parent) {
                $parentNode = $this->administrativeDivisionFind($parent, $tree);
                if ($parentNode) {
                    $parentNode->children[$child] = $tree[$child];
                    unset($tree[$child]);
                } else {
                    throw new \Exception(sprintf("Aprent node %s not found.", $parent));
                }
            }
        }

        return $tree;
    }

    /**
     * Find parent node.
     *
     * @param int   $parentId
     * @param array $tree
     *
     * @return ?stdClass
     */
    protected function administrativeDivisionFind(int $parentId, array &$tree): ?stdClass
    {
        foreach ($tree as $id => $node) {
            if ($id === $parentId) {
                return $node;
            }
            $cid = $this->administrativeDivisionFind($parentId, $node->children);
            if ($cid) {
                return $cid;
            }
        }

        return null;
    }

    /**
     * Generate a empty node.
     *
     * @param string $name
     *
     * @return stdClass
     */
    protected function getEmptyAdministrativeDivisionNode(string $name): stdClass
    {
        $node = new stdClass();
        $node->name = $name;
        $node->children = [];
        $node->code = '';

        return $node;
    }
}
