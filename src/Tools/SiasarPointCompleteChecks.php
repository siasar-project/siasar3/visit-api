<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use ApiPlatform\Core\Bridge\Symfony\Routing\IriConverter;
use App\Entity\InquiryFormLog;
use App\Entity\PointLog;
use App\Forms\FormRecord;
use App\Plugins\AbstractPointCheckActionBase;
use App\Plugins\PointCheckActionDiscovery;
use App\Repository\InquiryFormLogRepository;
use App\Service\SessionService;
use App\Traits\GetContainerTrait;
use Doctrine\Common\Annotations\AnnotationReader;
use Monolog\Logger;

/**
 * Check SIASAR Point after get completed.
 */
class SiasarPointCompleteChecks
{
    use GetContainerTrait;

    protected PointCheckActionDiscovery $pointCheckActionDiscovery;
    protected $entityManager;
    protected null|SessionService $sessionManager;
    protected null|IriConverter $iriConverter;
    protected Logger|null $inquiryFormLogger;
    protected AnnotationReader|null $annotationReader;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->pointCheckActionDiscovery = static::getContainerInstance()->get('pointcheckaction_discovery');
        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->iriConverter = static::getContainerInstance()->get('api_platform.iri_converter');
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->annotationReader = static::getContainerInstance()->get('annotations.reader');
    }

    /**
     * Check all point forms before go to reviewing.
     *
     * @param FormRecord $point
     *
     * @return bool
     *
     * @throws \ReflectionException
     */
    public function checkPoint(FormRecord $point): bool
    {
        $pWrapper = new SiasarPointWrapper($point, $this->entityManager, $this->sessionManager, $this->iriConverter);
        $records = array_merge($pWrapper->getCommunities(), $pWrapper->getSystems(), $pWrapper->getWSProviders());

        $actionsCommunity = $this->pointCheckActionDiscovery->getPointCheckActions('form.community');
        $actionsWSProvider = $this->pointCheckActionDiscovery->getPointCheckActions('form.wsprovider');
        $actionsSystem = $this->pointCheckActionDiscovery->getPointCheckActions('form.wssystem');
        $result = true;

        // Clear old logs.
        $this->clearPointLogs($point);
        $inquiryLogRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        foreach ($records as $record) {
            $this->clearInquiryLogs($inquiryLogRepo, $record);
        }
        $this->entityManager->flush();

        // Validate forms.
        foreach ($records as $record) {
            switch ($record->getForm()->getId()) {
                case 'form.community':
                    $collection = $actionsCommunity;
                    break;
                case 'form.wsprovider':
                    $collection = $actionsWSProvider;
                    break;
                case 'form.wssystem':
                    $collection = $actionsSystem;
                    break;
            }
            foreach ($collection as $action) {
                $class = $action['class'];
                /** @var AbstractPointCheckActionBase $action */
                $actionInstance = new $class(
                    $this->annotationReader,
                    $this->sessionManager,
                    $this->inquiryFormLogger,
                    $record,
                    $point
                );
                $singleResult = $actionInstance->check();
                if (!$singleResult) {
                    $record->{'field_status'} = 'draft';
                    $record->save();
                }
                $result &= $singleResult;
            }
        }

        return $result;
    }

    /**
     * Clear old point logs.
     *
     * This method flush modifications.
     *
     * @param FormRecord $point
     *
     * @return void
     */
    protected function clearPointLogs(FormRecord $point)
    {
        $pointLogRepo = $this->entityManager->getRepository(PointLog::class);
        $logs = $pointLogRepo->findBy(['point' => $point->getId()]);
        foreach ($logs as $log) {
            $pointLogRepo->remove($log);
        }
        $this->entityManager->flush();
    }

    /**
     * Clear old inquiry logs.
     *
     * This method don't call flush at the end.
     *
     * @param InquiryFormLogRepository $formLogRepository
     * @param FormRecord               $point
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     */
    protected function clearInquiryLogs(InquiryFormLogRepository $formLogRepository, FormRecord $point)
    {
        $logs = $formLogRepository->findBy(['recordId' => $point->getId()]);
        foreach ($logs as $log) {
            $formLogRepository->remove($log);
        }
    }
}
