<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Entity\File;
use App\Entity\InquiryFormLog;
use App\Entity\User;
use App\Forms\FormRecord;
use App\RepositorySecured\InquiryFormLogRepositorySecured;
use App\Service\SessionService;
use App\Traits\StringTranslationTrait;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * This class wraps a SIASAR Point FormRecord adding functionality.
 *
 * An SIASAR Point might require this but without a record in the database.
 * SP is SIASAR Point.
 */
class SiasarPointWrapper
{
    use StringTranslationTrait;

    /**
     * Create action code.
     */
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_TO_PLAN = 'to_plan';
    public const ACTION_COMPLETE = 'complete';
    public const ACTION_CALCULATE = 'calculate';
    public const ACTION_REJECT = 'reject';
    public const ACTION_DELETE = 'delete';

    protected ?FormRecord $record;
    protected PointLogs $logs;
    protected IriConverterInterface $iriConverter;
    protected EntityManagerInterface $entityManager;

    /**
     * Build a new wrapper.
     *
     * @param FormRecord|null        $record
     * @param EntityManagerInterface $entityManager
     * @param SessionService         $sessionService
     * @param IriConverterInterface  $iriConverter
     *
     * @throws \Exception
     */
    public function __construct(FormRecord $record = null, EntityManagerInterface $entityManager, SessionService $sessionService, IriConverterInterface $iriConverter)
    {
        // Validate record.
        if ($record) {
            if ('form.point' !== $record->getForm()->getId()) {
                throw new \Exception(
                    $this->t(
                        'SiasarPointWrapper error, the form record must be from "form.point" but it\'s from "@form".',
                        [
                            '@form' => $record->getForm()->getId(),
                        ]
                    )
                );
            }
        }
        // Init.
        $this->entityManager = $entityManager;
        $this->record = $record;
        $this->logs = new PointLogs($this, $entityManager, $sessionService);
        $this->iriConverter = $iriConverter;
    }

    /**
     * Get SP ID.
     *
     * @return string
     */
    public function getId(): string
    {
        if ($this->record) {
            return $this->record->getId();
        }

        return '';
    }

    /**
     * Get raw status.
     *
     * @return string
     */
    public function getStatusCode(): string
    {
        if ($this->record) {
            //Get status from record.
            return $this->record->{'field_status'};
        }

        return 'without planning';
    }

    /**
     * Get translated status.
     *
     * @return string
     */
    public function getStatus(): string
    {
        $status = $this->getStatusCode();

        switch ($status) {
            case 'reviewing':
                $allValidated = true;
                $inquiries = array_merge($this->getCommunities(), $this->getSystems(), $this->getWSProviders());
                foreach ($inquiries as $inquiry) {
                    if (!in_array($inquiry->{'field_status'}, ['validated', 'locked'])) {
                        $allValidated = false;
                        break;
                    }
                }
                if ($allValidated) {
                    $status = 'reviewed';
                }
                break;

            default:
                $status = $this->getStatusCode();
                break;
        }

        return $this->t($status);
    }

    /**
     * Get SP version.
     *
     * The version is the creation date and time.
     *
     * @return string
     */
    public function getVersion(): string
    {
        $date = new \DateTime();
        if ($this->record) {
            // Get version from record creation date.
            return $this->record->{'field_changed'}->format('Y-m-d');
        }

        return $date->format('Y-m-d');
    }

    /**
     * Get SP created date.
     *
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        if ($this->record) {
            // Get from record.
            return $this->record->{'field_created'};
        }

        return new \DateTime();
    }

    /**
     * Get SP updated date.
     *
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        if ($this->record) {
            // Get from record.
            return $this->record->{'field_changed'};
        }

        return new \DateTime();
    }

    /**
     * Get allowed user actions.
     *
     * @param array $context Current point context.
     *
     * @return array|string[]
     */
    public function getActionsAllowed(array $context = []): array
    {
        $resp = [static::ACTION_CREATE];
        if ($this->record) {
            $resp = [];

            switch ($this->record->{'field_status'}) {
                case 'planning':
                    $resp = [static::ACTION_UPDATE, self::ACTION_DELETE];
                    if ($this->record->{'field_chk_to_plan'}) {
                        $resp[] = self::ACTION_TO_PLAN;
                    }
                    break;

                case 'digitizing':
                    // If we are not known the point relation tree, we can't complete the point.
                    if (isset($context['inquiry_relations'])) {
                        $allowComplete = true;
                        // Validate inquiry relations.
                        if (isset($context['inquiry_relations']['systems'])) {
                            foreach ($context['inquiry_relations']['systems'] as $system) {
                                if (0 === count($system["communities"]) || 0 === count($system["providers"])) {
                                    $allowComplete = false;
                                    break;
                                }
                            }
                            if ($allowComplete) {
                                // All systems are linked. We must validate communities and providers now.
                                $inSystem = false;
                                foreach ($context["inquiry_relations"]["communities"] as $comm) {
                                    foreach ($context['inquiry_relations']['systems'] as $system) {
                                        foreach ($system['communities'] as $sComm) {
                                            if ($sComm === $comm['id']) {
                                                $inSystem = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (!$inSystem && count($context['inquiry_relations']['systems']) > 0) {
                                    $allowComplete = false;
                                }
                                $inSystem = false;
                                foreach ($context["inquiry_relations"]["providers"] as $comm) {
                                    foreach ($context['inquiry_relations']['systems'] as $system) {
                                        foreach ($system['providers'] as $sComm) {
                                            if ($sComm === $comm['id']) {
                                                $inSystem = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (!$inSystem && count($context['inquiry_relations']['systems']) > 0) {
                                    $allowComplete = false;
                                }
                            }
                        }
                        // Validate status.
                        if ($allowComplete) {
                            $inquiries = array_merge($this->getCommunities(), $this->getSystems(), $this->getWSProviders());
                            foreach ($inquiries as $inquiry) {
                                if ('finished' !== $inquiry->{'field_status'} && !in_array($inquiry->{'field_status'}, ['validated', 'locked'])) {
                                    $allowComplete = false;
                                    break;
                                }
                            }
                        }
                        if ($allowComplete) {
                            $resp[] = self::ACTION_COMPLETE;
                        }
                    }
                    break;

                case 'reviewing':
                    if (isset($this->record)) {
                        $inquiries = array_merge(
                            $this->getCommunities(),
                            $this->getWSProviders(),
                            $this->getSystems()
                        );
                        $allValidated = true;
                        $allValidatedOrFinished = true;
                        $anyDraft = false;
                        foreach ($inquiries as $inquiry) {
                            if (!in_array($inquiry->{'field_status'}, ['validated', 'locked'])) {
                                $allValidated = false;
                            }
                            if (!in_array($inquiry->{'field_status'}, ['validated', 'locked']) && 'finished' !== $inquiry->{'field_status'}) {
                                $allValidatedOrFinished = false;
                            }
                            if ('draft' === $inquiry->{'field_status'}) {
                                $anyDraft = true;
                            }
                            if (!$allValidated && $anyDraft) {
                                // Here, we know that we can't calculate and we can reject the point.
                                break;
                            }
                        }
                        if ($allValidated) {
                            $resp[] = self::ACTION_CALCULATE;
                        }
                        if ($anyDraft) {
                            $resp[] = self::ACTION_REJECT;
                        }
                        if ($allValidatedOrFinished) {
                            $resp[] = self::ACTION_COMPLETE;
                        }
                    }
                    break;
            }
        }

        return $resp;
    }

    /**
     * Get point logs.
     *
     * @return PointLogs
     */
    public function getLogs(): PointLogs
    {
        return $this->logs;
    }

    /**
     * Get wrapped form record.
     *
     * @return FormRecord
     */
    public function getRecord(): FormRecord
    {
        return $this->record;
    }

    /**
     * Is a Point without form record?
     *
     * @return bool
     */
    public function isNew(): bool
    {
        return (is_null($this->record));
    }

    /**
     * Get point team list.
     *
     * @return array
     */
    public function getTeam(): array
    {
        if ($this->record) {
            // Get from record.
            return $this->record->{'field_team'};
        }

        return [];
    }

    /**
     * Get point communities list.
     *
     * @return FormRecord[]
     */
    public function getCommunities(): array
    {
        if ($this->record) {
            // Get from record.
            $forms = $this->record->{'field_communities'};
            $resp = [];
            foreach ($forms as $form) {
                if (!$form || $form->{'field_deleted'}) {
                    continue;
                }
                $resp[] = $form;
            }

            return $resp;
        }

        return [];
    }

    /**
     * Get point systems list.
     *
     * @return FormRecord[]
     */
    public function getSystems(): array
    {
        if ($this->record) {
            // Get from record.
            $forms = $this->record->{'field_wsystems'};
            $resp = [];
            foreach ($forms as $form) {
                if (!$form || $form->{'field_deleted'}) {
                    continue;
                }
                $resp[] = $form;
            }

            return $resp;
        }

        return [];
    }

    /**
     * Get point providers list.
     *
     * @return FormRecord[]
     */
    public function getWSProviders(): array
    {
        if ($this->record) {
            // Get from record.
            $forms = $this->record->{'field_wsps'};
            $resp = [];
            foreach ($forms as $form) {
                if (!$form || $form->{'field_deleted'}) {
                    continue;
                }
                $resp[] = $form;
            }

            return $resp;
        }

        return [];
    }

    /**
     * Get virtual Point representation.
     *
     * Used how end-point response.
     *
     * @param AdministrativeDivision|null $division
     *
     * @return array
     */
    public function getMetaRecord(AdministrativeDivision $division = null): array
    {
        $divisions = [];
        $communities = [];
        $communitiesMap = [];
        $wsps = [];
        $wsystems = [];
        $team = [];
        $tree = [
            'communities' => [],
            'systems' => [],
            'providers' => [],
        ];
        $image = '';
        $notes = [
            'warnings' => -1,
            'errors' => -1,
        ];

        if ($this->record) {
            $notes = [
                'warnings' => 0,
                'errors' => 0,
            ];
            $references = $this->record->{'field_communities'};

            /** @var FormRecord $communityRecord */
            foreach ($references as $communityRecord) {
                $deleted = !$communityRecord || $communityRecord->{'field_deleted'};
                if ($deleted) {
                    continue;
                }
                // Update points errors and warnings.
                $recordLogs = $this->getFormRecordLogSummary($communityRecord);
                $notes['errors'] += $recordLogs['errors'];
                $notes['warnings'] += $recordLogs['warnings'];

                // Update tree.
                $regionName = self::t('Community');
                /** @var AdministrativeDivision $region */
                $region = $communityRecord->{'field_region'};
                if ($region) {
                    $regionName = $region->getName();
                }
                $tree['communities'][] = [
                    'id' => $communityRecord->getId(),
                    'label' => $regionName,
                ];
                // Update lists.
                $communitiesMap[$communityRecord->{'field_region'}->getId()] = $communityRecord->getId();
                $divisions[] = $communityRecord->{'field_region'};
                $communities[] = $communityRecord->getId();

                // Get the image
                if ($division && $division->getId() === $communityRecord->{'field_region'}->getId()) {
                    $fieldImage = $communityRecord->getForm()->getMeta()['icon_field'];
                    $images = $communityRecord->{$fieldImage};

                    /** @var File[] $images */
                    foreach ($images as $currentImage) {
                        if ($currentImage && $currentImage->formatExtraJson()) {
                            $extras = $currentImage->formatExtraJson();
                            $image = $extras['url'];
                            break;
                        }
                    }
                }
            }

            $wsps = $this->record->{'field_wsps'};
            $wsystems = $this->record->{'field_wsystems'};
            $team = $this->record->{'field_team'};
        }

        if ($division) {
            array_unshift($divisions, $division);
        }

        $metaDivisions = [];
        foreach ($divisions as $division) {
            $metaDivisions[] = [
                'id' => $this->iriConverter->getIriFromItem($division),
                'name' => $division->getName(),
                'meta' => $division->formatExtraJson(),
            ];
        }

        $metaWsps = [];
        /** @var FormRecord $wsp */
        foreach ($wsps as $wsp) {
            $deleted = !$wsp || $wsp->{'field_deleted'};
            if ($deleted) {
                continue;
            }
            // Update points errors and warnings.
            $recordLogs = $this->getFormRecordLogSummary($wsp);
            $notes['errors'] += $recordLogs['errors'];
            $notes['warnings'] += $recordLogs['warnings'];
            // Update tree.
            $label = $wsp->{'field_provider_name'};
            $tree['providers'][] = [
                'id' => $wsp->getId(),
                'label' => empty($label) ? self::t('Service provider') : $label,
                'f1.4' => $wsp->{'field_provider_type'},
            ];
            // Update list.
            $metaWsps[] = [
                'id' => $wsp->getId(),
                'name' => $label,
                'meta' => $wsp->formatExtraJson(),
            ];
        }

        $metaWSystems = [];
        /** @var FormRecord $wsystem */
        foreach ($wsystems as $wsystem) {
            $deleted = !$wsystem || $wsystem->{'field_deleted'};
            if ($deleted) {
                continue;
            }
            // Update points errors and warnings.
            $recordLogs = $this->getFormRecordLogSummary($wsystem);
            $notes['errors'] += $recordLogs['errors'];
            $notes['warnings'] += $recordLogs['warnings'];
            // Update tree.
            $label = $wsystem->{'field_system_name'};
            $treeNode = [
                'id' => $wsystem->getId(),
                'label' => empty($label) ? self::t('Water system') : $label,
                'communities' => [],
                'providers' => [],
            ];
            $commsServed = $wsystem->{'field_served_communities'};
            foreach ($commsServed as $commServed) {
                if ($commServed) {
                    $commRegion = $commServed->get('field_community');
                    if ($commRegion && isset($communitiesMap[$commRegion['value']])) {
                        $treeNode['communities'][] = $communitiesMap[$commRegion['value']];
                    }

                    $provider = $commServed->get('field_provider');
                    $treeNode['providers'][] = $provider['value'];
                }
            }
            $tree['systems'][] = $treeNode;
            // Update list.
            $metaWSystems[] = [
                'id' => $wsystem->getId(),
                'name' => $label,
                'meta' => $wsystem->formatExtraJson(),
            ];
        }

        /** @var User $member */
        foreach ($team as &$member) {
            $member = [
                'value' => $member->getId(),
                'class' => User::class,
                'meta' => $member->formatExtraJson(),
            ];
        }

        // Return virtual point structure.
        return [
            'id' => $this->getId(),
            'status_code' => $this->getStatusCode(),
            'status' => $this->getStatus(),
            'version' => $this->getVersion(),
            'updated' => $this->getUpdated()->format(DateTimeInterface::W3C),
            'administrative_division' => $metaDivisions,
            'community' => $communities,
            'wsp' => $metaWsps,
            'wsystem' => $metaWSystems,
            'allowed_actions' => $this->getActionsAllowed(
                [
                    'inquiry_relations' => $tree,
                ]
            ),
            'notes' => $notes,
            'image' => $image,
            'inquiry_relations' => $tree,
            'team' => $team,
        ];
    }

    /**
     * Count form record errors and warnings.
     *
     * @param FormRecord $record
     *
     * @return array
     */
    protected function getFormRecordLogSummary(FormRecord $record)
    {
        /** @var InquiryFormLogRepositorySecured $formLogRepo */
        $formLogRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $recordErrors = $formLogRepo->count([
            'formId' => $record->getForm()->getId(),
            'recordId' => $record->getId(),
            'level' => 400,
        ]);
        $recordWarnings = $formLogRepo->count([
            'formId' => $record->getForm()->getId(),
            'recordId' => $record->getId(),
            'level' => 300,
        ]);

        return [
            'errors' => $recordErrors,
            'warnings' => $recordWarnings,
        ];
    }
}
