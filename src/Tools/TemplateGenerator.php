<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use App\Annotations\IsCountryParametric;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Tools to generate and work with parametric templates.
 */
class TemplateGenerator
{
    protected ManagerRegistry $entityManager;
    protected Reader $annotationReader;

    /**
     * TemplateGenerator constructor.
     *
     * @param ManagerRegistry $entityManager
     * @param Reader          $annotationReader
     */
    public function __construct(ManagerRegistry $entityManager, Reader $annotationReader)
    {
        $this->annotationReader = $annotationReader;
        $this->entityManager = $entityManager;
    }

    /**
     * Get parametric entities.
     *
     * @return ClassMetadata[]
     *
     * @throws \ReflectionException
     */
    public function getParamtricEntities(): array
    {
        // Find Doctrine entities.
        /** @var ClassMetadata[] $metas */
        $metas = $this->entityManager->getManager()->getMetadataFactory()->getAllMetadata();
        // Filter to get only parametric.
        $parametrics = [];
        foreach ($metas as $meta) {
            $annotations = $this->annotationReader->getClassAnnotations(new \ReflectionClass($meta->getName()));

            foreach ($annotations as $note) {
                if ($note instanceof IsCountryParametric) {
                    $parametrics[] = $meta;
                }
            }
        }

        return $parametrics;
    }

    /**
     * Get main class comment.
     *
     * @param string $className
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    public function getParametricComment(string $className): string
    {
        $rc = new ReflectionClass($className);
        $doc = $rc->getDocComment();
        if (!$doc) {
            return '';
        }

        // Note that this method to get class comments is the simplest and bad.
        $parts = explode("\n", $doc);
        if (!isset($parts[1])) {
            return '';
        }
        $doc = trim(str_replace(' *', '', $parts[1]));

        return $doc;
    }

    /**
     * Get parametric meta data by class name.
     *
     * @param string $className
     *
     * @return ClassMetadata|null
     *
     * @throws \ReflectionException
     */
    public function getParametricByClass(string $className): ClassMetadata|null
    {
        $paramtrics = $this->getParamtricEntities();
        foreach ($paramtrics as $parametric) {
            if ($parametric->getName() === $className) {
                return $parametric;
            }
        }

        return null;
    }

    /**
     * Get importable/exportable fields with type.
     *
     * @param ClassMetadata $parametric
     *
     * @return array
     */
    public function getImportableFields(ClassMetadata $parametric): array
    {
        $resp = [];
        foreach ($parametric->getFieldNames() as $fieldName) {
            $type = $parametric->fieldMappings[$fieldName]['type'];
            if ('ulid' !== $type) {
                $resp[$fieldName] = $type;
            }
        }
        foreach ($parametric->getAssociationMappings() as $associationMapping) {
            if ('App\Entity\Country' !== $associationMapping['targetEntity']) {
                if ($associationMapping['isOwningSide']) {
                    $resp[$associationMapping['fieldName']] = $associationMapping['targetEntity'];
                }
            }
        }

        return $resp;
    }

    /**
     * Get class name only.
     *
     * @param string $name
     *
     * @return string
     */
    public static function getClassnameOnly(string $name): string
    {
        return substr(strrchr($name, "\\"), 1);
    }
}
