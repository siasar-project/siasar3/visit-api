<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

/**
 * Wraps __toString in a trait to avoid some fatals.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
trait ToStringTrait
{

    /**
     * Implements the magic __toString() method.
     *
     * @return string
     */
    public function __toString(): string
    {
        try {
            return (string) $this->render();
        } catch (\Exception $e) {
            // User errors in __toString() methods are considered fatal in the Drupal
            // error handler.
            trigger_error(get_class($e).' thrown while calling __toString on a '.get_class($this).' object in '.$e->getFile().' on line '.$e->getLine().': '.$e->getMessage(), E_USER_ERROR);
            // In case we are using another error handler that did not fatal on the
            // E_USER_ERROR, we terminate execution. However, for test purposes allow
            // a return value.
            return $this->wrappedDie();
        }
    }

    /**
     * Renders the object as a string.
     *
     * @return string|object
     *   The rendered string or an object implementing __toString().
     */
    abstract public function render();

    /**
     * For test purposes, wrap die() in an overridable method.
     */
    protected function wrappedDie(): void
    {
        die();
    }
}
