<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

use Symfony\Component\Uid\Ulid;

/**
 * General porpoise tools.
 */
class Tools
{

    /**
     * Format any ULID string to Rfc4122.
     *
     * @param string $id
     *
     * @return string
     */
    public static function ulidToRfc4122(string $id): string
    {
        $ulid = new Ulid($id);

        return $ulid->toRfc4122();
    }

    /**
     * Format a PHP ini value to bytes.
     *
     * @param $val
     *
     * @return int
     *
     * @see https://stackoverflow.com/a/55154826/6385708
     */
    public static function phpIniToBytes($val): int
    {
        $val = trim($val);
        if (empty($val)) {
            return 0;
        }
        preg_match('#([0-9]+)[\s]*([a-z]+)#i', $val, $matches);
        $last = '';
        if (isset($matches[2])) {
            $last = $matches[2];
        }
        if (isset($matches[1])) {
            $val = (int) $matches[1];
        }
        switch (strtolower($last)) {
            case 'g':
                // Same option.
            case 'gb':
                $val *= 1024;
                // Accumulative options.
            case 'm':
                // Same option.
            case 'mb':
                $val *= 1024;
                // Accumulative options.
            case 'k':
                // Same option.
            case 'kb':
                $val *= 1024;
        }

        return (int) $val;
    }

    /**
     * Format bytes to string.
     *
     * @param $bytes
     *
     * @return string
     *
     * @see https://www.php.net/manual/en/function.number-format.php#72969
     */
    public static function formatBytes($bytes): string
    {
        $units = ["B", "KB", "MB", "GB", "TB", "PB"];
        $unitIndex = 0;
        while ($bytes >= 1024) {
            $unitIndex++;
            $bytes = $bytes/1024;
        }

        return number_format($bytes, ($unitIndex ? 2 : 0), ",", ".")." ".$units[$unitIndex];
    }
}
