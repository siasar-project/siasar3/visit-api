<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tools;

/**
 * In memory session handler.
 *
 * Intended to be used in console commands where the user, or any public agent,
 * can't change sessions ID.
 *
 * @see https://www.php.net/manual/en/class.sessionhandlerinterface.php
 *
 * CAUTION: For brevity, this example omits input validation. However, the $id
 * parameters are actually user supplied values which require proper
 * validation/sanitization to avoid vulnerabilities, such as path traversal
 * issues. So do not use this example unmodified in production environments.
 */
class VulnerableInMemorySessionHandler implements \SessionHandlerInterface
{
    protected array $sessions = [];

    /**
     * @inheritDoc
     */
    public function open($savePath, $sessionName): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function close(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function read($id): string|false
    {
        if (isset($this->sessions[$id])) {
            return $this->sessions[$id];
        }

        return '';
    }

    /**
     * @inheritDoc
     */
    public function write($id, $data): bool
    {
        $this->sessions[$id] = $data;

        return true;
    }

    /**
     * @inheritDoc
     */
    public function destroy($id): bool
    {
        unset($this->sessions[$id]);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function gc($maxlifetime): int|false
    {
        return true;
    }
}
