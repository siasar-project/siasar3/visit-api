<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

/**
 * Add methods to delegate display messages.
 */
trait CallablePromptMessagesTrait
{

    /**
     * @var callable
     */
    protected $displayErrorCallback;
    /**
     * @var callable
     */
    protected $displayInfoCallback;
    /**
     * @var callable
     */
    protected $displayWarningCallback;
    /**
     * @var callable
     */
    protected $displayTextCallback;

    /**
     * Print an error message.
     *
     * @param string $message
     *
     * @return void
     */
    public function displayError(string $message)
    {
        $callback = $this->displayErrorCallback;
        if ($callback) {
            $callback($message);
        }
    }

    /**
     * Set external error message printer.
     *
     * @param callable $callback
     *
     * @return void
     */
    public function setErrorCallback(callable $callback)
    {
        $this->displayErrorCallback = $callback;
    }

    /**
     * Set external info message printer.
     *
     * @param callable $callback
     *
     * @return void
     */
    public function setInfoCallback(callable $callback)
    {
        $this->displayInfoCallback = $callback;
    }

    /**
     * Print an info message.
     *
     * @param string $message
     *
     * @return void
     */
    public function displayInfo(string $message)
    {
        $callback = $this->displayInfoCallback;
        if ($callback) {
            $callback($message);
        }
    }

    /**
     * Set external warning message printer.
     *
     * @param callable $callback
     *
     * @return void
     */
    public function setWarningCallback(callable $callback)
    {
        $this->displayWarningCallback = $callback;
    }

    /**
     * Print a Warning message.
     *
     * @param string $message
     *
     * @return void
     */
    public function displayWarning(string $message)
    {
        $callback = $this->displayWarningCallback;
        if ($callback) {
            $callback($message);
        }
    }

    /**
     * Set external text message printer.
     *
     * Note: This doesn't add new line character at the end.
     *
     * @param callable $callback
     *
     * @return void
     */
    public function setTextCallback(callable $callback)
    {
        $this->displayTextCallback = $callback;
    }

    /**
     * Print a Text message.
     *
     * @param string $message
     * @param bool   $newline
     *
     * @return void
     */
    public function displayText(string $message, bool $newline = false): void
    {
        $callback = $this->displayTextCallback;
        if ($callback) {
            $callback($message, $newline);
        }
    }
}
