<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

/**
 * Add country_field property to fields that require a country.
 *
 * @see self::getDefaultSettingsFromTrait
 */
trait EntityCountryReferenceFieldTrait
{

    /**
     * Get default field type settings.
     *
     * Use this method if the host class define the method 'getDefaultSettings'.
     *
     * @return array
     */
    public function getDefaultSettingsFromTrait(): array
    {
        $settings = parent::getDefaultSettings();
        $settings['country_field'] = '';

        return $settings;
    }

    /**
     * Get default field type settings.
     *
     * @return array
     */
    public function getDefaultSettings(): array
    {
        return $this->getDefaultSettingsFromTrait();
    }



    /**
     * Validate field settings.
     *
     * @return bool
     */
    public function isValidSettingsTrait(): bool
    {
        if (empty($this->getCountryFieldName())) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require a "country_reference" field in form "@form".',
                    [
                        '@id' => $this->getId(),
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }

        $countryField = $this->formManager->getFieldDefinition($this->getCountryFieldName());
        if (!$countryField) {
            throw new \Exception(
                $this->t(
                    'The field "@id" require "@country" field in form "@form", but it\'s not found.',
                    [
                        '@id' => $this->getId(),
                        '@country' => $this->getCountryFieldName(),
                        '@form' => $this->formManager->getId(),
                    ]
                )
            );
        }
        if ($countryField->isMultivalued()) {
            throw new \Exception(
                $this->t(
                    '[@type] Field "@id" can\'t use a multivalued field how country reference, "@ref".',
                    [
                        '@type' => $this->getFieldType(),
                        '@id' => $this->getId(),
                        '@ref' => $this->getCountryFieldName(),
                    ]
                )
            );
        }

        return true;
    }

    /**
     * Get the country field name.
     *
     * @return string
     */
    protected function getCountryFieldName(): string
    {
        if (isset($this->settings["settings"]["country_field"])) {
            return $this->settings["settings"]["country_field"];
        }

        return '';
    }
}
