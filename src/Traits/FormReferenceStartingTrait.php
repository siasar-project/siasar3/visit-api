<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use App\Tools\Parametric;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * Starting parametric instance.
 */
trait FormReferenceStartingTrait
{
    use GetContainerTrait;
    use StringTranslationTrait;

    /**
     * @var bool
     *
     * @Ignore
     */
    protected bool $defaultParametric = false;

    /**
     * @var bool
     *
     * @Ignore
     */
    protected bool $starting = false;

    /**
     * {@inheritDoc}
     */
    public function isDefaultParametric(): bool
    {
        return (isset($this->defaultParametric) && $this->defaultParametric);
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultParametric(bool $value): self
    {
        $this->defaultParametric = $value;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isStarting(): bool
    {
        return $this->starting;
    }

    /**
     * {@inheritDoc}
     */
    public function setStarting(bool $value): self
    {
        $this->starting = $value;

        return $this;
    }

    /**
     * @return Parametric
     *
     * @Ignore
     */
    protected static function getParametricTool(): Parametric
    {
        return self::getContainerInstance()->get('parametric_tool');
    }
}
