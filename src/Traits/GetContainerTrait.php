<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

declare(strict_types=1);

namespace App\Traits;

use App\Kernel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * Get container from kernel.
 *
 * Get in consideration that the container that we can get with this trait may
 * generate new services instances. That could generate problems with Doctrine
 * entities.
 *
 * @category Tools
 *
 * @author   Lea Haensenberger <none@null.null>
 * @author   Lukas Kahwe Smith <smith@pooteeweet.org>
 * @author   Benjamin Eberlei <kontakt@beberlei.de>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
trait GetContainerTrait
{
    /**
     * Ignore is required to allow use this trait in Entities.
     *
     * @Ignore
     */
    protected static $containers;

    /**
     * Ignore is required to allow use this trait in Entities.
     *
     * @Ignore
     */
    protected static ?KernelInterface $kernelGetContainerTrait = null;

    /**
     * This declaration is to be ignore in Normalizers.
     *
     * @Ignore
     */
    protected $containerInstance;
    /**
     * This declaration is to be ignore in Normalizers.
     *
     * @Ignore
     */
    protected $kernelContainerInstance;
    /**
     * This declaration is to be ignore in Normalizers.
     *
     * @Ignore
     */
    protected $kernelInstance;

    /**
     * Get an instance of the dependency injection container.
     *
     * @return ContainerInterface
     */
    protected static function getContainerInstance(): ContainerInterface
    {
        $environment = static::determineEnvironment();

        if (empty(static::$containers[$environment])) {
            static::$containers[$environment] = static::getKernelContainerInstance();
        }

        return static::$containers[$environment];
    }

    /**
     * Get container from Kernel.
     *
     * @return ContainerInterface
     */
    protected static function getKernelContainerInstance(): ContainerInterface
    {
        return static::getKernelInstance()->getContainer();
    }

    /**
     * Get kernel.
     *
     * If the kernel is not detected or don't have container this build a new kernel.
     *
     * @return KernelInterface|null
     */
    protected static function getKernelInstance(): ?KernelInterface
    {
        // Check that the kernel has not been booted separately (eg. with static::createClient())
        if (null === static::$kernelGetContainerTrait || null === static::$kernelGetContainerTrait->getContainer()) {
            static::$kernelGetContainerTrait = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
            static::$kernelGetContainerTrait->boot();
        }

        return static::$kernelGetContainerTrait;
    }

    /**
     * Get active app environment.
     *
     * @see KernelTestCase::createKernel()
     *
     * @return string
     */
    protected static function determineEnvironment(): string
    {
        if (isset($_ENV['APP_ENV'])) {
            return $_ENV['APP_ENV'];
        }

        if (isset($_SERVER['APP_ENV'])) {
            return $_SERVER['APP_ENV'];
        }

        return 'test';
    }
}
