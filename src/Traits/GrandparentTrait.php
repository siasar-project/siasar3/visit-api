<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

/**
 * Add method to get then grandparent class.
 *
 * @see https://www.amitmerchant.com/get-instance-grandparent-certain-class-php/
 */
trait GrandparentTrait
{
    /**
     * Get the grandparent class of current class
     *
     * @return string
     */
    private function getGrandparentClass(): string
    {
        return get_parent_class(get_parent_class(get_class()));
    }
}
