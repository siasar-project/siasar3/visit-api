<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allow a class to do manual dependency injection.
 *
 * Require that the factory or manager use this method to instantiate objects.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
interface InjectableObjectInterface
{
    /**
     * Instantiate with required dependencies injected.
     *
     * @param ContainerInterface $container Service container.
     *
     * @return $this
     */
    public static function instantiate(ContainerInterface $container): mixed;
}
