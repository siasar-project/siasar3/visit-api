<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use App\Entity\PointLog;
use App\Forms\FormReferenceEntityInterface;
use App\Security\Handlers\InquiryAccessHandler;
use App\Service\SessionService;
use App\Tools\ContextLogNormalizer;
use App\Tools\Json;
use App\Tools\TranslatableMarkup;
use Monolog\Logger;

/**
 * Logger helper functions.
 */
trait InquiryFormLoggerTrait
{
    use GetContainerTrait;

    protected ?SessionService $sessionService;
    protected InquiryAccessHandler $accessHandler;

    /**
     * System is unusable.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function emergency(string $message, array $context = array())
    {
        $this->log(Logger::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function alert(string $message, array $context = array())
    {
        $this->log(Logger::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function critical(string $message, array $context = array())
    {
        $this->log(Logger::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function error(string $message, array $context = array())
    {
        $this->log(Logger::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function warning(string $message, array $context = array())
    {
        $this->log(Logger::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function notice(string $message, array $context = array())
    {
        $this->log(Logger::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function info(string $message, array $context = array())
    {
        $this->log(Logger::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function debug(string $message, array $context = array())
    {
        $this->log(Logger::DEBUG, $message, $context);
    }

    /**
     * Add action to activity log.
     *
     * @param mixed  $level
     * @param string $msg
     * @param array  $context
     */
    protected function log(mixed $level, string $msg, array $context = []): void
    {
        $user = $this->sessionService->getUser();
        $context['user'] = 'ANONYMOUS';
        $context['user_id'] = '';
        if ($user) {
            $context['user'] = $user->getUsername();
            $context['user_id'] = $user->getId();
        }

        // todo Binary IDs are missed in this conversion. If we do not do this conversion the log fails by JSON encoding.
        $nContext = ContextLogNormalizer::normalize($context);
        $nContext = Json::decode(Json::encode($nContext));
        $this->inquiryFormLogger->log($level, $msg, $nContext);
    }
}
