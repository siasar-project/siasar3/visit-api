<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use ApiPlatform\Core\OpenApi\Model\Encoding;
use ApiPlatform\Core\OpenApi\Model\MediaType;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\Parameter;
use ApiPlatform\Core\OpenApi\Model\Response;
use ApiPlatform\Core\OpenApi\OpenApi;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * OpenApi schema helper functions.
 */
trait OpenApiSchemaTrait
{
    use GetContainerTrait;
    use StringTranslationTrait;

    /**
     * Get fail response.
     *
     * @param string $message  Error message.
     * @param bool   $withBody Add a form message body?
     *
     * @return Response
     */
    protected static function getResponseFail(string $message, bool $withBody = false): Response
    {
        $haveErrors = null;
        if ($withBody) {
            $haveErrors = new \ArrayObject(
                [
                    'application/json' => new MediaType(
                        new \ArrayObject(
                            [
                                '$ref' => '#/components/schemas/form_error',
                            ]
                        ),
                        null,
                        null,
                        new Encoding('application/json')
                    ),
                ],
            );
        }

        return new Response($message, $haveErrors);
    }

    /**
     * Get ID parameter definition.
     *
     * @return Parameter
     */
    protected static function getIdParam(): Parameter
    {
        return new Parameter(
            'id',
            'path',
            self::t('Record ID')->render(),
            true,
            false,
            false,
            ["type" => "string"],
            'simple'
        );
    }

    /**
     * Get system settings.
     *
     * @return ParameterBagInterface
     */
    protected static function getSystemSettingsService(): ParameterBagInterface
    {
        return static::getContainerInstance()->getParameterBag();
    }

    /**
     * Get page parameter.
     *
     * @return Parameter
     */
    protected static function getPageParameter(): Parameter
    {
        return new Parameter(
            self::getSystemSettingsService()->get('api_platform.collection.pagination.page_parameter_name'),
            'query',
            self::t(
                'The collection page number. We will get @cnt items per page.',
                ['@cnt' => self::getSystemSettingsService()->get('api_platform.collection.pagination.items_per_page')]
            )->render(),
            false,
            false,
            false,
            [
                "type" => "integer",
                "default" => 1,
            ],
            'simple'
        );
    }

    /**
     * Get order parameter.
     *
     * @param string $paramName
     *
     * @return Parameter
     */
    protected static function getOrderParameter(string $paramName): Parameter
    {
        return new Parameter(
            "order[$paramName]",
            'query',
            '',
            false,
            false,
            true,
            [
                "type" => "string",
                "enum" => ['asc', 'desc'],
            ],
            'simple'
        );
    }

    /**
     * Get filter parameter.
     *
     * @param string $id
     * @param bool   $multiple
     * @param bool   $required
     *
     * @return Parameter
     */
    protected static function getFilterParameter(string $id, bool $multiple = false, bool $required = false): Parameter
    {
        $type = ['type' => 'string'];
        if ($multiple) {
            $type = [
                'type' => 'array',
                'items' => ['type' => 'string'],
            ];
        }

        return new Parameter(
            $id,
            'query',
            '',
            $required,
            false,
            !$required,
            $type,
            'simple'
        );
    }

    /**
     * Get parameter schema by field.
     *
     * @param FieldTypeInterface $field
     *
     * @return array
     */
    protected function getParamSchemaByField(FieldTypeInterface $field): array
    {
        switch ($field->getFieldType()) {
            case 'select':
                return [
                    "type" => "string",
                    "enum" => array_keys($field->getSettings()["settings"]["options"]),
                ];
            case 'boolean':
                return [
                    "type" => "boolean",
                ];
            case 'decimal':
            case 'integer':
                return [
                    "type" => "number",
                ];
            case 'date':
                return [
                    "type" => "date",
                ];
            default:
                return [
                    "type" => "string",
                ];
        }
    }

    /**
     * Add default response codes.
     *
     * @param Operation $op
     *
     * @return void
     */
    protected function addDefaultResponseCodes(Operation $op): void
    {
        // Add default responses.
        $createdResponse = new Response('Forbidden');
        $op->addResponse($createdResponse, 403);
        $createdResponse = new Response('Unauthorized');
        $op->addResponse($createdResponse, 401);
        $createdResponse = new Response('Service Unavailable');
        $op->addResponse($createdResponse, 503);
    }

    /**
     * Recursive ArrayObjecto to array transformer.
     *
     * @param \ArrayObject $object Subject.
     *
     * @return array
     */
    protected function arrayObjectToArray(\ArrayObject $object): array
    {
        $resp = (array) $object;
        foreach ($resp as $key => $value) {
            if ($value instanceof \ArrayObject) {
                $resp[$key] = $this->arrayObjectToArray($value);
            } elseif (is_array($value)) {
                foreach ($value as $vKey => $vValue) {
                    if ($vValue instanceof \ArrayObject) {
                        $resp[$key][$vKey] = $this->arrayObjectToArray($vValue);
                    } else {
                        $resp[$key][$vKey] = $vValue;
                    }
                }
            } else {
                $resp[$key] = $value;
            }
        }

        return $resp;
    }

    /**
     * Add a new schema to the OpenAPI definition.
     *
     * @param OpenApi $openApi
     * @param string  $key
     * @param array   $value
     * @param bool    $duplicatedException If true throw exception when duplicated key, else ignore the new value.
     *
     * @return void
     *
     * @throws \Exception
     */
    protected static function addComponentSchema(OpenApi $openApi, string $key, array $value, bool $duplicatedException = true): void
    {
        $schemas = $openApi->getComponents()->getSchemas();
        $duplicated = $schemas->offsetExists($key);
        if ($duplicated && $duplicatedException) {
            throw new \Exception(sprintf('Duplicated OpenAPI schema key: %s', $key));
        }
        if (!$duplicated) {
            $openApi->getComponents()->getSchemas()->offsetSet($key, $value);
        }
    }
}
