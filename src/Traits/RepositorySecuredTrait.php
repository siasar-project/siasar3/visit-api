<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Security\Handlers\DoctrineAccessHandler;
use App\Service\SessionService;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\ORMException;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\Value;

/**
 * Class that use this trait must inherit from a repository class.
 *
 * And implements the constructor to init accessHandler.
 *
 * @template T
 */
trait RepositorySecuredTrait
{
    use GetContainerTrait;
    use StringTranslationTrait;

    protected DoctrineAccessHandler $accessHandler;
    protected ?SessionService $sessionService;
    protected IriConverterInterface $iriConverter;

    /**
     * Save entity.
     *
     * @param Object $entity
     *
     * @throws ORMException
     * @throws ORMException
     */
    public function save(Object $entity): void
    {
        // Get user session.
        $user = $this->accessHandler->getCurrentUser();
        // Validate create permission.
        $this->accessHandler->checkEntityAccess($user, DoctrineAccessHandler::ACCESS_ACTION_CREATE, ['country' => $user->getCountry()]);
        // Insert.
        parent::save($entity);
    }

    /**
     * Remove entity.
     *
     * @param Object $entity
     *
     * @throws ORMException
     */
    public function remove(Object $entity): void
    {
        // Get user session.
        $user = $this->accessHandler->getCurrentUser();
        // Validate create permission.
        $this->accessHandler->checkEntityAccess($user, DoctrineAccessHandler::ACCESS_ACTION_DELETE, ['country' => $user->getCountry()]);
        // Remove.
        parent::remove($entity);
    }

    /**
     * @param mixed $id
     * @param null  $lockMode
     * @param null  $lockVersion
     *
     * @return ?T
     */
    public function find($id, $lockMode = null, $lockVersion = null): ?Object
    {
        $resp = current($this->findBy(['id' => $id]));

        return (!is_bool($resp) ? $resp : null );
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return ?T
     */
    public function findOneBy(array $criteria, array $orderBy = null): ?Object
    {
        $resp = current($this->findBy($criteria, $orderBy, 1));

        return (!is_bool($resp) ? $resp : null );
    }

    /**
     * @return T[]
     */
    public function findAll(): array
    {
        return $this->findBy([]);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return T[]
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return parent::matching($this->getMatchingCriteria($criteria, $orderBy, $limit, $offset))->getValues();
    }

    /**
     * @param array $criteria
     *
     * @return int
     */
    public function count(array $criteria): int
    {
        return parent::matching($this->getMatchingCriteria($criteria, [], null, null))->count();
    }

    /**
     * Get current session user.
     *
     * If not session started throw exception.
     *
     * @return UserInterface
     *
     * @throws \Exception
     */
    public function getCurrentUser(): UserInterface
    {
        $user = $this->sessionService->getUser();
        if (!$user) {
            throw new \Exception(
                $this->t(
                    '[@class] This entity require user session.',
                    ['@class' => get_class($this)]
                )
            );
        }

        return $user;
    }

    /**
     * Build the matching criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param            $limit
     * @param            $offset
     *
     * @return Criteria
     *
     * @throws \Exception
     */
    protected function getMatchingCriteria(array $criteria, array $orderBy = null, $limit = null, $offset = null): Criteria
    {
        $user = $this->accessHandler->getCurrentUser();
        $this->accessHandler->checkEntityAccess($user, DoctrineAccessHandler::ACCESS_ACTION_READ, ['country' => $user->getCountry()]);

        $expr = Criteria::expr();
        $matchCriteria = Criteria::create();

        foreach ($criteria as $field => $equals) {
            try {
                $associationMapping = $this->_class->getAssociationMapping($field);
            } catch (\Exception $e) {
                $associationMapping = null;
            }
            if ($associationMapping) {
                if (is_null($equals)) {
                    $matchCriteria->andWhere($expr->isNull($field));
                } else {
                    // TODO: Must this repository be secured?
                    $otherRepo = $this->_em->getRepository($associationMapping["targetEntity"]);
                    if (is_array($equals)) {
                        $op = key($equals);
                        $values = [];
                        foreach ($equals[$op] as $value) {
                            $other = $otherRepo->find($value);
                            if ($other) {
                                $values[] = $other->getId();
                            }
                        }
                        $matchCriteria->andWhere(new Comparison($field, strtoupper($op), new Value($values)));
                    } else {
                        $other = $otherRepo->find($equals);
                        $matchCriteria->andWhere($expr->eq($field, $other));
                    }
                }
            } else {
                if (is_array($equals)) {
                    $op = key($equals);
                    $matchCriteria->andWhere(new Comparison($field, strtoupper($op), new Value(current($equals))));
                } else {
                    $matchCriteria->andWhere($expr->eq($field, $equals));
                }
            }
        }
        $matchCriteria->orderBy(!is_null($orderBy) ? $orderBy : []);
        $matchCriteria->setFirstResult(is_null($offset) ? 0 : $offset);
        if (!is_null($limit)) {
            $matchCriteria->setMaxResults($limit);
        }

        return  $this->accessHandler->updateMatchingCriteria($matchCriteria);
    }
}
