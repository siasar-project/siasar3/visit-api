<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use App\Tools\Parametric;

/**
 * Extended service entity repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
trait ServiceEntityRepositoryTrait
{
    use GetContainerTrait;

    /**
     * Save entity.
     *
     * @param Object $entity
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function save(Object $entity): void
    {
        $this->_em->persist($entity);
    }

    /**
     * Save entity and flush it.
     *
     * @param Object $entity
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveNow(Object $entity): void
    {
        $this->save($entity);
        $this->_em->flush();
    }

    /**
     * Remove entity.
     *
     * @param Object $entity
     *
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(Object $entity): void
    {
        $this->_em->remove($entity);
    }

    /**
     * Remove entity and flush it.
     *
     * @param Object $entity
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeNow(Object $entity): void
    {
        $this->remove($entity);
        $this->_em->flush();
    }

    /**
     * @return Parametric
     */
    protected static function getParametricTool(): Parametric
    {
        return self::getContainerInstance()->get('parametric_tool');
    }
}
