<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Traits;

use App\Repository\LocaleTargetRepository;
use App\Tools\PluralTranslatableMarkup;
use App\Tools\TranslatableMarkup;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * Translation trait.
 *
 * Using this trait will add t() method to the class. These must be
 * used for every translatable string.
 * This allows string extractor tools to find translatable strings.
 *
 * If the class is capable of injecting services from the container, it should
 * inject the 'LocaleTargetRepository::class' service and assign it to
 * $this->stringTranslation.
 *
 * @see container
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
trait StringTranslationTrait
{

    /**
     * The string translation service.
     *
     * Ignore is required to allow use this trait in Entities.
     *
     * @Ignore
     */
    protected LocaleTargetRepository $stringTranslation;

    /**
     * Translates a string to the current language or to a given language.
     *
     * See \Drupal\Core\StringTranslation\TranslatableMarkup::__construct() for
     * important security information and usage guidelines.
     *
     * In order for strings to be localized, make them available in one of the
     * ways supported by the
     * @link https://www.drupal.org/node/322729 Localization API #endlink. When
     * possible, use the \Drupal\Core\StringTranslation\StringTranslationTrait
     * $this->t(). Otherwise create a new
     * \Drupal\Core\StringTranslation\TranslatableMarkup object.
     *
     * @param string $string
     *   A string containing the English text to translate.
     * @param array  $args
     *   (optional) An associative array of replacements to make after
     *   translation. Based on the first character of the key, the value is
     *   escaped and/or themed. See
     *   \Drupal\Component\Render\FormattableMarkup::placeholderFormat() for
     *   details.
     * @param array  $options
     *   (optional) An associative array of additional options, with the following
     *   elements:
     *   - 'langcode' (defaults to the current language): A language code, to
     *     translate to a language other than what is used to display the page.
     *   - 'context' (defaults to the empty context): The context the source
     *     string belongs to. See the
     * @link i18n Internationalization topic #endlink for more information
     *     about string contexts.
     *
     * @return TranslatableMarkup
     *   An object that, when cast to a string, returns the translated string.
     *
     * @see  \Drupal\Component\Render\FormattableMarkup::placeholderFormat()
     * @see  \Drupal\Core\StringTranslation\TranslatableMarkup::__construct()
     *
     * @ingroup sanitization
     */
    protected static function t(string $string, array $args = [], array $options = []): TranslatableMarkup
    {
        return new TranslatableMarkup($string, $args, $options);
    }

    /**
     * Formats a string containing a count of items.
     *
     * @param int    $count    Amount.
     * @param string $singular Singular source.
     * @param string $plural   Plural source.
     * @param array  $args     Arguments.
     * @param array  $options  Options.
     *
     * @return PluralTranslatableMarkup
     *
     * @see \Drupal\Core\StringTranslation\TranslationInterface::formatPlural()
     */
    protected function formatPlural($count, $singular, $plural, array $args = [], array $options = [])
    {
        return new PluralTranslatableMarkup($count, $singular, $plural, $args, $options);
    }

    /**
     * Returns the number of plurals supported by a given language.
     *
     * @see \Drupal\locale\PluralFormulaInterface::getNumberOfPlurals()
     */
//    protected function getNumberOfPlurals($langcode = NULL)
//    {
//        if (\Drupal::hasService('locale.plural.formula')) {
//            return \Drupal::service('locale.plural.formula')->getNumberOfPlurals($langcode);
//        }
//        // We assume 2 plurals if Locale's services are not available.
//        return 2;
//    }
}
