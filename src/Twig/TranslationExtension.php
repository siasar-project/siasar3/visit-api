<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Twig;

use App\Tools\TranslatableMarkup;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig translation extension.
 *
 * Usage example: {{ "You signed up as @name with the following email:"|t({"@name":username}) }}
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class TranslationExtension extends AbstractExtension
{
    /**
     * Get new filters.
     *
     * @return TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('t', [$this, 'translate']),
        ];
    }

    /**
     * Translation in twig.
     *
     * @param string $string  String to translate, must be in english.
     * @param array  $args    Arguments.
     * @param array  $options Options.
     *
     * @return TranslatableMarkup
     */
    public function translate(string $string, array $args = [], array $options = []): TranslatableMarkup
    {
        return new TranslatableMarkup($string, $args, $options);
    }
}
