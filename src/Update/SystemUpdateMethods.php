<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Update;

use App\Entity\AdministrativeDivision;
use App\Entity\Material;
use App\Forms\FormManagerInterface;
use App\Repository\MaterialRepository;
use App\Traits\GetContainerTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * System updates.
 *
 * To add a new update action add a new method named updateN, where N is the
 * updated version.
 *
 * The first one is 0, the next 1, etc...
 *
 * System will remember last executed method and the next time will execute
 * last + 1.
 *
 * Each update must verify that the changes are required, and it's not been
 * applied before in the system.
 */
class SystemUpdateMethods
{
    use GetContainerTrait;

    protected Connection $connection;
    protected Application $application;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->application = self::getContainerInstance()->get('console.application');
    }

//    /**
//     * Example description.
//     *
//     * @param SymfonyStyle|null $io
//     *
//     * @return void
//     */
//    public function update0(SymfonyStyle $io = null): void
//    {
//        $io->text('Example description executed.');
//    }

    /**
     * Update currency field database columns to 18 & 6 digits.
     *
     * @param SymfonyStyle|null $io
     *
     * @return void
     */
    public function update0(SymfonyStyle $io = null): void
    {
        $formFactory = static::getContainerInstance()->get('form_factory');
        $forms = $formFactory->findAll();
        $io->progressStart(count($forms));
        /** @var FormManagerInterface $form */
        foreach ($forms as $form) {
            $fields = $form->getFields();
            foreach ($fields as $field) {
                if ('currency' === $field->getFieldType()) {
                    if ($field->isMultivalued()) {
                        $tableName = $field->getFieldTableName();
                    } else {
                        $tableName = $form->getTableName();
                    }
                    $ddl = sprintf(
                        'alter table %s MODIFY COLUMN %s COMMENT \'%s\'',
                        $tableName,
                        sprintf('%s_amount DECIMAL(24,6)', $field->getId()),
                        str_replace("'", '"', $field->getLabel()).' value'
                    );
                    $this->connection->executeStatement($ddl);
                }
            }
            $io->progressAdvance();
        }
        $io->progressFinish();
    }

    /**
     * Ensure that any administrative division have a branch value.
     *
     * @param SymfonyStyle|null $io
     *
     * @return void
     */
    public function update1(SymfonyStyle $io = null): void
    {
        /** @var ManagerRegistry $entityManager */
        $entityManager = static::getContainerInstance()->get('doctrine');
        $divisionRepo = $entityManager->getRepository(AdministrativeDivision::class);

        $total = $divisionRepo->count([]);

        // Do a paginated load to update each administrative division.
        $io->progressStart($total);
        $page = 0;
        $pageSize = 10000;
        $divs = [];
        do {
            unset($divs);
            $entityManager->getManager()->clear();
            $divs = $divisionRepo->findBy([], [], $pageSize, $page);
            /** @var AdministrativeDivision $div */
            foreach ($divs as $div) {
                $div->updateInternals();
                $divisionRepo->save($div);
                $io->progressAdvance();
            }
            $entityManager->getManager()->flush();
            $page += $pageSize;
        } while (!empty($divs));
        $io->progressFinish();
    }



    /**
     * Fix material parametric values and add unique constrain.
     *
     * @param SymfonyStyle|null $io
     *
     * @return void
     */
    public function update2(SymfonyStyle $io = null): void
    {
        $activeCountries = [];
        // Get all materials and fix keyIndex coping from type.
        /** @var Registry $database */
        $database = static::getContainerInstance()->get('doctrine');
        /** @var MaterialRepository $materialRepo */
        $materialRepo = $database->getRepository(Material::class);
        $materials = $materialRepo->findAll();
        $io->progressStart(count($materials));
        foreach ($materials as $material) {
            $activeCountries[$material->getCountry()->getId()] = true;
            // If is the current default value a cross icon at the end.
            if ('Other' === $material->getType() && empty($material->getKeyIndex())) {
                $material->setType('⚠ '.$material->getType());
            }
            $material->setKeyIndex($material->getType());
            // Force update.
            $material->setStarting(true);
            $materialRepo->save($material);
            $io->progressAdvance();
        }
        $database->getManager()->flush();
        $io->progressFinish();
        // Add database index.
        $conn = $database->getConnection();
        $sql = 'CREATE UNIQUE INDEX unique_key ON material (key_index, country_code)';
        try {
            $conn->exec($sql);
        } catch (\Exception $e) {
            $io->warning('There was an error creating the unique index: '.$e->getMessage());
        }
        // Update default materials.
        $command = $this->application->find('doctrine:parametric:default');
        foreach ($activeCountries as $countryCode => $activeCountry) {
            $commandInput = new ArrayInput([
                'command' => 'doctrine:parametric:default',
                'country' => $countryCode,
            ]);

            $returnCode = $command->run($commandInput, $io);
        }
    }
}
