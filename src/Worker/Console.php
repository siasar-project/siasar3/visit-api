<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Worker;

use Dtc\QueueBundle\Model\Worker;
use Exception;

/**
 * Execute console command how task worker.
 */
class Console extends Worker
{
    /**
     * Execute an console command.
     *
     * @param string $command    Console command to execute.
     * @param mixed  $parameters Command parameters.
     *
     * @return void
     *
     * @throws Exception
     */
    public function execute(string $command, mixed $parameters)
    {
        $output = '';
        $respCode = 0;
        $cmd = sprintf('bin/console %s %s', $command, $parameters);
        exec($cmd, $output, $respCode);
        if (0 !== $respCode) {
            throw new Exception(implode("\n", $output));
        }
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return 'Console';
    }
}
