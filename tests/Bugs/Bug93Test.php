<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Bugs;

use App\Entity\Country;
use App\Forms\FormFactory;
use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\InitTestEnvironmentTrait;
use Doctrine\ORM\EntityManager;
use App\Forms\FieldTypes\FieldTypeManager;
use phpDocumentor\Reflection\Types\Static_;

/**
 * Tests to fix bugs.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class Bug93Test extends ApiTestBase
{
    use InitTestEnvironmentTrait;

    protected string $adminUsername;
    protected string $adminPassword;
    protected ?FormFactory $formFactory;
    protected ?FieldTypeManager $fieldTypeManager;
    protected ?EntityManager $em;

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->endpoint = '/api/v1';
        $this->formFactory = self::$container->get('form_factory');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->em = self::$container->get('doctrine.orm.entity_manager');
        $this->doctrine = self::$container->get('database_connection');

        $configuration = $this->em->getRepository('App\Entity\Configuration');
        $this->initConfigurations(
            $configuration,
            [
                'form.school' => dirname(__FILE__).'/../assets/form.school93.yml',
                'form.school.contact' => dirname(__FILE__).'/../assets/form.school93.contact.yml',
            ]
        );
        $form = $this->formFactory->create('form.school.contact');
        $form->install();
        $form = $this->formFactory->create('form.school');
        $form->install();
    }

    /**
     * Test that fix https://gitlab.com/siasar3/visit-api/-/issues/93.
     */
    public function test93UlidField():void
    {
        // Set user session.
        $this->loginUser('Pedro');
        $form = $this->formFactory->find('form.school.contact');
        $countryRepo = $this->em->getRepository(Country::class);
        $schoolContactId = $form->insert(['field_country' => $countryRepo->find('hn')]);
        // Build query.
        $json = <<<EOF
{
	"field_country": {
		"value": "hn",
		"class": "App\\\\Entity\\\\Country"
	},
	"field_region": {
		"value": "01FFFBVPS6K583BY7QCEK1HSZF",
		"class": "App\\\\Entity\\\\AdministrativeDivision"
	},
	"field_reference": "field_reference",
	"field_questionnaire_date": {
		"value": "20210923 12:00",
		"timezone": "Europe/Madrid"
	},
	"field_questionnaire_interviewer": {
		"value": "01FHX4RK866FRCDAYYYCZKVFXJ",
		"class": "App\\\\Entity\\\\User"
	},
	"field_questionnaire_contacts": [{
		"value": "$schoolContactId",
		"form": "form.school.contact"
	}],
	"field_location_lat": "2",
	"field_location_lon": "12",
	"field_location_alt": {
		"value": "100",
        "unit": "metre"
	},
	"field_school_name": "school name",
	"field_school_code": "school code",
	"field_school_type": ["3"],
	"field_school_type_other": [""],
	"field_staff_total_number_of_women": "12",
	"field_staff_total_number_of_men": "12",
	"field_student_total_number_of_female": "12",
	"field_student_total_number_of_male": "12",
	"field_rural_communities_served": {
		"value": "01FFFBVPS6K583BY7QCEK1HSZF",
		"class": "App\\\\Entity\\\\AdministrativeDivision"
	},
	"field_national_classification": "national classification",
	"field_school_observations": "observations ..",
	"field_have_water_supply_system": ["2"],
	"field_name_water_supply_system": ["name of system that supplies the school"],
	"field_functioning_of_water_system": "B",
	"field_water_for_drinking": false,
	"field_main_source_for_drinking": "5",
	"field_reasons_not_using_water_system": "3",
	"field_reasons_not_using_water_system_other": "",
	"field_main_source_is_water_available_at_school": false,
	"field_school_have_toilets": true,
	"field_type_toilets": "1",
	"field_girls_only_toilets_total": "12",
	"field_girls_only_toilets_usable": "12",
	"field_boys_only_toilets_total": "12",
	"field_boys_only_toilets_usable": "12",
	"field_common_use_toilets_total": "12",
	"field_common_use_toilets_usable": "12",
	"field_how_clean_student_toilets": "2",
	"field_limited_mobility_vision_toilet": true,
	"field_handwashing_facilities": true,
	"field_handwashing_facilities_soap_water": "2",
	"field_menstrual_facilities_soap_water": "1",
	"field_menstrual_covered_bins": true
}
EOF;
        parent::$admin->request(
            'POST',
            $this->endpoint.'/form/data/form.schools',
            [],
            [],
            [],
            $json
        );
        // Validate.
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(201, $response->getStatusCode(), print_r($responseData, true));
    }
}
