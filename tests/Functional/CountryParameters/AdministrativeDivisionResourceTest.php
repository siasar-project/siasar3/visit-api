<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Entity\AdministrativeDivision;
use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tools\Json;

/**
 * test administrative_divisions endpoint
 */
class AdministrativeDivisionResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "MAO",
    ];

    /**
     * Id should be string ulid
     */
    public function testIdIsUlid()
    {
        $programIntervention = new AdministrativeDivision();

        $id = $programIntervention->getId();

        $this->assertIsString($id, "the Id must be a ulid string");
    }

    /**
     * testing administrative_division listing
     */
    public function testAdministrativeDivisionListing()
    {
        self::$admin->request('GET', '/api/v1/administrative_divisions');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing administrative_division country listing
     */
    public function testAdministrativeDivisionCountryListing()
    {
        // Login.
        $session = $this->loginRest('SectorialValidatorEs');

        // List my country only divisions, without using any filter.
        $session->request('GET', '/api/v1/administrative_divisions');
        $responseData = $session->getResponse();
        $this->assertEquals(200, $responseData->getStatusCode());

        $data = json_decode($responseData->getContent(), JSON_FORCE_OBJECT);
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/es', $item['country']);
        }
    }

    /**
     * testing administrative_division country roots listing.
     */
    public function testAdministrativeDivisionCountryRootsListing()
    {
        $query = [
            'page' => 1,
            'country' => '/api/v1/countries/hn',
            'parent' => 'null',
        ];
        self::$admin->request('GET', '/api/v1/administrative_divisions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), JSON_FORCE_OBJECT);
        $this->assertTrue(count($data) > 0);
        foreach ($data as $admDiv) {
            $this->assertEquals('/api/v1/countries/hn', $admDiv["country"]);
            $this->assertNull($admDiv["parent"]);
        }
    }

    /**
     * testing administrative_division children listing.
     */
    public function testAdministrativeDivisionChildrenListing()
    {
        $query = [
            'page' => 1,
            'parent' => '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF',
        ];
        self::$admin->request('GET', '/api/v1/administrative_divisions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), JSON_FORCE_OBJECT);
        $this->assertTrue(count($data) > 0);
        foreach ($data as $admDiv) {
            $this->assertEquals('/api/v1/countries/hn', $admDiv["country"]);
            $this->assertEquals('/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF', $admDiv["parent"]);
        }
    }

    /**
     * testing administrative_division sort listing.
     */
    public function testAdministrativeDivisionSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
            'parent' => '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF',
        ];
        self::$admin->request('GET', '/api/v1/administrative_divisions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), JSON_FORCE_OBJECT);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
            $this->assertEquals('/api/v1/countries/hn', $admDiv["country"]);
            $this->assertEquals('/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF', $admDiv["parent"]);
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
            'parent' => '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF',
        ];
        self::$admin->request('GET', '/api/v1/administrative_divisions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), JSON_FORCE_OBJECT);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing administrative_division paginated listing.
     */
    public function testAdministrativeDivisionPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/administrative_divisions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), JSON_FORCE_OBJECT);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing administrative_division creation
     */
    public function testAdministrativeDivisionPosting()
    {
        self::$admin->request('POST', '/api/v1/administrative_divisions', [], [], [], \json_encode($this->payload));

        $response = self::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing administrative_division read
     */
    public function testAdministrativeDivisionShowing()
    {
        // Without parent.
        self::$admin->request('GET', '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = (new Json())->decode($response->getContent());
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RF', $data['id']);
        $this->assertEquals('/api/v1/countries/hn', $data['country']);
        $this->assertEquals('ATLANTIDA', $data['name']);
//        $this->assertIsNumeric($data['administrativeDivisions']);
//        $this->assertEquals(2, $data['administrativeDivisions']);
        $this->assertEquals('LEVEL1-CODE', $data['code']);
        $this->assertNull($data['parent']);

        // With parent.
        self::$admin->request('GET', '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RG');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = (new Json())->decode($response->getContent());
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RG', $data['id']);
        $this->assertEquals('/api/v1/countries/hn', $data['country']);
        $this->assertEquals('La Ceiba', $data['name']);
//        $this->assertIsNumeric($data['administrativeDivisions']);
//        $this->assertEquals(2, $data['administrativeDivisions']);
        $this->assertEquals('LEVEL2-CODE', $data['code']);
        $this->assertEquals('/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF', $data['parent']);
    }

    /**
     * testing administrative_division edition
     */
    public function testAdministrativeDivisionPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));

        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing administrative_division deletion
     */
    public function testAdministrativeDivisionExceptionOnDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RF');
        $response = self::$admin->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $data = (new Json())->decode($response->getContent());
        $this->assertEquals('Cannot delete a administrative division with children, it have 2.', $data["detail"]);
    }

    /**
     * Test to delete a referenced administrative_division.
     */
    public function testAdministrativeDivisionDeleteInUseFail()
    {
        self::$admin->request('DELETE', '/api/v1/administrative_divisions/01FG3SCJTNG84009ZVPNHT8BPJ');
        $response = self::$admin->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
        $data = (new Json())->decode($response->getContent());
        $this->assertEquals('This administrative division is in use by other entity, maybe User.', $data["detail"]);
    }

    /**
     * Test to delete a referenced administrative_division.
     */
    public function testAdministrativeDivisionDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6');
        $response = self::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * testing administrative_division deletion by user
     */
    public function testAdministrativeDivisionUserDeleting()
    {
        // Login.
        $session = $this->loginRest('Marta');

        $session->request('DELETE', '/api/v1/administrative_divisions/01FFFBVPS6K583BY7QCEK1HSZF');
        $response = $session->getResponse();
        $data = (new Json())->decode($response->getContent());
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('The "delete" action require the permission "delete doctrine administrativedivision".', $data["detail"]);
    }

    /**
     * testing administrative_division over-leveling.
     */
    public function testAdministrativeDivisionExtraLevels()
    {
        $payload = [
            "country" => "/api/v1/countries/hn",
            "name" => "Too Many levels",
            "parent" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3",
            "LEVEL5-CODE",
        ];

        self::$admin->request('POST', '/api/v1/administrative_divisions', [], [], [], \json_encode($payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $data = (new Json())->decode($response->getContent());
        $this->assertEquals('No Administrative Division Type at level 5.', $data["detail"]);
    }
}
