<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test administrative_division_types endpoint
 */
class AdministrativeDivisionTypeResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Province",
      "level" => 2,
    ];

    /**
     * testing administrative_division_type listing
     */
    public function testAdministrativeDivisionTypeListing()
    {
        self::$client->request('GET', '/api/v1/administrative_division_types');
        $response = parent::$client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * testin administrative_division_type creation
     */
    public function testAdministrativeDivisionTypePosting()
    {
        self::$client->request('POST', '/api/v1/administrative_division_types', [], [], [], \json_encode($this->payload));
        $response = parent::$client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * testin administrative_division_type detection
     */
    public function testAdministrativeDivisionTypeShowing()
    {
        self::$admin->request('GET', '/api/v1/administrative_division_types/01FEKF2HY5XDSWW36K9JVFFPTT');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testin administrative_division_type edition
     */
    public function testAdministrativeDivisionTypePutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/administrative_division_types/01FEKF2HY5XDSWW36K9JVFFPTT', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(405, $response->getStatusCode());
    }

    /**
     * testin administrative_division_type detetion
     */
    public function testAdministrativeDivisionTypeDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/administrative_division_types/01FEKF2HY5XDSWW36K9JVFFPTT');
        $response = parent::$admin->getResponse();
        $this->assertEquals(405, $response->getStatusCode());
    }
}
