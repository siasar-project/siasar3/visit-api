<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test materials endpoint
 */
class BaseTestCase extends ApiTestBase
{

    protected static ?KernelBrowser $admin = null;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {

        self::$client = self::createClient();
        self::$client->setServerParameters(
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_ACCEPT' => 'application/json',
            ]
        );

        $this->loadFixtures();
        $this->endpoint = '/api/v1/users';

        $payload = [
            'username' => 'Pedro',
            'password' => '123456',
        ];

        self::$client->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$client->getResponse();
        $responseData = $this->getResponseData($response);

        self::$client->setServerParameters(
            [
                'HTTP_AUTHORIZATION' => 'Bearer '.$responseData['token'],
                'CONTENT_TYPE' => 'application/json',
                'HTTP_ACCEPT' => 'application/json',
            ]
        );
    }
}
