<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tools\Json;

/**
 * test country endpoint
 */
class CountryResourceTest extends ApiTestBase
{
    private $payload = [
      "name" => "MAO",
    ];

    /**
     * testing country listing
     */
    public function testCountryListing()
    {
        self::$admin->request('GET', '/api/v1/countries');
        $response = self::$admin->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(19, $data);
    }

    /**
     * testing country listing
     */
    public function testCountryUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/countries');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);
    }

    /**
     * testing country sort listing.
     */
    public function testCountrySortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/countries', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/countries', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['code'], $admDiv['code']);
            break;
        }
    }

    /**
     * testing country paginated listing.
     */
    public function testCountryPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/countries', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing country creation
     */
    public function testCountryPosting()
    {
        self::$admin->request('POST', '/api/v1/country', [], [], [], \json_encode($this->payload));

        $response = self::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * testing country read
     */
    public function testCountryShowing()
    {
        // Without parent.
        self::$admin->request('GET', '/api/v1/countries/hn');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertEquals('hn', $data['code']);
        $this->assertEquals('Honduras', $data['name']);
    }

    /**
     * testing country read structure.
     */
    public function testCountryGetStructure()
    {
        // Without parent.
        self::$admin->request('GET', '/api/v1/countries/hn');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $expected = [
            'id' => 'string',
            'code' => 'string',
            'name' => 'string',
            'languages' => 'array',
            // To testing, the flag isn't loaded.
            'flag' => 'null',
            'materials' => 'array',
            'distributionMaterials' => 'array',
            'communityServices' => 'array',
            'treatmentTechnologyFiltrations' => 'array',
            'treatmentTechnologySedimentations' => 'array',
            'treatmentTechnologyCoagFloccus' => 'array',
            'treatmentTechnologyAeraOxidas' => 'array',
            'treatmentTechnologyDesalinations' => 'array',
            'functionsCarriedOutTaps' => 'array',
            'functionsCarriedOutWsps' => 'array',
            'funderInterventions' => 'array',
            'typeInterventions' => 'array',
            'institutionInterventions' => 'array',
            'divisionTypes' => 'array',
            'mainOfficialLanguage' => 'array',
            'officialLanguages' => 'array',
            'mainUnitConcentration' => 'string',
            'unitConcentrations' => 'array',
            'mainUnitFlow' => 'string',
            'unitFlows' => 'array',
            'mainUnitLength' => 'string',
            'unitLengths' => 'array',
            'mainUnitVolume' => 'string',
            'unitVolumes' => 'array',
            'ethnicities' => 'array',
            'currencies' => 'array',
            'disinfectingSubstances' => 'array',
            'typologyChlorinationInstallations' => 'array',
            'geographicalScopes' => 'array',
            'typeTaps' => 'array',
            'interventionStatuses' => 'array',
            'waterQualityInterventionTypes' => 'array',
            'waterQualityEntities' => 'array',
            'typeHealthcareFacilities' => 'array',
            'programInterventions' => 'array',
            'technicalAssistanceProviders' => 'array',
            // 'administrativeDivisions' => 'array',
            'deep' => 'number',
            'formLevel' => 'array',
            'mutatebleFields' => 'array',
            'pumpTypes' => 'array',
            'defaultDiameters' => 'array',
            'specialComponents' => 'array',
        ];
        // Validate keys and data type.
        foreach ($expected as $key => $type) {
            $this->assertArrayHasKey($key, $data);
            switch ($type) {
                case 'string':
                    $this->assertIsString($data[$key]);
                    break;
                case 'number':
                    $this->assertIsNumeric($data[$key]);
                    break;
                case 'array':
                    $this->assertIsArray($data[$key]);
                    break;
            }
        }
        // Detect un-validated properties.
        $dataKeys = array_keys($data);
        foreach ($dataKeys as $key) {
            $this->assertArrayHasKey($key, $expected, 'Unexpected Country property found.');
        }
    }

    /**
     * testing country read
     */
    public function testCountryUserShowing()
    {
        $session = $this->loginRest('Marta');
        // Without parent.
        $session->request('GET', '/api/v1/countries/hn');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertEquals('hn', $data['code']);
        $this->assertEquals('Honduras', $data['name']);
    }

    /**
     * testing country edition
     */
    public function testCountryPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Country name';
        self::$admin->request('PUT', '/api/v1/countries/hn', [], [], [], \json_encode($payload));

        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertEquals('Country name', $data['name']);
    }

    /**
     * testing country edition
     */
    public function testCountryUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Country name';
        $session->request('PUT', '/api/v1/countries/hn', [], [], [], \json_encode($payload));

        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing country deletion
     */
    public function testCountryDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/countries/hn');
        $response = self::$admin->getResponse();
        $this->assertEquals(405, $response->getStatusCode());
    }
}
