<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Entity\Currency;

/**
 * test currencies endpoint
 */
class CurrencyResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Peso",
    ];

    /**
     * Id should be string ulid
     */
    public function testIdIsUlid()
    {
        $currency = new Currency();

        $id = $currency->getId();

        $this->assertTrue(is_string($id));
    }

    /**
     * testing currency listing
     */
    public function testCurrencyListing()
    {
        self::$admin->request('GET', '/api/v1/currencies');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing currency listing
     */
    public function testCurrencyUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/currencies');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing currency sort listing.
     */
    public function testCurrencySortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/currencies', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/currencies', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing currency paginated listing.
     */
    public function testCurrencyPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/currencies', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing currency creation
     */
    public function testCurrencyPosting()
    {
        self::$admin->request('POST', '/api/v1/currencies', [], [], [], \json_encode($this->payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing currency creation
     */
    public function testCurrencyUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/currencies', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing currency detection
     */
    public function testCurrencyShowing()
    {
        self::$admin->request('GET', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Dollar', $data['name']);
    }

    /**
     * testing currency detection
     */
    public function testCurrencyUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Dollar', $data['name']);
    }

    /**
     * testing currency edition
     */
    public function testCurrencyPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing currency edition
     */
    public function testCurrencyUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing currency deletion
     */
    public function testCurrencyDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF');
        $response = self::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        self::$admin->request('GET', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF');
        $response = self::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * testing currency deletion
     */
    public function testCurrencyUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());

        $session->request('GET', '/api/v1/currencies/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
}
