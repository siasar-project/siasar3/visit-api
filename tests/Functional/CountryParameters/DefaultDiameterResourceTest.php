<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\DefaultDiameter;
use App\Repository\DefaultDiameterRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test DefaultDiameter endpoint
 */
class DefaultDiameterResourceTest extends ApiTestBase
{
    /**
     * testing listing
     */
    public function testDefaultDiameterListing()
    {
        self::$admin->request('GET', '/api/v1/default_diameters');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testDefaultDiameterUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/default_diameters');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testDefaultDiameterSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'value' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/default_diameters', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'value' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/default_diameters', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testDefaultDiameterPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/default_diameters', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testDefaultDiameterPosting()
    {
        self::$admin->request('POST', '/api/v1/default_diameters', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "value" => "25.2", "unit" => "metre"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testDefaultDiameterPostingBadUnit()
    {
        self::$admin->request('POST', '/api/v1/default_diameters', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "value" => "25.2", "unit" => "pico"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testDefaultDiameterUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/default_diameters', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "value" => "25.2", "unit" => "meter"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testDefaultDiameterShowing()
    {
        self::$admin->request('GET', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('metre', $data['unit']);
        $this->assertEquals(5, $data['value']);
    }

    /**
     * testing detection
     */
    public function testDefaultDiameterUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('metre', $data['unit']);
        $this->assertEquals(5, $data['value']);
    }

    /**
     * testing edition
     */
    public function testDefaultDiameterPutting()
    {
        self::$admin->request('PUT', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "value" => "10"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testDefaultDiameterPuttingBadUnit()
    {
        self::$admin->request('PUT', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "value" => "10", "unit" => "pata"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testDefaultDiameterUserPutting()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "value" => "10"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testDefaultDiameterDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testDefaultDiameterUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/default_diameters/01FK0CDCMPHJEY58ABTQPEYC5X');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
