<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\DistributionMaterial;
use App\Repository\DistributionMaterialRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Tools\Json;

/**
 * test DistributionMaterial endpoint
 */
class DistributionMaterialResourceTest extends ApiTestBase
{
    /**
     * testing listing
     */
    public function testDistributionMaterialListing()
    {
        self::$admin->request('GET', '/api/v1/distribution_materials');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testDistributionMaterialUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/distribution_materials');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testDistributionMaterialSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'type' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/distribution_materials', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'type' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/distribution_materials', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testDistributionMaterialPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/distribution_materials', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testDistributionMaterialPosting()
    {
        $json = '{"country": "/api/v1/countries/hn", "keyIndex": "6", "type": "Ruby"}';
        self::$admin->request('POST', '/api/v1/distribution_materials', [], [], [], $json);
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testDistributionMaterialUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/distribution_materials', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "keyIndex" => "6", "type" => "Ruby"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testDistributionMaterialShowing()
    {
        self::$admin->request('GET', '/api/v1/distribution_materials/01G2PR25MW2PYV03EDE2EFE2X8');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['type']);
    }

    /**
     * testing detection
     */
    public function testDistributionMaterialUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/distribution_materials/01G2PR25MW2PYV03EDE2EFE2X8');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['type']);
    }

    /**
     * testing edition
     */
    public function testDistributionMaterialPutting()
    {
        self::$admin->request('PUT', '/api/v1/distribution_materials/01G2PR25MW2PYV03EDE2EFE2X8', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "type" => "Silver"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testDistributionMaterialUserPutting()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/distribution_materials/01G2PR25MW2PYV03EDE2EFE2X8', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "type" => "Silver"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testDistributionMaterialDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/distribution_materials/01G2PR25MW2PYV03EDE2EFE2X8');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testDistributionMaterialUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/distribution_materials/01G2PR25MW2PYV03EDE2EFE2X8');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
