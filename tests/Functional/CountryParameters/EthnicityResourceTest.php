<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test ethnicities endpoint
 */
class EthnicityResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Lenca",
    ];

    /**
     * testing ethnicity listing
     */
    public function testEthnicityListing()
    {
        self::$admin->request('GET', '/api/v1/ethnicities');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing currency listing
     */
    public function testEthnicityUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/ethnicities');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing Ethnicity sort listing.
     */
    public function testEthnicitySortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/ethnicities', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/ethnicities', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing Ethnicity paginated listing.
     */
    public function testEthnicityPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/ethnicities', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing ethnicity creation
     */
    public function testEthnicityPosting()
    {
        self::$admin->request('POST', '/api/v1/ethnicities', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing ethnicity creation
     */
    public function testEthnicityUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/ethnicities', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing ethnicity detection
     */
    public function testEthnicityShowing()
    {
        self::$admin->request('GET', '/api/v1/ethnicities/01FENEDYFA6KNAA4J459P5S0RF');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Miskito', $data['name']);
    }

    /**
     * testing ethnicity detection
     */
    public function testEthnicityUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/ethnicities/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Miskito', $data['name']);
    }

    /**
     * testing ethnicity edition
     */
    public function testEthnicityPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/ethnicities/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing ethnicity edition
     */
    public function testEthnicityUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/ethnicities/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Testing ethnicity detection
     */
    public function testEthnicityDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/ethnicities/01FENEDYFA6KNAA4J459P5S0RF');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing ethnicity detection
     */
    public function testEthnicityUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/ethnicities/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
