<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\FunctionsCarriedOutWsp;
use App\Repository\FunctionsCarriedOutWspRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Tools\Json;

/**
 * test FunctionsCarriedOutWsp endpoint
 */
class FunctionsCarriedOutWspResourceTest extends ApiTestBase
{
    /**
     * testing listing
     */
    public function testFunctionsCarriedOutWspListing()
    {
        self::$admin->request('GET', '/api/v1/functions_carried_out_wsps');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testFunctionsCarriedOutWspUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/functions_carried_out_wsps');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testFunctionsCarriedOutWspSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'type' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/functions_carried_out_wsps', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'type' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/functions_carried_out_wsps', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testFunctionsCarriedOutWspPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/functions_carried_out_wsps', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testFunctionsCarriedOutWspPosting()
    {
        $json = '{"country": "/api/v1/countries/hn", "keyIndex": "3", "type": "Ruby"}';
        self::$admin->request('POST', '/api/v1/functions_carried_out_wsps', [], [], [], $json);
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testFunctionsCarriedOutWspUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/functions_carried_out_wsps', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "keyIndex" => "6", "type" => "Ruby"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testFunctionsCarriedOutWspShowing()
    {
        self::$admin->request('GET', '/api/v1/functions_carried_out_wsps/01G3BEJ0GVNCXRJH3RJSRJHW2V');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['type']);
    }

    /**
     * testing detection
     */
    public function testFunctionsCarriedOutWspUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/functions_carried_out_wsps/01G3BEJ0GVNCXRJH3RJSRJHW2V');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['type']);
    }

    /**
     * testing edition
     */
    public function testFunctionsCarriedOutWspPutting()
    {
        self::$admin->request('PUT', '/api/v1/functions_carried_out_wsps/01G3BEJ0GVNCXRJH3RJSRJHW2V', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "type" => "Silver"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testFunctionsCarriedOutWspUserPutting()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/functions_carried_out_wsps/01G3BEJ0GVNCXRJH3RJSRJHW2V', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "type" => "Silver"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testFunctionsCarriedOutWspDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/functions_carried_out_wsps/01G3BEJ0GVNCXRJH3RJSRJHW2V');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testFunctionsCarriedOutWspUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/functions_carried_out_wsps/01G3BEJ0GVNCXRJH3RJSRJHW2V');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
