<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\InstitutionIntervention;
use App\Repository\InstitutionInterventionRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test institution_interventions endpoint
 */
class InstitutionInterventionsResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Ruby",
      "address" => "NS/NC",
      "phone" => "+1 223 555-7869",
      "contact" => "NS/NC",
    ];

    /**
     * testing listing
     */
    public function testInstitutionInterventionListing()
    {
        self::$admin->request('GET', '/api/v1/institution_interventions');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testInstitutionInterventionUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/institution_interventions');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Unable to retrieve InstitutionIntervention: The "read" action require the permission "read doctrine institutionintervention".', $data['detail']);
    }

    /**
     * testing InstitutionIntervention reader_role listing
     */
    public function testInstitutionInterventionReaderListing()
    {
        $session = $this->loginRest('Reader');
        $session->request('GET', '/api/v1/institution_interventions');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testInstitutionInterventionSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/institution_interventions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/institution_interventions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testInstitutionInterventionPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/institution_interventions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testInstitutionInterventionPosting()
    {
        self::$admin->request('POST', '/api/v1/institution_interventions', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testInstitutionInterventionUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/institution_interventions', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testInstitutionInterventionShowing()
    {
        self::$admin->request('GET', '/api/v1/institution_interventions/01FENEA89D72SCVR49R98VQ425');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('OMS', $data['name']);
    }

    /**
     * testing detection
     */
    public function testInstitutionInterventionUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/institution_interventions/01FENEA89D72SCVR49R98VQ425');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testInstitutionInterventionPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/institution_interventions/01FENEA89D72SCVR49R98VQ425', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testInstitutionInterventionUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/institution_interventions/01FENEA89D72SCVR49R98VQ425', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testInstitutionInterventionDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/institution_interventions/01FENEA89D72SCVR49R98VQ425');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testInstitutionInterventionUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/institution_interventions/01FENEA89D72SCVR49R98VQ425');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
