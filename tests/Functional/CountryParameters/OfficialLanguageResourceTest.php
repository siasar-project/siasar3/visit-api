<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use ApiPlatform\Core\Exception\InvalidArgumentException;

/**
 * test official_languages endpoint
 */
class OfficialLanguageResourceTest extends ApiTestBase
{
    protected $payload = [
      "country" => "/api/v1/countries/hn",
      "language" => "SPAN",
    ];

    /**
     * testing listing
     */
    public function testOfficialLanguagesListing()
    {
        self::$admin->request('GET', '/api/v1/official_languages');
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testOfficialLanguagesUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/official_languages');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testOfficialLanguagesSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'language' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/official_languages', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'language' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/official_languages', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testOfficialLanguagesPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/official_languages', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testOfficialLanguagesPosting()
    {
        self::$admin->request('POST', '/api/v1/official_languages', [], [], [], \json_encode($this->payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testOfficialLanguagesUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/official_languages', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testOfficialLanguagesShowing()
    {
        self::$admin->request('GET', '/api/v1/official_languages/01FH5AGXZR8H9E7EVKBA93D4W3');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Español', $data['language']);
    }

    /**
     * testing detection
     */
    public function testOfficialLanguagesUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/official_languages/01FH5AGXZR8H9E7EVKBA93D4W3');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Español', $data['language']);
    }

    /**
     * testing edition
     */
    public function testOfficialLanguagesPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/official_languages/01FH5AGXZR8H9E7EVKBA93D4W3', [], [], [], \json_encode($payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testOfficialLanguagesUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload['language'] = 'Another name';
        $session->request('PUT', '/api/v1/official_languages/01FH5AGXZR8H9E7EVKBA93D4W3', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testOfficialLanguagesDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/official_languages/01FH5AGXZR8H9E7EVKBA93D4W3');
        $response = self::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing detection
     */
    public function testOfficialLanguagesUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/official_languages/01FH5AGXZR8H9E7EVKBA93D4W3');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing country main official_language edition
     */
    public function testMainOfficialLanguagePuting()
    {
        $payload = [
            "mainOfficialLanguage" => "/api/v1/official_languages/01FH5AHS6A6NC4Q1NT6TNJ3P3G",
        ];
        self::$admin->request('PUT', '/api/v1/countries/hn', [], [], [], \json_encode($payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing country main official_language exception
     */
    public function testMainOfficialLanguageError()
    {
        $payload = [
            "mainOfficialLanguage" => "01FH5AGXZR8H9E7EVKBA93D4W3",
        ];
        self::$admin->request('PUT', '/api/v1/countries/hn', [], [], [], \json_encode($payload));
        $response = self::$admin->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }
}
