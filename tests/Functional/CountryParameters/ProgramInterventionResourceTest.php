<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Entity\ProgramIntervention;
use App\Tests\Functional\OpenApi\ApiTestBase;

/**
 * test program_interventions endpoint
 */
class ProgramInterventionResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "MAO",
    ];

    /**
     * Id should be string ulid
     */
    public function testIdIsUlid()
    {
        $programIntervention = new ProgramIntervention();

        $id = $programIntervention->getId();

        $this->assertTrue(is_string($id), "the Id must be a ulid string");
    }

    /**
     * testing listing
     */
    public function testProgramInterventionListing()
    {
        self::$admin->request('GET', '/api/v1/program_interventions');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testProgramInterventionUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/program_interventions');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testProgramInterventionSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/program_interventions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/program_interventions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testProgramInterventionPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/program_interventions', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testProgramInterventionPosting()
    {
        self::$admin->request('POST', '/api/v1/program_interventions', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testProgramInterventionUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/program_interventions', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testProgramInterventionShowing()
    {
        self::$admin->request('GET', '/api/v1/program_interventions/01FENEDYFA6KNAA4J459P5S0RF');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('MANAO', $data['name']);
    }

    /**
     * testing detection
     */
    public function testProgramInterventionUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/program_interventions/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('MANAO', $data['name']);
    }

    /**
     * testing edition
     */
    public function testProgramInterventionPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/program_interventions/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testProgramInterventionUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/program_interventions/01FENEDYFA6KNAA4J459P5S0RF', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testProgramInterventionDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/program_interventions/01FENEDYFA6KNAA4J459P5S0RF');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testProgramInterventionUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/program_interventions/01FENEDYFA6KNAA4J459P5S0RF');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
