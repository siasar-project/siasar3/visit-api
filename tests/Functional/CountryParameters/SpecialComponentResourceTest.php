<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\SpecialComponent;
use App\Repository\SpecialComponentRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test SpecialComponent endpoint
 */
class SpecialComponentResourceTest extends ApiTestBase
{
    /**
     * testing listing
     */
    public function testSpecialComponentListing()
    {
        self::$admin->request('GET', '/api/v1/special_components');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testSpecialComponentUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/special_components');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testSpecialComponentSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/special_components', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/special_components', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testSpecialComponentPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/special_components', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testSpecialComponentPosting()
    {
        self::$admin->request(
            'POST',
            '/api/v1/special_components',
            [],
            [],
            [],
            \json_encode(
                [
                    "country" => "/api/v1/countries/hn",
                    "keyIndex" => "10",
                    "name" => "Ruby",
                ]
            )
        );
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testSpecialComponentUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/special_components', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "name" => "Ruby"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testSpecialComponentShowing()
    {
        self::$admin->request('GET', '/api/v1/special_components/01FK0PBV597J0VEQEW0AKH2SMN');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Trocola', $data['name']);
    }

    /**
     * testing detection
     */
    public function testSpecialComponentUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/special_components/01FK0PBV597J0VEQEW0AKH2SMN');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Trocola', $data['name']);
    }

    /**
     * testing edition
     */
    public function testSpecialComponentPutting()
    {
        self::$admin->request('PUT', '/api/v1/special_components/01FK0PBV597J0VEQEW0AKH2SMN', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "name" => "Silver"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testSpecialComponentUserPutting()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/special_components/01FK0PBV597J0VEQEW0AKH2SMN', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "name" => "Silver"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testSpecialComponentDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/special_components/01FK0PBV597J0VEQEW0AKH2SMN');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testSpecialComponentUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/special_components/01FK0PBV597J0VEQEW0AKH2SMN');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
