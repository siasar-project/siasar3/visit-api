<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test TreatmentTechnologyAeraOxida endpoint
 */
class TreatmentTechnologyAeraOxidaResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Lenca",
    ];

    /**
     * testing TreatmentTechnologyAeraOxida listing
     */
    public function testTreatmentTechnologyAeraOxidaListing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_aera_oxidas');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing TreatmentTechnologyAeraOxida listing
     */
    public function testTreatmentTechnologyAeraOxidaUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_aera_oxidas');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing TreatmentTechnologyAeraOxida sort listing.
     */
    public function testTreatmentTechnologyAeraOxidaSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_aera_oxidas', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_aera_oxidas', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing TreatmentTechnologyAeraOxida paginated listing.
     */
    public function testTreatmentTechnologyAeraOxidaPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_aera_oxidas', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing TreatmentTechnologyAeraOxida creation
     */
    public function testTreatmentTechnologyAeraOxidaPosting()
    {
        self::$admin->request('POST', '/api/v1/treatment_technology_aera_oxidas', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyAeraOxida creation
     */
    public function testTreatmentTechnologyAeraOxidaUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/treatment_technology_aera_oxidas', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyAeraOxida detection
     */
    public function testTreatmentTechnologyAeraOxidaShowing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_aera_oxidas/01G2SWW6ZJ7H5Z4JSCMP0DC0AE');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyAeraOxida detection
     */
    public function testTreatmentTechnologyAeraOxidaUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_aera_oxidas/01G2SWW6ZJ7H5Z4JSCMP0DC0AE');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyAeraOxida edition
     */
    public function testTreatmentTechnologyAeraOxidaPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/treatment_technology_aera_oxidas/01G2SWW6ZJ7H5Z4JSCMP0DC0AE', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing TreatmentTechnologyAeraOxida edition
     */
    public function testTreatmentTechnologyAeraOxidaUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/treatment_technology_aera_oxidas/01G2SWW6ZJ7H5Z4JSCMP0DC0AE', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyAeraOxida detection
     */
    public function testTreatmentTechnologyAeraOxidaDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/treatment_technology_aera_oxidas/01G2SWW6ZJ7H5Z4JSCMP0DC0AE');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyAeraOxida detection
     */
    public function testTreatmentTechnologyAeraOxidaUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/treatment_technology_aera_oxidas/01G2SWW6ZJ7H5Z4JSCMP0DC0AE');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
