<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test TreatmentTechnologyCoagFloccu endpoint
 */
class TreatmentTechnologyCoagFloccuResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Lenca",
    ];

    /**
     * testing TreatmentTechnologyCoagFloccu listing
     */
    public function testTreatmentTechnologyCoagFloccuListing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_coag_floccus');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing TreatmentTechnologyCoagFloccu listing
     */
    public function testTreatmentTechnologyCoagFloccuUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_coag_floccus');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing TreatmentTechnologyCoagFloccu sort listing.
     */
    public function testTreatmentTechnologyCoagFloccuSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_coag_floccus', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_coag_floccus', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing TreatmentTechnologyCoagFloccu paginated listing.
     */
    public function testTreatmentTechnologyCoagFloccuPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_coag_floccus', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing TreatmentTechnologyCoagFloccu creation
     */
    public function testTreatmentTechnologyCoagFloccuPosting()
    {
        self::$admin->request('POST', '/api/v1/treatment_technology_coag_floccus', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyCoagFloccu creation
     */
    public function testTreatmentTechnologyCoagFloccuUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/treatment_technology_coag_floccus', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyCoagFloccu detection
     */
    public function testTreatmentTechnologyCoagFloccuShowing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_coag_floccus/01G2W4E3PHR0B3XE4D3F5WGAW1');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyCoagFloccu detection
     */
    public function testTreatmentTechnologyCoagFloccuUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_coag_floccus/01G2W4E3PHR0B3XE4D3F5WGAW1');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyCoagFloccu edition
     */
    public function testTreatmentTechnologyCoagFloccuPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/treatment_technology_coag_floccus/01G2W4E3PHR0B3XE4D3F5WGAW1', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing TreatmentTechnologyCoagFloccu edition
     */
    public function testTreatmentTechnologyCoagFloccuUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/treatment_technology_coag_floccus/01G2W4E3PHR0B3XE4D3F5WGAW1', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyCoagFloccu detection
     */
    public function testTreatmentTechnologyCoagFloccuDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/treatment_technology_coag_floccus/01G2W4E3PHR0B3XE4D3F5WGAW1');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyCoagFloccu detection
     */
    public function testTreatmentTechnologyCoagFloccuUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/treatment_technology_coag_floccus/01G2W4E3PHR0B3XE4D3F5WGAW1');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
