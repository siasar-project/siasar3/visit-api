<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test TreatmentTechnologyDesalination endpoint
 */
class TreatmentTechnologyDesalinationResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Lenca",
    ];

    /**
     * testing TreatmentTechnologyDesalination listing
     */
    public function testTreatmentTechnologyDesalinationListing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_desalinations');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing currency listing
     */
    public function testTreatmentTechnologyDesalinationUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_desalinations');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing TreatmentTechnologyDesalination sort listing.
     */
    public function testTreatmentTechnologyDesalinationSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_desalinations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_desalinations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing TreatmentTechnologyDesalination paginated listing.
     */
    public function testTreatmentTechnologyDesalinationPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_desalinations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing TreatmentTechnologyDesalination creation
     */
    public function testTreatmentTechnologyDesalinationPosting()
    {
        self::$admin->request('POST', '/api/v1/treatment_technology_desalinations', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyDesalination creation
     */
    public function testTreatmentTechnologyDesalinationUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/treatment_technology_desalinations', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyDesalination detection
     */
    public function testTreatmentTechnologyDesalinationShowing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_desalinations/01G2VNN3DYM1FKK48J9C1HWWD1');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyDesalination detection
     */
    public function testTreatmentTechnologyDesalinationUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_desalinations/01G2VNN3DYM1FKK48J9C1HWWD1');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyDesalination edition
     */
    public function testTreatmentTechnologyDesalinationPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/treatment_technology_desalinations/01G2VNN3DYM1FKK48J9C1HWWD1', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing TreatmentTechnologyDesalination edition
     */
    public function testTreatmentTechnologyDesalinationUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/treatment_technology_desalinations/01G2VNN3DYM1FKK48J9C1HWWD1', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyDesalination detection
     */
    public function testTreatmentTechnologyDesalinationDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/treatment_technology_desalinations/01G2VNN3DYM1FKK48J9C1HWWD1');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyDesalination detection
     */
    public function testTreatmentTechnologyDesalinationUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/treatment_technology_desalinations/01G2VNN3DYM1FKK48J9C1HWWD1');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
