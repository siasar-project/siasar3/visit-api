<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test TreatmentTechnologyFiltration endpoint
 */
class TreatmentTechnologyFiltrationResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Lenca",
    ];

    /**
     * testing TreatmentTechnologyFiltration listing
     */
    public function testTreatmentTechnologyFiltrationListing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_filtrations');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing TreatmentTechnologyFiltration listing
     */
    public function testTreatmentTechnologyFiltrationUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_filtrations');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing TreatmentTechnologyFiltration sort listing.
     */
    public function testTreatmentTechnologyFiltrationSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_filtrations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_filtrations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing TreatmentTechnologyFiltration paginated listing.
     */
    public function testTreatmentTechnologyFiltrationPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_filtrations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing TreatmentTechnologyFiltration creation
     */
    public function testTreatmentTechnologyFiltrationPosting()
    {
        self::$admin->request('POST', '/api/v1/treatment_technology_filtrations', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyFiltration creation
     */
    public function testTreatmentTechnologyFiltrationUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/treatment_technology_filtrations', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologyFiltration detection
     */
    public function testTreatmentTechnologyFiltrationShowing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_filtrations/01G2W89T2CG4DZR2S5KFMB331J');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyFiltration detection
     */
    public function testTreatmentTechnologyFiltrationUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_filtrations/01G2W89T2CG4DZR2S5KFMB331J');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologyFiltration edition
     */
    public function testTreatmentTechnologyFiltrationPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/treatment_technology_filtrations/01G2W8A019MRDK0JDNHYXEG47P', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing TreatmentTechnologyFiltration edition
     */
    public function testTreatmentTechnologyFiltrationUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/treatment_technology_filtrations/01G2W8A019MRDK0JDNHYXEG47P', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyFiltration detection
     */
    public function testTreatmentTechnologyFiltrationDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/treatment_technology_filtrations/01G2W8A019MRDK0JDNHYXEG47P');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologyFiltration detection
     */
    public function testTreatmentTechnologyFiltrationUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/treatment_technology_filtrations/01G2W8A019MRDK0JDNHYXEG47P');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
