<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * test TreatmentTechnologySedimentation endpoint
 */
class TreatmentTechnologySedimentationResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "name" => "Lenca",
    ];

    /**
     * testing TreatmentTechnologySedimentation listing
     */
    public function testTreatmentTechnologySedimentationListing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_sedimentations');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing TreatmentTechnologySedimentation listing
     */
    public function testTreatmentTechnologySedimentationUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_sedimentations');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing TreatmentTechnologySedimentation sort listing.
     */
    public function testTreatmentTechnologySedimentationSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_sedimentations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_sedimentations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing TreatmentTechnologySedimentation paginated listing.
     */
    public function testTreatmentTechnologySedimentationPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/treatment_technology_sedimentations', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing TreatmentTechnologySedimentation creation
     */
    public function testTreatmentTechnologySedimentationPosting()
    {
        self::$admin->request('POST', '/api/v1/treatment_technology_sedimentations', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologySedimentation creation
     */
    public function testTreatmentTechnologySedimentationUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/treatment_technology_sedimentations', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing TreatmentTechnologySedimentation detection
     */
    public function testTreatmentTechnologySedimentationShowing()
    {
        self::$admin->request('GET', '/api/v1/treatment_technology_sedimentations/01G2VSTVWWZX3D98A3GYBTEREP');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologySedimentation detection
     */
    public function testTreatmentTechnologySedimentationUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/treatment_technology_sedimentations/01G2VSTVWWZX3D98A3GYBTEREP');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Ruby', $data['name']);
    }

    /**
     * testing TreatmentTechnologySedimentation edition
     */
    public function testTreatmentTechnologySedimentationPutting()
    {
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/treatment_technology_sedimentations/01G2VSTVWWZX3D98A3GYBTEREP', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Another name', $data['name']);
    }

    /**
     * testing TreatmentTechnologySedimentation edition
     */
    public function testTreatmentTechnologySedimentationUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/treatment_technology_sedimentations/01G2VSTVWWZX3D98A3GYBTEREP', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologySedimentation detection
     */
    public function testTreatmentTechnologySedimentationDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/treatment_technology_sedimentations/01G2VSTVWWZX3D98A3GYBTEREP');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing TreatmentTechnologySedimentation detection
     */
    public function testTreatmentTechnologySedimentationUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/treatment_technology_sedimentations/01G2VSTVWWZX3D98A3GYBTEREP');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
