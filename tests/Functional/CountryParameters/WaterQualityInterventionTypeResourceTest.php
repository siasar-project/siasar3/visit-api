<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\CountryParameters;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\WaterQualityInterventionType;
use App\Repository\WaterQualityInterventionTypeRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Tools\Json;

/**
 * test WaterQualityInterventionType endpoint
 */
class WaterQualityInterventionTypeResourceTest extends ApiTestBase
{
    /**
     * testing listing
     */
    public function testGeographicalWaterQualityInterventionTypeListing()
    {
        self::$admin->request('GET', '/api/v1/water_quality_intervention_types');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testGeographicalWaterQualityInterventionTypeUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/water_quality_intervention_types');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testGeographicalWaterQualityInterventionTypeSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'name' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/water_quality_intervention_types', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'name' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/water_quality_intervention_types', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testGeographicalWaterQualityInterventionTypePaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/water_quality_intervention_types', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testGeographicalWaterQualityInterventionTypePosting()
    {
        $json = '{"country": "/api/v1/countries/hn", "keyIndex": "12", "name": "Ruby"}';
        self::$admin->request('POST', '/api/v1/water_quality_intervention_types', [], [], [], $json);
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testGeographicalWaterQualityInterventionTypeUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/water_quality_intervention_types', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "keyIndex" => "6", "name" => "Ruby"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testGeographicalWaterQualityInterventionTypeShowing()
    {
        self::$admin->request('GET', '/api/v1/water_quality_intervention_types/01G4JC2FBDFPKB3G1J7JF6DHXJ');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Scope 2', $data['name']);
    }

    /**
     * testing detection
     */
    public function testGeographicalWaterQualityInterventionTypeUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/water_quality_intervention_types/01G4JC2FBDFPKB3G1J7JF6DHXJ');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Scope 2', $data['name']);
    }

    /**
     * testing edition
     */
    public function testGeographicalWaterQualityInterventionTypePutting()
    {
        self::$admin->request('PUT', '/api/v1/water_quality_intervention_types/01G4JC2FBDFPKB3G1J7JF6DHXJ', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "name" => "Silver"]));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testGeographicalWaterQualityInterventionTypeUserPutting()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/water_quality_intervention_types/01G4JC2FBDFPKB3G1J7JF6DHXJ', [], [], [], \json_encode(["country" => "/api/v1/countries/hn", "name" => "Silver"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing deletion
     */
    public function testGeographicalWaterQualityInterventionTypeDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/water_quality_intervention_types/01G4JC2FBDFPKB3G1J7JF6DHXJ');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testGeographicalWaterQualityInterventionTypeUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/water_quality_intervention_types/01G4JC2FBDFPKB3G1J7JF6DHXJ');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
