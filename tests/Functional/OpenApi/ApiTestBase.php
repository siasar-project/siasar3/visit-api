<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\OpenApi;

use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\OpenApi\GraphQl\GraphQlSchemaBuilder;
use App\Repository\ConfigurationRepository;
use App\Service\SessionService;
use App\Tests\Functional\TestBase;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\InitTestEnvironmentTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * Test user base class.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ApiTestBase extends TestBase
{
    use TestFixturesTrait;
    use InitTestEnvironmentTrait;

    protected string $endpoint;
    protected static ?KernelBrowser $admin = null;
    protected ?EntityManager $entityManager;
    protected ?ConfigurationRepository $configuration;
    protected FormFactory|null $formFactory;
    protected array $extraConfiguration = [];

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $_ENV['TESTS_LOADING_FIXTURES'] = true;
        $this->loadFixtures();

        $this->entityManager = $this->getContainerInstance()->get('doctrine')->getManager();
        $this->entityManager->clear();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');

        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');

        $this->initConfigurations($this->configuration, $this->extraConfiguration);

        $this->endpoint = '/api/v1/users';

        //if (null === self::$admin) {
            self::$admin = clone self::$client;
            $payload = [
                'username' => 'Pedro',
                'password' => '123456',
            ];

            self::$admin->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

            $response = self::$admin->getResponse();
            $responseData = $this->getResponseData($response);

            self::$admin->setServerParameters(
                [
                    'HTTP_AUTHORIZATION' => 'Bearer '.$responseData['token'],
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json',
                ]
            );
            // Server parameters aren't set while testing.
            $_ENV['TESTS_HEADER_HTTP_AUTHORIZATION'] = 'Bearer '.$responseData['token'];
        //}
        $_ENV['TESTS_LOADING_FIXTURES'] = false;
    }

    /**
     * Compare, asserting, a API response object with the creation array.
     *
     * @param FormManagerInterface $form                The form.
     * @param array                $payload             The creation array.
     * @param array                $rawRecordFieldValue The response object.
     * @param bool                 $graphql             Is a GraphQL test?
     *
     * @throws \ReflectionException
     */
    protected function assertApiPayload(FormManagerInterface $form, array $payload, array $rawRecordFieldValue, bool $graphql = false): void
    {
        $fields = $form->getFields();
        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            $fieldName = $field->getId();
            if (!isset($payload[$fieldName])) {
                continue;
            }
            $properties = $field->getProperties();
            if ($field->isMultivalued()) {
                foreach ($payload[$fieldName] as $index => $value) {
                    if (count($properties) === 1) {
                        $ref = $value[key($properties)];
                        if ($graphql && $field->getFieldType() === 'select') {
                            $ref = 'v'.$ref;
                        }
                        $this->assertEquals($ref, $rawRecordFieldValue[$fieldName][$index]);
                    } else {
                        foreach ($properties as $pKey => $pDefault) {
                            $this->assertEquals($value[$pKey], $rawRecordFieldValue[$fieldName][$index][$pKey]);
                        }
                    }
                }
            } else {
                if (count($properties) === 1) {
                    $ref = $payload[$fieldName][key($properties)];
                    if ($graphql && $field->getFieldType() === 'select') {
                        $ref = 'v'.$ref;
                    }
                    $this->assertEquals($ref, $rawRecordFieldValue[$fieldName]);
                } else {
                    foreach ($properties as $pKey => $pDefault) {
                        $this->assertEquals($payload[$fieldName][$pKey], $rawRecordFieldValue[$fieldName][$pKey]);
                    }
                }
            }
        }
    }

    /**
     * Compare, asserting, a record with the creation array.
     *
     * @param array      $payload The creation array.
     * @param FormRecord $record  The created record.
     *
     * @throws \ReflectionException
     */
    protected function assertPayloadRecord(array $payload, FormRecord $record): void
    {
        $form = $record->getForm();
        $fields = $form->getFields();
        $rawRecordFieldValue = $record->getCurrentRaw();
        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            if ($field->getFieldType() === 'map_point' || $field->getFieldType() === 'editor_update') {
                continue;
            }
            $fieldName = 'field_'.$field->getFieldType();
            $properties = $field->getProperties();
            if (count($properties) === 1) {
                $this->assertEquals($payload[$fieldName][key($properties)], $rawRecordFieldValue[$fieldName][key($properties)]);
            } else {
                $data = $field->formatToUse($rawRecordFieldValue[$fieldName]);
                foreach ($properties as $pKey => $pDefault) {
                    $this->assertEquals($payload[$fieldName][$pKey], $data[$pKey]);
                }
            }
        }
    }

    /**
     * Prepare GraphQl query to send.
     *
     * @param string $query The GraphQl query.
     *
     * @return string
     */
    protected function wrapGraphQl(string $query): string
    {
        return sprintf(
            '{"query":"%s","variables":{},"operationName":null}',
            str_replace("\n", ' ', str_replace('"', '\"', $query))
        );
    }

    /**
     * Get GraphQl complete form query.
     *
     * @param FormManagerInterface|null $form      The form.
     * @param string                    $command   GraphQL mutation or type name.
     * @param array                     $args      Query arguments.
     * @param string                    $queryType Query type, default 'query'.
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    protected function getGraphQlQuery(?FormManagerInterface $form, string $command = '', array $args = [], string  $queryType = 'query'): string
    {
        if ($form) {
            $fields = $form->getFields();
        } else {
            $fields = [];
        }
        $query = $queryType." { ";
        $query .= (empty($command)) ? GraphQlSchemaBuilder::formatFormName($form->getId()) : $command;
        if (count($args) > 0) {
            $query .= '(';
            $toSendArguments = [];
            foreach ($args as $key => $value) {
                if ('id' === $key) {
                    $toSendArguments[] = sprintf('id: "/api/v1/form/data/%ss/%s"', $form->getId(), $value);
                } else {
                    $toSendArguments[] = sprintf('%s: "%s"', $key, $value);
                }
            }
            $query .= implode(', ', $toSendArguments);
            $query .= ')';
        }
        if (count($fields) > 0) {
            $query .= " { ";
            $query .= "_id id ";
        }
        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            $query .= "{$field->getId()} ";
            $props = $field->getProperties();
            if (count($props) > 1) {
                $query .= "{ ";
                foreach ($props as $pKey => $default) {
                    $query .= $pKey.' ';
                }
                if ($field->getFieldType() === 'date') {
                    // Add virtual sub-fields.
                    $query .= 'utc_value timestamp ';
                }
                $query .= "} ";
            }
        }
        if (count($fields) > 0) {
            $query .= "}";
        }
        $query .= "}";

        return $query;
    }

    /**
     * Build a GraphQL query body.
     *
     * @param FormManagerInterface $form The form.
     * @param string|null          $id   Record ID.
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    protected function buildQueryBody(FormManagerInterface $form, string $id = null): string
    {
        $fields = $form->getFields();
        $query = "{ ";
        $query .= "\"query\":\"query { ";
        $query .= GraphQlSchemaBuilder::formatFormName($form->getId());
        if ($id) {
            $query .= sprintf('(id: \"/api/v1/form/data/%ss/%s\")', $form->getId(), $id);
        }
        $query .= " { ";
        $query .= "_id id ";
        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            $query .= "{$field->getId()} ";
            $props = $field->getProperties();
            if (count($props) > 1) {
                $query .= "{ ";
                foreach ($props as $pKey => $default) {
                    $query .= $pKey.' ';
                }
                $query .= "} ";
            }
        }
        $query .= "} ";
        $query .= "}\" ";
        $query .= ",\"variables\":{}";
        $query .= ",\"operationName\":null";
        $query .= "} ";

        return $query;
    }

    /**
     * Get a creation record array with example data.
     *
     * @param FormManagerInterface $form The form.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function getCreationRecordArrayTestFieldtypes(FormManagerInterface $form): array
    {
        $fields = $form->getFields();
        $createRecord = [];
        /** @var FieldTypeInterface $field */
        foreach ($fields as $field) {
            if ($field->getFieldType() === 'map_point' || $field->getFieldType() === 'editor_update') {
                continue;
            }
            if ($field->getId() === 'field_refcountry') {
                $createRecord['field_refcountry'] = $field->getExampleData($createRecord);
            } elseif ($field->isMultivalued()) {
                $createRecord['field_'.$field->getFieldType()] = [
                    $field->getExampleData($createRecord),
                    $field->getExampleData($createRecord),
                ];
            } else {
                $createRecord['field_'.$field->getFieldType()] = $field->getExampleData($createRecord);
            }
        }

        return $createRecord;
    }

    /**
     * Load testing data.
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function insertData(): array
    {
        $form = $this->formFactory->find('test.api');
        $newIds = [];
        for ($i = 0; $i < 50; ++$i) {
            $date = new \Datetime('now', new \DateTimeZone('Europe/Madrid'));
            $newIds[] = $form->insert(
                [
                    'field_name' => "Pedro",
                    'field_date' => [
                        'value' => $date->format('Y-m-d H:i:s'),
                        'timezone' => 'Europe/Madrid',
                    ],
                    'field_number' => $i,
                ]
            );
        }

        return $newIds;
    }

    /**
     * Delete all form records.
     *
     * @param FormManagerInterface $form The form.
     */
    protected function cleanForm(FormManagerInterface $form): void
    {
        $form->uninstall();
        $form->install();
    }

    /**
     * Install and/or clear any record in it.
     *
     * @param string $formId
     *
     * @return FormManagerInterface
     */
    protected function installForm(string $formId): FormManagerInterface
    {
        $form = $this->formFactory->find($formId);
        $this->cleanForm($form);

        return $form;
    }
}
