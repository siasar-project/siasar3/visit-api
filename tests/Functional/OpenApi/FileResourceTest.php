<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Entity\File;
use App\Repository\FileRepository;
use App\Service\FileSystem;
use App\Tests\Functional\OpenApi\ApiTestBase;
use GuzzleHttp\Client;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * test file endpoint
 */
class FileResourceTest extends ApiTestBase
{
    private $payload = [
      "country" => "/api/v1/countries/hn",
      "fileName" => "Lenca",
    ];
    protected FileRepository $fileRepository;
    protected FileSystem $fileSystem;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $entityManager = self::$container->get('doctrine')->getManager();
        $this->fileRepository = $entityManager->getRepository('App\Entity\File');
        $this->fileSystem = self::$container->get('os_file_system');

        $dir = $this->getKernelInstance()->getProjectDir();
        $this->prjDir = $dir.'/'.$_ENV['APP_CONFIG_PUBLIC_FILES'];
        $this->assetsDir = $dir.'/tests/assets';
    }

    /**
     * testing file listing
     */
    public function testFileListing()
    {
        self::$admin->request('GET', '/api/v1/files');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing file listing
     */
    public function testFileUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/files');
        $response = $session->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Unable to retrieve File: The "read" action require the permission "read doctrine file".', $data['detail']);
    }

    /**
     * testing file sort listing.
     */
    public function testFileSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'fileName' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/files', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'fileName' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/files', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing file paginated listing.
     */
    public function testFilePaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/files', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing file creation
     */
    public function testFilePostingFail()
    {
        self::$admin->request('POST', '/api/v1/files', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * testing file creation
     */
    public function testFilePostingOk()
    {
        // The upload test method move the original file.
        copy(
            $this->getKernelInstance()->getProjectDir().'/tests/assets/siasar_logo_blue.svg',
            $this->getKernelInstance()->getProjectDir().'/tests/assets/bak.siasar_logo_blue.svg'
        );
        // Upload the file.
        $avatar = new UploadedFile(
            $this->getKernelInstance()->getProjectDir().'/tests/assets/bak.siasar_logo_blue.svg',
            'siasar_logo_blue.svg',
            'image/svg',
            null,
            true
        );

        $session = $this->loginRest('Pedro');
        $session->request(
            'POST',
            '/api/v1/files',
            // $parameters
            [
                'country' => '/api/v1/countries/hn',
            ],
            // $files
            ['file' => $avatar],
            // $server
            [
                'CONTENT_TYPE' => 'multipart/form-data',
            ]
        );
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        // Try to download to verify.
//        $data = $this->getResponseData($response);
//        $fileUrl = '/api/v1/files/'.$data['id'].'/download';
//        // When we request download PHPUnit fail, download the file and stop de test.
//        $session->request('GET', $fileUrl);
//        $responseData = $session->getInternalResponse()->getContent();
//        $response = $session->getResponse();
//        $data = $this->getResponseData($response);
//        // We can't assert nothing.
    }

    /**
     * testing file creation
     */
    public function testFileUserPosting()
    {
        // Upload the file.
        $avatar = new UploadedFile(
            $this->getKernelInstance()->getProjectDir().'/tests/assets/siasar_logo_blue.svg',
            'siasar_logo_blue.svg',
            'image/svg',
            null,
            true
        );

        $session = $this->loginRest('Marta');
        $session->request(
            'POST',
            '/api/v1/files',
            // $parameters
            [
                'country' => '/api/v1/countries/hn',
            ],
            // $files
            ['file' => $avatar],
            // $server
            [
                'CONTENT_TYPE' => 'multipart/form-data',
            ]
        );
        $response = $session->getResponse();
        $this->assertEquals(422, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Error while creating file: The &quot;create&quot; action require the permission &quot;create doctrine file&quot;..', $data[0]);
    }

    /**
     * testing file detection
     */
    public function testFileShowing()
    {
        self::$admin->request('GET', '/api/v1/files/01FHQXRG6CAT51T97262HQFRWW');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('dummy.bin', $data['fileName']);
        $this->assertEquals('/api/v1/countries/hn', $data['country']);
    }

    /**
     * testing file detection
     */
    public function testFileUserShowing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/files/01FHQXRG6CAT51T97262HQFRWW');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing file edition
     */
    public function testFilePutting()
    {
        $payload = $this->payload;
        $payload['fileName'] = 'Another name';
        self::$admin->request('PUT', '/api/v1/files/01FHQXRG6CAT51T97262HQFRWW', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(405, $response->getStatusCode());
    }

    /**
     * testing file edition
     */
    public function testFileUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload = $this->payload;
        $payload['name'] = 'Another name';
        $session->request('PUT', '/api/v1/files/01FHQXRG6CAT51T97262HQFRWW', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(405, $response->getStatusCode());
    }

    /**
     * Testing file detection
     */
    public function testFileDeleting()
    {
        self::$admin->request('DELETE', '/api/v1/files/01FHQXRG6CAT51T97262HQFRWW');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing file detection
     */
    public function testFileUserDeleting()
    {
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/files/01FHQXRG6CAT51T97262HQFRWW');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * Test CORS download headers.
     */
    public function testFileGetCorsHeaders(): void
    {
        $session = $this->loginRest('Pedro');


        // Copy the file to test with it.
        @copy($this->assetsDir.'/siasar_logo_blue.svg', $this->prjDir.'/siasar_logo_blue.svg');
        // Create the file entity.
        /** @var File $file */
        $file = $this->fileRepository->create($this->prjDir.'/siasar_logo_blue.svg');

        ob_start();
        $session->request('GET', '/api/v1/files/'.$file->getId().'/download');
        ob_get_clean();
        $response = $session->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(5264, $response->headers->get('content-length'));
    }
}
