<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Forms\FormFactory;
use App\Tests\Functional\OpenApi\ApiTestBase;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * test Form Structure endpoint
 */
class FormStructureTest extends ApiTestBase
{
    protected ?FormFactory $formFactory;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->formFactory = self::$container->get('form_factory');
        $form = $this->formFactory->create('endpoint');
        $form->saveNow();
        $form->install();
    }

    /**
     * testing form listing
     */
    public function testFormListing()
    {
        self::$admin->request('GET', '/api/v1/form/structure');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
        $this->assertEquals('form.endpoint', $data[0]['id']);
        $this->assertEquals('/form/structure/form.endpoint', $data[0]['path']);
    }

    /**
     * testing form listing
     */
    public function testFormUserListing()
    {
        // Login.
        $session = $this->loginRest('Marta');

        $session->request('GET', '/api/v1/form/structure');
        $response = $session->getResponse();
        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * testing form listing
     */
    public function testFormReaderListing()
    {
        // Login.
        $session = $this->loginRest('Reader');

        $session->request('GET', '/api/v1/form/structure');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
        $this->assertEquals('form.endpoint', $data[0]['id']);
        $this->assertEquals('/form/structure/form.endpoint', $data[0]['path']);
    }

    /**
     * testing form listing
     */
    public function testFormGet()
    {
        self::$admin->request('GET', '/api/v1/form/structure/form.endpoint');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
        $this->assertEquals('form.endpoint', $data['id']);
        $this->assertEquals('App\Forms\FormManager', $data['type']);
        $this->assertEmpty($data['fields']);
    }

    /**
     * testing form listing
     */
    public function testFormReaderGet()
    {
        // Login.
        $session = $this->loginRest('Reader');

        $session->request('GET', '/api/v1/form/structure/form.endpoint');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
        $this->assertEquals('form.endpoint', $data['id']);
        $this->assertEquals('App\Forms\FormManager', $data['type']);
        $this->assertEmpty($data['fields']);
    }

    /**
     * testing form listing
     */
    public function testFormUserGet()
    {
        // Login.
        $session = $this->loginRest('Marta');

        $session->request('GET', '/api/v1/form/structure/form.endpoint');
        $response = $session->getResponse();
        $this->assertEquals(401, $response->getStatusCode());
    }
}
