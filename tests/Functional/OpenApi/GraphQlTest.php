<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\OpenApi;

use App\Entity\Country;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\SubformFormManager;
use App\Tests\TestFixturesTrait;
use Doctrine\ORM\EntityManager;

/**
 * GraphQL tests.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class GraphQlTest extends ApiTestBase
{
    use TestFixturesTrait;

    protected string $adminUsername;
    protected string $adminPassword;
    protected ?FormFactory $formFactory;
    protected ?FieldTypeManager $fieldTypeManager;
    protected ?EntityManager $em;
    private ?object $jwtEncoder;

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        // Skip tests because GraphQl is disabled.
        if (!static::getContainerInstance()->getParameter('api_platform.graphql.enabled')) {
            $this->markTestSkipped('GraphQl is disabled. Skip tests.');
        }

        $this->extraConfiguration = [
            'form.wsprovider.contact' => dirname(__FILE__).'/../../assets/form.wsprovider.contact.yml',
            'form.wsprovider.tap' => dirname(__FILE__).'/../../assets/form.wsprovider.tap.yml',
            'form.wsprovider' => dirname(__FILE__).'/../../assets/form.wsprovider.yml',
            'form.wssystem.communities' => dirname(__FILE__).'/../../assets/form.wssystem.communities.yml',
            'form.wssystem.contact' => dirname(__FILE__).'/../../assets/form.wssystem.contact.yml',
            'form.wssystem.intakeflow' => dirname(__FILE__).'/../../assets/form.wssystem.intakeflow.yml',
            'form.wssystem.projects' => dirname(__FILE__).'/../../assets/form.wssystem.projects.yml',
            'form.wssystem.qualitybacteriological' => dirname(__FILE__).'/../../assets/form.wssystem.qualitybacteriological.yml',
            'form.wssystem.qualityphysiochemical' => dirname(__FILE__).'/../../assets/form.wssystem.qualityphysiochemical.yml',
            'form.wssystem.residualtests' => dirname(__FILE__).'/../../assets/form.wssystem.residualtests.yml',
            'form.wssystem.sourceflow' => dirname(__FILE__).'/../../assets/form.wssystem.sourceflow.yml',
            'form.wssystem.transmisionline' => dirname(__FILE__).'/../../assets/form.wssystem.transmisionline.yml',
            'form.wssystem.sourceintake' => dirname(__FILE__).'/../../assets/form.wssystem.sourceintake.yml',
            'form.wssystem.treatmentpoints' => dirname(__FILE__).'/../../assets/form.wssystem.treatmentpoints.yml',
            'form.wssystem.storage' => dirname(__FILE__).'/../../assets/form.wssystem.storage.yml',
            'form.wssystem.distribution' => dirname(__FILE__).'/../../assets/form.wssystem.distribution.yml',
            'form.wssystem' => dirname(__FILE__).'/../../assets/form.wssystem.yml',
            'form.community.intervention' => dirname(__FILE__).'/../../assets/form.community.intervention.yml',
            'form.community.contact' => dirname(__FILE__).'/../../assets/form.community.contact.yml',
            'form.community' => dirname(__FILE__).'/../../assets/form.community.yml',
            'form.point' => dirname(__FILE__).'/../../assets/form.point.yml',
        ];
        parent::setUp();
        $_SERVER['HTTP_HEADER'] = ['HTTP_AUTHORIZATION' => parent::$admin->getServerParameter('HTTP_AUTHORIZATION')];
        // self::bootKernel();

        $this->endpoint = '/api/v1/graphql';
        $this->adminUsername = 'Pedro';
        $this->adminPassword = '123456';
        $this->formFactory = self::$container->get('form_factory');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->em = self::$container->get('doctrine.orm.entity_manager');
        $this->jwtEncoder = self::$container->get('lexik_jwt_authentication.encoder.lcobucci');

        $this->loginUser('pedro');
        // Init Community form.
        $this->installForm('form.community.intervention');
        $this->installForm('form.community.contact');
        $form = $this->formFactory->find('form.community');
        $form->install();
        // Add a record to have an example record.
        $commId = $form->insert([]);
        // Init Point form.
        $form = $this->installForm('form.point');
        // Add a record to have an example record.
        $countryRepo = $this->em->getRepository(Country::class);
        $form->insert(
            [
                'field_country' => $countryRepo->find('hn'),
                'field_communities' => [
                    [
                        'value' => $commId,
                        'form' => 'form.community',
                    ],
                ],
            ]
        );
        // Init WSystem form.
        $this->installForm('form.wssystem.communities');
        $this->installForm('form.wssystem.contact');
        $this->installForm('form.wssystem.intakeflow');
        $this->installForm('form.wssystem.projects');
        $this->installForm('form.wssystem.qualitybacteriological');
        $this->installForm('form.wssystem.qualityphysiochemical');
        $this->installForm('form.wssystem.residualtests');
        $this->installForm('form.wssystem.sourceflow');
        $this->installForm('form.wssystem.transmisionline');
        $this->installForm('form.wssystem.sourceintake');
        $this->installForm('form.wssystem.treatmentpoints');
        $this->installForm('form.wssystem.storage');
        $this->installForm('form.wssystem.distribution');
        $form = $this->installForm('form.wssystem');
        $form->insert([]);
        // Init WSProvider form.
        $this->installForm('form.wsprovider.contact');
        $this->installForm('form.wsprovider.tap');
        $form = $this->installForm('form.wsprovider');
        // Add a record to have an example record.
        $form->insert([]);

        // Subform.
        $form = $this->formFactory->create('form.subform.ql', SubformFormManager::class);
        $form->addField('integer', 'Reference', 'Record reference');
        $form->setMeta(['parent_form' => 'form.test.graphql.fieldtypes']);
        $form->saveNow();
        $form->uninstall();
        $form->install();
        $form->insert(['field_country' => $countryRepo->find('hn')]);

        // Basic test form.
        $form = $this->formFactory->create('test.graphql');
        $form->setDescription('A GraphQL test form.');
        $form->addField('short_text', 'name', 'Name');
        $form->addField('date', 'date', 'Date');
        $form->addField('integer', 'number', 'Number');
        $form->saveNow();
        $form->install();
        $this->cleanForm($form);
        // Load 100 records.
        $date = new \DateTime('now', new \DateTimeZone('Europe/Madrid'));
        for ($i = 1; $i <= 100; ++$i) {
            $form->insert(
                [
                    'field_name' => "Record ".$i,
                    'field_date' => [
                        'value' => $date->format('Y-m-d H:i:s'),
                        'timezone' => 'Europe/Madrid',
                    ],
                    'field_number' => $i,
                ]
            );
        }

        // All fields types test form.
        $form = $this->formFactory->create('test.graphql.fieldtypes');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform.ql';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
        }
        $form->saveNow();
        $form->install();
        $this->cleanForm($form);
    }

    /**
     * Test that we get a valid session token.
     */
    public function testLogin(): void
    {
        // Get token using GraphQL.
        $query = $this->wrapGraphQl(
            $this->getGraphQlQuery(
                null,
                'login',
                [
                    'username' => $this->adminUsername,
                    'password' => $this->adminPassword,
                ],
                'mutation'
            )
        );

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertIsString($responseData["data"]["login"]);
        // Decode token and validate user name.
        $payload = $this->jwtEncoder->decode($responseData["data"]["login"]);
        $this->assertEquals('Pedro', $payload["username"]);
        // Validate user ID in token.
        $user = $this->em->getRepository(User::class)->findBy(['username' => $this->adminUsername]);
        $this->assertEquals($user[0]->getId(), $payload["id"]);
    }

    /**
     * Test that we can get a item.
     */
    public function testGetAItem(): void
    {
        $form = $this->formFactory->find('test.graphql');
        $id = $form->insert([
            'field_name' => 'Pedro',
            'field_date' => [
                'value' => '2021-06-29 17:16',
                'timezone' => 'Europe/Madrid',
            ],
            'field_number' => 1,
        ]);
        $query = $this->wrapGraphQl($this->getGraphQlQuery($form, '', ['id' => $id]));

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        // Verify.
        $this->assertArrayHasKey("form__test__graphql", $responseData["data"]);
        $this->assertEquals('/api/v1/form/data/form.test.graphqls/'.$id, $responseData["data"]["form__test__graphql"]["id"]);
        $this->assertEquals($id, $responseData["data"]["form__test__graphql"]["_id"]);
        $this->assertEquals('Pedro', $responseData["data"]["form__test__graphql"]["field_name"]);
        $this->assertEquals(1, $responseData["data"]["form__test__graphql"]["field_number"]);

        $this->assertEquals('2021-06-29 17:16:00', $responseData["data"]["form__test__graphql"]["field_date"]['value']);
        $this->assertEquals('Europe/Madrid', $responseData["data"]["form__test__graphql"]["field_date"]['timezone']);
        $this->assertEquals('2021-06-29 15:16:00', $responseData["data"]["form__test__graphql"]["field_date"]['utc_value']);
        $this->assertEquals('1624979760', $responseData["data"]["form__test__graphql"]["field_date"]['timestamp']);
    }

    /**
     * Test that we can get some fields.
     */
    public function testGetSomeField(): void
    {
        $form = $this->formFactory->find('test.graphql');
        $id = $form->insert([
            'field_name' => 'Pedro',
            'field_date' => [
                'value' => '2021-06-29 17:16',
                'timezone' => 'Europe/Madrid',
            ],
            'field_number' => 1,
        ]);
        $query = '{"query":"query { form__test__graphql(id: \"/api/v1/form/data/form.test.graphqls/'.$id.'\") {field_name}}","variables":{},"operationName":null}';

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        // Verify.
        $this->assertArrayHasKey("form__test__graphql", $responseData["data"]);
        $this->assertArrayNotHasKey('id'.$id, $responseData["data"]["form__test__graphql"]);
        $this->assertArrayNotHasKey('_id', $responseData["data"]["form__test__graphql"]);
        $this->assertEquals('Pedro', $responseData["data"]["form__test__graphql"]["field_name"]);
        $this->assertArrayNotHasKey('field_number', $responseData["data"]["form__test__graphql"]);
        $this->assertArrayNotHasKey('field_date', $responseData["data"]["form__test__graphql"]);
    }

    /**
     * Test that we can get some date sub-fields.
     */
    public function testGetSomeDateSubField(): void
    {
        $form = $this->formFactory->find('test.graphql');
        $id = $form->insert([
            'field_name' => 'Pedro',
            'field_date' => [
                'value' => '2021-06-29 17:16',
                'timezone' => 'Europe/Madrid',
            ],
            'field_number' => 1,
        ]);
        $query = '{"query":"query { form__test__graphql(id: \"/api/v1/form/data/form.test.graphqls/'.$id.'\") {field_date {timestamp}}}","variables":{},"operationName":null}';

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        // Verify.
        $this->assertArrayHasKey("form__test__graphql", $responseData["data"]);
        $this->assertArrayNotHasKey('id'.$id, $responseData["data"]["form__test__graphql"]);
        $this->assertArrayNotHasKey('_id', $responseData["data"]["form__test__graphql"]);
        $this->assertEquals('1624979760', $responseData["data"]["form__test__graphql"]["field_date"]['timestamp']);
        $this->assertArrayNotHasKey('field_number', $responseData["data"]["form__test__graphql"]);
        $this->assertArrayNotHasKey('field_name', $responseData["data"]["form__test__graphql"]);
    }

    /**
     * Test that we can get a record.
     */
    public function testAllFieldTypesGetItem(): void
    {
        $form = $this->formFactory->find('test.graphql.fieldtypes');
        $payload = $this->getCreationRecordArrayTestFieldtypes($form);
        $newId = $form->insert($payload);
        $query = $this->wrapGraphQl($this->getGraphQlQuery($form, '', ['id' => $newId]));

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($newId, $responseData["data"]["form__test__graphql__fieldtypes"]['_id']);
        $this->assertApiPayload($form, $payload, $responseData["data"]["form__test__graphql__fieldtypes"], true);
    }

    /**
     * Test that we can get multivalued fields.
     */
    public function testGetMultivaluedField(): void
    {
        // Subform.
        $subform = $this->formFactory->create('form.subform.ql2', SubformFormManager::class);
        $subform->addField('integer', 'Reference', 'Record reference');
        $subform->setMeta(['parent_form' => 'form.test.graphql.mv']);
        $subform->saveNow();
        $subform->uninstall();
        $subform->install();
        $countryRepo = $this->em->getRepository(Country::class);
        $subform->insert(['field_country' => $countryRepo->find('hn')]);
        // Prepare form.
        $form = $this->formFactory->create('test.graphql.mv');
        $form->addField('country_reference', 'refcountry', 'Ref. country');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [
                'multivalued' => true,
            ];
            // Set required field options.
            $continueFor = false;
            switch ($fieldTypeId) {
                case 'inquiring_status':
                case 'publishing_status':
                case 'pointing_status':
                    $continueFor = true;
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform.ql2';
                    }
                    $options['country_field'] = 'field_refcountry';
                    break;
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
            }
            if ($continueFor) {
                continue;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
            ++$i;
        }
        $form->saveNow();
        $form->install();

        // Insert a record.
        $payload = $this->getCreationRecordArrayTestFieldtypes($form);
        $newId = $form->insert($payload);

        // Query it.
        $query = $this->wrapGraphQl($this->getGraphQlQuery($form, '', ['id' => $newId]));
        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        // Verify record.
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($newId, $responseData["data"]["form__test__graphql__mv"]['_id']);
        $this->assertApiPayload($form, $payload, $responseData["data"]["form__test__graphql__mv"], true);
    }

    /**
     * Test that we can list records.
     */
    public function testGetListGraphQl():void
    {
        $form = $this->formFactory->find('test.graphql');
        // , order:[{id:"desc"}]
        $query = $this->wrapGraphQl('query{
          form__test__graphqls (first: 5, after: "NA=="){
            edges {
              node {
                _id
                id
                field_name
                field_date {value timezone utc_value timestamp}
                field_number
              }
              cursor
              int_cursor
            }
            pageInfo {
              endCursor
              startCursor
              hasNextPage
              hasPreviousPage
            }
            totalCount
          }
        }');

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());
        // Get records and verify it.
        $this->assertArrayHasKey('form__test__graphqls', $responseData["data"]);
        $this->assertArrayHasKey('edges', $responseData["data"]['form__test__graphqls']);
        $this->assertCount(5, $responseData["data"]['form__test__graphqls']['edges']);
        $this->assertEquals('NQ==', $responseData["data"]["form__test__graphqls"]["edges"][0]["cursor"]);
        $this->assertArrayHasKey('_id', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]);
        $this->assertArrayHasKey('id', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]);
        $this->assertArrayHasKey('field_name', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]);

        $this->assertArrayHasKey('field_date', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]);
        $this->assertArrayHasKey('value', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]['field_date']);
        $this->assertArrayHasKey('timezone', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]['field_date']);
        $this->assertArrayHasKey('utc_value', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]['field_date']);
        $this->assertArrayHasKey('timestamp', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]['field_date']);

        $this->assertArrayHasKey('field_number', $responseData["data"]["form__test__graphqls"]["edges"][0]["node"]);

        $this->assertEquals('OQ==', $responseData["data"]["form__test__graphqls"]["edges"][4]["cursor"]);

        // Verify page info.
        $this->assertEquals('OQ==', $responseData["data"]["form__test__graphqls"]["pageInfo"]["endCursor"]);
        $this->assertEquals('NQ==', $responseData["data"]["form__test__graphqls"]["pageInfo"]["startCursor"]);
        $this->assertTrue($responseData["data"]["form__test__graphqls"]["pageInfo"]["hasNextPage"]);
        $this->assertTrue($responseData["data"]["form__test__graphqls"]["pageInfo"]["hasPreviousPage"]);

        // Verify total.
        $this->assertEquals(100, $responseData["data"]["form__test__graphqls"]["totalCount"]);
    }

    /**
     * Test that we can create a records.
     */
    public function testCreateRecord():void
    {
        $form = $this->formFactory->find('test.graphql');
        // , order:[{id:"desc"}]
        $query = $this->wrapGraphQl('mutation{
           createForm__test__graphql (input: {clientMutationId: "ReferenciaDeCliente", field_name: "Alfonso", field_date: {value: "2021-07-13 11:03", timezone: "Europe/Madrid"}, field_number: 5}){
                form__test__graphql {
                    _id
                    id
                    field_name
                    field_date {
                        value
                        timezone
                        utc_value
                        timestamp
                    }
                    field_number
                }
                clientMutationId
          }
        }');

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('createForm__test__graphql', $responseData["data"]);
        $this->assertEquals('ReferenciaDeCliente', $responseData["data"]["createForm__test__graphql"]["clientMutationId"]);

        // Is the record saved?
        $id = $responseData["data"]["createForm__test__graphql"]["form__test__graphql"]["_id"];
        $record = $form->find($id);
        $this->assertNotNull($record);
        $this->assertEquals('Alfonso', $record->{'field_name'});
        $this->assertEquals(5, $record->{'field_number'});

        $this->assertEquals('2021-07-13 11:03:00', $record->get('field_date')['value']->format('Y-m-d H:i:s'));
        $this->assertEquals('Europe/Madrid', $record->get('field_date')['timezone']);
    }

    /**
     * Test that we can update a records.
     */
    public function testUpdateRecord():void
    {
        $form = $this->formFactory->find('test.graphql');
        /** @var FormRecord $record */
        $record = $form->findOneBy(['field_number' => 1]);

        // , order:[{id:"desc"}]
        $query = $this->wrapGraphQl('mutation{
           updateForm__test__graphql (input: {id: "/api/v1/form/data/form__test__graphqls/'.$record->getId().'", clientMutationId: "ReferenciaDeCliente", field_name: "Alfonso", field_date: {value: "2021-07-13 11:03", timezone: "Europe/Madrid"}, field_number: 5}){
                form__test__graphql {
                    _id
                    id
                    field_name
                    field_date {
                        value
                        timezone
                        utc_value
                        timestamp
                    }
                    field_number
                }
                clientMutationId
          }
        }');

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('updateForm__test__graphql', $responseData["data"]);
        $this->assertEquals('ReferenciaDeCliente', $responseData["data"]["updateForm__test__graphql"]["clientMutationId"]);

        // Is the record saved?
        $id = $responseData["data"]["updateForm__test__graphql"]["form__test__graphql"]["_id"];
        $record = $form->find($id);
        $this->assertNotNull($record);
        $this->assertEquals('Alfonso', $record->{'field_name'});
        $this->assertEquals(5, $record->{'field_number'});

        $this->assertEquals('2021-07-13 11:03:00', $record->get('field_date')['value']->format('Y-m-d H:i:s'));
        $this->assertEquals('Europe/Madrid', $record->get('field_date')['timezone']);
    }

    /**
     * Test that we can delete a records.
     */
    public function testDeleteRecord():void
    {
        $form = $this->formFactory->find('test.graphql');
        $id = $form->insert(
            [
                'field_name' => 'PSF',
                'field_date' => [
                    'value' => '2021-07-13 11:03:00',
                    'timezone' => 'Europe/Madrid',
                ],
                'field_number' => 1,
            ]
        );
        /** @var FormRecord $record */
        $record = $form->find($id);

        $query = $this->wrapGraphQl('mutation{
           deleteForm__test__graphql (input: {id: "/api/v1/form/data/form__test__graphqls/'.$record->getId().'", clientMutationId: "ReferenciaDeCliente"}){
                form__test__graphql {
                    _id
                    id
                    field_name
                    field_date {
                        value
                        timezone
                        utc_value
                        timestamp
                    }
                    field_number
                }
                clientMutationId
          }
        }');

        parent::$admin->request('POST', $this->endpoint, [], [], [], $query);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertArrayHasKey('deleteForm__test__graphql', $responseData["data"]);
        $this->assertEquals('ReferenciaDeCliente', $responseData["data"]["deleteForm__test__graphql"]["clientMutationId"]);

        // Is the record returned?
        $id = $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["_id"];
        $this->assertEquals('PSF', $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["field_name"]);
        $this->assertEquals(1, $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["field_number"]);

        $this->assertEquals('2021-07-13 11:03:00', $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["field_date"]["value"]);
        $this->assertEquals('Europe/Madrid', $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["field_date"]["timezone"]);
        $this->assertEquals('2021-07-13 09:03:00', $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["field_date"]["utc_value"]);
        $this->assertEquals(1626166980, $responseData["data"]["deleteForm__test__graphql"]["form__test__graphql"]["field_date"]["timestamp"]);

        // Is the record deleted?
        $record = $form->find($id);
        $this->assertNull($record);
    }
}
