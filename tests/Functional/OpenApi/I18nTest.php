<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Entity\LocaleSource;
use App\Entity\LocaleTarget;
use App\Service\ConfigurationService;
use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tools\Json;

/**
 * test Form Structure endpoint
 */
class I18nTest extends ApiTestBase
{
    protected ?ConfigurationService $configurationService;

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->configurationService = self::$container->get('configuration_service');
    }

    /**
     * testing access to add literal.
     */
    public function testI18nAddAnonymous()
    {
        $session = $this->loginRest('');

        $session->request('POST', '/api/v1/configuration/i18n/add');
        $response = $session->getResponse();
        $this->assertEquals(401, $response->getStatusCode());
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddEmptyBodyUser()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $session->request('POST', '/api/v1/configuration/i18n/add');
        $response = $session->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddBadBodyUser()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $body = ['other' => 'Hi world'];

        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddBadGoodBodyUser()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $body = ['other' => 'Hi world', 'literal' => 'Hi world'];

        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddBadBodyTypeUser()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $body = ['literal' => ['key' => 'Hi world']];

        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddWithoutPermissionUser()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $body = ['literal' => 'Hi world'];

        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
        $this->assertEquals('"The permission \u0022create i18n literal\u0022 is required."', $response->getContent());
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddUser()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $body = ['literal' => 'SALMO 91'];

        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Validate that the new literal is created.
        $sourceRepo = $this->entityManager->getRepository(LocaleSource::class);
        $source = $sourceRepo->findBy(['message' => 'SALMO 91']);
        $this->assertNotCount(0, $source);
    }

    /**
     * testing i18n add literal.
     */
    public function testI18nAddDuplicatedUser()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $body = ['literal' => 'SALMO 91'];

        // First.
        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Duplicated.
        $session->request('POST', '/api/v1/configuration/i18n/add', [], [], [], Json::encode($body));
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        // Validate that the new literal is created.
        $sourceRepo = $this->entityManager->getRepository(LocaleSource::class);
        $source = $sourceRepo->findBy(['message' => 'SALMO 91']);
        $this->assertCount(1, $source);
    }

    /**
     * Testing i18n get literals
     */
    public function testI18nGetAllLiterals()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $session->request('GET', '/api/v1/configuration/i18n/es_es');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Testing i18n get translations
     */
    public function testI18nGetAllTranslations()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Testing i18n get literals with invalid ISO Code
     */
    public function testI18nGetTranslationsInvalidISOCode()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $session->request('GET', '/api/v1/configuration/i18n/es_234/translator');
        $response = $session->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('"ISO Code \u0022es_234\u0022 not found."', $response->getContent());
    }

    /**
     * Testing i18n get translations with pagination
     */
    public function testI18nGetTranslationsWithPagination()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator');
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $total = $data['total'];
        $cont = 0;
        $totalCalculated = 0;

        while ($data['total'] !== 0) {
            $parameters = [
                'page' => $cont + 1,
                'itemsPerPage' => 2,
            ];

            $session->request('GET', '/api/v1/configuration/i18n/es_es/translator', $parameters);
            $response = $session->getResponse();
            $data = Json::decode($response->getContent());
            $totalCalculated += $data['total'];
            $cont++;
        }

        $this->assertEquals($total, $totalCalculated);
    }

    /**
     * Testing i18n get list of literals with search.
     * It searchs in translated, untranslated and both.
     */
    public function testI18nGetTranslationsWithSearch()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $parameters = [
            'contains' => 'test',
        ];
        // Search in both (translated and untranslated literals)
        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator', $parameters);
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals(2, $data['total']);

        // Search only in translated literals
        $parameters['search_in'] = 'translated';
        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator', $parameters);
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals('Esto es una prueba', $data['literals']['This is a test']);

        // Search only in untranslated literals
        $parameters['search_in'] = 'untranslated';
        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator', $parameters);
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals('testing translations', array_keys($data['literals'])[0]);
    }

    /**
     * Testing i18n add translation
     */
    public function testI18nAddTranslation()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $content = [
            'source' => 'lorem ipsum',
            'target' => 'Ipsum lorem',
        ];

        $session->request('POST', '/api/v1/configuration/i18n/es_es', [], [], [], Json::encode($content));

        // Search literal
        $parameters = [
            'search_in' => 'translated',
            'contains' => 'lorem',
        ];
        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator', $parameters);
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals($content['target'], $data['literals'][$content['source']]);
    }

    /**
     * Testing i18n add translation without permissions
     */
    public function testI18nAddTranslationWithoutPermission()
    {
        $session = $this->loginRest('Marta');
        $this->loginUser('Marta');

        $content = [
            'source' => 'lorem ipsum',
            'target' => 'Ipsum lorem',
        ];

        $session->request('POST', '/api/v1/configuration/i18n/es_es', [], [], [], Json::encode($content));
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('The "update" action require the permission "update doctrine localetarget".', $data['detail']);
    }

    /**
     * Testing i18n update translation
     */
    public function testI18nUpdateTranslation()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $parameters = ['contains' => 'This is a test'];
        $newTranslation = 'nueva traduccion';

        $session->request('GET', '/api/v1/configuration/i18n/es_es/translator', $parameters);
        $response = $session->getResponse();
        $prev = Json::decode($response->getContent());
        $literal = key($prev['literals']);

        $content = [
            'source' => $literal,
            'target' => $newTranslation,
        ];

        $session->request('POST', '/api/v1/configuration/i18n/es_es', [], [], [], Json::encode($content));
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());

        // Validate that the translation is updated.
        $targetRepo = $this->entityManager->getRepository(LocaleTarget::class);
        /** @var LocaleTarget $target */
        $target = $targetRepo->findOneBy(['translation' => $newTranslation]);
        $this->assertEquals($newTranslation, $target->getTranslation());
        $this->assertEquals($literal, $target->getLocaleSource()->getMessage());

        // Try to find the old target
        $oldTarget = $targetRepo->findOneBy(['translation' => 'Esto es una prueba']);
        $this->assertEmpty($oldTarget);
    }

    /**
     * Testing i18n add translation of an untranslatable language
     */
    public function testI18nTranslateUntranslatableLanguage()
    {
        $session = $this->loginRest('Translator');
        $this->loginUser('Translator');

        $content = [
            'source' => 'This is a test',
            'target' => 'new translation',
        ];

        $session->request('POST', '/api/v1/configuration/i18n/es', [], [], [], Json::encode($content));
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals('This language is not translatable.', $data);
    }
}
