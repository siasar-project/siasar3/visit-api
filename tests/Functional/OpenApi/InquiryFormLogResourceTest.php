<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\OpenApi;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\InquiryFormLog;
use App\Forms\FormFactory;
use App\Forms\InquiryFormManager;
use App\Repository\InquiryFormLogRepository;
use App\Service\SessionService;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\Tools\Json;

/**
 * test InquiryFormLog endpoint
 */
class InquiryFormLogResourceTest extends ApiTestBase
{
    protected ?FormFactory $formFactory;
    protected ?SessionService $sessionService;

    private $recordId;

    private $payload = [
        "message" => "Test from post",
        "context" => [],
        "level" => 200,
        "levelName" => "INFO",
        "extra" => [],
        "country" => "/api/v1/countries/hn",
    ];

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->formFactory = self::$container->get('form_factory');
        $this->sessionService = self::$container->get('session_service');

        $form = $this->formFactory->find('test.inquiry.log', false);

        if (!$form) {
            $form = $this->formFactory->create('test.inquiry.log', InquiryFormManager::class);
            $form->addField('short_text', 'field_name', 'Name');
            $form->saveNow();
            $form->install();
            $this->recordId = $form->insert(['field_name' => 'Field test']);
        }
    }

    /**
     * testing listing
     */
    public function testInquiryFormLogListing()
    {
        self::$admin->request('GET', '/api/v1/inquiry_form_logs');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing user listing
     */
    public function testInquiryFormLogUserListing()
    {
        $session = $this->loginRest('Supervisor');
        $session->request('GET', '/api/v1/inquiry_form_logs');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing sort listing.
     */
    public function testInquiryFormLogSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'message' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/inquiry_form_logs', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'message' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/inquiry_form_logs', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testInquiryFormLogPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/inquiry_form_logs', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing detection
     */
    public function testInquiryFormLogShowing()
    {
        self::$admin->request('GET', '/api/v1/inquiry_form_logs/01G3VFNTGWWPJ9C33BVVKWPD11');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Insert form record 1', $data['message']);
    }

    /**
     * testing detection
     */
    public function testInquiryFormLogUserShowing()
    {
        $session = $this->loginRest('Supervisor');
        $session->request('GET', '/api/v1/inquiry_form_logs/01G3VFNTGWWPJ9C33BVVKWPD11');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Insert form record 1', $data['message']);
        $this->assertEquals('/api/v1/countries/hn', $data['country']);
    }

    /**
     * testing creation
     */
    public function testInquiryFormLogPosting()
    {
        $payload = $this->payload;
        $payload['formId'] = "test.inquiry.log";
        $payload['recordId'] = $this->recordId;
        self::$admin->request('POST', '/api/v1/inquiry_form_logs', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing user creation
     */
    public function testInquiryFormLogUserPosting()
    {
        $payload = $this->payload;
        $payload['formId'] = "test.inquiry.log";
        $payload['recordId'] = $this->recordId;

        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/inquiry_form_logs', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testInquiryFormLogWithoutFormIdPosting()
    {
        $this->sessionService->resetSession();
        $session = $this->loginRest('Pedro');
        $session->request('POST', '/api/v1/inquiry_form_logs', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals("The logger requires a context 'form_id'", $data['detail']);
    }

    /**
     * testing creation
     */
    public function testInquiryFormLogWithFormIdInContextPosting()
    {
        $payload = $this->payload;
        $context = $payload['context'];
        $context['form_id'] = "test.inquiry.log";
        $context['record_id'] = $this->recordId;
        $payload['context'] = $context;

        self::$admin->request('POST', '/api/v1/inquiry_form_logs', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation with invalid form_id
     */
    public function testInquiryFormLogWithInvalidFormIdPosting()
    {
        $payload = $this->payload;
        $payload['formId'] = "invalid.form";
        $payload['recordId'] = $this->recordId;

        self::$admin->request('POST', '/api/v1/inquiry_form_logs', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(500, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals("Form 'invalid.form' not found.", $data['detail']);
    }
}
