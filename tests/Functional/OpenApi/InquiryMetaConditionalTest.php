<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Forms\FormFactory;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\CountryRepository;
use App\Tests\Functional\OpenApi\ApiTestBase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * test inquiry forms field meta conditional.
 */
class InquiryMetaConditionalTest extends ApiTestBase
{
    protected ?FormFactory $formFactory;
    protected ?EntityManager $em;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->formFactory = self::$container->get('form_factory');
        $this->em = self::$container->get('doctrine.orm.entity_manager');

        $form = $this->formFactory->create('meta.visible', InquiryFormManager::class);
        $form->addField('short_text', 'name', 'Name');
        $form->addField('short_text', 'label', 'Label')
            ->setMeta(
                [
                    'conditional' => [
                        'visible' => [
                            'or' => [
                                'field_name' => 'Drupal',
                            ],
                        ],
                    ],
                ]
            );
        $form->saveNow();
        $form->uninstall();
        $form->install();
    }

    /**
     * Test that filter record content by country level 1.
     */
    public function testConditionalVisibleFilter()
    {
        // Transit to finished state.
        $form = $this->formFactory->find('meta.visible');
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_name' => 'Symfony',
                'field_label' => 'Symfony',
            ]
        );
        // Validate not visible update.
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertNotEmpty($record->{'field_name'});
        $this->assertEquals('Symfony', $record->{'field_name'});
        $this->assertEmpty($record->{'field_label'});
        // Validate visible update.
        $record->{'field_status'} = 'draft';
        $record->save();
        $record->{'field_name'} = 'Drupal';
        $record->{'field_label'} = 'Drupal';
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertNotEmpty($record->{'field_name'});
        $this->assertEquals('Drupal', $record->{'field_name'});
        $this->assertNotEmpty($record->{'field_label'});
        $this->assertEquals('Drupal', $record->{'field_label'});
    }

    /**
     * Test that filter record content by country level 1.
     */
    public function testConditionalOptionFilterFail()
    {
        // Create form.
        $form = $this->formFactory->create('meta.option.fail', InquiryFormManager::class);
        $form->addField('short_text', 'name', 'Name');
        $form->addField(
            'select',
            'options',
            'Options',
            [
                'options' => [
                    1 => 'For Drupal',
                    2 => 'For Symfony',
                ],
            ]
        )
            ->setMeta(
                [
                    'conditional' => [
                        'option' => [
                            '1' => [
                                'or' => [
                                    'field_name' => 'Drupal',
                                ],
                            ],
                        ],
                    ],
                ]
            );
        $form->saveNow();
        $form->install();
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        // Create record.
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF_',
                'field_name' => 'Symfony',
                'field_options' => 1,
            ]
        );
        // Prepare exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.meta.option.fail->field_options] The selected option, 1, it\'s not visible.');

        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
    }

    /**
     * Test that filter record content by country level 1.
     */
    public function testConditionalOptionFilterOk()
    {
        // Create form.
        $form = $this->formFactory->create('meta.option.ok', InquiryFormManager::class);
        $form->addField('short_text', 'name', 'Name');
        $form->addField(
            'select',
            'options',
            'Options',
            [
                'options' => [
                    1 => 'For Drupal',
                    2 => 'For Symfony',
                ],
            ]
        )
            ->setMeta(
                [
                    'conditional' => [
                        'option' => [
                            '1' => [
                                'or' => [
                                    'field_name' => 'Drupal',
                                ],
                            ],
                        ],
                    ],
                ]
            );
        $form->saveNow();
        $form->install();
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        // Create record.
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF_',
                'field_name' => 'Drupal',
                'field_options' => 1,
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate .
        $this->assertEquals(1, $record->{'field_options'});
        $this->assertEquals('Drupal', $record->{'field_name'});
        $this->assertEquals('finished', $record->{'field_status'});
    }

    /**
     * Test that if we change a field the status transit from finished to draft.
     *
     * @throws \Exception
     */
    public function testAutoDownFromFinishToDraft()
    {
        $form = $this->formFactory->find('meta.visible');
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_name' => 'Symfony',
                'field_label' => 'Symfony',
            ]
        );
        // Transit to finished.
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Do a change to go to draft.
        $record->{'field_name'} = 'Drupal';
        $record->save();
        // Validate.
        $this->assertEquals('draft', $record->{'field_status'});
    }

    /**
     * Test subform with parent conditional.
     *
     * @throws \Exception
     */
    public function testParentFormReferenceClear(): void
    {
        // Create subform.
        $subform = $this->formFactory->create('test.parent.subform', SubformFormManager::class);
        $subform->setMeta(['parent_form' => 'form.test.parent']);
        $subform->addField('short_text', 'name', 'Name');
        $subform->addField('short_text', 'label', 'label')
            ->setMeta(
                [
                    'conditional' => [
                        'visible' => [
                            'or' => [
                                'parent.field_with_label' => true,
                            ],
                        ],
                    ],
                ]
            );
        $subform->saveNow();
        $subform->install();
        // Create form.
        $form = $this->formFactory->create('test.parent', InquiryFormManager::class);
        $form->addField('boolean', 'with_label', 'Have label');
        $form->addField(
            'subform_reference',
            'subform',
            'Subform',
            [
                'multivalued' => true,
                'subform' => 'form.test.parent.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        // Add records.
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $sfRecordId = $subform->insert(
            [
                'field_country' => $country,
                'field_name' => 'CMS',
                'field_label' => 'Drupal',
            ]
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_subform' => [['value' => $sfRecordId, 'form' => 'form.test.parent.subform']],
                'field_with_label' => false,
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate.
        $subRecord = $subform->find($sfRecordId);
        // Assert that subform it's clean.
        $this->assertEmpty($subRecord->{'field_label'});
    }

    /**
     * Test subform with parent conditional.
     *
     * @throws \Exception
     */
    public function testParentFormReferenceNotClear(): void
    {
        // Create subform.
        $subform = $this->formFactory->create('test.parent.subform', SubformFormManager::class);
        $subform->setMeta(['parent_form' => 'form.test.parent']);
        $subform->addField('short_text', 'name', 'Name');
        $subform->addField('short_text', 'label', 'label')
            ->setMeta(
                [
                    'conditional' => [
                        'visible' => [
                            'or' => [
                                'parent.field_with_label' => true,
                            ],
                        ],
                    ],
                ]
            );
        $subform->saveNow();
        $subform->install();
        // Create form.
        $form = $this->formFactory->create('test.parent', InquiryFormManager::class);
        $form->addField('boolean', 'with_label', 'Have label');
        $form->addField(
            'subform_reference',
            'subform',
            'Subform',
            [
                'multivalued' => true,
                'subform' => 'form.test.parent.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        // Add records.
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $sfRecordId = $subform->insert(
            [
                'field_country' => $country,
                'field_name' => 'CMS',
                'field_label' => 'Drupal',
            ]
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_subform' => [['value' => $sfRecordId, 'form' => 'form.test.parent.subform']],
                'field_with_label' => true,
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate.
        $subRecord = $subform->find($sfRecordId);
        // Assert that subform it's clean.
        $this->assertNotEmpty($subRecord->{'field_label'});
    }

    /**
     * Test subform with child conditional.
     *
     * @throws \Exception
     */
    public function testChildFormReferenceClear(): void
    {
        // Create subform.
        $subform = $this->formFactory->create('test.child.subform', SubformFormManager::class);
        $subform->setMeta(['parent_form' => 'form.test.child']);
        $subform->addField('short_text', 'name', 'Name');
        $subform->addField('short_text', 'label', 'label');
        $subform->saveNow();
        $subform->install();
        // Create form.
        $form = $this->formFactory->create('test.child', InquiryFormManager::class);
        $form->addField(
            'subform_reference',
            'subform',
            'Subform',
            [
                'multivalued' => true,
                'subform' => 'form.test.child.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->addField('short_text', 'name', 'Name')
            ->setMeta(
                [
                    'conditional' => [
                        'visible' => [
                            'or' => [
                                'field_subform.field_name' => 'Drupal',
                            ],
                        ],
                    ],
                ]
            );
        $form->saveNow();
        $form->install();
        // Add records.
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $sfRecordId = $subform->insert(
            [
                'field_country' => $country,
                'field_name' => 'CMS',
                'field_label' => 'Drupal',
            ]
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_subform' => [['value' => $sfRecordId, 'form' => 'form.test.child.subform']],
                'field_name' => 'Nombre',
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate.
        // form_test.child->field_name'll have value if test.child.subform->field_subform.field_name == 'Drupal'.
        // Assert that form it's clean.
        $this->assertEmpty($record->{'field_name'});
    }

    /**
     * Test subform with child conditional.
     *
     * @throws \Exception
     */
    public function testChildFormReferenceNotClear(): void
    {
        // Create subform.
        $subform = $this->formFactory->create('test.child.subform', SubformFormManager::class);
        $subform->setMeta(['parent_form' => 'form.test.child']);
        $subform->addField('short_text', 'name', 'Name');
        $subform->addField('short_text', 'label', 'label');
        $subform->saveNow();
        $subform->install();
        // Create form.
        $form = $this->formFactory->create('test.child', InquiryFormManager::class);
        $form->addField(
            'subform_reference',
            'subform',
            'Subform',
            [
                'multivalued' => true,
                'subform' => 'form.test.child.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->addField('short_text', 'name', 'Name')
            ->setMeta(
                [
                    'conditional' => [
                        'visible' => [
                            'or' => [
                                'field_subform.field_name' => 'Drupal',
                            ],
                        ],
                    ],
                ]
            );
        $form->saveNow();
        $form->install();
        // Add records.
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $sfRecordId = $subform->insert(
            [
                'field_country' => $country,
                'field_name' => 'Drupal',
                'field_label' => 'Drupal',
            ]
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_subform' => [['value' => $sfRecordId, 'form' => 'form.test.child.subform']],
                'field_name' => 'Nombre',
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate.
        // form_test.child->field_name'll have value if test.child.subform->field_subform.field_name == 'Drupal'.
        // Assert that form it's clean.
        $this->assertNotEmpty($record->{'field_name'});
    }
}
