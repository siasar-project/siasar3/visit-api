<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Entity\AdministrativeDivision;
use App\Entity\Configuration;
use App\Entity\Country;
use App\Forms\FormFactory;
use App\Forms\InquiryFormManager;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\CountryRepository;
use App\Tests\Functional\OpenApi\ApiTestBase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * test inquiry forms is clear and validated before transit to finished state.
 */
class InquiryToFinishedTransitionTest extends ApiTestBase
{
    protected ?FormFactory $formFactory;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->formFactory = self::$container->get('form_factory');

        $form = $this->formFactory->create('level.filter', InquiryFormManager::class);
        $form->setMeta(['allow_sdg' => true]);
        $form->addField('short_text', 'level1', 'level1')->setMeta(['level' => 1]);
        $form->addField('short_text', 'level2', 'level2')->setMeta(['level' => 2]);
        $form->addField('short_text', 'level3', 'level3')->setMeta(['level' => 3]);
        $form->addField('short_text', 'level3sdg', 'level 3 + SDG')->setMeta(['level' => 3, 'sdg' => true]);
        $form->saveNow();
        $form->uninstall();
        $form->install();
    }

    /**
     * Test that filter record content by country level 1.
     */
    public function testFormFilterByLevel1()
    {
        // Set base fields and set country level to 1.
        $recordId = $this->setLevelFields('form.level.filter', 1);
        // Transit to finished state.
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate filter
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $this->assertNotEmpty($record->{'field_level1'});
        $this->assertEmpty($record->{'field_level2'});
        $this->assertEmpty($record->{'field_level3'});
        $this->assertEmpty($record->{'field_level3sdg'});
        $this->assertEquals('finished', $record->{'field_status'});
    }

    /**
     * Test that filter record content by country level 2.
     */
    public function testFormFilterByLevel2()
    {
        // Set base fields and set country level to 2.
        $recordId = $this->setLevelFields('form.level.filter', 2);
        // Transit to finished state.
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate filter
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $this->assertNotEmpty($record->{'field_level1'});
        $this->assertNotEmpty($record->{'field_level2'});
        $this->assertEmpty($record->{'field_level3'});
        $this->assertEmpty($record->{'field_level3sdg'});
        $this->assertEquals('finished', $record->{'field_status'});
    }

    /**
     * Test that filter record content by country level 3.
     */
    public function testFormFilterByLevel3()
    {
        // Set base fields and set country level to 3.
        $recordId = $this->setLevelFields('form.level.filter', 3);
        // Transit to finished state.
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate filter
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $this->assertNotEmpty($record->{'field_level1'});
        $this->assertNotEmpty($record->{'field_level2'});
        $this->assertNotEmpty($record->{'field_level3'});
        $this->assertEmpty($record->{'field_level3sdg'});
        $this->assertEquals('finished', $record->{'field_status'});
    }

    /**
     * Test that filter record content by country SDG.
     */
    public function testFormFilterBySDG()
    {
        // Set base fields and set country level to 3.
        $recordId = $this->setLevelFields('form.level.filter', 3, true);
        // Transit to finished state.
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Validate filter
        $form = $this->formFactory->find('level.filter');
        $record = $form->find($recordId);
        $this->assertNotEmpty($record->{'field_level1'});
        $this->assertNotEmpty($record->{'field_level2'});
        $this->assertNotEmpty($record->{'field_level3'});
        $this->assertNotEmpty($record->{'field_level3sdg'});
        $this->assertEquals('finished', $record->{'field_status'});
    }
    /**
     * Set fields content.
     *
     * @param string $formId
     * @param int    $countryLevel
     * @param bool   $sdg
     *
     * @return string
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function setLevelFields(string $formId, int $countryLevel, bool $sdg = false): string
    {
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        // Each level and SDG to test have a fixture country with the configuration required.
        // We can't change country entity data while testing.
        /** @var Country $country */
        $country = $countryRepo->find('hn'.($countryLevel > 1 ? $countryLevel : '').($sdg ? 'sdg' : ''));
        $levels = $country->getFormLevel();
        // This is a test artifact fix.
        if (isset($levels["form.community"])) {
            // Remove not related "form.community" level.
            unset($levels["form.community"]);
        }
        // The next 4 code line don't work.
        $levels[$formId]['level'] = $countryLevel;
        $levels[$formId]['sdg'] = $sdg;
        $country->setFormLevel($levels);
        $countryRepo->saveNow($country);
        // Set level fields.
        $form = $this->formFactory->find('level.filter');

        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );

        return $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF',
                'field_level1' => '1',
                'field_level2' => '2',
                'field_level3' => '3',
                'field_level3sdg' => 'sdg',
            ]
        );
    }
}
