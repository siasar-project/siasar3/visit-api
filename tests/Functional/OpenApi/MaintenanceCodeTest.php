<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Forms\FormFactory;
use App\Service\ConfigurationService;
use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tests\Unit\Service\User\RoleTestBase;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * test Form Structure endpoint
 */
class MaintenanceCodeTest extends ApiTestBase
{
    protected ?FormFactory $formFactory;
    protected ?ConfigurationService $configurationService;

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->formFactory = self::$container->get('form_factory');
        $this->configurationService = self::$container->get('configuration_service');
        $form = $this->formFactory->create('endpoint');
        $form->saveNow();
        $form->install();
    }

    /**
     * testing stop user with maintenance mode.
     */
    public function testUserMaintenanceModeOn()
    {
        $session = $this->loginRest('Digitizer');
        $this->loginUser('Digitizer');

        $this->configurationService->setMaintenanceMode(true);
        $session->request('GET', '/api/v1/administrative_divisions');
        $response = $session->getResponse();
        $this->assertEquals(503, $response->getStatusCode());
        $data = $response->getContent();
        $this->assertNotEmpty($data);
        $this->assertEquals('"This site is in maintenance mode."', $data);
    }

    /**
     * testing allow user without maintenance mode.
     */
    public function testUserMaintenanceModeOff()
    {
        $session = $this->loginRest('Digitizer');
        $this->loginUser('Digitizer');

        $this->configurationService->setMaintenanceMode(false);
        $session->request('GET', '/api/v1/administrative_divisions');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing allow user with maintenance mode.
     */
    public function testAdminMaintenanceModeOn()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        $this->configurationService->setMaintenanceMode(true);
        $session->request('GET', '/api/v1/administrative_divisions');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RF', $data[0]['id']);
        $this->assertEquals('/api/v1/countries/hn', $data[0]['country']);
    }

    /**
     * testing maintenance mode the end-points.
     */
    public function testAdminMaintenanceModeOnEndPoints()
    {
        $session = $this->loginRest('Pedro');
        $this->loginUser('Pedro');

        // Active maintenance mode.
        $session->request('POST', '/api/v1/configuration/maintenance-mode', [], [], [], \json_encode(['active' => true]));
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($this->configurationService->inMaintenanceMode());

        // Inactive maintenance mode.
        $session->request('POST', '/api/v1/configuration/maintenance-mode', [], [], [], \json_encode(['active' => false]));
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertFalse($this->configurationService->inMaintenanceMode());
    }
}
