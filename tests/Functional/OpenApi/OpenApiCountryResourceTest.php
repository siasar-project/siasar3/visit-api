<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\OpenApi;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Forms\InquiryFormManager;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\CountryRepository;
use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tools\Json;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * test file endpoint
 */
class OpenApiCountryResourceTest extends ApiTestBase
{
    /**
     * testing listing
     */
    public function testOpenApiCountryListing()
    {
        // Dummy form.
        $form = $this->formFactory->create('dummy.levels', InquiryFormManager::class);
        $form->saveNow();

        self::$admin->request('GET', '/api/v1/countries');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
        foreach ($data as $aCountry) {
            if ('hn' === $aCountry['code']) {
                $this->assertEquals('Honduras', $aCountry["name"]);
                $this->assertCount(2, $aCountry["languages"]);
                foreach ($aCountry["languages"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["materials"]);
                foreach ($aCountry["materials"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["funderInterventions"]);
                foreach ($aCountry["funderInterventions"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["typeInterventions"]);
                foreach ($aCountry["typeInterventions"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["institutionInterventions"]);
                foreach ($aCountry["institutionInterventions"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(4, $aCountry["divisionTypes"]);
                foreach ($aCountry["divisionTypes"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertEquals(4, $aCountry["deep"]);
                $this->assertCount(3, $aCountry["mainOfficialLanguage"]);
                $this->assertArrayHasKey('id', $aCountry["mainOfficialLanguage"]);
                $this->assertEquals('01FH5AHS6A6NC4Q1NT6TNJ3P3G', $aCountry["mainOfficialLanguage"]["id"]);
                $this->assertCount(2, $aCountry["officialLanguages"]);
                foreach ($aCountry["officialLanguages"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(3, $aCountry["ethnicities"]);
                foreach ($aCountry["ethnicities"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["currencies"]);
                foreach ($aCountry["currencies"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["programInterventions"]);
                foreach ($aCountry["programInterventions"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
                $this->assertCount(2, $aCountry["technicalAssistanceProviders"]);
                foreach ($aCountry["technicalAssistanceProviders"] as $item) {
                    $this->assertArrayHasKey('id', $item);
                }
//                $this->assertCount(2, $aCountry["administrativeDivisions"]);
//                foreach ($aCountry["administrativeDivisions"] as $item) {
//                    $this->assertArrayHasKey('id', $item);
//                }
                $this->assertEquals(1, $aCountry["formLevel"]["form.dummy.levels"]["level"]);
                $this->assertEquals('Level 1: Standard', $aCountry["formLevel"]["form.dummy.levels"]["label"]);
            }
        }
    }

    /**
     * Test that we can set an invalid form level.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSetFormLevelFail(): void
    {
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        $country->setFormLevel(['form.formlevel.fail' => ['level' => 2]]);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Form "form.formlevel.fail" not found, country "hn" level not valid.');

        $countryRepo->saveNow($country);
    }

    /**
     * Test that we can set an invalid form level.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSetFormLevelValueFail(): void
    {
        $form = $this->formFactory->create('formlevel.fail', InquiryFormManager::class);
        $form->saveNow();
        $form->install();

        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        $country->setFormLevel(['form.formlevel.fail' => ['level' => 4]]);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Country "hn" level "4" not valid to form "form.formlevel.fail".');

        $countryRepo->saveNow($country);
    }

    /**
     * Test that we can set a valid form level.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSetFormLevelValueFailByRecord(): void
    {
        $form = $this->formFactory->create('formlevel.fail', InquiryFormManager::class);
        $form->saveNow();
        $form->install();
        // Draft record to stop changing form level update.
        $form->insert([]);

        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        $country->setFormLevel(['form.formlevel.fail' => ['level' => 3]]);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Country "hn" level "3" not valid to form "form.formlevel.fail" because have not validated inquiries.');

        $countryRepo->saveNow($country);
    }

    /**
     * Test that we can set a valid form level without other Country conflicts.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSetFormLevelValueOkByRecordOtherCountry(): void
    {
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);

        $form = $this->formFactory->create('countrylevel.ok', InquiryFormManager::class);
        $form->saveNow();
        $form->install();
        // Draft record to stop changing form level update.
        $form->insert(
            [
                'field_country' => $countryRepo->find('ni'),
                'field_region' => $divisionRepo->find('01FSRXA7RH5X1PAKSS8RTPQ574'),
            ]
        );

        // Update.
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        $country->setFormLevel(['form.countrylevel.ok' => ['level' => 3]]);
        $countryRepo->saveNow($country);

        // Validate.
        $country = $countryRepo->find('hn');
        $levels = $country->getFormLevel();
        $this->assertArrayHasKey('form.countrylevel.ok', $levels);
        $this->assertEquals(3, $levels['form.countrylevel.ok']['level']);
    }

    /**
     * Test that we can set a valid form level.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSetFormLevelValueOk(): void
    {
        $form = $this->formFactory->create('formlevel.ok', InquiryFormManager::class);
        $form->saveNow();
        $form->install();
        // Draft record to stop changing form level update.
        $recordId = $form->insert([]);
        $record = $form->find($recordId);
        // Record finished status.
        $record->{'field_reference'} = 'AAA';
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $record->{'field_region'} = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $record->save();
        $record->{'field_status'} = 'finished';
        $record->save();
        // Record validated status.
        $record->{'field_status'} = 'validated';
        $record->save();

        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        $country->setFormLevel(['form.formlevel.ok' => ['level' => 3]]);
        $countryRepo->saveNow($country);

        $country = $countryRepo->find('hn');
        $levels = $country->getFormLevel();
        $this->assertEquals(3, $levels["form.formlevel.ok"]["level"]);
    }

    /**
     * Country update variations.
     *
     * @return array[]
     */
    public function countryUpdateUserRoleVariations(): array
    {
        return [
            ['status' => 401, 'user' => ''], // Anonymous
            ['status' => 200, 'user' => 'Pedro'], // Admin
            ['status' => 200, 'user' => 'AdminLocal'], // Admin local
            ['status' => 403, 'user' => 'Digitizer'], // Digitizer
            ['status' => 403, 'user' => 'Reader'], // Reader
            ['status' => 403, 'user' => 'SectorialGlobal'], // Sectorial global
            ['status' => 403, 'user' => 'SectorialLocal'], // Sectorial local
            ['status' => 403, 'user' => 'SectorialValidator'], // Sectorial Validator
            ['status' => 403, 'user' => 'Supervisor'], // Supervisor
            ['status' => 403, 'user' => 'Marta'], // User
        ];
    }

    /**
     * Test that a user role can, or not, update Country Form levels.
     *
     * @dataProvider countryUpdateUserRoleVariations
     *
     * @param $status
     * @param $user
     *
     * @throws \Exception
     */
    public function testAdminLocalCountryUpdate($status, $user): void
    {
        $form = $this->formFactory->create('security.fail', InquiryFormManager::class);
        $form->saveNow();
        $form->install();

        $session = $this->loginRest($user);

        $payload = [
            'formLevel' => [
                'form.security.fail' => [
                    'level' => 2,
                ],
            ],
        ];

        $session->request('PUT', '/api/v1/countries/hn', [], [], [], Json::encode($payload));
        $response = $session->getResponse();
        $this->assertEquals($status, $response->getStatusCode());
    }
}
