<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\OpenApi;

use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationRead;
use App\Entity\Country;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\SubformFormManager;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\ORM\EntityManager;
use App\Forms\FieldTypes\FieldTypeManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Open API query parameters tests.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class OpenApiQueryParamsTest extends ApiTestBase
{
    use TestFixturesTrait;

    protected string $adminUsername;
    protected string $adminPassword;
    protected ?FormFactory $formFactory;
    protected ?FieldTypeManager $fieldTypeManager;
    protected ?EntityManager $em;
    protected FormManagerInterface $form;
    protected array $fieldsSort;
    protected array $fieldsFilter;
    protected ParameterBagInterface $systemSettings;
    protected $itemsPerPage = 2;

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->extraConfiguration = [
            'form.wsprovider.contact' => dirname(__FILE__).'/../../assets/form.wsprovider.contact.yml',
            'form.wsprovider.tap' => dirname(__FILE__).'/../../assets/form.wsprovider.tap.yml',
            'form.wsprovider' => dirname(__FILE__).'/../../assets/form.wsprovider.yml',
            'form.wssystem.communities' => dirname(__FILE__).'/../../assets/form.wssystem.communities.yml',
            'form.wssystem.contact' => dirname(__FILE__).'/../../assets/form.wssystem.contact.yml',
            'form.wssystem.intakeflow' => dirname(__FILE__).'/../../assets/form.wssystem.intakeflow.yml',
            'form.wssystem.projects' => dirname(__FILE__).'/../../assets/form.wssystem.projects.yml',
            'form.wssystem.qualitybacteriological' => dirname(__FILE__).'/../../assets/form.wssystem.qualitybacteriological.yml',
            'form.wssystem.qualityphysiochemical' => dirname(__FILE__).'/../../assets/form.wssystem.qualityphysiochemical.yml',
            'form.wssystem.residualtests' => dirname(__FILE__).'/../../assets/form.wssystem.residualtests.yml',
            'form.wssystem.sourceflow' => dirname(__FILE__).'/../../assets/form.wssystem.sourceflow.yml',
            'form.wssystem.transmisionline' => dirname(__FILE__).'/../../assets/form.wssystem.transmisionline.yml',
            'form.wssystem.treatmentpoints' => dirname(__FILE__).'/../../assets/form.wssystem.treatmentpoints.yml',
            'form.wssystem.storage' => dirname(__FILE__).'/../../assets/form.wssystem.storage.yml',
            'form.wssystem.distribution' => dirname(__FILE__).'/../../assets/form.wssystem.distribution.yml',
            'form.wssystem.sourceintake' => dirname(__FILE__).'/../../assets/form.wssystem.sourceintake.yml',
            'form.wssystem' => dirname(__FILE__).'/../../assets/form.wssystem.yml',
            'form.community.intervention' => dirname(__FILE__).'/../../assets/form.community.intervention.yml',
            'form.community.contact' => dirname(__FILE__).'/../../assets/form.community.contact.yml',
            'form.community' => dirname(__FILE__).'/../../assets/form.community.yml',
            'form.point' => dirname(__FILE__).'/../../assets/form.point.yml',
            'form.mail' => dirname(__FILE__).'/../../assets/form.mail.yml',
        ];

        parent::setUp();
        self::bootKernel();

        $this->endpoint = '/api/v1';
        $this->adminUsername = 'Pedro';
        $this->adminPassword = '123456';
        $this->formFactory = self::$container->get('form_factory');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->em = self::$container->get('doctrine.orm.entity_manager');
        $this->systemSettings = self::$container->getParameterBag();

        $this->loginUser('pedro');
        // Init WSystem form.
        $this->installForm('form.wssystem.communities');
        $this->installForm('form.wssystem.contact');
        $this->installForm('form.wssystem.intakeflow');
        $this->installForm('form.wssystem.projects');
        $this->installForm('form.wssystem.qualitybacteriological');
        $this->installForm('form.wssystem.qualityphysiochemical');
        $this->installForm('form.wssystem.residualtests');
        $this->installForm('form.wssystem.sourceflow');
        $this->installForm('form.wssystem.transmisionline');
        $this->installForm('form.wssystem.sourceintake');
        $this->installForm('form.wssystem.treatmentpoints');
        $this->installForm('form.wssystem.storage');
        $this->installForm('form.wssystem.distribution');
        $form = $this->installForm('form.wssystem');
        $form->insert([]);
        // Init WSProvider form.
        $this->installForm('form.wsprovider.contact');
        $this->installForm('form.wsprovider.tap');
        $form = $this->installForm('form.wsprovider');
        $form->install();
        // Add a record to have an example record.
        $form->insert([]);
        // Init Community form.
        $this->installForm('form.community.intervention');
        $this->installForm('form.community.contact');
        $form = $this->formFactory->find('form.community');
        $form->install();
        // Add a record to have an example record.
        $commId = $form->insert([]);
        // Install mail form.
        $this->installForm('form.mail');
        $form = $this->formFactory->find('form.mail');
        $form->install();
        // Add a record to have an example record.
        $form->insert([
            'field_subject' => 'Hola',
            'field_body' => 'Hola',
            'field_from' => ['value' => '01FHX4RK866FRCDAYYYCZKVFXJ', 'class' => 'App\Entity\User'],
            'field_to' => [
                ['value' => '01FHX4RK866FRCDAYYYCZKVFXJ', 'class' => 'App\Entity\User'],
            ],
        ]);
        // Init Point form.
        $form = $this->installForm('form.point');
        // Add a record to have an example record.
        $countryRepo = $this->em->getRepository(Country::class);
        $form->insert(
            [
                'field_country' => $countryRepo->find('hn'),
                'field_communities' => [
                    [
                        'value' => $commId,
                        'form' => 'form.community',
                    ],
                ],
            ]
        );

        // Subform.
        $form = $this->formFactory->create('form.subform.hhp', SubformFormManager::class);
        $form->addField('integer', 'Reference', 'Record reference');
        $form->setMeta(['parent_form' => 'form.all.field.type']);
        $form->saveNow();
        $form->uninstall();
        $form->install();
        $form->insert(['field_country' => $countryRepo->find('hn')]);

        // Basic test form.
        $this->fieldsSort = [];
        $this->fieldsFilter = [];
        if (!$this->formFactory->exist('all.field.type')) {
            $this->form = $this->formFactory->create('all.field.type');
        } else {
            $this->form = $this->formFactory->findEditable('all.field.type');
        }
        // Add fields.
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        $order = 0;
        /**
         * @var string $fieldTypeId
         * @var FieldTypeInterface $fieldType
         */
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            // Single field.
            ++$order;
            $options = [
                'weight' => $order,
                'meta' => [
                    'catalog_id' => 's_'.$i,
                ],
            ];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform.hhp';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'map_point':
                    $this->form->addField('decimal', 'lon', 'location_lon');
                    $this->form->addField('decimal', 'lat', 'location_lat');
                    $options['lat_field'] = 'field_lat';
                    $options['lon_field'] = 'field_lon';
                    break;
                case 'editor_update':
                    $this->form->addField('user_reference', 'ur', 'ur');
                    $this->form->addField('date', 'updated', 'date');
                    $options['editor_field'] = 'field_ur';
                    $options['update_field'] = 'field_updated';
                    break;
            }
            $field = $this->form->addField($fieldTypeId, $fieldTypeId, 'Single field '.$fieldTypeId, $options);
            $options = $field->getSettings();
            if ($field->isSortable()) {
                $options['settings']['sort'] = true;
                $this->fieldsSort[] = $field->getId();
            }
            if ($field->isFilterable()) {
                $options['settings']['filter'] = true;
                $this->fieldsFilter[] = $field->getId();
            }
            // Multivalued field.
            ++$order;
            $options = [
                'weight' => $order,
                'multivalued' => true,
                'meta' => [
                    'catalog_id' => 'm_'.$i,
                ],
            ];
            // Set required field options.
            $continueFor = false;
            switch ($fieldTypeId) {
                case 'publishing_status':
                case 'inquiring_status':
                case 'pointing_status':
                case 'map_point':
                case 'editor_update':
                    $continueFor = true;
                    break;
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform.hhp';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_country_reference';
                    break;
            }
            //
            ++$i;
            if ($continueFor) {
                continue;
            }
            $this->form->addField($fieldTypeId, 'm_'.$fieldTypeId, 'Multivalued field '.$fieldTypeId, $options);
        }
        // Install.
        $this->form->saveNow();
        $this->form->install();
        $this->cleanForm($this->form);
        // Load dummy data.
        $fields = $this->form->getFields();
        // Add dummy records.
        //$itemsPerPage = 3 + $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
        for ($i = 0; $i < ($this->itemsPerPage + 1); ++$i) {
            $recordData = [];
            foreach ($fields as $field) {
                if ($field->getId() === 'field_country_reference') {
                    $recordData[$field->getId()] = $field->getExampleData($recordData);
                    break;
                }
            }
            /** @var FieldTypeInterface $field */
            foreach ($fields as $field) {
                if ($field->isMultivalued()) {
                    $recordData[$field->getId()] = [
                        $field->getExampleData($recordData),
                        $field->getExampleData($recordData),
                    ];
                } else {
                    $recordData[$field->getId()] = $field->getExampleData($recordData);
                }
            }
            $this->form->insert($recordData);
        }
    }

    /**
     * Test sort parameters.
     */
    public function testSortAndFilterParameters():void
    {
        // Cut fields here to debug a simple field type.
//        $this->fieldsSort = ["field_entity_reference"];
//        $this->fieldsFilter = ["field_entity_reference"];
        // We will run all tests here to not repopulate form each time.
        $order = [];
        foreach ($this->fieldsSort as $fieldName) {
            $order[] = [
                [$fieldName, 'asc'],
            ];
            $order[] = [
                [$fieldName, 'desc'],
            ];
        }
        $filter = [];
        $operators = ['', 'between', 'lt', '<', 'gt', '>', 'lte', '<=', '>=', 'exists', 'not_exists'];
        $operatorsDate = ['', 'after', 'before', 'strictly_after', 'strictly_before'];
        foreach ($this->fieldsFilter as $fieldName) {
            $field = $this->form->getFieldDefinition($fieldName);
            // Filter to not date' fields.
            foreach ($operators as $operator) {
                if ('date' !== $field->getFieldType()) {
                    switch ($operator) {
                        case 'between':
                            $example = $field->getExampleData();
                            $example1 = $field->getExampleData();
                            $filter[] = [
                                [$fieldName, $operator, current($example).'..'.current($example1)],
                            ];
                            break;
                        case 'exists':
                        case 'not_exists':
                            // deprecated: ?property[exists]=<true|false|1|0>
                            $filter[] = [
                                [$fieldName, $operator, true],
                            ];
                            // deprecated: ?property[exists]=<true|false|1|0>
                            $filter[] = [
                                [$fieldName, $operator, false],
                            ];
                            // ?exists[property]=<true|false|1|0>
                            $filter[] = [
                                [$operator, $fieldName, true],
                            ];
                            // ?exists[property]=<true|false|1|0>
                            $filter[] = [
                                [$operator, $fieldName, false],
                            ];
                            break;
                        default:
                            $example = $field->getExampleData();
                            $filter[] = [
                                [$fieldName, $operator, current($example)],
                            ];
                            break;
                    }
                    unset($example);
                    unset($example1);
                }
            }
            // Filter to date' fields.
            foreach ($operatorsDate as $operator) {
                if ('date' === $field->getFieldType()) {
                    $example = $field->getExampleData();
                    // At this point we don't need known about date subfields, or properties.
                    $filter[] = [
                        // We use the field name, not the field value column.
                        [$fieldName, $operator, current($example)],
                    ];
                }
            }
        }

        // Test order.
        foreach ($order as $params) {
            $apiResp = $this->queryRestApi($params);
            /** @var FormRecord[] $database */
            $database = $this->queryDatabase($params);
            $this->assertEquals(count($database), count($apiResp), print_r($params, true));
            // Result ID must be the same, in same order.
            foreach ($apiResp as $index => $result) {
                $this->assertEquals($database[$index]->getId(), $result['id']);
            }
        }
        // Test filter.
        foreach ($filter as $params) {
            $apiResp = $this->queryRestApi([], $params);
            /** @var FormRecord[] $database */
            $database = $this->queryDatabase([], $params);
            $this->assertEquals(count($database), count($apiResp), print_r($params, true));
            // Result ID must be the same, in same order.
            foreach ($apiResp as $index => $result) {
                $this->assertEquals($database[$index]->getId(), $result['id']);
            }
        }
        // Quick test with order and filter at same time.
        $apiResp = $this->queryRestApi($order[0], $filter[0]);
        /** @var FormRecord[] $database */
        $database = $this->queryDatabase($order[0], $filter[0]);
        $this->assertEquals(count($database), count($apiResp), print_r($order[0] + $filter[0], true));
        // Result ID must be the same, in same order.
        foreach ($apiResp as $index => $result) {
            $this->assertEquals($database[$index]->getId(), $result['id']);
        }
    }

    /**
     * Query using the form manager.
     *
     * @param array $order
     * @param array $filter
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    protected function queryDatabase(array $order = [], array $filter = []): array
    {
        //$itemsPerPage = $this->systemSettings->get('api_platform.collection.pagination.items_per_page');
        $flat = [];
        foreach ($order as $item) {
            $flat[$item[0]] = $item[1];
        }
        $flatFilter = [];
        foreach ($filter as $param) {
            if (!empty($param[1])) {
                $flatFilter[$param[0]] = [
                    $param[1] => $param[2],
                ];
            } else {
                $flatFilter[$param[0]] = $param[2];
            }
        }

        return $this->form->findBy($flatFilter, $flat, $this->itemsPerPage, 0);
    }

    /**
     * Query using Open API.
     *
     * @param array $order
     * @param array $filter
     *
     * @return array
     */
    protected function queryRestApi(array $order = [], array $filter = []): array
    {
        $parameters = [
            'page' => 1,
            'itemsPerPage' => $this->itemsPerPage,
        ];
        foreach ($order as $param) {
            $parameters["order[{$param[0]}]"] = $param[1];
        }
        foreach ($filter as $param) {
            $oper = '';
            if (!empty($param[1])) {
                $oper = "[{$param[1]}]";
            }
            $parameters["{$param[0]}$oper"] = $param[2];
        }
        parent::$admin->request('GET', $this->endpoint.'/form/data/all.field.types', $parameters);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $message = '';
        if (200 !== $response->getStatusCode()) {
            $message = $responseData["detail"];
        }
        $this->assertEquals(200, $response->getStatusCode(), $message."\n".print_r($parameters, true));

        return $responseData;
    }
}
