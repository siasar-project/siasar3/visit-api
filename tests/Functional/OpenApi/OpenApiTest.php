<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\OpenApi;

use App\Entity\AdministrativeDivision;
use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationRead;
use App\Entity\Country;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tools\Json;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Open API tests.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class OpenApiTest extends ApiTestBase
{
    use TestFixturesTrait;

    protected ?FormFactory $formFactory;
    protected ?FieldTypeManager $fieldTypeManager;
    protected ?EntityManager $em;

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->extraConfiguration = array_merge($this->extraConfiguration, [
            'form.wsprovider.contact' => dirname(__FILE__).'/../../assets/form.wsprovider.contact.yml',
            'form.wsprovider.tap' => dirname(__FILE__).'/../../assets/form.wsprovider.tap.yml',
            'form.wsprovider' => dirname(__FILE__).'/../../assets/form.wsprovider.yml',
            'form.wssystem.communities' => dirname(__FILE__).'/../../assets/form.wssystem.communities.yml',
            'form.wssystem.contact' => dirname(__FILE__).'/../../assets/form.wssystem.contact.yml',
            'form.wssystem.intakeflow' => dirname(__FILE__).'/../../assets/form.wssystem.intakeflow.yml',
            'form.wssystem.projects' => dirname(__FILE__).'/../../assets/form.wssystem.projects.yml',
            'form.wssystem.qualitybacteriological' => dirname(__FILE__).'/../../assets/form.wssystem.qualitybacteriological.yml',
            'form.wssystem.qualityphysiochemical' => dirname(__FILE__).'/../../assets/form.wssystem.qualityphysiochemical.yml',
            'form.wssystem.residualtests' => dirname(__FILE__).'/../../assets/form.wssystem.residualtests.yml',
            'form.wssystem.sourceflow' => dirname(__FILE__).'/../../assets/form.wssystem.sourceflow.yml',
            'form.wssystem.transmisionline' => dirname(__FILE__).'/../../assets/form.wssystem.transmisionline.yml',
            'form.wssystem.treatmentpoints' => dirname(__FILE__).'/../../assets/form.wssystem.treatmentpoints.yml',
            'form.wssystem.storage' => dirname(__FILE__).'/../../assets/form.wssystem.storage.yml',
            'form.wssystem.distribution' => dirname(__FILE__).'/../../assets/form.wssystem.distribution.yml',
            'form.wssystem.sourceintake' => dirname(__FILE__).'/../../assets/form.wssystem.sourceintake.yml',
            'form.wssystem' => dirname(__FILE__).'/../../assets/form.wssystem.yml',
            'form.community.contact' => dirname(__FILE__).'/../../assets/form.community.contact.yml',
            'form.community.intervention' => dirname(__FILE__).'/../../assets/form.community.intervention.yml',
            'form.community' => dirname(__FILE__).'/../../assets/form.community.yml',
            'form.point' => dirname(__FILE__).'/../../assets/form.point.yml',
            'form.mail' => dirname(__FILE__).'/../../assets/form.mail.yml',
        ]);
        parent::setUp();

        $this->endpoint = '/api/v1';
        $this->formFactory = self::$container->get('form_factory');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->em = self::$container->get('doctrine.orm.entity_manager');

        $this->loginUser('pedro');
        // Init WSystem form.
        $this->installForm('form.wssystem.communities');
        $this->installForm('form.wssystem.contact');
        $this->installForm('form.wssystem.intakeflow');
        $this->installForm('form.wssystem.projects');
        $this->installForm('form.wssystem.qualitybacteriological');
        $this->installForm('form.wssystem.qualityphysiochemical');
        $this->installForm('form.wssystem.residualtests');
        $this->installForm('form.wssystem.sourceflow');
        $this->installForm('form.wssystem.transmisionline');
        $this->installForm('form.wssystem.sourceintake');
        $this->installForm('form.wssystem.treatmentpoints');
        $this->installForm('form.wssystem.storage');
        $this->installForm('form.wssystem.distribution');
        $form = $this->installForm('form.wssystem');
        $form->insert([]);
        // Init WSProvider form.
        $this->installForm('form.wsprovider.contact');
        $this->installForm('form.wsprovider.tap');
        $form = $this->installForm('form.wsprovider');
        $form->install();
        // Add a record to have an example record.
        $form->insert([]);
        // Init Community form.
        $this->installForm('form.community.contact');
        $this->installForm('form.community.intervention');
        $form = $this->formFactory->find('form.community');
        $form->install();
        // Add a record to have an example record.
        $commId = $form->insert([]);
        // Init mail form.
        $this->installForm('form.mail');
        $form = $this->formFactory->find('form.mail');
        $form->install();
        // Add a record to have an example record.
        $form->insert([
            'field_subject' => 'Hola',
            'field_body' => 'Hola',
            'field_from' => ['value' => '01FHX4RK866FRCDAYYYCZKVFXJ', 'class' => 'App\Entity\User'],
            'field_to' => [
                ['value' => '01FHX4RK866FRCDAYYYCZKVFXJ', 'class' => 'App\Entity\User'],
            ],
        ]);
        // Init Point form.
        $form = $this->installForm('form.point');
        // Add a record to have an example record.
        $countryRepo = $this->em->getRepository(Country::class);
        $form->insert(
            [
                'field_country' => $countryRepo->find('hn'),
                'field_communities' => [
                    [
                        'value' => $commId,
                        'form' => 'form.community',
                    ],
                ],
            ]
        );

        // Subform.
        $form = $this->formFactory->create('form.subform.oat', SubformFormManager::class);
        $form->addField('integer', 'Reference', 'Record reference');
        $form->setMeta(['parent_form' => 'form.test.fieldtypes']);
        $form->saveNow();
        $form->uninstall();
        $form->install();
        $countryRepo = $this->em->getRepository(Country::class);
        $form->insert(['field_country' => $countryRepo->find('hn')]);

        // Basic test form.
        if (!$this->formFactory->exist('test.api')) {
            $form = $this->formFactory->create('test.api');
        } else {
            $form = $this->formFactory->findEditable('test.api');
        }
        $form->addField('short_text', 'name', 'Name');
        $form->addField('date', 'date', 'Date');
        $form->addField('integer', 'number', 'Number');
        $form->saveNow();
        $form->install();
        $this->cleanForm($form);

        // All fields types test form.
        $form = $this->formFactory->create('test.fieldtypes');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform.oat';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'map_point':
                    $form->addField('decimal', 'lon', 'location_lon');
                    $form->addField('decimal', 'lat', 'location_lat');
                    $options['lat_field'] = 'field_lat';
                    $options['lon_field'] = 'field_lon';
                    break;
                case 'editor_update':
                    $form->addField('user_reference', 'ur', 'ur');
                    $form->addField('date', 'updated', 'date');
                    $options['editor_field'] = 'field_ur';
                    $options['update_field'] = 'field_updated';
                    break;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
        }
        $form->saveNow();
        $form->install();
        $this->cleanForm($form);
    }

    /**
     * Test that we can insert a new form record.
     */
    public function testAddFormRecord():void
    {
        parent::$admin->request(
            'POST',
            $this->endpoint.'/form/data/form.test.apis',
            [],
            [],
            [],
            '{"field_name": "PSF", "field_date": {"value": "2021-06-29 15:01", "timezone": "Europe/Madrid"}}'
        );
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals('PSF', $responseData['field_name']);
        $this->assertEquals('2021-06-29 15:01:00', $responseData['field_date']['value']);
        $this->assertEquals('Europe/Madrid', $responseData['field_date']['timezone']);
    }

    /**
     * Test that we can't insert an invalid form record.
     */
    public function testAdd401FormRecord():void
    {
        parent::$admin->request(
            'POST',
            $this->endpoint.'/form/data/form.test.apis',
            [],
            [],
            [],
            '{"field_name": "PSF", "field_date": {"value": "Invalid date", "timezone": "Europe/Madrid"}}'
        );
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertIsString($responseData['message']);
    }

    /**
     * Test that we can list records.
     */
    public function testGetListFormRecord():void
    {
        $form = $this->formFactory->find('test.api');
        $newIds = [
            $form->insert(
                [
                    'field_name' => "Pedro",
                    'field_date' => [
                        'value' => '2021-06-29 17:16',
                        'timezone' => 'Europe/Madrid',
                    ],
                ]
            ),
            $form->insert(
                [
                    'field_name' => "Antonio",
                    'field_date' => [
                        'value' => '2021-06-29 17:16',
                        'timezone' => 'Europe/Madrid',
                    ],
                ]
            ),
        ];

        parent::$admin->request('GET', $this->endpoint.'/form/data/form.test.apis');
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());
        foreach ($responseData as $record) {
            $this->assertTrue(in_array($record['id'], $newIds));
        }
    }

    /**
     * Test that we can use list pagination.
     */
    public function testUseListPagination():void
    {
        $form = $this->formFactory->find('test.api');
        $newIds = $this->insertData();

        $pages = 0;
        do {
            $this->assertLessThan(4, $pages, 'Detected infinite pagination loop!');
            parent::$admin->request('GET', $this->endpoint.'/form/data/form.test.apis', ['page' => $pages + 1]);
            $response = parent::$admin->getResponse();
            $responseData = $this->getResponseData($response);
            $totalItems = count($responseData);

            $this->assertEquals(200, $response->getStatusCode());

            foreach ($responseData as $record) {
                $this->assertTrue(in_array($record['id'], $newIds));
            }
            ++$pages;
        } while ($totalItems > 0);
        $this->assertEquals(4, $pages);
    }

    /**
     * Test that we can get a record.
     */
    public function testGetItem(): void
    {
        $form = $this->formFactory->find('test.api');
        $newId = $form->insert(
            [
                'field_name' => "Pedro",
                'field_date' => [
                    'value' => '2021-06-29 17:16',
                    'timezone' => 'Europe/Madrid',
                ],
            ]
        );

        parent::$admin->request('GET', $this->endpoint.'/form/data/form.test.apis/'.$newId);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals($newId, $responseData['id']);
        $this->assertEquals('Pedro', $responseData['field_name']);
        $this->assertNull($responseData['field_number']);
        $this->assertEquals('2021-06-29 17:16:00', $responseData['field_date']['value']);
        $this->assertEquals('Europe/Madrid', $responseData['field_date']['timezone']);
    }

    /**
     * Test that we can update a record.
     */
    public function testUpdateItem(): void
    {
        $form = $this->formFactory->find('test.api');
        $newId = $form->insert(
            [
                'field_name' => "Pedro",
                'field_date' => [
                    'value' => '2021-06-29 10:16',
                    'timezone' => 'America/Tegucigalpa',
                ],
                'field_number' => 0,
            ]
        );

        // Update by RestFul.
        parent::$admin->request(
            'PUT',
            $this->endpoint.'/form/data/form.test.apis/'.$newId,
            [],
            [],
            [],
            '{"field_name": "PSF", "field_date": {"value": "2021-06-30 20:05:00", "timezone": "America/Tegucigalpa"}, "field_number": 10}'
        );
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        // Verify response.
        $this->assertEquals($newId, $responseData['id']);
        $this->assertEquals('PSF', $responseData['field_name']);
        $this->assertEquals('10', $responseData['field_number']);
        $this->assertEquals('2021-06-30 20:05:00', $responseData['field_date']['value']);
        $this->assertEquals('America/Tegucigalpa', $responseData['field_date']['timezone']);
        // Verify by direct read.
        $updated = $form->find($newId);
        $this->assertEquals($newId, $responseData['id']);
        $this->assertEquals($updated->{'field_name'}, $responseData['field_name']);
        $this->assertEquals($updated->{'field_number'}, $responseData['field_number']);
        $this->assertEquals($updated->get('field_date')['value']->format('Y-m-d H:i:s'), $responseData['field_date']['value']);
        $this->assertEquals($updated->get('field_date')['timezone'], $responseData['field_date']['timezone']);
    }

    /**
     * Test that we can delete a record.
     */
    public function testDeleteItem(): void
    {
        $form = $this->formFactory->find('test.api');
        $newId = $form->insert(
            [
                'field_name' => "Pedro",
                'field_date' => [
                    'value' => '2021-06-29 10:16',
                    'timezone' => 'America/Tegucigalpa',
                ],
                'field_number' => 0,
            ]
        );

        // Wrong form id.
        parent::$admin->request('DELETE', $this->endpoint.'/form/data/form.test.apis-NOT/'.$newId);
        $response = parent::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        // Delete by RestFul, but not.
        parent::$admin->request('DELETE', $this->endpoint.'/form/data/form.test.apis/'.$newId.'NOT');
        $response = parent::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        // Delete by RestFul.
        parent::$admin->request('DELETE', $this->endpoint.'/form/data/form.test.apis/'.$newId);
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        // Is it deleted?
        $record = $form->find($newId);
        $this->assertNull($record);
    }

    /**
     * Test that control invalid form IDs.
     */
    public function testBadFormIdItem(): void
    {
        // Add.
        parent::$admin->request(
            'POST',
            $this->endpoint.'/form/data/bad-form-id',
            [],
            [],
            [],
            '{"field_name": "PSF", "field_date": {"value": "2021-06-29 15:01", "timezone": "Europe/Madrid"}}'
        );
        $response = parent::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        // Collection.
        parent::$admin->request('GET', $this->endpoint.'/form/data/bad-form-id');
        $response = parent::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        // Get items.
        parent::$admin->request('GET', $this->endpoint.'/form/data/bad-form-id/bad-record-id');
        $response = parent::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        // Put items.
        parent::$admin->request(
            'PUT',
            $this->endpoint.'/form/data/bad-form-id/bad-record-id',
            [],
            [],
            [],
            '{"field_name": "PSF", "field_date": {"value": "2021-06-29 15:01", "timezone": "Europe/Madrid"}}'
        );
        $response = parent::$admin->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        // Delete items.
        parent::$admin->request('DELETE', $this->endpoint.'/form/data/bad-form-id/bad-record-id');
        $response = parent::$admin->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * Test that we can insert records without not required fields.
     *
     * @throws \Exception
     */
    public function testSimpleNotRequiredFieldUseDefaults(): void
    {
        // Prepare subform.
        $form = $this->formFactory->create('form.subform2', SubformFormManager::class);
        $form->addField('integer', 'Reference', 'Record reference');
        $form->setMeta(['parent_form' => 'form.test.restful.default']);
        $form->saveNow();
        $form->uninstall();
        $form->install();
        $countryRepo = $this->em->getRepository(Country::class);
        $form->insert(['field_country' => $countryRepo->find('hn')]);
        // Create form.
        $form = $this->formFactory->create('test.restful.default');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform2';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'map_point':
                    $form->addField('decimal', 'lon', 'location_lon');
                    $form->addField('decimal', 'lat', 'location_lat');
                    $options['lat_field'] = 'field_lat';
                    $options['lon_field'] = 'field_lon';
                    break;
                case 'editor_update':
                    $form->addField('user_reference', 'ur', 'ur');
                    $form->addField('date', 'updated', 'date');
                    $options['editor_field'] = 'field_ur';
                    $options['update_field'] = 'field_updated';
                    break;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
            ++$i;
        }
        $form->saveNow();
        $form->install();
        // Insert data.
        parent::$admin->request('POST', $this->endpoint.'/form/data/test.restful.defaults', [], [], [], '{}');
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $responseData = $this->getResponseData($response);
        // Verify data.
        $id = $responseData["id"];
        $this->assertIsString($id);
        $record = $form->find($id);
        $this->assertNotNull($record);
    }

    /**
     * Test that we can add a record with any field type.
     *
     * @throws \ReflectionException
     */
    public function testAddAllFieldtypes(): void
    {
        // Prepare example record.
        $form = $this->formFactory->find('test.fieldtypes');
        $payload = json_encode($this->getCreationRecordArrayTestFieldtypes($form));
        parent::$admin->request('POST', $this->endpoint.'/form/data/'.$form->getId().'s', [], [], [], $payload);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(201, $response->getStatusCode());
        // Verify saved content.
        $payload = json_decode($payload, true);
        $record = $form->find($responseData['id']);

        $this->assertPayloadRecord($payload, $record);
    }

    /**
     * Test that we can list records.
     */
    public function testAllFieldTypesGetList():void
    {
        $form = $this->formFactory->find('test.fieldtypes');
        $payload = $this->getCreationRecordArrayTestFieldtypes($form);
        $form->insert($payload);
        $form->insert($payload);
        $form->insert($payload);

        parent::$admin->request('GET', $this->endpoint.'/form/data/'.$form->getId().'s');
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());
        foreach ($responseData as $record) {
            $this->assertApiPayload($form, $payload, $record);
        }
    }

    /**
     * Test that we can get a record.
     */
    public function testAllFieldTypesGetItem(): void
    {
        $form = $this->formFactory->find('test.fieldtypes');
        $payload = $this->getCreationRecordArrayTestFieldtypes($form);
        $newId = $form->insert($payload);

        parent::$admin->request('GET', $this->endpoint.'/form/data/'.$form->getId().'s/'.$newId);
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($newId, $responseData['id']);
        $this->assertApiPayload($form, $payload, $responseData);
    }

    /**
     * Test that we can update a record.
     */
    public function testAllFieldTypesUpdateItem(): void
    {
        $form = $this->formFactory->find('test.fieldtypes');
        $payload = $this->getCreationRecordArrayTestFieldtypes($form);
        $newId = $form->insert($payload);

        // Update by RestFul.
        parent::$admin->request('PUT', $this->endpoint.'/form/data/'.$form->getId().'s/'.$newId, [], [], [], json_encode($payload));
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        // Verify response.
        $this->assertApiPayload($form, $payload, $responseData);

        // Verify by direct read.
        $updated = $form->find($newId);
        $this->assertPayloadRecord($payload, $updated);
    }

    /**
     * Test that we can delete a record.
     */
    public function testAllFieldTypesDeleteItem(): void
    {
        $form = $this->formFactory->find('test.fieldtypes');
        $payload = $this->getCreationRecordArrayTestFieldtypes($form);
        $newId = $form->insert($payload);

        // Delete by RestFul.
        parent::$admin->request('DELETE', $this->endpoint.'/form/data/'.$form->getId().'s/'.$newId);
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        // Is it deleted?
        $record = $form->find($newId);
        $this->assertNull($record);
    }

    /**
     * Test that we can get the first page of form structure list.
     */
    public function testGetFormStructureListFirstPage(): void
    {
        parent::$admin->request('GET', $this->endpoint.'/form/structure');
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(200, $response->getStatusCode());
        $forms = $this->formFactory->findAll();
        $this->assertLessThanOrEqual(count($forms), count($responseData));
        foreach ($responseData as $item) {
            $this->assertEquals('/form/structure/'.$item['id'], $item['path']);
        }
    }

    /**
     * Test that we can get the form structure.
     */
    public function testGetFormStructure(): void
    {
        $form = $this->formFactory->find('form.test.api');
        /** @var ConfigurationRead $cfg */
        $cfg = $this->em->getRepository(Configuration::class)->find('form.test.api');

        parent::$admin->request('GET', $this->endpoint.'/form/structure/'.$form->getId());
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(serialize($cfg->getValue()), serialize($responseData));
    }

    /**
     * Test that we get the meta tree catalog.
     */
    public function testFormMetaTree(): void
    {
        // To mount form from YML.
        $cfgDir = __DIR__.'/../../assets/form.meta.test.yml';
        $data = Yaml::parse(file_get_contents($cfgDir));
        /** @var ConfigurationRepository $configRepo */
        $configRepo = $this->em->getRepository(Configuration::class);
        $config = $configRepo->create('form.meta.test', $data);
        $config->saveNow();
        // Get form from API.
        parent::$admin->request('GET', $this->endpoint.'/form/structure/form.meta.test');
        $response = parent::$admin->getResponse();
        $json = json_decode($response->getContent());
        // Validate: Deprecated fields are ignored.
        $expected = '{"id":"form.meta.test","type":"App\\\\Forms\\\\ChangedFormManager","fields":{"field_changed":{"id":"field_changed","type":"date","label":"Changed","description":"Last change time.","indexable":false,"internal":true,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":[],"sort":false,"filter":false}},"field_created":{"id":"field_created","type":"date","label":"Created","description":"Creation time.","indexable":false,"internal":true,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":[],"sort":false,"filter":false}},"field_questionnaire_date":{"id":"field_questionnaire_date","type":"date","label":"Date","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"0.1"},"sort":false,"filter":false}},"field_questionnaire_interviewer":{"id":"field_questionnaire_interviewer","type":"short_text","label":"Interviewer","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"0.2"},"sort":false,"filter":false,"max-length":255}},"field_questionnaire_contacts":{"id":"field_questionnaire_contacts","type":"form_record_reference","label":"Interviewee(s)\/contacts","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":true,"weight":0,"meta":{"catalog_id":"0.3"}}},"field_location_lat":{"id":"field_location_lat","type":"decimal","label":"Latitude","description":"Decimal degrees","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.1.1"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"precision":12,"scale":8,"unknowable":false}},"field_location_lon":{"id":"field_location_lon","type":"decimal","label":"Longitude","description":"Decimal degrees","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.1.2"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"precision":12,"scale":8,"unknowable":false}},"field_location_alt":{"id":"field_location_alt","type":"decimal","label":"Altitude","description":"TODO To change to a specialized field.","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.1.3"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"precision":12,"scale":3,"unknowable":false}},"field_minor_local_entity":{"id":"field_minor_local_entity","type":"short_text","label":"Minor local entity","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.1.4"},"sort":false,"filter":false,"max-length":255}},"field_mayor_local_entity":{"id":"field_mayor_local_entity","type":"short_text","label":"Major local entity","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.1.5"},"sort":false,"filter":false,"max-length":255}},"field_regional_entity":{"id":"field_regional_entity","type":"short_text","label":"Regional entity","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.1.6"},"sort":false,"filter":false,"max-length":255}},"field_school_name":{"id":"field_school_name","type":"short_text","label":"Name of School","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.2"},"sort":false,"filter":false,"max-length":255}},"field_school_code":{"id":"field_school_code","type":"short_text","label":"School Code","description":"Only if schools are assigned codes in the country","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.3"},"sort":false,"filter":false,"max-length":255}},"field_school_type":{"id":"field_school_type","type":"select","label":"Type of school","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":true,"weight":0,"meta":{"catalog_id":"1.4","field_other":"school_type_other"},"sort":false,"filter":false,"options":{"1\n":"Pre-primary","2\n":"Primary","3\n":"Secondary","99\n":"Other, specify"}}},"field_school_type_other":{"id":"field_school_type_other","type":"short_text","label":"Other type of school","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":true,"weight":0,"meta":{"catalog_id":"1.4.1","conditional":{"visible":{"or":[{"field_school_type":99}]}}},"sort":false,"filter":false,"max-length":255}},"field_staff_total_number_of_women":{"id":"field_staff_total_number_of_women","type":"integer","label":"Total number of women","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.5.1"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_staff_total_number_of_men":{"id":"field_staff_total_number_of_men","type":"integer","label":"Total number of men","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.5.2"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_student_total_number_of_female":{"id":"field_student_total_number_of_female","type":"integer","label":"Total number of female students","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.6.1"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_student_total_number_of_male":{"id":"field_student_total_number_of_male","type":"integer","label":"Total number of male students","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.6.2"},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_rural_communities_served":{"id":"field_rural_communities_served","type":"long_text","label":"Names of rural communities served by the school","description":"Communities whose members the school welcomes as student at the time of the visit","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.7"},"sort":false,"filter":false}},"field_national_classification":{"id":"field_national_classification","type":"short_text","label":"National classifications","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.8"},"sort":false,"filter":false,"max-length":255}},"field_school_observations":{"id":"field_school_observations","type":"long_text","label":"Observations about the school","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"1.9"},"sort":false,"filter":false}},"field_have_water_supply_system":{"id":"field_have_water_supply_system","type":"select","label":"Does the school have a water supply system?","description":"System as understood by SIASAR: Piped water, well or protected spring, Rainwater collection, always from an improved water source","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":true,"weight":0,"meta":{"catalog_id":"2.1"},"sort":false,"filter":false,"options":{"1\n":"Yes, shared use with the community","2\n":"Yes, exclusively used by the school","3\n":"No"}}},"field_name_water_supply_system":{"id":"field_name_water_supply_system","type":"short_text","label":"Name of system that supplies the school","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":true,"weight":0,"meta":{"catalog_id":"2.2","conditional":{"visible":{"or":[{"field_have_water_supply_system":1},{"field_have_water_supply_system":2}]}}},"sort":false,"filter":false,"max-length":255}},"field_functioning_of_water_system":{"id":"field_functioning_of_water_system","type":"select","label":"Functioning of the school\u2019s water supply system","description":"Confirm at the time of the visit","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"2.3"},"sort":false,"filter":false,"options":{"A\n":"Good: Operational and functions correctly throughout the year","B\n":"Fair: Operational, but sometimes does not function correctly during the year","C\n":"Poor: Operational, but does not function correctly most of the year","D\n":"Inoperable: Non-operational"}}},"field_water_for_drinking":{"id":"field_water_for_drinking","type":"boolean","label":"Is the water provided by the system used by the school for drinking?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"2.4"},"sort":false,"filter":false,"true_label":"Yes","false_label":"No","show_labels":true}},"field_main_source_for_drinking":{"id":"field_main_source_for_drinking","type":"select","label":"What is the main source of drinking water for the school?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"2.5.1","conditional":{"visible":{"or":[{"field_have_water_supply_system":3},{"field_water_for_drinking":"false"}]}}},"sort":false,"filter":false,"options":{"1\n":"Rainwater","2\n":"Unprotected well\/spring","3\n":"Packaged bottled water","4\n":"Tanker-truck or cart","5\n":"Surface water (lake, river, stream)","6\n":"No water source"}}},"field_reasons_not_using_water_system":{"id":"field_reasons_not_using_water_system","type":"select","label":"Indicate the main reasons for not using water provided by the system","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"2.5.2","conditional":{"visible":{"or":[{"field_water_for_drinking":"false"}]}}},"sort":false,"filter":false,"options":{"1\n":"Acceptability (i.e. taste, odor, color, etc.)","2\n":"Quality","3\n":"Affordability","4\n":"Accessibility","5\n":"Availability (continuity and reliability)","99\n":"Other, specify"}}},"field_reasons_not_using_water_system_other":{"id":"field_reasons_not_using_water_system_other","type":"short_text","label":"Other reason","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"2.5.2.1","conditional":{"visible":{"and":[{"field_reasons_not_using_water_system":99}]}}},"sort":false,"filter":false,"max-length":255}},"field_main_source_is_water_available_at_school":{"id":"field_main_source_is_water_available_at_school","type":"boolean","label":"Is drinking water from the main source currently available at the school?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"2.6","conditional":{"visible":{"or":[{"field_have_water_supply_system":3},{"field_water_for_drinking":"false"}]}}},"sort":false,"filter":false,"true_label":"Yes","false_label":"No","show_labels":true}},"field_school_have_toilets":{"id":"field_school_have_toilets","type":"boolean","label":"Does the school have toilets or latrines?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.1"},"sort":false,"filter":false,"true_label":"Yes","false_label":"No","show_labels":true}},"field_type_toilets":{"id":"field_type_toilets","type":"select","label":"What type of student toilets\/latrines are at the school?","description":"Check one - most common","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.2","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"options":{"1\n":"Flush \/ Pour-flush toilets","2\n":"Pit latrines with slab","3\n":"Composting toilets","4\n":"Pit latrines without slab","5\n":"Hanging latrines","6\n":"Bucket latrines"}}},"field_girls_only_toilets_total":{"id":"field_girls_only_toilets_total","type":"integer","label":"Girls\u2019 only toilets \/ Total","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.3.1.1","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_girls_only_toilets_usable":{"id":"field_girls_only_toilets_usable","type":"integer","label":"Girls\u2019 only toilets \/ Usable","description":"Available, functional, private","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.3.1.2","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_boys_only_toilets_total":{"id":"field_boys_only_toilets_total","type":"integer","label":"Boys\u2019 only toilets \/ Total","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.3.2.1","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_boys_only_toilets_usable":{"id":"field_boys_only_toilets_usable","type":"integer","label":"Boys\u2019 only toilets \/ Usable","description":"Available, functional, private","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.3.2.2","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_common_use_toilets_total":{"id":"field_common_use_toilets_total","type":"integer","label":"Common use toilets \/ Total","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.3.3.1","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_common_use_toilets_usable":{"id":"field_common_use_toilets_usable","type":"integer","label":"Common use toilets \/ Usable","description":"Available, functional, private","indexable":false,"internal":false,"deprecated":false,"settings":{"required":true,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.3.3.2","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"min":-9223372036854775808,"max":9223372036854775807,"unknowable":false}},"field_how_clean_student_toilets":{"id":"field_how_clean_student_toilets","type":"select","label":"In general, how clean are the student toilets\/latrines?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.4","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"options":{"1\n":"Clean: all toilets do not have a strong smell or significant numbers of flies or mosquitos, and there is no visible faeces on the floor, walls, seat (or pan) or around the facility","2\n":"Somewhat clean: there is some smell and\/or some sign of faecal matter in some of the toilets\/latrines.","3\n":"Not clean: there is a strong smell and\/or presence of faecal matter in most toilets\/latrines"}}},"field_limited_mobility_vision_toilet":{"id":"field_limited_mobility_vision_toilet","type":"boolean","label":"Is there at least one usable toilet\/latrine that is accessible to those with limited mobility or vision?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"3.5","conditional":{"visible":{"or":[{"field_school_have_toilets":"false"}]}}},"sort":false,"filter":false,"true_label":"Yes","false_label":"No","show_labels":true}},"field_handwashing_facilities":{"id":"field_handwashing_facilities","type":"boolean","label":"Are there handwashing facilities at the school?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"4.1"},"sort":false,"filter":false,"true_label":"Yes","false_label":"No","show_labels":true}},"field_handwashing_facilities_soap_water":{"id":"field_handwashing_facilities_soap_water","type":"select","label":"Are both soap and water currently available at the handwashing facilities?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"4.2","conditional":{"visible":{"or":[{"field_handwashing_facilities":"true"}]}}},"sort":false,"filter":false,"options":{"1\n":"Yes, water and soap","2\n":"Water only","3\n":"Soap only","4\n":"Neither water nor soap"}}},"field_menstrual_facilities_soap_water":{"id":"field_menstrual_facilities_soap_water","type":"select","label":"Are water and soap available in a private space for girls to manage menstrual hygiene?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"4.3","conditional":{"visible":{"or":[{"field_handwashing_facilities":"true"}]}}},"sort":false,"filter":false,"options":{"1\n":"Yes, water and soap","2\n":"Water only","3\n":"Soap only","4\n":"Neither water nor soap"}}},"field_menstrual_covered_bins":{"id":"field_menstrual_covered_bins","type":"boolean","label":"Are there covered bins for disposal of menstrual hygiene materials in girls\u2019 toilets?","description":"","indexable":false,"internal":false,"deprecated":false,"settings":{"required":false,"multivalued":false,"weight":0,"meta":{"catalog_id":"4.4","conditional":{"visible":{"or":[{"field_handwashing_facilities":"true"}]}}},"sort":false,"filter":false,"true_label":"Yes","false_label":"No","show_labels":true}},"field_deprecated":{"settings":{"meta":[]}}},"title":"Schools Questionnaire","description":"Collected Schools information.","meta":{"title":"Schools Questionnaire","version":"Version 12.4 \u2013 April 2021","field_groups":[{"id":"0","title":"QUESTIONNAIRE DATA","children":{"0.1":{"id":"0.1","title":"Date","field_id":"field_questionnaire_date","weight":0},"0.2":{"id":"0.2","title":"Interviewer","field_id":"field_questionnaire_interviewer","weight":0},"0.3":{"id":"0.3","title":"Interviewee(s)\/contacts","field_id":"field_questionnaire_contacts","weight":0}}},{"id":"1","title":"GENERAL INFORMATION","children":{"1.1":{"id":"1.1","title":"Location of the school","children":{"1.1.1":{"id":"1.1.1","title":"Latitude","field_id":"field_location_lat","weight":0},"1.1.2":{"id":"1.1.2","title":"Longitude","field_id":"field_location_lon","weight":0},"1.1.3":{"id":"1.1.3","title":"Altitude","field_id":"field_location_alt","weight":0},"1.1.4":{"id":"1.1.4","title":"Minor local entity","field_id":"field_minor_local_entity","weight":0},"1.1.5":{"id":"1.1.5","title":"Major local entity","field_id":"field_mayor_local_entity","weight":0},"1.1.6":{"id":"1.1.6","title":"Regional entity","field_id":"field_regional_entity","weight":0}}},"1.2":{"id":"1.2","title":"Name of School","field_id":"field_school_name","weight":0},"1.3":{"id":"1.3","title":"School Code","field_id":"field_school_code","weight":0},"1.4":{"id":"1.4","title":"Type of school","field_id":"field_school_type","weight":0,"children":{"1.4.1":{"id":"1.4.1","title":"Other type of school","field_id":"field_school_type_other","weight":0}}},"1.5":{"id":"1.5","title":"Teaching and administrative staff","children":{"1.5.1":{"id":"1.5.1","title":"Total number of women","field_id":"field_staff_total_number_of_women","weight":0},"1.5.2":{"id":"1.5.2","title":"Total number of men","field_id":"field_staff_total_number_of_men","weight":0}}},"1.6":{"id":"1.6","title":"Student population","children":{"1.6.1":{"id":"1.6.1","title":"Total number of female students","field_id":"field_student_total_number_of_female","weight":0},"1.6.2":{"id":"1.6.2","title":"Total number of male students","field_id":"field_student_total_number_of_male","weight":0}}},"1.7":{"id":"1.7","title":"Names of rural communities served by the school","field_id":"field_rural_communities_served","weight":0},"1.8":{"id":"1.8","title":"National classifications","field_id":"field_national_classification","weight":0},"1.9":{"id":"1.9","title":"Observations about the school","field_id":"field_school_observations","weight":0}}},{"id":"2","title":"WATER SUPPLY","children":{"2.1":{"id":"2.1","title":"Does the school have a water supply system?","field_id":"field_have_water_supply_system","weight":0},"2.2":{"id":"2.2","title":"Name of system that supplies the school","field_id":"field_name_water_supply_system","weight":0},"2.3":{"id":"2.3","title":"Functioning of the school\u2019s water supply system","field_id":"field_functioning_of_water_system","weight":0},"2.4":{"id":"2.4","title":"Is the water provided by the system used by the school for drinking?","field_id":"field_water_for_drinking","weight":0},"2.5":{"id":"2.5","title":"2.5","children":{"2.5.1":{"id":"2.5.1","title":"What is the main source of drinking water for the school?","field_id":"field_main_source_for_drinking","weight":0},"2.5.2":{"id":"2.5.2","title":"Indicate the main reasons for not using water provided by the system","field_id":"field_reasons_not_using_water_system","weight":0,"children":{"2.5.2.1":{"id":"2.5.2.1","title":"Other reason","field_id":"field_reasons_not_using_water_system_other","weight":0}}}}},"2.6":{"id":"2.6","title":"Is drinking water from the main source currently available at the school?","field_id":"field_main_source_is_water_available_at_school","weight":0}}},{"id":"3","title":"SANITATION","children":{"3.1":{"id":"3.1","title":"Does the school have toilets or latrines?","field_id":"field_school_have_toilets","weight":0},"3.2":{"id":"3.2","title":"What type of student toilets\/latrines are at the school?","field_id":"field_type_toilets","weight":0},"3.3":{"id":"3.3","title":"How many toilets \/ latrines are at the school?","help":"it\u2019s recommended to involve, to the extent possible, the teacher to help with the answer.","children":{"3.3.1":{"id":"3.3.1","title":"3.3.1","children":{"3.3.1.1":{"id":"3.3.1.1","title":"Girls\u2019 only toilets \/ Total","field_id":"field_girls_only_toilets_total","weight":0},"3.3.1.2":{"id":"3.3.1.2","title":"Girls\u2019 only toilets \/ Usable","field_id":"field_girls_only_toilets_usable","weight":0}}},"3.3.2":{"id":"3.3.2","title":"3.3.2","children":{"3.3.2.1":{"id":"3.3.2.1","title":"Boys\u2019 only toilets \/ Total","field_id":"field_boys_only_toilets_total","weight":0},"3.3.2.2":{"id":"3.3.2.2","title":"Boys\u2019 only toilets \/ Usable","field_id":"field_boys_only_toilets_usable","weight":0}}},"3.3.3":{"id":"3.3.3","title":"3.3.3","children":{"3.3.3.1":{"id":"3.3.3.1","title":"Common use toilets \/ Total","field_id":"field_common_use_toilets_total","weight":0},"3.3.3.2":{"id":"3.3.3.2","title":"Common use toilets \/ Usable","field_id":"field_common_use_toilets_usable","weight":0}}}}},"3.4":{"id":"3.4","title":"In general, how clean are the student toilets\/latrines?","field_id":"field_how_clean_student_toilets","weight":0},"3.5":{"id":"3.5","title":"Is there at least one usable toilet\/latrine that is accessible to those with limited mobility or vision?","field_id":"field_limited_mobility_vision_toilet","weight":0}}},{"id":"4","title":"HYGIENE","children":{"4.1":{"id":"4.1","title":"Are there handwashing facilities at the school?","field_id":"field_handwashing_facilities","weight":0},"4.2":{"id":"4.2","title":"Are both soap and water currently available at the handwashing facilities?","field_id":"field_handwashing_facilities_soap_water","weight":0},"4.3":{"id":"4.3","title":"Are water and soap available in a private space for girls to manage menstrual hygiene?","field_id":"field_menstrual_facilities_soap_water","weight":0},"4.4":{"id":"4.4","title":"Are there covered bins for disposal of menstrual hygiene materials in girls\u2019 toilets?","field_id":"field_menstrual_covered_bins","weight":0}}}]}}';
        $this->assertEquals($expected, $response->getContent());
    }

    /**
     * Test that we can clone a validated record.
     */
    public function testFormCloneValidated(): void
    {
        $record = $this->initTestFormClone();

        // Clone it.
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());

        $form = $this->formFactory->find('test.api.inquiry');
        $clonedId = $data["id"];

        // Verify mark, before load, and after.
        $this->assertNotEquals($clonedId, $record->getId());
        $clonedRecord = $form->find($clonedId);
        $this->assertEquals('draft', $clonedRecord->{'field_status'});
        $this->assertFalse($clonedRecord->{'field_validated'});
        // Source record must have locked status.
        $record = $form->find($record->getId());
        $this->assertEquals('locked', $record->{'field_status'});
    }

    /**
     * Test that we can clone a locked record.
     */
    public function testFormCloneLocked(): void
    {
        $record = $this->initTestFormClone();
        $form = $this->formFactory->find('test.api.inquiry');

        // Set Locked.
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());
        // Remove the new clone to allow clone a locked record.
        $form->drop($data["id"]);

        // Clone it.
        $record = $form->find($record->getId());
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());

        $clonedId = $data["id"];

        // Verify mark, before load, and after.
        $this->assertNotEquals($clonedId, $record->getId());
        $clonedRecord = $form->find($clonedId);
        $this->assertEquals('draft', $clonedRecord->{'field_status'});
        $this->assertFalse($clonedRecord->{'field_validated'});
        // Source record must have locked status.
        $record = $form->find($record->getId());
        $this->assertEquals('locked', $record->{'field_status'});
    }

    /**
     * Test that we can clone a locked record with draft active.
     */
    public function testFormCloneLockedWithDraft(): void
    {
        $record = $this->initTestFormClone();
        $form = $this->formFactory->find('test.api.inquiry');

        // Set Locked, this generates a new clone in draft status.
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');

        // Clone it.
        $record = $form->find($record->getId());
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());
        // This action must fail because we have a recent draft.
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals("This record can't be cloned.", $data["message"]);
    }

    /**
     * Test that we can clone a draft record.
     */
    public function testFormCloneDraft(): void
    {
        $record = $this->initTestFormClone();
        $form = $this->formFactory->find('test.api.inquiry');

        // Set draft, the record come from validated.
        $record = $form->find($record->getId());
        $record->{'field_status'} = 'draft';
        $record->save();

        // Clone it.
        $record = $form->find($record->getId());
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals("This record can't be cloned.", $data["message"]);
    }

    /**
     * Test that we can clone a finished record.
     */
    public function testFormCloneFinished(): void
    {
        $record = $this->initTestFormClone();
        $form = $this->formFactory->find('test.api.inquiry');

        // Set draft, the record come from validated.
        $record = $form->find($record->getId());
        $record->{'field_status'} = 'draft';
        $record->save();
        // Set finished, the record come from validated.
        $record = $form->find($record->getId());
        $record->{'field_status'} = 'finished';
        $record->save();

        // Clone it.
        $record = $form->find($record->getId());
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals("This record can't be cloned.", $data["message"]);
    }

    /**
     * Test that we can clone a removed record.
     */
    public function testFormCloneRemoved(): void
    {
        $record = $this->initTestFormClone();
        $form = $this->formFactory->find('test.api.inquiry');

        // Set removed, the record come from validated.
        $form->drop($record->getId());

        // Clone it.
        $record = $form->find($record->getId());
        parent::$admin->request('PUT', $this->endpoint.'/form/data/form.test.api.inquirys/'.$record->getId().'/clone');
        $response = parent::$admin->getResponse();
        $data = Json::decode($response->getContent());

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals("This record can't be cloned.", $data["message"]);
    }

    /**
     * Auxiliary method to test cloning by API.
     *
     * @return FormRecord
     *
     * @throws \Exception
     */
    protected function initTestFormClone(): FormRecord
    {
        try {
            $form = $this->formFactory->find('test.api.inquiry');
        } catch (\Exception $e) {
            $form = $this->formFactory->create('test.api.inquiry', InquiryFormManager::class);
            $form->addField('short_text', 'name', 'Name');
            $form->saveNow();
        }
        // Reset form records.
        $form->uninstall();
        $form->install();

        // Load.
        $form = $this->formFactory->find('test.api.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $id = $form->insert([
            'field_reference' => 'Validated to update',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Set finished.
        $record = $form->find($id);
        $record->{'field_status'} = 'finished';
        $record->save();

        // Set validated.
        $record = $form->find($id);
        $record->{'field_status'} = 'validated';
        $record->save();

        return $record;
    }
}
