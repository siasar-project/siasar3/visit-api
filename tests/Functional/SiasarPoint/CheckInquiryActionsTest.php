<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\SiasarPoint;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Annotations\InquiryCheckAction;
use App\Entity\InquiryFormLog;
use App\Forms\FormManager;
use App\Forms\FormRecord;
use App\Forms\HouseholdFormManager;
use App\Forms\InquiryFormManager;
use App\Plugins\AbstractInquiryCheckActionBase;
use App\Plugins\InquiryCheckActionDiscovery;
use App\Repository\InquiryFormLogRepository;
use App\Service\SessionService;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;
use ReflectionMethod;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;

/**
 * Test Inquiry collection end-point.
 */
class CheckInquiryActionsTest extends PointTestBase
{
    protected static bool $firstSetup = true;
    protected IriConverterInterface $iriConverter;
    protected InquiryCheckActionDiscovery $inquiryCheckActionDiscovery;
    protected $inquiryActions;
    protected SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected Reader $annotationReader;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        if (self::$firstSetup) {
            parent::setUp();
        } else {
            self::bootKernel();
            $_ENV['TESTS_LOADING_FIXTURES'] = true;
        }

        $this->iriConverter = static::getContainerInstance()->get('api_platform.iri_converter');
        $this->inquiryCheckActionDiscovery = static::getContainerInstance()->get('inquirycheckaction_discovery');
        $this->sessionService = static::getContainerInstance()->get('session_service');
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->annotationReader = static::getContainerInstance()->get('annotations.reader');

        $this->managerRegistry = self::$container->get('doctrine');
        $kernel = self::$kernel;
        $this->application = new App($kernel);
        $this->formFactory = self::$container->get('form_factory');

        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->entityManager->clear();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->doctrine = static::getContainerInstance()->get('database_connection');
        $this->accessManager = static::getContainerInstance()->get('access_manager');
        $this->tokenManager = static::getContainerInstance()->get('lexik_jwt_authentication.jwt_manager');
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->session = static::getContainerInstance()->get('session');

        if (self::$firstSetup) {
            // Create valid forms.
            $this->loginUser('Pedro');

            $formsTypes = array_merge(
                $this->formFactory->findByType(InquiryFormManager::class),
                $this->formFactory->findByType(HouseholdFormManager::class)
            );
            /** @var FormManager $form */
            foreach ($formsTypes as $form) {
                $form->insert([]);
            }
        }

        // Clear logs.
        /** @var InquiryFormLogRepository $logsRepo */
        $logsRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $logs = $logsRepo->findAll();
        foreach ($logs as $log) {
            $logsRepo->removeNow($log);
        }

        self::$firstSetup = false;
    }

    /**
     * Test inquiry check actions.
     *
     * @dataProvider getActions
     *
     * @param string             $class
     * @param InquiryCheckAction $annotation
     *
     * @return void
     */
    public function testInquiryCheckAction(string $class, InquiryCheckAction $annotation): void
    {
        // Validate that action is active.
        $this->assertTrue($annotation->isActive(), 'Only active actions can be loaded. This action is inactive.');

        // Validate class.
        $this->assertIsString($class);
        $this->assertTrue(class_exists($class));
        $checkId = $annotation->getId();
        $this->assertStringContainsString($checkId, $class);

        // Instantiate class.
        $formType = $annotation->getForm();
        if ('*' === $formType) {
            $formType = 'form.wssystem';
        }
        $form = $this->formFactory->find($formType);
        $inquiry = $form->findOneBy([]);

        /** @var AbstractInquiryCheckActionBase $action */
        $action = new $class(
            $this->annotationReader,
            $this->sessionService,
            $this->inquiryFormLogger,
            $inquiry
        );

        // Important note: We can't force action to fail, by that we can't check that all action create fail logs.
        // Execute it.
        $result = $action->check();
        if (!$result) {
            $this->assertAndRemoveLog($annotation, $inquiry);
        }

        // Force action to log without check conditions.
        if ($annotation->getLevel() !== 'warning') {
            $method = new ReflectionMethod($action, 'logResult');
            $method->setAccessible(true);
            $method->invoke($action);
            $this->assertAndRemoveLog($annotation, $inquiry);
        }
    }

    /**
     * @return array[] [['class' => string, 'annotation' => InquiryCheckAction]]
     *
     * @throws \ReflectionException
     */
    public function getActions()
    {
        $this->inquiryCheckActionDiscovery = static::getContainerInstance()->get('inquirycheckaction_discovery');
        $resp = $this->inquiryCheckActionDiscovery->getInquiryCheckActions();

        // return [$resp['EPSAA01']];
        return $resp;
    }

    /**
     * Validate that a message log was created, is valid, and then remove it.
     *
     * @param InquiryCheckAction $annotation
     * @param FormRecord         $inquiry
     *
     * @return void
     */
    protected function assertAndRemoveLog(InquiryCheckAction $annotation, FormRecord $inquiry)
    {
        $logsRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $logs = $logsRepo->findAll();
        if (!$annotation->isMultiMessage()) {
            $this->assertCount(1, $logs);
            $expected = sprintf('[%s] %s', $annotation->getId(), $annotation->getMessage());
            $this->assertEquals($expected, $logs[0]->getMessage());
        }
        $this->assertEquals(strtolower($annotation->getLevel()), strtolower($logs[0]->getLevelName()));
        $this->assertEquals('hn', $logs[0]->getCountry()->getId());
        $this->assertEquals($inquiry->getForm()->getId(), $logs[0]->getFormId());
        $this->assertEquals($inquiry->getId(), $logs[0]->getRecordId());
        // Remove log.
        $logsRepo->removeNow($logs[0]);
    }
}
