<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\SiasarPoint;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Annotations\PointCheckAction;
use App\Entity\AdministrativeDivision;
use App\Entity\InquiryFormLog;
use App\Forms\FormRecord;
use App\Plugins\AbstractPointCheckActionBase;
use App\Plugins\PointCheckActionDiscovery;
use App\Repository\InquiryFormLogRepository;
use App\Service\SessionService;
use App\Tools\SiasarPointWrapper;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;
use ReflectionMethod;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;

/**
 * Test Siasar Point collection end-point.
 */
class CheckPointActionsTest extends PointTestBase
{
    protected static bool $firstSetup = true;
    protected IriConverterInterface $iriConverter;
    protected PointCheckActionDiscovery $pointCheckActionDiscovery;
    protected $pointActions;
    protected SessionService $sessionService;
    protected LoggerInterface $inquiryFormLogger;
    protected Reader $annotationReader;
    protected static FormRecord $point;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        if (self::$firstSetup) {
            parent::setUp();
        } else {
            self::bootKernel();
            $_ENV['TESTS_LOADING_FIXTURES'] = true;
        }

        $this->iriConverter = static::getContainerInstance()->get('api_platform.iri_converter');
        $this->pointCheckActionDiscovery = static::getContainerInstance()->get('pointcheckaction_discovery');
        $this->sessionService = static::getContainerInstance()->get('session_service');
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
        $this->annotationReader = static::getContainerInstance()->get('annotations.reader');

        $this->managerRegistry = self::$container->get('doctrine');
        $kernel = self::$kernel;
        $this->application = new App($kernel);
        $this->formFactory = self::$container->get('form_factory');

        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->entityManager->clear();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->doctrine = static::getContainerInstance()->get('database_connection');
        $this->accessManager = static::getContainerInstance()->get('access_manager');
        $this->tokenManager = static::getContainerInstance()->get('lexik_jwt_authentication.jwt_manager');
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->session = static::getContainerInstance()->get('session');

        if (self::$firstSetup) {
            // Create valid point.
            $session = $this->loginRest('pedro');

            self::$point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
            self::$point = $this->requestPoint('PUT', $session, self::$point->getId(), ["field_chk_to_plan" => true]);
            self::$point = $this->requestPoint('PUT', $session, self::$point->getId(), ["field_status" => 'digitizing']);
            // Create WSP
            $wsp = $this->createWSPinPoint($session, self::$point->getId());
            // Add WS System
            // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
            $form = $this->formFactory->find('form.wssystem.communities');
            $sysCommId = $form->insert(
                [
                    "field_community" => [
                        "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                        "class" => AdministrativeDivision::class,
                    ],
                    "field_provider" => [
                        "value" => $wsp->getId(),
                        "form" => "form.wsprovider",
                    ],
                    'field_households' => 50,
                ]
            );

            $sysPayload = [
                "field_system_name" => "Name of system 1",
                "field_served_communities" => [
                    [
                        "value" => $sysCommId,
                        "form" => "form.wssystem.communities",
                    ],
                ],
            ];
            $this->createWSSystemInPoint($session, self::$point->getId(), $sysPayload);
        }

        // Clear logs.
        /** @var InquiryFormLogRepository $logsRepo */
        $logsRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $logs = $logsRepo->findAll();
        foreach ($logs as $log) {
            $logsRepo->removeNow($log);
        }

        self::$firstSetup = false;
    }

    /**
     * Test point check actions.
     *
     * @dataProvider getActions
     *
     * @param string           $class
     * @param PointCheckAction $annotation
     *
     * @return void
     */
    public function testPointCheckAction(string $class, PointCheckAction $annotation): void
    {
        // Validate that action is active.
        $this->assertTrue($annotation->isActive(), 'Only active actions can be loaded. This action is inactive.');

        // Validate class.
        $this->assertIsString($class);
        $this->assertTrue(class_exists($class));
        $checkId = $annotation->getId();
        $this->assertStringContainsString($checkId, $class);

        // Instantiate class.
        $pWrapper = new SiasarPointWrapper(self::$point, $this->entityManager, $this->sessionManager, $this->iriConverter);

        $inquiry = null;
        switch ($annotation->getForm()) {
            case '*':
            case 'form.community':
                $inquiry = current($pWrapper->getCommunities());
                break;
            case 'form.wsprovider':
                $inquiry = current($pWrapper->getWSProviders());
                break;
            case 'form.wssystem':
                $inquiry = current($pWrapper->getSystems());
                break;
        }

        /** @var AbstractPointCheckActionBase $action */
        $action = new $class(
            $this->annotationReader,
            $this->sessionService,
            $this->inquiryFormLogger,
            $inquiry,
            self::$point
        );

        // Important note: We can't force action to fail, by that we can't check that all action create fail logs.
        // Execute it.
        $result = $action->check();
        if (!$result) {
            $this->assertAndRemoveLog($annotation, $inquiry);
        }

        // Force action to log without check conditions.
        if ($annotation->getLevel() !== 'warning') {
            $method = new ReflectionMethod($action, 'logResult');
            $method->setAccessible(true);
            $method->invoke($action);
            $this->assertAndRemoveLog($annotation, $inquiry);
        }
    }

    /**
     * @return array[] [['class' => string, 'annotation' => PointCheckAction]]
     *
     * @throws \ReflectionException
     */
    public function getActions()
    {
        $this->pointCheckActionDiscovery = static::getContainerInstance()->get('pointcheckaction_discovery');
        $resp = $this->pointCheckActionDiscovery->getPointCheckActions();

        return $resp;
    }

    /**
     * Validate that a message log was created, is valid, and then remove it.
     *
     * @param PointCheckAction $annotation
     * @param FormRecord       $inquiry
     *
     * @return void
     */
    protected function assertAndRemoveLog(PointCheckAction $annotation, FormRecord $inquiry)
    {
        $logsRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $logs = $logsRepo->findAll();
        $this->assertCount(1, $logs);
        $expected = sprintf('[%s] %s', $annotation->getId(), $annotation->getMessage());
        $this->assertEquals($expected, $logs[0]->getMessage());
        $this->assertEquals(strtolower($annotation->getLevel()), strtolower($logs[0]->getLevelName()));
        $this->assertEquals('hn', $logs[0]->getCountry()->getId());
        $this->assertEquals($inquiry->getForm()->getId(), $logs[0]->getFormId());
        $this->assertEquals($inquiry->getId(), $logs[0]->getRecordId());
        // Remove log.
        $logsRepo->removeNow($logs[0]);
    }
}
