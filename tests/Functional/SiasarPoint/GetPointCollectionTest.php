<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Martin Dell'Oro <martindell.oro@gmail.com>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\SiasarPoint;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\File;
use App\Entity\GeographicalScope;
use App\Entity\PointLog;
use App\Entity\User;
use App\Forms\FormRecord;
use App\Service\SessionService;
use App\Tools\Json;
use App\Tools\Measurements\FlowMeasurement;
use App\Tools\SiasarPointWrapper;
use Doctrine\DBAL\Query\QueryBuilder;
use PHPUnit\Util\Exception;
use Symfony\Component\Uid\Ulid;

/**
 * Test Siasar Point collection end-point.
 */
class GetPointCollectionTest extends PointTestBase
{
    protected IriConverterInterface $iriConverter;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->iriConverter = static::getContainerInstance()->get('api_platform.iri_converter');
    }

    /**
     * Test empty point collection.
     */
    public function testEmptyList(): void
    {
        $session = $this->loginRest('Pedro');
        $session->request(
            'GET',
            '/api/v1/form/data/form.points',
            [
                'page' => '1',
                'country' => '/api/v1/countries/hn',
            ]
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);

        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        $maxDeep = $country->getDeep();

        foreach ($data as $division) {
            $this->assertEmpty($division['id']);
            $this->assertEquals('without planning', $division['status_code']);
            $this->assertNotEmpty($division['status']);
            // Version is the updated value because doesn't have Point created.
            $dateUpdated = strtotime($division['updated']);
            $dateVersion = strtotime($division['version']);
            $this->assertEquals(date('Y-m-d', $dateVersion), date('Y-m-d', $dateUpdated));
            $this->assertArrayHasKey('id', current($division['administrative_division']));
            $this->assertArrayHasKey('name', current($division['administrative_division']));
            $this->assertContains('create', $division['allowed_actions']);
            // Division must be a community.
            /** @var AdministrativeDivision $adEntity */
            $adEntity = $this->iriConverter->getItemFromIri(current($division["administrative_division"])["id"]);
            $this->assertEquals($maxDeep, $adEntity->getLevel());
        }
    }

    /**
     * Test create a new Siasar Point.
     */
    public function testCreatePoint(): void
    {
        $session = $this->loginRest('Pedro');
        // Create the new Point.
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [
                'page' => '1',
                'country' => ['/api/v1/countries/hn'],
            ],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01FFFBVPS6K583BY7QCEK1HSZF"}'
        );
        // Query to validate.
        $session->request('GET', '/api/v1/form/data/form.points', ['page' => '1', 'country' => ['/api/v1/countries/hn']]);
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);

        $this->assertNotEmpty($data[0]['id']);
        $this->assertEquals('planning', $data[0]['status_code']);
        $this->assertEquals('/api/v1/administrative_divisions/01FFFBVPS6K583BY7QCEK1HSZF', $data[0]['administrative_division'][0]['id']);
        $this->assertEquals('7-CODE', $data[0]['administrative_division'][0]['name']);
        $this->assertContains('update', $data[0]['allowed_actions']);

        $form = $this->formFactory->find('form.point');
        $point = $form->find($data[0]['id']);
        $this->assertEquals($point->{'field_created'}->format('Y-m-d'), $data[0]['version']);
    }

    /**
     * Test create an existing Siasar Point.
     */
    public function testCreateDuplicatePoint(): void
    {
        $session = $this->loginRest('Pedro');
        // Create the new Point.
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01FFFBVPS6K583BY7QCEK1HSZF"}'
        );

        // Try to create another point in the same administrative division
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01FFFBVPS6K583BY7QCEK1HSZF"}'
        );
        // Query to validate.
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('The Point cannot be created. There is a previous version.', $data['message']);
    }

    /**
     * Test update a Siasar Point with status planning.
     */
    public function testPutItemWithStatusPlanning(): void
    {
        $session = $this->loginRest('Pedro');
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6"}'
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        // Update the new Point.
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$data['id'],
            [],
            [],
            [],
            '{"field_chk_to_plan": true}'
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * Test add equipment to Siasar Point with status planning
     */
    public function testPutItem(): void
    {
        $session = $this->loginRest('Pedro');
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6"}'
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        // Update the new Point.
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$data['id'],
            [],
            [],
            [],
            '{"field_chk_to_plan": true}'
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
    }

    /**
     * Test unplan (delete) a Siasar Point with status "planning"
     */
    public function testUnplanPoint(): void
    {
        $session = $this->loginRest('Pedro');
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $point = $this->requestPoint('POST', $session, null, $payload);
        // Get the form.community created
        $communityForm = $this->formFactory->find('form.community');
        $communityRecord = $communityForm->find($point->{'field_communities'}[0]->getId());
        $communityRecord->{'field_have_households'} = true;
        $communityRecord->save();

        $session->request('DELETE', '/api/v1/form/data/form.points/'.$point->getId());
        $response = $session->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Test unplan (delete) a Siasar Point with other status
     */
    public function testUnplanPointFail(): void
    {
        $session = $this->loginRest('Pedro');
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6"}'
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);

        // Update the Point status
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$data['id'],
            [],
            [],
            [],
            '{"field_chk_to_plan": true}'
        );
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$data['id'],
            [],
            [],
            [],
            '{"field_status": "digitizing"}'
        );
        $putResponse = $session->getResponse();
        $this->assertEquals(200, $putResponse->getStatusCode());
        // Delete Point
        $session->request('DELETE', '/api/v1/form/data/form.points/'.$data['id']);
        $deleteResponse = $session->getResponse();
        $deleteData = $this->getResponseArray($deleteResponse);
        $this->assertEquals('Point can\'t be removed.', $deleteData['detail']);
    }

    /**
     * Test add a new WSP to specific Point
     */
    public function testPointAddWSPForm(): void
    {
        $session = $this->loginRest('Pedro');
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6"}'
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        // Update the Point status
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$data['id'],
            [],
            [],
            [],
            '{"field_chk_to_plan": true}'
        );
        // Create WSP
        $session->request(
            'POST',
            '/api/v1/form/data/form.points/'.$data['id'].'/form.wsprovider',
            [],
            [],
            [],
            '{"field_provider_name": "Name of WSP", "field_provider_code": "WSP001"}'
        );
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * Test add a new WSSystem to specific Point
     */
    public function testPointAddWSSystemForm(): void
    {
        $session = $this->loginRest('Pedro');
        $session->request(
            'POST',
            '/api/v1/form/data/form.points',
            [],
            [],
            [],
            '{"administrative_division": "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6"}'
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(201, $response->getStatusCode());
        // Update the Point status
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$data['id'],
            [],
            [],
            [],
            '{"field_chk_to_plan": true}'
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode(), $response->getContent());
        $data = $this->getResponseArray($response);
        // Create WSSystem
        $session->request(
            'POST',
            '/api/v1/form/data/form.points/'.$data['id'].'/form.wssystem',
            [],
            [],
            [],
            '{"field_system_name": "Name of system", "field_system_code": "SYS001"}'
        );
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * Test associate a WSP from other Point to a WSSystem
     */
    public function testPointAddWSPFromOtherPointForm(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $point = $this->requestPoint('POST', $session, null, $payload);

        // Create WSP in the new Point
        $wsp1 = $this->createWSPinPoint($session, $point->getId());
        $this->assertEquals($point->getId(), $wsp1->{'field_point'}->getId());

        // Create another Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3",
            "field_chk_to_plan" => true,
        ];
        $point2 = $this->requestPoint('POST', $session, null, $payload);

        // Create WSP in the new Point
        $wsp2 = $this->createWSPinPoint($session, $point2->getId());
        $this->assertEquals($point2->getId(), $wsp2->{'field_point'}->getId());

        // Create WSSystem with a WSP from a different Point
        // Add 'form.wssystem.communities' subform first to associate it to the system
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01G7VA38911P7RX73J7GK8JFA6',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp2->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);
        $this->assertCount(2, $point->{'field_wsps'});
        $this->assertEquals($wsp1->getId(), $point->{'field_wsps'}[0]->getId());
        $this->assertEquals($wsp2->getId(), $point->{'field_wsps'}[1]->getId());
    }

    /**
     * Test that we can get inquiries in Point.
     */
    public function testGetPointInquiriesList(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $point = $this->requestPoint('POST', $session, null, $payload);

        // Create WSS in the new Point
        $wsp1 = $this->createWSPinPoint($session, $point->getId());
        $this->assertEquals($point->getId(), $wsp1->{'field_point'}->getId());
        // Add WSSystem.
        $sysPayload = [
            "field_system_name" => "Name of system",
        ];
        $system = $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);
        // Validate point inquiries response.
        $mustHave = [];
        $pComm = current($point->{'field_communities'});
        $this->assertEquals('01G7VA38911P7RX73J7GK8JFA6', $pComm->get('field_region')['value']);
        $mustHave[] = $pComm->getId();
        $pWsp = current($point->{'field_wsps'});
        $this->assertEquals($wsp1->getId(), $pWsp->getId());
        $mustHave[] = $pWsp->getId();
        $pWss = current($point->{'field_wsystems'});
        $this->assertEquals($system->getId(), $pWss->getId());
        $mustHave[] = $pWss->getId();

        $session->request('GET', '/api/v1/form/data/form.points/'.$point->getId().'/inquiries');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        foreach ($data as $item) {
            $this->assertContains($item['id'], $mustHave);
        }
    }

    /**
     * Test clean Point on update System
     */
    public function testCleanPointOnUpdateSystemForm(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $point = $this->requestPoint('POST', $session, null, $payload);

        // Create another Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3",
            "field_chk_to_plan" => true,
        ];
        $point2 = $this->requestPoint('POST', $session, null, $payload);

        // Create WSP in the new Point
        $wsp = $this->createWSPinPoint($session, $point2->getId());
        $this->assertEquals($point2->getId(), $wsp->{'field_point'}->getId());

        // Create WSSystem with a WSP from a different Point
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01G7VA38911P7RX73J7GK8JFA6',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);
        $this->assertCount(1, $point->{'field_wsps'});
        $this->assertEquals($wsp->getId(), $point->{'field_wsps'}[0]->getId());

        // Delete field 1.12 to remove WSP on Point
        $system->{'field_served_communities'} = [];
        $system->save();
        $formPoint = $this->formFactory->find('form.point');
        $point = $formPoint->find($point->getId());
        $this->assertCount(0, $point->{'field_wsps'});
    }

    /**
     * Test that we can get point team list.
     */
    public function testPointGetTeam(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $point = $this->requestPoint('POST', $session, null, $payload);
        // Add team.
        $payload = [
            'field_team' => [
                ['value' => '01FHX4RK866FRCDAYYYCZKVFXJ', 'class' => User::class],
                ['value' => '01FHX4RRD9KGQ45YZDP001CZ07', 'class' => User::class],
                ['value' => '01FHX4S6GB2Y7FW4N357DRYKT6', 'class' => User::class],
            ],
        ];
        $point = $this->requestPoint('PUT', $session, $point->getId(), $payload);
        // Get team.
        $session->request('GET', '/api/v1/form/data/form.points/'.$point->getId());
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);

        $mustHave = ['01FHX4RK866FRCDAYYYCZKVFXJ', '01FHX4RRD9KGQ45YZDP001CZ07', '01FHX4S6GB2Y7FW4N357DRYKT6'];
        foreach ($data["team"] as $member) {
            $this->assertContains($member['value'], $mustHave);
        }
    }

    /**
     * test Point fusion on not final status
     */
    public function testMergePoints(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3",
            "field_chk_to_plan" => true,
        ];
        $sourcePoint = $this->requestPoint('POST', $session, null, $payload);
        $sourceComm = $sourcePoint->{'field_communities'}[0];

        // Create another Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $targetPoint = $this->requestPoint('POST', $session, null, $payload);
        // Create WSP
        $wsp = $this->createWSPinPoint($session, $targetPoint->getId());

        // Create WSSystem with a Community of a different Point
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );
        $sysPayload = [
            "field_system_name" => "Name of system",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $targetPoint->getId(), $sysPayload);

        $this->assertCount(2, $targetPoint->{'field_communities'});
        $this->assertEquals($sourceComm->getId(), $targetPoint->{'field_communities'}[1]->getId());
        $this->assertEquals($wsp->getId(), $targetPoint->{'field_wsps'}[0]->getId());

        // Try to get the merged point
        $session->request('GET', '/api/v1/form/data/form.points/'.$sourcePoint->getId());
        $response = $session->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * test Point fusion on final status (clone inquiries)
     */
    public function testMergePointsOnFinalStatus(): void
    {
        $session = $this->loginRest('Pedro');
        $userRepo = $this->entityManager->getRepository(User::class);
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3",
            "field_chk_to_plan" => true,
        ];
        $sourcePoint = $this->requestPoint('POST', $session, null, $payload);
        $community = $sourcePoint->{'field_communities'}[0];
        // Load required fields to avoid future errors.
        $community->{'field_location_alt'} = [
            'value' => 1,
            'unit' => 'metre',
        ];
        $community->{'field_interviewer'} = [
            'value' => SessionService::getUser()->getId(),
            'class' => USer::class,
        ];
        $community->{'field_community_name'} = 'Comunidad del anillo';
        $community->{'field_total_population'} = 1;
        $community->{'field_total_households'} = 2;
        $community->{'field_location_lat'} = 2;
        $community->{'field_location_lon'} = 2;
        $community->{'field_destination_solid_waste'} = '99';
        $community->save();

        // Create WSP
        $wsp = $this->createWSPinPoint($session, $sourcePoint->getId());

        // Create WSSystem
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        // Add required subform record
        $form = $this->formFactory->find('form.wssystem.sourceintake');
        $sourceintakeRecordId = $form->insert($this->generateRecordData($form));
        // Add required subform record
        $form = $this->formFactory->find('form.wssystem.distribution');
        $data = $this->generateRecordData($form);
        $def = new FlowMeasurement(rand(0, 100));
        $data['field_service_flow'] = [
            'value' => (string) $def->getValue(),
            'unit' => $def->getUnit(),
        ];
        $distributionRecordId = $form->insert($data);

        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
            "field_water_source_intake" => [
                [
                    "value" => $sourceintakeRecordId,
                    "form" => "form.wssystem.sourceintake",
                ],
            ],
            "field_distribution_infrastructure" => [
                [
                    "value" => $distributionRecordId,
                    "form" => "form.wssystem.distribution",
                ],
            ],
            "field_location_alt" => [
                'value' => 1,
                'unit' => 'metre',
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $sourcePoint->getId(), $sysPayload);
        $this->updateRecordData($system);
        $system->save();

        // Update community field_status to "validated"
        $doctrine = self::$container->get('database_connection');
        $ulid = Ulid::fromString($community->getId());
        /**
         * @var QueryBuilder $query
         */
        $query = $doctrine->createQueryBuilder()
            ->update($community->getForm()->getTableName(), 'fc')
            ->set('fc.field_status', '"validated"')
            ->where('HEX(fc.id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'');
        $query->execute();

        // Update system field_status to "validated"
        $ulid = Ulid::fromString($system->getId());
        $query = $doctrine->createQueryBuilder()
            ->update($system->getForm()->getTableName(), 's')
            ->set('s.field_status', '"validated"')
            ->where('HEX(s.id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'');
        $query->execute();

        // Update point status to "calculated"
        $ulid = Ulid::fromString($sourcePoint->getId());
        $query = $doctrine->createQueryBuilder()
            ->update($sourcePoint->getForm()->getTableName(), 'p')
            ->set('p.field_status', '"calculated"')
            ->where('HEX(p.id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'');
        $query->execute();

        // Create another Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $targetPoint = $this->requestPoint('POST', $session, null, $payload);

        // Create WSSystem
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId2 = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                "field_households" => 10,
            ]
        );
        $sysPayload2 = [
            "field_system_name" => "Name of system 2",
            "field_served_communities" => [
                [
                    "value" => $sysCommId2,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system2 = $this->createWSSystemInPoint($session, $targetPoint->getId(), $sysPayload2);

        $session->request('GET', '/api/v1/form/data/form.points/'.$targetPoint->getId());
        $response = $session->getResponse();
        $point = $this->getResponseArray($response);

        // Check the total of communities and systems in the targetPoint
        $this->assertCount(2, $point['community']);
        $this->assertCount(2, $point['wsystem']);

        // Check the total of form.community in database
        $query = $doctrine->createQueryBuilder()
            ->select('*')
            ->from('form_community');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        $match = ['locked', 'draft', 'draft'];
        foreach ($result as $key => $value) {
            $this->assertEquals($match[$key], $value['field_status']);
        }

        // Check the total of form_wssystem in database
        $query = $doctrine->createQueryBuilder()
            ->select('*')
            ->from('form_wssystem');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        $match = ['locked', 'draft', 'draft'];
        foreach ($result as $key => $value) {
            $this->assertEquals($match[$key], $value['field_status']);
        }
    }

    /**
     * Test delete community from specific Point
     */
    public function testPointDeleteCommunityOk2(): void
    {
        $session = $this->loginRest('Pedro');
        $pointManager = $this->formFactory->find('form.point');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        // Add community
        $community = $this->createCommunityInPoint($session, $point->getId(), []);
        $this->assertNotNull($community);
        $point = $pointManager->find($point->getId());
        $this->assertCount(2, $point->{'field_communities'});

        // Remove community reference.
        /** @var FormRecord[] $systems */
        $systems = $point->{'field_wsystems'};
        $system = $systems[0];
        $system->{'field_served_communities'} = [];
        $system->save();

        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.community/'.$community->getId(),
        );
        $response = $session->getResponse();
        $this->assertEquals(204, $response->getStatusCode());

        $point = $pointManager->find($point->getId());
        $this->assertCount(1, $point->{'field_communities'});
    }

    /**
     * Test can't delete community from specific Point with live reference.
     */
    public function testPointDeleteCommunityFailByReference(): void
    {
        $session = $this->loginRest('Pedro');
        $pointManager = $this->formFactory->find('form.point');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        // Add community
        $community = $this->createCommunityInPoint($session, $point->getId(), []);
        $this->assertNotNull($community);
        $point = $pointManager->find($point->getId());
        $this->assertCount(2, $point->{'field_communities'});

        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.community/'.$community->getId(),
        );
        $response = $session->getResponse();
        $this->assertEquals(422, $response->getStatusCode());

        $point = $pointManager->find($point->getId());
        $this->assertCount(2, $point->{'field_communities'});
    }

    /**
     * Test can't delete the last community from specific Point.
     */
    public function testPointDeleteCommunityFailByUnique(): void
    {
        $session = $this->loginRest('Pedro');
        $pointManager = $this->formFactory->find('form.point');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);

        $comms = $point->{'field_communities'};
        $this->assertCount(1, $comms);

        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.community/'.$comms[0]->getId(),
        );
        $response = $session->getResponse();
        $this->assertEquals(422, $response->getStatusCode());

        $point = $pointManager->find($point->getId());
        $this->assertCount(1, $point->{'field_communities'});
    }

    /**
     * Test delete WSSystem from specific Point
     */
    public function testPointDeleteWSSystemOk(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        // Add WS System
        $system = $this->createWSSystemInPoint($session, $point->getId(), []);

        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.wssystem/'.$system->getId(),
        );
        $response = $session->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Test delete WSSystem with communities associated from a specific Point
     */
    public function testPointDeleteWSSystemWithDataFail(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        // Create WSP
        $wsp = $this->createWSPinPoint($session, $point->getId());
        // Add WS System
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);
        // Delete WS System
        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.wssystem/'.$system->getId(),
        );
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals("The record '".$system->getId()."' cannot be deleted. It already has associated communities.", $data["message"]);
    }

    /**
     * Test update inquiries of a Point with status "complete"
     */
    public function testUpdateInquiriesOnCompletePoint(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3", "field_chk_to_plan" => true]);

        // Update point status to "complete"
        $doctrine = self::$container->get('database_connection');
        $ulid = Ulid::fromString($point->getId());
        $query = $doctrine->createQueryBuilder()
            ->update($point->getForm()->getTableName(), 'p')
            ->set('p.field_status', '"complete"')
            ->where('HEX(p.id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'');
        $query->execute();

        $session->request('PUT', '/api/v1/form/data/form.communitys/'.$point->{'field_communities'}[0]->getId(), [], [], [], json_encode(['field_have_households' => true]));
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());

        $this->assertEquals(401, $data['code']);
        $this->assertEquals("You can't edit inquiries in this point. It's on status \"complete\"", $data['message']);
    }

    /**
     * Test point can go to complete.
     */
    public function testPointCompleteTransition(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_status" => 'digitizing']);
        // Create WSP
        $wsp = $this->createWSPinPoint($session, $point->getId());
        // Add WS System
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );
        // Add form.wssystem.sourceintake
        $form = $this->formFactory->find('form.wssystem.sourceintake');
        $sourceintake = $form->insert(
            [
                'field_source_longitude' => -87,
                'field_source_latitude' => 14.95,
                'field_source_code' => 'Anita, Sara y Pedro',
                'field_source_name' => 'Ana y Pedro',
                'field_source_type' => 3,
                'field_water_protected' => '97',
                'field_presence_vegetation' => '97',
                'field_erosion' => '97',
                'field_physical_state_intake' => 'A',
            ]
        );
        // Add form.wssystem.distribution
        $form = $this->formFactory->find('form.wssystem.distribution');
        $distribution = $form->insert(
            [
                'field_distribution_network_code' => 'Ana Gómez',
                'field_households_connection' => 3,
                'field_serve_household' => '3',
                'field_service_week_days' => 7,
                'field_service_week_hours' => 24,
                'field_service_methodology' => 1,
                'field_service_flow' => [
                    'value' => 1,
                    'unit' => 'liter/second',
                ],
                'field_distribution_state' => 'A',
                'field_accessibility_public_standpost' => '1',
                'field_communities_serviced_network' => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
            ]
        );
        // form.wssystem
        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_type_system" => "99",
            'field_have_distribution_network' => true,
            'field_distribution_infrastructure' => [
                [
                    "value" => $distribution,
                    "form" => "form.wssystem.distribution",
                ],
            ],
            'field_water_source_intake' => [
                [
                    "value" => $sourceintake,
                    "form" => "form.wssystem.sourceintake",
                ],
            ],
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);
        // Finish all forms.
        $pWrapper = new SiasarPointWrapper($point, $this->entityManager, $this->sessionManager, $this->iriConverter);
        $communities = $pWrapper->getCommunities();
        foreach ($communities as $community) {
            $community->{'field_interviewer'} = [
                'value' => SessionService::getUser()->getId(),
                'class' => User::class,
            ];
            $community->{'field_location_alt'} = [
                'value' => 1,
                'unit' => 'metre',
            ];
            $community->{'field_community_name'} = 'Aljaraque';
            $community->{'field_total_population'} = 1;
            $community->{'field_total_households'} = 5;
            $community->{'field_households_without_water_supply_system'} = 5;
            $community->{'field_flush_toilets'} = true;
            $community->{'field_septic_tank_number'} = 5;
            $community->{'field_formal_service_provider_offers'} = '1';
            $community->{'field_with_basic_handwashing'} = 5;
            $community->{'field_location_lat'} = 5;
            $community->{'field_location_lon'} = 5;
            $community->{'field_status'} = 'finished';
            $community->{'field_destination_solid_waste'} = '99';
            try {
                $community->save();
            } catch (\Exception $e) {
                $logs = $e->getMessage()."\n".$this->getInquiryLogMessages($community);
                throw new Exception($logs);
            }
            $logs = $this->getInquiryLogMessages($community);
            $this->assertEmpty($logs, $logs);
        }
        $systems = $pWrapper->getSystems();
        foreach ($systems as $system) {
            $system->set('field_region', [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ]);
            $system->set('field_questionnaire_interviewer', [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ]);
            $system->set('field_design', [
                "value" => '01GESDGFYWHQ621KATV3QH9BFH',
                "class" => File::class,
            ]);
            $system->set('field_location_alt', [
                'value' => 1,
                'unit' => 'metre',
            ]);
            $system->set('field_location_lat', 5);
            $system->set('field_location_lon', 5);
            $system->{'field_status'} = 'finished';
            $system->save();
            $logs = $this->getInquiryLogMessages($system);
            $this->assertEmpty($logs, $logs);
        }
        $wsproviders = $pWrapper->getWSProviders();
        foreach ($wsproviders as $wsProvider) {
            $wsProvider->set('field_region', [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ]);
            $wsProvider->set('field_questionnaire_interviewer', [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ]);
            $wsProvider->set('field_provider_name', 'Periquillo de los palotes');
            $wsProvider->set('field_provider_type', '1');
            $wsProvider->set('field_provider_have_rules_for_service', '4');
            $wsProvider->set('field_provider_legal_status', '1');
            $wsProvider->set('field_provider_scope', [
                "value" => '01G2MJ1G49729QPKS7CV3F5FGK',
                "class" => GeographicalScope::class,
            ]);
            $wsProvider->set('field_last_election_committee_date', [
                "month" => '10',
                "year" => '2020',
            ]);
            $wsProvider->set('field_board_meets_last_year', 4);
            $wsProvider->set('field_committee_members', 4);
            $wsProvider->set('field_women_committee_members', 2);
            $wsProvider->set('field_water_tariff_inplace_inuse', '3');
            $wsProvider->set('field_tariff_type', '1');
            $wsProvider->set('field_average_monthly_tariff', [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\Entity\Currency',
                'amount' => 10,
            ]);
            $wsProvider->set('field_users_required_pay', 100);
            $wsProvider->set('field_users_uptodate_payment', 100);
            $wsProvider->set('field_monthly_amount_billed', [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\Entity\Currency',
                'amount' => 10,
            ]);
            // Deprecated field.
//            $wsProvider->set('field_monthly_amount_collected', [
//                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
//                'class' => 'App\Entity\Currency',
//                'amount' => 10,
//            ]);
            $wsProvider->set('field_location_alt', ['value' => 1, 'unit' => 'metre']);
            $wsProvider->set('field_disclosed_financial_information', 3);
            $wsProvider->set('field_carryout_om_last_year', '4');
            $wsProvider->set('field_location_lat', 5);
            $wsProvider->set('field_location_lon', 5);
            $wsProvider->{'field_status'} = 'finished';
            $wsProvider->getForm()->update($wsProvider, true);
            $logs = $this->getInquiryLogMessages($wsProvider);
            $this->assertEmpty($logs, $logs);
        }
        // Set state to complete.
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_status" => 'complete']);
        // The point will go to 'checking', and must fail and return to 'digitizing', or pass going to 'reviewing'.
        $logsRepo = $this->entityManager->getRepository(PointLog::class);
        $logsCount = $logsRepo->count([]);
        if ($logsCount > 0) {
            $this->assertEquals('digitizing', $point->{'field_status'});
        } else {
            $this->assertEquals('reviewing', $point->{'field_status'});
        }
    }

    /**
     * Test point can't go to calculated.
     */
    public function testPointCalculatedTransitionFail(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_status" => 'digitizing']);
        // Create WSP
        $wsp = $this->createWSPinPoint($session, $point->getId());
        // Add WS System
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_type_system" => "99",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);
        // Finish all forms.
        $pWrapper = new SiasarPointWrapper($point, $this->entityManager, $this->sessionManager, $this->iriConverter);
        $communities = $pWrapper->getCommunities();
        foreach ($communities as $community) {
            $community->{'field_interviewer'} = [
                'value' => SessionService::getUser()->getId(),
                'class' => User::class,
            ];
            $community->{'field_location_alt'} = [
                'value' => 1,
                'unit' => 'metre',
            ];
            $community->{'field_community_name'} = 'Aljaraque';
            $community->{'field_total_population'} = 1;
            $community->{'field_total_households'} = 2;
            $community->{'field_households_without_water_supply_system'} = 5;
            $community->{'field_with_basic_handwashing'} = 5;
            $community->{'field_status'} = 'finished';
            try {
                $community->save();
            } catch (\Exception $e) {
                $logs = $e->getMessage()."\n".$this->getInquiryLogMessages($community);
                throw new Exception($logs);
            }
        }
        $systems = $pWrapper->getSystems();
        foreach ($systems as $system) {
            $system->set('field_region', [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ]);
            $system->set('field_questionnaire_interviewer', [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ]);
            $system->set('field_design', [
                "value" => '01GESDGFYWHQ621KATV3QH9BFH',
                "class" => File::class,
            ]);
            $system->set('field_location_alt', [
                'value' => 1,
                'unit' => 'metre',
            ]);
            $system->{'field_status'} = 'finished';
            $system->save();
        }
        $wsproviders = $pWrapper->getWSProviders();
        foreach ($wsproviders as $wsProvider) {
            $wsProvider->set('field_region', [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ]);
            $wsProvider->set('field_questionnaire_interviewer', [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ]);
            $wsProvider->set('field_provider_name', 'Periquillo de los palotes');
            $wsProvider->set('field_provider_type', '1');
            $wsProvider->set('field_provider_have_rules_for_service', '4');
            $wsProvider->set('field_provider_legal_status', '1');
            $wsProvider->set('field_provider_scope', [
                "value" => '01G2MJ1G49729QPKS7CV3F5FGK',
                "class" => GeographicalScope::class,
            ]);
            $wsProvider->set('field_board_meets_last_year', 4);
            $wsProvider->set('field_committee_members', 4);
            $wsProvider->set('field_women_committee_members', 2);
            $wsProvider->set('field_water_tariff_inplace_inuse', '3');
            $wsProvider->set('field_tariff_type', '1');
            $wsProvider->set('field_average_monthly_tariff', [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\Entity\Currency',
                'amount' => 10,
            ]);
            $wsProvider->set('field_users_required_pay', 100);
            $wsProvider->set('field_users_uptodate_payment', 100);
            $wsProvider->set('field_monthly_amount_billed', [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\Entity\Currency',
                'amount' => 10,
            ]);
            $wsProvider->set('field_location_alt', ['value' => 1, 'unit' => 'metre']);
            $wsProvider->set('field_disclosed_financial_information', 3);
            $wsProvider->set('field_carryout_om_last_year', '4');
            $wsProvider->set('field_location_lat', 5);
            $wsProvider->set('field_location_lon', 5);
            $wsProvider->{'field_status'} = 'finished';
            $wsProvider->getForm()->update($wsProvider, true);
        }
        // Set state to calculated.
        $session->request(
            'PUT',
            '/api/v1/form/data/form.points/'.$point->getId(),
            [],
            [],
            [],
            json_encode(["field_status" => 'calculated'])
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals("Point status is not allowed, it's not possible to set it to planning, checking, reviewing, calculated", $data["message"]);
    }

    /**
     * Test clone a Point
     */
    public function testClonePoint(): void
    {
        $doctrine = self::$container->get('database_connection');
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_status" => 'digitizing']);

        // Finish all forms.
        $pWrapper = new SiasarPointWrapper($point, $this->entityManager, $this->sessionManager, $this->iriConverter);
        $communities = $pWrapper->getCommunities();
        foreach ($communities as $community) {
            $community->{'field_interviewer'} = [
                'value' => SessionService::getUser()->getId(),
                'class' => User::class,
            ];
            $community->{'field_location_alt'} = [
                'value' => 1,
                'unit' => 'metre',
            ];
            $community->{'field_community_name'} = 'Aljaraque';
            $community->{'field_total_population'} = 5;
            $community->{'field_total_households'} = 5;
            $community->{'field_households_without_water_supply_system'} = 5;
            $community->{'field_with_basic_handwashing'} = 5;
            $community->{'field_flush_toilets'} = true;
            $community->{'field_sewer_connection_number'} = 5;
            $community->set('field_location_lat', 5);
            $community->set('field_location_lon', 5);
            $community->{'field_status'} = 'finished';
            $community->{'field_destination_solid_waste'} = '99';
            try {
                $community->save();
                // Update community status to "validated"
                $ulid = Ulid::fromString($community->getId());
                $query = $doctrine->createQueryBuilder()
                    ->update($community->getForm()->getTableName(), 'fc')
                    ->set('fc.field_status', '"validated"')
                    ->where('HEX(fc.id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'');
                $query->execute();
            } catch (\Exception $e) {
                $logs = $e->getMessage()."\n".$this->getInquiryLogMessages($community);
                throw new Exception($logs);
            }
        }

        // Set state to complete.
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_status" => 'complete']);

        // Update point status to "calculated"
        $ulid = Ulid::fromString($point->getId());
        $query = $doctrine->createQueryBuilder()
            ->update($point->getForm()->getTableName(), 'p')
            ->set('p.field_status', '"calculated"')
            ->where('HEX(p.id) = \''.strtoupper(bin2hex($ulid->toBinary())).'\'');
        $query->execute();

        // Clone Point
        $point = $this->requestPoint('GET', $session, $point->getId());
        $cloned = $point->clone();
        $this->assertEquals('calculated', $point->{'field_status'});
        $this->assertEquals('planning', $cloned->{'field_status'});
    }

    /**
     * Test clone a Point with invalid status
     */
    public function testClonePointInvalidStatus(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);
        // Try to clone point
        $session->request('PUT', '/api/v1/form/data/form.points/'.$point->getId().'/clone');
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals("This point can't be cloned.", $data['message']);
    }

    /**
     * test Point fusion at finish WSP (field_provider_type != 3 and 4)
     */
    public function testMergePointsAtFinishWsp(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
            "field_chk_to_plan" => true,
        ];
        $point1 = $this->requestPoint('POST', $session, null, $payload);
        $point1 = $this->requestPoint('PUT', $session, $point1->getId(), ["field_status" => 'digitizing']);

        // WSP Payload
        $wspPayload = [
            'field_region' => [
                "value" => '01G7VA38911P7RX73J7GK8JFA6',
                "class" => AdministrativeDivision::class,
            ],
            'field_questionnaire_interviewer' => [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ],
            'field_provider_name' => 'Periquillo de los palotes',
            'field_provider_type' => '1',
            'field_provider_have_rules_for_service' => '4',
            'field_provider_legal_status' => '1',
            'field_provider_scope' => [
                "value" => '01G2MJ1G49729QPKS7CV3F5FGK',
                "class" => GeographicalScope::class,
            ],
            'field_last_election_committee_date' => [
                "month" => '10',
                "year" => '2020',
            ],
            'field_board_meets_last_year' => 4,
            'field_committee_members' => 4,
            'field_women_committee_members' => 2,
            'field_water_tariff_inplace_inuse' => '3',
            'field_tariff_type' => '1',
            'field_average_monthly_tariff' => [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\Entity\Currency',
                'amount' => 10,
            ],
            'field_users_required_pay' => 100,
            'field_users_uptodate_payment' => 100,
            'field_monthly_amount_billed' => [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\Entity\Currency',
                'amount' => 10,
            ],
            'field_location_alt' => [
                'value' => 1,
                'unit' => 'metre',
            ],
            'field_disclosed_financial_information' => 3,
            'field_carryout_om_last_year' => '4',
            'field_location_lat' => 5,
            'field_location_lon' => 5,
            'field_chk_total_expenses' => '1',
        ];

        // Create WSP in the new Point
        $wsp = $this->createWSPinPoint($session, $point1->getId(), $wspPayload);
        $this->assertEquals($point1->getId(), $wsp->{'field_point'}->getId());

        // Add 'form.wssystem.communities' subform first to associate it to the system
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01G7VA38911P7RX73J7GK8JFA6',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );
        $sysPayload = [
            "field_system_name" => "Name of system",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $point1->getId(), $sysPayload);

        // Create another Point
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3",
            "field_chk_to_plan" => true,
        ];
        $point2 = $this->requestPoint('POST', $session, null, $payload);
        $point2 = $this->requestPoint('PUT', $session, $point2->getId(), ["field_status" => 'digitizing']);

        // Add 'form.wssystem.communities' subform first to associate it to the system
        $form = $this->formFactory->find('form.wssystem.communities');
        $sys2CommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sys2Payload = [
            "field_system_name" => "Other name of system",
            "field_served_communities" => [
                [
                    "value" => $sys2CommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system2 = $this->createWSSystemInPoint($session, $point2->getId(), $sys2Payload);
        $this->assertCount(1, $point2->{'field_communities'});
        $this->assertCount(1, $point2->{'field_wsystems'});
        $this->assertEquals($wsp->getId(), $point2->{'field_wsps'}[0]->getId());

        // Finish WSP to execute the fusion
        $wsp->{'field_status'} = 'finished';
        $wsp->save();
        $logs = $this->getInquiryLogMessages($wsp);
        $this->assertEmpty($logs, $logs);

        $mergedPoint = $this->requestPoint('GET', $session, $point2->getId());
        $this->assertCount(2, $mergedPoint->{'field_communities'});
        $this->assertCount(2, $mergedPoint->{'field_wsystems'});

        // Try to get the merged point
        $session->request('GET', '/api/v1/form/data/form.points/'.$point1->getId());
        $response = $session->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }

    /**
     * test recalculate points at validate WSP (field_provider_type = 3 or 4)
     */
    public function testRecalculatePoints()
    {
        $calculatedPoint = $this->loadRecord(dirname(__FILE__).'/../../assets/form_records/point_01GM8YJPVV9HY6QKBNJWFXB805_calculated.json');
        $this->assertEquals('calculated', $calculatedPoint->{'field_status'});
        $wsp = current($calculatedPoint->{'field_wsps'});

        // Create Point
        $session = $this->loginRest('Pedro');
        $payload = [
            "administrative_division" => "/api/v1/administrative_divisions/01G7VA38911P7RX73J7GK8JFA6",
        ];
        $point = $this->requestPoint('POST', $session, null, $payload);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true]);

        // Add 'form.wssystem.communities' subform first to associate it to the system
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01G7VA38911P7RX73J7GK8JFA6',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );
        $sysPayload = [
            "field_system_name" => "Name of system",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);

        // Restore `field_point` that was removed in the dump
        $wsp->set('field_point', [
            "value" => $calculatedPoint->getId(),
            "form" => 'form.point',
        ]);
        // Back to draft
        $wsp->{'field_status'} = 'draft';
        $wsp->save();
        // Back to finished
        $wsp->{'field_status'} = 'finished';
        $wsp->save();
        // Back to validated
        $wsp->{'field_status'} = 'validated';
        $wsp->save();

        $calculatedPoint = $this->requestPoint('GET', $session, $calculatedPoint->getId());
        $this->assertEquals('calculating', $calculatedPoint->{'field_status'});
    }

    /**
     * Test replan point: create new point with selected inquiries
     */
    public function testReplanPoint()
    {
        $session = $this->loginRest('Pedro');

        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true, "field_status" => 'digitizing']);

        // Create WSP
        $wsp = $this->createWSPinPoint($session, $point->getId());
        // Add WS System
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01G7VA38911P7RX73J7GK8JFA6',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_type_system" => "99",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $system = $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);

        $point = $this->requestPoint('GET', $session, $point->getId());
        $this->assertCount(2, $point->{'field_communities'});

        // Delete field 1.12 to unlink community on Point
        $system->{'field_served_communities'} = [];
        $system->save();

        $requestBody = [
            "form.community" => [
                $point->{'field_communities'}[1]->getId(),
            ],
        ];
        $session->request('PUT', '/api/v1/form/data/form.points/'.$point->getId().'/replan', [], [], [], json_encode($requestBody));
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());

        $point = $this->requestPoint('GET', $session, $point->getId());
        $this->assertCount(1, $point->{'field_communities'});
    }

    /**
     * Test replan point with a community linked to a system
     */
    public function testReplanPointCommunityLinked()
    {
        $session = $this->loginRest('Pedro');

        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true, "field_status" => 'digitizing']);

        // Create WSP
        $wsp = $this->createWSPinPoint($session, $point->getId());
        // Add WS System
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_type_system" => "99",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);

        $requestBody = [
            "form.community" => [
                $point->{'field_communities'}[0]->getId(),
            ],
        ];
        $session->request('PUT', '/api/v1/form/data/form.points/'.$point->getId().'/replan', [], [], [], json_encode($requestBody));
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Community "6-CODE" is linked to a system.', $data['message']);
    }

    /**
     * Test replan point with a only one community
     */
    public function testReplanPointUniqueCommunity()
    {
        $session = $this->loginRest('Pedro');

        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true, "field_status" => 'digitizing']);

        $requestBody = [
            "form.community" => [
                $point->{'field_communities'}[0]->getId(),
            ],
        ];
        $session->request('PUT', '/api/v1/form/data/form.points/'.$point->getId().'/replan', [], [], [], json_encode($requestBody));
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('There must be at least one community left in the Point.', $data['message']);
    }

    /**
     * Test delete a community from specific Point
     */
    public function testPointDeleteCommunityOk(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true, "field_status" => 'digitizing']);
        // Create WSP
        $wsp = $this->createWSPinPoint($session, $point->getId());
        // Add WS System
        // Add first a 'form.wssystem.communities' to associate it to the system (field 1.12)
        $form = $this->formFactory->find('form.wssystem.communities');
        $sysCommId = $form->insert(
            [
                "field_community" => [
                    "value" => '01G7VA38911P7RX73J7GK8JFA6',
                    "class" => AdministrativeDivision::class,
                ],
                "field_provider" => [
                    "value" => $wsp->getId(),
                    "form" => "form.wsprovider",
                ],
                'field_households' => 50,
            ]
        );

        $sysPayload = [
            "field_system_name" => "Name of system 1",
            "field_type_system" => "99",
            "field_served_communities" => [
                [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ],
            ],
        ];
        $this->createWSSystemInPoint($session, $point->getId(), $sysPayload);

        $point = $this->requestPoint('GET', $session, $point->getId());
        $this->assertCount(2, $point->{'field_communities'});

        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.community/'.$point->{'field_communities'}[0]->getId(),
        );
        $response = $session->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Test delete a community from specific Point
     */
    public function testPointDeleteCommunityFail(): void
    {
        $session = $this->loginRest('Pedro');
        // Create Point
        $point = $this->requestPoint('POST', $session, null, ["administrative_division" => "/api/v1/administrative_divisions/01FFFC1K7EG1E2AZ4H2H5ZXQS3"]);
        $point = $this->requestPoint('PUT', $session, $point->getId(), ["field_chk_to_plan" => true, "field_status" => 'digitizing']);

        $session->request(
            'DELETE',
            '/api/v1/form/data/form.points/'.$point->getId().'/form.community/'.$point->{'field_communities'}[0]->getId(),
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals("Community \"".$point->{'field_communities'}[0]->getId()."\" is the last in the Point \"".$point->getId()."\", we can't remove it", $data['message']);
    }
}
