<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
namespace App\Tests\Functional\SiasarPoint;

use App\Entity\AdministrativeDivision;
use App\Entity\HouseholdProcess;
use App\Entity\User;
use App\Forms\FormRecord;
use App\Repository\HouseholdProcessRepository;
use App\Tools\Json;
use Exception;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * test household process endpoint
 */
class HouseholdProcessResourceTest extends PointTestBase
{
    private $payload = [
        "communityReference" => "01G74YQ9XYARRPPRJZ69GPFCME",
        "country" => "/api/v1/countries/hn",
    ];

    /**
     * Setup tests.
     *
     * @return void
     */
    public function setUp(): void
    {
        $this->extraConfiguration['form.community.household'] = dirname(__FILE__).'/../../assets/form.community.household.yml';
        parent::setUp();
        $this->loginUser('pedro');
    }

    /**
     * testing listing
     */
    public function testHouseholdProcessListing()
    {
        $session = $this->loginRest('Pedro');
        $session->request('GET', '/api/v1/household_processes');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing listing user without access to any household process.
     */
    public function testHouseholdProcessUnauthorizedListing()
    {
        $this->loginUser('Nico');
        $session = $this->loginRest('Nico');
        $session->request('GET', '/api/v1/household_processes');
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEmpty($data);
    }

    /**
     * testing user listing
     */
    public function testHouseholdProcessUserListing()
    {
        $this->loginUser('Pedro');
        // Use current user ID how replacement loading records.
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy(['username' => 'Supervisor']);
        $this->createHHProcessWithUser($user);
        // Test listing.
        $this->entityManager->clear();
        $session = $this->loginRest('Supervisor');
        $session->request('GET', '/api/v1/household_processes');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotEmpty($data);

        // Validate permission visibility.
        foreach ($data as $item) {
            $this->assertEquals('/api/v1/countries/hn', $item['country']);
        }
    }

    /**
     * testing household_process sort listing.
     */
    public function testHouseholdProcessSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'id' => 'asc',
            ],
        ];
        $session = $this->loginRest('Pedro');
        $session->request('GET', '/api/v1/household_processes', $query);
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $householdProcess) {
            if (!$firstData) {
                $firstData = $householdProcess;
                break;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'id' => 'desc',
            ],
        ];
        $session->request('GET', '/api/v1/household_processes', $query);
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $householdProcess) {
            $this->assertNotEquals($firstData['id'], $householdProcess['id']);
            break;
        }
    }

    /**
     * testing household_process paginated listing.
     */
    public function testHouseholdProcessPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        $session = $this->loginRest('Pedro');
        $session->request('GET', '/api/v1/household_processes', $query);
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing household_process detection
     */
    public function testHouseholdProcessShowing()
    {
        $session = $this->loginRest('Pedro');
        $session->request('GET', '/api/v1/household_processes/01G6WNZ83K89S4TCMCB1FD8MWZ');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('01G6WNZ83K89S4TCMCB1FD8MWZ', $data['id']);
        $this->assertEquals('/api/v1/countries/hn', $data['country']);
    }

    /**
     * testing household_process detection
     */
    public function testHouseholdProcessUserShowing()
    {
        $session = $this->loginRest('Supervisor');
        $session->request('GET', '/api/v1/household_processes/01G6WNZ83K89S4TCMCB1FD8MWZ');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('01G6WNZ83K89S4TCMCB1FD8MWZ', $data['id']);
        $this->assertEquals('/api/v1/countries/hn', $data['country']);
    }

    /**
     * testing household_process edition
     */
    public function testHouseholdProcessPutting()
    {
        $payload['open'] = false;
        $session = $this->loginRest('Pedro');
        $session->request('PUT', '/api/v1/household_processes/01G6WNZ83K89S4TCMCB1FD8MWZ', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals(false, $data['open']);
    }

    /**
     * testing household_process edition
     */
    public function testHouseholdProcessUserPutting()
    {
        $session = $this->loginRest('Marta');
        $payload['open'] = false;
        $session->request('PUT', '/api/v1/household_processes/01G6WNZ83K89S4TCMCB1FD8MWZ', [], [], [], \json_encode($payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing add household survey to a specific household_process
     */
    public function testHouseholdProcessAddHousehold()
    {
        $session = $this->loginRest('Pedro');
        $session->request('POST', '/api/v1/household_processes/01G6WNZ83K89S4TCMCB1FD8MWZ/add_household');
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing we can delete a household questionary from a specific household_process
     */
    public function testHouseholdProcessDeleteHousehold()
    {
        $session = $this->loginRest('Pedro');
        $session->request('POST', '/api/v1/household_processes/01G6WNZ83K89S4TCMCB1FD8MWZ/add_household');
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $data = $this->getResponseArray($response);

        // Validate creation.
        $householdForm = $this->formFactory->find('form.community.household');
        $household = $householdForm->find($data['id']);
        $this->assertNotNull($household);

        // Delete it and validate.
        $session->request('DELETE', '/api/v1/form/data/form.community.households/'.$data["id"]);
        $household = $householdForm->find($data['id']);
        $this->assertNull($household);
    }

    /**
     * testing user add household survey to a specific household_process
     */
    public function testHouseholdProcessUserAddHousehold()
    {
        $this->loginUser('Pedro');
        // Use current user ID how replacement loading records.
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy(['username' => 'Marta']);
        $hhp = $this->createHHProcessWithUser($user);
        // Run test.
        $session = $this->loginRest('Marta');
        $session->request('POST', sprintf('/api/v1/household_processes/%s/add_household', $hhp->getId()));
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals(
            'The "create" action require the permission "create household record".',
            $data['message']
        );
    }

    /**
     * testing add household survey to a closed household_process
     */
    public function testHouseholdProcessClosedAddHousehold()
    {
        $session = $this->loginRest('Pedro');
        $session->request('POST', '/api/v1/household_processes/01G76T4K7YF38XC59VFCSG5Z0Y/add_household');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('The process is closed.', $data[0]);
    }

    /**
     * testing edit household record in a closed process.
     */
    public function testHouseholdProcessClosedEditHousehold()
    {
        $session = $this->loginRest('Pedro');
        // Create household record.
        $session->request('POST', '/api/v1/household_processes/01G6WP0CM5RV3SJQG2P3HKNCRJ/add_household');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);

        // Close the process.
        /** @var HouseholdProcessRepository $processRepo */
        $processRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        /** @var HouseholdProcess $process */
        $process = $processRepo->find('01G6WP0CM5RV3SJQG2P3HKNCRJ');
        $process->setOpen(false);
        $processRepo->saveNow($process);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('The process is closed.');

        // Edit record.
        $householdForm = $this->formFactory->find('form.community.household');
        $household = $householdForm->find($data['id']);
        $household->{'field_finished'} = true;
        $household->save();
    }

    /**
     * testing list household surveys by current user
     */
    public function testHouseholdProcessMyHousholdsListing()
    {
        $this->loginUser('Pedro');
        // Use current user ID how replacement loading records.
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy(['username' => 'Supervisor']);
        $hhp = $this->createHHProcessWithUser($user);
        // List household surveys by current user
        $session = $this->loginRest('Supervisor');
        // Insert first a record to this user
        $session->request('POST', sprintf('/api/v1/household_processes/%s/add_household', $hhp->getId()));
        // Read
        $session->request('GET', '/api/v1/household_processes/my_households');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertNotEmpty($data);
    }

    /**
     * testing that close household process fire the household process calculates.
     */
    public function testHouseholdProcessCalculate()
    {
        $this->loginUser('Pedro');

        // Create community record.
        $commForm = $this->formFactory->find('form.community');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $commRecordId = $commForm->insert(['field_region' => $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3')]);
        $commRecord = $commForm->find($commRecordId);
        $commRecord->{'field_have_households'} = true;
        $commRecord->{'field_total_households'} = 125;
        $commRecord->save();
        /** @var HouseholdProcess $process */
        $process = $commRecord->{'field_household_process'};

        $session = $this->loginRest('Pedro');
        // Add household record.
        $session->request(
            'POST',
            sprintf('/api/v1/household_processes/%s/add_household', $process->getId())
        );
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        $data = Json::decode($response->getContent());

        $form = $this->formFactory->find('form.community.household');
        // Update household form.
        $record = $form->find($data['id']);
        $record->{'field_have_access_water_from_community_supply'} = true;
        $record->{'field_toilet_facility'} = 1;
        $record->{'field_show_wash_hands'} = 1;
        $record->{'field_observed_soap_at_place'} = 1;
        $record->{'field_observed_water_at_place'} = 1;
        $record->save();

        // Add a second household record.
        $cloned1 = $record->clone();
        $cloned1->{'field_have_access_water_from_community_supply'} = false;
        $cloned1->{'field_finished'} = true;
        $cloned1->save();

        $record->{'field_finished'} = true;
        $record->save();

        // Add a third household record.
        $cloned2 = $record->clone();

        $householdRecords = [
            $record,
            $cloned1,
            $cloned2,
        ];

        // Close household process.
        $session->request(
            'PUT',
            sprintf('/api/v1/household_processes/%s/close', $process->getId())
        );
        $response = $session->getResponse();
        $data = Json::decode($response->getContent());
        $message = '';
        if (isset($data["message"])) {
            $message = $data["message"];
        }
        $this->assertEquals(200, $response->getStatusCode(), $message);

        // Validate results.
        $cntFinished = 0;
        $cntAnswered = 0;
        foreach ($householdRecords as $hhRecord) {
            if ($hhRecord->{'field_finished'}) {
                $cntFinished++;
            }
            if ($hhRecord->{'field_finished'} && $hhRecord->{'field_have_access_water_from_community_supply'}) {
                $cntAnswered++;
            }
        }
        $commRecord = $commForm->find($commRecordId);
        $result = ($cntAnswered * $commRecord->{'field_total_households'}) / $cntFinished;
        $this->assertEquals(round($result), $commRecord->{'field_n_hh_communal_ws'});
    }

    /**
     * testing household_process no duplicate after update community.
     */
    public function testNotCreateDuplicatedHHProcessAfterSaveCommunity()
    {
        $this->loginUser('Pedro');
        // Use current user ID how replacement loading records.
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy(['username' => 'Supervisor']);
        $hhp = $this->createHHProcessWithUser($user);
        $this->assertNotNull($hhp);
        // Find all HH Process to the survey.
        $commRef = $hhp->getCommunityReference();
        $communityForm = $this->formFactory->find('form.community');
        $community = $communityForm->find($commRef);
        $this->assertNotNull($community);
        // Save to test duplication error.
        $community->{'field_n_hh_from_comm_ws_not_drink'} = 25;
        $community->{'field_have_households'} = true;
        $community->save();
        // Find duplicates.
        $hhpRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $hhprocesses = $hhpRepo->findBy([
            'communityReference' => $commRef,
        ]);
        $this->assertCount(1, $hhprocesses);
    }

    /**
     * Create a new point with data and a valid household process.
     *
     * @param UserInterface $user Add this user how records owner.
     *
     * @return HouseholdProcess
     *   The household process created.
     *
     * @throws Exception
     */
    protected function createHHProcessWithUser(UserInterface $user): HouseholdProcess
    {
        // Add data to test.
        // Load point and inquiries records.
        $this->loadRecord(
            dirname(__FILE__).'/../../assets/form_records/point_01H1YW4H5T3X8NH378DAZ2C4QY.json',
            [
                User::class => $user->getId(),
            ]
        );
        // Change household process in the community.
        $point = current($this->formFactory->find('point')->findAll());
        /** @var FormRecord[] $communities */
        $communities = $point->{'field_communities'};
        // Force community to lose household process.
        $communities[0]->{'field_have_households'} = false;
        $communities[0]->{'field_household_process'} = null;
        $communities[0]->getForm()->update($communities[0], true);
        // Create a new household process linked to this community.
        $communities[0]->{'field_have_households'} = true;
        $communities[0]->save();

        // Load a household record.
        $hhp = $communities[0]->{'field_household_process'}->getId();
        $this->loadRecord(
            dirname(__FILE__).'/../../assets/form_records/household_01H2H13CQ02ZPM2BHV6EACZ6QB.json',
            [
                User::class => $user->getId(),
                HouseholdProcess::class => $hhp,
            ]
        );

        return $communities[0]->{'field_household_process'};
    }
}
