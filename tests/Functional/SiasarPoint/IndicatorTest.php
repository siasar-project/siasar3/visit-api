<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\SiasarPoint;

use App\Entity\AdministrativeDivision;
use App\Entity\Indicator;
use App\Entity\TechnicalAssistanceProvider;
use App\Entity\TypeHealthcareFacility;
use App\Entity\TypeTap;
use App\Entity\User;
use App\Indicators\HCCIndicator;
use App\Indicators\PATIndicator;
use App\Indicators\SHCIndicator;
use App\Indicators\SimpleIndicatorContext;
use App\Repository\IndicatorRepository;
use App\Service\SessionService;
use Symfony\Component\Uid\Ulid;

/**
 * Test Inquiry collection end-point.
 */
class IndicatorTest extends PointTestBase
{
    protected SessionService $sessionService;
    protected IndicatorRepository $indicatorRepo;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->sessionService = static::getContainerInstance()->get('session_service');
        $this->indicatorRepo = $this->entityManager->getRepository(Indicator::class);
    }

    /**
     * Test form.tap indicator calculate.
     *
     * @return void
     */
    public function testTapIndicator(): void
    {
        $form = $this->formFactory->find('form.tap');
        $recordId = $form->insert([
            'field_region' => [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ],
            'field_questionnaire_interviewer' => [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ],
            'field_location_alt' => [
                'value' => 1,
                'unit' => 'metre',
            ],
            'field_provider_name' => [
                "value" => '01FENEDYFA6KNAA4J459P5S0RF',
                "class" => TechnicalAssistanceProvider::class,
            ],
            'field_provider_type' => [
                "value" => '01G3TXHW5BWC4RSFHN3SREGRDQ',
                "class" => TypeTap::class,
            ],
            'field_number_tap_should_assist' => 1,
            'field_number_tap_supported_last_12_months' => 1,
            'field_what_phase_provide_technical_assistance' => [
                '1',
                '2',
            ],
            'field_assistance_provided_planning' => true,
            'field_providing_ta_system' => true,
            'field_assistance_provided_planning_assisted' => 1,
            'field_location_lat' => 1,
            'field_location_lon' => 1,
        ]);
        $record = $form->find($recordId);

        // Move record to validated, to throw indicator calculation.
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertEquals('finished', $record->{'field_status'}, $this->getInquiryLogMessages($record));
        $record->{'field_status'} = 'validated';
        $record->save();
        $this->assertEquals('validated', $record->{'field_status'}, $this->getInquiryLogMessages($record));

        // Find the generated indicator.
        $ulid = Ulid::fromString($recordId);
        $indicators = $this->indicatorRepo->findBy(
            [
                'record' => $ulid->toBinary(),
                'name' => 'PAT',
            ]
        );
        $this->assertCount(1, $indicators);
        $this->assertEquals(0.25, $indicators[0]->getValue());
        $resolver = new PATIndicator(new SimpleIndicatorContext($record));
        $this->assertEquals('D', $resolver->getLabel());

        // Query the end-point to validate indicator.
        $session = $this->loginRest('Pedro');
        $session->request(
            'GET',
            '/api/v1/form/data/form.taps/'.$recordId
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertEquals(0.25, $data["indicator"]);
    }

    /**
     * Test form.school indicator calculate.
     *
     * @return void
     */
    public function testSchoolIndicator(): void
    {
        $form = $this->formFactory->find('form.school');
        $recordId = $form->insert([
            'field_girls_only_toilets_total' => 1,
            'field_staff_total_number_of_women' => 1,
            'field_region' => [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ],
            'field_questionnaire_interviewer' => [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ],
            'field_location_alt' => [
                'value' => 1,
                'unit' => 'metre',
            ],
            'field_school_name' => 'MadSushiDev',
            'field_staff_total_number_of_men' => 1,
            'field_student_total_number_of_female' => 1,
            'field_student_total_number_of_male' => 1,
            'field_location_lat' => 1,
            'field_location_lon' => 1,
            'field_handwashing_facilities_soap_water' => '1',
            'field_handwashing_facilities' => true,
            'field_school_type' => ['1'],
            'field_rural_communities_served' => [
                [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
            ],
            'field_have_water_supply_system' => ['2'],
        ]);
        $record = $form->find($recordId);

        // Move record to validated, to throw indicator calculation.
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertEquals('finished', $record->{'field_status'}, $this->getInquiryLogMessages($record));
        $record->{'field_status'} = 'validated';
        $record->save();
        $this->assertEquals('validated', $record->{'field_status'}, $this->getInquiryLogMessages($record));

        // Find the generated indicator.
        $ulid = Ulid::fromString($recordId);
        $indicators = $this->indicatorRepo->findBy(
            [
                'record' => $ulid->toBinary(),
                'name' => 'SHC',
            ]
        );
        $this->assertCount(1, $indicators);
        $this->assertEquals(0.250, $indicators[0]->getValue());
        $resolver = new SHCIndicator(new SimpleIndicatorContext($record));
        $this->assertEquals('D', $resolver->getLabel());

        // Query the end-point to validate indicator.
        $session = $this->loginRest('Pedro');
        $session->request(
            'GET',
            '/api/v1/form/data/form.schools/'.$recordId
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertEquals(0.25, $data["indicator"]);
    }

    /**
     * Test form.school indicator not calculable.
     *
     * @return void
     */
    public function testSchoolIndicatorNotCalculable(): void
    {
        $form = $this->formFactory->find('form.school');
        $recordId = $form->insert([
            'field_girls_only_toilets_total' => 1,
            'field_staff_total_number_of_women' => 1,
            'field_region' => [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ],
            'field_questionnaire_interviewer' => [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ],
            'field_location_alt' => [
                'value' => 1,
                'unit' => 'metre',
            ],
            'field_school_name' => 'MadSushiDev',
            'field_staff_total_number_of_men' => 1,
            'field_student_total_number_of_female' => 0,
            'field_student_total_number_of_male' => 0,
            'field_location_lat' => 1,
            'field_location_lon' => 1,
            'field_handwashing_facilities_soap_water' => '1',
            'field_handwashing_facilities' => true,
            'field_school_type' => ['1'],
            'field_rural_communities_served' => [
                [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
            ],
            'field_have_water_supply_system' => ['2'],
        ]);
        $record = $form->find($recordId);

        // Move record to validated, to throw indicator calculation.
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertEquals('finished', $record->{'field_status'}, $this->getInquiryLogMessages($record));
        $record->{'field_status'} = 'validated';
        $record->save();
        $this->assertEquals('validated', $record->{'field_status'}, $this->getInquiryLogMessages($record));

        // Some indicator must be -1.
        $ulid = Ulid::fromString($recordId);
        $indicatorsNotCalculable = $this->indicatorRepo->count(
            [
                'record' => $ulid->toBinary(),
                'value' => -1,
            ]
        );
        $this->assertNotEquals(0, $indicatorsNotCalculable, 'None exists -1 indicators');

        // Find the generated indicator.
        $ulid = Ulid::fromString($recordId);
        $indicators = $this->indicatorRepo->findBy(
            [
                'record' => $ulid->toBinary(),
                'name' => 'SHC',
            ]
        );
        $this->assertCount(1, $indicators);
        $this->assertEquals(0.250, $indicators[0]->getValue());
        $resolver = new SHCIndicator(new SimpleIndicatorContext($record));
        $this->assertEquals('D', $resolver->getLabel());

        // Query the end-point to validate indicator.
        $session = $this->loginRest('Pedro');
        $session->request(
            'GET',
            '/api/v1/form/data/form.schools/'.$recordId
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertEquals(0.25, $data["indicator"]);
    }

    /**
     * Test form.health.care indicator calculate.
     *
     * @return void
     */
    public function testHealthCareIndicator(): void
    {
        $form = $this->formFactory->find('form.health.care');
        $recordId = $form->insert([
            'field_region' => [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ],
            'field_questionnaire_interviewer' => [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ],
            'field_location_alt' => [
                'value' => 1,
                'unit' => 'metre',
            ],
            'field_name' => 'Andalucía',
            'field_female_employees' => 1,
            'field_male_employees' => 1,
            'field_female_patients_per_day' => 10,
            'field_male_patients_per_day' => 10,
            'field_are_soap_and_water' => 1,
            'field_have_water_supply' => ['1'],
            'field_have_toilets' => true,
            'field_usable_toilet_women' => 1,
            'field_type_toilets' => '1',
            'field_total_toilet_women' => 10,
            'field_location_lat' => 10,
            'field_location_lon' => 10,
            'field_type' => [
                [
                    "value" => '01G3TP16FW16HYFHJ9S13MDCGH',
                    "class" => TypeHealthcareFacility::class,
                ],
            ],
            'field_communities_served' => [
                [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
            ],
        ]);
        $record = $form->find($recordId);

        // Move record to validated, to throw indicator calculation.
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertEquals('finished', $record->{'field_status'}, $this->getInquiryLogMessages($record));
        $record->{'field_status'} = 'validated';
        $record->save();
        $this->assertEquals('validated', $record->{'field_status'}, $this->getInquiryLogMessages($record));

        // Find the generated indicator.
        $ulid = Ulid::fromString($recordId);
        $indicators = $this->indicatorRepo->findBy(
            [
                'record' => $ulid->toBinary(),
                'name' => 'HCC',
            ]
        );
        $this->assertCount(1, $indicators);
        $this->assertEquals(0.1, $indicators[0]->getValue());
        $resolver = new HCCIndicator(new SimpleIndicatorContext($record));
        $this->assertEquals('D', $resolver->getLabel());

        // Query the end-point to validate indicator.
        $session = $this->loginRest('Pedro');
        $session->request(
            'GET',
            '/api/v1/form/data/form.health.cares/'.$recordId
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertEquals(0.1, $data["indicator"]);
    }

    /**
     * Test form.health.care indicator not calculable.
     *
     * @return void
     */
    public function testHealthCareIndicatorNotCalculable(): void
    {
        $form = $this->formFactory->find('form.health.care');
        $recordId = $form->insert([
            'field_region' => [
                "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                "class" => AdministrativeDivision::class,
            ],
            'field_questionnaire_interviewer' => [
                "value" => '01FHX4RK866FRCDAYYYCZKVFXJ',
                "class" => User::class,
            ],
            'field_location_alt' => [
                'value' => 1,
                'unit' => 'metre',
            ],
            'field_name' => 'Andalucía',
            'field_female_employees' => 1,
            'field_male_employees' => 1,
            'field_female_patients_per_day' => 1,
            'field_male_patients_per_day' => 0,
            'field_are_soap_and_water' => 1,
            'field_have_water_supply' => ['1'],
            'field_have_toilets' => true,
            'field_usable_toilet_women' => 1,
            'field_type_toilets' => '1',
            'field_total_toilet_women' => 10,
            'field_location_lat' => 10,
            'field_location_lon' => 10,
            'field_type' => [
                [
                    "value" => '01G3TP16FW16HYFHJ9S13MDCGH',
                    "class" => TypeHealthcareFacility::class,
                ],
            ],
            'field_communities_served' => [
                [
                    "value" => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
                    "class" => AdministrativeDivision::class,
                ],
            ],
        ]);
        $record = $form->find($recordId);

        // Move record to validated, to throw indicator calculation.
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertEquals('finished', $record->{'field_status'}, $this->getInquiryLogMessages($record));
        $record->{'field_status'} = 'validated';
        $record->save();
        $this->assertEquals('validated', $record->{'field_status'}, $this->getInquiryLogMessages($record));

        // Some indicator must be -1.
        $ulid = Ulid::fromString($recordId);
        $indicatorsNotCalculable = $this->indicatorRepo->count(
            [
                'record' => $ulid->toBinary(),
                'value' => -1,
            ]
        );
        $this->assertEquals(0, $indicatorsNotCalculable, 'None exists -1 indicators');

        // Find the generated indicator.
        $indicators = $this->indicatorRepo->findBy(
            [
                'record' => $ulid->toBinary(),
                'name' => 'HCC',
            ]
        );
        $this->assertCount(1, $indicators);
        $this->assertEquals(0.1, $indicators[0]->getValue());
        $resolver = new HCCIndicator(new SimpleIndicatorContext($record));
        $this->assertEquals('D', $resolver->getLabel());

        // Query the end-point to validate indicator.
        $session = $this->loginRest('Pedro');
        $session->request(
            'GET',
            '/api/v1/form/data/form.health.cares/'.$recordId
        );
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        $this->assertEquals(0.1, $data["indicator"]);
    }
}
