<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\SiasarPoint;

use App\Entity\Indicator;
use App\Entity\InquiryFormLog;
use App\Forms\FormRecord;
use App\Indicators\AbstractIndicator;
use App\Indicators\IndicatorInterface;
use App\Indicators\PointIndicatorContext;
use App\Repository\IndicatorRepository;
use App\Service\SessionService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Uid\Ulid;

/**
 * Test Inquiry collection end-point.
 */
class PointIndicatorTest extends PointTestBase
{
    protected SessionService $sessionService;
    protected IndicatorRepository $indicatorRepo;
    protected LoggerInterface $inquiryFormLogger;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->sessionService = static::getContainerInstance()->get('session_service');
        $this->indicatorRepo = $this->entityManager->getRepository(Indicator::class);
        $this->inquiryFormLogger = static::getContainerInstance()->get('monolog.logger.inquiry_form');
    }

    /**
     * Test that can execute Point forms indicators.
     *
     * @dataProvider getPoints
     *
     * @param string      $file
     * @param string|null $tester
     * @param array       $indicators
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testPointIndicator(string $file, string $tester = null, array $indicators = []):void
    {
        $this->validatePointIndicator($file, $tester, $indicators);
    }

    /**
     * @return array[]  [[string, string|null]]
     */
    public function getPoints()
    {
        return [
            ['file' => 'point_01GQ27ZWJ8B36CDFQZVFFME85B.json', 'tester' => null],
            ['file' => 'point_01GM8PTSVRY84F6QQ2XK2MV0GZ.json', 'tester' => null],
            ['file' => 'point_01H14W53EFSF93P65HQ1B5N64C.json', 'tester' => null,
                'indicators' => [
                    // field_reference (com)
                    '646cfe9b11c26' => [
                        'value' => 0.47,
                        'label' => 'C',
                        'sub_indicators' => [
                            'WSL' => 0.65,
                            'WSLACC' => 0.71,
                            'WSLCON' => 0.88,
                            'WSLSEA' => 0.5,
                            'WSLQUA' => 0.5,
                            'SHL' => 0.31,
                        ],
                    ],
                    //field_reference (sp)
                    '646d01cf43d05' => [
                        'value' => 0.43,
                        'label' => 'C',
                        'sub_indicators' => [
                            'SEPORG' => 0.46,
                            'SEPOPM' => 0.57,
                            'SEPECO' => 0.29,
                            'SEPENV' => 0.4,
                        ],
                    ],
                    //field_reference (sys)
                    '646d063a6ca50' => [
                        'value' => 0.69,
                        'label' => 'C',
                        'sub_indicators' => [
                            'WSIAUT' => 0.83,
                            'WSIINF' => 0.91,
                            'WSIPRO' => 1,
                            'WSITRE' => 0,
                        ],
                    ],
                    // field_reference (sys)
                    '646e1eb12cfb9' => [
                        'value' => 0.38,
                        'label' => 'D',
                        'sub_indicators' => [
                            'WSIAUT' => 0,
                            'WSIINF' => 0.63,
                            'WSIPRO' => 0.5,
                            'WSITRE' => 0,
                        ],
                    ],
                ],
            ],
            ['file' => 'point_01H2ZXC89FZS7X4XYB77DXTA6C.json', 'tester' => null,
                'indicators' => [
                    '648b38dd71b59' => [
                        'value' => 0.56,
                        'label' => 'C',
                        'sub_indicators' => [
                            'WSICom' => 0.68,
                            'WSL' => 0.73,
                            'SHL' => 0.33,
                            'SEPCom' => 0.59,
                        ],
                    ],
                    '648b3b539efb0' => [
                        'value' => 0.97,
                        'label' => 'A',
                        'sub_indicators' => [
                            'SEPENV' => 1.00,
                            'SEPOPM' => 0.91,
                            'SEPECO' => 0.98,
                            'SEPORG' => 1,
                        ],
                    ],
                ],
            ],
            ['file' => 'point_01H52VAYA3W3EM2G726EXCNHBW.json', 'tester' => null,
                'indicators' => [
                    '64ad7e5442e28' => [
                        'value' => 0.65,
                        'label' => 'C',
                        'sub_indicators' => [
                            'WSL' => 0.76,
                            'SHL' => 0.36,
                            'WSICom' => 0.80,
                            'SEPCom' => 0.80,
                        ],
                    ],
                    '64ad94a3d05a2' => [
                        'value' => 0.99,
                        'label' => 'A',
                        'sub_indicators' => [
                            'WSL' => 0.96,
                            'SHL' => 1,
                            'WSICom' => 1,
                            'SEPCom' => 1,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Load point forms and validate that get a valid indicator value.
     *
     * @param string      $file
     * @param string|null $tester
     * @param array       $indicators
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function validatePointIndicator(string $file, string $tester = null, array $indicators = [])
    {
        $point = $this->loadRecord(dirname(__FILE__).'/../../assets/form_records/'.$file);
        $communities = $point->{'field_communities'};
        $wsps = $point->{'field_wsps'};
        $systems = $point->{'field_wsystems'};
        $records = array_merge($communities, $wsps, $systems);
        foreach ($records as $record) {
            $context = new PointIndicatorContext($record, $point);
            $formMetaDefinition = $record->getForm()->getMeta();
            $indicatorClass = $formMetaDefinition['indicator_class'];
            /** @var AbstractIndicator $indicator */
            $indicator = new $indicatorClass($context);
            if (!$tester) {
                $value = $indicator->getValue();
                $label = $indicator->getLabel(true);
                if (isset($indicators[$record->{'field_reference'}])) {
                    if (isset($indicators[$record->{'field_reference'}]['sub_indicators'])) {
                        $targetValues = $indicators[$record->{'field_reference'}]['sub_indicators'];
                        $indicatorValues = $this->formatIndicatorsList($indicator);
                        foreach ($targetValues as $targetName => $targetValue) {
                            $this->assertEquals($targetValue, $indicatorValues['rnd'][$targetName], sprintf('Sub-indicator %s fail with value %s, it must be %s', $targetName, $indicatorValues['rnd'][$targetName], $targetValue));
                        }
                    }
                    $this->assertEquals($indicators[$record->{'field_reference'}]['value'], round($value, 2));
                    $this->assertEquals($indicators[$record->{'field_reference'}]['label'], $label);
                }
                $this->assertTrue($value >= 0, sprintf('%s with value %s', $label, $value));
            } else {
                $this->$tester($indicator, $record);
            }
        }
    }

    /**
     * Assert that a bad point back to draft and set a error log message.
     *
     * @param IndicatorInterface $indicator
     * @param FormRecord         $record
     *
     * @return void
     */
    protected function assertErrorByBadPoint(IndicatorInterface $indicator, FormRecord $record)
    {
        // Indicator value.
        $indValue = $indicator->getValue();
        // And the record must have a new log message.
        $logRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $ulid = Ulid::fromString($record->getId());
        $logs = $logRepo->findBy(['recordId' => $ulid->toBinary()]);
        if (AbstractIndicator::VALUE_ERROR === $indValue) {
            $this->assertGreaterThanOrEqual(1, count($logs));
            // Point status must be digitizing.
            $point = $indicator->getPoint();
            $this->assertEquals('digitizing', $point->{'field_status'});
        }
    }
}
