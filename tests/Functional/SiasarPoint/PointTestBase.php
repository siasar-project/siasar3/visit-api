<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\SiasarPoint;

use App\Entity\AdministrativeDivision;
use App\Entity\CommunityService;
use App\Entity\Country;
use App\Entity\Currency;
use App\Entity\Ethnicity;
use App\Entity\File;
use App\Entity\FunctionsCarriedOutWsp;
use App\Entity\GeographicalScope;
use App\Entity\HouseholdProcess;
use App\Entity\InquiryFormLog;
use App\Entity\OfficialLanguage;
use App\Entity\PointLog;
use App\Entity\TechnicalAssistanceProvider;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeInterface;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Indicators\IndicatorInterface;
use App\Repository\InquiryFormLogRepository;
use App\Repository\PointLogRepository;
use App\Tests\Unit\Service\User\RoleTestBase;
use App\Tools\Json;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Siasar Point base test class.
 */
class PointTestBase extends RoleTestBase
{
    // Used to translate IDs.
    protected array $idTranslation = [];
    protected App $application;
    protected ManagerRegistry $managerRegistry;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->extraConfigurations = [
            'form.tap.contact' => dirname(__FILE__).'/../../assets/form.tap.contact.yml',
            'form.tap' => dirname(__FILE__).'/../../assets/form.tap.yml',
            'form.wsprovider.contact' => dirname(__FILE__).'/../../assets/form.wsprovider.contact.yml',
            'form.wsprovider.tap' => dirname(__FILE__).'/../../assets/form.wsprovider.tap.yml',
            'form.wsprovider' => dirname(__FILE__).'/../../assets/form.wsprovider.yml',
            'form.community.intervention' => dirname(__FILE__).'/../../assets/form.community.intervention.yml',
            'form.community.contact' => dirname(__FILE__).'/../../assets/form.community.contact.yml',
            'form.community' => dirname(__FILE__).'/../../assets/form.community.yml',
            'form.wssystem.communities' => dirname(__FILE__).'/../../assets/form.wssystem.communities.yml',
            'form.wssystem.contact' => dirname(__FILE__).'/../../assets/form.wssystem.contact.yml',
            'form.wssystem.intakeflow' => dirname(__FILE__).'/../../assets/form.wssystem.intakeflow.yml',
            'form.wssystem.projects' => dirname(__FILE__).'/../../assets/form.wssystem.projects.yml',
            'form.wssystem.qualitybacteriological' => dirname(__FILE__).'/../../assets/form.wssystem.qualitybacteriological.yml',
            'form.wssystem.qualityphysiochemical' => dirname(__FILE__).'/../../assets/form.wssystem.qualityphysiochemical.yml',
            'form.wssystem.residualtests' => dirname(__FILE__).'/../../assets/form.wssystem.residualtests.yml',
            'form.wssystem.sourceflow' => dirname(__FILE__).'/../../assets/form.wssystem.sourceflow.yml',
            'form.wssystem.sourceintake' => dirname(__FILE__).'/../../assets/form.wssystem.sourceintake.yml',
            'form.wssystem.transmisionline' => dirname(__FILE__).'/../../assets/form.wssystem.transmisionline.yml',
            'form.wssystem.treatmentpoints' => dirname(__FILE__).'/../../assets/form.wssystem.treatmentpoints.yml',
            'form.wssystem.storage' => dirname(__FILE__).'/../../assets/form.wssystem.storage.yml',
            'form.wssystem.distribution' => dirname(__FILE__).'/../../assets/form.wssystem.distribution.yml',
            'form.wssystem' => dirname(__FILE__).'/../../assets/form.wssystem.yml',
            'form.point' => dirname(__FILE__).'/../../assets/form.point.yml',
            'form.community.household' => dirname(__FILE__).'/../../assets/form.community.household.yml',
            'form.health.care.contact' => dirname(__FILE__).'/../../assets/form.health.care.contact.yml',
            'form.health.care' => dirname(__FILE__).'/../../assets/form.health.care.yml',
            'form.school.contact' => dirname(__FILE__).'/../../assets/form.school.contact.yml',
            'form.school' => dirname(__FILE__).'/../../assets/form.school.yml',
        ];
        parent::setUp();
        $kernel = self::$kernel;

        if (null === self::$client) {
            try {
                self::$client = $kernel->getContainer()->get('test.client');
            } catch (ServiceNotFoundException $e) {
                if (class_exists(KernelBrowser::class)) {
                    throw new \LogicException('You cannot create the client used in functional tests if the "framework.test" config is not set to true.');
                }
                throw new \LogicException('You cannot create the client used in functional tests if the BrowserKit component is not available. Try running "composer require symfony/browser-kit".');
            }

            self::$client->setServerParameters(
                [
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json',
                ]
            );
        }

        $this->managerRegistry = self::$container->get('doctrine');
        $this->application = new App($kernel);
        $this->formFactory = self::$container->get('form_factory');

        foreach ($this->extraConfigurations as $formId => $file) {
            $form = $this->formFactory->find($formId);
            $form->uninstall();
            $form->install();
        }

        $this->loginUser('pedro');
        $form = $this->formFactory->find('form.point');
        $form->uninstall();
        $form->install();

        $householdForm = $this->formFactory->find('form.community.household');
        $householdForm->uninstall();
        $householdForm->install();
    }

    /**
     * Create a new form record from file content.
     *
     * @param string $file
     * @param array  $replaceContext Default target IDs.
     *
     * @return FormRecord|null
     *
     * @throws \Exception
     */
    public function loadRecord(string $file, array $replaceContext = []): ?FormRecord
    {
        $json = Json::decode(file_get_contents($file));

        return $this->loadRecordJson($json, $replaceContext);
    }

    /**
     * Create a new form record from an array.
     *
     * @param array $json
     * @param array $replaceContext Default target IDs.
     *
     * @return FormRecord|null
     *
     * @throws \ReflectionException
     */
    public function loadRecordJson(array $json, array $replaceContext = []): ?FormRecord
    {
        $form = $this->formFactory->find($json["meta"]["form"]);
        $recordData = [];
        if ('form.point' === $json["meta"]["form"]) {
            $communitiesJson = $json["data"]["field_communities"];
            $recordData["field_communities"] = [];
            foreach ($communitiesJson as $communityJson) {
                $subJson = $json["subforms"][$communityJson['value']];
                $oldId = $subJson["data"]["id"];
                $id = $this->translateId($oldId);
                unset($subJson["data"]["id"]);
                if (empty($id)) {
                    $community = $this->loadRecordJson($json["subforms"][$communityJson['value']]);
                    $recordData["field_communities"][] = [
                        'value' => $community->getId(),
                        'form' => 'form.community',
                    ];
                    $this->addIdToTranslator($oldId, $community->getId());
                } else {
                    $recordData["field_communities"][] = [
                        'value' => $id,
                        'form' => 'form.community',
                    ];
                }
            }
            $wspsJson = $json["data"]["field_wsps"];
            $recordData["field_wsps"] = [];
            if ($wspsJson) {
                foreach ($wspsJson as $wspJson) {
                    $subJson = $json["subforms"][$wspJson['value']];
                    $oldId = $subJson["data"]["id"];
                    $id = $this->translateId($oldId);
                    unset($subJson["data"]["id"]);
                    unset($json["subforms"][$wspJson['value']]["data"]["field_point"]);
                    if (empty($id)) {
                        $wsp = $this->loadRecordJson($json["subforms"][$wspJson['value']]);
                        $recordData["field_wsps"][] = [
                            'value' => $wsp->getId(),
                            'form' => 'form.wsprovider',
                        ];
                        $this->addIdToTranslator($oldId, $wsp->getId());
                    } else {
                        $recordData["field_wsps"][] = [
                            'value' => $id,
                            'form' => 'form.wsprovider',
                        ];
                    }
                }
            }
        }
        $recordId = $form->insert($recordData);
        $this->addIdToTranslator($json["data"]['id'], $recordId);
        unset($json["data"]['id']);
        foreach ($json["data"] as $fieldName => &$fieldValue) {
            $field = $form->getFieldDefinition($fieldName);
            if ($field->isMultivalued()) {
                if (is_array($fieldValue)) {
                    foreach ($fieldValue as &$subvalue) {
                        $subvalue = $this->translateFieldValue($subvalue, $json, $replaceContext);
                    }
                }
            } else {
                $fieldValue = $this->translateFieldValue($fieldValue, $json, $replaceContext);
            }
        }
        // Update record.
        $record = $form->find($recordId);
        $recordData['field_ulid_reference'] = '';
        foreach ($json["data"] as $fieldName => &$fieldValue) {
            if (!isset($recordData[$fieldName])) {
                $record->{$fieldName} = $fieldValue;
            }
        }
        $record->getForm()->update($record, true);

        return $form->find($recordId);
    }

    /**
     * Add a new id entry.
     *
     * @param string $id
     * @param string $newId
     *
     * @return void
     */
    public function addIdToTranslator(string $id, string $newId)
    {
        $this->idTranslation[$id] = $newId;
    }

    /**
     * Translate an ID.
     *
     * @param string $id
     *
     * @return string
     *
     * @throws \Exception
     */
    public function translateId(string $id): string
    {
        if (isset($this->idTranslation[$id])) {
            return $this->idTranslation[$id];
        }

        return '';
    }

    /**
     * Translate field value.
     *
     * @param mixed $fieldValue
     * @param array $json           Data from file.
     * @param array $replaceContext Default target IDs.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function translateFieldValue(mixed $fieldValue, array $json, array $replaceContext = []): mixed
    {
        $defaultReplacements = [
            Country::class => 'hn',
            AdministrativeDivision::class => '01FFFC1K7EG1E2AZ4H2H5ZXQS3',
            User::class => '01FHX4RK866FRCDAYYYCZKVFXJ',
            File::class => '01GESDGFYWHQ621KATV3QH9BFH',
            Currency::class => '01FENEDYFA6KNAA4J459P5S0RF',
            TechnicalAssistanceProvider::class => '01FHWVW2GVTP8RRG1M3ZKS6H9A',
            OfficialLanguage::class => '01FH5AGXZR8H9E7EVKBA93D4W3',
            HouseholdProcess::class => '01G6WNZ83K89S4TCMCB1FD8MWZ',
            FunctionsCarriedOutWsp::class => '01G3BEJ0GVNCXRJH3RJSRJHW2V',
            GeographicalScope::class => '01G2MJ1AQ7WV10E96SF9SC3DMQ',
            Ethnicity::class => '01FENEDYFA6KNAA4J459P5S0RF',
            CommunityService::class => '01G3VA2YV4BHW5TDNJFK0VVHHX',
        ];
        $replaceContext = array_merge($defaultReplacements, $replaceContext);
        if (!is_array($fieldValue)) {
            return $fieldValue;
        }
        if (isset($fieldValue['class']) && !empty($fieldValue['value'])) {
            if (isset($replaceContext[$fieldValue['class']])) {
                $this->addIdToTranslator($fieldValue['value'], $replaceContext[$fieldValue['class']]);
                $fieldValue['value'] = $replaceContext[$fieldValue['class']];
            }
        }
        if (isset($fieldValue['form'])) {
            // Unset field_point of form.wsprovider
            if ('form.point' === $fieldValue['form']) {
                $fieldValue['value'] = '';
                $fieldValue['form'] = '';
            } else {
                if ('00000000000000000000000000' !== $fieldValue["value"]) {
                    $newId = $this->translateId($fieldValue["value"]);
                    if (!empty($newId)) {
                        $fieldValue['value'] = $newId;
                    } else {
                        $subrecord = $this->loadRecordJson($json['subforms'][$fieldValue["value"]]);
                        $this->addIdToTranslator($fieldValue["value"], $subrecord->getId());
                        $fieldValue['value'] = $subrecord->getId();
                    }
                }
            }
        }

        return $fieldValue;
    }

    /**
     * Request to endpoints of Point
     *
     * @param string             $method
     * @param KernelBrowser|null $session
     * @param string|null        $id
     * @param array              $payload
     *
     * @return FormRecord
     */
    protected function requestPoint(string $method, KernelBrowser $session = null, string $id = null, array $payload = []): FormRecord
    {
        if (null === $session) {
            $session = $this->loginRest('Pedro');
        }

        $uri = '/api/v1/form/data/form.points';
        $responseStatus = 201;
        if ('POST' !== $method) {
            $uri = '/api/v1/form/data/form.points/'.$id;
            $responseStatus = 200;
        }

        $session->request(
            $method,
            $uri,
            [],
            [],
            [],
            json_encode($payload)
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals($responseStatus, $response->getStatusCode(), print_r($data, true));

        $formPoint = $this->formFactory->find('form.point');
        $point = $formPoint->find($data['id']);

        return $point;
    }

    /**
     * Add WSP in a Point
     *
     * @param KernelBrowser|null $session
     * @param string             $pointId
     * @param array              $payload
     *
     * @return FormRecord
     */
    protected function createWSPinPoint(KernelBrowser $session = null, string $pointId, array $payload = []): FormRecord
    {
        if (null === $session) {
            $session = $this->loginRest('Pedro');
        }

        $session->request(
            'POST',
            '/api/v1/form/data/form.points/'.$pointId.'/form.wsprovider',
            [],
            [],
            [],
            json_encode($payload)
        );
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);

        $form = $this->formFactory->find('form.wsprovider');
        $wsp = $form->find($data['id']);

        return $wsp;
    }

    /**
     * Add WS System in a Point
     *
     * @param KernelBrowser|null $session
     * @param string             $pointId
     * @param array              $payload
     *
     * @return FormRecord
     */
    protected function createWSSystemInPoint(KernelBrowser $session = null, string $pointId, array $payload = []): FormRecord
    {
        if (null === $session) {
            $session = $this->loginRest('Pedro');
        }

        $session->request(
            'POST',
            '/api/v1/form/data/form.points/'.$pointId.'/form.wssystem',
            [],
            [],
            [],
            json_encode($payload)
        );
        $response = $session->getResponse();
        $this->assertEquals(201, $response->getStatusCode(), $response->getContent());
        $data = $this->getResponseArray($response);

        $form = $this->formFactory->find('form.wssystem');
        $system = $form->find($data['id']);

        return $system;
    }

    /**
     * Add community in a Point
     *
     * @param KernelBrowser|null $session
     * @param string             $pointId
     * @param array              $payload
     *
     * @return FormRecord|null
     */
    protected function createCommunityInPoint(KernelBrowser $session = null, string $pointId, array $payload = []): FormRecord|null
    {
        if (null === $session) {
            $session = $this->loginRest('Pedro');
        }

        $pointManager = $this->formFactory->find('form.point');
        $point = $pointManager->find($pointId);
        $systems = $point->{'field_wsystems'};
        if (count($systems) > 0) {
            $system = current($systems);
        } else {
            $system = $this->createWSSystemInPoint($session, $pointId, $payload);
        }
        // Find an administrative division not referenced in this point.
        /** @var AdministrativeDivision[] $referecedDivs */
        $referecedDivs = [];
        /** @var FormRecord $pointSystem */
        foreach ($point->{'field_wsystems'} as $pointSystem) {
            $references = $pointSystem->{'field_served_communities'};
            /** @var FormRecord $reference */
            foreach ($references as $reference) {
                /** @var FormRecord $refComm */
                $refComm = $reference->{'field_community'};
                if ($refComm) {
                    $referecedDivs[$refComm->getId()] = $refComm;
                }
            }
        }
        $divRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $divs = $divRepo->findBy([
            'country' => $point->{'field_country'},
            'level' => 4,
        ]);
        $newComm = null;
        foreach ($divs as $div) {
            if (!isset($referecedDivs[$div->getId()])) {
                $form = $this->formFactory->find('form.wssystem.communities');
                $sysCommId = $form->insert(
                    [
                        "field_community" => [
                            "value" => $div->getId(),
                            "class" => AdministrativeDivision::class,
                        ],
                        "field_provider" => [
                            "value" => $this->createWSPinPoint($session, $pointId)->getId(),
                            "form" => "form.wsprovider",
                        ],
                        'field_households' => 50,
                    ]
                );
                $newComm = $div;
                $refs = $system->{'field_served_communities'};
                $refs[] = [
                    "value" => $sysCommId,
                    "form" => "form.wssystem.communities",
                ];
                $system->{'field_served_communities'} = $refs;
                $system->save();
                if (count($systems) === 0) {
                    $refs = $point->{'field_wsystems'};
                    $refs[] = $system;
                    $point->save();
                }
                break;
            }
        }

        if ($newComm) {
            $pComms = $point->{'field_communities'};
            /** @var FormRecord[] $comms */
            foreach ($pComms as $comm) {
                /** @var AdministrativeDivision $cDiv */
                $cDiv = $comm->{'field_region'};
                if ($cDiv->getId() === $newComm->getId()) {
                    return $comm;
                }
            }
        }

        return null;
    }

    /**
     * Generate data for empty fields
     *
     * @param FormRecord $record
     */
    protected function updateRecordData(FormRecord &$record): void
    {
        /** @var FieldTypeInterface $field */
        foreach ($record->getFields() as $field) {
            $settings = $field->getSettings();
            // The isRequired() method can be true by settings but false by other contexts.
            if ($settings["settings"]["required"] && $field->isEmpty()) {
                if ($field->isMultivalued()) {
                    $record->{$field->getId()} = [
                        $field->getExampleData(),
                        $field->getExampleData(),
                    ];
                } else {
                    $record->set($field->getId(), $field->getExampleData());
                }
            }
        }
    }

    /**
     * Generate data for empty fields
     *
     * @param FormManagerInterface $form
     *
     * @return array
     */
    protected function generateRecordData(FormManagerInterface $form): array
    {
        $resp = [];
        /** @var FieldTypeInterface $field */
        foreach ($form->getFields() as $field) {
            $settings = $field->getSettings();
            // The isRequired() method can be true by settings but false by other contexts.
            if ($settings["settings"]["required"]) {
                if ($field->isMultivalued()) {
                    $resp[$field->getId()] = [
                        $field->getExampleData(),
                        $field->getExampleData(),
                    ];
                } else {
                    $resp[$field->getId()] = $field->getExampleData();
                }
            }
        }

        return $resp;
    }

    /**
     * Get inquiry log messages.
     *
     * @param FormRecord $inquiry
     *
     * @return string
     */
    protected function getInquiryLogMessages(FormRecord $inquiry): string
    {
        $resp = '';
        /** @var InquiryFormLogRepository $logsRepo */
        $logsRepo = $this->entityManager->getRepository(InquiryFormLog::class);
        $logs = $logsRepo->findBy(['recordId' => $inquiry->getId(), 'level' => 400]);
        foreach ($logs as $log) {
            $resp .= "\n".$log->getMessage();
        }

        return $resp;
    }

    /**
     * Get point log messages.
     *
     * @param FormRecord $point
     *
     * @return string
     */
    protected function getPointLogMessages(FormRecord $point): string
    {
        $resp = '';
        /** @var PointLogRepository $logsRepo */
        $logsRepo = $this->entityManager->getRepository(PointLog::class);
        $logs = $logsRepo->findBy(['point' => $point->getId()]);
        foreach ($logs as $log) {
            $resp .= "\n".$log->getMessage();
        }

        return $resp;
    }

    /**
     * Format indicator instances array to name/value array.
     *
     * @param IndicatorInterface $indicator
     *
     * @return array
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function formatIndicatorsList(IndicatorInterface $indicator): array
    {
        $resp = [
            'raw' => [],
            'rnd' => [],
        ];
        $indicatorValues = $indicator->getAllValues();

        foreach ($indicatorValues as $indicatorValue) {
            // Raw indicator result. Used to debug.
            $resp['raw'][$indicatorValue->getName()] = $indicatorValue->getValue();
            // Rounded values to compare with documentation.
            $resp['rnd'][$indicatorValue->getName()] = round($indicatorValue->getValue(), 2);
        }

        return $resp;
    }
}
