<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional;

use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Test base class.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class TestBase extends WebTestCase
{
    use TestFixturesTrait;

    protected static ?KernelBrowser $client = null;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        if (null === self::$client) {
            self::$client = static::createClient();
            self::$client->setServerParameters(
                [
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json',
                ]
            );
        }
        $_ENV['TESTS_LOADING_FIXTURES'] = true;
        $this->loadFixtures();
        $_ENV['TESTS_LOADING_FIXTURES'] = false;
    }

    /**
     * Parse response.
     *
     * @param Response $response Raw response.
     *
     * @return array
     */
    protected function getResponseData(Response $response): array
    {
        return \json_decode($response->getContent(), true);
    }

    /**
     * Init data base connection.
     *
     * @return Connection
     */
    protected function initDbConnection(): Connection
    {
        return $this->getContainerInstance()->get('doctrine')->getConnection();
    }
}
