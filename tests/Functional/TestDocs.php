<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Test docs access.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class TestDocs extends TestBase
{
    /**
     * Test resend activation email without auto activation.
     *
     * @return void
     */
    public function testGetDocsPage(): void
    {
        self::$client->setServerParameters(
            [
                'CONTENT_TYPE' => 'text/html',
                'HTTP_ACCEPT' => 'text/html',
            ]
        );

        self::$client->request(
            'GET',
            '/api/v1/docs',
            [],
            [],
            [],
            null
        );

        $response = self::$client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('text/html; charset=UTF-8', $response->headers->get('content-type'));
    }
}
