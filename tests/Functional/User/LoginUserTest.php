<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Test user login.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LoginUserTest extends UserTestBase
{

    protected ?UserRepository $userRepository;
    protected EntityManager|null $entityManager;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::getContainerInstance()->get('doctrine')->getManager();
        $this->userRepository = $this->entityManager->getRepository(User::class);
    }

    /**
     * Test not active user login.
     *
     * @return void
     */
    public function testNotActiveUserLogin(): void
    {
        $payload = [
            'username' => 'Marco',
            'password' => '123456',
        ];

        self::$client->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$client->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(JsonResponse::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * Test not registered user login.
     *
     * @return void
     */
    public function testNotRegisteredUserLogin(): void
    {
        $payload = [
            'username' => 'Intruso',
            'password' => '123456',
        ];

        self::$client->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$client->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(JsonResponse::HTTP_UNAUTHORIZED, $response->getStatusCode());
    }

    /**
     * Test active user login.
     *
     * @return void
     */
    public function testActiveUserLogin(): void
    {
        $payload = [
            'username' => 'Pedro',
            'password' => '123456',
        ];

        self::$client->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$client->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertIsString($responseData['token']);
        $this->assertIsString($responseData["refresh_token"]);
    }

    /**
     * Test active user refresh.
     *
     * @return void
     */
    public function testActiveUserRefresh(): void
    {
        $payload = [
            'username' => 'Pedro',
            'password' => '123456',
        ];

        self::$client->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$client->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertIsString($responseData['token']);
        $this->assertIsString($responseData["refresh_token"]);
        $this->assertIsInt($responseData["refresh_token_expire"]);
        $this->assertIsInt($responseData["token_expire"]);

        $payload = [
            'refresh_token' => $responseData["refresh_token"],
        ];
        self::$client->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));
        $response = self::$client->getResponse();
        $responseData = $this->getResponseData($response);
        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
        $this->assertIsString($responseData['token']);
        $this->assertIsString($responseData["refresh_token"]);
        $this->assertIsInt($responseData["refresh_token_expire"]);
        $this->assertIsInt($responseData["token_expire"]);
    }

    /**
     * Test active user get user data.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testActiveUserGetUserData(): void
    {
        // First we need get the user from database to know the user ID.
        /**
         * The user to query.
         *
         * @var User $user
         */
        $user = $this->userRepository->findOneByEmail('pedro@front.id');
        // Then query to the API.
        self::$admin->request('GET', \sprintf('%s/%s', $this->endpoint, $user->getId()), [], [], [], null);
        $response = self::$admin->getResponse();
        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Test that we can get own user ID.
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function testUsersMeResource()
    {
        $pedro = $this->userRepository->findOneByUserName('Pedro');

        parent::$admin->request('GET', $this->endpoint.'/me');
        $response = parent::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $somebody = $this->userRepository->find($responseData['id']);

        $this->assertEquals($pedro->getEmail(), $somebody->getEmail());
        $this->assertEquals($pedro->getId(), $somebody->getId());
        $this->assertEquals(200, $response->getStatusCode());
    }
}
