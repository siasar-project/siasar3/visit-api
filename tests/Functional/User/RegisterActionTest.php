<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Tests\Functional\User\UserTestBase;

/**
 * Test user registration.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class RegisterActionTest extends UserTestBase
{
    /**
     * Test register user.
     *
     * @return void
     */
    public function testRegister(): void
    {
        $payload = [
            'username' => 'Stewie',
            'email' => 'stewie@api.com',
            'password' => '123456',
            'country' => '/api/v1/countries/es',
            'timezone' => 'UTC',
        ];

        self::$admin->request('POST', \sprintf('%s', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$admin->getResponse();
        $responseData = $this->getResponseData($response);

        $this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
        $this->assertEquals($payload['email'], $responseData['email']);
        $this->assertEquals($payload['timezone'], $responseData['timezone']);

        /** @var UserRepository $users */
        $users = $this->entityManager->getRepository(User::class);
        $user = $users->findOneByUserName('Stewie');
        $this->assertEquals('stewie@api.com', $user->getEmail());
        $this->assertEquals('UTC', $user->getTimezone());
    }

    /**
     * Test register user with missing parameters.
     *
     * @return void
     */
    public function testRegisterWithMissingParameters(): void
    {
        $payload = [
            'username' => 'Stewie',
            'password' => '123456',
        ];

        self::$admin->request('POST', \sprintf('%s', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$admin->getResponse();

        $this->assertEquals(JsonResponse::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    /**
     * Test register user with invalid password.
     *
     * @return void
     */
    public function testRegisterWithInvalidPassword(): void
    {
        $payload = [
            'name' => 'Stewie',
            'email' => 'stewie@api.com',
            'password' => '1',
        ];

        self::$admin->request('POST', \sprintf('%s', $this->endpoint), [], [], [], \json_encode($payload));

        $response = self::$admin->getResponse();

        $this->assertEquals(JsonResponse::HTTP_BAD_REQUEST, $response->getStatusCode());
    }
}
