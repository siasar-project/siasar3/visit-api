<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\User;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Test base class.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ResendActivationEmailActionTest extends UserTestBase
{
    /**
     * Test resend activation email without auto activation.
     *
     * @return void
     */
    public function testResendActivationEmailWithoutAutoActivation(): void
    {
        $payload = ['email' => 'marco@front.id'];

        $_ENV['APP_USER_AUTO_ACTIVATION'] = 0;
        self::$client->request(
            'POST',
            \sprintf('%s/resend_activation_email', $this->endpoint),
            [],
            [],
            [],
            \json_encode($payload)
        );

        $response = self::$client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
    }

    /**
     * Test resend activation email without auto activation to active user.
     *
     * @return void
     */
    public function testResendActivationEmailToActiveUser(): void
    {
        $payload = ['email' => 'pedro@front.id'];

        $_ENV['APP_USER_AUTO_ACTIVATION'] = 0;
        self::$client->request(
            'POST',
            \sprintf('%s/resend_activation_email', $this->endpoint),
            [],
            [],
            [],
            \json_encode($payload)
        );

        $response = self::$client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }

    /**
     * Test resend activation email with auto activation.
     *
     * @return void
     */
    public function testResendActivationEmailWithAutoActivation(): void
    {
        $payload = ['email' => 'marco@front.id'];

        $_ENV['APP_USER_AUTO_ACTIVATION'] = 1;
        self::$client->request(
            'POST',
            \sprintf('%s/resend_activation_email', $this->endpoint),
            [],
            [],
            [],
            \json_encode($payload)
        );

        $response = self::$client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
    }
}
