<?php
/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Jose Norberto Sanchez <josensanchez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\User;

use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Entity\Material;
use App\Repository\MaterialRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use App\EventListener\MaterialListener;

/**
 * test Users endpoint
 */
class UserResourceTest extends ApiTestBase
{
    protected $payload = [
        "country" => "/api/v1/countries/hn",
        'username' => 'Stewie',
        'email' => 'stewie@api.com',
        'password' => '123456',
    ];

    /**
     * testing listing
     */
    public function testUserListing()
    {
        self::$admin->request('GET', '/api/v1/users');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     * testing listing
     */
    public function testUserUserListing()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/users');
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('Unable to retrieve User: The "read" action require the permission "read doctrine user".', $data['detail']);
    }

    /**
     * testing listing
     */
    public function testUserAdminLocalListing()
    {
        $session = $this->loginRest('AdminLocal');
        $session->request('GET', '/api/v1/users');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseArray($response);
        foreach ($data as $aUser) {
            $this->assertNotEmpty($aUser["id"]);
            $this->assertNotEmpty($aUser["username"]);
        }
    }

    /**
     * testing sort listing.
     */
    public function testUserSortListing()
    {
        $query = [
            'page' => 1,
            'order' => [
                'username' => 'asc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/users', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) > 0);
        $firstData = null;
        foreach ($data as $admDiv) {
            if (!$firstData) {
                $firstData = $admDiv;
            }
        }

        $query = [
            'page' => 1,
            'order' => [
                'username' => 'desc',
            ],
        ];
        self::$admin->request('GET', '/api/v1/users', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        foreach ($data as $admDiv) {
            $this->assertNotEquals($firstData['id'], $admDiv['id']);
            break;
        }
    }

    /**
     * testing paginated listing.
     */
    public function testUserPaginatedListing()
    {
        $query = [
            'page' => 9999,
        ];
        self::$admin->request('GET', '/api/v1/users', $query);
        $response = self::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $data = $this->getResponseData($response);
        $this->assertTrue(count($data) === 0);
    }

    /**
     * testing creation
     */
    public function testUserPosting()
    {
        self::$admin->request('POST', '/api/v1/users', [], [], [], \json_encode($this->payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
    }

    /**
     * testing creation
     */
    public function testUserUserPosting()
    {
        $session = $this->loginRest('Marta');
        $session->request('POST', '/api/v1/users', [], [], [], \json_encode($this->payload));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testUserShowing()
    {
        self::$admin->request('GET', '/api/v1/users/01FHX4RK866FRCDAYYYCZKVFXJ');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Pedro', $data['username']);
    }

    /**
     * testing detection
     */
    public function testUserShowingOther()
    {
        self::$admin->request('GET', '/api/v1/users/01FHX4RRD9KGQ45YZDP001CZ07');
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Carlos', $data['username']);
    }

    /**
     * testing detection
     */
    public function testUserUserShowing()
    {
        $session = $this->loginRest('Marta');
        // User 'Pedro'.
        $session->request('GET', '/api/v1/users/01FHX4RK866FRCDAYYYCZKVFXJ');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing detection
     */
    public function testUserUserShowingMe()
    {
        $session = $this->loginRest('Marta');
        $session->request('GET', '/api/v1/users/01FHX4S6GB2Y7FW4N357DRYKT6');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Marta', $data['username']);
    }

    /**
     * testing Local admins only can see local users.
     */
    public function testUserAdminLocalShowing()
    {
        $session = $this->loginRest('AdminLocal');
        // User 'Digitizer' in same country.
        $session->request('GET', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E');
        $response = $session->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Digitizer', $data['username']);
    }

    /**
     * testing Local admins only can see local users.
     */
    public function testUserAdminLocalShowingOtherCountry()
    {
        $session = $this->loginRest('AdminLocal');
        // User 'SectorialValidatorEs' in other country.
        $session->request('GET', '/api/v1/users/01FHX4TQZZMDRX7RYBQ6VVV4W2');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing duplicate username.
     */
    public function testUserPuttingDuplicateUsername()
    {
        $payload = [];
        $payload["username"] = "Pedro";
        // Update username.
        self::$admin->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * testing duplicate email.
     */
    public function testUserPuttingDuplicateEmail()
    {
        $payload = [];
        $payload["email"] = "pedro@front.id";
        // Update username.
        self::$admin->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(422, $response->getStatusCode());
    }

    /**
     * testing edition
     */
    public function testUserPutting()
    {
        $payload = [];
        $payload["username"] = "Digitizer 1";
        // Update username.
        self::$admin->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $data = $this->getResponseData($response);
        $this->assertEquals('Digitizer 1', $data["username"]);
    }

    /**
     * testing add administrative division.
     */
    public function testUserPuttingAddDivision()
    {
        $payload = [];
        // Read user "Digitizer".
        self::$admin->request('GET', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E');
        $response = parent::$admin->getResponse();
        $data = $this->getResponseData($response);
        $payload["administrativeDivisions"] = [];
        foreach ($data["administrativeDivisions"] as $admDivision) {
            $payload["administrativeDivisions"][] = '/api/v1/administrative_divisions/'.$admDivision["id"];
        }
        $payload["administrativeDivisions"][] = '/api/v1/administrative_divisions/01FFFBCZ1KJET70X2MGYYB3RJ0';
        $payload["administrativeDivisions"][] = '/api/v1/administrative_divisions/01FENEDYFA6KNAA4J459P5S0RG';
        // Update administrativeDivision.
        self::$admin->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        // Read again to validate.
        self::$admin->request('GET', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E');
        $response = parent::$admin->getResponse();
        $data = $this->getResponseData($response);
        $divisions = [
            '01FENEDYFA6KNAA4J459P5S0RF',
            '01FFFBCZ1KJET70X2MGYYB3RJ0',
            '01FENEDYFA6KNAA4J459P5S0RG',
        ];
        $this->assertCount(count($divisions), $data["administrativeDivisions"]);
        foreach ($data["administrativeDivisions"] as $division) {
            $this->assertTrue(in_array($division["id"], $divisions));
        }
    }

    /**
     * testing add language.
     */
    public function testUserPuttingAddLanguage()
    {
        $payload = [
            'language' => '/api/v1/languages/SPAN',
        ];
        self::$admin->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        // Read again to validate.
        self::$admin->request('GET', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E');
        $response = parent::$admin->getResponse();
        $data = $this->getResponseData($response);
        $this->assertEquals('/api/v1/languages/SPAN', $data['language']);
    }

    /**
     * testing add country.
     */
    public function testUserPuttingAddCountry()
    {
        $payload = [
            'country' => '/api/v1/countries/es',
        ];
        self::$admin->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals("You can't update the country of other users.", $data['detail']);
    }

    /**
     * testing update own country.
     */
    public function testUserPuttingUpdateCountry()
    {
        $payload = [
            'country' => '/api/v1/countries/es',
        ];
        self::$admin->request('PUT', '/api/v1/users/01FHX4RK866FRCDAYYYCZKVFXJ', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        // Read again to validate.
        self::$admin->request('GET', '/api/v1/users/01FHX4RK866FRCDAYYYCZKVFXJ');
        $response = parent::$admin->getResponse();
        $data = $this->getResponseData($response);
        $this->assertEquals('es', $data['country']['code']);
    }

    /**
     * testing update without division.
     */
    public function testUserPuttingUpdateWithoutDivision()
    {
        $payload = [
            'administrativeDivisions' => [],
        ];
        self::$admin->request('PUT', '/api/v1/users/01FHX4RK866FRCDAYYYCZKVFXJ', [], [], [], \json_encode($payload));
        $response = parent::$admin->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        // Read again to validate.
        self::$admin->request('GET', '/api/v1/users/01FHX4RK866FRCDAYYYCZKVFXJ');
        $response = parent::$admin->getResponse();
        $data = $this->getResponseData($response);
        $this->assertCount(0, $data['administrativeDivisions']);
    }

    /**
     * testing edition
     */
    public function testUserUserPutting()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E', [], [], [], \json_encode(["country" => "/api/v1/countries/hn"]));
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    /**
     * testing update own country without permissions
     */
    public function testUserUserPuttingUpdateCountry()
    {
        $session = $this->loginRest('Marta');
        $session->request('PUT', '/api/v1/users/01FHX4S6GB2Y7FW4N357DRYKT6', [], [], [], \json_encode(["country" => "/api/v1/countries/es"]));
        $response = $session->getResponse();
        $data = $this->getResponseArray($response);
        $this->assertEquals(500, $response->getStatusCode());
        $this->assertEquals('To update own Country requires the permission "all countries".', $data['detail']);
    }

    /**
     * testing deletion
     */
    public function testUserDeleting()
    {
        // Delete user "Digitizer".
        self::$admin->request('DELETE', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E');
        $response = parent::$admin->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * Testing deletion
     */
    public function testUserUserDeleting()
    {
        // Delete user "Digitizer".
        $session = $this->loginRest('Marta');
        $session->request('DELETE', '/api/v1/users/01FHX4TYSQTY378JVN201G2Y9E');
        $response = $session->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }
}
