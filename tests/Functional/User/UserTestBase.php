<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Functional\User;

use App\Repository\ConfigurationRepository;
use App\Tests\Functional\TestBase;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\InitTestEnvironmentTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * Test user base class.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserTestBase extends TestBase
{
    use TestFixturesTrait;
    use InitTestEnvironmentTrait;

    protected string $endpoint;
    protected static ?KernelBrowser $admin = null;
    protected ?ConfigurationRepository $configuration;
    protected ?EntityManager $entityManager;

    /**
     * Setup tests.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->entityManager = $this->getContainerInstance()->get('doctrine')->getManager();
        $this->entityManager->clear();
        $this->doctrine = $this->getContainerInstance()->get('database_connection');
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->initConfigurations($this->configuration);

        $this->endpoint = '/api/v1/users';

        if (null === self::$admin) {
            self::$admin = clone self::$client;
            $payload = [
                'username' => 'Pedro',
                'password' => '123456',
            ];

            self::$admin->request('POST', \sprintf('%s/login', $this->endpoint), [], [], [], \json_encode($payload));

            $response = self::$admin->getResponse();
            $responseData = $this->getResponseData($response);

            self::$admin->setServerParameters(
                [
                    'HTTP_AUTHORIZATION' => 'Bearer '.$responseData['token'],
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json',
                ]
            );
        }
    }
}
