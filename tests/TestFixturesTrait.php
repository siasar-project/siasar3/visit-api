<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests;

use App\Service\FileSystem;
use App\Traits\GetContainerTrait;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\EntityManager;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;

/**
 * Test fixtures tool.
 */
trait TestFixturesTrait
{
    use GetContainerTrait;

    protected AbstractDatabaseTool $databaseTool;
    protected ?EntityManager $tsEntityManager;
    protected ?Registry $doctrineRegistry;
    protected ?AbstractSchemaManager $schemaManager;
    protected ?Connection $connection;

    /**
     * Load defined test fixtures.
     *
     * @throws \Exception
     */
    protected function loadFixtures(): void
    {
        $_ENV['TESTS_LOADING_FIXTURES'] = true;
        // Clear database.
        $this->cleanDatabase();
        // Load fixtures.
        $this->databaseTool = static::getContainerInstance()->get(DatabaseToolCollection::class)->get();
        $fixtures = FileSystem::systemGetFilesAndFolders(self::getKernelInstance()->getProjectDir().'/src/TestFixtures');
        $this->databaseTool->loadAliceFixture($fixtures["files"], true);
        $_ENV['TESTS_LOADING_FIXTURES'] = false;
    }

    /**
     * Clean database.
     */
    protected function cleanDatabase(): void
    {
        $this->truncateEntities();
        // $this->removeForms();
    }

    /**
     * Remove all doctrine entities in database.
     *
     * @see https://symfonycasts.com/screencast/phpunit/control-database
     * @see https://stackoverflow.com/a/22814107
     */
    protected function truncateEntities()
    {
        $connection = $this->getEntityManager()->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        $this->useCheckForeignKeys(false);
        $metas = $this->getEntityManager()->getMetadataFactory()->getAllMetadata();
        foreach ($metas as $meta) {
            $query = $databasePlatform->getTruncateTableSQL(
                $this->getEntityManager()->getClassMetadata($meta->getName())->getTableName()
            );
            $connection->executeUpdate($query);
        }
        $this->useCheckForeignKeys(true);
        $this->getEntityManager()->clear();
    }

    /**
     * Stop or not to use foreign keys.
     *
     * @param bool $use
     *
     * @throws \Doctrine\DBAL\Exception
     */
    protected function useCheckForeignKeys(bool $use): void
    {
        $connection = $this->getEntityManager()->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        if ($databasePlatform->supportsForeignKeyConstraints()) {
            $connection->query('SET FOREIGN_KEY_CHECKS='.($use ? '1' : '0'));
        }
    }

    /**
     * Drop all forms tables.
     *
     * @throws \Doctrine\DBAL\Exception
     */
    protected function removeForms(): void
    {
        $connection = $this->getEntityManager()->getConnection();
        $databasePlatform = $connection->getDatabasePlatform();
        $schemaManager = $this->getSchemaManager();
        $tables = $schemaManager->listTables();
        /** @var Table $table */
        foreach ($tables as $table) {
            if (str_starts_with($table->getName(), 'form_')) {
                $schemaManager->dropTable($table->getName());
            }
        }
    }

    /**
     * Get ORM entity manager.
     *
     * @return EntityManager|null
     */
    protected function getEntityManager(): ?EntityManager
    {
        if (!isset($this->tsentityManager) || !$this->tsentityManager) {
            $this->tsentityManager = $this->getDoctrine()->getManager();
        }

        return $this->tsentityManager;
    }

    /**
     * Get doctrine service.
     *
     * @return Registry
     */
    protected function getDoctrine(): Registry
    {
        if (!isset($this->doctrineRegistry) || !$this->doctrineRegistry) {
            $this->doctrineRegistry = static::getContainerInstance()->get('doctrine');
        }

        return $this->doctrineRegistry;
    }

    /**
     * Get connection servioce.
     *
     * @return Connection
     */
    protected function getConnection(): Connection
    {
        if (!isset($this->connection) || !$this->connection) {
            $this->connection = static::getContainerInstance()->get('database_connection');
        }

        return $this->connection;
    }

    /**
     * Get schema manager service.
     *
     * @return AbstractSchemaManager|null
     */
    protected function getSchemaManager(): ?AbstractSchemaManager
    {
        if (!isset($this->schemaManager) || !$this->schemaManager) {
            $this->schemaManager = $this->getConnection()->getSchemaManager();
        }

        return $this->schemaManager;
    }
}
