<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Entity\AdministrativeDivision;
use App\Forms\ChangedFormManager;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Test forms records.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class CloneRecordTest extends RoleTestBase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected FormManagerInterface|null $form;
    protected array $data;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can clone a FormRecord.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCloneBasicFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.clone.basic.record');
        $form->addField('integer', 'field1', 'Field 1');
        $form->addField('boolean', 'field2', 'Field 2');
        $form->addField('short_text', 'field3', 'Field 3');
        $form->addField('long_text', 'field4', 'Field 4');
        $form->saveNow();
        $form->install();

        $rid = $form->insert(
            [
                'field_field1' => 10,
                'field_field2' => true,
                'field_field3' => 'Un hombre de la virgen estuvo aquí',
                'field_field4' => "Linea 1\nLinea 2\nLinea 3",
            ]
        );
        // Clone the record.
        $cRid = $form->clone($rid);
        // Load the record.
        $clone = $form->find($cRid);
        // Prepare a validation array.
        $fields = [
            'id' => true,
            'field_field1' => true,
            'field_field2' => true,
            'field_field3' => true,
            'field_field4' => true,
        ];
        // Verify that we can get all fields.
        $fieldCount = 0;
        foreach ($clone as $fieldName => $value) {
            ++$fieldCount;
            // Is it a record field?
            $this->assertArrayHasKey($fieldName, $fields);
        }
        // See we all field?
        $this->assertEquals(5, $fieldCount);
        // Validate that all fields were copied.
        $record = $form->find($rid);
        foreach ($record as $fieldName => $value) {
            if ('id' !== $fieldName) {
                $this->assertEquals($record->{$fieldName}, $clone->{$fieldName});
            } else {
                $this->assertNotEquals($record->getId(), $clone->getId());
            }
        }
    }

    /**
     * Test that we can clone a FormRecord from a record.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCloneBasicFormRecordByRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.clone.by.record');
        $form->addField('integer', 'field1', 'Field 1');
        $form->addField('boolean', 'field2', 'Field 2');
        $form->addField('short_text', 'field3', 'Field 3');
        $form->addField('long_text', 'field4', 'Field 4');
        $form->saveNow();
        $form->install();

        $rid = $form->insert(
            [
                'field_field1' => 10,
                'field_field2' => true,
                'field_field3' => 'Un hombre de la virgen estuvo aquí',
                'field_field4' => "Linea 1\nLinea 2\nLinea 3",
            ]
        );
        // Clone the record.
        $record = $form->find($rid);
        // Load the record.
        $clone = $record->clone();
        // Prepare a validation array.
        $fields = [
            'id' => true,
            'field_field1' => true,
            'field_field2' => true,
            'field_field3' => true,
            'field_field4' => true,
        ];
        // Verify that we can get all fields.
        $fieldCount = 0;
        foreach ($clone as $fieldName => $value) {
            ++$fieldCount;
            // Is it a record field?
            $this->assertArrayHasKey($fieldName, $fields);
        }
        // See we all field?
        $this->assertEquals(5, $fieldCount);
        // Validate that all fields were copied.
        foreach ($record as $fieldName => $value) {
            if ('id' !== $fieldName) {
                $this->assertEquals($record->{$fieldName}, $clone->{$fieldName});
            } else {
                $this->assertNotEquals($record->getId(), $clone->getId());
            }
        }
    }

    /**
     * Test that we can clone a ChangedFormRecord from a record.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCloneChangedFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.clone.changed.record', ChangedFormManager::class);
        $form->addField('integer', 'field1', 'Field 1');
        $form->addField('boolean', 'field2', 'Field 2');
        $form->addField('short_text', 'field3', 'Field 3');
        $form->addField('long_text', 'field4', 'Field 4');
        $form->saveNow();
        $form->install();

        $rid = $form->insert(
            [
                'field_field1' => 10,
                'field_field2' => true,
                'field_field3' => 'Un hombre de la virgen estuvo aquí',
                'field_field4' => "Linea 1\nLinea 2\nLinea 3",
            ]
        );
        // Clone the record.
        $record = $form->find($rid);
        // Sleep a second because we have date time fields that don't must be copied.
        sleep(1);
        // Load the record.
        $clone = $record->clone();
        // Prepare a validation array.
        $fields = [
            'id' => true,
            'field_field1' => true,
            'field_field2' => true,
            'field_field3' => true,
            'field_field4' => true,
            'field_created' => true,
            'field_changed' => true,
        ];
        // Verify that we can get all fields.
        $fieldCount = 0;
        foreach ($clone as $fieldName => $value) {
            ++$fieldCount;
            // Is it a record field?
            $this->assertArrayHasKey($fieldName, $fields);
        }
        // See we all field?
        $this->assertEquals(7, $fieldCount);
        // Validate that all data fields were copied.
        $notClonedFields = ['id', 'field_created', 'field_changed'];
        foreach ($record as $fieldName => $value) {
            if (!in_array($fieldName, $notClonedFields)) {
                $this->assertEquals($record->{$fieldName}, $clone->{$fieldName});
            } else {
                $this->assertNotEquals($record->{$fieldName}, $clone->{$fieldName});
            }
        }
    }

    /**
     * Test that we can clone a InquiryFormRecord from a record.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCloneInquiryFormRecord(): void
    {
        $this->loginUser('Pedro');
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.clone.inquiry.record', InquiryFormManager::class);
        $form->addField('integer', 'field1', 'Field 1');
        $form->addField('boolean', 'field2', 'Field 2');
        $form->addField('short_text', 'field3', 'Field 3');
        $form->addField('long_text', 'field4', 'Field 4');
        $form->saveNow();
        $form->install();

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');

        $rid = $form->insert(
            [
                'field_field1' => 10,
                'field_field2' => true,
                'field_field3' => 'Un hombre de la virgen estuvo aquí',
                'field_field4' => "Linea 1\nLinea 2\nLinea 3",
                'field_region' => $division,
            ]
        );
        // Clone the record.
        $record = $form->find($rid);
        $record->{'field_status'} = 'finished';
        $record->save();
        $record = $form->find($rid);
        $record->{'field_status'} = 'validated';
        $record->save();
        // Sleep a second because we have date time fields that don't must be copied.
        sleep(1);
        // Change user.
        $this->loginUser('SectorialGlobal');
        // Load the record.
        $clone = $record->clone();
        // Prepare a validation array.
        $fields = [
            'id' => true,
            'field_field1' => true,
            'field_field2' => true,
            'field_field3' => true,
            'field_field4' => true,
            'field_created' => true,
            'field_changed' => true,
            'field_country' => true,
            'field_region' => true,
            'field_creator' => true,
            'field_editors' => true,
            'field_editors_update' => true,
            'field_reference' => true,
            'field_ulid_reference' => true,
            'field_validated' => true,
            'field_deleted' => true,
            'field_status' => true,
        ];
        // Verify that we can get all fields.
        $fieldCount = 0;
        foreach ($clone as $fieldName => $value) {
            ++$fieldCount;
            // Is it a record field?
            $this->assertArrayHasKey($fieldName, $fields);
        }
        // See we all field?
        $this->assertEquals(17, $fieldCount);
        // Validate that all data fields were copied.
        $notClonedFields = [
            'id',
            'field_created',
            'field_changed',
            'field_creator',
            'field_editors',
            'field_editors_update',
            'field_status',
            'field_validated',
        ];
        foreach ($record as $fieldName => $value) {
            if (!in_array($fieldName, $notClonedFields)) {
                $this->assertEquals($record->{$fieldName}, $clone->{$fieldName}, sprintf('Field: %s', $fieldName));
            } else {
                $this->assertNotEquals($record->{$fieldName}, $clone->{$fieldName}, sprintf('Field: %s', $fieldName));
            }
        }
    }

    /**
     * Test that we can clone a from record with multivalued fields.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCloneFormRecordWithMultivalued(): void
    {
        $this->loginUser('Pedro');
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.clone2.inquiry.record', InquiryFormManager::class);
        $form->addField('integer', 'field1', 'Field 1');
        $form->addField('boolean', 'field2', 'Field 2', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Add new record.
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');
        $rId = $form->insert([
            'field_field1' => 1,
            'field_field2' => [
                ['value' => true],
                ['value' => false],
                ['value' => true],
            ],
            'field_region' => $division,
        ]);
        $record = $form->find($rId);
        $record->{'field_status'} = 'finished';
        $record->save();
        $record = $form->find($rId);
        $record->{'field_status'} = 'validated';
        $record->save();
        $record = $form->find($rId);

        // Clone it.
        $clone = $record->clone();

        // Validate fields.
        $this->assertEquals($record->{'field_field1'}, $clone->{'field_field1'});
        $this->assertEquals($record->{'field_field2'}, $clone->{'field_field2'});
    }

    /**
     * Test that we can clone a from record with subform fields.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCloneFormRecordWithSubform(): void
    {
        $this->loginUser('Pedro');
        // Prepare a form with a record.
        $subForm = $this->formFactory->create('test.clone.subform', SubformFormManager::class);
        $subForm->setMeta(['parent_form' => 'form.test.clone.form']);
        $subForm->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $subForm->addField('integer', 'single', 'Single field');
        $subForm->saveNow();
        $subForm->install();

        $form = $this->formFactory->create('test.clone.form', InquiryFormManager::class);
        $form->addField(
            'subform_reference',
            'multi',
            'multi',
            [
                'multivalued' => true,
                'subform' => 'form.test.clone.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->addField(
            'subform_reference',
            'single',
            'single',
            [
                'subform' => 'form.test.clone.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();

        // Create a new record.
        $subForm = $this->formFactory->find('test.clone.subform');
        // Create three new subform records.
        $subRecordIds = [
            'id1' => $subForm->insert(
                [
                    'field_single' => 1,
                    'field_reference' => [1, 2, 3, 4, 5],
                ]
            ),
            'id2' => $subForm->insert(
                [
                    'field_single' => 1,
                    'field_reference' => [1, 2, 3, 4, 5],
                ]
            ),
            'id3' => $subForm->insert(
                [
                    'field_single' => 1,
                    'field_reference' => [1, 2, 3, 4, 5],
                ]
            ),
        ];
        $form = $this->formFactory->find('test.clone.form');
        // Create a new record using subform records.
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');
        $recordId = $form->insert(
            [
                'field_single' => [
                    'value' => $subRecordIds['id1'],
                    'form' => 'form.test.clone.subform',
                ],
                'field_multi' => [
                    [
                        'value' => $subRecordIds['id2'],
                        'form' => 'form.test.clone.subform',
                    ],
                    [
                        'value' => $subRecordIds['id3'],
                        'form' => 'form.test.clone.subform',
                    ],
                ],
                'field_region' => $division,
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_status'} = 'finished';
        $record->save();
        $record = $form->find($recordId);
        $record->{'field_status'} = 'validated';
        $record->save();

        // Clone the main record.
        // Note: Cloning a Inquiry form record do a field_changed update.
        sleep(1);
        $cloneId = $form->clone($recordId);
        $clone = $form->find($cloneId);

        // Validate that record was cloned.
        $record = $form->find($recordId);
        $fields = $form->getFields();
        foreach ($fields as $field) {
            switch ($field->getId()) {
                // Fields that can be equals.
                case 'field_country':
                case 'field_changed':
                case 'field_region':
                case 'field_creator':
                case 'field_editors':
                case 'field_editors_update':
                case 'field_reference':
                case 'field_ulid_reference':
                case 'field_validated':
                case 'field_deleted':
                case 'field_status':
                    continue 2;
            }
            if ($field->isMultivalued()) {
                $this->assertEquals(count($record->{$field->getId()}), count($clone->{$field->getId()}), 'In field '.$field->getId());
                $recordValues = $record->get($field->getId());
                $cloneValues = $clone->get($field->getId());
                for ($i = 0; $i < count($recordValues); ++$i) {
                    $this->assertNotEquals($recordValues[$i]['value'], $cloneValues[$i]['value'], 'In field '.$field->getId());
                }
            } else {
                $recordValues = $record->get($field->getId());
                $cloneValues = $clone->get($field->getId());
                if (is_array($recordValues)) {
                    $this->assertNotEquals($recordValues['value'], $cloneValues['value'], 'In field '.$field->getId());
                } else {
                    $this->assertNotEquals($recordValues, $cloneValues, 'In field '.$field->getId());
                }
            }
        }

        // Validate that subform records were cloned too.
        $clonedSubRecords = [
            $clone->{'field_single'},
            $clone->{'field_multi'}[0],
            $clone->{'field_multi'}[1],
        ];
        $subRecords = [
            $subForm->find($subRecordIds['id1']),
            $subForm->find($subRecordIds['id2']),
            $subForm->find($subRecordIds['id3']),
        ];

        /**
         * @var int $index
         * @var FormRecord $subRecord
         */
        foreach ($subRecords as $index => $subRecord) {
            $clone = $clonedSubRecords[$index];
            $fields = $subRecord->getForm()->getFields();
            foreach ($fields as $field) {
                switch ($field->getId()) {
                    // Fields that can't be equals.
                    case 'field_country':
                    case 'field_changed':
                    case 'field_created':
                        continue 2;
                }
                if ($field->isMultivalued()) {
                    $this->assertEquals(count($subRecord->{$field->getId()}), count($clone->{$field->getId()}), 'In field '.$field->getId());
                    $recordValues = $subRecord->get($field->getId());
                    $cloneValues = $clone->get($field->getId());
                    for ($i = 0; $i < count($recordValues); ++$i) {
                        if (is_array($recordValues[$i])) {
                            $this->assertEquals($recordValues[$i]['value'], $cloneValues[$i]['value'], 'In field '.$field->getId());
                        } else {
                            $this->assertEquals($recordValues[$i], $cloneValues[$i], 'In field '.$field->getId());
                        }
                    }
                } else {
                    $recordValues = $subRecord->get($field->getId());
                    $cloneValues = $clone->get($field->getId());
                    if (is_array($recordValues)) {
                        $this->assertEquals($recordValues['value'], $cloneValues['value'], 'In field '.$field->getId());
                    } else {
                        $this->assertEquals($recordValues, $cloneValues, 'In field '.$field->getId());
                    }
                }
            }
        }
    }
}
