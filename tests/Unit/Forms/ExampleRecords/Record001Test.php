<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\ExampleRecords;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Annotations\PointCheckAction;
use App\Entity\AdministrativeDivision;
use App\Entity\InquiryFormLog;
use App\Forms\FormRecord;
use App\Plugins\AbstractPointCheckActionBase;
use App\Plugins\PointCheckActionDiscovery;
use App\Repository\InquiryFormLogRepository;
use App\Service\SessionService;
use App\Tests\Functional\SiasarPoint\PointTestBase;
use App\Tools\SiasarPointWrapper;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;
use ReflectionMethod;

/**
 * Test Siasar Point collection end-point.
 */
class Record001Test extends PointTestBase
{

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Test that a record in "form.wsprovider" can change status without loose the field "field_tariff_type".
     *
     * @return void
     *
     * @throws \Exception
     */
    public function test001(): void
    {
        $record = $this->loadRecord(dirname(__FILE__).'/../../../assets/form_records/record_01GHGBYCFRYEXCARQDYBHPAE72.json');
        $this->assertNotEmpty($record->{'field_tariff_type'});
        $record->{'field_status'} = 'finished';
        $record->save();
        $this->assertNotEmpty($record->{'field_tariff_type'});
    }
}
