<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeAdministrativeDivisionEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a division reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_division" require a "country_reference" field in form "form.test.division.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.division.country.fail.empty');
        // Try to add the field type.
        $form->addField('administrative_division_reference', 'division', 'Division');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a division reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_division" require "country" field in form "form.test.division.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.division.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'administrative_division_reference',
            'division',
            'Division',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a division reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.division.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'administrative_division_reference',
            'division',
            'Division',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist division.
     *
     * @throws \Exception
     */
    public function testInsertDivisionDataFailDivisionNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[administrative_division_reference] Field "Division [field_division]" have a wrong reference: "01FFHQPYMYKY76RY6EDKR271EZ".');

        // Create form.
        $form = $this->formFactory->create('test.division.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_division' => new AdministrativeDivision('01FFHQPYMYKY76RY6EDKR271EZ'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertDivisionDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.division.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );
    }

    /**
     * Test insert wrong division.
     *
     * @throws \Exception
     */
    public function testInsertDivisionDataFailBadDivision(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[administrative_division_reference] Field "Division [field_division]" have a reference, "01FFFBVPS6K583BY7QCEK1HSZF", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.division.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );
    }

    /**
     * Test that we can insert a division in a form.
     *
     * @throws \Exception
     */
    public function testInsertDivisionDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.division.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist division.
     *
     * @throws \Exception
     */
    public function testUpdateDivisionDataFailDivisionNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.division.udata.divnotfound] errors: 
[administrative_division_reference] Field &quot;Division [field_division]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.division.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );

        $record = $form->find($id);
        $record->{'field_division'} = new AdministrativeDivision('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateDivisionDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.division.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.division.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong division.
     *
     * @throws \Exception
     */
    public function testUpdateDivisionDataFailBadDivision(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.division.udata.baddiv] errors: 
[administrative_division_reference] Field &quot;Division [field_division]&quot; have a reference, &quot;01FFFBVPS6K583BY7QCEK1HSZF&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.division.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a division in a form.
     *
     * @throws \Exception
     */
    public function testUpdateDivisionDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.division.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_division' => $division,
            ]
        );

        $record = $form->find($id);
        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');
        $record->{'field_division'} = $division;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FFFC1K7EG1E2AZ4H2H5ZXQS3', $record->get('field_division')['value']);
    }

    /**
     * Test that we can generate a division example data.
     */
    public function testDivisionExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.division.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_division');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_division'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.division.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_division');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_division'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_division')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.division.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('administrative_division_reference', 'division', 'Division', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_division');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_division')->isEmpty());
    }
}
