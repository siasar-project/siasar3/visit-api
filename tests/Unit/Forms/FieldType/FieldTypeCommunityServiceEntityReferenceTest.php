<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\CommunityService;
use App\Entity\Country;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeCommunityServiceEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a CommunityService reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_cs" require a "country_reference" field in form "form.test.cs.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.cs.country.fail.empty');
        // Try to add the field type.
        $form->addField('community_service_reference', 'cs', 'cs');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a CommunityService reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_cs" require "country" field in form "form.test.cs.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.cs.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'community_service_reference',
            'cs',
            'cs',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a CommunityService reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.cs.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'community_service_reference',
            'cs',
            'cs',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist CommunityService.
     *
     * @throws \Exception
     */
    public function testInsertCommunityServiceDataFailCommunityServiceNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[community_service_reference] Field "cs [field_cs]" have a wrong reference: "01FHWG6F44KF2545GDEFQTQ0VA".');

        // Create form.
        $form = $this->formFactory->create('test.cs.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_cs' => new CommunityService('01FHWG6F44KF2545GDEFQTQ0VA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertCommunityServiceDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.cs.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );
    }

    /**
     * Test insert wrong CommunityService.
     *
     * @throws \Exception
     */
    public function testInsertCommunityServiceDataFailBadCommunityService(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[community_service_reference] Field "cs [field_cs]" have a reference, "01G3VA2YV4BHW5TDNJFK0VVHHX", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.cs.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );
    }

    /**
     * Test that we can insert a CommunityService in a form.
     *
     * @throws \Exception
     */
    public function testInsertCommunityServiceDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.cs.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist CommunityService.
     *
     * @throws \Exception
     */
    public function testUpdateCommunityServiceDataFailCommunityServiceNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.cs.udata.divnotfound] errors: 
[community_service_reference] Field &quot;cs [field_cs]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.cs.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );

        $record = $form->find($id);
        $record->{'field_cs'} = new CommunityService('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateCommunityServiceDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.cs.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.cs.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong CommunityService.
     *
     * @throws \Exception
     */
    public function testUpdateCommunityServiceDataFailBadCommunityService(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.cs.udata.baddiv] errors: 
[community_service_reference] Field &quot;cs [field_cs]&quot; have a reference, &quot;01G3VA2YV4BHW5TDNJFK0VVHHX&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.cs.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a CommunityService in a form.
     *
     * @throws \Exception
     */
    public function testUpdateCommunityServiceDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.cs.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $csRepo = $this->entityManager->getRepository(CommunityService::class);
        $cs = $csRepo->find('01G3VA2YV4BHW5TDNJFK0VVHHX');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_cs' => $cs,
            ]
        );

        $record = $form->find($id);
        $cs = $csRepo->find('01G3VA37QGHVK8BDE3DBKR1BHZ');
        $record->{'field_cs'} = $cs;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G3VA37QGHVK8BDE3DBKR1BHZ', $record->get('field_cs')['value']);
    }

    /**
     * Test that we can generate a CommunityService example data.
     */
    public function testCommunityServiceExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.cs.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_cs');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_cs'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.comservice.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_cs');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_cs'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_cs')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.comservice.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('community_service_reference', 'cs', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_cs');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_cs')->isEmpty());
    }
}
