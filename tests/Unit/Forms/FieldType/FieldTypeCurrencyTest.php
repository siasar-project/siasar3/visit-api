<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\Currency;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tools\CurrencyAmount;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeCurrencyTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a Currency reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_currency" require a "country_reference" field in form "form.test.currency.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.currency.country.fail.empty');
        // Try to add the field type.
        $form->addField('currency', 'currency', 'currency');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a currency reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_currency" require "country" field in form "form.test.currency.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.currency.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'currency',
            'currency',
            'currency',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a currency reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.currency.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'currency',
            'currency',
            'currency',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist currency.
     *
     * @throws \Exception
     */
    public function testInsertCurrencyDataFailCurrencyNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[currency] Field "field_currency" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.currency.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, new Currency('01FJYCWTPB91FFQMK7YVY495FA')),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertCurrencyDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.currency.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );
    }

    /**
     * Test insert wrong currency.
     *
     * @throws \Exception
     */
    public function testInsertCurrencyDataFailBadCurrency(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[currency] Field "field_currency" have a reference, "01FHQ6D3KYJXQGG72N36552NG9", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.currency.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );
    }

    /**
     * Test that we can insert a currency in a form.
     *
     * @throws \Exception
     */
    public function testInsertCurrencyDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.currency.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
        $currencyAmount = $record->{'field_currency'};
        $this->assertEquals(10, $currencyAmount->amount);
        $this->assertEquals('01FHQ6D3KYJXQGG72N36552NG9', $currencyAmount->currency->getId());

        $currencyAmount = $record->get('field_currency');
        $this->assertEquals(10, $currencyAmount["amount"]);
        $this->assertEquals('01FHQ6D3KYJXQGG72N36552NG9', $currencyAmount["value"]);
        $this->assertEquals('App\Entity\Currency', $currencyAmount["class"]);
    }

    /**
     * Test update not exist currency.
     *
     * @throws \Exception
     */
    public function testUpdateCurrencyDataFailCurrencyNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.currency.udata.divnotfound] errors: 
[currency] Field &quot;field_currency&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.currency.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );

        $record = $form->find($id);
        $record->{'field_currency'} = [
            'value' => '01FFHQPYMYKY76RY6EDKR271EZ',
            'class' => Currency::class,
            'amount' => 0,
        ];
        $record->save();
    }

    /**
     * Test insert a empty currency value.
     *
     * @throws \Exception
     */
    public function testInsertNullCurrencyDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.currency.null');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert(
            [
                'field_country' => $country,
            ]
        );
        $this->assertNotEmpty($id);
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateCurrencyDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.currency.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.currency.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong currency.
     *
     * @throws \Exception
     */
    public function testUpdateCurrencyDataFailBadCurrency(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.currency.udata.baddiv] errors: 
[currency] Field &quot;field_currency&quot; have a reference, &quot;01FHQ6D3KYJXQGG72N36552NG9&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.currency.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a currency in a form.
     *
     * @throws \Exception
     */
    public function testUpdateCurrencyDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.currency.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $currencyRepo = $this->entityManager->getRepository(Currency::class);
        $currency = $currencyRepo->find('01FHQ6D3KYJXQGG72N36552NG9');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_currency' => new CurrencyAmount(10, $currency),
            ]
        );

        $record = $form->find($id);
        $record->{'field_currency'} = [
            'value' => '01FENEDYFA6KNAA4J459P5S0RF',
            'class' => Currency::class,
            'amount' => 15,
        ];
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RF', $record->get('field_currency')['value']);
        $this->assertEquals(15, $record->get('field_currency')['amount']);
    }

    /**
     * Test that we can generate a currency example data.
     */
    public function testCurrencyExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.currency.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'currency', 'currency', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_currency');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_currency'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.currency.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.currency.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('currency', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
