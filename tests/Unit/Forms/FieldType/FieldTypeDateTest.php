<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test date fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeDateTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an date field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallDateInForm(): void
    {
        $form = $this->formFactory->create('test.date.fieldtype');
        // Try to add the trust field type.
        $form->addField('date', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_name_value', $columns);
        $this->assertArrayHasKey('field_name_timezone', $columns);
        $this->assertEquals('datetime', $columns["field_name_value"]->getType()->getName());
        $this->assertEquals('text', $columns["field_name_timezone"]->getType()->getName());
    }

    /**
     * Test that we can insert an date value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertDateInForm(): void
    {
        $form = $this->formFactory->create('test.insert.date');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('date', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Load dummy user.
        $timezone = new \DateTimeZone('Europe/Madrid');
        $date = new \DateTime('09-03-2016 09:30:00', $timezone);
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_name' => [
                    'value' => $date,
                    'timezone' => $timezone,
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        /**
         * Reference date.
         *
         * @var \DateTime $dateRef
         */
        $dateRef = $record->{'field_name'};
        $this->assertEquals($date, $dateRef);
        $this->assertEquals(get_class($date), get_class($dateRef));
        $this->assertEquals($timezone->getName(), $dateRef->getTimezone()->getName());
        $this->assertEquals('09-03-2016 09:30:00', $dateRef->format('d-m-Y H:i:s'));
        // Read field how properties array.
        $dateRef = $record->get('field_name');
        $this->assertEquals($date->getTimestamp(), $dateRef['value']->getTimestamp());
        $this->assertEquals($timezone->getName(), $dateRef['timezone']);
        $this->assertEquals('09-03-2016 09:30:00', $dateRef['value']->format('d-m-Y H:i:s'));

        // Insert by instance.
        $timezone = new \DateTimeZone('Europe/Madrid');
        $date = new \DateTime('09-03-2016 09:30:00', $timezone);
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_name' => $date,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $dateRef = $record->{'field_name'};
        $this->assertEquals($date, $dateRef);
        $this->assertEquals(get_class($date), get_class($dateRef));
        $this->assertEquals($timezone->getName(), $dateRef->getTimezone()->getName());
        $this->assertEquals('09-03-2016 09:30:00', $dateRef->format('d-m-Y H:i:s'));
        // Read field how properties array.
        $dateRef = $record->get('field_name');
        $this->assertEquals($date->getTimestamp(), $dateRef['value']->getTimestamp());
        $this->assertEquals($timezone->getName(), $dateRef['timezone']);
        $this->assertEquals('09-03-2016 09:30:00', $dateRef['value']->format('d-m-Y H:i:s'));
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedDateFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.date');
        $manager->addField('date', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.date');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_timezone', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedDateFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.date');
        $manager->addField('date', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.date');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedDateFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.multivalued.entity.reference.a');
        $form->addField('date', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.multivalued.entity.reference.a');
        $dates = [
            new \DateTime('01-03-2021 11:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('02-03-2021 12:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('03-03-2021 13:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('04-03-2021 14:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('05-03-2021 15:00:00', new \DateTimeZone('Europe/Madrid')),
        ];
        $recordId = $form->insert(
            [
                'field_reference' => $dates,
            ]
        );
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_timezone', $item);
            // In month 10 the difference with UTC it's 2 hours, I use month 3
            // to get a difference of 1 hour.
            // The reference hour it's 11 o'clock to not get a left cero in the
            // UTC date.
            $this->assertEquals(
                '2021-03-0'.($index + 1).' '.($index + 10).':00:00',
                $item['field_reference_value']
            );
            $this->assertEquals('Europe/Madrid', $item['field_reference_timezone']);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedDateFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.date');
        $form->addField('date', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.date');
        $recordId = $form->insert(
            [
                'field_reference' => [
                    new \DateTime('01-03-2021 11:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('02-03-2021 12:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('03-03-2021 13:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('04-03-2021 14:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('05-03-2021 15:00:00', new \DateTimeZone('Europe/Madrid')),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [
            new \DateTime('03-03-2021 13:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('04-03-2021 14:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('05-03-2021 15:00:00', new \DateTimeZone('Europe/Madrid')),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_timezone', $item);
            // In month 10 the difference with UTC it's 2 hours, I use month 3
            // to get a difference of 1 hour.
            // The reference hour it's 11 o'clock to not get a left cero in the
            // UTC date.
            $this->assertEquals(
                '2021-03-0'.($index + 3).' '.($index + 12).':00:00',
                $item['field_reference_value']
            );
            $this->assertEquals('Europe/Madrid', $item['field_reference_timezone']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedDateFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.date');
        $form->addField('date', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.date');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    new \DateTime('01-03-2021 11:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('02-03-2021 12:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('03-03-2021 13:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('04-03-2021 14:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('05-03-2021 15:00:00', new \DateTimeZone('Europe/Madrid')),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [
            new \DateTime('03-03-2021 13:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('04-03-2021 14:00:00', new \DateTimeZone('Europe/Madrid')),
            new \DateTime('05-03-2021 15:00:00', new \DateTimeZone('Europe/Madrid')),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_timezone', $item);
            // In month 10 the difference with UTC it's 2 hours, I use month 3
            // to get a difference of 1 hour.
            // The reference hour it's 11 o'clock to not get a left cero in the
            // UTC date.
            $this->assertEquals(
                '2021-03-0'.($index + 3).' '.($index + 12).':00:00',
                $item['field_reference_value']
            );
            $this->assertEquals('Europe/Madrid', $item['field_reference_timezone']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldDateInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.date');
        $form->addField('date', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.date');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    new \DateTime('01-03-2021 11:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('02-03-2021 12:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('03-03-2021 13:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('04-03-2021 14:00:00', new \DateTimeZone('Europe/Madrid')),
                    new \DateTime('05-03-2021 15:00:00', new \DateTimeZone('Europe/Madrid')),
                ],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $index => $item) {
            $this->assertInstanceOf(\DateTime::class, $item);
            $this->assertEquals(
                '2021-03-0'.($index + 1).' '.($index + 11).':00:00',
                $item->format('Y-m-d H:i:s')
            );
            $this->assertEquals('Europe/Madrid', $item->getTimezone()->getName());
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $index => $item) {
            $this->assertArrayHasKey('value', $item);
            $this->assertArrayHasKey('timezone', $item);
            // Internally the date field use UTC to store date.
            $this->assertEquals(
                '2021-03-0'.($index + 1).' '.($index + 10).':00:00',
                $item['value']
            );
            $this->assertEquals('Europe/Madrid', $item['timezone']);

            $this->assertIsString($item['value']);
            $this->assertIsString($item['timezone']);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.date.notempty');
        $form->addField('date', 'reference', 'cs');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.date.empty');
        $form->addField('date', 'reference', 'cs');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        // Can't be empty.
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
