<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tools\Measurements\LengthMeasurement;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Yaml\Yaml;

/**
 * Test Diameter fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeDiameterTest extends KernelTestCase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected mixed $units;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');

        $units = Yaml::parseFile(dirname(__FILE__).'/../../../assets/system.units.length.yml');
        $cfg = $this->configuration->findEditable('system.units.length');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.length', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        $this->units = $units;
    }

    /**
     * Test that we can install a diameter field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallDiameterInForm(): void
    {
        $form = $this->formFactory->create('test.diameter.fieldtype');
        // Try to add the trust field type.
        $form->addField('diameter', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_name_value', $columns);
        $this->assertArrayHasKey('field_name_unit', $columns);
        $this->assertEquals('decimal', $columns["field_name_value"]->getType()->getName());
        $this->assertEquals('text', $columns["field_name_unit"]->getType()->getName());
    }

    /**
     * Test that we can insert a diameter value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertDiameterInForm(): void
    {
        $form = $this->formFactory->create('test.insert.diameter');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('diameter', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Load dummy user.
        $unit = 'kilometre';
        $length = new LengthMeasurement(1, $unit);
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_name' => [
                    'value' => $length,
                    'unit' => $unit,
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        /**
         * Reference length.
         *
         * @var LengthMeasurement $lengthRef
         */
        $lengthRef = $record->{'field_name'};
        $this->assertEquals($length, $lengthRef);
        $this->assertEquals(get_class($length), get_class($lengthRef));
        $this->assertEquals($unit, $lengthRef->getUnit());
        $this->assertEquals(1, $lengthRef->getValue());
        // Read field how properties array.
        $lengthRef = $record->get('field_name');
        $this->assertEquals($length->getUnit(), $lengthRef['value']->getUnit());
        $this->assertEquals($unit, $lengthRef['unit']);
        $this->assertEquals(1, $lengthRef['value']->getValue());

        // Insert by instance.
        $unit = 'kilometre';
        $length = new LengthMeasurement(1, $unit);
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_name' => $length,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $lengthRef = $record->{'field_name'};
        $this->assertEquals($length, $lengthRef);
        $this->assertEquals(get_class($length), get_class($lengthRef));
        $this->assertEquals($unit, $lengthRef->getUnit());
        $this->assertEquals(1, $lengthRef->getValue());
        // Read field how properties array.
        $lengthRef = $record->get('field_name');
        $this->assertEquals($length->getUnit(), $lengthRef['value']->getUnit());
        $this->assertEquals($unit, $lengthRef['unit']);
        $this->assertEquals(1, $lengthRef['value']->getValue());
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedDiameterFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.diameter');
        $manager->addField('diameter', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.diameter');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_unit', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedDiameterFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.diameter');
        $manager->addField('diameter', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.diameter');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedDiameterFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.multivalued.entity.reference.dga');
        $form->addField('diameter', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.multivalued.entity.reference.dga');
        $lengths = [
            new LengthMeasurement(11, 'kilometre'),
            new LengthMeasurement(12, 'kilometre'),
            new LengthMeasurement(13, 'kilometre'),
            new LengthMeasurement(14, 'kilometre'),
            new LengthMeasurement(15, 'kilometre'),
        ];
        $recordId = $form->insert(
            [
                'field_reference' => $lengths,
            ]
        );
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_unit', $item);

            $this->assertEquals(($index + 11) * 1000, $item['field_reference_value']);
            $this->assertEquals('kilometre', $item['field_reference_unit']);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedDiameterFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.diameter');
        $form->addField('diameter', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.diameter');
        $recordId = $form->insert(
            [
                'field_reference' => [
                    new LengthMeasurement(11, 'kilometre'),
                    new LengthMeasurement(12, 'kilometre'),
                    new LengthMeasurement(13, 'kilometre'),
                    new LengthMeasurement(14, 'kilometre'),
                    new LengthMeasurement(15, 'kilometre'),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [
            new LengthMeasurement(13, 'kilometre'),
            new LengthMeasurement(14, 'kilometre'),
            new LengthMeasurement(15, 'kilometre'),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_unit', $item);

            $this->assertEquals(($index + 13) * 1000, $item['field_reference_value']);
            $this->assertEquals('kilometre', $item['field_reference_unit']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedDiameterFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.diameter');
        $form->addField('diameter', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.diameter');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    new LengthMeasurement(11, 'kilometre'),
                    new LengthMeasurement(12, 'kilometre'),
                    new LengthMeasurement(13, 'kilometre'),
                    new LengthMeasurement(14, 'kilometre'),
                    new LengthMeasurement(15, 'kilometre'),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [
            new LengthMeasurement(13, 'kilometre'),
            new LengthMeasurement(14, 'kilometre'),
            new LengthMeasurement(15, 'kilometre'),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_unit', $item);

            $this->assertEquals(($index + 13) * 1000, $item['field_reference_value']);
            $this->assertEquals('kilometre', $item['field_reference_unit']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldDiameterInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.diameter');
        $form->addField('diameter', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.diameter');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    new LengthMeasurement(11, 'kilometre'),
                    new LengthMeasurement(12, 'kilometre'),
                    new LengthMeasurement(13, 'kilometre'),
                    new LengthMeasurement(14, 'kilometre'),
                    new LengthMeasurement(15, 'kilometre'),
                ],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $index => $item) {
            $this->assertInstanceOf(LengthMeasurement::class, $item);
            $this->assertEquals($index + 11, $item->getValue());
            $this->assertEquals('kilometre', $item->getUnit());
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $index => $item) {
            $this->assertArrayHasKey('value', $item);
            $this->assertArrayHasKey('unit', $item);
            // Internally the length field use metre to store values.
            $this->assertEquals(($index + 11) * 1000, $item['value']);
            $this->assertEquals('kilometre', $item['unit']);

            $this->assertIsNumeric($item['value']);
            $this->assertIsString($item['unit']);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.diameter.empty');
        $form->addField('diameter', 'reference', 'Record reference');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();
        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.diameter.empty');
        $form->addField('diameter', 'reference', 'Record reference');
        $form->saveNow();
        $form->install();
        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
