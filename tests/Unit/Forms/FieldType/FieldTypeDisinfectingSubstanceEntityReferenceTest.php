<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\DisinfectingSubstance;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeDisinfectingSubstanceEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeDisinfectingSubstanceEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ds" require a "country_reference" field in form "form.test.ds.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ds.country.fail.empty');
        // Try to add the field type.
        $form->addField('disinfect_substance_reference', 'ds', 'ds');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a DisinfectingSubstance reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ds" require "country" field in form "form.test.ds.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ds.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'disinfect_substance_reference',
            'ds',
            'ds',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a DisinfectingSubstance reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ds.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'disinfect_substance_reference',
            'ds',
            'ds',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist DisinfectingSubstance.
     *
     * @throws \Exception
     */
    public function testInsertDisinfectingSubstanceDataFailDisinfectingSubstanceNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[disinfect_substance_reference] Field "field_ds" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ds.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ds' => new DisinfectingSubstance('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertDisinfectingSubstanceDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ds.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );
    }

    /**
     * Test insert wrong DisinfectingSubstance.
     *
     * @throws \Exception
     */
    public function testInsertDisinfectingSubstanceDataFailBadDisinfectingSubstance(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[disinfect_substance_reference] Field "field_ds" have a reference, "01G2WJVF3Q0MZYJWH2D9KEY1A2", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ds.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );
    }

    /**
     * Test that we can insert a DisinfectingSubstance in a form.
     *
     * @throws \Exception
     */
    public function testInsertDisinfectingSubstanceDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ds.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist DisinfectingSubstance.
     *
     * @throws \Exception
     */
    public function testUpdateDisinfectingSubstanceDataFailDisinfectingSubstanceNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ds.updata.notfound] errors: 
[disinfect_substance_reference] Field &quot;field_ds&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ds.updata.notfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ds'} = [
            'value' => '01FFHQPYMYKY76RY6EDKR271EZ',
            'class' => DisinfectingSubstance::class,
        ];
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateDisinfectingSubstanceDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ds.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ds.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong DisinfectingSubstance.
     *
     * @throws \Exception
     */
    public function testUpdateDisinfectingSubstanceDataFailBadDisinfectingSubstance(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ds.udata.baddiv] errors: 
[disinfect_substance_reference] Field &quot;field_ds&quot; have a reference, &quot;01G2WJVF3Q0MZYJWH2D9KEY1A2&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ds.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a DisinfectingSubstance in a form.
     *
     * @throws \Exception
     */
    public function testUpdateDisinfectingSubstanceDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ds.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desinRepo = $this->entityManager->getRepository(DisinfectingSubstance::class);
        /** @var DisinfectingSubstance $desin */
        $desin = $desinRepo->find('01G2WJVF3Q0MZYJWH2D9KEY1A2');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ds' => $desin,
            ]
        );

        $record = $form->find($id);
        /** @var DisinfectingSubstance $desin */
        $desin = $desinRepo->find('01G2WJVM9VFNXM0SB7D65V2E7A');
        $record->{'field_ds'} = [
            'value' => $desin->getId(),
            'class' => DisinfectingSubstance::class,
        ];
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2WJVM9VFNXM0SB7D65V2E7A', $record->get('field_ds')['value']);
    }

    /**
     * Test that we can generate a DisinfectingSubstance example data.
     */
    public function testDisinfectingSubstanceExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ds.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'ds', 'ds', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ds');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ds'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.disinfect.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.disinfect.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('disinfect_substance_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
