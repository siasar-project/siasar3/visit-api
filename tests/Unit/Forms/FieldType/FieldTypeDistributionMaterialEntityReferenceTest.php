<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\DistributionMaterial;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeDistributionMaterialEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a Distribution Material reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_material" require a "country_reference" field in form "form.test.dmaterial.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.dmaterial.country.fail.empty');
        // Try to add the field type.
        $form->addField('distribution_material_reference', 'material', 'material');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a Distribution material reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_material" require "country" field in form "form.test.dmaterial.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.dmaterial.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'distribution_material_reference',
            'material',
            'material',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a Distribution material reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.dmaterial.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'distribution_material_reference',
            'material',
            'material',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist Distribution material.
     *
     * @throws \Exception
     */
    public function testInsertDistributionMaterialDataFailDistributionMaterialNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[distribution_material_reference] Field "material [field_material]" have a wrong reference: "01FHWG6F44KF2545GDEFQTQ0VA".');

        // Create form.
        $form = $this->formFactory->create('test.dmaterial.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_material' => new DistributionMaterial('01FHWG6F44KF2545GDEFQTQ0VA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertDistributionMaterialDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.dmaterial.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );
    }

    /**
     * Test insert wrong Distribution material.
     *
     * @throws \Exception
     */
    public function testInsertDistributionMaterialDataFailBadDistributionMaterial(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[distribution_material_reference] Field "material [field_material]" have a reference, "01G2PR2VHQ3866Q9B8XYWNDJHW", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.dmaterial.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );
    }

    /**
     * Test that we can insert a Distribution material in a form.
     *
     * @throws \Exception
     */
    public function testInsertDistributionMaterialDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.dmaterial.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist Distribution material.
     *
     * @throws \Exception
     */
    public function testUpdateDistributionMaterialDataFailDistributionMaterialNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.dmaterial.udata.divnotfound] errors: 
[distribution_material_reference] Field &quot;material [field_material]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.dmaterial.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );

        $record = $form->find($id);
        $record->{'field_material'} = new DistributionMaterial('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateDistributionMaterialDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.dmaterial.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.dmaterial.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong Distribution material.
     *
     * @throws \Exception
     */
    public function testUpdateDistributionMaterialDataFailBadDistributionMaterial(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.dmaterial.udata.baddiv] errors: 
[distribution_material_reference] Field &quot;material [field_material]&quot; have a reference, &quot;01G2PR2VHQ3866Q9B8XYWNDJHW&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.dmaterial.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a Distribution material in a form.
     *
     * @throws \Exception
     */
    public function testUpdateDistributionMaterialDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.dmaterial.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $distributionMaterialRepo = $this->entityManager->getRepository(DistributionMaterial::class);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR2VHQ3866Q9B8XYWNDJHW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_material' => $distributionMaterial,
            ]
        );

        $record = $form->find($id);
        $distributionMaterial = $distributionMaterialRepo->find('01G2PR25MW2PYV03EDE2EFE2X8');
        $record->{'field_material'} = $distributionMaterial;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2PR25MW2PYV03EDE2EFE2X8', $record->get('field_material')['value']);
    }

    /**
     * Test that we can generate a Distribution material example data.
     */
    public function testDistributionMaterialExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.dmaterial.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'material', 'material', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_material');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_material'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.distribution.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.distribution.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('distribution_material_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
