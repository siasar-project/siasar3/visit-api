<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an entity reference field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallEntityReferenceInForm(): void
    {
        $form = $this->formFactory->create('test.entity.reference.fieldtype');
        // Try to add the trust field type.
        $form->addField('entity_reference', 'Owner', 'User owner');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_owner_value', $columns);
        $this->assertArrayHasKey('field_owner_class', $columns);
        $this->assertEquals('binary', $columns["field_owner_value"]->getType()->getName());
        $this->assertEquals('text', $columns["field_owner_class"]->getType()->getName());
    }

    /**
     * Test that we can insert an entity reference value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertEntityReferenceInForm(): void
    {
        $form = $this->formFactory->create('test.insert.entity.reference');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('entity_reference', 'Owner', 'User owner');
        $form->saveNow();
        $form->install();
        // Load dummy user.
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Pedro']);
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_owner' => [
                    'value' => $user->getId(),
                    'class' => get_class($user),
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $userRef = $record->{'field_owner'};
        $this->assertEquals($user->getId(), $userRef->getId());
        $this->assertEquals(get_class($user), get_class($userRef));
        // Read field how properties array.
        $userRef = $record->get('field_owner');
        $this->assertEquals($user->getId(), $userRef['value']);
        $this->assertEquals(get_class($user), $userRef['class']);

        // Insert by instance.
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']);
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_owner' => $user,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $userRef = $record->{'field_owner'};
        $this->assertEquals($user->getId(), $userRef->getId());
        $this->assertEquals(get_class($user), get_class($userRef));
        // Read field how properties array.
        $userRef = $record->get('field_owner');
        $this->assertEquals($user->getId(), $userRef['value']);
        $this->assertEquals(get_class($user), $userRef['class']);
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.entity.reference');
        $manager->addField('entity_reference', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.entity.reference');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_class', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.entity.reference');
        $manager->addField('entity_reference', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.entity.reference');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.multivalued.entity.reference.h');
        $form->addField('entity_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.multivalued.entity.reference.h');
        $users = [
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Pedro']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Carlos']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marta']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Nico']),
        ];
        $recordId = $form->insert(
            [
                'field_reference' => $users,
            ]
        );
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_class', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $dbUser = $this->entityManager->getRepository('App\Entity\User')->find($ulid);
            $this->assertInstanceOf('App\Entity\User', $dbUser);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.i');
        $form->addField('entity_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.i');
        $recordId = $form->insert(
            [
                'field_reference' => [
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Pedro']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Carlos']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marta']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Nico']),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marta']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Nico']),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_class', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $dbUser = $this->entityManager->getRepository('App\Entity\User')->find($ulid);
            $this->assertInstanceOf('App\Entity\User', $dbUser);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.j');
        $form->addField('entity_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.j');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Pedro']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Carlos']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marta']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Nico']),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marta']),
            $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Nico']),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_class', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $dbUser = $this->entityManager->getRepository('App\Entity\User')->find($ulid);
            $this->assertInstanceOf('App\Entity\User', $dbUser);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.c');
        $form->addField('entity_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.c');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Pedro']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Carlos']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marco']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Marta']),
                    $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Nico']),
                ],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $item) {
            $this->assertInstanceOf('App\Entity\User', $item);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertArrayHasKey('value', $item);
            $this->assertArrayHasKey('class', $item);
            $this->assertIsString($item['value']);
            $this->assertEquals('App\Entity\User', $item['class']);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.entity.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('entity_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.entity.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('entity_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
