<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\Ethnicity;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeEthnicityEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeEthnicityEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ethnicity" require a "country_reference" field in form "form.test.ethnicity.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ethnicity.country.fail.empty');
        // Try to add the field type.
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a ethnicity reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ethnicity" require "country" field in form "form.test.ethnicity.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ethnicity.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'ethnicity_reference',
            'ethnicity',
            'ethnicity',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a ethnicity reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ethnicity.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'ethnicity_reference',
            'ethnicity',
            'ethnicity',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist ethnicity.
     *
     * @throws \Exception
     */
    public function testInsertEthnicityDataFailEthnicityNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[ethnicity_reference] Field "ethnicity [field_ethnicity]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ethnicity.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => new Ethnicity('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertEthnicityDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ethnicity.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );
    }

    /**
     * Test insert wrong ethnicity.
     *
     * @throws \Exception
     */
    public function testInsertEthnicityDataFailBadEthnicity(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[ethnicity_reference] Field "ethnicity [field_ethnicity]" have a reference, "01FHQA6AXYXMA0STNGNHCRVJEG", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ethnicity.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );
    }

    /**
     * Test that we can insert a ethnicity in a form.
     *
     * @throws \Exception
     */
    public function testInsertEthnicityDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ethnicity.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist ethnicity.
     *
     * @throws \Exception
     */
    public function testUpdateEthnicityDataFailEthnicityNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ethnicity.udata.divnotfound] errors: 
[ethnicity_reference] Field &quot;ethnicity [field_ethnicity]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ethnicity.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ethnicity'} = new Ethnicity('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateEthnicityDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ethnicity.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ethnicity.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong ethnicity.
     *
     * @throws \Exception
     */
    public function testUpdateEthnicityDataFailBadEthnicity(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ethnicity.udata.baddiv] errors: 
[ethnicity_reference] Field &quot;ethnicity [field_ethnicity]&quot; have a reference, &quot;01FHQA6AXYXMA0STNGNHCRVJEG&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ethnicity.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a ethnicity in a form.
     *
     * @throws \Exception
     */
    public function testUpdateEthnicityDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ethnicity.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ethnicityRepo = $this->entityManager->getRepository(Ethnicity::class);
        $ethnicity = $ethnicityRepo->find('01FHQA6AXYXMA0STNGNHCRVJEG');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ethnicity' => $ethnicity,
            ]
        );

        $record = $form->find($id);
        $ethnicity = $ethnicityRepo->find('01FENEDYFA6KNAA4J459P5S0RF');
        $record->{'field_ethnicity'} = $ethnicity;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RF', $record->get('field_ethnicity')['value']);
    }

    /**
     * Test that we can generate a ethnicity example data.
     */
    public function testEthnicityExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ethnicity.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'ethnicity', 'ethnicity', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ethnicity');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ethnicity'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.ethnicity.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.ethnicity.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('ethnicity_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
