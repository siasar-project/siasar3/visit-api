<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\File;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\InitTestEnvironmentTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test file entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeFileEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;
    use InitTestEnvironmentTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');

        $this->initConfigurations($this->configuration);

        // Inflate files.
        $dir = __DIR__.'/../../../files/';
        $files = $this->entityManager->getRepository('App\Entity\File')->findAll();
        /** @var File $file */
        foreach ($files as $file) {
            $this->inflateFile($dir.$file->getPath());
        }
    }

    /**
     * This method is called after each test.
     */
    protected function tearDown(): void
    {
        // Inflate files.
        $dir = __DIR__.'/../../../files/';
        $files = $this->entityManager->getRepository('App\Entity\File')->findAll();
        /** @var File $file */
        foreach ($files as $file) {
            unlink($dir.$file->getPath());
        }
    }

    /**
     * Test that we can install a file reference field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallFileEntityReferenceInForm(): void
    {
        $form = $this->formFactory->create('test.file.reference.fieldtype');
        // Try to add the trust field type.
        $form->addField('file_reference', 'file', 'File');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_file_value', $columns);
        $this->assertArrayHasKey('field_file_class', $columns);
        $this->assertArrayHasKey('field_file_filename', $columns);
        $this->assertEquals('binary', $columns["field_file_value"]->getType()->getName());
        $this->assertEquals('text', $columns["field_file_class"]->getType()->getName());
        $this->assertEquals('text', $columns["field_file_filename"]->getType()->getName());
    }

    /**
     * Test that we can insert a file reference value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertFileEntityReferenceInForm(): void
    {
        $form = $this->formFactory->create('test.insert.file.reference');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('file_reference', 'file', 'File');
        $form->saveNow();
        $form->install();
        // Load dummy file.
        /** @var File $file */
        $file = $this->entityManager->getRepository('App\Entity\File')->findOneBy([], ['fileName' => 'asc']);
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_file' => [
                    'value' => $file->getId(),
                    'class' => get_class($file),
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        /** @var File $fileRef */
        $fileRef = $record->{'field_file'};
        $this->assertEquals($file->getId(), $fileRef->getId());
        $this->assertEquals(get_class($file), get_class($fileRef));
        $this->assertEquals($file->getFileName(), $fileRef->getFileName());
        // Read field how properties array.
        $fileRef = $record->get('field_file');
        $this->assertEquals($file->getId(), $fileRef['value']);
        $this->assertEquals(get_class($file), $fileRef['class']);
        $this->assertEquals($file->getFileName(), $fileRef['filename']);

        // Insert by instance.
        /** @var File $file */
        $file = $this->entityManager->getRepository('App\Entity\File')->findOneBy([], ['fileName' => 'desc']);
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_file' => $file,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        /** @var File $fileRef */
        $fileRef = $record->{'field_file'};
        $this->assertEquals($file->getId(), $fileRef->getId());
        $this->assertEquals(get_class($file), get_class($fileRef));
        $this->assertEquals($file->getFileName(), $fileRef->getFileName());
        // Read field how properties array.
        $fileRef = $record->get('field_file');
        $this->assertEquals($file->getId(), $fileRef['value']);
        $this->assertEquals(get_class($file), $fileRef['class']);
        $this->assertEquals($file->getFileName(), $fileRef['filename']);
    }

    /**
     * Test that we can add a multivalued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.file.reference');
        $manager->addField('file_reference', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.file.reference');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_class', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_filename', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall a form with multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.file.reference');
        $manager->addField('file_reference', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.file.reference');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multivalued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.multivalued.file.reference.h');
        $form->addField('file_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.multivalued.file.reference.h');
        $files = [
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
        ];
        $recordId = $form->insert(
            [
                'field_reference' => $files,
            ]
        );
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_class', $item);
            $this->assertArrayHasKey('field_reference_filename', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $dbFile = $this->entityManager->getRepository('App\Entity\File')->find($ulid);
            $this->assertInstanceOf('App\Entity\File', $dbFile);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedFieldInFormRecord(): void
    {
        $tableId = uniqid();
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.with.multivalued.'.$tableId);
        $form->addField('file_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.with.multivalued.'.$tableId);
        $recordId = $form->insert(
            [
                'field_reference' => [
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
        ];
        $record->save();
        // Verify that we have a new database id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_class', $item);
            $this->assertArrayHasKey('field_reference_filename', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $dbFile = $this->entityManager->getRepository('App\Entity\File')->find($ulid);
            $this->assertInstanceOf('App\Entity\File', $dbFile);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        $tableId = uniqid();
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.single.multivalued.'.$tableId);
        $form->addField('file_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.single.multivalued.'.$tableId);
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
            $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_class', $item);
            $this->assertArrayHasKey('field_reference_filename', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $dbFile = $this->entityManager->getRepository('App\Entity\File')->find($ulid);
            $this->assertInstanceOf('App\Entity\File', $dbFile);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        $tableId = uniqid();
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.single.multivalued.'.$tableId);
        $form->addField('file_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.single.multivalued.'.$tableId);
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'example.txt']),
                    $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']),
                ],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $item) {
            $this->assertInstanceOf('App\Entity\File', $item);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertArrayHasKey('value', $item);
            $this->assertArrayHasKey('class', $item);
            $this->assertArrayHasKey('filename', $item);
            $this->assertIsString($item['value']);
            $this->assertEquals('App\Entity\File', $item['class']);
        }
    }

    /**
     * Test that we can limit files by file extension.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFileReferenceExtensionFail(): void
    {
        $this->loginUser('Pedro');
        $form = $this->formFactory->create('test.file.extension');
        // Try to add the trust field type.
        $form->addField('file_reference', 'file', 'File', ['allow_extension' => ['txt', 'pdf']]);
        $form->saveNow();
        $form->install();

        // Load dummy file.
        /** @var File $file */
        $file = $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[file_reference] Field "field_file" file extension not valid, must be txt, pdf.');

        // Insert by internal properties.
        $form->insert(
            [
                'field_file' => $file,
            ]
        );
    }

    /**
     * Test that we can limit files by file extension and on fail remove file.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFileReferenceExtensionFailAndErase(): void
    {
        $this->loginUser('Pedro');
        $form = $this->formFactory->create('test.file.extension');
        // Try to add the trust field type.
        $form->addField('file_reference', 'file', 'File', ['allow_extension' => ['txt', 'pdf']]);
        $form->saveNow();
        $form->install();

        // Load dummy file.
        /** @var File $file */
        $file = $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']);

        // Insert by internal properties.
        try {
            $form->insert(
                [
                    'field_file' => $file,
                ]
            );
        } catch (\Exception $e) {
            // Is the file erased?
            $file = $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']);
            $this->assertNull($file);
        }
    }

    /**
     * Test that we can limit files size.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFileReferenceSizeFail(): void
    {
        $this->loginUser('Pedro');
        $form = $this->formFactory->create('test.file.size');
        // Try to add the trust field type.
        $form->addField('file_reference', 'file', 'File', ['maximum_file_size' => 1]);
        $form->saveNow();
        $form->install();

        // Load dummy file.
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)->findOneBy(['fileName' => 'dummy.bin']);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[file_reference] Field "field_file" file size is too big, 4 B, must be less than 1 B.');

        // Insert by internal properties.
        $form->insert(
            [
                'field_file' => $file,
            ]
        );
    }

    /**
     * Test that we can limit files size and erase on fail.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFileReferenceSizeFailAndErase(): void
    {
        $this->loginUser('Pedro');
        $form = $this->formFactory->create('test.file.size');
        // Try to add the trust field type.
        $form->addField('file_reference', 'file', 'File', ['maximum_file_size' => 1]);
        $form->saveNow();
        $form->install();

        // Load dummy file.
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)->findOneBy(['fileName' => 'dummy.bin']);

        // Insert by internal properties.
        try {
            $form->insert(
                [
                    'field_file' => $file,
                ]
            );
        } catch (\Exception $e) {
            // Is the file erased?
            $file = $this->entityManager->getRepository('App\Entity\File')->findOneBy(['fileName' => 'dummy.bin']);
            $this->assertNull($file);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.file.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('file_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData([]);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.file.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('file_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Create the file and put 4 bytes in it.
     *
     * @param $filePath
     *
     * @return void
     */
    protected function inflateFile($filePath)
    {
        $folder = pathinfo($filePath, PATHINFO_DIRNAME);
        @mkdir($folder, 0777, true);
        file_put_contents($filePath, '1234');
    }
}
