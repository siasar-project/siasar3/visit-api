<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test form records reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeFormRecordReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an form record reference field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallFormRecordReferenceInForm(): void
    {
        $form = $this->formFactory->create('test.form.record.reference.fieldtype');
        // Try to add the trust field type.
        $form->addField('form_record_reference', 'Subform', 'Custom fields');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_subform_value', $columns);
        $this->assertArrayHasKey('field_subform_form', $columns);
        $this->assertEquals('binary', $columns["field_subform_value"]->getType()->getName());
        $this->assertEquals('text', $columns["field_subform_form"]->getType()->getName());
    }

    /**
     * Test that we can insert an form record reference value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertFormRecordReferenceInForm(): void
    {
        $form = $this->formFactory->create('test.insert.form.record.reference');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('form_record_reference', 'Subform', 'Custom fields');
        $form->saveNow();
        $form->install();
        // Load dummy user.
        $subRecord = $this->getSubformRecord();
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_subform' => [
                    'value' => $subRecord->getId(),
                    'form' => $subRecord->getForm()->getId(),
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $recordRef = $record->{'field_subform'};
        $this->assertEquals($subRecord->getId(), $recordRef->getId());
        $this->assertEquals($subRecord->getForm()->getId(), $recordRef->getForm()->getId());
        // Read field how properties array.
        $recordRef = $record->get('field_subform');
        $this->assertEquals($subRecord->getId(), $recordRef['value']);
        $this->assertEquals($subRecord->getForm()->getId(), $recordRef['form']);

        // Insert by instance.
        $subRecord = $this->getSubformRecord();
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_subform' => $subRecord,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $recordRef = $record->{'field_subform'};
        $this->assertEquals($subRecord->getId(), $recordRef->getId());
        $this->assertEquals($subRecord->getForm()->getId(), $recordRef->getForm()->getId());
        $this->assertEquals($subRecord->{'field_reference'}, $recordRef->{'field_reference'});
        // Read field how properties array.
        $recordRef = $record->get('field_subform');
        $this->assertEquals($subRecord->getId(), $recordRef['value']);
        $this->assertEquals($subRecord->getForm()->getId(), $recordRef['form']);
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.record.reference');
        $manager->addField('form_record_reference', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.record.reference');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_form', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.record.reference');
        $manager->addField('form_record_reference', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.record.reference');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.d');
        $form->addField('form_record_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $subFormRecord = $this->getSubformRecord();
        $form = $this->formFactory->find('test.form.record.with.multivalued.d');
        $recordId = $form->insert(
            [
                'field_reference' => [
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [
            $subFormRecord,
            $subFormRecord,
            $subFormRecord,
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_form', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $subform = $this->formFactory->find($item['field_reference_form']);
            $dbRecord = $subform->find($ulid);
            $this->assertInstanceOf('App\Forms\FormRecord', $dbRecord);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.d');
        $form->addField('form_record_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $subFormRecord = $this->getSubformRecord();
        $form = $this->formFactory->find('test.record.single.multivalued.d');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [
            $subFormRecord,
            $subFormRecord,
            $subFormRecord,
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_form', $item);
            $ulid = Ulid::fromString($item['field_reference_value']);
            $subform = $this->formFactory->find($item['field_reference_form']);
            $dbRecord = $subform->find($ulid);
            $this->assertInstanceOf('App\Forms\FormRecord', $dbRecord);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.d');
        $form->addField('form_record_reference', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $subFormRecord = $this->getSubformRecord();
        $form = $this->formFactory->find('test.read.single.multivalued.d');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                    $subFormRecord,
                ],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $item) {
            $this->assertInstanceOf('App\Forms\FormRecord', $item);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertArrayHasKey('value', $item);
            $this->assertArrayHasKey('form', $item);
            $this->assertIsString($item['value']);
            $ulid = Ulid::fromString($item['value']);
            $subform = $this->formFactory->find($item['form']);
            $dbRecord = $subform->find($ulid);
            $this->assertInstanceOf('App\Forms\FormRecord', $dbRecord);
            $this->assertEquals($subform->getId(), $item['form']);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.record.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField(
            'form_record_reference',
            'Subform',
            'Custom fields',
            ['country_field' => 'field_country']
        );
        $form->saveNow();
        $form->install();

        $id = $form->insert(['field_subform' => $this->getSubformRecord()]);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_subform')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.record.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField(
            'form_record_reference',
            'Subform',
            'Custom fields',
            ['country_field' => 'field_country']
        );
        $form->saveNow();
        $form->install();
        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_subform')->isEmpty());
    }

    /**
     * Create a dummy form record to use how base test.
     *
     * @return FormRecord
     *
     * @throws \Exception
     */
    protected function getSubformRecord(): FormRecord
    {
        // Using only one field we can create and filter with the same array.
        $data = ['field_reference' => random_int(1, 100)];
        // Create the form.
        $form = $this->formFactory->create('test.subform');
        $form->addField('integer', 'Reference', 'Record reference');
        $form->saveNow();
        // Install it.
        $form->install();
        // Create and insert record.
        $form->insert($data);
        // Load the new record.
        $record = $form->findOneBy($data);
        // Return it.
        return $record;
    }
}
