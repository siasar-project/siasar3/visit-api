<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\FunctionsCarriedOutWsp;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeFunctionsCarriedOutWspEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FunctionsCarriedOutWsp reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_functions" require a "country_reference" field in form "form.test.functions.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.functions.country.fail.empty');
        // Try to add the field type.
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a FunctionsCarriedOutWsp reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_functions" require "country" field in form "form.test.functions.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.functions.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'functions_carried_out_wsp_reference',
            'functions',
            'functions',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a FunctionsCarriedOutWsp reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.functions.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'functions_carried_out_wsp_reference',
            'functions',
            'functions',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist FunctionsCarriedOutWsp.
     *
     * @throws \Exception
     */
    public function testInsertFunctionsCarriedOutWspDataFailFunctionsCarriedOutWspNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[functions_carried_out_wsp_reference] Field "field_functions" have a wrong reference: "01FHWG6F44KF2545GDEFQTQ0VA".');

        // Create form.
        $form = $this->formFactory->create('test.functions.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_functions' => new FunctionsCarriedOutWsp('01FHWG6F44KF2545GDEFQTQ0VA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertFunctionsCarriedOutWspDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.functions.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );
    }

    /**
     * Test insert wrong FunctionsCarriedOutWsp.
     *
     * @throws \Exception
     */
    public function testInsertFunctionsCarriedOutWspDataFailBadFunctionsCarriedOutWsp(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[functions_carried_out_wsp_reference] Field "field_functions" have a reference, "01G3BEJ5S77AYEWMGWGHX011B3", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.functions.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );
    }

    /**
     * Test that we can insert a FunctionsCarriedOutWsp in a form.
     *
     * @throws \Exception
     */
    public function testInsertFunctionsCarriedOutWspDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.functions.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist FunctionsCarriedOutWsp.
     *
     * @throws \Exception
     */
    public function testUpdateFunctionsCarriedOutWspDataFailFunctionsCarriedOutWspNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.functions.udata.divnotfound] errors: 
[functions_carried_out_wsp_reference] Field &quot;field_functions&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.functions.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );

        $record = $form->find($id);
        $record->{'field_functions'} = new FunctionsCarriedOutWsp('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateFunctionsCarriedOutWspDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.functions.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.functions.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong FunctionsCarriedOutWsp.
     *
     * @throws \Exception
     */
    public function testUpdateFunctionsCarriedOutWspDataFailBadFunctionsCarriedOutWsp(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.functions.udata.baddiv] errors: 
[functions_carried_out_wsp_reference] Field &quot;field_functions&quot; have a reference, &quot;01G3BEJ5S77AYEWMGWGHX011B3&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.functions.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a FunctionsCarriedOutWsp in a form.
     *
     * @throws \Exception
     */
    public function testUpdateFunctionsCarriedOutWspDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.functions.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $functionsRepo = $this->entityManager->getRepository(FunctionsCarriedOutWsp::class);
        $functions = $functionsRepo->find('01G3BEJ5S77AYEWMGWGHX011B3');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_functions' => $functions,
            ]
        );

        $record = $form->find($id);
        $functions = $functionsRepo->find('01G3BEJ0GVNCXRJH3RJSRJHW2V');
        $record->{'field_functions'} = $functions;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G3BEJ0GVNCXRJH3RJSRJHW2V', $record->get('field_functions')['value']);
    }

    /**
     * Test that we can generate a FunctionsCarriedOutWsp example data.
     */
    public function testFunctionsCarriedOutWspExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.functions.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'functions', 'functions', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_functions');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_functions'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.carriedoutwsp.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.carriedoutwsp.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('functions_carried_out_wsp_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
