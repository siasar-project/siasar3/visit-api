<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\FunderIntervention;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeFunderInterventionEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FunderIntervention reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_funderintervention" require a "country_reference" field in form "form.test.funderintervention.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.funderintervention.country.fail.empty');
        // Try to add the field type.
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a funder_intervention_reference reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_funderintervention" require "country" field in form "form.test.funderintervention.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.funderintervention.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'funder_intervention_reference',
            'funderintervention',
            'funderintervention',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a funder_intervention reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.funderintervention.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'funder_intervention_reference',
            'funderintervention',
            'funderintervention',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist funder_intervention.
     *
     * @throws \Exception
     */
    public function testInsertFunderInterventionDataFailFunderInterventionNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[funder_intervention_reference] Field "funderintervention [field_funderintervention]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.funderintervention.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => new FunderIntervention('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertFunderInterventionDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.funderintervention.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );
    }

    /**
     * Test insert wrong funder_intervention.
     *
     * @throws \Exception
     */
    public function testInsertFunderInterventionDataFailBadFunderIntervention(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[funder_intervention_reference] Field "funderintervention [field_funderintervention]" have a reference, "01FHR00SHAEM8DEMPDY1QVNYH4", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.funderintervention.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );
    }

    /**
     * Test that we can insert a funder_intervention in a form.
     *
     * @throws \Exception
     */
    public function testInsertFunderInterventionDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.funderintervention.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist funderintervention.
     *
     * @throws \Exception
     */
    public function testUpdateFunderInterventionDataFailFunderInterventionNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.funderintervention.udata.divnotfound] errors: 
[funder_intervention_reference] Field &quot;funderintervention [field_funderintervention]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.funderintervention.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );

        $record = $form->find($id);
        $record->{'field_funderintervention'} = new FunderIntervention('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateFunderInterventionDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.funderintervention.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.funderintervention.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong funderintervention.
     *
     * @throws \Exception
     */
    public function testUpdateFunderInterventionDataFailBadFunderIntervention(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.funderintervention.udata.baddiv] errors: 
[funder_intervention_reference] Field &quot;funderintervention [field_funderintervention]&quot; have a reference, &quot;01FHR00SHAEM8DEMPDY1QVNYH4&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.funderintervention.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a funderintervention in a form.
     *
     * @throws \Exception
     */
    public function testUpdateFunderInterventionDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.funderintervention.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $funderInterventionRepo = $this->entityManager->getRepository(FunderIntervention::class);
        $funderIntervention = $funderInterventionRepo->find('01FHR00SHAEM8DEMPDY1QVNYH4');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_funderintervention' => $funderIntervention,
            ]
        );

        $record = $form->find($id);
        $funderIntervention = $funderInterventionRepo->find('01FENEA1F0QK031D3ZB6T4P97V');
        $record->{'field_funderintervention'} = $funderIntervention;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FENEA1F0QK031D3ZB6T4P97V', $record->get('field_funderintervention')['value']);
    }

    /**
     * Test that we can generate a funderintervention example data.
     */
    public function testFunderInterventionExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.funderintervention.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'funderintervention', 'funderintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_funderintervention');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_funderintervention'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.funderintervention.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.funderintervention.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('funder_intervention_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
