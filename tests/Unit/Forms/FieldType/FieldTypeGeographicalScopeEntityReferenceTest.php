<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\GeographicalScope;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeGeographicalScopeEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeGeographicalScopeEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_scope" require a "country_reference" field in form "form.test.scope.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.scope.country.fail.empty');
        // Try to add the field type.
        $form->addField('geographical_scope_reference', 'scope', 'scope');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a geographical_scope reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_scope" require "country" field in form "form.test.scope.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.scope.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'geographical_scope_reference',
            'scope',
            'scope',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a geographical scope reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.scope.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'geographical_scope_reference',
            'scope',
            'scope',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist scope.
     *
     * @throws \Exception
     */
    public function testInsertScopeDataFailScopeNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[geographical_scope_reference] Field "field_scope" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.scope.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_scope' => new GeographicalScope('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertScopeDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.scope.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );
    }

    /**
     * Test insert wrong scope.
     *
     * @throws \Exception
     */
    public function testInsertScopeDataFailBadScope(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[geographical_scope_reference] Field "field_scope" have a reference, "01G2MJ1AQ7WV10E96SF9SC3DMQ", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.scope.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );
    }

    /**
     * Test that we can insert a scope in a form.
     *
     * @throws \Exception
     */
    public function testInsertScopeDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.scope.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist scope.
     *
     * @throws \Exception
     */
    public function testUpdateScopeDataFailScopeNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.scope.updata.notfound] errors: 
[geographical_scope_reference] Field &quot;field_scope&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.scope.updata.notfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );

        $record = $form->find($id);
        $record->{'field_scope'} = [
            'value' => '01FFHQPYMYKY76RY6EDKR271EZ',
            'class' => GeographicalScope::class,
        ];
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateScopeDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.scope.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.scope.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong scope.
     *
     * @throws \Exception
     */
    public function testUpdateScopeDataFailBadScope(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.scope.udata.baddiv] errors: 
[geographical_scope_reference] Field &quot;field_scope&quot; have a reference, &quot;01G2MJ1AQ7WV10E96SF9SC3DMQ&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.scope.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a scope in a form.
     *
     * @throws \Exception
     */
    public function testUpdateScopeDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.scope.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $scopeRepo = $this->entityManager->getRepository(GeographicalScope::class);
        /** @var GeographicalScope $scope */
        $scope = $scopeRepo->find('01G2MJ1AQ7WV10E96SF9SC3DMQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_scope' => $scope,
            ]
        );

        $record = $form->find($id);
        /** @var GeographicalScope $scope */
        $scope = $scopeRepo->find('01G2MJ1G49729QPKS7CV3F5FGK');
        $record->{'field_scope'} = [
            'value' => $scope->getId(),
            'class' => GeographicalScope::class,
        ];
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2MJ1G49729QPKS7CV3F5FGK', $record->get('field_scope')['value']);
    }

    /**
     * Test that we can generate a scope example data.
     */
    public function testScopeExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.scope.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'scope', 'scope', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_scope');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_scope'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.geographical.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.geographical.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('geographical_scope_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
