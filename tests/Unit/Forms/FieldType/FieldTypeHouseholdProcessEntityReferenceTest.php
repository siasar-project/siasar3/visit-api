<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodriguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeHouseholdProcessEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeHouseholdProcessEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_household_process" require a "country_reference" field in form "form.test.householdprocess.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.householdprocess.country.fail.empty');
        // Try to add the field type.
        $form->addField('household_process_reference', 'household_process', 'household_process');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a household process reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_household_process" require "country" field in form "form.test.householdprocess.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.householdprocess.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'household_process_reference',
            'household_process',
            'household_process',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a household process reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.householdprocess.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'household_process_reference',
            'household_process',
            'household_process',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist household process.
     *
     * @throws \Exception
     */
    public function testInsertHouseholdProcessDataFailHouseholdProcessNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[household_process_reference] Field "field_household_process" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.householdprocess.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => new HouseholdProcess('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertHouseholdProcessDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.householdprocess.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );
    }

    /**
     * Test insert wrong household process.
     *
     * @throws \Exception
     */
    public function testInsertHouseholdProcessDataFailBadHouseholdProcess(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[household_process_reference] Field "field_household_process" have a reference, "01G6WNZ83K89S4TCMCB1FD8MWZ", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.householdprocess.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );
    }

    /**
     * Test that we can insert a household process in a form.
     *
     * @throws \Exception
     */
    public function testInsertHouseholdProcessDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.householdprocess.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist household process.
     *
     * @throws \Exception
     */
    public function testUpdateHouseholdProcessDataFailHouseholdProcessNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.householdprocess.udata.divnotfound] errors: 
[household_process_reference] Field &quot;field_household_process&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.householdprocess.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );

        $record = $form->find($id);
        $record->{'field_household_process'} = new HouseholdProcess('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateHouseholdProcessDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.householdprocess.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.householdprocess.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong household process.
     *
     * @throws \Exception
     */
    public function testUpdateHouseholdProcessDataFailBadHouseholdProcess(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.householdprocess.udata.baddiv] errors: 
[household_process_reference] Field &quot;field_household_process&quot; have a reference, &quot;01G6WNZ83K89S4TCMCB1FD8MWZ&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.householdprocess.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a household process in a form.
     *
     * @throws \Exception
     */
    public function testUpdateHouseholdProcessDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.householdprocess.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $householdProcess = $householdProcessRepo->find('01G6WNZ83K89S4TCMCB1FD8MWZ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_household_process' => $householdProcess,
            ]
        );

        $record = $form->find($id);
        $householdProcess = $householdProcessRepo->find('01G6WP0CM5RV3SJQG2P3HKNCRJ');
        $record->{'field_household_process'} = $householdProcess;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G6WP0CM5RV3SJQG2P3HKNCRJ', $record->get('field_household_process')['value']);
    }

    /**
     * Test that we can generate a household process example data.
     */
    public function testHouseholdProcessExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.householdprocess.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'household_process', 'household_process', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_household_process');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_household_process'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.householdprocess.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.householdprocess.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('household_process_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
