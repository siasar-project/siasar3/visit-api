<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Repository\ConfigurationRepository;
use App\Tests\Functional\OpenApi\ApiTestBase;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;

/**
 * Test inquiring_status fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeInquiringStatusTest extends ApiTestBase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an inquiring_status field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallInquiringStatusInForm(): void
    {
        $form = $this->formFactory->create('test.inquiring.status.fieldtype');
        // Try to add the trust field type.
        $form->addField('inquiring_status', 'status', 'Status');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_status', $columns);
        $this->assertEquals('text', $columns["field_status"]->getType()->getName());
    }

    /**
     * Test that we can insert an inquiring_status value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertInquiringStatusInForm(): void
    {
        $form = $this->formFactory->create('test.insert.inquiring.status');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('inquiring_status', 'status', 'Status');
        $form->saveNow();
        $form->install();

        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_status' => [
                    'value' => 'draft',
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $dbValue = $record->{'field_status'};
        $this->assertEquals('draft', $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_status');
        $this->assertEquals('draft', $dbValue);

        // Insert by instance.
        $form->insert(
            [
                'field_reference' => 2,
                'field_status' => 'draft',
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $dbValue = $record->{'field_status'};
        $this->assertEquals('draft', $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_status');
        $this->assertEquals('draft', $dbValue);

        // Insert default.
        $form->insert(
            [
                'field_reference' => 3,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 3]);
        // Read field how target format.
        $dbValue = $record->{'field_status'};
        $this->assertEquals('draft', $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_status');
        $this->assertEquals('draft', $dbValue);
    }

    /**
     * Test that we can do a valid status change.
     *
     * @return void
     */
    public function testCanChangeStatus(): void
    {
        $this->loginUser('pedro');
        $form = $this->formFactory->create('test.allow.inquiring.status');
        $form->addField('inquiring_status', 'status', 'status');
        $form->saveNow();
        $form->install();

        // Insert with default value.
        $id = $form->insert([]);
        // Change record status.
        $record = $form->find($id);
        $record->{'field_status'} = 'finished';
        $record->save();
        // Verify the new status is saved.
        $record = $form->find($id);
        $this->assertEquals('finished', $record->{'field_status'});
    }

    /**
     * Test that we can complete status workflow.
     *
     * @return void
     */
    public function testCanCompleteInquiring(): void
    {
        $this->loginUser('pedro');
        $form = $this->formFactory->create('test.complete.inquiring.status');
        $form->addField('inquiring_status', 'status', 'status');
        $form->saveNow();
        $form->install();

        // Insert with default value.
        $id = $form->insert([]);
        $record = $form->find($id);


        // to finished
        $this->stepTestCanCompleteInquiring($form, $record, 'finished');
        // to draft
        $this->stepTestCanCompleteInquiring($form, $record, 'draft');
        // to finished, to validated
        $this->stepTestCanCompleteInquiring($form, $record, 'finished');
        $this->stepTestCanCompleteInquiring($form, $record, 'validated');
        // to draft
        $this->stepTestCanCompleteInquiring($form, $record, 'draft');
        // to finished, to validated, to locked
        $this->stepTestCanCompleteInquiring($form, $record, 'finished');
        $this->stepTestCanCompleteInquiring($form, $record, 'validated');
        $this->stepTestCanCompleteInquiring($form, $record, 'locked');
        // new route: draft.
        $id = $form->insert([]);
        $record = $form->find($id);
        // to finished, to validated, to removed
        $this->stepTestCanCompleteInquiring($form, $record, 'finished');
        $this->stepTestCanCompleteInquiring($form, $record, 'validated');
        $this->stepTestCanCompleteInquiring($form, $record, 'removed');
    }

    /**
     * Test that we can't do a invalid status change.
     *
     * @return void
     */
    public function testCantChangeStatus(): void
    {
        $form = $this->formFactory->create('test.allow.change.status');
        $form->addField('inquiring_status', 'status', 'status');
        $form->saveNow();
        $form->install();

        // Insert with default value.
        $id = $form->insert([]);
        // Set expected exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.allow.change.status] errors: 
[inquiring_status\&quot;status [field_status]&quot;] This form can&#039;t change to &quot;locked&quot; from &quot;draft&quot; status. Allowed values are: finished');
        // Change record status.
        $record = $form->find($id);
        $record->{'field_status'} = 'locked';
        $record->save();
    }

    /**
     * Test that we can't create an multivalued inquiring_status field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInquiringStatusMultivaluedFieldInForm(): void
    {
        // Set expected exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('An "inquiring_status" field type can\'t be multivalued.');
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.g1');
        $form->addField('inquiring_status', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.inquiringstatus.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('inquiring_status', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData([]);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.inquiringstatus.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('inquiring_status', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        // This field can't be empty.
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Perform status transition and verify that is applied.
     *
     * @param FormManagerInterface $form    Form manager.
     * @param FormRecord           $record  Record.
     * @param string               $toPlace Destination place.
     *
     * @return void
     */
    protected function stepTestCanCompleteInquiring(FormManagerInterface $form, FormRecord $record, string $toPlace): void
    {
        // Change status.
        $record->{'field_status'} = $toPlace;
        $record->save();

        // Verify the new status is saved.
        $validationRecord = $form->find($record->getId());
        $this->assertEquals($toPlace, $validationRecord->{'field_status'});
    }
}
