<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\OfficialLanguage;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeLanguageEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a Language reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_language" require a "country_reference" field in form "form.test.language.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.language.country.fail.empty');
        // Try to add the field type.
        $form->addField('language_reference', 'language', 'language');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a language reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_language" require "country" field in form "form.test.language.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.language.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'language_reference',
            'language',
            'language',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a language reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.language.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'language_reference',
            'language',
            'language',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist language.
     *
     * @throws \Exception
     */
    public function testInsertLanguageDataFailLanguageNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[language_reference] Field "field_language" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.language.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_language' => new OfficialLanguage('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertLanguageDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.language.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );
    }

    /**
     * Test insert wrong language.
     *
     * @throws \Exception
     */
    public function testInsertLanguageDataFailBadLanguage(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[language_reference] Field "field_language" have a reference, "01FH5AHS6A6NC4Q1NT6TNJ3P3G", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.language.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );
    }

    /**
     * Test that we can insert a language in a form.
     *
     * @throws \Exception
     */
    public function testInsertLanguageDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.language.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist language.
     *
     * @throws \Exception
     */
    public function testUpdateLanguageDataFailLanguageNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.language.udata.divnotfound] errors: 
[language_reference] Field &quot;field_language&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.language.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );

        $record = $form->find($id);
        $record->{'field_language'} = new OfficialLanguage('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateLanguageDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.language.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.language.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong language.
     *
     * @throws \Exception
     */
    public function testUpdateLanguageDataFailBadLanguage(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.language.udata.baddiv] errors: 
[language_reference] Field &quot;field_language&quot; have a reference, &quot;01FH5AHS6A6NC4Q1NT6TNJ3P3G&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.language.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a language in a form.
     *
     * @throws \Exception
     */
    public function testUpdateLanguageDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.language.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $languageRepo = $this->entityManager->getRepository(OfficialLanguage::class);
        $language = $languageRepo->find('01FH5AHS6A6NC4Q1NT6TNJ3P3G');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_language' => $language,
            ]
        );

        $record = $form->find($id);
        $language = $languageRepo->find('01FH5AGXZR8H9E7EVKBA93D4W3');
        $record->{'field_language'} = $language;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FH5AGXZR8H9E7EVKBA93D4W3', $record->get('field_language')['value']);
    }

    /**
     * Test that we can generate a language example data.
     */
    public function testLanguageExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.language.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'language', 'language', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_language');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_language'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.language.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.language.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('language_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
