<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test mail fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeMailTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an mail field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallMailInForm(): void
    {
        $form = $this->formFactory->create('test.mail.fieldtype');
        // Try to add the trust field type.
        $form->addField('mail', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_text', $columns);
        $this->assertEquals('text', $columns["field_text"]->getType()->getName());
    }

    /**
     * Test that we can insert an mail value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMailInForm(): void
    {
        $form = $this->formFactory->create('test.insert.mail');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('mail', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $text = 'aaaaa976@gmail.com';
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'value' => $text,
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $dbValue = $record->{'field_text'};
        $this->assertEquals($text, $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_text');
        $this->assertEquals($text, $dbValue);

        // Insert by instance.
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_text' => $text,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $dbValue = $record->{'field_text'};
        $this->assertEquals($text, $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_text');
        $this->assertEquals($text, $dbValue);
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMailMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.mail');
        $manager->addField('mail', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.mail');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMailMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.mail');
        $manager->addField('mail', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.mail');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMailMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.mail');
        $form->addField('mail', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.mail');
        $mail = 'aaaaa976@gmail.com';
        $recordId = $form->insert(
            [
                'field_reference' => [$mail, $mail, $mail, $mail, $mail],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [$mail, $mail, $mail];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMailMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.mail');
        $form->addField('mail', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('mail', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.mail');
        $mail = 'aaaaa976@gmail.com';
        $recordId = $form->insert(
            [
                'field_single' => $mail,
                'field_reference' => [$mail, $mail, $mail, $mail, $mail],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 'pedro@front.id';
        $record->{'field_reference'} = [$mail, $mail, $mail];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals('pedro@front.id', $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        // $index + 1 must be the field_reference value.
        // See $form->insert some lines before.
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMailMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.mail');
        $form->addField('mail', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('mail', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.mail');
        $mail = 'aaaaa976@gmail.com';
        $recordId = $form->insert(
            [
                'field_single' => $mail,
                'field_reference' => [$mail, $mail, $mail, $mail, $mail],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals($mail, $record->{'field_single'});
        // Read end value.
        $aux = [$mail, $mail, $mail, $mail, $mail];
        foreach ($record->{'field_reference'} as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($aux[$key], $item);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($aux[$key], $item);
        }
    }

    /**
     * Test that we can't insert an invalid mail.
     *
     * @return void
     */
    public function testInsertInvalidMail():void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.invalid.mail');
        $form->addField('mail', 'single', 'Single field');
        $form->saveNow();
        $form->install();
        // Prepate exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[mail] Field "Single field [field_single]" have not a valid mail: "aaaaa976-gmail.com".');
        // Create a new record.
        $form = $this->formFactory->find('test.invalid.mail');
        $mail = 'aaaaa976-gmail.com';
        $form->insert(
            [
                'field_single' => $mail,
            ]
        );
    }

    /**
     * Test that we can't update with an invalid mail.
     *
     * @return void
     */
    public function testUpdateInvalidMail():void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.update.invalid.mail');
        $form->addField('mail', 'single', 'Single field');
        $form->saveNow();
        $form->install();
        // Create a new record.
        $form = $this->formFactory->find('test.update.invalid.mail');
        // Insert.
        $id = $form->insert(
            [
                'field_single' => 'aaaaa976@gmail.com',
            ]
        );
        // Prepate exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.update.invalid.mail] errors: 
[mail] Field &quot;Single field [field_single]&quot; have not a valid mail: &quot;aaaaa976-gmail.com&quot;.');
        // Update.
        $record = $form->find($id);
        $record->{'field_single'} = 'aaaaa976-gmail.com';
        $record->save();
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.mail.notempty');
        $form->addField('mail', 'reference', 'cs');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.mail.empty');
        $form->addField('mail', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
