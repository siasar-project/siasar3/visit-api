<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManager;
use App\Forms\FormRecord;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test Map Point fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeMapPointTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an Map Point field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallMapPointInForm(): void
    {
        $form = $this->formFactory->create('test.mapp.fieldtype');
        // Try to add the trust field type.
        $this->addFormFields($form);
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayNotHasKey('field_mp', $columns);
    }

    /**
     * Test that we can't insert an Map Point value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMapPointInFormIgnored(): void
    {
        $form = $this->formFactory->create('test.insert.mapp');
        // Try to add the trust field type.
        $this->addFormFields($form);
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $form->insert(
            [
                'field_mp' => ['value' => 1],
            ]
        );
        // The map point field will be ignored updating or inserting.
        $doctrine = self::$container->get('database_connection');
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayNotHasKey('field_mp', $columns);
    }

    /**
     * Test that we can insert an Map Point value to autogenerate.
     *
     * The map point must be ignored, but it will have LAT/LON values.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAutoMapPointInForm(): void
    {
        $form = $this->formFactory->create('test.insert.auto.mapp');
        // Try to add the trust field type.
        $this->addFormFields($form);
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $recordId = $form->insert([]);
        /** @var FormRecord $record */
        $record = $form->find($recordId);
        // Read field how target format.
        $dbValue = $record->{'field_mp'};
        $this->assertNotNull($dbValue);
    }

    /**
     * Test that we can't add an multivalued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInFormFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('An "map_point" field type can\'t be multivalued.');

        $manager = $this->formFactory->create('test.multivalued.mapp');
        $this->addFormFields($manager);
        $manager->addField(
            'map_point',
            'mps',
            'Map Points',
            [
                'multivalued' => true,
                'lat_field' => 'field_location_lat',
                'lon_field' => 'field_location_lon',
            ]
        );
        $manager->saveNow();
        $manager->install();
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.mappoint.notempty');
        $this->addFormFields($form);
        $form->addField(
            'map_point',
            'reference',
            'Map Points',
            [
                'lat_field' => 'field_location_lat',
                'lon_field' => 'field_location_lon',
            ]
        );
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.mappoint.notempty');
        $this->addFormFields($form);
        $form->addField(
            'map_point',
            'reference',
            'Map Points',
            [
                'lat_field' => 'field_location_lat',
                'lon_field' => 'field_location_lon',
            ]
        );
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        // This field can't be empty.
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Add Map Point field with required decimal fields.
     *
     * @param FormManager $form
     *
     * @return void
     *
     * @throws \Exception
     */
    protected function addFormFields(FormManager $form): void
    {
        $form->addField(
            'decimal',
            'location_lon',
            'location_lon'
        );
        $form->addField(
            'decimal',
            'location_lat',
            'location_lat'
        );
        $form->addField(
            'map_point',
            'mp',
            'Map point',
            [
                'lat_field' => 'field_location_lat',
                'lon_field' => 'field_location_lon',
            ]
        );
    }
}
