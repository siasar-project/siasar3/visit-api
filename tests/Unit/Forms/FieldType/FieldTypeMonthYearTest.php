<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test month and year fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeMonthYearTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an month_year field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallMonthYearInForm(): void
    {
        $form = $this->formFactory->create('test.my.fieldtype');
        // Try to add the trust field type.
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_text_month', $columns);
        $this->assertArrayHasKey('field_text_year', $columns);
        $this->assertEquals('integer', $columns["field_text_month"]->getType()->getName());
        $this->assertEquals('integer', $columns["field_text_year"]->getType()->getName());
    }

    /**
     * Test that we can insert n month_year value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInForm(): void
    {
        $form = $this->formFactory->create('test.insert.my');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'month' => 10,
                    'year' => 2021,
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $dbValue = $record->{'field_text'};
        $this->assertEquals(10, $dbValue['month']);
        $this->assertEquals(2021, $dbValue['year']);
        // Read field how properties array.
        $dbValue = $record->get('field_text');
        $this->assertEquals(10, $dbValue['month']);
        $this->assertEquals(2021, $dbValue['year']);
    }

    /**
     * Test that month is required property.
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInFormNoMonth(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.insert.'.$fid);
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[month_year] Field "text [field_text]" is uncompleted. "month" or "year" is empty.');
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'year' => 2021,
                ],
            ]
        );
    }

    /**
     * Test that year is required property.
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInFormNoYear(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.insert.'.$fid);
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[month_year] Field "text [field_text]" is uncompleted. "month" or "year" is empty.');
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'month' => 10,
                ],
            ]
        );
    }

    /**
     * Test min month value.
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInFormMinMonth(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.insert.'.$fid);
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[month_year] Wrong month, "-1", in field "text [field_text]". Values must be between 1 and 12.');
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'month' => -1,
                    'year' => 2021,
                ],
            ]
        );
    }

    /**
     * Test max month value.
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInFormMaxMonth(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.insert.'.$fid);
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[month_year] Wrong month, "13", in field "text [field_text]". Values must be between 1 and 12.');
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'month' => 13,
                    'year' => 2021,
                ],
            ]
        );
    }

    /**
     * Test min year value.
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInFormMinYear(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.insert.'.$fid);
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[month_year] Wrong year, "1899", in field "text [field_text]". Values must be between 1900 and 9999.');
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'month' => 1,
                    'year' => 1899,
                ],
            ]
        );
    }

    /**
     * Test max year value.
     *
     * @throws \Exception
     */
    public function testInsertMonthYearInFormMaxYear(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.insert.'.$fid);
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('month_year', 'text', 'text');
        $form->saveNow();
        $form->install();
        // Asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[month_year] Wrong year, "10000", in field "text [field_text]". Values must be between 1900 and 9999.');
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'month' => 1,
                    'year' => 10000,
                ],
            ]
        );
    }

    /**
     * Test that we can add a multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMonthYearMultiValuedFieldInForm(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.multivalued.'.$fid);
        $form->addField('month_year', 'text', 'text', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Verify that we have a multivalued settings in the field.
        $form = $this->formFactory->find('test.multivalued.'.$fid);
        $fields = $form->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_text_month', $tableData->getColumns());
        $this->assertArrayNotHasKey('field_text_year', $tableData->getColumns());

        // Verify that the multivalued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_text_month', $tableData->getColumns());
        $this->assertArrayHasKey('field_text_year', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall a form with multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMonthYearMultiValuedFieldInForm(): void
    {
        $fid = uniqid();
        $form = $this->formFactory->create('test.multivalued.uninstall.'.$fid);
        $form->addField('month_year', 'text', 'text', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        $form = $this->formFactory->find('test.multivalued.uninstall.'.$fid);
        $formTableName = $form->getTableName();

        $fields = $form->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $form->uninstall();

        // Verify that the multivalued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMonthYearMultivaluedFieldInFormRecord(): void
    {
        $fid = uniqid();
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.with.multi.'.$fid);
        $form->addField('month_year', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.with.multi.'.$fid);
        $options = [
            ['month' => 1, 'year' => 2021],
            ['month' => 2, 'year' => 2021],
            ['month' => 3, 'year' => 2021],
            ['month' => 4, 'year' => 2021],
            ['month' => 5, 'year' => 2021],
        ];
        $recordId = $form->insert(['field_reference' => $options]);
        // Update the field value.
        $record = $form->find($recordId);
        $options = [
            ['month' => 6, 'year' => 2021],
            ['month' => 7, 'year' => 2021],
            ['month' => 8, 'year' => 2021],
        ];
        $record->{'field_reference'} = $options;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_month', $item);
            $this->assertIsNumeric($item['field_reference_month']);
            $this->assertEquals($options[$index]['month'], $item['field_reference_month']);

            $this->assertArrayHasKey('field_reference_year', $item);
            $this->assertIsNumeric($item['field_reference_year']);
            $this->assertEquals($options[$index]['year'], $item['field_reference_year']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMonthYearMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        $fid = uniqid();
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.single.multivalued.'.$fid);
        $form->addField('month_year', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('month_year', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.single.multivalued.'.$fid);
        $options = [
            ['month' => 1, 'year' => 2021],
            ['month' => 2, 'year' => 2021],
            ['month' => 3, 'year' => 2021],
            ['month' => 4, 'year' => 2021],
            ['month' => 5, 'year' => 2021],
        ];
        $recordId = $form->insert(
            [
                'field_single' => ['month' => 6, 'year' => 2021],
                'field_reference' => $options,
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = ['month' => 12, 'year' => 2021];
        $options = [
            ['month' => 6, 'year' => 2021],
            ['month' => 7, 'year' => 2021],
            ['month' => 8, 'year' => 2021],
        ];
        $record->{'field_reference'} = $options;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single_month', $result[0]);
        $this->assertArrayHasKey('field_single_year', $result[0]);
        $this->assertEquals(12, $result[0]['field_single_month']);
        $this->assertEquals(2021, $result[0]['field_single_year']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_month', $item);
            $this->assertIsNumeric($item['field_reference_month']);
            $this->assertEquals($options[$index]['month'], $item['field_reference_month']);

            $this->assertArrayHasKey('field_reference_year', $item);
            $this->assertIsNumeric($item['field_reference_year']);
            $this->assertEquals($options[$index]['year'], $item['field_reference_year']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMonthYearMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        $fid = uniqid();
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.multivalued.'.$fid);
        $form->addField('month_year', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('month_year', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.multivalued.'.$fid);
        $options = [
            ['month' => 1, 'year' => 2021],
            ['month' => 2, 'year' => 2021],
            ['month' => 3, 'year' => 2021],
            ['month' => 4, 'year' => 2021],
            ['month' => 5, 'year' => 2021],
        ];
        $recordId = $form->insert(
            [
                'field_single' => ['month' => 6, 'year' => 2021],
                'field_reference' => $options,
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(['month' => 6, 'year' => 2021], $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $key => $item) {
            $this->assertIsNumeric($item['month']);
            $this->assertEquals($options[$key]['month'], $item['month']);
            $this->assertIsNumeric($item['year']);
            $this->assertEquals($options[$key]['year'], $item['year']);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertIsNumeric($item['month']);
            $this->assertEquals($options[$key]['month'], $item['month']);
            $this->assertIsNumeric($item['year']);
            $this->assertEquals($options[$key]['year'], $item['year']);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.monthyear.notempty');
        $form->addField('month_year', 'reference', 'cs');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.monthyear.empty');
        $form->addField('month_year', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        $isEmpty = $record->getFieldDefinition('field_reference')->isEmpty();
        $this->assertTrue($isEmpty);
    }
}
