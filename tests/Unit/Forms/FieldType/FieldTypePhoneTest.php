<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test phone fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypePhoneTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an phone field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallPhoneInForm(): void
    {
        $form = $this->formFactory->create('test.phone.fieldtype');
        // Try to add the trust field type.
        $form->addField('phone', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_name', $columns);
        $this->assertEquals('text', $columns["field_name"]->getType()->getName());
    }

    /**
     * Test that we can insert an phone value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertPhoneInForm(): void
    {
        $form = $this->formFactory->create('test.insert.phone');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('phone', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_name' => ['value' => '+212111111111'],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $phoneRef = $record->{'field_name'};
        $this->assertEquals('+212111111111', $phoneRef);
        // Read field how properties array.
        $phoneRef = $record->get('field_name');
        $this->assertEquals('+212111111111', $phoneRef);

        // Insert by instance.
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_name' => '+212111111111',
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $phoneRef = $record->{'field_name'};
        $this->assertEquals('+212111111111', $phoneRef);
        // Read field how properties array.
        $phoneRef = $record->get('field_name');
        $this->assertEquals('+212111111111', $phoneRef);
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedPhoneFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.phone');
        $manager->addField('phone', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.phone');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedPhoneFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.phone');
        $manager->addField('phone', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.phone');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedPhoneFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.multivalued.entity.reference.phone');
        $form->addField('phone', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.multivalued.entity.reference.phone');
        $phones = ['0311111111~3', '0311112222,5,9', '+212111111111', '+254711111111', '+27111111111'];
        $recordId = $form->insert(
            [
                'field_reference' => $phones,
            ]
        );
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertEquals($phones[$index], $item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedPhoneFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.phone');
        $form->addField('phone', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.phone');
        $phones = ['0311111111~3', '0311112222,5,9', '+212111111111', '+254711111111', '+27111111111'];
        $recordId = $form->insert(
            [
                'field_reference' => $phones,
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $phones = ['+212111111111', '+254711111111', '+27111111111'];
        $record->{'field_reference'} = $phones;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);

            $this->assertEquals($phones[$index], $item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedPhoneFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.phone');
        $form->addField('phone', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.phone');
        $phones = ['0311111111~3', '0311112222,5,9', '+212111111111', '+254711111111', '+27111111111'];
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => $phones,
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $phones = ['+212111111111', '+254711111111', '+27111111111'];
        $record->{'field_reference'} = $phones;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);

            $this->assertEquals($phones[$index], $item['field_reference']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldPhoneInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.phone');
        $form->addField('phone', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.phone');
        $phones = ['0311111111~3', '0311112222,5,9', '+212111111111', '+254711111111', '+27111111111'];
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => $phones,
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $index => $item) {
            $this->assertIsString($item);
            $this->assertEquals($phones[$index], $item);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $index => $item) {
            $this->assertEquals($phones[$index], $item);
            $this->assertIsString($item);
        }
    }

    /**
     * Data provider to testValidIdInConfigurationWrite.
     *
     * @return string[][]
     */
    public function validPhoneValues(): array
    {
        return [
            ['+2547 111 11111'],
            ['+212111111111'],
            ['0311111111~3'],
            ['0311112222,5,9'],
            ['+254711111111'],
            ['+27111111111'],
            ['+8675511112222'],
            ['11111111111'],
            ['4001111111'],
            ['80011111111'],
            ['119'],
            ['12122'],
            ['11112222'],
            ['1112222222'],
            ['1111122222'],
            ['+911111122222'],
        ];
    }

    /**
     * Test that we can write valid phone numbers.
     *
     * @param string $value Test value.
     *
     * @return void
     *
     * @throws \Exception
     *
     * @dataProvider validPhoneValues
     */
    public function testWriteValidPhonesInForm(string $value): void
    {
        $form = $this->formFactory->create('test.write.valid.phone');
        $form->addField('phone', 'phone', 'Phone');
        $form->saveNow();
        $form->install();
        $form->insert(['field_phone' => $value]);
        $this->expectNotToPerformAssertions();
    }

    /**
     * Data provider to testValidIdInConfigurationWrite.
     *
     * @return string[][]
     */
    public function invalidPhoneValues(): array
    {
        return [
            ['03-11111111~3'],
            ['031111a2222,5,9'],
            ['+21211A1111111'],
            ['AAAAA-BBBBB'],
        ];
    }

    /**
     * Test that we can't write invalid phone numbers.
     *
     * @param string $value Test value.
     *
     * @return void
     *
     * @throws \Doctrine\ORM\ORMException
     *
     * @dataProvider invalidPhoneValues
     */
    public function testWriteInvalidPhonesInForm(string $value): void
    {
        $form = $this->formFactory->create('test.write.invalid.phone');
        $form->addField('phone', 'phone', 'Phone');
        $form->saveNow();
        $form->install();

        $catch = false;
        try {
            $form->insert(['field_phone' => $value]);
        } catch (\Exception $e) {
            $catch = true;
            $this->assertInstanceOf(\Exception::class, $e);
            $this->assertEquals(
                sprintf(
                    '[phone] Field "Phone [field_phone]" have a invalid phone number, "%s", it must match "/^(\+)?[0-9 ,]*[,~0-9]{2,3}$/".',
                    $value
                ),
                $e->getMessage()
            );
        } finally {
            $this->assertTrue($catch, 'Mask accept the bad number "'.$value.'"');
        }
        $this->formFactory->delete('test-write-invalid-phone');
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.phone.notempty');
        $form->addField('phone', 'reference', 'cs');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.phone.empty');
        $form->addField('phone', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
