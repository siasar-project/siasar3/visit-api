<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\DefaultDiameter;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypePredominantDiameterTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a diameter reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_diameter" require a "country_reference" field in form "form.test.diameter.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.diameter.country.fail.empty');
        // Try to add the field type.
        $form->addField('default_diameter_reference', 'diameter', 'diameter');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add an diameter reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_diameter" require "country" field in form "form.test.diameter.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.diameter.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'default_diameter_reference',
            'diameter',
            'diameter',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add an diameter reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.diameter.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'default_diameter_reference',
            'diameter',
            'diameter',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist diameter.
     *
     * @throws \Exception
     */
    public function testInsertDiameterDataFailDiameterNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[default_diameter_reference] Field "field_diameter" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.diameter.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => new DefaultDiameter('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertDiameterDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.diameter.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $diametersRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diametersRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );
    }

    /**
     * Test insert wrong diameter.
     *
     * @throws \Exception
     */
    public function testInsertDiameterDataFailBadDiameter(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[default_diameter_reference] Field "field_diameter" have a reference, "01FK0CDCMPHJEY58ABTQPEYC5X", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.diameter.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $diameterRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diameterRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );
    }

    /**
     * Test that we can insert an diameter in a form.
     *
     * @throws \Exception
     */
    public function testInsertDiameterDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.diameter.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $diameterRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diameterRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist diameter.
     *
     * @throws \Exception
     */
    public function testUpdateDiameterDataFailDiameterNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.diameter.udata.divnotfound] errors: 
[default_diameter_reference] Field &quot;field_diameter&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.diameter.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $diameterRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diameterRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );

        $record = $form->find($id);
        $record->{'field_diameter'} = new DefaultDiameter('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateDiameterDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.diameter.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.diameter.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $diameterRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diameterRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong diameter.
     *
     * @throws \Exception
     */
    public function testUpdateDiameterDataFailBadDiameter(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.diameter.udata.baddiv] errors: 
[default_diameter_reference] Field &quot;field_diameter&quot; have a reference, &quot;01FK0CDCMPHJEY58ABTQPEYC5X&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.diameter.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $diameterRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diameterRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update an diameter in a form.
     *
     * @throws \Exception
     */
    public function testUpdateDiameterDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.diameter.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $diameterRepo = $this->entityManager->getRepository(DefaultDiameter::class);
        $diameter = $diameterRepo->find('01FK0CDCMPHJEY58ABTQPEYC5X');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_diameter' => $diameter,
            ]
        );

        $record = $form->find($id);
        $diameter = $diameterRepo->find('01FK0CDK44D0T1W0QEM0W0TCM2');
        $record->{'field_diameter'} = $diameter;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FK0CDK44D0T1W0QEM0W0TCM2', $record->get('field_diameter')['value']);
    }

    /**
     * Test that we can generate an diameter example data.
     */
    public function testDiameterExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.diameter.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'diameter', 'diameter', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_diameter');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_diameter'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.defaultdiameter.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.defaultdiameter.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('default_diameter_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
