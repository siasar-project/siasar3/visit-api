<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\ProgramIntervention;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeProgramEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a program reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_program" require a "country_reference" field in form "form.test.program.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.program.country.fail.empty');
        // Try to add the field type.
        $form->addField('program_reference', 'program', 'program');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a program reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_program" require "country" field in form "form.test.program.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.program.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'program_reference',
            'program',
            'program',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a program reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.program.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'program_reference',
            'program',
            'program',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist program.
     *
     * @throws \Exception
     */
    public function testInsertProgramDataFailProgramNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[program_reference] Field "field_program" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.program.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_program' => new ProgramIntervention('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertProgramDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.program.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );
    }

    /**
     * Test insert wrong program.
     *
     * @throws \Exception
     */
    public function testInsertProgramDataFailBadProgram(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[program_reference] Field "field_program" have a reference, "01FHWNDHY2XVCKXFB2DBBJXRWD", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.program.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );
    }

    /**
     * Test that we can insert a program in a form.
     *
     * @throws \Exception
     */
    public function testInsertProgramDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.program.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist program.
     *
     * @throws \Exception
     */
    public function testUpdateProgramDataFailProgramNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.program.udata.divnotfound] errors: 
[program_reference] Field &quot;field_program&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.program.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );

        $record = $form->find($id);
        $record->{'field_program'} = new ProgramIntervention('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateProgramDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.program.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.program.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong program.
     *
     * @throws \Exception
     */
    public function testUpdateProgramDataFailBadProgram(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.program.udata.baddiv] errors: 
[program_reference] Field &quot;field_program&quot; have a reference, &quot;01FHWNDHY2XVCKXFB2DBBJXRWD&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.program.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a program in a form.
     *
     * @throws \Exception
     */
    public function testUpdateProgramDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.program.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $programRepo = $this->entityManager->getRepository(ProgramIntervention::class);
        $program = $programRepo->find('01FHWNDHY2XVCKXFB2DBBJXRWD');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_program' => $program,
            ]
        );

        $record = $form->find($id);
        $program = $programRepo->find('01FENEDYFA6KNAA4J459P5S0RF');
        $record->{'field_program'} = $program;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RF', $record->get('field_program')['value']);
    }

    /**
     * Test that we can generate a program example data.
     */
    public function testProgramExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.program.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'program', 'program', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_program');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_program'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.program.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.program.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('program_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
