<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\PumpType;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypePumpTypeEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a PumpType reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_pumptype" require a "country_reference" field in form "form.test.pumptype.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.pumptype.country.fail.empty');
        // Try to add the field type.
        $form->addField('pump_type_reference', 'PumpType', 'pumptype');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a pumptype reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_pumptype" require "country" field in form "form.test.pumptype.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.pumptype.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'pump_type_reference',
            'pumptype',
            'pumptype',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a pumptype reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.pumptype.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'pump_type_reference',
            'pumptype',
            'pumptype',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist pumptype.
     *
     * @throws \Exception
     */
    public function testInsertPumpTypeDataFailPumpTypeNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[pump_type_reference] Field "pumptype [field_pumptype]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.pumptype.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => new PumpType('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertPumpTypeDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.pumptype.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $pumpTypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumpType = $pumpTypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumpType,
            ]
        );
    }

    /**
     * Test insert wrong pump type.
     *
     * @throws \Exception
     */
    public function testInsertPumpTypeDataFailBadPumpType(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[pump_type_reference] Field "pumptype [field_pumptype]" have a reference, "01FJYCWTPB91FFQMK7YVY495FW", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.pumptype.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $pumpTypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumpType = $pumpTypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumpType,
            ]
        );
    }

    /**
     * Test that we can insert a pumptype in a form.
     *
     * @throws \Exception
     */
    public function testInsertPumpTypeDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.pumptype.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $pumpTypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumptype = $pumpTypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumptype,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist pumptype.
     *
     * @throws \Exception
     */
    public function testUpdatePumpTypeDataFailPumpTypeNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.pumptype.udata.divnotfound] errors: 
[pump_type_reference] Field &quot;pumptype [field_pumptype]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.pumptype.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $pumptypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumptype = $pumptypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumptype,
            ]
        );

        $record = $form->find($id);
        $record->{'field_pumptype'} = new PumpType('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdatePumpTypeDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.pumptype.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.pumptype.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $pumptypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumptype = $pumptypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumptype,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong pump type.
     *
     * @throws \Exception
     */
    public function testUpdatePumpTypeDataFailBadPumpType(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.pumptype.udata.baddiv] errors: 
[pump_type_reference] Field &quot;pumptype [field_pumptype]&quot; have a reference, &quot;01FJYCWTPB91FFQMK7YVY495FW&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.pumptype.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $pumptypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumptype = $pumptypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumptype,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a pump type in a form.
     *
     * @throws \Exception
     */
    public function testUpdatePumpTypeDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.pumptype.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $pumptypeRepo = $this->entityManager->getRepository(PumpType::class);
        $pumptype = $pumptypeRepo->find('01FJYCWTPB91FFQMK7YVY495FW');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_pumptype' => $pumptype,
            ]
        );

        $record = $form->find($id);
        $pumptype = $pumptypeRepo->find('01FJYCWMCX38TSN8A2PWJ0SE83');
        $record->{'field_pumptype'} = $pumptype;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FJYCWMCX38TSN8A2PWJ0SE83', $record->get('field_pumptype')['value']);
    }

    /**
     * Test that we can generate a pump type example data.
     */
    public function testPumpTypeExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.pumptype.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'pumptype', 'pumptype', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_pumptype');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_pumptype'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.pumptype.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.pumptype.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('pump_type_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
