<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test radio boolean fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeRadioBooleanTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install a radio boolean field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallRadioBooleanInForm(): void
    {
        $form = $this->formFactory->create('test.radioboolean.fieldtype');
        // Try to add the trust field type.
        $form->addField('radio_boolean', 'bool', 'Boolean value');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_bool', $columns);
        $this->assertEquals('string', $columns["field_bool"]->getType()->getName());
        $this->assertEquals(1, $columns["field_bool"]->getLength());
    }

    /**
     * Test that we can insert a radio boolean value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertRadioBooleanInForm(): void
    {
        $form = $this->formFactory->create('test.insert.radioboolean');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('radio_boolean', 'bool', 'Boolean value');
        $form->saveNow();
        $form->install();

        // Set of boolean variants to test.
        // 'send' is the literal wrote in field.
        // 'read' is the expected read value.
        // Important: 'send' can't be array, by compatibility.
        $valueTests = [
            ['send' => true, 'read' => true],
            ['send' => false, 'read' => false],
            ['send' => new \stdClass(), 'read' => true],
            ['send' => 42, 'read' => true],
            ['send' => -42, 'read' => true],
            ['send' => 'true', 'read' => true],
            ['send' => 'on', 'read' => true],
            ['send' => 'off', 'read' => false],
            ['send' => 'yes', 'read' => true],
            ['send' => 'no', 'read' => false],
            ['send' => 'ja', 'read' => false],
            ['send' => 'nein', 'read' => false],
            ['send' => '1', 'read' => true],
            ['send' => null, 'read' => false],
            ['send' => 0, 'read' => false],
            ['send' => 'false', 'read' => false],
            ['send' => 'string', 'read' => false],
            ['send' => '0.0', 'read' => false],
            ['send' => '4.2', 'read' => false],
            ['send' => '0', 'read' => false],
            ['send' => '', 'read' => false],
        ];

        $testIndex = 1;
        foreach ($valueTests as $valueTest) {
            // Insert by internal properties, with TRUE.
            $form->insert(
                [
                    'field_reference' => ['value' => $testIndex],
                    'field_bool' => [
                        'value' => $valueTest['send'],
                    ],
                ]
            );
            $record = $form->findOneBy(['field_reference' => $testIndex]);
            // Read field how target format.
            $dbValue = $record->{'field_bool'};
            $this->assertEquals($valueTest['read'], $dbValue);
            // Read field how properties array.
            $dbValue = $record->get('field_bool');
            $this->assertEquals(($valueTest['read'] ? '1' : '0'), $dbValue);

            // Insert by instance, with TRUE.
            $form->insert(
                [
                    'field_reference' => ['value' => $testIndex + 1],
                    'field_bool' => $valueTest['send'],
                ]
            );
            $record = $form->findOneBy(['field_reference' => $testIndex + 1]);
            // Read field how target format.
            $dbValue = $record->{'field_bool'};
            $this->assertEquals($valueTest['read'], $dbValue);
            // Read field how properties array.
            $dbValue = $record->get('field_bool');
            $this->assertEquals(($valueTest['read'] ? '1' : '0'), $dbValue);

            // Update test index.
            $testIndex += 2;
        }
    }

    /**
     * Test that we can add a multivalued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.radioboolean');
        $manager->addField('radio_boolean', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.radioboolean');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.radioboolean');
        $manager->addField('radio_boolean', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.radioboolean');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.f11');
        $form->addField('radio_boolean', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.f11');
        $recordId = $form->insert(
            [
                'field_reference' => [true, false, true, false, true],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [false, true, false];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        // $index + 1 must be the field_reference value.
        // See $form->insert some lines before.
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
            $this->assertContainsEquals($item['field_reference'], ['0', '1']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.fa');
        $form->addField('radio_boolean', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('radio_boolean', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.fa');
        $recordId = $form->insert(
            [
                'field_single' => false,
                'field_reference' => [true, false, true, false, true],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = true;
        $record->{'field_reference'} = [false, true, false];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(true, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        // $index + 1 must be the field_reference value.
        // See $form->insert some lines before.
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
            $this->assertContainsEquals($item['field_reference'], ['0', '1']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.fb');
        $form->addField('radio_boolean', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('radio_boolean', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.fb');
        $values = [true, false, true, false, true];
        $recordId = $form->insert(
            [
                'field_single' => true,
                'field_reference' => $values,
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertTrue($record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $key => $item) {
            $this->assertIsBool($item);
            $this->assertEquals($values[$key], $item);
        }
        // Read internal values.
        $references = $record->get('field_reference');
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertIsString($item);
            $this->assertContainsEquals($item, ['0', '1']);
        }
    }

    /**
     * Test that boolean field labels can't be empty.
     */
    public function testBooleanLabelsCantBeEmpty()
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.empty.labels.boolean');
        $form->addField(
            'radio_boolean',
            'single',
            'Single field',
            [
                'true_label' => '',
                'false_label' => '',
            ]
        );
        $form->saveNow();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_single" require value labels.');

        $form->install();
    }

    /**
     * Test that boolean field labels can't be empty.
     */
    public function testBooleanLabelsCantBeEmpty2()
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.empty.labels.boolean');
        $form->addField(
            'radio_boolean',
            'single',
            'Single field',
            [
                'true_label' => ' ',
                'false_label' => 'No',
            ]
        );
        $form->saveNow();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_single" require value labels.');

        $form->install();
    }

    /**
     * Test that boolean field labels can't be empty.
     */
    public function testBooleanLabelsCantBeEmpty3()
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.empty.labels.boolean');
        $form->addField(
            'radio_boolean',
            'single',
            'Single field',
            [
                'true_label' => 'Yes',
                'false_label' => '',
            ]
        );
        $form->saveNow();

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_single" require value labels.');

        $form->install();
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.radioboolean.empty');
        $form->addField('radio_boolean', 'bool', 'Boolean value');
        $form->saveNow();
        $form->install();
        $recordId = $form->insert(['field_bool' => true]);
        $record = $form->find($recordId);
        $field = $record->getFieldDefinition('field_bool');
        $this->assertFalse($field->isEmpty());
    }
}
