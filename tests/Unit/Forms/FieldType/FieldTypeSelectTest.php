<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test select fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeSelectTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that a select field requires at least one option.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testSelectOptionsAreRequired(): void
    {
        $form = $this->formFactory->create('test.select.fieldtype');
        // Prepare exception capture.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The select field "text [field_text]" requires at least one option to select.');
        // Try to add the trust field type.
        $form->addField('select', 'text', 'text');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add options to a select field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testSelectWithOptions(): void
    {
        $form = $this->formFactory->create('test.select.options');
        $form->addField(
            'select',
            'text',
            'text',
            [
                'options' => [
                    1 => 'Opción 1',
                    2 => 'Opción 2',
                ],
            ]
        );
        $form->saveNow();
        // Retrieve configuration and verify options.
        $conf = $this->configuration->find('form.test.select.options');
        $values = $conf->getValue();
        $options = $values["fields"]["field_text"]["settings"]["options"];

        $this->assertCount(2, $options);

        $this->assertArrayHasKey(1, $options);
        $this->assertArrayHasKey(2, $options);

        $this->assertEquals('Opción 1', $options[1]);
        $this->assertEquals('Opción 2', $options[2]);
    }

    /**
     * Test that we can install an select field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallSelectInForm(): void
    {
        $form = $this->formFactory->create('test.select.fieldtype');
        // Try to add the trust field type.
        $form->addField('select', 'text', 'text', ['options' => [1 => 'front.id']]);
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_text', $columns);
        $this->assertEquals('text', $columns["field_text"]->getType()->getName());
    }

    /**
     * Test that we can insert an select value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertSelectInForm(): void
    {
        $form = $this->formFactory->create('test.insert.select');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('select', 'text', 'text', ['options' => [1 => 'front.id']]);
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $option = '1';
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_text' => [
                    'value' => $option,
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $dbValue = $record->{'field_text'};
        $this->assertEquals($option, $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_text');
        $this->assertEquals($option, $dbValue);

        // Insert by instance.
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_text' => $option,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $dbValue = $record->{'field_text'};
        $this->assertEquals($option, $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_text');
        $this->assertEquals($option, $dbValue);
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddSelectMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.select');
        $manager->addField(
            'select',
            'name',
            'name',
            [
                'multivalued' => true,
                'options' => [
                    1 => 'Blanco',
                    2 => 'Gris',
                    3 => 'Negro',
                ],
            ]
        );
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.select');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallSelectMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.select');
        $manager->addField(
            'select',
            'name',
            'name',
            [
                'multivalued' => true,
                'options' => ['a' => 'front.id'],
            ]
        );
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.select');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateSelectMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.select');
        $form->addField(
            'select',
            'Reference',
            'Record reference',
            [
                'multivalued' => true,
                'options' => [
                    'a' => 'Opción A',
                    'b' => 'Opción B',
                ],
            ]
        );
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.select');
        $options = ['a', 'b', 'a', 'b', 'a'];
        $recordId = $form->insert(['field_reference' => $options]);
        // Update the field value.
        $record = $form->find($recordId);
        $options = ['a', 'b', 'a'];
        $record->{'field_reference'} = $options;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
            $this->assertEquals($options[$index], $item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateSelectMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.select');
        $form->addField(
            'select',
            'Reference',
            'Record reference',
            [
                'multivalued' => true,
                'options' => [
                    1 => 'Symfony',
                    2 => 'Drupal',
                ],
            ]
        );
        $form->addField(
            'select',
            'single',
            'Single field',
            [
                'options' => [
                    1 => 'Symfony',
                    2 => 'Drupal',
                ],
            ]
        );
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.select');
        $options = [2, 1, 2, 1, 2];
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => $options,
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $options = [2, 1, 2];
        $record->{'field_reference'} = $options;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
            $this->assertEquals($options[$index], $item['field_reference']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadSelectMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.select');
        $form->addField(
            'select',
            'Reference',
            'Record reference',
            [
                'multivalued' => true,
                'options' => [
                    1 => 'Huelva',
                    2 => 'Almonte',
                ],
            ]
        );
        $form->addField(
            'select',
            'single',
            'Single field',
            [
                'options' => [
                    1 => 'Huelva',
                    2 => 'Almonte',
                ],
            ]
        );
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.select');
        $options = [2, 1, 2, 1, 2];
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => $options,
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($options[$key], $item);
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($options[$key], $item);
        }
    }

    /**
     * Test that we can't insert an invalid select option.
     *
     * @return void
     */
    public function testInsertInvalidOption():void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.invalid.select');
        $form->addField(
            'select',
            'single',
            'Single field',
            [
                'options' => [
                    1 => 'front.id',
                ],
            ]
        );
        $form->saveNow();
        $form->install();
        // Prepare exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[select] Field "Single field [field_single]" have a invalid option, "2". Options are: `1`, ``.');
        // Create a new record.
        $form = $this->formFactory->find('test.invalid.select');
        $form->insert(
            [
                'field_single' => 2,
            ]
        );
    }

    /**
     * Test that we can't update with an invalid select.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateInvalidSelect():void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.update.invalid.select');
        $form->addField(
            'select',
            'single',
            'Single field',
            [
                'options' => [
                    1 => 'Rocio grande',
                    2 => 'Rocio chico',
                ],
            ]
        );
        $form->saveNow();
        $form->install();
        // Create a new record.
        $form = $this->formFactory->find('test.update.invalid.select');
        // Insert.
        $id = $form->insert(
            [
                'field_single' => 1,
            ]
        );
        // Prepate exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.update.invalid.select] errors: 
[select] Field &quot;Single field [field_single]&quot; have a invalid option, &quot;3&quot;. Options are: `1`, `2`, ``.');
        // Update.
        $record = $form->find($id);
        $record->{'field_single'} = 3;
        $record->save();
    }

    /**
     * Test that we can add options and groups to a select field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testSelectWithOptionsGroups(): void
    {
        $form = $this->formFactory->create('test.select.optionsgroups');
        $form->addField(
            'select',
            'text',
            'text',
            [
                'options' => [
                    "_1" => 'Grupo 1',
                    1 => 'Opción 1.1',
                    2 => 'Opción 1.2',
                ],
            ]
        );
        $form->saveNow();
        // Retrieve configuration and verify options.
        $conf = $this->configuration->find('form.test.select.optionsgroups');
        $values = $conf->getValue();
        $options = $values["fields"]["field_text"]["settings"]["options"];

        $this->assertCount(3, $options);

        $this->assertArrayHasKey("_1", $options);
        $this->assertArrayHasKey(1, $options);
        $this->assertArrayHasKey(2, $options);

        $this->assertEquals('Grupo 1', $options["_1"]);
        $this->assertEquals('Opción 1.1', $options[1]);
        $this->assertEquals('Opción 1.2', $options[2]);
    }

    /**
     * Test that we can't add groups only to a select field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testSelectWithOptionsGroupsOnly(): void
    {
        $form = $this->formFactory->create('test.select.optionsgroups');
        $form->addField(
            'select',
            'text',
            'text',
            [
                'options' => [
                    "_1" => 'Grupo 1',
                ],
            ]
        );
        // Save form.
        $form->saveNow();

        // Prepare exception capture.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The select field "text [field_text]" requires at least one option to select.');
        $form->install();
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.select.notempty');
        $form->addField(
            'select',
            'reference',
            'text',
            [
                'options' => [
                    "_1" => 'Grupo 1',
                    1 => 'Opción 1.1',
                    2 => 'Opción 1.2',
                ],
            ]
        );
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.select.empty');
        $form->addField(
            'select',
            'reference',
            'text',
            [
                'options' => [
                    "_1" => 'Grupo 1',
                    1 => 'Opción 1.1',
                    2 => 'Opción 1.2',
                ],
            ]
        );
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
