<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\SpecialComponent;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeSpecialComponentEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a special_component reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_specialcomponent" require a "country_reference" field in form "form.test.specialcomponent.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.country.fail.empty');
        // Try to add the field type.
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a specialcomponent reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_specialcomponent" require "country" field in form "form.test.specialcomponent.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'special_component_reference',
            'specialcomponent',
            'specialcomponent',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a specialcomponent reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'special_component_reference',
            'specialcomponent',
            'specialcomponent',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist specialcomponent.
     *
     * @throws \Exception
     */
    public function testInsertSpecialComponentDataFailSpecialComponentNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[special_component_reference] Field "specialcomponent [field_specialcomponent]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => new SpecialComponent('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertSpecialComponentDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );
    }

    /**
     * Test insert wrong specialcomponent.
     *
     * @throws \Exception
     */
    public function testInsertSpecialComponentDataFailBadSpecialComponent(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[special_component_reference] Field "specialcomponent [field_specialcomponent]" have a reference, "01FK0PC11BQW337MQ44KB096T8", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );
    }

    /**
     * Test that we can insert a specialcomponent in a form.
     *
     * @throws \Exception
     */
    public function testInsertSpecialComponentDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist specialcomponent.
     *
     * @throws \Exception
     */
    public function testUpdateSpecialComponentDataFailSpecialComponentNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.specialcomponent.udata.divnotfound] errors: 
[special_component_reference] Field &quot;specialcomponent [field_specialcomponent]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );

        $record = $form->find($id);
        $record->{'field_specialcomponent'} = new SpecialComponent('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateSpecialComponentDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.specialcomponent.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong specialcomponent.
     *
     * @throws \Exception
     */
    public function testUpdateSpecialComponentDataFailBadSpecialComponent(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.specialcomponent.udata.baddiv] errors: 
[special_component_reference] Field &quot;specialcomponent [field_specialcomponent]&quot; have a reference, &quot;01FK0PC11BQW337MQ44KB096T8&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a specialcomponent in a form.
     *
     * @throws \Exception
     */
    public function testUpdateSpecialComponentDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $specialComponentRepo = $this->entityManager->getRepository(SpecialComponent::class);
        $specialComponent = $specialComponentRepo->find('01FK0PC11BQW337MQ44KB096T8');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_specialcomponent' => $specialComponent,
            ]
        );

        $record = $form->find($id);
        $specialComponent = $specialComponentRepo->find('01FK0PBV597J0VEQEW0AKH2SMN');
        $record->{'field_specialcomponent'} = $specialComponent;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FK0PBV597J0VEQEW0AKH2SMN', $record->get('field_specialcomponent')['value']);
    }

    /**
     * Test that we can generate a specialcomponent example data.
     */
    public function testSpecialComponentExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.specialcomponent.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'specialcomponent', 'specialcomponent', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_specialcomponent');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_specialcomponent'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.specialcomponent.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.specialcomponent.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('special_component_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
