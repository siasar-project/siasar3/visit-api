<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TechnicalAssistanceProvider;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTapEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a tap reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_tap" require a "country_reference" field in form "form.test.tap.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.tap.country.fail.empty');
        // Try to add the field type.
        $form->addField('tap_reference', 'tap', 'tap');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a tap reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_tap" require "country" field in form "form.test.tap.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.tap.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'tap_reference',
            'tap',
            'tap',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a tap reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tap.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'tap_reference',
            'tap',
            'tap',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist tap.
     *
     * @throws \Exception
     */
    public function testInsertTapDataFailTapNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[tap_reference] Field "field_tap" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.tap.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_tap' => new TechnicalAssistanceProvider('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTapDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.tap.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );
    }

    /**
     * Test insert wrong tap.
     *
     * @throws \Exception
     */
    public function testInsertTapDataFailBadTap(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[tap_reference] Field "field_tap" have a reference, "01FENEDYFA6KNAA4J459P5S0RF", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.tap.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );
    }

    /**
     * Test that we can insert a tap in a form.
     *
     * @throws \Exception
     */
    public function testInsertTapDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tap.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist tap.
     *
     * @throws \Exception
     */
    public function testUpdateTapDataFailTapNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.tap.udata.divnotfound] errors: 
[tap_reference] Field &quot;field_tap&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.tap.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );

        $record = $form->find($id);
        $record->{'field_tap'} = new TechnicalAssistanceProvider('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTapDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.tap.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.tap.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong tap.
     *
     * @throws \Exception
     */
    public function testUpdateTapDataFailBadTap(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.tap.udata.baddiv] errors: 
[tap_reference] Field &quot;field_tap&quot; have a reference, &quot;01FENEDYFA6KNAA4J459P5S0RF&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.tap.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a tap in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTapDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tap.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $tapRepo = $this->entityManager->getRepository(TechnicalAssistanceProvider::class);
        $tap = $tapRepo->find('01FENEDYFA6KNAA4J459P5S0RF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tap' => $tap,
            ]
        );

        $record = $form->find($id);
        $tap = $tapRepo->find('01FHWVW2GVTP8RRG1M3ZKS6H9A');
        $record->{'field_tap'} = $tap;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FHWVW2GVTP8RRG1M3ZKS6H9A', $record->get('field_tap')['value']);
    }

    /**
     * Test that we can generate a tap example data.
     */
    public function testTapExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tap.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'tap', 'tap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_tap');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_tap'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.tap.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.tap.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('tap_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
