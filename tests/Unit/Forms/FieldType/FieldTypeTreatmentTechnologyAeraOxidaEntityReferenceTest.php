<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TreatmentTechnologyAeraOxida;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTreatmentTechnologyAeraOxidaEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTreatmentTechnologyAeraOxidaEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ao" require a "country_reference" field in form "form.test.ao.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ao.country.fail.empty');
        // Try to add the field type.
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a TreatmentTechnologyAeraOxida reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ao" require "country" field in form "form.test.ao.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ao.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'treat_tech_aera_oxida_reference',
            'ao',
            'ao',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a TreatmentTechnologyAeraOxida reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ao.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'treat_tech_aera_oxida_reference',
            'ao',
            'ao',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist TreatmentTechnologyAeraOxida.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyAeraOxidaDataFailTreatmentTechnologyAeraOxidaNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_aera_oxida_reference] Field "field_ao" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ao.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ao' => new TreatmentTechnologyAeraOxida('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyAeraOxidaDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ao.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $aeraOxideRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxide = $aeraOxideRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxide,
            ]
        );
    }

    /**
     * Test insert wrong TreatmentTechnologyAeraOxida.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyAeraOxidaDataFailBadTreatmentTechnologyAeraOxida(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_aera_oxida_reference] Field "field_ao" have a reference, "01G2SWW6ZJ7H5Z4JSCMP0DC0AE", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ao.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $aeraOxidaRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxida,
            ]
        );
    }

    /**
     * Test that we can insert a TreatmentTechnologyAeraOxida in a form.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyAeraOxidaDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ao.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $aeraOxidaRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxida,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TreatmentTechnologyAeraOxida.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyAeraOxidaDataFailTreatmentTechnologyAeraOxidaNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ao.udata.divnotfound] errors: 
[treat_tech_aera_oxida_reference] Field &quot;field_ao&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ao.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $aeraOxidaRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxida,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ao'} = new TreatmentTechnologyAeraOxida('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyAeraOxidaDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ao.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ao.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $aeraOxidaRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxida,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TreatmentTechnologyAeraOxida.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyAeraOxidaDataFailBadTreatmentTechnologyAeraOxida(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ao.udata.baddiv] errors: 
[treat_tech_aera_oxida_reference] Field &quot;field_ao&quot; have a reference, &quot;01G2SWW6ZJ7H5Z4JSCMP0DC0AE&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ao.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $aeraOxidaRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxida,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TreatmentTechnologyAeraOxida in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyAeraOxidaDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ao.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $aeraOxidaRepo = $this->entityManager->getRepository(TreatmentTechnologyAeraOxida::class);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWW6ZJ7H5Z4JSCMP0DC0AE');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ao' => $aeraOxida,
            ]
        );

        $record = $form->find($id);
        $aeraOxida = $aeraOxidaRepo->find('01G2SWWJKVS2Z8N41F86KXH0EA');
        $record->{'field_ao'} = $aeraOxida;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2SWWJKVS2Z8N41F86KXH0EA', $record->get('field_ao')['value']);
    }

    /**
     * Test that we can generate a TreatmentTechnologyAeraOxida example data.
     */
    public function testTreatmentTechnologyAeraOxidaExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ao.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'ao', 'ao', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ao');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ao'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechaeraoxida.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechaeraoxida.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_aera_oxida_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
