<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TreatmentTechnologyCoagFloccu;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTreatmentTechnologyCoagFloccuEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTreatmentTechnologyCoagFloccuEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttcf" require a "country_reference" field in form "form.test.ttcf.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ttcf.country.fail.empty');
        // Try to add the field type.
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a TreatmentTechnologyCoagFloccu reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttcf" require "country" field in form "form.test.ttcf.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ttcf.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'treat_tech_coag_floccu_reference',
            'ttcf',
            'ttcf',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a TreatmentTechnologyCoagFloccu reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttcf.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'treat_tech_coag_floccu_reference',
            'ttcf',
            'ttcf',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist TreatmentTechnologyCoagFloccu.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyCoagFloccuDataFailTreatmentTechnologyCoagFloccuNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_coag_floccu_reference] Field "field_ttcf" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ttcf.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => new TreatmentTechnologyCoagFloccu('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyCoagFloccuDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ttcf.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );
    }

    /**
     * Test insert wrong TreatmentTechnologyCoagFloccu.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyCoagFloccuDataFailBadTreatmentTechnologyCoagFloccu(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_coag_floccu_reference] Field "field_ttcf" have a reference, "01G2W4E3PHR0B3XE4D3F5WGAW1", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ttcf.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );
    }

    /**
     * Test that we can insert a TreatmentTechnologyCoagFloccu in a form.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyCoagFloccuDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttcf.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TreatmentTechnologyCoagFloccu.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyCoagFloccuDataFailTreatmentTechnologyCoagFloccuNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttcf.udata.divnotfound] errors: 
[treat_tech_coag_floccu_reference] Field &quot;field_ttcf&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttcf.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ttcf'} = new TreatmentTechnologyCoagFloccu('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyCoagFloccuDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttcf.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttcf.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TreatmentTechnologyCoagFloccu.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyCoagFloccuDataFailBadTreatmentTechnologyCoagFloccu(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttcf.udata.baddiv] errors: 
[treat_tech_coag_floccu_reference] Field &quot;field_ttcf&quot; have a reference, &quot;01G2W4E3PHR0B3XE4D3F5WGAW1&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttcf.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TreatmentTechnologyCoagFloccu in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyCoagFloccuDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttcf.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $coagRepo = $this->entityManager->getRepository(TreatmentTechnologyCoagFloccu::class);
        $coag = $coagRepo->find('01G2W4E3PHR0B3XE4D3F5WGAW1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttcf' => $coag,
            ]
        );

        $record = $form->find($id);
        $coag = $coagRepo->find('01G2W4EB3BEVCN5E2X3JWF74AA');
        $record->{'field_ttcf'} = $coag;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2W4EB3BEVCN5E2X3JWF74AA', $record->get('field_ttcf')['value']);
    }

    /**
     * Test that we can generate a TreatmentTechnologyCoagFloccu example data.
     */
    public function testTreatmentTreatmentTechnologyCoagFloccuExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttcf.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'ttcf', 'ttcf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ttcf');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ttcf'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechcoagfloccu.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechcoagfloccu.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_coag_floccu_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
