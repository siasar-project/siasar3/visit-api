<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TreatmentTechnologyDesalination;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTreatmentTechnologyDesalinationEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTreatmentTechnologyDesalinationEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttd" require a "country_reference" field in form "form.test.ttd.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ttd.country.fail.empty');
        // Try to add the field type.
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a TreatmentTechnologyDesalination reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttd" require "country" field in form "form.test.ttd.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ttd.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'treat_tech_desali_reference',
            'ttd',
            'ttd',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a TreatmentTechnologyDesalination reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttd.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'treat_tech_desali_reference',
            'ttd',
            'ttd',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist TreatmentTechnologyDesalination.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyDesalinationDataFailTreatmentTechnologyDesalinationNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_desali_reference] Field "ttd [field_ttd]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ttd.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => new TreatmentTechnologyDesalination('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyDesalinationDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ttd.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );
    }

    /**
     * Test insert wrong TreatmentTechnologyDesalination.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyDesalinationDataFailBadTreatmentTechnologyDesalination(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_desali_reference] Field "ttd [field_ttd]" have a reference, "01G2VNN3DYM1FKK48J9C1HWWD1", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ttd.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );
    }

    /**
     * Test that we can insert a TreatmentTechnologyDesalination in a form.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyDesalinationDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttd.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TreatmentTechnologyDesalination.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyDesalinationDataFailTreatmentTechnologyDesalinationNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttd.udata.divnotfound] errors: 
[treat_tech_desali_reference] Field &quot;ttd [field_ttd]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttd.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ttd'} = new TreatmentTechnologyDesalination('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyDesalinationDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttd.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttd.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TreatmentTechnologyDesalination.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyDesalinationDataFailBadTreatmentTechnologyDesalination(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttd.udata.baddiv] errors: 
[treat_tech_desali_reference] Field &quot;ttd [field_ttd]&quot; have a reference, &quot;01G2VNN3DYM1FKK48J9C1HWWD1&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttd.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TreatmentTechnologyDesalination in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyDesalinationDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttd.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $desalinaRepo = $this->entityManager->getRepository(TreatmentTechnologyDesalination::class);
        $desalina = $desalinaRepo->find('01G2VNN3DYM1FKK48J9C1HWWD1');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttd' => $desalina,
            ]
        );

        $record = $form->find($id);
        $desalina = $desalinaRepo->find('01G2VNN9M096N21JK4N8WE5MTM');
        $record->{'field_ttd'} = $desalina;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2VNN9M096N21JK4N8WE5MTM', $record->get('field_ttd')['value']);
    }

    /**
     * Test that we can generate a TreatmentTechnologyDesalination example data.
     */
    public function testTreatmentTechnologyDesalinationExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttd.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'ttd', 'ttd', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ttd');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ttd'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechdesali.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechdesali.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_desali_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
