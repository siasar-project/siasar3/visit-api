<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TreatmentTechnologyFiltration;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTreatmentTechnologyFiltrationEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTreatmentTechnologyFiltrationEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttff" require a "country_reference" field in form "form.test.ttff.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ttff.country.fail.empty');
        // Try to add the field type.
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a TreatmentTechnologyFiltration reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttff" require "country" field in form "form.test.ttff.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ttff.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'treat_tech_filtra_reference',
            'ttff',
            'ttff',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a TreatmentTechnologyFiltration reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttff.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'treat_tech_filtra_reference',
            'ttff',
            'ttff',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist TreatmentTechnologyFiltration.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyFiltrationDataFailTreatmentTechnologyFiltrationNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_filtra_reference] Field "ttff [field_ttff]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ttff.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => new TreatmentTechnologyFiltration('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyFiltrationDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ttff.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );
    }

    /**
     * Test insert wrong TreatmentTechnologyFiltration.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyFiltrationDataFailBadTreatmentTechnologyFiltration(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_filtra_reference] Field "ttff [field_ttff]" have a reference, "01G2W89T2CG4DZR2S5KFMB331J", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ttff.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );
    }

    /**
     * Test that we can insert a TreatmentTechnologyFiltration in a form.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologyFiltrationDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttff.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TreatmentTechnologyFiltration.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyFiltrationDataFailTreatmentTechnologyFiltrationNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttff.udata.divnotfound] errors: 
[treat_tech_filtra_reference] Field &quot;ttff [field_ttff]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttff.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ttff'} = new TreatmentTechnologyFiltration('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyFiltrationDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttff.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttff.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TreatmentTechnologyFiltration.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyFiltrationDataFailBadTreatmentTechnologyFiltration(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttff.udata.baddiv] errors: 
[treat_tech_filtra_reference] Field &quot;ttff [field_ttff]&quot; have a reference, &quot;01G2W89T2CG4DZR2S5KFMB331J&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttff.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TreatmentTechnologyFiltration in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologyFiltrationDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttff.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $filtraRepo = $this->entityManager->getRepository(TreatmentTechnologyFiltration::class);
        $filtra = $filtraRepo->find('01G2W89T2CG4DZR2S5KFMB331J');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttff' => $filtra,
            ]
        );

        $record = $form->find($id);
        $filtra = $filtraRepo->find('01G2W8A019MRDK0JDNHYXEG47P');
        $record->{'field_ttff'} = $filtra;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2W8A019MRDK0JDNHYXEG47P', $record->get('field_ttff')['value']);
    }

    /**
     * Test that we can generate a TreatmentTechnologyFiltration example data.
     */
    public function testTreatmentTechnologyFiltrationExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttff.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'ttff', 'ttff', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ttff');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ttff'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechfiltra.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechfiltra.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_filtra_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
