<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TreatmentTechnologySedimentation;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTreatmentTechnologySedimentationEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTreatmentTechnologySedimentationEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_tts" require a "country_reference" field in form "form.test.tts.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.tts.country.fail.empty');
        // Try to add the field type.
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a TreatmentTechnologySedimentation reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_tts" require "country" field in form "form.test.tts.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.tts.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'treat_tech_sedimen_reference',
            'tts',
            'tts',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a TreatmentTechnologySedimentation reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tts.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'treat_tech_sedimen_reference',
            'tts',
            'tts',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist TreatmentTechnologySedimentation.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologySedimentationDataFailTreatmentTechnologySedimentationNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_sedimen_reference] Field "tts [field_tts]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.tts.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_tts' => new TreatmentTechnologySedimentation('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologySedimentationDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.tts.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );
    }

    /**
     * Test insert wrong TreatmentTechnologySedimentation.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologySedimentationDataFailBadTreatmentTechnologySedimentation(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[treat_tech_sedimen_reference] Field "tts [field_tts]" have a reference, "01G2VSTVWWZX3D98A3GYBTEREP", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.tts.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );
    }

    /**
     * Test that we can insert a TreatmentTechnologySedimentation in a form.
     *
     * @throws \Exception
     */
    public function testInsertTreatmentTechnologySedimentationDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tts.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TreatmentTechnologySedimentation.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologySedimentationDataFailTreatmentTechnologySedimentationNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.tts.udata.divnotfound] errors: 
[treat_tech_sedimen_reference] Field &quot;tts [field_tts]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.tts.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );

        $record = $form->find($id);
        $record->{'field_tts'} = new TreatmentTechnologySedimentation('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologySedimentationDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.tts.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.tts.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TreatmentTechnologySedimentation.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologySedimentationDataFailBadTreatmentTechnologySedimentation(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.tts.udata.baddiv] errors: 
[treat_tech_sedimen_reference] Field &quot;tts [field_tts]&quot; have a reference, &quot;01G2VSTVWWZX3D98A3GYBTEREP&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.tts.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TreatmentTechnologySedimentation in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTreatmentTechnologySedimentationDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tts.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $sedimenRepo = $this->entityManager->getRepository(TreatmentTechnologySedimentation::class);
        $sedimen = $sedimenRepo->find('01G2VSTVWWZX3D98A3GYBTEREP');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_tts' => $sedimen,
            ]
        );

        $record = $form->find($id);
        $sedimen = $sedimenRepo->find('01G2VSV16V2FG9J9P9TYADSNTT');
        $record->{'field_tts'} = $sedimen;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G2VSV16V2FG9J9P9TYADSNTT', $record->get('field_tts')['value']);
    }

    /**
     * Test that we can generate a TreatmentTechnologySedimentation example data.
     */
    public function testTreatmentTechnologySedimentationExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.tts.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'tts', 'tts', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_tts');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_tts'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechsedimen.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.treattechsedimen.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('treat_tech_sedimen_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
