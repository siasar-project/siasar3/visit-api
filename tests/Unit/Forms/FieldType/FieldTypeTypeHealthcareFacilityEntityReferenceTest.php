<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TypeHealthcareFacility;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTypeHealthcareFacilityEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTypeHealthcareFacilityEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_thf" require a "country_reference" field in form "form.test.thf.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.thf.country.fail.empty');
        // Try to add the field type.
        $form->addField('type_health_facility_reference', 'thf', 'thf');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a type_health_facility_reference reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_thf" require "country" field in form "form.test.thf.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.thf.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'type_health_facility_reference',
            'thf',
            'thf',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a type_health_facility_reference reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.thf.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'type_health_facility_reference',
            'thf',
            'thf',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist thf.
     *
     * @throws \Exception
     */
    public function testInsertTypeHealthcareFacilityDataFailTypeHealthcareFacilityNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[type_health_facility_reference] Field "field_thf" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.thf.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_thf' => new TypeHealthcareFacility('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTypeHealthcareFacilityDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.thf.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );
    }

    /**
     * Test insert wrong TypeHealthcareFacility.
     *
     * @throws \Exception
     */
    public function testInsertTypeHealthcareFacilityDataFailBadTypeHealthcareFacility(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[type_health_facility_reference] Field "field_thf" have a reference, "01G3TP16FW16HYFHJ9S13MDCGH", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.thf.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );
    }

    /**
     * Test that we can insert a TypeHealthcareFacility in a form.
     *
     * @throws \Exception
     */
    public function testInsertTypeHealthcareFacilityDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.thf.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TypeHealthcareFacility.
     *
     * @throws \Exception
     */
    public function testUpdateTypeHealthcareFacilityDataFailTypeHealthcareFacilityNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.thf.updata.notfound] errors: 
[type_health_facility_reference] Field &quot;field_thf&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.thf.updata.notfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );

        $record = $form->find($id);
        $record->{'field_thf'} = [
            'value' => '01FFHQPYMYKY76RY6EDKR271EZ',
            'class' => TypeHealthcareFacility::class,
        ];
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTypeHealthcareFacilityDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.thf.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.thf.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TypeHealthcareFacility.
     *
     * @throws \Exception
     */
    public function testUpdateTypeHealthcareFacilityDataFailBadTypeHealthcareFacility(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.thf.udata.baddiv] errors: 
[type_health_facility_reference] Field &quot;field_thf&quot; have a reference, &quot;01G3TP16FW16HYFHJ9S13MDCGH&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.thf.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TypeHealthcareFacility in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTypeHealthcareFacilityDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.thf.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $thfRepo = $this->entityManager->getRepository(TypeHealthcareFacility::class);
        /** @var TypeHealthcareFacility $thf */
        $thf = $thfRepo->find('01G3TP16FW16HYFHJ9S13MDCGH');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_thf' => $thf,
            ]
        );

        $record = $form->find($id);
        /** @var TypeHealthcareFacility $thf */
        $thf = $thfRepo->find('01G3TP1EFM2ZN2CNAXGA2H58V8');
        $record->{'field_thf'} = [
            'value' => $thf->getId(),
            'class' => TypeHealthcareFacility::class,
        ];
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G3TP1EFM2ZN2CNAXGA2H58V8', $record->get('field_thf')['value']);
    }

    /**
     * Test that we can generate a TypeHealthcareFacility example data.
     */
    public function testTypeHealthcareFacilityExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.thf.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'thf', 'thf', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_thf');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_thf'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.typehealthfacility.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.typehealthfacility.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_health_facility_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
