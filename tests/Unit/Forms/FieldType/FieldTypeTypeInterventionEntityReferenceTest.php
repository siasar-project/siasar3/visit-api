<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TypeIntervention;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTypeInterventionEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a TypeIntervention reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_typeintervention" require a "country_reference" field in form "form.test.typeintervention.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.typeintervention.country.fail.empty');
        // Try to add the field type.
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a type_intervention_reference reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_typeintervention" require "country" field in form "form.test.typeintervention.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.typeintervention.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'type_intervention_reference',
            'typeintervention',
            'typeintervention',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a type_intervention reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.typeintervention.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'type_intervention_reference',
            'typeintervention',
            'typeintervention',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist type_intervention.
     *
     * @throws \Exception
     */
    public function testInsertTypeInterventionDataFailTypeInterventionNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[type_intervention_reference] Field "typeintervention [field_typeintervention]" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.typeintervention.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => new TypeIntervention('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTypeInterventionDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "Country [field_country]" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.typeintervention.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );
    }

    /**
     * Test insert wrong type_intervention.
     *
     * @throws \Exception
     */
    public function testInsertTypeInterventionDataFailBadTypeIntervention(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[type_intervention_reference] Field "typeintervention [field_typeintervention]" have a reference, "01FHWY0EB2DA9HHDATZ5ANEH10", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.typeintervention.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );
    }

    /**
     * Test that we can insert a type_intervention in a form.
     *
     * @throws \Exception
     */
    public function testInsertTypeInterventionDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.typeintervention.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist typeintervention.
     *
     * @throws \Exception
     */
    public function testUpdateTypeInterventionDataFailTypeInterventionNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.typeintervention.udata.divnotfound] errors: 
[type_intervention_reference] Field &quot;typeintervention [field_typeintervention]&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.typeintervention.udata.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );

        $record = $form->find($id);
        $record->{'field_typeintervention'} = new TypeIntervention('01FFHQPYMYKY76RY6EDKR271EZ');
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTypeInterventionDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.typeintervention.udata.countrynotfound] errors: 
[country_reference] Field &quot;Country [field_country]&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.typeintervention.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong typeintervention.
     *
     * @throws \Exception
     */
    public function testUpdateTypeInterventionDataFailBadTypeIntervention(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.typeintervention.udata.baddiv] errors: 
[type_intervention_reference] Field &quot;typeintervention [field_typeintervention]&quot; have a reference, &quot;01FHWY0EB2DA9HHDATZ5ANEH10&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.typeintervention.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a typeintervention in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTypeInterventionDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.typeintervention.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $typeInterventionRepo = $this->entityManager->getRepository(TypeIntervention::class);
        $typeIntervention = $typeInterventionRepo->find('01FHWY0EB2DA9HHDATZ5ANEH10');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_typeintervention' => $typeIntervention,
            ]
        );

        $record = $form->find($id);
        $typeIntervention = $typeInterventionRepo->find('01FENEDYFA6KNAA4J459P5S0RF');
        $record->{'field_typeintervention'} = $typeIntervention;
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01FENEDYFA6KNAA4J459P5S0RF', $record->get('field_typeintervention')['value']);
    }

    /**
     * Test that we can generate a typeintervention example data.
     */
    public function testTypeInterventionExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.typeintervention.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'typeintervention', 'typeintervention', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_typeintervention');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_typeintervention'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.typeintervention.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.typeintervention.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_intervention_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
