<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */


namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\TypeTap;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeTypeTapEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeTypeTapEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttap" require a "country_reference" field in form "form.test.ttap.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.ttap.country.fail.empty');
        // Try to add the field type.
        $form->addField('type_tap_reference', 'ttap', 'ttap');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a TypeTap reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_ttap" require "country" field in form "form.test.ttap.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.ttap.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'type_tap_reference',
            'ttap',
            'ttap',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a TypeTap reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttap.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'type_tap_reference',
            'ttap',
            'ttap',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist TypeTap.
     *
     * @throws \Exception
     */
    public function testInsertTypeTapDataFailTypeTapNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[type_tap_reference] Field "field_ttap" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.ttap.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => new TypeTap('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertTypeTapDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.ttap.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );
    }

    /**
     * Test insert wrong TypeTap.
     *
     * @throws \Exception
     */
    public function testInsertTypeTapDataFailBadTypeTap(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[type_tap_reference] Field "field_ttap" have a reference, "01G3TXHW5BWC4RSFHN3SREGRDQ", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.ttap.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );
    }

    /**
     * Test that we can insert a TypeTap in a form.
     *
     * @throws \Exception
     */
    public function testInsertTypeTapDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttap.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist TypeTap.
     *
     * @throws \Exception
     */
    public function testUpdateTypeTapDataFailTypeTapNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttap.updata.notfound] errors: 
[type_tap_reference] Field &quot;field_ttap&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttap.updata.notfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );

        $record = $form->find($id);
        $record->{'field_ttap'} = [
            'value' => '01FFHQPYMYKY76RY6EDKR271EZ',
            'class' => TypeTap::class,
        ];
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateTypeTapDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttap.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttap.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong TypeTap.
     *
     * @throws \Exception
     */
    public function testUpdateTypeTapDataFailBadTypeTap(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.ttap.udata.baddiv] errors: 
[type_tap_reference] Field &quot;field_ttap&quot; have a reference, &quot;01G3TXHW5BWC4RSFHN3SREGRDQ&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.ttap.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a TypeTap in a form.
     *
     * @throws \Exception
     */
    public function testUpdateTypeTapDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttap.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $ttapRepo = $this->entityManager->getRepository(TypeTap::class);
        /** @var TypeTap $ttap */
        $ttap = $ttapRepo->find('01G3TXHW5BWC4RSFHN3SREGRDQ');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_ttap' => $ttap,
            ]
        );

        $record = $form->find($id);
        /** @var TypeTap $ttap */
        $ttap = $ttapRepo->find('01G3TXJ1ECWV2M5NB5F2MGV35Z');
        $record->{'field_ttap'} = [
            'value' => $ttap->getId(),
            'class' => TypeTap::class,
        ];
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G3TXJ1ECWV2M5NB5F2MGV35Z', $record->get('field_ttap')['value']);
    }

    /**
     * Test that we can generate a TypeTap example data.
     */
    public function testTypeTapExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.ttap.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'ttap', 'ttap', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_ttap');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_ttap'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.typetap.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.typetap.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('type_tap_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
