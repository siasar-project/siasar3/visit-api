<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormRecord;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test ULID fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeUlidTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can install an ULID field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallUlidInForm(): void
    {
        $form = $this->formFactory->create('test.ulid.fieldtype');
        // Try to add the trust field type.
        $form->addField('ulid', 'ulid', 'Ulid number');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_ulid', $columns);
        $this->assertEquals('ulid', $columns["field_ulid"]->getType()->getName());
    }

    /**
     * Test that we can insert an ULID value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertUlidInForm(): void
    {
        $form = $this->formFactory->create('test.insert.ulid');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('ulid', 'ulid', 'ULID');
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $ulid = new Ulid();
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_ulid' => $ulid,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        $dbValue = $record->{'field_ulid'};
        $this->assertEquals($ulid->toBase32(), $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_ulid');
        $this->assertEquals($ulid->toBase32(), $dbValue);

        // Insert by instance.
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_ulid' => $ulid->toBase32(),
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $dbValue = $record->{'field_ulid'};
        $this->assertEquals($ulid->toBase32(), $dbValue);
        // Read field how properties array.
        $dbValue = $record->get('field_ulid');
        $this->assertEquals($ulid->toBase32(), $dbValue);
    }

    /**
     * Test that we can insert an ULID value to autogenera.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAutoUlidInForm(): void
    {
        $form = $this->formFactory->create('test.insert.auto.ulid');
        // Try to add the trust field type.
        $form->addField('ulid', 'ulid', 'ULID', ['required' => true]);
        $form->saveNow();
        $form->install();
        // Insert by internal properties.
        $recordId = $form->insert([]);
        /** @var FormRecord $record */
        $record = $form->find($recordId);
        // Read field how target format.
        $dbValue = $record->{'field_ulid'};
        $this->assertNotNull($dbValue);
        // If is a bad database value the tests will fail.
        $ulid = new Ulid($dbValue);
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.ulid');
        $manager->addField('ulid', 'ulid', 'ULID', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.ulid');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_ulid', $tableData->getColumns());

        // Verify that the multivalued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_ulid', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.ulid');
        $manager->addField('ulid', 'ulid', 'ULID', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.ulid');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multivalued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.f2');
        $form->addField('ulid', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.f2');
        $ulids = [new Ulid(), new Ulid(), new Ulid(), new Ulid(), new Ulid()];
        $recordId = $form->insert(
            [
                'field_reference' => $ulids,
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $ulids = [new Ulid(), new Ulid(), new Ulid()];
        $record->{'field_reference'} = $ulids;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
            $this->assertEquals($ulids[$index]->toBinary(), $item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.e1');
        $form->addField('ulid', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('ulid', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.e1');
        $ulid = new Ulid();
        $ulids = [new Ulid(), new Ulid(), new Ulid(), new Ulid(), new Ulid()];
        $recordId = $form->insert(
            [
                'field_single' => $ulid,
                'field_reference' => $ulids,
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $ulid1 = new Ulid();
        $ulids = [new Ulid(), new Ulid(), new Ulid()];
        $record->{'field_single'} = $ulid1;
        $record->{'field_reference'} = $ulids;
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals($ulid1->toBinary(), $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsString($item['field_reference']);
            $this->assertEquals($ulids[$index]->toBinary(), $item['field_reference']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.g');
        $form->addField('ulid', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('ulid', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.g');
        $ulid1 = new Ulid();
        $ulids = [new Ulid(), new Ulid(), new Ulid(), new Ulid(), new Ulid()];
        $recordId = $form->insert(
            [
                'field_single' => $ulid1,
                'field_reference' => $ulids,
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals($ulid1->toBase32(), $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($ulids[$key]->toBase32(), $item);
        }
        // Read internal values.
        $references = $record->get('field_reference');
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($ulids[$key]->toBinary(), $item);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.ulid.notempty');
        $form->addField(
            'ulid',
            'reference',
            'Map Points'
        );
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.ulid.empty');
        $form->addField(
            'ulid',
            'reference',
            'Map Points'
        );
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');

        $id = $form->insert([]);
        $record = $form->find($id);
        // This field can't be empty.
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
