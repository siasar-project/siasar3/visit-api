<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms\FieldType;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tools\Measurements\VolumeMeasurement;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Yaml\Yaml;

/**
 * Test volume fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeVolumeTest extends KernelTestCase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected mixed $units;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');

        $units = Yaml::parseFile(dirname(__FILE__).'/../../../assets/system.units.volume.yml');
        $cfg = $this->configuration->findEditable('system.units.volume');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.volume', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        $this->units = $units;
    }

    /**
     * Test that we can install an volume field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallVolumeInForm(): void
    {
        $form = $this->formFactory->create('test.volume.fieldtype');
        // Try to add the trust field type.
        $form->addField('volume', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $columns = $tableData->getColumns();
        $this->assertArrayHasKey('field_name_value', $columns);
        $this->assertArrayHasKey('field_name_unit', $columns);
        $this->assertEquals('decimal', $columns["field_name_value"]->getType()->getName());
        $this->assertEquals('text', $columns["field_name_unit"]->getType()->getName());
    }

    /**
     * Test that we can insert an volume value in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertVolumeInForm(): void
    {
        $form = $this->formFactory->create('test.insert.volume');
        // Try to add the trust field type.
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('volume', 'name', 'name');
        $form->saveNow();
        $form->install();
        // Load dummy user.
        $unit = 'cubic kilometre';
        $volume = new VolumeMeasurement(1, $unit);
        // Insert by internal properties.
        $form->insert(
            [
                'field_reference' => ['value' => 1],
                'field_name' => [
                    'value' => $volume,
                    'unit' => $unit,
                ],
            ]
        );
        $record = $form->findOneBy(['field_reference' => 1]);
        // Read field how target format.
        /**
         * Reference volume.
         *
         * @var VolumeMeasurement $volumeRef
         */
        $volumeRef = $record->{'field_name'};
        $this->assertEquals($volume, $volumeRef);
        $this->assertEquals(get_class($volume), get_class($volumeRef));
        $this->assertEquals($unit, $volumeRef->getUnit());
        $this->assertEquals(1, $volumeRef->getValue());
        // Read field how properties array.
        $volumeRef = $record->get('field_name');
        $this->assertEquals($volume->getUnit(), $volumeRef['value']->getUnit());
        $this->assertEquals($unit, $volumeRef['unit']);
        $this->assertEquals(1, $volumeRef['value']->getValue());

        // Insert by instance.
        $unit = 'cubic kilometre';
        $volume = new VolumeMeasurement(1, $unit);
        $form->insert(
            [
                'field_reference' => ['value' => 2],
                'field_name' => $volume,
            ]
        );
        $record = $form->findOneBy(['field_reference' => 2]);
        // Read field how target format.
        $volumeRef = $record->{'field_name'};
        $this->assertEquals($volume, $volumeRef);
        $this->assertEquals(get_class($volume), get_class($volumeRef));
        $this->assertEquals($unit, $volumeRef->getUnit());
        $this->assertEquals(1, $volumeRef->getValue());
        // Read field how properties array.
        $volumeRef = $record->get('field_name');
        $this->assertEquals($volume->getUnit(), $volumeRef['value']->getUnit());
        $this->assertEquals($unit, $volumeRef['unit']);
        $this->assertEquals(1, $volumeRef['value']->getValue());
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedVolumeFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.volume');
        $manager->addField('volume', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.volume');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_name_unit', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedVolumeFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall.volume');
        $manager->addField('volume', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall.volume');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedVolumeFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.multivalued.entity.reference.vga');
        $form->addField('volume', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.multivalued.entity.reference.vga');
        $volumes = [
            new VolumeMeasurement(11, 'cubic kilometre'),
            new VolumeMeasurement(12, 'cubic kilometre'),
            new VolumeMeasurement(13, 'cubic kilometre'),
            new VolumeMeasurement(14, 'cubic kilometre'),
            new VolumeMeasurement(15, 'cubic kilometre'),
        ];
        $recordId = $form->insert(
            [
                'field_reference' => $volumes,
            ]
        );
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_unit', $item);

            $this->assertEquals(($index + 11) * 1000000000, $item['field_reference_value']);
            $this->assertEquals('cubic kilometre', $item['field_reference_unit']);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedVolumeFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.volume');
        $form->addField('volume', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.volume');
        $recordId = $form->insert(
            [
                'field_reference' => [
                    new VolumeMeasurement(11, 'cubic kilometre'),
                    new VolumeMeasurement(12, 'cubic kilometre'),
                    new VolumeMeasurement(13, 'cubic kilometre'),
                    new VolumeMeasurement(14, 'cubic kilometre'),
                    new VolumeMeasurement(15, 'cubic kilometre'),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [
            new VolumeMeasurement(13, 'cubic kilometre'),
            new VolumeMeasurement(14, 'cubic kilometre'),
            new VolumeMeasurement(15, 'cubic kilometre'),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_unit', $item);

            $this->assertEquals(($index + 13) * 1000000000, $item['field_reference_value']);
            $this->assertEquals('cubic kilometre', $item['field_reference_unit']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedVolumeFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued.volume');
        $form->addField('volume', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued.volume');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    new VolumeMeasurement(11, 'cubic kilometre'),
                    new VolumeMeasurement(12, 'cubic kilometre'),
                    new VolumeMeasurement(13, 'cubic kilometre'),
                    new VolumeMeasurement(14, 'cubic kilometre'),
                    new VolumeMeasurement(15, 'cubic kilometre'),
                ],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [
            new VolumeMeasurement(13, 'cubic kilometre'),
            new VolumeMeasurement(14, 'cubic kilometre'),
            new VolumeMeasurement(15, 'cubic kilometre'),
        ];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference_value', $item);
            $this->assertArrayHasKey('field_reference_unit', $item);

            $this->assertEquals(($index + 13) * 1000000000, $item['field_reference_value']);
            $this->assertEquals('cubic kilometre', $item['field_reference_unit']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldVolumeInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued.volume');
        $form->addField('volume', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued.volume');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [
                    new VolumeMeasurement(11, 'cubic kilometre'),
                    new VolumeMeasurement(12, 'cubic kilometre'),
                    new VolumeMeasurement(13, 'cubic kilometre'),
                    new VolumeMeasurement(14, 'cubic kilometre'),
                    new VolumeMeasurement(15, 'cubic kilometre'),
                ],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $index => $item) {
            $this->assertInstanceOf(VolumeMeasurement::class, $item);
            $this->assertEquals($index + 11, $item->getValue());
            $this->assertEquals('cubic kilometre', $item->getUnit());
        }
        // Read internal values.
        foreach ($record->get('field_reference') as $index => $item) {
            $this->assertArrayHasKey('value', $item);
            $this->assertArrayHasKey('unit', $item);
            // Internally the volume field use metre to store values.
            $this->assertEquals(($index + 11) * 1000000000, $item['value']);
            $this->assertEquals('cubic kilometre', $item['unit']);

            $this->assertIsNumeric($item['value']);
            $this->assertIsString($item['unit']);
        }
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.volume.empty');
        $form->addField('volume', 'reference', 'Record reference');
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_reference'] = $field->getExampleData();
        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.volume.empty');
        $form->addField('volume', 'reference', 'Record reference');
        $form->saveNow();
        $form->install();
        $id = $form->insert([]);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
