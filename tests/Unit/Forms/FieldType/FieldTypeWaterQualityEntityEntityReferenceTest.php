<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */


namespace App\Tests\Unit\Forms\FieldType;

use App\Entity\Country;
use App\Entity\WaterQualityEntity;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test entity reference fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FieldTypeWaterQualityEntityEntityReferenceTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can't add a FieldTypeWaterQualityEntityEntityReferenceTest reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByEmpty(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_is" require a "country_reference" field in form "form.test.wqe.country.fail.empty".');
        // Create form.
        $form = $this->formFactory->create('test.wqe.country.fail.empty');
        // Try to add the field type.
        $form->addField('water_quality_entity_reference', 'is', 'is');
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can't add a WaterQualityEntity reference field without country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredFailByWrong(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The field "field_is" require "country" field in form "form.test.wqe.country.fail.wrong", but it\'s not found.');
        // Create form.
        $form = $this->formFactory->create('test.wqe.country.fail.wrong');
        // Try to add the field type.
        $form->addField(
            'water_quality_entity_reference',
            'is',
            'is',
            [
                'country_field' => 'country',
            ]
        );
        $form->saveNow();
        $form->install();
    }

    /**
     * Test that we can add a WaterQualityEntity reference field with country field.
     *
     * @throws \Exception
     */
    public function testCountryRequiredOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.wqe.country.ok');
        $form->addField('country_reference', 'country', 'Country');
        // Try to add the field type.
        $form->addField(
            'water_quality_entity_reference',
            'is',
            'is',
            [
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->install();
        $this->assertTrue($form->isInstalled());
    }

    /**
     * Test insert not exist WaterQualityEntity.
     *
     * @throws \Exception
     */
    public function testInsertWaterQualityEntityDataFailWaterQualityEntityNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[water_quality_entity_reference] Field "field_is" have a wrong reference: "01FJYCWTPB91FFQMK7YVY495FA".');

        // Create form.
        $form = $this->formFactory->create('test.wqe.data.divnotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $form->insert(
            [
                'field_country' => $country,
                'field_is' => new WaterQualityEntity('01FJYCWTPB91FFQMK7YVY495FA'),
            ]
        );
    }

    /**
     * Test insert not exist country.
     *
     * @throws \Exception
     */
    public function testInsertWaterQualityEntityDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[country_reference] Field "field_country" have a wrong reference: "und".');

        // Create form.
        $form = $this->formFactory->create('test.wqe.data.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $country = new Country('und');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );
    }

    /**
     * Test insert wrong WaterQualityEntity.
     *
     * @throws \Exception
     */
    public function testInsertWaterQualityEntityDataFailBadWaterQualityEntity(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[water_quality_entity_reference] Field "field_is" have a reference, "01G4HYMF72CZ03CD38614945YF", outside of country "es".');

        // Create form.
        $form = $this->formFactory->create('test.wqe.data.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );
    }

    /**
     * Test that we can insert a WaterQualityEntity in a form.
     *
     * @throws \Exception
     */
    public function testInsertWaterQualityEntityDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.wqe.data.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );

        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test update not exist WaterQualityEntity.
     *
     * @throws \Exception
     */
    public function testUpdateWaterQualityEntityDataFailWaterQualityEntityNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.wqe.updata.notfound] errors: 
[water_quality_entity_reference] Field &quot;field_is&quot; have a wrong reference: &quot;01FFHQPYMYKY76RY6EDKR271EZ&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.wqe.updata.notfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );

        $record = $form->find($id);
        $record->{'field_is'} = [
            'value' => '01FFHQPYMYKY76RY6EDKR271EZ',
            'class' => WaterQualityEntity::class,
        ];
        $record->save();
    }

    /**
     * Test update not exist country.
     *
     * @throws \Exception
     */
    public function testUpdateWaterQualityEntityDataFailCountryNotFound(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.wqe.udata.countrynotfound] errors: 
[country_reference] Field &quot;field_country&quot; have a wrong reference: &quot;und&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.wqe.udata.countrynotfound');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );

        $country = new Country('und');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test update wrong WaterQualityEntity.
     *
     * @throws \Exception
     */
    public function testUpdateWaterQualityEntityDataFailBadWaterQualityEntity(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[form.test.wqe.udata.baddiv] errors: 
[water_quality_entity_reference] Field &quot;field_is&quot; have a reference, &quot;01G4HYMF72CZ03CD38614945YF&quot;, outside of country &quot;es&quot;.');

        // Create form.
        $form = $this->formFactory->create('test.wqe.udata.baddiv');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');

        $record = $form->find($id);
        $record->{'field_country'} = $country;
        $record->save();
    }

    /**
     * Test that we can update a WaterQualityEntity in a form.
     *
     * @throws \Exception
     */
    public function testUpdateWaterQualityEntityDataOk(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.wqe.udata.ok');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $isRepo = $this->entityManager->getRepository(WaterQualityEntity::class);
        /** @var WaterQualityEntity $is */
        $is = $isRepo->find('01G4HYMF72CZ03CD38614945YF');

        $id = $form->insert(
            [
                'field_country' => $country,
                'field_is' => $is,
            ]
        );

        $record = $form->find($id);
        /** @var WaterQualityEntity $is */
        $is = $isRepo->find('01G4HYMS8DBTVQNZPZ2EC9C3VE');
        $record->{'field_is'} = [
            'value' => $is->getId(),
            'class' => WaterQualityEntity::class,
        ];
        $record->save();

        $record = $form->find($id);
        $this->assertEquals('01G4HYMS8DBTVQNZPZ2EC9C3VE', $record->get('field_is')['value']);
    }

    /**
     * Test that we can generate a WaterQualityEntity example data.
     */
    public function testWaterQualityEntityExampleData(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.wqe.data.example');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'is', 'is', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();

        $divField = $form->getFieldDefinition('field_is');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_is'] = $divField->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);

        $this->assertNotNull($record);
    }

    /**
     * Test that we validate not empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldNotEmpty(): void
    {
        $form = $this->formFactory->create('test.waterqualityentity.notempty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];
        $exampleData['field_reference'] = $field->getExampleData($exampleData);

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertFalse($record->getFieldDefinition('field_reference')->isEmpty());
    }

    /**
     * Test that we validate empty.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFieldEmpty(): void
    {
        $form = $this->formFactory->create('test.waterqualityentity.empty');
        $form->addField('country_reference', 'country', 'Country');
        $form->addField('water_quality_entity_reference', 'reference', 'cs', ['country_field' => 'field_country']);
        $form->saveNow();
        $form->install();
        $field = $form->getFieldDefinition('field_reference');
        $exampleData['field_country'] = [
            'value' => 'hn',
            'class' => Country::class,
        ];

        $id = $form->insert($exampleData);
        $record = $form->find($id);
        $this->assertTrue($record->getFieldDefinition('field_reference')->isEmpty());
    }
}
