<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Entity\Configuration\ConfigurationWriteInterface;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\ChangedFormManager;
use App\Forms\FormDatabaseUpdater;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test forms factory.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormFactoryTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
    }

    /**
     * Test that we can create a new form manager.
     *
     * And the manager configuration is editable.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testCreateFormManager(): void
    {
        // We can create a manager?
        $manager = $this->formFactory->create('test');
        $this->assertNotNull($manager);
        // Have the manager a editable configuration?
        $reflectionClass = new ReflectionClass($manager::class);
        $reflectionProperty = $reflectionClass->getProperty('config');
        $reflectionProperty->setAccessible(true);
        $managerConfig =  $reflectionProperty->getValue($manager);
        $this->assertEquals('App\Entity\Configuration\ConfigurationWrite', $managerConfig::class);
        $this->assertEquals('form.test', $manager->getId());
    }

    /**
     * Test that we can save and load a form manager.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testSaveFormManager(): void
    {
        // Create and save.
        $manager = $this->formFactory->create('test.save');
        $manager->saveNow();
        // Load to test.
        $confs = $this->configuration->find('form.test.save');
        $this->assertEquals('form.test.save', $confs->getValue()['id']);
        $this->assertEquals('App\Forms\FormManager', $confs->getValue()['type']);
    }

    /**
     * Test that we can delete a form.
     *
     * @return void
     */
    public function testDeleteForm(): void
    {
        // Create and save.
        $manager = $this->formFactory->create('test.delete');
        $manager->saveNow();
        // Delete it.
        $this->formFactory->delete('test.delete');
        // The configuration don't must exist.
        $confs = $this->configuration->find('form.test.delete');
        $this->assertNull($confs);
    }

    /**
     * Test that a tagged field type is discovered.
     *
     * @return void
     */
    public function testShortTextFieldTypesExist(): void
    {
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $this->assertArrayHasKey('short_text', $fieldTypes);
    }

    /**
     * Test that we can't add a bad field type in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInvalidFieldTypeInForm(): void
    {
        $manager = $this->formFactory->create('test.invalid.fieldtype');

        // Expected reaction.
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $expectedMessage = sprintf(
            'Field type "%s" not found. Available types: %s',
            'not_field_type',
            implode(', ', array_keys($fieldTypes))
        );
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expectedMessage);

        // Try to add the bad field type.
        $manager->addField('not_field_type', 'bad_field', 'Bad field');
    }

    /**
     * Test that we can't add an ID field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddIdFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.id.field');

        // Expected reaction.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('All forms have already an id field.');

        // Try to add the bad field type.
        $manager->addField('short_text', 'id', 'id');
    }

    /**
     * Test that we can't add an ID field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testDuplicateFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.duplicate.field');

        // Expected reaction.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Field "field_name" already exists.');

        // Try to add a field type twelve.
        $manager->addField('short_text', 'name', 'name');
        $manager->addField('short_text', 'name', 'name');
    }

    /**
     * Test that we can't alter field settings by getSettings().
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testWeCantAlterFieldSettingsByGetSettingsInForm(): void
    {
        $manager = $this->formFactory->create('test.weight.getsettings');
        $manager->addField('short_text', 'name', 'Name');
        $manager->saveNow();
        // Verify that we can't alter the settings this way.
        $manager = $this->formFactory->find('test.weight.getsettings');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $settings['settings']['new'] = 'bad-settings';
        $settings = $field->getSettings();
        $this->assertArrayNotHasKey('new', $settings['settings']);
    }

    /**
     * Test that we can add a field, and it will have weight.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddFieldWithDefaultWeightInForm(): void
    {
        $manager = $this->formFactory->create('test.default.weight');
        $manager->addField('short_text', 'name', 'Name');
        $manager->saveNow();
        // Verify that we have a weight settings in the field.
        $manager = $this->formFactory->find('test.default.weight');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('weight', $settings['settings']);
    }

    /**
     * Test that we can add a field, and it will have the set weight.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddFieldWithWeightInForm(): void
    {
        $manager = $this->formFactory->create('test.weight');
        $manager->addField('short_text', 'name', 'Name', ['weight' => 10]);
        $manager->saveNow();
        // Verify that we have a weight settings in the field.
        $manager = $this->formFactory->find('test.weight');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('weight', $settings['settings']);
        $this->assertEquals(10, $settings['settings']['weight']);
    }

    /**
     * Test that we can add valid field types in a form.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function testAddValidFieldTypeInForm(): void
    {
        $manager = $this->formFactory->create('test.valid.fieldtype');
        // Try to add the trust field type.
        $manager->addField('short_text', 'label', 'Section label');
        $manager->saveNow();

        // Get internal form configuration array.
        $manager = $this->formFactory->find('test.valid.fieldtype');
        $reflectionClass = new ReflectionClass($manager::class);
        $reflectionProperty = $reflectionClass->getProperty('config');
        $reflectionProperty->setAccessible(true);
        $managerConfig =  $reflectionProperty->getValue($manager);
        // Assert values.
        $values = $managerConfig->getValue();
        $this->assertArrayHasKey('fields', $values);
        $this->assertArrayHasKey('field_label', $values['fields']);
        $this->assertArrayHasKey('type', $values['fields']['field_label']);
        $this->assertEquals('short_text', $values['fields']['field_label']['type']);

        $this->assertArrayHasKey('id', $values['fields']['field_label']);
        $this->assertEquals('field_label', $values['fields']['field_label']['id']);

        $this->assertArrayHasKey('label', $values['fields']['field_label']);
        $this->assertEquals('Section label', $values['fields']['field_label']['label']);

        $this->assertArrayHasKey('settings', $values['fields']['field_label']);
        $this->assertIsArray($values['fields']['field_label']['settings']);
    }

    /**
     * Test that we can install a new form with a field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInstallFormManager(): void
    {
        // Create and save.
        $manager = $this->formFactory->create('test.field');
        $this->assertFalse($manager->isInstalled(), 'The form is already installed.');

        $result = $manager->addField('short_text', 'name', 'Section name');
        $this->assertNotNull($result, 'I can\'t install field_name.');

        $result = $manager->addField('short_text', 'label', 'Second field');
        $this->assertNotNull($result, 'I can\'t install field_label.');

        $manager->saveNow();

        $manager->install();

        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('field_label', $tableData->getColumns());
    }

    /**
     * Test that we can uninstall a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallFormManager(): void
    {
        // Create and save.
        $manager = $this->formFactory->create('test.uninstall');
        $this->assertFalse($manager->isInstalled(), 'The form is already installed.');
        $manager->saveNow();
        $manager->install();
        // Uninstall form.
        $manager = $this->formFactory->find('test.uninstall');
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $manager->uninstall();
        $this->assertFalse($manager->isInstalled(), 'The form is installed.');
        // Verify that is uninstalled.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $this->assertFalse($schemaManager->tablesExist($manager->getTableName()), 'The form is not uninstalled.');
    }

    /**
     * Test that we can create a form by separate steps.
     *
     * @return void
     *
     * @throws \ReflectionException
     */
    public function testIncrementalFormCreation(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.incremental');
        $form->saveNow();
        // Load and add field.
        $form = $this->formFactory->findEditable('test.incremental');
        $form->addField('short_text', 'text', 'label');
        $form->saveNow();
        // Load and add field.
        $form = $this->formFactory->findEditable('test.incremental');
        $form->addField('integer', 'integer', 'label', ['multivalued' => true]);
        $form->saveNow();
        // Install and verify database structure.
        $form->install();
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('field_text', $tableData->getColumns());
        // Verify that the multi valued table exist.
        $fields = $form->getFields();
        $field = $fields[1];
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_integer', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can add an multi valued field in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testAddMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.field');
        $manager->addField('short_text', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        // Verify that we have a multivalued settings in the field.
        $manager = $this->formFactory->find('test.multivalued.field');
        $fields = $manager->getFields();
        $field = $fields[0];
        $settings = $field->getSettings();
        $this->assertArrayHasKey('multivalued', $settings['settings']);

        // Verify that the form table don't have this field columns.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($manager->getTableName());
        $this->assertTrue($manager->isInstalled(), 'The form is not installed.');
        $this->assertArrayNotHasKey('field_name', $tableData->getColumns());

        // Verify that the multi valued table exist.
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableData = $schemaManager->listTableDetails($field->getFieldTableName());
        $this->assertTrue($schemaManager->tablesExist($field->getFieldTableName()));
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('record', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('primary', $tableData->getIndexes());
        $this->assertArrayHasKey('record', $tableData->getIndexes());
    }

    /**
     * Test that we can uninstall an form with multi valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUninstallMultiValuedFieldInForm(): void
    {
        $manager = $this->formFactory->create('test.multivalued.uninstall');
        $manager->addField('short_text', 'name', 'name', ['multivalued' => true]);
        $manager->saveNow();
        $manager->install();

        $manager = $this->formFactory->find('test.multivalued.uninstall');
        $formTableName = $manager->getTableName();

        $fields = $manager->getFields();
        $field = $fields[0];
        $fieldTableName = $field->getFieldTableName();

        $manager->uninstall();

        // Verify that the multi valued table don't exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $existTable = $schemaManager->tablesExist($formTableName);
        $this->assertFalse($existTable);
        $existTable = $schemaManager->tablesExist($fieldTableName);
        $this->assertFalse($existTable);
    }

    /**
     * Test that we can get all forms.
     *
     * @return void
     */
    public function testFindAllForms(): void
    {
        $form = $this->formFactory->create('test.findall.1');
        $form->saveNow();

        $form = $this->formFactory->create('test.findall.2');
        $form->saveNow();

        $forms = $this->formFactory->findAll();

        $this->assertCount(2, $forms);
        $this->assertArrayHasKey('form.test.findall.1', $forms);
        $this->assertArrayHasKey('form.test.findall.2', $forms);
    }

    /**
     * Test that if we add an indexable field, the index is created.
     */
    public function testIndexIsCreated(): void
    {
        $form = $this->formFactory->create('test.index', ChangedFormManager::class);
        $form->addField('short_text', 'name', 'Name')
            ->setDescription('This field must have a index.')
            ->setIndexable();
        $form->saveNow();
        $form->install();

        // Verify that the index exist.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        /**
         * Table data.
         *
         * @var \Doctrine\DBAL\Schema\Table $tableData
         */
        $tableIndexes = $schemaManager->listTableIndexes($form->getTableName());
        $this->assertArrayHasKey('ix_field_name', $tableIndexes);
        $this->assertArrayNotHasKey('ix_field_created', $tableIndexes);
        $this->assertArrayNotHasKey('ix_field_changed', $tableIndexes);
    }

    /**
     * Test that we can have a deprecated field.
     */
    public function testAddDeprecatedField(): void
    {
        $form = $this->formFactory->create('test.deprecated');
        $form->addField('short_text', 'name', 'Name')
            ->setDeprecated(true);
        $form->addField('short_text', 'label', 'Label');
        $form->saveNow();
        $form->install();

        // Verify installation.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        $this->assertTrue($form->isInstalled(), 'The form is not installed.');
        $this->assertArrayHasKey('id', $tableData->getColumns());
        $this->assertArrayHasKey('field_name', $tableData->getColumns());
        $this->assertArrayHasKey('field_label', $tableData->getColumns());
    }

    /**
     * Test that detect and add new fields.
     */
    public function testUpdaterAddNewFields(): void
    {
        // Create old form.
        $form = $this->formFactory->create('test.updater.addfield');
        $form->addField('short_text', 'field1', 'Field 1');
        $form->addField('short_text', 'field2', 'Field 2');
        $form->saveNow();
        $form->install();

        // Add new field.
        $old = $this->configuration->findEditable('form.test.updater.addfield')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.addfield')->getValue();
        $new['fields'] += [
            'field_changed' => [
                'id' => 'field_changed',
                'type' => 'date',
                'label' => 'New field',
                'description' => 'A new field.',
                'indexable' => true,
                'internal' => true,
                'deprecated' => '',
                'settings' => [
                    'required' => false,
                    'multivalued' => false,
                    'weight' => 0,
                    'meta' => [],
                ],
            ],
        ];

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();

        // Verify table.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        // Table columns added.
        $this->assertArrayHasKey('field_changed_value', $tableData->getColumns());
        $this->assertArrayHasKey('field_changed_timezone', $tableData->getColumns());
        // Table indexes added.
        $this->assertArrayHasKey('ix_field_changed_value', $tableData->getIndexes());
        $this->assertArrayHasKey('ix_field_changed_timezone', $tableData->getIndexes());
    }

    /**
     * Test that we can't change a field type.
     */
    public function testUpdaterChangeFieldType(): void
    {
        // Create old form.
        $form = $this->formFactory->create('test.updater.changetype');
        $form->addField('short_text', 'field1', 'Field 1');
        $form->addField('short_text', 'field2', 'Field 2');
        $form->saveNow();
        $form->install();

        // Add new field.
        $old = $this->configuration->findEditable('form.test.updater.changetype')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.changetype')->getValue();
        $new['fields']['field_field1']['type'] = 'date';

        // Prepare to the exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("[field_field1] A form field can't change the field type, from 'short_text' to 'date'.");

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();
    }

    /**
     * Test that we can't change multivalued field property.
     */
    public function testUpdaterChangeFieldMultivalued(): void
    {
        // Create old form.
        $form = $this->formFactory->create('test.updater.changemultivalued');
        $form->addField('short_text', 'field1', 'Field 1');
        $form->addField('short_text', 'field2', 'Field 2');
        $form->saveNow();
        $form->install();

        // Add new field.
        $old = $this->configuration->findEditable('form.test.updater.changemultivalued')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.changemultivalued')->getValue();
        $new['fields']['field_field1']['settings']['multivalued'] = true;

        // Prepare to the exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("[field_field1] A form field can't change the field multivalued property.");

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();
    }

    /**
     * Test that we can add/remove an field index.
     */
    public function testUpdaterChangeFieldIndexable(): void
    {
        // Create old form.
        $form = $this->formFactory->create('test.updater.indexable');
        $form->addField('short_text', 'field1', 'Field 1');
        $form->addField('short_text', 'field2', 'Field 2');
        $form->saveNow();
        $form->install();

        // Add new index.
        $old = $this->configuration->findEditable('form.test.updater.indexable')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.indexable')->getValue();
        $new['fields']['field_field1']['indexable'] = true;

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();

        // Verify table.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        // Table columns added.
        $this->assertArrayHasKey('field_field1', $tableData->getColumns());
        // Table indexes added.
        $this->assertArrayHasKey('ix_field_field1', $tableData->getIndexes());

        // Now, remove index again.
        /** @var ConfigurationWriteInterface $cfgNew */
        $cfgNew = $this->configuration->findEditable('form.test.updater.indexable');
        $cfgNew->setValue($new);
        $cfgNew->saveNow();

        $old = $this->configuration->findEditable('form.test.updater.indexable')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.indexable')->getValue();
        $new['fields']['field_field1']['indexable'] = false;

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();

        // Verify table.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        // Table columns added.
        $this->assertArrayHasKey('field_field1', $tableData->getColumns());
        // Table indexes added.
        $this->assertArrayNotHasKey('ix_field_field1', $tableData->getIndexes());
    }

    /**
     * Test that detect and remove fields.
     */
    public function testUpdaterRemoveFields(): void
    {
        // Create old form.
        $form = $this->formFactory->create('test.updater.removefield');
        $form->addField('short_text', 'field1', 'Field 1')
            ->setIndexable();
        $form->addField('short_text', 'field2', 'Field 2');
        $form->saveNow();
        $form->install();

        // Add new field.
        $old = $this->configuration->findEditable('form.test.updater.removefield')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.removefield')->getValue();
        unset($new['fields']['field_field1']);

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();

        // Verify table.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tableData = $schemaManager->listTableDetails($form->getTableName());
        // Table columns removed.
        $this->assertArrayNotHasKey('field_field1', $tableData->getColumns());
        // Table indexes added.
        $this->assertArrayNotHasKey('ix_field_field1', $tableData->getIndexes());
    }

    /**
     * Test that detect and remove multivalued fields.
     */
    public function testUpdaterRemoveMultivaluedFields(): void
    {
        // Create old form.
        $form = $this->formFactory->create('test.updater.removefieldmulti');
        $form->addField('short_text', 'field1', 'Field 1', ['multivalued' => true]);
        $form->addField('short_text', 'field2', 'Field 2');
        $form->saveNow();
        $form->install();

        // Add new field.
        $old = $this->configuration->findEditable('form.test.updater.removefieldmulti')->getValue();
        $new = $this->configuration->findEditable('form.test.updater.removefieldmulti')->getValue();
        unset($new['fields']['field_field1']);

        // Update.
        $updater = new FormDatabaseUpdater($this->formFactory, $old, $new);
        $updater->updateSchema();

        // Verify table.
        $doctrine = self::$container->get('database_connection');
        /**
         * Data base schema manager.
         *
         * @var AbstractSchemaManager $schemaManager
         */
        $schemaManager = $doctrine->getSchemaManager();
        $tablesData = $schemaManager->listTableNames();
        // Table columns removed.
        $this->assertNotContains('form_test_updater_removefield__field_field1', $tablesData);
    }
}
