<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Entity\Configuration;
use App\Entity\Country;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\ChangedFormManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\InitTestEnvironmentTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test forms managers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormManagerTest extends KernelTestCase
{
    use TestFixturesTrait;
    use InitTestEnvironmentTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected FormManagerInterface|null $form;
    protected array $data;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');

        $this->initConfigurations($this->configuration, [
            'form.mail' => dirname(__FILE__).'/../../assets/form.mail.yml',
        ]);

        // Create test form.
        $fields = [
            'reference' => [
                'type' => 'integer',
                'label' => 'Reference ID',
                'settings' => [
                    'required' => 'true',
                ],
            ],
            'name' => [
                'type' => 'short_text',
                'label' => 'Record name',
                'settings' => [],
            ],
            'range' => [
                'type' => 'integer',
                'label' => 'Range field',
                'settings' => [
                    'min' => 0,
                    'max' => 100,
                ],
            ],
        ];

        $this->data = [
            [
                'field_reference' => ['value' => 1],
                'field_name' => ['value' => 'Naranja'],
                'field_range' => ['value' => rand(0, 100)],
            ],
            [
                'field_reference' => ['value' => 2],
                'field_name' => ['value' => 'Pera'],
                'field_range' => ['value' => rand(0, 100)],
            ],
            [
                'field_reference' => ['value' => 3],
                'field_name' => ['value' => 'Limon'],
                'field_range' => ['value' => rand(0, 100)],
            ],
            [
                'field_reference' => ['value' => 4],
                'field_name' => ['value' => 'Manzana'],
                'field_range' => ['value' => rand(0, 100)],
            ],
            [
                'field_reference' => ['value' => 5],
                'field_name' => ['value' => 'Fresa'],
                'field_range' => ['value' => rand(0, 100)],
            ],
        ];

        $form = $this->formFactory->create('form.mail');
        $form->uninstall();
        $form->install();

        $this->form = $this->formFactory->create('test.form.manager');
        foreach ($fields as $fieldName => $field) {
            $this->form->addField($field['type'], $fieldName, $field['label'], $field['settings']);
        }
        $this->form->saveNow();
        if (!$this->form->isInstalled()) {
            $this->form->install();
        }
        foreach ($this->data as $record) {
            $this->form->insert($record);
        }

        // Subform.
        $this->loginUser('pedro');
        $form = $this->formFactory->create('form.subform', SubformFormManager::class);
        $form->addField('integer', 'Reference', 'Record reference');
        $form->saveNow();
        $form->uninstall();
        $form->install();
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $form->insert(['field_country' => $countryRepo->find('hn')]);
    }

    /**
     * Test that we can't insert outside range.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testIntegerOutsideRangeInForm(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[integer] Field "Range field [field_range]" have a invalid value, it must be 0 <= 2000 <= 100.');
        $this->form->insert(
            [
                'field_reference' => ['value' => 6],
                'field_name' => ['value' => 'Coche'],
                'field_range' => ['value' => 2000],
            ]
        );
    }

    /**
     * Test that we can find all records a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFindAllInForm(): void
    {
        // Find all.
        $resp = $this->form->findAll();
        $this->assertCount(5, $resp);

        // Verify that a record is readable.
        $this->assertIsString($resp[0]->id, '"ID" is not an string.');
        $this->assertEquals(1, $resp[0]->field_reference);
        $this->assertEquals('Naranja', $resp[0]->field_name);
        $this->assertIsNumeric($resp[0]->field_range, '"field_range" is not a number.');
    }

    /**
     * Test that we can find an record by ID a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFindInForm(): void
    {
        // Used to know the records IDs.
        $resp = $this->form->findAll();
        // Find by ID.
        $record = $this->form->find($resp[0]->id);
        $this->assertEquals($this->data[0]['field_name']['value'], $record->{'field_name'});
    }

    /**
     * Test that we can find one record by any criteria in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFindOneByInForm(): void
    {
        // Find one by.
        $record = $this->form->findOneBy(['field_name' => 'Limon']);
        $this->assertNotNull($record);
        $this->assertEquals('3', $record->{'field_reference'});
        $this->assertEquals('Limon', $record->{'field_name'});
        $this->assertIsNumeric($record->{'field_range'}, '"field_range" is not a number.');
    }

    /**
     * Test that we can find by any criteria, order, limit or offset in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testFindByInForm(): void
    {
        // Find by Criteria.
        $records = $this->form->findBy(['field_name' => 'Fresa']);
        $this->assertCount(1, $records);
        $this->assertEquals('5', $records[0]->field_reference);
        $this->assertEquals('Fresa', $records[0]->field_name);
        $this->assertIsNumeric($records[0]->field_range, '"field_range" is not a number.');

        // Find by with order.
        $records = $this->form->findBy([], ['field_name' => 'desc']);
        $this->assertCount(5, $records);
        $this->assertEquals('2', $records[0]->field_reference);
        $this->assertEquals('Pera', $records[0]->field_name);
        $this->assertIsNumeric($records[0]->field_range, '"field_range" is not a number.');

        // Find by with limit.
        $records = $this->form->findBy([], null, 1);
        $this->assertCount(1, $records);

        // Find by with offset.
        $records = $this->form->findBy([], null, null, 1);
        $this->assertCount(4, $records);
        $this->assertEquals('2', $records[0]->field_reference);
        $this->assertEquals('Pera', $records[0]->field_name);
        $this->assertIsNumeric($records[0]->field_range, '"field_range" is not a number.');
    }

    /**
     * Test that we can create a big form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testBigForm(): void
    {
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => 'Pedro']);
        $subRecord = $this->getSubformRecord();
        $fieldTypes = [
            [
                'type' => 'entity_reference',
                'value' => $user,
            ],
            [
                'type' => 'decimal',
                'value' => 3,
            ],
            [
                'type' => 'integer',
                'value' => 9,
            ],
            [
                'type' => 'short_text',
                'value' => 'Chaparral',
            ],
            [
                'type' => 'form_record_reference',
                'value' => $subRecord,
            ],
            [
                'type' => 'boolean',
                'value' => true,
            ],
        ];
        $form = $this->formFactory->create('test.big.form');
        // Add 200 fields.
        $typeIndex = 0;
        for ($i = 1; $i <= 525; ++$i) {
            $form->addField(
                $fieldTypes[$typeIndex]['type'],
                'field'.$i,
                sprintf('#%s %s', $i, $fieldTypes[$typeIndex]['type'])
            );
            if ($typeIndex >= 5) {
                $typeIndex = 0;
            } else {
                ++$typeIndex;
            }
        }
        $form->saveNow();
        $form->install();

        $form = $this->formFactory->find('test.big.form');
        $this->assertTrue($form->isInstalled());

        // Insert 1 record.
        $fields = $form->getFieldDefinitions();
        $createRecord = [];
        foreach ($fields as $fieldName => $field) {
            $createRecord[$fieldName] = $this->getValueByFieldType($field['type'], $fieldTypes);
        }
        $form->insert($createRecord);

        /**
         * Form record.
         *
         * @var FormRecord $record
         */
        $record = $form->findOneBy([]);
        $this->assertNotNull($record);

        // Verify record completion.
        foreach ($record as $fieldName => $value) {
            if ('id' === $fieldName) {
                continue;
            }
            $fieldType = $record->getFieldDefinition($fieldName)->getFieldType();
            switch ($fieldType) {
                case 'form_record_reference':
                case 'entity_reference':
                    $this->assertEquals(
                        $this->getValueByFieldType($fieldType, $fieldTypes)->getId(),
                        $record->{$fieldName}->getId()
                    );
                    break;

                default:
                    $this->assertEquals($this->getValueByFieldType($fieldType, $fieldTypes), $record->{$fieldName});
                    break;
            }
        }
    }

    /**
     * Test that we can update a record.
     *
     * The FormRecord call the FormManager::Update method.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateFormRecord(): void
    {
        // Prepare form.
        $form = $this->formFactory->create('test.update.record');
        $form->addField('short_text', 'label', 'Section label');
        $form->saveNow();
        $form->install();
        // Insert record, and load it.
        $form->insert(
            [
                'field_label' => 'Symfony',
            ]
        );
        $record = $form->findOneBy([]);
        $id = $record->getId();
        $this->assertEquals('Symfony', $record->{'field_label'});
        // Update record.
        $record->{'field_label'} = 'Drupal';
        $record->save();
        // Load again and verify the change.
        $record = $form->findOneBy([]);
        $this->assertEquals('Drupal', $record->{'field_label'});
        $this->assertEquals($id, $record->getId());
    }

    /**
     * Test that we can delete a record.
     *
     * The FormRecord call the FormManager::Update method.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testDeleteFormRecord(): void
    {
        // Prepare form.
        $form = $this->formFactory->create('test.delete.record');
        $form->addField('short_text', 'label', 'Section label');
        $form->saveNow();
        $form->install();
        // Insert record, and load it.
        $form->insert(
            [
                'field_label' => 'Symfony',
            ]
        );
        $record = $form->findOneBy([]);
        $this->assertNotNull($record);
        // Delete the record.
        $record->delete();
        // Exist record now?
        $record = $form->findOneBy([]);
        $this->assertEmpty($record);
    }

    /**
     * Test that we can't insert a not valid data in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testNoValidInsertInForm(): void
    {
        // Create and install the form.
        $manager = $this->formFactory->create('test.not.valid.data');
        $manager->addField('short_text', 'name', 'Sample name', ['max-length' => 1]);
        $manager->saveNow();
        $manager->install();
        // Prepare asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[short_text] Field "Sample name [field_name]" will be truncated. Data length is 16, and the limit is 1.');
        // Insert a record.
        $manager->insert(
            [
                'field_name' => ['value' => 'Viva la vijen !!'],
            ]
        );
    }

    /**
     * Test that we can't insert an empty required in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testNoEmptyRequiredInsertInForm(): void
    {
        // Create and install the form.
        $manager = $this->formFactory->create('test.required.field');
        $manager->addField('short_text', 'name', 'Sample name', ['required' => 'true']);
        $manager->saveNow();
        $manager->install();
        // Prepare asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[short_text] Field "Sample name [field_name]" is required.');
        // Insert a record.
        $manager->insert(
            [
                'field_name' => ['value' => ''],
            ]
        );
    }

    /**
     * Test that we can insert in a form.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertInForm(): void
    {
        // Create and install the form.
        $manager = $this->formFactory->create('test.insert');
        $manager->addField('short_text', 'name', 'Sample name');
        $manager->saveNow();
        $manager->install();
        // Insert a record.
        $manager->insert(
            [
                'field_name' => [
                    'value' => 'Un cazador almonteño la encontró en el tronco de un árbol.',
                ],
            ]
        );
        // Verify that the data is saved.
        $doctrine = self::$container->get('database_connection');
        /**
         * Doctrine query builder.
         *
         * @var QueryBuilder $query
         */
        $query = $doctrine->createQueryBuilder()
            ->select('*')
            ->from($manager->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();
        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $this->assertArrayHasKey('field_name', $result[0]);
        $this->assertEquals('Un cazador almonteño la encontró en el tronco de un árbol.', $result[0]['field_name']);
    }

    /**
     * Test that we can set metadata in forms.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testMetadataInForm(): void
    {
        // Build with metadata.
        $form = $this->formFactory->create('test.metadata');
        $form->setMeta(
            [
                'version' => '12.4 – April 2021',
                'title' => 'SCHOOLS QUESTIONNAIRE',
            ]
        );
        $form->saveNow();

        // Saved?
        $form = $this->formFactory->find('test.metadata');
        $meta = $form->getMeta();

        $this->assertArrayHasKey('version', $meta);
        $this->assertArrayHasKey('title', $meta);

        $this->assertEquals('12.4 – April 2021', $meta['version']);
        $this->assertEquals('SCHOOLS QUESTIONNAIRE', $meta['title']);

        // Update metas.
        $form = $this->formFactory->findEditable('test.metadata');
        $meta = $form->getMeta();
        $meta['extras'] = 'Algún dato más';
        $form->setMeta($meta);
        $form->saveNow();

        // Saved?
        $form = $this->formFactory->find('test.metadata');
        $meta = $form->getMeta();

        $this->assertArrayHasKey('version', $meta);
        $this->assertArrayHasKey('title', $meta);
        $this->assertArrayHasKey('extras', $meta);

        $this->assertEquals('12.4 – April 2021', $meta['version']);
        $this->assertEquals('SCHOOLS QUESTIONNAIRE', $meta['title']);
        $this->assertEquals('Algún dato más', $meta['extras']);
    }

    /**
     * Test that a form can have title and/or description.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testTitleDescriptionInForm(): void
    {
        $form = $this->formFactory->create('test.title.description');
        $form->setTitle('Pruebas');
        $form->setDescription('Formularios para almacenar los resultados de las distintas pruebas.');
        $form->saveNow();

        $form = $this->formFactory->find('test.title.description');
        $this->assertEquals('Pruebas', $form->getTitle());
        $this->assertEquals(
            'Formularios para almacenar los resultados de las distintas pruebas.',
            $form->getDescription()
        );
    }

    /**
     * Test that we can set, and read, field label and description.
     */
    public function testFieldLabelAndDescription(): void
    {
        $form = $this->formFactory->create('test.field.label.description');
        $form->addField('short_text', 'dummy', 'Label 1');
        $form->saveNow();

        // Reload the new form.
        $form = $this->formFactory->findEditable('test.field.label.description');
        $field = $form->getFieldDefinition('field_dummy');
        $this->assertEquals('Label 1', $field->getLabel());

        // Set new label and description.
        $field->setLabel('Label 2');
        $field->setDescription('Description 1');
        $form->saveNow();

        // Test the new texts.
        $form = $this->formFactory->find('test.field.label.description');
        $field = $form->getFieldDefinition('field_dummy');
        $this->assertEquals('Label 2', $field->getLabel());
        $this->assertEquals('Description 1', $field->getDescription());
    }

    /**
     * Test that we can insert records without not required fields.
     *
     * @throws \Exception
     */
    public function testSimpleNotRequiredFieldUseDefaults(): void
    {
        $subForm = $this->formFactory->create('form.subform20', SubformFormManager::class);
        $subForm->addField('integer', 'Reference', 'Record reference');
        $subForm->setMeta(['parent_form' => 'form.test.default.values']);
        $subForm->saveNow();
        $subForm->uninstall();
        $subForm->install();
        $form = $this->formFactory->create('test.default.values');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_inter_tp_reference':
                case 'water_quality_entity_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform20';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'map_point':
                    $form->addField('decimal', 'lon', 'location_lon');
                    $form->addField('decimal', 'lat', 'location_lat');
                    $options['lat_field'] = 'field_lat';
                    $options['lon_field'] = 'field_lon';
                    break;
                case 'editor_update':
                    $form->addField('user_reference', 'ur', 'ur');
                    $form->addField('date', 'updated', 'date');
                    $options['editor_field'] = 'field_ur';
                    $options['update_field'] = 'field_updated';
                    break;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
            ++$i;
        }
        $form->saveNow();
        $form->install();
        $id = $form->insert([]);
        $this->assertIsString($id);
    }

    /**
     * Test that we can insert records without not required multivalued fields.
     *
     * @throws \Exception
     */
    public function testMultivaluedNotRequiredFieldUseDefaults(): void
    {
        $subForm = $this->formFactory->create('form.subform30', SubformFormManager::class);
        $subForm->addField('integer', 'Reference', 'Record reference');
        $subForm->setMeta(['parent_form' => 'form.test.def.mv.vals']);
        $subForm->saveNow();
        $subForm->uninstall();
        $subForm->install();
        $form = $this->formFactory->create('test.def.mv.vals');
        $form->addField('country_reference', 'refcountry', 'Ref. country');
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [
                'multivalued' => true,
            ];
            // Set required field options.
            $continueFor = false;
            switch ($fieldTypeId) {
                case 'publishing_status':
                case 'inquiring_status':
                case 'pointing_status':
                case 'map_point':
                case 'editor_update':
                    $continueFor = true;
                    break;
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform30';
                    }
                    $options['country_field'] = 'field_refcountry';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_refcountry';
                    break;
            }
            if ($continueFor) {
                continue;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
            ++$i;
        }
        $form->saveNow();
        $form->install();
        $id = $form->insert([]);
        $this->assertIsString($id);
    }

    /**
     * Test that we can create a FormManager other than the default class.
     */
    public function testCreatedOtherFormManagerClass(): void
    {
        // Creation.
        $form = $this->formFactory->create('test.manager.class', ChangedFormManager::class);
        $form->addField('short_text', 'name', 'Name');
        $form->saveNow();
        $form->install();

        // Load.
        $form = $this->formFactory->find('test.manager.class');

        $this->assertInstanceOf(ChangedFormManager::class, $form);

        // Has the base fields?
        $field = $form->getFieldDefinition('field_changed');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_created');
        $this->assertNotNull($field);

        // Insert data.
        $id = $form->insert([
            'field_name' => 'Pedro',
        ]);
        $record = $form->find($id);

        $this->assertEquals('Pedro', $record->{'field_name'});
        $this->assertInstanceOf(\DateTime::class, $record->{'field_changed'});
        $this->assertInstanceOf(\DateTime::class, $record->{'field_created'});
    }

    /**
     * Test that we can change required fields to not required.
     *
     * @throws \Exception
     */
    public function testChangeRequiredToNotRequired(): void
    {
        $subForm = $this->formFactory->create('form.subform40', SubformFormManager::class);
        $subForm->addField('integer', 'Reference', 'Record reference');
        $subForm->setMeta(['parent_form' => 'form.test.required']);
        $subForm->saveNow();
        $subForm->uninstall();
        $subForm->install();
        $this->loginUser('Pedro');
        // Create form with required fields.
        // InquiryFormManager inserting a draft record must disable readonly attribute.
        $form = $this->formFactory->create('test.required', InquiryFormManager::class);
        $fieldTypes = $this->fieldTypeManager->getFieldTypes();
        $i = 1;
        foreach ($fieldTypes as $fieldTypeId => $fieldType) {
            $options = [
                'required' => true,
            ];
            // Set required field options.
            switch ($fieldTypeId) {
                case 'radio_select':
                case 'select':
                    $options['options'] = [
                        1 => 'A',
                        2 => 'B',
                    ];
                    break;
                case 'administrative_division_reference':
                case 'treat_tech_desali_reference':
                case 'treat_tech_coag_floccu_reference':
                case 'treat_tech_filtra_reference':
                case 'treat_tech_aera_oxida_reference':
                case 'treat_tech_sedimen_reference':
                case 'disinfect_substance_reference':
                case 'typo_chlori_install_reference':
                case 'tap_reference':
                case 'language_reference':
                case 'ethnicity_reference':
                case 'program_reference':
                case 'funder_intervention_reference':
                case 'type_intervention_reference':
                case 'geographical_scope_reference':
                case 'type_tap_reference':
                case 'intervention_status_reference':
                case 'water_quality_entity_reference':
                case 'water_quality_inter_tp_reference':
                case 'type_health_facility_reference':
                case 'currency':
                case 'special_component_reference':
                case 'institution_reference':
                case 'default_diameter_reference':
                case 'material_reference':
                case 'distribution_material_reference':
                case 'community_service_reference':
                case 'pump_type_reference':
                case 'wsprovider_reference':
                case 'wsystem_reference':
                case 'functions_carried_out_tap_reference':
                case 'functions_carried_out_wsp_reference':
                case 'community_reference':
                case 'subform_reference':
                case 'household_process_reference':
                case 'point_reference':
                    if ('subform_reference' === $fieldTypeId) {
                        $options['subform'] = 'form.subform40';
                    }
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'thread_mail_reference':
                    $options['subform'] = 'form.mail';
                    $options['country_field'] = 'field_country_reference';
                    break;
                case 'map_point':
                    $form->addField('decimal', 'lon', 'location_lon');
                    $form->addField('decimal', 'lat', 'location_lat');
                    $options['lat_field'] = 'field_lat';
                    $options['lon_field'] = 'field_lon';
                    break;
                case 'editor_update':
                    $form->addField('user_reference', 'ur', 'ur');
                    $form->addField('date', 'updated', 'date');
                    $options['editor_field'] = 'field_ur';
                    $options['update_field'] = 'field_updated';
                    break;
            }
            $form->addField($fieldTypeId, $fieldTypeId, 'Field '.$fieldTypeId, $options);
            ++$i;
        }
        $form->saveNow();
        $form->install();

        // Insert defaults.
        $form = $this->formFactory->find('test.required');
        $id = $form->insert([]);
        $this->assertIsString($id);

        // Validate that required work ok if it's not a draft.
        $form = $this->formFactory->find('test.required');

        $this->expectException(\Exception::class);

        $form->insert(['field_status' => 'finished']);
    }

    /**
     * Create a dummy form record to use how base test.
     *
     * @return FormRecord
     *
     * @throws \Exception
     */
    protected function getSubformRecord(): FormRecord
    {
        // Using only one field we can create and filter with the same array.
        $data = ['field_reference' => random_int(1, 100)];
        // Create the form.
        $form = $this->formFactory->create('test.subform');
        $form->addField('integer', 'Reference', 'Record reference');
        $form->saveNow();
        // Install it.
        $form->install();
        // Create and insert record.
        $form->insert($data);
        // Load the new record.
        $records = $form->findOneBy($data);
        // Return it.
        return $records;
    }

    /**
     * Find and return a value by field type.
     *
     * @param string $type Field type.
     * @param array  $data Dummy data.
     *
     * @return mixed
     */
    protected function getValueByFieldType(string $type, array $data): mixed
    {
        foreach ($data as $def) {
            if ($def['type'] === $type) {
                return $def['value'];
            }
        }

        return null;
    }
}
