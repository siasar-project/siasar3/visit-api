<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\EntityManager;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;

/**
 * Test forms records.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormRecordTest extends RoleTestBase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected FormManagerInterface|null $form;
    protected array $data;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
        $this->formFactory = self::$container->get('form_factory');
        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can do foreach over FormRecord fields.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testDoForeachInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record');
        $form->addField('integer', 'Reference', 'Record reference');
        $form->addField('integer', 'field2', 'Field 2');
        $form->addField('integer', 'field3', 'Field 3');
        $form->addField('integer', 'field4', 'Field 4');
        $form->saveNow();
        $form->install();

        $form->insert(
            [
                'field_reference' => 1,
                'field_field2' => 2,
                'field_field3' => 3,
                'field_field4' => 4,
            ]
        );
        // Load the record.
        $record = $form->findOneBy(['field_reference' => 1]);
        // Prepare a validation array.
        $fields = [
            'id' => true,
            'field_reference' => true,
            'field_field2' => true,
            'field_field3' => true,
            'field_field4' => true,
        ];
        // Verify that we can get all fields.
        $fieldCount = 0;
        foreach ($record as $fieldName => $value) {
            ++$fieldCount;
            // Is it a record field?
            $this->assertArrayHasKey($fieldName, $fields);
        }
        // See we all fields?
        $this->assertEquals(5, $fieldCount);
    }

    /**
     * Test that we can't insert an empty multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertEmptyMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.empty.multivalued');
        $form->addField(
            'integer',
            'Reference',
            'Record reference',
            [
                'multivalued' => true,
                'required' => true,
            ]
        );
        $form->saveNow();
        $form->install();
        // Prepare asserts.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[integer] Field "Record reference [field_reference]" is required.');

        // Create a new record.
        $form = $this->formFactory->find('test.empty.multivalued');
        $recordId = $form->insert([]);
    }

    /**
     * Test that we can insert a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testInsertMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued');
        $form->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued');
        $recordId = $form->insert(
            [
                'field_reference' => [1, 2, 3, 4, 5],
            ]
        );
        // Verify that we have a new database id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(5, $result);
        // $index + 1 must be the field_reference value.
        // See $form->insert some lines before.
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsNumeric($item['field_reference']);
            $this->assertEquals($index + 1, $item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.form.record.with.multivalued.a');
        $form->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.form.record.with.multivalued.a');
        $recordId = $form->insert(
            [
                'field_reference' => [1, 2, 3, 4, 5],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_reference'} = [3, 4, 5];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        // $index + 1 must be the field_reference value.
        // See $form->insert some lines before.
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsNumeric($item['field_reference']);
            $this->assertEquals($index + 3, $item['field_reference']);
        }
    }

    /**
     * Test that we can update a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUpdateMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.record.single.multivalued');
        $form->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.record.single.multivalued');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [1, 2, 3, 4, 5],
            ]
        );
        // Update the field value.
        $record = $form->find($recordId);
        $record->{'field_single'} = 2;
        $record->{'field_reference'} = [3, 4, 5];
        $record->save();
        // Verify that we have a new data base id.
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(1, $result);
        $this->assertArrayHasKey('id', $result[0]);
        $ulid = Ulid::fromString($result[0]['id']);
        $this->assertEquals($recordId, $ulid->toBase32());
        $this->assertArrayHasKey('field_single', $result[0]);
        $this->assertEquals(2, $result[0]['field_single']);

        // Verify that the multivalued table have the data.
        $field = $form->getFieldDefinition('field_reference');
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($field->getFieldTableName(), 'u')
            ->orderBy('field_reference', 'ASC');
        $result = $query->execute()->fetchAllAssociative();

        $this->assertCount(3, $result);
        // $index + 1 must be the field_reference value.
        // See $form->insert some lines before.
        foreach ($result as $index => $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('record', $item);
            $ulid = Ulid::fromString($item['record']);
            $this->assertEquals($recordId, $ulid->toBase32());

            $this->assertArrayHasKey('field_reference', $item);
            $this->assertIsNumeric($item['field_reference']);
            $this->assertEquals($index + 3, $item['field_reference']);
        }
    }

    /**
     * Test that we can read a multivalued field with a single valued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testReadMultivaluedWithSingleValuedFieldInFormRecord(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.read.single.multivalued');
        $form->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.read.single.multivalued');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [1, 2, 3, 4, 5],
            ]
        );
        // Read data.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_single'});
        // Read end value.
        foreach ($record->{'field_reference'} as $key => $item) {
            $this->assertIsNumeric($item);
            $this->assertEquals($key + 1, $item);
        }
        // Read internal values.
        $references = $record->get('field_reference');
        foreach ($record->get('field_reference') as $key => $item) {
            $this->assertIsString($item);
            $this->assertEquals($key + 1, $item);
        }
    }

    /**
     * Test that we can remove a multivalued field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testDropFormRecordWithMultivalued(): void
    {
        // Prepare a form with a record.
        $form = $this->formFactory->create('test.drop.multivalued');
        $form->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $form->addField('integer', 'single', 'Single field');
        $form->saveNow();
        $form->install();

        // Create a new record.
        $form = $this->formFactory->find('test.drop.multivalued');
        $recordId = $form->insert(
            [
                'field_single' => 1,
                'field_reference' => [1, 2, 3, 4, 5],
            ]
        );

        // Drop the new record.
        $form->drop($recordId);

        // Validate in database.
        $recordUlid = Ulid::fromString($recordId);
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $query->andWhere($query->expr()->eq('HEX(id)', '"'.strtoupper(bin2hex($recordUlid->toBinary())).'"'));
        $result = $query->execute()->fetchAllAssociative();
        $this->assertCount(0, $result);

        $fields = $form->getFields();
        foreach ($fields as $field) {
            if ($field->isMultivalued()) {
                $query = $this->doctrine->createQueryBuilder()
                    ->select('*')
                    ->from($field->getFieldTableName(), $field->getFieldTableAlias());
                $query->andWhere($query->expr()->eq('HEX(record)', '"'.strtoupper(bin2hex($recordUlid->toBinary())).'"'));
                $result = $query->execute()->fetchAllAssociative();
                $this->assertCount(0, $result);
            }
        }
    }

    /**
     * Test that we can remove a subform record.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testDropSubFormRecordWithMultivalued(): void
    {
        $this->loginUser('Pedro');
        // Prepare a form with a record.
        $subForm = $this->formFactory->create('test.drop.subform', SubformFormManager::class);
        $subForm->setMeta(['parent_form' => 'form.test.drop.form']);
        $subForm->addField('integer', 'Reference', 'Record reference', ['multivalued' => true]);
        $subForm->addField('integer', 'single', 'Single field');
        $subForm->saveNow();
        $subForm->uninstall();
        $subForm->install();

        $form = $this->formFactory->create('test.drop.form', InquiryFormManager::class);
        $form->addField(
            'subform_reference',
            'multi',
            'multi',
            [
                'multivalued' => true,
                'subform' => 'form.test.drop.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->addField(
            'subform_reference',
            'single',
            'single',
            [
                'subform' => 'form.test.drop.subform',
                'country_field' => 'field_country',
            ]
        );
        $form->saveNow();
        $form->uninstall();
        $form->install();

        // Create a new record.
        $subForm = $this->formFactory->find('test.drop.subform');
        // Create three new subform records.
        $subRecords = [
            'id1' => $subForm->insert(
                [
                    'field_single' => 1,
                    'field_reference' => [1, 2, 3, 4, 5],
                ]
            ),
            'id2' => $subForm->insert(
                [
                    'field_single' => 1,
                    'field_reference' => [1, 2, 3, 4, 5],
                ]
            ),
            'id3' => $subForm->insert(
                [
                    'field_single' => 1,
                    'field_reference' => [1, 2, 3, 4, 5],
                ]
            ),
        ];
        $form = $this->formFactory->find('test.drop.form');
        // Create a new record using subform records.
        $recordId = $form->insert(
            [
                'field_single' => [
                    'value' => $subRecords['id1'],
                    'form' => 'form.test.drop.subform',
                ],
                'field_multi' => [
                    [
                        'value' => $subRecords['id2'],
                        'form' => 'form.test.drop.subform',
                    ],
                    [
                        'value' => $subRecords['id3'],
                        'form' => 'form.test.drop.subform',
                    ],
                ],
            ]
        );

        // Drop the new record.
        $form->drop($recordId);

        // Validate record drop.
        $recordUlid = Ulid::fromString($recordId);
        $query = $this->doctrine->createQueryBuilder()
            ->select('*')
            ->from($form->getTableName(), 'u');
        $query->andWhere($query->expr()->eq('HEX(id)', '"'.strtoupper(bin2hex($recordUlid->toBinary())).'"'));
        $result = $query->execute()->fetchAllAssociative();
        $this->assertCount(0, $result);

        $fields = $form->getFields();
        foreach ($fields as $field) {
            if ($field->isMultivalued()) {
                $query = $this->doctrine->createQueryBuilder()
                    ->select('*')
                    ->from($field->getFieldTableName(), $field->getFieldTableAlias());
                $query->andWhere($query->expr()->eq('HEX(record)', '"'.strtoupper(bin2hex($recordUlid->toBinary())).'"'));
                $result = $query->execute()->fetchAllAssociative();
                $this->assertCount(0, $result);
            }
        }

        // Validate sub-record drops.
        foreach ($subRecords as $sRId) {
            $srUlid = Ulid::fromString($sRId);
            $query = $this->doctrine->createQueryBuilder()
                ->select('*')
                ->from($subForm->getTableName(), 'u');
            $query->andWhere($query->expr()->eq('HEX(id)', '"'.strtoupper(bin2hex($srUlid->toBinary())).'"'));
            $result = $query->execute()->fetchAllAssociative();
            $this->assertCount(0, $result);

            $fields = $subForm->getFields();
            foreach ($fields as $field) {
                if ($field->isMultivalued()) {
                    $query = $this->doctrine->createQueryBuilder()
                        ->select('*')
                        ->from($field->getFieldTableName(), $field->getFieldTableAlias());
                    $query->andWhere($query->expr()->eq('HEX(record)', '"'.strtoupper(bin2hex($srUlid->toBinary())).'"'));
                    $result = $query->execute()->fetchAllAssociative();
                    $this->assertCount(0, $result);
                }
            }
        }
    }
}
