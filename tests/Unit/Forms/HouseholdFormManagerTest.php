<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\HouseholdProcess;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Repository\ConfigurationRepository;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Uid\Ulid;

/**
 * Test household forms managers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class HouseholdFormManagerTest extends RoleTestBase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected FormManagerInterface|null $form;
    protected array $data;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->extraConfigurations = [
            'form.community.household' => dirname(__FILE__).'/../../assets/form.community.household.yml',
        ];
        parent::setUp();

        $this->fieldTypeManager = static::getContainerInstance()->get('fieldtype_manager');
        $this->formFactory = static::getContainerInstance()->get('form_factory');

        // Basic tests.
        $this->loginUser('Pedro');
        $this->sessionManager->resetSession();

        foreach ($this->extraConfigurations as $formId => $file) {
            $form = $this->formFactory->find($formId);
            $form->uninstall();
            $form->install();
        }
    }

    /**
     * Test that we  can create household.
     */
    public function testInsertHouseholdOk(): void
    {
        $form = $this->formFactory->find('form.community.household');
        $form->insert([]);
        $this->assertEquals(1, $form->countBy([]));
    }

    /**
     * Test that we can't finish household record without required fields.
     */
    public function testFinishHouseHoldFail(): void
    {
        // Create HouseholdProcess instance.
        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $countryRepo = $this->entityManager->getRepository(Country::class);

        $admDiv = $divisionRepo->find('01FFFBAWKRKV16EPSW3BDDZEX9');
        /** @var HouseholdProcess $householdProcess */
        $householdProcess = new HouseholdProcess();
        $householdProcess->setCommunityReference(new Ulid());
        $householdProcess->setCountry($countryRepo->find('hn'));
        $householdProcess->setAdministrativeDivision($admDiv);
        $householdProcessRepo->saveNow($householdProcess);
        // Create household record.
        $form = $this->formFactory->find('form.community.household');
        $recordId = $form->insert(
            [
                'field_household_process' => $householdProcess,
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_finished'} = true;

        // Required field stop saving.
        $this->expectException(\Exception::class);

        $record->save();
    }

    /**
     * Test that we can finish household record with required fields.
     */
    public function testFinishHouseHoldOk(): void
    {
        // Get a user.
        $userRepo = $this->entityManager->getRepository(User::class);
        $user = current($userRepo->findBy(['username' => 'Pedro']));
        // Create HouseholdProcess instance.
        $householdProcessRepo = $this->entityManager->getRepository(HouseholdProcess::class);
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $countryRepo = $this->entityManager->getRepository(Country::class);

        $admDiv = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');
        /** @var HouseholdProcess $householdProcess */
        $householdProcess = new HouseholdProcess();
        $householdProcess->setCommunityReference(new Ulid());
        $householdProcess->setCountry($countryRepo->find('hn'));
        $householdProcess->setAdministrativeDivision($admDiv);
        $householdProcessRepo->saveNow($householdProcess);
        // Create household record.
        $form = $this->formFactory->find('form.community.household');
        $recordId = $form->insert(
            [
                'field_household_process' => $householdProcess,
                'field_interviewer' => $user,
                'field_community' => $admDiv,
                'field_toilet_facility' => 5,
                'field_show_wash_hands' => 5,
            ]
        );
        $record = $form->find($recordId);
        $record->{'field_finished'} = true;
        $record->save();

        // Validate
        $record = $form->find($recordId);
        $this->assertEquals($user->getId(), $record->{'field_interviewer'}->getId());
    }
}
