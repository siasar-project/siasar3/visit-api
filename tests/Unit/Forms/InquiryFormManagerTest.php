<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\InquiryFormLog;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Repository\AdministrativeDivisionRepository;
use App\Repository\ConfigurationRepository;
use App\Repository\CountryRepository;
use App\Service\SessionService;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Test inquiry forms managers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class InquiryFormManagerTest extends RoleTestBase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected FormManagerInterface|null $form;
    protected array $data;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fieldTypeManager = static::getContainerInstance()->get('fieldtype_manager');
        $this->formFactory = static::getContainerInstance()->get('form_factory');

        // Basic tests.
        try {
            $form = $this->formFactory->find('test.inquiry');
        } catch (\Exception $e) {
            $form = $this->formFactory->create('test.inquiry', InquiryFormManager::class);
            $form->addField('short_text', 'name', 'Name');
            $form->saveNow();
            $form->install();
        } finally {
            $this->loginUser('Pedro');
            $this->sessionManager->resetSession();
            $form->uninstall();
            $form->install();
            $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
            $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
            $form->insert([
                'field_reference' => 'To update',
                'field_region' => $division,
            ]);
        }
        // Flow tests.
        try {
            $form = $this->formFactory->find('test.inquiry.flow');
            $form->uninstall();
            $form->install();
        } catch (\Exception $e) {
            $form = $this->formFactory->create('test.inquiry.flow', InquiryFormManager::class);
            $form->addField('short_text', 'name', 'Name');
            $form->saveNow();
            $form->install();
        }
    }

    /**
     * Test that we can create a FormManager other than the default class.
     */
    public function testCreatedInquiryFormManagerClass(): void
    {
        // Init admin user session.
        $this->loginUser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');

        $this->assertInstanceOf(InquiryFormManager::class, $form);

        // Has the base fields?
        $field = $form->getFieldDefinition('field_changed');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_created');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_country');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_region');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_creator');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_editors');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_reference');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_ulid_reference');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_validated');
        $this->assertNotNull($field);

        $field = $form->getFieldDefinition('field_deleted');
        $this->assertNotNull($field);

        // Insert data.
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert([
            'field_reference' => 'A',
            'field_name' => 'Pedro',
            'field_region' => $division,
        ]);
        $record = $form->find($id);

        $this->assertEquals('Pedro', $record->{'field_name'});
        $this->assertInstanceOf(\DateTime::class, $record->{'field_changed'});
        $this->assertInstanceOf(\DateTime::class, $record->{'field_created'});
        $this->assertInstanceOf(Country::class, $record->{'field_country'});
        $this->assertInstanceOf(AdministrativeDivision::class, $record->{'field_region'});
        $this->assertInstanceOf(User::class, $record->{'field_creator'});
        $this->assertIsArray($record->{'field_editors'});
        $this->assertIsString($record->{'field_reference'});
        $this->assertIsString($record->{'field_ulid_reference'});
        $this->assertIsBool($record->{'field_validated'});
        $this->assertIsBool($record->{'field_deleted'});
    }

    /**
     * Test that a ROLE_USER only can't create inquiry.
     */
    public function testUserInsertInquiryFail(): void
    {
        // Init user session.
        $this->loginuser('SectorialValidator');

        // Load.
        $form = $this->formFactory->find('test.inquiry');

        // Insert data.
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The "create" action require the permission "create inquiry record".');

        $form->insert([
            'field_reference' => 'A',
            'field_name' => 'Marta',
            'field_region' => $division,
        ]);
    }

    /**
     * Test that a ROLE_SUPERVISOR can create inquiry.
     */
    public function testUserInsertInquiryOk(): void
    {
        // Init user session.
        $this->loginUser('Supervisor');

        // Load.
        $form = $this->formFactory->find('test.inquiry');

        // Insert data.
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        $id = $form->insert([
            'field_reference' => 'A',
            'field_name' => 'Supervisor',
            'field_region' => $division,
        ]);

        $this->assertIsString($id);

        // Test logger. (No more inquiry logging)
//        $record = $form->find($id);
//        $logRepository = $this->entityManager->getRepository(InquiryFormLog::class);
//        $logs = $logRepository->findBy([]);
//        $logged = false;
//        foreach ($logs as $log) {
//            if (is_array($log->getContext()) && array_key_exists('received', $log->getContext())) {
//                if ($log->getContext()['received']['field_reference'] === $record->{'field_reference'}) {
//                    $logged = true;
//                    break;
//                }
//            }
//        }
//        $this->assertTrue($logged, 'Inquiry record inserted but not logged.');
    }

    /**
     * Test that a ROLE_USER only can't create inquiry.
     */
    public function testUserUpdateInquiryFail(): void
    {
        // Init user session.
        $this->loginuser('Nico');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        // Find record to update.
        $record = current($form->findBy(['field_reference' => 'To update']));

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        /** @var FormRecord $record */
        $record->{'field_region'} = $division;

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The "update" action require the permission "update inquiry record".');

        $record->save();
    }

    /**
     * Test that a ROLE_USER only can't create inquiry.
     */
    public function testUserUpdateInquiryOk(): void
    {
        // Init user session.
        $this->loginuser('Supervisor');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        // Find record to update.
        $record = current($form->findBy(['field_reference' => 'To update']));

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

        /** @var FormRecord $record */
        $record->{'field_region'} = $division;
        $record->save();
        $record = $form->find($record->getId());

        $this->assertEquals($division->getId(), $record->{'field_region'}->getId());

        // Test logger. (No more inquiry logging)
//        $logRepository = $this->entityManager->getRepository(InquiryFormLog::class);
//        $logs = $logRepository->findBy([]);
//        $logged = false;
//        foreach ($logs as $log) {
//            if ('Update form record' === $log->getMessage() && $record->getId() === $log->getContext()['record_id']) {
//                $logged = true;
//                break;
//            }
//        }
//        $this->assertTrue($logged, 'Inquiry record updated but not logged.');
    }

    /**
     * Test that the division must be in the country.
     */
    public function testInsertRegionInCountryInquiryFail(): void
    {
        // Init user session.
        $this->loginuser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('es');
        // Find record to update.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[administrative_division_reference] Field "Administrative division [field_region]" have a reference, "01FFFBVPS6K583BY7QCEK1HSZF", outside of country "es".');

        $form->insert([
            'field_reference' => 'A',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);
    }

    /**
     * Test that a ROLE_SUPERVISOR can't update regions inquiry.
     */
    public function testSupervisorUpdateRegionInquiryFail(): void
    {
        // Init user session.
        $this->loginuser('Supervisor');

        $userRepo = $this->entityManager->getRepository(User::class);
        $user = $userRepo->findOneBy(['username' => 'Supervisor']);

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        // Find record to update.
        $record = current($form->findBy(['field_reference' => 'To update']));

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');
        // Limit user to the example division.
        $user->addAdministrativeDivision($division);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        /** @var FormRecord $record */
        $record->{'field_region'} = $division;
        $record->save();

        // Update record to a non-allowed user division.
        $record = $form->find($record->getId());
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $record->{'field_region'} = $division;

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The "region update" action require the permission "region update inquiry record".');

        $record->save();
    }

    /**
     * Test that a ROLE_SECTORIAL_GLOBAL can update regions inquiry.
     */
    public function testSectorialGlobalUpdateRegionInquiryOk(): void
    {
        // Init user session.
        $this->loginuser('SectorialGlobal');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        // Find record to update.
        $record = current($form->findBy(['field_reference' => 'To update']));

        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');

        /** @var FormRecord $record */
        $record->{'field_region'} = $division;
        $record->save();

        $this->assertEquals($division->getId(), $record->{'field_region'}->getId());
    }

    /**
     * Test that a validated inquiry is marked.
     */
    public function testValidatedInquiryGetMarked(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        $this->sessionManager->resetSession();

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $id = $form->insert([
            'field_reference' => 'Validated',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Set finished.
        $record = $form->find($id);
        $record->{'field_status'} = 'finished';
        $record->save();

        // Set validated.
        $record = $form->find($id);
        $record->{'field_status'} = 'validated';
        $record->save();

        // Verify mark, before load, and after.
        $this->assertTrue($record->{'field_validated'});
        $record = $form->find($id);
        $this->assertTrue($record->{'field_validated'});
    }

    /**
     * Test that a validated inquiry can be updated, updating status to draft.
     */
    public function testValidatedInquiryUpdate(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        $this->sessionManager->resetSession();

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $id = $form->insert([
            'field_reference' => 'Validated to update',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Set finished.
        $record = $form->find($id);
        $record->{'field_status'} = 'finished';
        $record->save();

        // Set validated.
        $record = $form->find($id);
        $record->{'field_status'} = 'validated';
        $record->save();

        // Update it.
        $record->{'field_name'} = 'José';
        $record->save();

        // Verify mark, before load, and after.
        $this->assertEquals($id, $record->getId());
        $this->assertEquals('draft', $record->{'field_status'});
        $this->assertTrue($record->{'field_validated'});
        $record = $form->find($record->getId());
        $this->assertEquals('draft', $record->{'field_status'});
        $this->assertTrue($record->{'field_validated'});
    }

    /**
     * Test that a validated inquiry can be cloned to update, the new record status must be draft.
     */
    public function testValidatedInquiryClone(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        $this->sessionManager->resetSession();

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $id = $form->insert([
            'field_reference' => 'Validated to update',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Set finished.
        $record = $form->find($id);
        $record->{'field_status'} = 'finished';
        $record->save();

        // Set validated.
        $record = $form->find($id);
        $record->{'field_status'} = 'validated';
        $record->save();

        // Update it.
        $clonedRecord = $record->clone();
        $clonedId = $clonedRecord->getId();

        // Verify mark, before load, and after.
        $this->assertNotEquals($clonedId, $record->getId());
        $this->assertEquals('draft', $clonedRecord->{'field_status'});
        $this->assertFalse($clonedRecord->{'field_validated'});
        $clonedRecord = $form->find($clonedRecord->getId());
        $this->assertEquals('draft', $clonedRecord->{'field_status'});
        $this->assertFalse($clonedRecord->{'field_validated'});
        // Source record must have locked status.
        $record = $form->find($record->getId());
        $this->assertEquals('locked', $record->{'field_status'});
    }

    /**
     * Test that a inquiry can't be created other than draft.
     */
    public function testMustInsertHowDraft(): void
    {
        // Init user session.
        $this->loginuser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert([
            'field_reference' => 'Example',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
            'field_status' => 'validated',
        ]);
        $record = $form->find($id);
        $this->assertEquals('draft', $record->{'field_status'});
    }

    /**
     * Test that a user can't delete a inquiry.
     */
    public function testUserDropInquiryFail(): void
    {
        // Init user session.
        $this->loginuser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert([
            'field_reference' => 'Delete me fail',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Init user session.
        $this->loginuser('SectorialValidator');

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The "delete" action require the permission "delete inquiry record".');

        $form->drop($id);
    }

    /**
     * Test that a user can delete a inquiry.
     */
    public function testUserDropInquiryOk(): void
    {
        // Init user session.
        $this->loginuser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert([
            'field_reference' => 'Delete me ok',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Init user session.
        $this->loginuser('SectorialGlobal');
        $form->drop($id);

        // Validate.
        $record = $form->find($id);
        $this->assertNull($record);

        // Test logger. (No more inquiry logging)
//        $logRepository = $this->entityManager->getRepository(InquiryFormLog::class);
//        $logs = $logRepository->findBy([]);
//        $logged = false;
//        foreach ($logs as $log) {
//            if ('Deleted form record' === $log->getMessage() && $id === $log->getContext()['record_id']) {
//                $logged = true;
//                break;
//            }
//        }
//        $this->assertTrue($logged, 'Inquiry record deleted but not logged.');
    }

    /**
     * Test that a validated inquiry can't be deleted.
     */
    public function testLogicalDelete(): void
    {
        // Init user session.
        $this->loginuser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert([
            'field_reference' => 'Delete me ok',
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Set finished.
        $record = $form->find($id);
        $record->{'field_status'} = 'finished';
        $record->save();

        // Set validated.
        $record = $form->find($id);
        $record->{'field_status'} = 'validated';
        $record->save();

        // Drop.
        $form->drop($id);

        // Validate.
        $record = $form->find($id);
        $this->assertNotNull($record);
        $this->assertTrue($record->{'field_deleted'});
    }

    /**
     * Get update or delete test variations.
     *
     * @return array[]
     */
    public function stopUpdateDeleteInquiryDataProvider(): array
    {
        return [
            [
                'action' => 'drop',
                'status' => ['finished', 'validated', 'locked'],
            ],
            [
                'action' => 'drop',
                'status' => ['finished', 'validated', 'removed'],
            ],
        ];
    }

    /**
     * Test that we can't modify locked or removed records.
     *
     * @dataProvider stopUpdateDeleteInquiryDataProvider
     *
     * @param string $action
     * @param array  $status
     *
     * @throws \Exception
     */
    public function testStopUpdateDeleteInquiry(string $action, array $status): void
    {
        // Init user session.
        $this->loginuser('Pedro');

        // Load.
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert([
            'field_reference' => uniqid(),
            'field_name' => 'Pedro',
            'field_country' => $country,
            'field_region' => $division,
        ]);

        // Get final status.
        foreach ($status as $currentStatus) {
            $record = $form->find($id);
            $record->{'field_status'} = $currentStatus;
            $record->save();
        }

        // Validate.
        $record = $form->find($id);

        $this->expectException(\Exception::class);

        switch ($action) {
            // Update generates a clone.
//            case 'update':
//                $record->{'field_name'} = 'Update';
//                $this->expectExceptionMessage('Locked or removed records can\'t be updated.');
//                $record->save();
//                break;
            case 'drop':
                $this->expectExceptionMessage('Locked or removed records can\'t be deleted.');
                $form->drop($id);
                break;
        }
    }

    /**
     * Test that admin can read all records.
     *
     * @throws \Exception
     */
    public function testAdminRead(): void
    {
        // Note: By default this test have a record in Honduras with a root administrative division.
        $form = $this->formFactory->find('test.inquiry');
        $this->addReadRecordsSamples();
        $this->loginUser('Pedro');
        // Read.
        $records = $form->findAll();
        // Validate.
        $this->assertEquals(3, count($records));
    }

    /**
     * Test that user can read records in her administrative division.
     *
     * @throws \Exception
     */
    public function testUserReadRootDivision(): void
    {
        // Note: By default this test have a record in Honduras with a root administrative division.
        $form = $this->formFactory->find('test.inquiry');
        $this->addReadRecordsSamples();

        // With root administrative division.
        $this->loginUser('SectorialValidator');
        // Read.
        $records = $form->findAll();
        // Validate.
        $this->assertEquals(2, count($records));
    }

    /**
     * Test that user can read records in her administrative division.
     *
     * @throws \Exception
     */
    public function testUserReadLeafDivision(): void
    {
        // Note: By default this test have a record in Honduras with a root administrative division.
        $form = $this->formFactory->find('test.inquiry');
        // Clear old records.
        $form->uninstall();
        $form->install();
        // Add test records.
        $this->addReadRecordsSamples();

        // With last leaf administrative division.
        $this->loginUser('SectorialValidator1');
        // Read.
        $records = $form->findAll();
        // Validate.
        $this->assertEquals(1, count($records));
    }

    /**
     * Test that user can read records in her administrative division.
     *
     * @throws \Exception
     */
    public function testUserReadEs(): void
    {
        // Note: By default this test have a record in Honduras with a root administrative division.
        $form = $this->formFactory->find('test.inquiry');
        $this->addReadRecordsSamples();

        // With last leaf administrative division.
        $this->loginUser('SectorialValidatorEs');
        // Read.
        $records = $form->findAll();
        // Validate.
        $this->assertEquals(1, count($records));
    }

    /**
     * Return two valid, and completed, flows in Inquiring workflow.
     *
     * @return array[]
     */
    public function inquiryWorkflowGuardsOptions(): array
    {
        $flowId1 = uniqid();
        $flowId2 = uniqid();

        return [
            // To locked state.
            [
                // Insert.
                'username' => 'SectorialLocal',
                'toState' => 'draft',
                'flow' => $flowId1,
                'cntEditors' => 1,
            ],
            [
                'username' => 'Digitizer',
                'toState' => 'finished',
                'flow' => $flowId1,
                'cntEditors' => 2,
            ],
            [
                'username' => 'Digitizer',
                'toState' => 'draft',
                'flow' => $flowId1,
                'cntEditors' => 3,
            ],
            [
                'username' => 'Digitizer',
                'toState' => 'finished',
                'flow' => $flowId1,
                'cntEditors' => 4,
            ],
            [
                'username' => 'SectorialValidator',
                'toState' => 'validated',
                'flow' => $flowId1,
                'cntEditors' => 5,
            ],
            [
                'username' => 'SectorialValidator',
                'toState' => 'draft',
                'flow' => $flowId1,
                'cntEditors' => 6,
            ],
            [
                'username' => 'Digitizer',
                'toState' => 'finished',
                'flow' => $flowId1,
                'cntEditors' => 7,
            ],
            [
                'username' => 'SectorialValidator',
                'toState' => 'validated',
                'flow' => $flowId1,
                'cntEditors' => 8,
            ],
            [
                'username' => 'SectorialLocal',
                'toState' => 'locked',
                'flow' => $flowId1,
                'cntEditors' => 9,
            ],
            // To removed state.
            [
                // Insert.
                'username' => 'SectorialLocal',
                'toState' => 'draft',
                'flow' => $flowId2,
                'cntEditors' => 1,
            ],
            [
                'username' => 'Digitizer',
                'toState' => 'finished',
                'flow' => $flowId2,
                'cntEditors' => 2,
            ],
            [
                'username' => 'SectorialValidator',
                'toState' => 'validated',
                'flow' => $flowId2,
                'cntEditors' => 3,
            ],
            [
                'username' => 'SectorialLocal',
                'toState' => 'removed',
                'flow' => $flowId2,
                'cntEditors' => 4,
            ],
        ];
    }

    /**
     * Test flow guards.
     *
     * @-param string $username   Username that do the action.
     * @-param string $toState    Destination record state.
     * @-param string $flow       Current flow ID.
     * @-param int    $cntEditors Expected editors count value.
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     *
     * @-dataProvider inquiryWorkflowGuardsOptions
     */
    public function testInquiryWorkflowGuards(): void
    {
        // Each step must be a continuation of the before step.
        $flow = $this->inquiryWorkflowGuardsOptions();

        foreach ($flow as $step) {
            $username = $step['username'];
            $toState = $step['toState'];
            $flow = $step['flow'];
            $cntEditors = $step['cntEditors'];

            // Test step.
            $this->loginUser($username);

            /** @var InquiryFormManager $form */
            $form = $this->formFactory->find('test.inquiry.flow');

            $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
            $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');

            /** @var FormRecord $record */
            $record = current($form->findBy(['field_reference' => $flow]));
            if (!$record && 'draft' === $toState) {
                // Clear the form.
                $form->uninstall();
                $form->install();
                // Create the record.
                $id = $form->insert([
                    'field_reference' => $flow,
                    'field_country' => 'hn',
                    'field_region' => $division,
                ]);
                $record = $form->find($id);
                $form->stopEditorsFieldUpdate();
            }

            $record->{'field_status'} = $toState;
            $record->save();
            $form->allowEditorsFieldUpdate();

            // Validate.
            $record = $form->find($record->getId());
            $this->assertEquals($toState, $record->{'field_status'});
            $this->assertCount($cntEditors, $record->{'field_editors'});
            $this->assertCount($cntEditors, $record->{'field_editors_update'});
        }
    }

    /**
     * Test that required field are validated before transit to finished state.
     */
    public function testFinishedRequiredFieldsValidation(): void
    {
        // Create form.
        $form = $this->formFactory->create('test.required.validation', InquiryFormManager::class);
        $form->addField('short_text', 'name', 'Name', ['required' => true]);
        $form->saveNow();
        $form->install();
        // Add a record.
        // Set country.
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->entityManager->getRepository(Country::class);
        /** @var Country $country */
        $country = $countryRepo->find('hn');
        /** @var AdministrativeDivisionRepository $divisionRepo */
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        /** @var AdministrativeDivision $division */
        $division = current(
            $divisionRepo->findBy(
                [
                    'country' => $country->getId(),
                    'level' => $country->getDeep(),
                ],
                [],
                1
            )
        );
        $recordId = $form->insert(
            [
                'field_country' => $country,
                'field_region' => $division,
                'field_reference' => 'PSF_',
            ]
        );
        $record = $form->find($recordId);
        // Transit to finished to force required validation.
        $record->{'field_status'} = 'finished';
        $record->save();

        $logRepository = $this->entityManager->getRepository(InquiryFormLog::class);
        $logs = $logRepository->findBy(
            [
                'level' => '400',
                'formId' => 'form.test.required.validation',
            ]
        );
        $this->assertCount(1, $logs);
        $this->assertEquals('[RequiredFields] [short_text] Field "Name [field_name]" is required.', $logs[0]->getMessage());
    }

    /**
     * Add read sample records.
     *
     * @throws \Exception
     */
    protected function addReadRecordsSamples(): void
    {
        $this->loginUser('Pedro');
        $form = $this->formFactory->find('test.inquiry');
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);

        $division = $divisionRepo->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');
        $form->insert([
            'field_reference' => uniqid(),
            'field_country' => 'hn',
            'field_region' => $division,
        ]);

        $division = $divisionRepo->find('01FG3SCJTNG84009ZVPNHT8BPJ');
        $form->insert([
            'field_reference' => uniqid(),
            'field_country' => 'es',
            'field_region' => $division,
        ]);
    }
}
