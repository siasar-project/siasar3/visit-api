<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\InquiryFormLog;
use App\Entity\User;
use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Forms\FormManagerInterface;
use App\Forms\FormRecord;
use App\Forms\InquiryFormManager;
use App\Forms\SubformFormManager;
use App\Repository\ConfigurationRepository;
use App\Repository\CountryRepository;
use App\Service\SessionService;
use App\Tests\Unit\Service\User\RoleTestBase;
use App\Tools\Tools;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Test subform forms managers.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class SubformFormManagerTest extends RoleTestBase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;
    protected FormManagerInterface|null $form;
    protected array $data;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->fieldTypeManager = static::getContainerInstance()->get('fieldtype_manager');
        $this->formFactory = static::getContainerInstance()->get('form_factory');

        // Basic tests.

        // Subform.
        $form = $this->formFactory->find('test.inquiry.subform', false);
        if (!$form) {
            $form = $this->formFactory->create('test.inquiry.subform', SubformFormManager::class);
            $form->addField('short_text', 'label', 'Subform label');
            $form->setMeta(['parent_form' => 'form.test.inquirysf']);
            $form->saveNow();
        } else {
            $this->loginUser('Pedro');
            $this->sessionManager->resetSession();
            $form->uninstall();
        }
        $form->install();
        // Inquiry form.
        $form = $this->formFactory->find('test.inquirysf', false);
        if (!$form) {
            $form = $this->formFactory->create('test.inquirysf', InquiryFormManager::class);
            $form->addField('short_text', 'name', 'Name');
            $form->addField(
                'subform_reference',
                'sub',
                'Subform',
                [
                    'multivalued' => true,
                    'subform' => 'form.test.inquiry.subform',
                    'country_field' => 'field_country',
                ]
            );
            $form->saveNow();
            $form->install();
        }
        $this->loginUser('Pedro');
        $this->sessionManager->resetSession();
        $form->uninstall();
        $form->install();
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $form->insert([
            'field_reference' => 'To update',
            'field_region' => $division,
        ]);
        // Flow tests.
        $form = $this->formFactory->find('test.inquiry.flow', false);
        if (!$form) {
            $form = $this->formFactory->create('test.inquiry.flow', InquiryFormManager::class);
            $form->addField('short_text', 'name', 'Name');
            $form->saveNow();
        }
        $form->uninstall();
        $form->install();
    }

    /**
     * Test that a ROLE_USER only can't create subform.
     */
    public function testUserInsertSubformFail(): void
    {
        // Init user session.
        $this->loginuser('SectorialValidator');

        // Load.
        $form = $this->formFactory->find('test.inquiry.subform');

        // Insert data.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The "create" action require the permission "create inquiry record".');

        $form->insert([
            'field_label' => 'Label',
        ]);
    }

    /**
     * Test that a ROLE_SUPERVISOR can create subform.
     */
    public function testUserInsertSubformOk(): void
    {
        // Init user session.
        $this->loginUser('Supervisor');

        // Load.
        $form = $this->formFactory->find('test.inquiry.subform');

        // Insert data.
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $id = $form->insert([
            'field_label' => uniqid(),
            'field_country' => $country,
        ]);

        $this->assertIsString($id);

        // Test logger. (No more inquiry logging)
//        $record = $form->find($id);
//        $logRepository = $this->entityManager->getRepository(InquiryFormLog::class);
//        $logs = $logRepository->findBy([]);
//        $logged = false;
//        foreach ($logs as $log) {
//            if ('Insert sub-form record' !== $log->getMessage()) {
//                continue;
//            }
//            if (array_key_exists('received', $log->getContext())) {
//                if ($log->getContext()['received']['field_label'] === $record->{'field_label'}) {
//                    $logged = true;
//                    break;
//                }
//            }
//        }
//        $this->assertTrue($logged, 'Inquiry record inserted but not logged.');
    }

    /**
     * Test that a ROLE_USER only can't update subform.
     */
    public function testUserUpdateSubformFail(): void
    {
        // Init user session.
        $this->loginUser('Supervisor');
        // Load.
        $parentForm = $this->formFactory->find('test.inquirysf');
        $form = $this->formFactory->find('test.inquiry.subform');
        // Add records.
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $subRecordId = $form->insert(
            [
                'field_country' => $country,
                'field_label' => 'Label',
            ]
        );
        $subRecord = $form->find($subRecordId);
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $parentForm->insert([
            'field_country' => $country,
            'field_reference' => 'To update subform',
            'field_region' => $division,
            'field_sub' => [['value' => $subRecord->getId(), 'form' => 'form.test.inquiry.subform']],
        ]);
        // Init user session.
        $this->loginuser('Nico');
        // Find record to update.
        $subRecord = $form->find($subRecordId);
        /** @var FormRecord $subRecord */
        $subRecord->{'field_label'} = 'Label 1';

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('The "update" action require the permission "update inquiry record".');

        $subRecord->save();
    }

    /**
     * Test that a ROLE_USER only can't create subform.
     *
     * With parent in draft allow update
     */
    public function testUserUpdateSubformOk(): void
    {
        // Init user session.
        $this->loginUser('Supervisor');
        // Load.
        $parentForm = $this->formFactory->find('test.inquirysf');
        $form = $this->formFactory->find('test.inquiry.subform');
        // Add records.
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $subRecordId = $form->insert(
            [
                'field_country' => $country,
                'field_label' => 'Label',
            ]
        );
        $subRecord = $form->find($subRecordId);
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $parentForm->insert([
            'field_country' => $country,
            'field_reference' => 'To update subform',
            'field_region' => $division,
            'field_sub' => [$subRecord],
        ]);
        // Find record to update.
        $subRecord = $form->find($subRecordId);
        /** @var FormRecord $subRecord */
        $subRecord->{'field_label'} = 'Label 2';
        $subRecord->save();
        // Validate.
        $subRecord = $form->find($subRecordId);
        $this->assertEquals('Label 2', $subRecord->{'field_label'});

        // Test logger. (No more inquiry logging)
//        $logRepository = $this->entityManager->getRepository(InquiryFormLog::class);
//        $logs = $logRepository->findBy([]);
//        $logged = false;
//        foreach ($logs as $log) {
//            if ('Update sub-form record' === $log->getMessage() && $subRecord->getId() === $log->getContext()['record_id']) {
//                $logged = true;
//                break;
//            }
//        }
//        $this->assertTrue($logged, 'Subform record updated but not logged.');
    }

    /**
     * Test that a ROLE_USER only can't create subform.
     *
     * With parent in finished update parent to draft.
     */
    public function testUserUpdateSubformOkToDraft(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        // Load.
        $parentForm = null;
        $form = null;
        $parentRecordId = '';
        $subRecordId = '';
        $this->buildSubformBaseData($parentForm, $form, $parentRecordId, $subRecordId);
        // Find record to update.
        $subRecord = $form->find($subRecordId);
        /** @var FormRecord $subRecord */
        $subRecord->{'field_label'} = 'Label 2';
        $subRecord->save();
        // Validate.
        $subRecord = $form->find($subRecordId);
        $this->assertEquals('Label 2', $subRecord->{'field_label'});

        $parentRecord = $parentForm->find($parentRecordId);
        $this->assertEquals('draft', $parentRecord->{'field_status'});
    }

    /**
     * Test that with a locked parent can't update subform.
     *
     * With parent in validated, locked or removed stop the update.
     */
    public function testUserUpdateSubformValidatedFail(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        // Load.
        $parentForm = null;
        $form = null;
        $parentRecordId = '';
        $subRecordId = '';
        $this->buildSubformBaseData($parentForm, $form, $parentRecordId, $subRecordId);
        $parentRecord = $parentForm->find($parentRecordId);
        $parentRecord->{'field_status'} = 'validated';
        $parentRecord->save();
        $this->assertEquals('validated', $parentRecord->{'field_status'});
        // Intercept exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Records in validated, locked, or removed states can\'t be updated.');
        // Find record to update.
        $subRecord = $form->find($subRecordId);
        /** @var FormRecord $subRecord */
        $subRecord->{'field_label'} = 'Label 2';
        $subRecord->save();
    }

    /**
     * Test that with a locked parent can't update subform.
     *
     * With parent in validated, locked or removed stop the update.
     */
    public function testUserUpdateSubformLockedFail(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        // Load.
        $parentForm = null;
        $form = null;
        $parentRecordId = '';
        $subRecordId = '';
        $this->buildSubformBaseData($parentForm, $form, $parentRecordId, $subRecordId);
        $parentRecord = $parentForm->find($parentRecordId);
        $parentRecord->{'field_status'} = 'validated';
        $parentRecord->save();
        $this->assertEquals('validated', $parentRecord->{'field_status'});
        $parentRecord->{'field_status'} = 'locked';
        $parentRecord->save();
        $this->assertEquals('locked', $parentRecord->{'field_status'});
        // Intercept exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Records in validated, locked, or removed states can\'t be updated.');
        // Find record to update.
        $subRecord = $form->find($subRecordId);
        /** @var FormRecord $subRecord */
        $subRecord->{'field_label'} = 'Label 2';
        $subRecord->save();
    }

    /**
     * Test that with a locked parent can't update subform.
     *
     * With parent in validated, locked or removed stop the update.
     */
    public function testUserUpdateSubformRemovedFail(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        // Load.
        $parentForm = null;
        $form = null;
        $parentRecordId = '';
        $subRecordId = '';
        $this->buildSubformBaseData($parentForm, $form, $parentRecordId, $subRecordId);
        $parentRecord = $parentForm->find($parentRecordId);
        $parentRecord->{'field_status'} = 'validated';
        $parentRecord->save();
        $this->assertEquals('validated', $parentRecord->{'field_status'});
        $parentRecord->{'field_status'} = 'removed';
        $parentRecord->save();
        $this->assertEquals('removed', $parentRecord->{'field_status'});
        // Intercept exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Records in validated, locked, or removed states can\'t be updated.');
        // Find record to update.
        $subRecord = $form->find($subRecordId);
        /** @var FormRecord $subRecord */
        $subRecord->{'field_label'} = 'Label 2';
        $subRecord->save();
    }

    /**
     * Test that a user require "update inquiry record" permission on parent.
     *
     */
    public function testUserDropSubformRecordFail(): void
    {
        // Init user session.
        $this->loginUser('Pedro');
        // Load.
        $parentForm = null;
        /** @var SubformFormManager $form */
        $form = null;
        $parentRecordId = '';
        $subRecordId = '';
        $this->buildSubformBaseData($parentForm, $form, $parentRecordId, $subRecordId);
        $parentRecord = $parentForm->find($parentRecordId);
        $parentRecord->{'field_status'} = 'validated';
        $parentRecord->save();
        $this->assertEquals('validated', $parentRecord->{'field_status'});
        // Intercept exception.
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Records in validated, locked, or removed states can\'t be updated.');
        // Delete sub record.
        $form->drop($subRecordId);
    }

    /**
     * Init subform tests.
     *
     * @param ?FormManagerInterface $parentForm
     * @param ?FormManagerInterface $form
     * @param string                $parentRecordId
     * @param string                $subRecordId
     *
     * @throws \Exception
     */
    protected function buildSubformBaseData(?FormManagerInterface &$parentForm, ?FormManagerInterface &$form, string &$parentRecordId, string &$subRecordId): void
    {
        // Load.
        $parentForm = $this->formFactory->find('test.inquirysf');
        $form = $this->formFactory->find('test.inquiry.subform');
        // Add records.
        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        $subRecordId = $form->insert(
            [
                'field_country' => $country,
                'field_label' => 'Label',
            ]
        );
        $subRecord = $form->find($subRecordId);
        $divisionRepo = $this->entityManager->getRepository(AdministrativeDivision::class);
        $division = $divisionRepo->find('01FFFBVPS6K583BY7QCEK1HSZF');
        $parentRecordId = $parentForm->insert([
            'field_country' => $country,
            'field_reference' => 'To update subform',
            'field_region' => $division,
            'field_sub' => [$subRecord],
        ]);
        // Transit to finished.
        $parentRecord = $parentForm->find($parentRecordId);
        $parentRecord->{'field_status'} = 'finished';
        $parentRecord->save();
        $parentRecord = $parentForm->find($parentRecordId);
        $parentRecord->{'field_sub'};
        $this->assertEquals('finished', $parentRecord->{'field_status'});
    }
}
