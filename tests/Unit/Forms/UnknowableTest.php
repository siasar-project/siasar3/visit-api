<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Forms;

use App\Forms\FieldTypes\FieldTypeManager;
use App\Forms\FormFactory;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Yaml\Yaml;

/**
 * Test unknowable fields.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UnknowableTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected FieldTypeManager|null $fieldTypeManager;
    protected FormFactory|null $formFactory;
    protected null|Connection $doctrine;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        // Skip tests because unknowable is not used.
        $this->markTestSkipped('unknowable fields not used. Skip tests.');

//        parent::setUp();
//
//        self::bootKernel();
//        $this->loadFixtures();
//
//        $this->entityManager = self::$container->get('doctrine')->getManager();
//        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
//        $this->fieldTypeManager = self::$container->get('fieldtype_manager');
//        $this->formFactory = self::$container->get('form_factory');
//        $this->doctrine = self::$container->get('database_connection');
    }

    /**
     * Test that we can use an unknowable integer field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableInteger(): void
    {
        $form = $this->formFactory->create('test.unknowable.integer');
        // Try to add the trust field type.
        $form->addField('integer', 'data', 'data', ['unknowable' => true]);
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert(['field_data' => 1]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1, $record->{'field_data'});
        // Insert unknown value.
        $recordId = $form->insert(['field_data' => -9999999]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->{'field_data'});
    }

    /**
     * Test that we can use an unknowable decimal field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableDecimal(): void
    {
        $form = $this->formFactory->create('test.unknowable.decimal');
        // Try to add the trust field type.
        $form->addField('decimal', 'data', 'data', ['unknowable' => true, 'precision' => 10, 'scale' => 2]);
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert(['field_data' => 1.5]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1.5, $record->{'field_data'});
        // Insert unknown value.
        $recordId = $form->insert(['field_data' => -9999999]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->{'field_data'});
    }

    /**
     * Test that we can use an unknowable currency field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableCurrency(): void
    {
        $form = $this->formFactory->create('test.unknowable.currency');
        // Try to add the trust field type.
        $form->addField('currency', 'data', 'data', ['unknowable' => true, 'country_field' => 'field_country']);
        $form->addField('country_reference', 'country', 'country');
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert([
            'field_country' => [
                'value' => 'hn',
                'class' => 'App\\Entity\\Country',
            ],
            'field_data' => [
                'value' => '01FENEDYFA6KNAA4J459P5S0RF',
                'class' => 'App\\Entity\\Currency',
                'amount' => 1.5,
            ],
        ]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1.5, $record->get('field_data')['amount']);
        // Insert unknown value.
        $recordId = $form->insert([
            'field_country' => [
                'value' => 'hn',
                'class' => 'App\\Entity\\Country',
            ],
            'field_data' => [
                'value' => '',
                'class' => '',
                'amount' => -9999999,
            ],
        ]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->get('field_data')['amount']);
    }

    /**
     * Test that we can use an unknowable concentration field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableConcentration(): void
    {
        $units = Yaml::parseFile(dirname(__FILE__).'/../../assets/system.units.concentration.yml');
        $cfg = $this->configuration->findEditable('system.units.concentration');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.concentration', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        // Prepare form.
        $form = $this->formFactory->create('test.unknowable.concentration');
        // Try to add the trust field type.
        $form->addField('concentration', 'data', 'data', ['unknowable' => true]);
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert(['field_data' => ['value' => 1.5, 'unit' => 'particles per million']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1.5, $record->get('field_data')['value']->getValue());
        // Insert unknown value.
        $recordId = $form->insert(['field_data' => ['value' => -9999999, 'unit' => 'particles per million']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->get('field_data')['value']->getValue());
    }

    /**
     * Test that we can use an unknowable flow field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableFlow(): void
    {
        $units = Yaml::parseFile(dirname(__FILE__).'/../../assets/system.units.flow.yml');
        $cfg = $this->configuration->findEditable('system.units.flow');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.flow', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        // Prepare form.
        $form = $this->formFactory->create('test.unknowable.flow');
        // Try to add the trust field type.
        $form->addField('flow', 'data', 'data', ['unknowable' => true]);
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert(['field_data' => ['value' => 1.5, 'unit' => 'liter/second']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1.5, $record->get('field_data')['value']->getValue());
        // Insert unknown value.
        $recordId = $form->insert(['field_data' => ['value' => -9999999, 'unit' => 'liter/second']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->get('field_data')['value']->getValue());
    }

    /**
     * Test that we can use an unknowable length field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableLength(): void
    {
        $units = Yaml::parseFile(dirname(__FILE__).'/../../assets/system.units.length.yml');
        $cfg = $this->configuration->findEditable('system.units.length');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.length', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        // Prepare form.
        $form = $this->formFactory->create('test.unknowable.length');
        // Try to add the trust field type.
        $form->addField('length', 'data', 'data', ['unknowable' => true]);
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert(['field_data' => ['value' => 1.5, 'unit' => 'metre']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1.5, $record->get('field_data')['value']->getValue());
        // Insert unknown value.
        $recordId = $form->insert(['field_data' => ['value' => -9999999, 'unit' => 'metre']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->get('field_data')['value']->getValue());
    }

    /**
     * Test that we can use an unknowable volume field.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testUnknowableVolume(): void
    {
        $units = Yaml::parseFile(dirname(__FILE__).'/../../assets/system.units.volume.yml');
        $cfg = $this->configuration->findEditable('system.units.volume');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.volume', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        // Prepare form.
        $form = $this->formFactory->create('test.unknowable.volume');
        // Try to add the trust field type.
        $form->addField('volume', 'data', 'data', ['unknowable' => true]);
        $form->saveNow();
        $form->install();
        // Insert valid value.
        $recordId = $form->insert(['field_data' => ['value' => 1.5, 'unit' => 'cubic metre']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(1.5, $record->get('field_data')['value']->getValue());
        // Insert unknown value.
        $recordId = $form->insert(['field_data' => ['value' => -9999999, 'unit' => 'cubic metre']]);
        // Validate.
        $record = $form->find($recordId);
        $this->assertEquals(-9999999, $record->get('field_data')['value']->getValue());
    }
}
