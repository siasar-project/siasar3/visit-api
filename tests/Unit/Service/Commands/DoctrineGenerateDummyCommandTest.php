<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\DoctrineGenerateDummyCommand;
use App\Forms\FormFactory;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Basic doctrine generate dummy command test.
 */
class DoctrineGenerateDummyCommandTest extends RoleTestBase
{
    protected App $application;
    protected ManagerRegistry $managerRegistry;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->extraConfigurations = [
            'system.units.concentration' => dirname(__FILE__).'/../../../assets/system.units.concentration.yml',
            'system.units.flow' => dirname(__FILE__).'/../../../assets/system.units.flow.yml',
            'system.units.length' => dirname(__FILE__).'/../../../assets/system.units.length.yml',
            'system.units.volume' => dirname(__FILE__).'/../../../assets/system.units.volume.yml',
        ];
        parent::setUp();

        self::bootKernel();
        //$this->loadFixtures();
        $kernel = self::$kernel;

        $this->doctrineReg = self::$container->get('doctrine');
        $this->application = new App($kernel);
    }

    /**
     * Test DoctrineGenerateDummyCommand command.
     */
    public function testDoctrineGenerateDummies(): void
    {
        $this->application->add(new DoctrineGenerateDummyCommand($this->doctrineReg, 'doctrine:generate:dummies'));
        $command = $this->application->find('doctrine:generate:dummies');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                '--clear' => true,
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }
}
