<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\FormsCleanSubforms;
use App\Forms\FormFactory;
use App\Service\SessionService;
use App\Tests\Functional\SiasarPoint\PointTestBase;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * User generate command test.
 */
class FormsClearSubformCommandTest extends PointTestBase
{
    protected App $application;
    protected ManagerRegistry $managerRegistry;
    protected null|SessionService $sessionManager;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $kernel = self::$kernel;

        $this->managerRegistry = self::$container->get('doctrine');
        $this->application = new App($kernel);
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->formFactory = self::$container->get('form_factory');

        foreach ($this->extraConfigurations as $formId => $file) {
            $form = $this->formFactory->find($formId);
            $form->install();
        }
    }

    /**
     * Test forms:clean:subforms command.
     */
    public function testCommand(): void
    {
        $this->loadRecord(dirname(__FILE__).'/../../../assets/form_records/point_01GM8PTSVRY84F6QQ2XK2MV0GZ.json');
        $this->loadRecord(dirname(__FILE__).'/../../../assets/form_records/orphan_subform_01GTV6NKZ26EDAMMYN62QWSP4G.json');
        $this->application->add(new FormsCleanSubforms('forms:clean:subforms', $this->sessionManager, self::$container->get('form_factory')));
        $command = $this->application->find('forms:clean:subforms');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                'user' => 'pedro',
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }
}
