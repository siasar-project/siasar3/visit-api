<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\DoctrineGenerateDummyCommand;
use App\Command\FormsGenerate;
use App\Command\UserGenerateDummy;
use App\Entity\Country;
use App\Entity\User;
use App\Forms\FormFactory;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * User generate command test.
 */
class FormsGenerateCommandTest extends RoleTestBase
{
    protected App $application;
    protected ManagerRegistry $managerRegistry;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        // Skip tests because this is a debug command.
        $this->markTestSkipped('This is a debug command. Skip tests.');

        $this->extraConfigurations = [
            'form.meta.test' => dirname(__FILE__).'/../../../assets/form.meta.test.yml',
            'form.all.field.type' => dirname(__FILE__).'/../../../assets/form.all.field.type.yml',
            'form.school.contact' => dirname(__FILE__).'/../../../assets/form.school.contact.yml',
            'form.school' => dirname(__FILE__).'/../../../assets/form.school.yml',
            'form.tap.contact' => dirname(__FILE__).'/../../../assets/form.tap.contact.yml',
            'form.tap' => dirname(__FILE__).'/../../../assets/form.tap.yml',
        ];
        parent::setUp();
        $kernel = self::$kernel;

        $this->managerRegistry = self::$container->get('doctrine');
        $this->application = new App($kernel);
        $this->formFactory = self::$container->get('form_factory');

        foreach ($this->extraConfigurations as $formId => $file) {
            $form = $this->formFactory->find($formId);
            $form->uninstall();
            $form->install();
//            if ('form.school.contact' === $formId) {
//                $countryRepo = $this->entityManager->getRepository(Country::class);
//                $form->insert(['field_country' => $countryRepo->find('hn')]);
//            }
        }
    }

    /**
     * Test user:generate command.
     */
    public function testCommand(): void
    {
        $this->generateDoctrine();

        $this->application->add(new FormsGenerate($this->sessionManager, $this->managerRegistry, self::$container->get('form_factory'), 'forms:generate:dummies'));
        $command = $this->application->find('forms:generate:dummies');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                'user' => 'Pedro',
                '--clear' => true,
                '--max-records' => 2,
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }

    /**
     * Generate doctrine entities.
     */
    protected function generateDoctrine(): void
    {
        $this->application->add(new DoctrineGenerateDummyCommand($this->managerRegistry, 'doctrine:generate:dummies'));
        $command = $this->application->find('doctrine:generate:dummies');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                '--clear' => true,
            ]
        );
        $display = $commandTester->getDisplay();
    }
}
