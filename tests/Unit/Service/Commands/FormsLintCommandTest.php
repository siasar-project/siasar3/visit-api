<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\DoctrineGenerateDummyCommand;
use App\Command\FormsGenerate;
use App\Command\FormsLint;
use App\Command\UserGenerateDummy;
use App\Entity\Country;
use App\Entity\User;
use App\Forms\FormFactory;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * User generate command test.
 */
class FormsLintCommandTest extends RoleTestBase
{
    protected App $application;
    protected ManagerRegistry $managerRegistry;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        $this->extraConfigurations = [
            'form.wsprovider.contact' => dirname(__FILE__).'/../../../assets/form.wsprovider.contact.yml',
            'form.wsprovider.tap' => dirname(__FILE__).'/../../../assets/form.wsprovider.tap.yml',
            'form.wsprovider' => dirname(__FILE__).'/../../../assets/form.wsprovider.yml',
            'form.community.intervention' => dirname(__FILE__).'/../../../assets/form.community.intervention.yml',
            'form.community.contact' => dirname(__FILE__).'/../../../assets/form.community.contact.yml',
            'form.community' => dirname(__FILE__).'/../../../assets/form.community.yml',
        ];
        parent::setUp();
        $kernel = self::$kernel;

        $this->managerRegistry = self::$container->get('doctrine');
        $this->application = new App($kernel);
        $this->formFactory = self::$container->get('form_factory');

        foreach ($this->extraConfigurations as $formId => $file) {
            $form = $this->formFactory->find($formId);
            $form->install();
        }
    }

    /**
     * Test forms:lint command.
     */
    public function testCommand(): void
    {
        $this->application->add(new FormsLint('forms:lint', self::$container->get('form_factory')));
        $command = $this->application->find('forms:lint');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                'form_id' => 'form.wsprovider',
                '--yaml' => false,
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }
}
