<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\FormsLint;
use App\Forms\FormFactory;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * User generate command test.
 */
class FormsLintWithoutDBCommandTest extends KernelTestCase
{
    protected App $application;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->application = new App(self::$kernel);
    }

    /**
     * Test forms:lint -m command.
     */
    public function testCommandFromYaml(): void
    {
        $this->application->add(new FormsLint('forms:lint', self::$container->get('form_factory')));
        $command = $this->application->find('forms:lint');
        $commandTester = new CommandTester($command);
        // Important: This command is reading from real configuration export folder.
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                'form_id' => 'form.community',
                '--yaml' => true,
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }
}
