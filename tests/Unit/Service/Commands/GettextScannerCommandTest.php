<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\GetTextScannerCommand;
use App\Command\UserGenerateDummy;
use App\Entity\Country;
use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Entity\User;
use App\Repository\LocaleSourceRepository;
use App\Tests\TestFixturesTrait;
use App\Traits\StringTranslationTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * User generate command test.
 */
class GettextScannerCommandTest extends KernelTestCase
{
    use TestFixturesTrait;
    use StringTranslationTrait;

    protected App $application;
    protected ManagerRegistry $entityManager;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();
        $kernel = self::$kernel;

        $this->entityManager = self::$container->get('doctrine');
        $this->application = new App($kernel);
    }

    /**
     * Test gettext:scan command.
     */
    public function testCommand(): void
    {
        $myStringTranslated = $this->t('GettextScannerCommandTest');
        $this->application->add(
            new GetTextScannerCommand(
                'gettext:scan',
                $this->entityManager->getRepository(LocaleSource::class)
            )
        );
        $command = $this->application->find('gettext:scan');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                'folder' => $this->getKernelInstance()->getProjectDir().'/tests',
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
        $this->assertStringContainsString('[INFO] [siasar] Found 1 strings.', $display);

        // Test that don't duplicate literals in database.
        $command = $this->application->find('gettext:scan');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                'folder' => $this->getKernelInstance()->getProjectDir().'/tests',
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
        $this->assertStringContainsString('[INFO] [siasar] 0 strings created.', $display);
    }
}
