<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\DoctrineGenerateDummyCommand;
use App\Command\PanamaDivs;
use App\Forms\FormFactory;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\RoleTestBase;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Basic doctrine generate dummy command test.
 */
class PanamaDivsCommandTest extends RoleTestBase
{
    protected App $application;
    protected ManagerRegistry $managerRegistry;
    protected FormFactory $formFactory;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        // Skip tests because the panama:divs command is to development.
        if (!static::getContainerInstance()->getParameter('api_platform.graphql.enabled')) {
            $this->markTestSkipped('The tested command is to development.');
        }
        parent::setUp();

        self::bootKernel();
        //$this->loadFixtures();
        $kernel = self::$kernel;

        $this->doctrineReg = self::$container->get('doctrine');
        $this->application = new App($kernel);
    }

    /**
     * Test DoctrineGenerateDummyCommand command.
     */
    public function testPanamaDivs(): void
    {
        $this->application->add(new PanamaDivs('panama:divs'));
        $command = $this->application->find('panama:divs');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }
}
