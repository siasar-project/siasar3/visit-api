<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Commands;

use App\Command\UserGenerateDummy;
use App\Entity\Country;
use App\Entity\User;
use App\Tests\TestFixturesTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application as App;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * User generate command test.
 */
class UserGenerateCommandTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected App $application;
    protected ManagerRegistry $entityManager;

    /**
     * Prepare environment.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();
        $kernel = self::$kernel;

        $this->entityManager = self::$container->get('doctrine');
        $this->application = new App($kernel);
    }

    /**
     * Test user:generate command.
     */
    public function testCommand(): void
    {
        $this->application->add(new UserGenerateDummy('user:generate:dummies', $this->entityManager->getRepository(User::class), $this->entityManager->getRepository(Country::class), self::$container->get('App\Service\Password\EncoderService'), self::$container->get('access_manager'), self::$container->get('doctrine')));
        $command = $this->application->find('user:generate:dummies');
        $commandTester = new CommandTester($command);
        $resultStatus = $commandTester->execute(
            [
                'command' => $command->getName(),
                '--clear' => true,
            ]
        );
        $display = $commandTester->getDisplay();
        $this->assertEquals(Command::SUCCESS, $resultStatus);
    }
}
