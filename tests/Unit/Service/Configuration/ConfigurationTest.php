<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Configuration;

use App\Entity\Configuration;
use App\Entity\Configuration\ConfigurationWrite;
use App\Repository\ConfigurationRepository;
use App\Tests\TestFixturesTrait;
use App\Tools\TranslatableMarkup;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test Configuration repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ConfigurationTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected ?EntityManager $entityManager;
    protected ConfigurationRepository|null $configuration;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
    }

    /**
     * Test that when we create a configuration it's a ConfigurationWrite instance.
     *
     * @return void
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testCreateConfigurationReturnWritable(): void
    {
        /**
         * Loaded configuration.
         *
         * @var Configuration $cfg
         */
        $cfg = $this->configuration->create('test.class', ['class' => 'App\Entity\Configuration\ConfigurationWrite']);
        $cfg->save();
        $this->entityManager->flush();
        // $cfg must be a ConfigurationWrite instance.
        $this->assertEquals('App\Entity\Configuration\ConfigurationWrite', get_class($cfg));
    }

    /**
     * Test that we can create a configuration.
     *
     * @return void
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testCreateConfiguration(): void
    {
        $cfg = $this->configuration->create('dummy', ['value' => true]);
        $cfg->save();
        $this->entityManager->flush();

        // Test that dummy configuration it's saved.
        /**
         * Loaded configuration.
         *
         * @var Configuration $cfg
         */
        $cfg = $this->configuration->find('dummy');
        $this->assertEquals('dummy', $cfg->getId());
        $this->assertEquals(['value' => true], $cfg->getValue());
    }

    /**
     * Test that we can create an user configuration.
     *
     * @return void
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testCreateUserConfiguration(): void
    {
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneByUserName('Pedro');
        $cfg = $this->configuration->createInUser($user, 'dummy', ['value' => true]);
        $cfg->saveNow();

        // Test that dummy configuration it's saved.
        /**
         * Loaded configuration.
         *
         * @var Configuration $cfg
         */
        $cfg = $this->configuration->findInUser($user, 'dummy');
        $this->assertEquals($this->configuration->getUserKeyRoot($user).'dummy', $cfg->getId());
        $this->assertEquals(['value' => true], $cfg->getValue());
    }

    /**
     * Test that get configuration user key root is not empty.
     *
     * @return void
     */
    public function testUserConfigurationKeyRoot(): void
    {
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneByUserName('Pedro');
        $this->assertNotEmpty($this->configuration->getUserKeyRoot($user));
    }

    /**
     * Test that we can't create duplicated configuration.
     *
     * @return void
     */
    public function testCreateDuplicateConfiguration(): void
    {
        // Create original configuration.
        $cfg = $this->configuration->create('duplicated.dummy', ['value' => true]);
        $cfg->saveNow();

        // Expected exception.
        $this->expectException(UniqueConstraintViolationException::class);

        // Create duplicate.
        $cfg = $this->configuration->create('duplicated.dummy', ['value' => true]);
        $cfg->saveNow();
    }

    /**
     * Test that a ConfigurationWrite constructor don't allow a ID other than the configuration ID instance.
     *
     * @return void
     *
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function testBadIdInConfigurationWrite(): void
    {
        // Create configuration.
        $cfg = $this->configuration->create('test.bad.id', ['value' => true]);
        $cfg->saveNow();
        // Get how editable.
        $cfg = $this->configuration->findEditable('test.bad.id');
        // Init with wrong configuration ID.
        $throw = false;
        try {
            new ConfigurationWrite($this->entityManager, 'test.other', $cfg);
        } catch (\Exception $e) {
            $throw = true;
            $this->assertEquals(
                'Wrong id "test.other" to writable in ConfigurationWrite "test.bad.id".',
                $e->getMessage()
            );
        }
        $this->assertTrue($throw, 'App\Entity\Configuration\ConfigurationWrite allows the wrong constructor ID.');
    }

    /**
     * Data provider to testNotValidIdInConfigurationWrite.
     *
     * @return string[][]
     */
    public function notValidIdInConfigurationWriteValues(): array
    {
        return [
            ['goo gle.com'],
            ['duckduckgo..com'],
            ['duckduckgo-.com'],
            ['.duckduckgo.com'],
            ['<script'],
            ['alert('],
            ['.'],
            ['..'],
            [' '],
            ['-'],
            [''],
            ['test_name'],
        ];
    }

    /**
     * Test that a ConfigurationWrite constructor don't allow a not valid ID.
     *
     * @param string $value Vad configuration to test.
     *
     * @return void
     *
     * @throws Exception
     *
     * @dataProvider notValidIdInConfigurationWriteValues
     */
    public function testNotValidIdInConfigurationWrite(string $value): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage(new TranslatableMarkup('Not valid id name "@id".', ['@id' => $value]));
        $configurationRepository = new ConfigurationRepository(self::$container->get(ManagerRegistry::class));
        $configurationRepository->create($value, []);
    }

    /**
     * Data provider to testValidIdInConfigurationWrite.
     *
     * @return string[][]
     */
    public function validIdInConfigurationWriteValues(): array
    {
        return [
            ['0'],
            ['0a'],
            ['a0'],
            ['a'],
            ['a.b'],
            ['localhost'],
            ['duckduckgo.com'],
            ['news.duckduckgo.co.uk'],
            ['xn--fsqu00a.xn--0zwm56d'],
            ['test-name'],
        ];
    }

    /**
     * Test that a ConfigurationWrite constructor allow a valid ID.
     *
     * @param string $value Vad configuration to test.
     *
     * @return void
     *
     * @throws Exception
     *
     * @dataProvider validIdInConfigurationWriteValues
     */
    public function testValidIdInConfigurationWrite(string $value): void
    {
        $configurationRepository = new ConfigurationRepository(self::$container->get(ManagerRegistry::class));
        $config = $configurationRepository->create($value, []);
        $this->assertNotNull($config);
    }

    /**
     * Test that configuration repository return ConfigurationRead objects.
     *
     * @return void
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testGetConfigurationReadClass(): void
    {
        /**
         * Loaded configuration.
         *
         * @var Configuration $cfg
         */
        $cfg = $this->configuration->create('test.dummy', ['name' => 'Test Dummy']);
        $cfg->save();
        $this->entityManager->flush();

        // Find.
        $cfg = $this->configuration->find('test.dummy');
        $this->assertEquals('App\Entity\Configuration\ConfigurationRead', get_class($cfg));
        // Find One By.
        $cfg = $this->configuration->findOneBy(['id' => 'test.dummy']);
        $this->assertEquals('App\Entity\Configuration\ConfigurationRead', get_class($cfg));
        // Find By.
        $cfgs = $this->configuration->findBy(['id' => 'test.dummy']);
        foreach ($cfgs as $cfg) {
            $this->assertEquals('App\Entity\Configuration\ConfigurationRead', get_class($cfg));
        }
        // Find all.
        $cfgs = $this->configuration->findAll();
        foreach ($cfgs as $cfg) {
            $this->assertEquals(get_class($cfg), 'App\Entity\Configuration\ConfigurationRead');
        }
    }

    /**
     * Test that configuration repository return null or empty array when not found the configuration.
     *
     * @return void
     */
    public function testNotFoundConfiguration(): void
    {
        // Find.
        $cfg = $this->configuration->find('not.found');
        $this->assertNull($cfg);
        // Find One By.
        $cfg = $this->configuration->findOneBy(['id' => 'not.found']);
        $this->assertNull($cfg);
        // Find By.
        $cfgs = $this->configuration->findBy(['id' => 'not.found']);
        $this->assertEmpty($cfgs);
    }
}
