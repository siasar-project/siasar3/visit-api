<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Country;

use App\Entity\Configuration;
use App\Entity\Country;
use App\Repository\CountryRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test language repository
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class Country2ConfigurationTest extends KernelTestCase
{
    use TestFixturesTrait;

    protected ?ManagerRegistry $managerRegistry;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();

        $this->managerRegistry = self::$container->get(ManagerRegistry::class);
    }

    /**
     * Test that create a country, create a configuration too.
     *
     * @return void
     */
    public function testCreateCountryConfiguration(): void
    {
        // Add a new entity.
        $country = new Country('pt');
        $country->setName('Portugal');
        $this->managerRegistry->getManager()->persist($country);
        $this->managerRegistry->getManager()->flush();

        // Is the configuration created?
        $cfg = $this->managerRegistry->getRepository(Configuration::class)->find($country->configuratioGetConfigurationId());
        $this->assertNotNull($cfg);
    }

    /**
     * Test that update a country, update a configuration too.
     *
     * @return void
     */
    public function testUpdateCountryConfiguration(): void
    {
        // Add a new entity.
        $country = new Country('pt');
        $country->setName('Portugal');
        $this->managerRegistry->getManager()->persist($country);
        $this->managerRegistry->getManager()->flush();
        // Load it.
        /** @var CountryRepository $countryRepo */
        $countryRepo = self::$container->get(CountryRepository::class);
        $country = $countryRepo->find('pt');
        $country->setName('SPA');
        $countryRepo->saveNow($country);
        // Is the configuration updated?
        $cfg = $this->managerRegistry->getRepository(Configuration::class)->find($country->configuratioGetConfigurationId());
        $value = $cfg->getValue();
        $this->assertArrayHasKey('name', $value);
        $this->assertEquals('SPA', $value['name']);
    }

    /**
     * Test that remove a country, remove a configuration too.
     *
     * @return void
     */
    public function testRemoveCountryConfiguration(): void
    {
        // Add a new entity.
        $country = new Country('pt');
        $country->setName('Portugal');
        $this->managerRegistry->getManager()->persist($country);
        $this->managerRegistry->getManager()->flush();
        // Remember configuration id.
        $cfgId = $country->configuratioGetConfigurationId();
        // Remove it.
        /** @var CountryRepository $countryRepo */
        $countryRepo = self::$container->get(CountryRepository::class);
        $countryRepo->removeNow($country);
        // Is the configuration updated?
        $cfg = $this->managerRegistry->getRepository(Configuration::class)->find($cfgId);
        $this->assertNull($cfg);
    }
}
