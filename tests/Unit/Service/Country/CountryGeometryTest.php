<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Country;

use App\Entity\Country;
use App\Entity\CountryGeometry;
use App\Repository\CountryGeometryRepository;
use App\Repository\CountryRepository;
use App\Service\CountryGeometryService;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Test country geometries.
 */
class CountryGeometryTest extends \Symfony\Bundle\FrameworkBundle\Test\KernelTestCase
{
    use TestFixturesTrait;

    protected ?ManagerRegistry $managerRegistry;
    protected CountryGeometryService $countryGeometryService;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->loadFixtures();

        $this->managerRegistry = static::getContainer()->get(ManagerRegistry::class);
        $this->countryGeometryService = static::getContainer()->get(CountryGeometryService::class);
    }

    /**
     * Test points inside countries.
     *
     * @dataProvider getActions
     *
     * @param string $code
     * @param string $geometry
     * @param $latitude
     * @param $longitude
     *
     * @return void
     *
     * @throws Exception
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testInsideCountry(string $code, string $geometry, $latitude, $longitude)
    {
        $geojson = file_get_contents($geometry);
        /** @var CountryRepository $countryRepo */
        $countryRepo = $this->managerRegistry->getRepository(Country::class);
        $country = $countryRepo->find('hn');
        /** @var CountryGeometryRepository $countryGeometryRepo */
        $countryGeometryRepo = $this->managerRegistry->getRepository(Country::class);
        if (!$this->countryGeometryService->exist($country->getId(), true)) {
            $item = new CountryGeometry($country->getId());
            $countryGeometryRepo->saveNow($item);
        }
        $this->countryGeometryService->updateGeometry($country, $geojson);
        // --latitude=14.9836 --longitude=-87.0129 hn
        $this->assertTrue($this->countryGeometryService->validatePoint($country, $latitude, $longitude));
    }

    /**
     * @return array[] [['code' => string, 'country' => App/Entity/Country, 'latitude' => float, 'longitude' => float]]
     *
     * @throws \ReflectionException
     */
    public function getActions()
    {
        return [
            [
                'code' => 'hn',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/hn.geojson',
                'latitude' => 14.9836,
                'longitude' => -87.0129,
            ],
            [
                'code' => 'ni',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/ni.geojson',
                'latitude' => 12.3522309,
                'longitude' => -84.3115339,
            ],
            [
                'code' => 'bo',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/bo.geojson',
                'latitude' => -16.881697,
                'longitude' => -63.1429372,
            ],
            [
                'code' => 'co',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/co.geojson',
                'latitude' => 4.7492626,
                'longitude' => -71.5311256,
            ],
            [
                'code' => 'cr',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/cr.geojson',
                'latitude' => 9.6729603,
                'longitude' => -83.2868262,
            ],
            [
                'code' => 'do',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/do.geojson',
                'latitude' => 18.8442715,
                'longitude' => -70.1655788,
            ],
            [
                'code' => 'kg',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/kg.geojson',
                'latitude' => 41.0747775,
                'longitude' => 74.627999,
            ],
            [
                'code' => 'pa',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/pa.geojson',
                'latitude' => 8.4953,
                'longitude' => -80.6473,
            ],
            [
                // Punto real.
                'code' => 'pa',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/pa.geojson',
                'latitude' => 9.131489000000,
                'longitude' => -79.228709000000,
            ],
            [
                'code' => 'pe',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/pe.geojson',
                'latitude' => -9.0707571,
                'longitude' => -74.039808,
            ],
            [
                'code' => 'py',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/py.geojson',
                'latitude' => -23.1322354,
                'longitude' => -58.2257567,
            ],
            [
                'code' => 'tz',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/tz.geojson',
                'latitude' => -6.3569386,
                'longitude' => 35.4577302,
            ],
            [
                'code' => 'ug',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/ug.geojson',
                'latitude' => 1.2424463,
                'longitude' => 32.6564904,
            ],
            [
                'code' => 'br',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/br.geojson',
                'latitude' => -13.099539,
                'longitude' => -48.1085352,
            ],
            [
                'code' => 'mx',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/mx.geojson',
                'latitude' => 23.579644,
                'longitude' => -102.2041405,
            ],
            [
                'code' => 'sz',
                'geometry' => static::getKernelInstance()->getProjectDir().'/tests/assets/geometry/sz.geojson',
                'latitude' => -26.5540616,
                'longitude' => 31.9345273,
            ],
        ];
    }
}
