<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Country;

use App\Entity\Country;
use App\Service\ConfigurationService;
use App\Tests\Unit\Service\User\RoleTestBase;

/**
 * Test volume fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class CountryMeasurementUnitsTest extends RoleTestBase
{
    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->em = self::$container->get('doctrine.orm.entity_manager');
        $configuration = $this->em->getRepository('App\Entity\Configuration');
        $this->initConfigurations(
            $configuration,
            [
                'system.units.concentration' => dirname(__FILE__).'/../../../assets/system.units.concentration.yml',
                'system.units.flow' => dirname(__FILE__).'/../../../assets/system.units.flow.yml',
                'system.units.length' => dirname(__FILE__).'/../../../assets/system.units.length.yml',
                'system.units.volume' => dirname(__FILE__).'/../../../assets/system.units.volume.yml',
            ]
        );
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetMainUnitsOk(): void
    {
        $country = new Country();
        $country->setMainUnitConcentration('particles per million');
        $country->setMainUnitFlow('liter/second');
        $country->setMainUnitLength('metre');
        $country->setMainUnitVolume('cubic metre');

        $this->assertEquals('particles per million', $country->getMainUnitConcentration());
        $this->assertEquals('liter/second', $country->getMainUnitFlow());
        $this->assertEquals('metre', $country->getMainUnitLength());
        $this->assertEquals('cubic metre', $country->getMainUnitVolume());
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetMainUnitsConcentrationFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system concentration units.');

        $country = new Country();
        $country->setMainUnitConcentration('fail');
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetMainUnitsFlowFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system flow units.');

        $country = new Country();
        $country->setMainUnitFlow('fail');
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetMainUnitsLengthFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system length units.');

        $country = new Country();
        $country->setMainUnitLength('fail');
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetMainUnitsVolumeFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system volume units.');

        $country = new Country();
        $country->setMainUnitVolume('fail');
    }

    /**
     * Test to validate measurement collection units.
     *
     * @return void
     */
    public function testSetCollectionUnitsOk(): void
    {
        $country = new Country();
        $country->addUnitConcentration('particles per million');
        $country->addUnitFlow('liter/second');
        $country->addUnitLength('metre');
        $country->addUnitVolume('cubic metre');

        $this->assertContains('particles per million', $country->getUnitConcentrations());
        $this->assertContains('liter/second', $country->getUnitFlows());
        $this->assertContains('metre', $country->getUnitLengths());
        $this->assertContains('cubic metre', $country->getUnitVolumes());
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetCollectionUnitsConcentrationFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system concentration units.');

        $country = new Country();
        $country->addUnitConcentration('fail');
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetCollectionUnitsFlowFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system flow units.');

        $country = new Country();
        $country->addUnitFlow('fail');
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetCollectionUnitsLengthFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system length units.');

        $country = new Country();
        $country->addUnitLength('fail');
    }

    /**
     * Test to validate main measurement units.
     *
     * @return void
     */
    public function testSetCollectionUnitsVolumeFail(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Main unit "fail" must be present in system volume units.');

        $country = new Country();
        $country->addUnitVolume('fail');
    }
}
