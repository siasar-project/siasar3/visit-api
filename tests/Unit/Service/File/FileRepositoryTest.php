<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\File;

use App\Repository\ConfigurationRepository;
use App\Repository\CountryRepository;
use App\Repository\FileRepository;
use App\Service\FileSystem;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Service\User\InitTestEnvironmentTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test file repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FileRepositoryTest extends KernelTestCase
{
    use TestFixturesTrait;
    use InitTestEnvironmentTrait;

    protected string $prjDir;
    protected string $assetsDir;

    protected FileRepository $fileRepository;
    protected ConfigurationRepository $configurationRepository;
    protected CountryRepository $countryRepository;
    protected FileSystem $fileSystem;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        $kernel = self::bootKernel();
        $this->loadFixtures();

        $entityManager = self::$container->get('doctrine')->getManager();
        $this->configurationRepository = $entityManager->getRepository('App\Entity\Configuration');
        $this->fileRepository = $entityManager->getRepository('App\Entity\File');
        $this->countryRepository = $entityManager->getRepository('App\Entity\Country');
        $this->fileSystem = self::$container->get('os_file_system');
        $this->doctrine = self::$container->get('database_connection');

        $this->initConfigurations($this->configurationRepository);

        $dir = $kernel->getProjectDir();
        $this->prjDir = $dir.'/'.$_ENV['APP_CONFIG_PUBLIC_FILES'];
        $this->assetsDir = $dir.'/tests/assets';
    }

    /**
     * This method is called after each test.
     *
     * @see https://stackoverflow.com/a/3349792
     */
    protected function tearDown(): void
    {
        // Clear the public files' folder.
        $dir = $this->prjDir;
        // This method don't remove .gitkeep files and failed removing files'
        // folder, it's we want.
        try {
            @$this->fileRepository->systemFileRemove($dir);
        } catch (\Exception $e) {
        }
    }

    /**
     * Test that we can create a file, the file is moved to the files' folder
     * and the entity is created.
     */
    public function testCreateFileInCountry(): void
    {
        // Copy the file to test with it.
        @copy($this->assetsDir.'/siasar_logo_blue.svg', $this->prjDir.'/siasar_logo_blue.svg');

        // Create the file entity.
        $country = $this->countryRepository->find('hn');
        $file = $this->fileRepository->create($this->prjDir.'/siasar_logo_blue.svg', $country);
        $this->assertNotNull($file);

        // Verify the work.
        $fileInfo = array_merge(['dirname' => '', 'basename' => '', 'extension' => '', 'filename' => ''], pathinfo($file->getPath()));
        $this->assertEquals(
            $country->getCode().'/'.$file->getCreated()->format('Y-m'),
            $fileInfo['dirname']
        );
        $this->assertEquals('siasar_logo_blue.svg._'.$file->getId(), $fileInfo['basename']);
        $this->assertDirectoryExists($this->prjDir.'/'.$fileInfo['dirname']);
        $this->assertFileExists($this->prjDir.'/'.$file->getPath());
        $this->assertFileEquals($this->assetsDir.'/siasar_logo_blue.svg', $this->prjDir.'/'.$file->getPath());
    }

    /**
     * Test that we can create a file, the file is moved to the files' folder
     * and the entity is created.
     */
    public function testCreateFileOutCountry(): void
    {
        // Copy the file to test with it.
        @copy($this->assetsDir.'/siasar_logo_blue.svg', $this->prjDir.'/siasar_logo_blue.svg');

        // Create the file entity.
        $file = $this->fileRepository->create($this->prjDir.'/siasar_logo_blue.svg');
        $this->assertNotNull($file);

        // Verify the work.
        $fileInfo = array_merge(['dirname' => '', 'basename' => '', 'extension' => '', 'filename' => ''], pathinfo($file->getPath()));
        $this->assertEquals(
            'world/'.$file->getCreated()->format('Y-m'),
            $fileInfo['dirname']
        );
        $this->assertEquals('siasar_logo_blue.svg._'.$file->getId(), $fileInfo['basename']);
        $this->assertDirectoryExists($this->prjDir.'/'.$fileInfo['dirname']);
        $this->assertFileExists($this->prjDir.'/'.$file->getPath());
        $this->assertFileEquals($this->assetsDir.'/siasar_logo_blue.svg', $this->prjDir.'/'.$file->getPath());
    }

    /**
     * Test that removing a file remove it in database and filesystem.
     */
    public function testRemoveFile(): void
    {
        $this->loginUser('Pedro');
        // Copy the file to test with it.
        @copy($this->assetsDir.'/siasar_logo_blue.svg', $this->prjDir.'/siasar_logo_blue.svg');

        // Create the file entity.
        $file = $this->fileRepository->create($this->prjDir.'/siasar_logo_blue.svg');
        $fileId = $file->getId();
        $filePath = $file->getPath();
        $this->assertNotNull($file);

        // Remove the file.
        $this->fileRepository->removeNow($file);

        // Verify the work.
        $fileErased = $this->fileRepository->find($fileId);
        $this->assertNull($fileErased);
        $this->assertFileDoesNotExist($filePath);
    }

    /**
     * Test basic stream wrapper usage.
     */
    public function testFilesStreamWrapper(): void
    {
        file_put_contents('files://filename.txt', 'Y mi mujer, Ana, me preguntó: - Quieres un café?, mientras, yo escribia este texto...');
        $this->assertFileExists($this->prjDir.'/filename.txt');

        mkdir('files://happy_thoughts');
        $this->assertDirectoryExists($this->prjDir.'/happy_thoughts');
    }
}
