<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Language;

use App\Entity\Language;
use App\Entity\LocaleSource;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tests\TestFixturesTrait;
use App\Tests\Unit\Tools\TranslationTraitDummy;
use App\Tools\TranslatableMarkup;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Test language repository
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AlphabetsTest extends KernelTestCase
{
    use TestFixturesTrait;

    /**
     * Mock of language repository
     *
     * @var LanguageRepository|MockObject
     */
    protected $langRepositoryMock;
    protected ?LocaleSourceRepository $sourceRepository;
    protected ?LocaleSource $source;
    protected ?EntityManagerInterface $entityManager;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();

        $this->sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $this->source = $this->sourceRepository->create('Translation trait dummy');
        $this->entityManager = self::$container->get('doctrine')->getManager();
    }

    /**
     * Data provider to testValidIdInConfigurationWrite.
     *
     * @return string[][]
     */
    public function languagesValues(): array
    {
        return [
            ['RURU', 'Фиктивная черта перевода'],
            ['HEHE', 'דמה של תכונת תרגום'],
            ['ESES', 'Trait Dummy de traducción'],
            ['ARAR', 'ترجمة سمة وهمية'],
            ['ZHZH', '翻譯特質假人'],
            ['JAJA', '翻訳特性ダミー'],
        ];
    }

    /**
     * Test translate.
     *
     * @param string $langCode    Language code.
     * @param string $translation Translations string.
     *
     * @return void
     *
     * @dataProvider languagesValues
     */
    public function testRussianTranslation(string $langCode, string $translation): void
    {
        // Add new translation.
        $lang = $this->entityManager->getRepository(Language::class)->find($langCode);
        if (!$lang) {
            $lang = new Language($langCode);
            $lang->setIsoCode($langCode);
            $lang->setName($langCode);
        }

        $this->entityManager->persist($lang);
        $this->entityManager->flush();

        /** @var LocaleTargetRepository $targetRepository */
        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $targetRepository->create(
            $lang,
            $this->source,
            $translation
        );

        // Translate la string.
        $dummy = new TranslationTraitDummy();
        $this->assertEquals($translation, $dummy->getTranslatedText($langCode));
    }
}
