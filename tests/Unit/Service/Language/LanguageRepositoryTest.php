<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Language;

use App\Repository\LanguageRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Test language repository
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LanguageRepositoryTest extends KernelTestCase
{
    use TestFixturesTrait;

    /**
     * Mock of language repository
     *
     * @var LanguageRepository|MockObject
     */
    protected $langRepositoryMock;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();

        $this->langRepositoryMock = $this->getMockBuilder(LanguageRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs(
                [
                    self::$container->get(ManagerRegistry::class),
                ],
            )
            ->enableProxyingToOriginalMethods()
            ->onlyMethods(['saveNow', 'update', 'removeNow'])
            ->addMethods(['findOneById'])
            ->getMock();
    }

    /**
     * Test create a new language.
     *
     * @return void
     */
    public function testLanguageCreate(): void
    {
        $langRepository = self::$container->get(LanguageRepository::class);
        $langRepository->create('PTPT', 'pt', 'portuguese');

        $result = $langRepository->findOneById('PTPT');

        $this->assertEquals('PTPT', $result->getId());
    }

    /**
     * Test create a new language with a bad code.
     *
     * @return void
     */
    public function testBadIdLanguageCreate(): void
    {
        $langRepository = self::$container->get(LanguageRepository::class);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('[pt] Language IDs must be 4 characters blocks separated by underscore.');

        $langRepository->create('pt', 'pt', 'portuguese');
    }

    /**
     * Test create new source with invalid params.
     *
     * @return void
     */
    public function testLanguageCreateWithInvalidParams(): void
    {
        $this->expectException(Exception::class);
        $this->langRepositoryMock->create('', '', 'russian');
    }

    /**
     * Test create an existing language (duplicate entry).
     *
     * @return void
     */
    public function testLanguageCreateForAlreadyExistingLanguage(): void
    {
        $isoCode = 'es';
        $code = 'ESES';
        $name = 'spanish';

        $this->langRepositoryMock
            ->expects($this->exactly(1))
            ->method('saveNow')
            ->with($this->isType('object'))
            ->willThrowException(new UnprocessableEntityHttpException());

        $this->expectException(UnprocessableEntityHttpException::class);

        $this->langRepositoryMock->create($code, $isoCode, $name);
    }

    /**
     * Test create a new language with invalid parameters.
     *
     * @return void
     */
    public function testUpdateOneShouldThrowExceptionForInvalidLanguageId(): void
    {
        $id = '123';

        $this->langRepositoryMock
            ->expects($this->once())
            ->method('update')
            ->with($id)
            ->willThrowException(new BadRequestHttpException());

        $this->expectException(BadRequestHttpException::class);

        $this->langRepositoryMock->update($id, 'english');
    }

    /**
     * Test update a new language
     *
     * @return void
     */
    public function testUpdateOne(): void
    {
        $langRepository = self::$container->get(LanguageRepository::class);
        $langRepository->update('SPAN', 'español');

        $result = $langRepository->findOneById('SPAN');

        $this->assertEquals('SPAN', $result->getId());
        $this->assertEquals('español', $result->getName());
    }

    /**
     * Test remove a non-existing language ID
     *
     * @return void
     */
    public function testDeleteOneShouldThrowExceptionForInvalidLanguageId(): void
    {
        $id = '123';

        $this->langRepositoryMock
            ->expects($this->once())
            ->method('removeNow')
            ->with($id)
            ->willThrowException(new BadRequestHttpException());

        $this->expectException(BadRequestHttpException::class);

        $this->langRepositoryMock->removeNow($id);
    }
}
