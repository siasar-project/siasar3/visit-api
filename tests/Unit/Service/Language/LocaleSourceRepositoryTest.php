<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Language;

use App\Entity\LocaleSource;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tests\TestFixturesTrait;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * Test locale source repository.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LocaleSourceRepositoryTest extends KernelTestCase
{
    use TestFixturesTrait;

    /**
     * Mock of locale source repository.
     */
    protected LocaleSourceRepository|MockObject $sourceRepositoryMock;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();

        $this->sourceRepositoryMock = $this->getMockBuilder(LocaleSourceRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs(
                [
                    self::$container->get(ManagerRegistry::class),
                ],
            )
            ->enableProxyingToOriginalMethods()
            ->onlyMethods(['saveNow'])
            ->getMock();
    }

    /**
     * Test create new source.
     *
     * @return void
     */
    public function testLocaleSourceCreate(): void
    {
        /**
         * Locale source Repository
         *
         * @var LocaleSourceRepository $sourceRepository
         */
        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $sourceRepository->create('hello world');

        $result = $sourceRepository->findOneByMessage('hello world');

        $this->assertEquals('hello world', $result->getMessage());
    }

    /**
     * Test create new source with invalid params.
     *
     * @return void
     */
    public function testLocaleSourceCreateWithInvalidParams(): void
    {
        $this->sourceRepositoryMock
            ->expects($this->once())
            ->method('saveNow')
            ->with($this->isType('object'))
            ->willThrowException(new BadRequestException());

        $this->expectException(BadRequestException::class);

        $this->sourceRepositoryMock->create('');
    }

    /**
     * Test to get all translation of a source.
     *
     * @return void
     */
    public function testGetAllTranslationBySource(): void
    {
        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $source = $sourceRepository->findOneByMessage('not found');

        $translations = $sourceRepository->getAllTranslationsBySource($source);

        $this->assertEquals(count($translations), 3);
    }

    /**
     * Test to delete a source and all related translations.
     *
     * @return void
     */
    public function testRemoveLocaleSourceAndAllRelatedTranslations(): void
    {
        // Create source.
        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        /** @var LocaleSource $source */
        $source = $sourceRepository->findOneByMessage('not found');
        $sourceId = $source->getId();
        $this->assertNotNull($source);

        // Have we translation?
        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $target = $targetRepository->findOneBy(['localeSource' => $source->getId()]);
        $this->assertNotNull($target);

        // Removed source and translations?
        $sourceRepository->removeNow($source);
        $source = $sourceRepository->findOneByMessage('not found');
        $this->assertNull($source);

        $target = $targetRepository->findOneBy(['localeSource' => $sourceId]);
        $this->assertNull($target);
    }
}
