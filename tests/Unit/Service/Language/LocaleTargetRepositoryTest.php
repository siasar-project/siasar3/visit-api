<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\Language;

use App\Entity\LocaleTarget;
use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Service\SessionService;
use App\Tests\TestFixturesTrait;
use App\Tools\TranslatableMarkup;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Test locale target repository.
 *
 * @category SIASAR_3
 *
 * @author   Marta Rodríguez <marta.rodriguez@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class LocaleTargetRepositoryTest extends KernelTestCase
{
    use TestFixturesTrait;

    /**
     * Mock of locale target repository
     */
    protected LocaleTargetRepository|MockObject $targetRepositoryMock;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();

        $this->targetRepositoryMock = $this->getMockBuilder(LocaleTargetRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs(
                [
                    self::$container->get(ManagerRegistry::class),
                    self::$container->get(SessionService::class),
                ],
            )
            ->enableProxyingToOriginalMethods()
            ->onlyMethods(['saveNow'])
            ->getMock();
    }

    /**
     * Test create new source.
     *
     * @return void
     */
    public function testLocaleTargetCreate(): void
    {
        $languageRepository = self::$container->get(LanguageRepository::class);
        $lang = $languageRepository->findOneById('SPAN');

        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $source = $sourceRepository->findOneByMessage('testing translations');

        $targetRepository = self::$container->get(LocaleTargetRepository::class);

        $targetRepository->create(
            $lang,
            $source,
            'probando traducciones'
        );

        /** @var LocaleTarget $result */
        $result = $targetRepository->findOneBy(
            [
                'language' => 'SPAN',
                'localeSource' => $source->getId(),
            ],
        );

        $this->assertEquals('SPAN', $result->getLanguage()->getId());
        $this->assertEquals($source->getId(), $result->getLocaleSource()->getId());
        $this->assertEquals('probando traducciones', $result->getTranslation());
    }

    /**
     * Test to remove a translation by language and source.
     *
     * @return void
     */
    public function testLocaleTargetRemove(): void
    {
        $languageRepository = self::$container->get(LanguageRepository::class);
        $lang = $languageRepository->findOneById('SPAN');

        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $source = $sourceRepository->findOneByMessage('not found');

        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $localeTarget = $targetRepository->findOneBy(
            [
                'language' => $lang,
                'localeSource' => $source,
            ],
        );

        $this->assertNotNull($localeTarget);

        $targetRepository->removeNow($localeTarget);

        $localeTarget = $targetRepository->findOneBy(
            [
                'language' => $lang,
                'localeSource' => $source,
            ],
        );

        $this->assertNull($localeTarget);
    }

    /**
     * Test that we can get translation from backups languages.
     */
    public function testTranslateWithThreeLevels(): void
    {
        // Create languages.
        /** @var LanguageRepository $languageRepository */
        $languageRepository = self::$container->get(LanguageRepository::class);
        $lang1 = $languageRepository->create('TEST', 'test', 'Test');
        $lang2 = $languageRepository->create('TEST_TEST', 'test_test', 'Test 2');
        $lang3 = $languageRepository->create('TEST_TEST_TEST', 'test_test3', 'Test 3');

        // Create source.
        /** @var LocaleSourceRepository $sourceRepository */
        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $source = $sourceRepository->create('source');

        // Translate backup only.
        /** @var LocaleTargetRepository $targetRepository */
        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $targetRepository->create($lang1, $source, 'test');

        // Translating to language 2, can we get a translation from language 1?
        $text = new TranslatableMarkup($source->getMessage(), [], ['langcode' => $lang3->getId()]);
        $this->assertEquals('test', $text->render());

        // Translate lang 2.
        /** @var LocaleTargetRepository $targetRepository */
        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $targetRepository->create($lang2, $source, 'test 2');

        // Translating to language 2, can we get a translation from language 2?
        $text = new TranslatableMarkup($source->getMessage(), [], ['langcode' => $lang3->getId()]);
        $this->assertEquals('test 2', $text->render());

        // Translate lang 3.
        /** @var LocaleTargetRepository $targetRepository */
        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $targetRepository->create($lang3, $source, 'test 3');

        // Translating to language 2, can we get a translation from language 2?
        $text = new TranslatableMarkup($source->getMessage(), [], ['langcode' => $lang3->getId()]);
        $this->assertEquals('test 3', $text->render());
    }
}
