<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\User;

use App\Entity\AdministrativeDivision;
use App\Entity\Country;
use App\Entity\User;

/**
 * Test volume fields types.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class AccessManagerTest extends RoleTestBase
{
    /**
     * Test that we can query permissions list.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testGetSystemPermissions(): void
    {
        $permissions = $this->accessManager->getSystemPermissions();

        $this->assertCount(188, $permissions);
    }

    /**
     * Test that we can query permissions group list.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testGetSystemPermissionGroups(): void
    {
        $groups = $this->accessManager->getSystemPermissionsGroups();

        $this->assertCount(13, $groups);
    }

    /**
     * Test that we can query role list.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testGetSystemRoles(): void
    {
        $roles = $this->accessManager->getSystemRoles();

        $this->assertCount(10, $roles);
    }

    /**
     * Test that we can query permissions by role.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testGetPermissionsByRoles(): void
    {
        $permissions = $this->accessManager->getPermissionsByRoles(['ROLE_USER', 'ROLE_ADMIN']);

        $this->assertCount(35, $permissions);
    }

    /**
     * Test that we can't query permissions by undefined role.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function testGetPermissionsByUndefinedRole(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Role "ROLE_UNDECLARED" not defined.');

        $permissions = $this->accessManager->getPermissionsByRoles(['ROLE_UNDECLARED']);
    }

    /**
     * Test that an admin user has 'all permissions' permission.
     */
    public function testAdminHasPermission(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        $admin = $users->findOneByEmail('pedro@front.id');

        $this->assertTrue($this->accessManager->hasPermission($admin, 'all permissions'));

        // Works cache?
        $this->assertTrue($this->accessManager->hasPermission($admin, 'all permissions'));
    }

    /**
     * Test that an admin have 'test permission 2' permission, that is not defined to it.
     */
    public function testAdminHaveTest2Permission(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        $user = $users->findOneByEmail('pedro@front.id');

        $this->assertTrue($this->accessManager->hasPermission($user, 'test permission 2'));
    }

    /**
     * Test that a user don't have 'test permission 2' permission, that is not defined to it.
     */
    public function testUserNotHaveTest2Permission(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        $user = $users->findOneByEmail('carlos@front.id');

        // This test set the country from the user to be sure that is in the correct country context.
        // But this doesn't have security sense.
        $this->assertFalse($this->accessManager->hasPermission($user, 'test permission 2', [$user->getCountry()]));
    }

    /**
     * Test that a user don't have 'all permissions' permission.
     */
    public function testUserDontHasPermission(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        $user = $users->findOneByEmail('carlos@front.id');

        $this->assertFalse($this->accessManager->hasPermission($user, 'all permissions'));
    }

    /**
     * Test that a user have 'test permission' permission.
     */
    public function testUserHaveTestPermission(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('carlos@front.id');

        $this->assertTrue($this->accessManager->hasPermission($user, 'test permission', [$user->getCountry()]));
    }

    /**
     * Test that a user haven't 'test permission' permission outside the country.
     */
    public function testUserHaveNotTestPermissionOutside(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('hola+SectorialValidatorEs@front.id');

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $this->assertFalse($this->accessManager->hasPermission($user, 'test permission', [$country]));
    }

    /**
     * Test that a user have 'test permission' permission if not country context.
     */
    public function testUserHaveTestPermissionWithoutContext(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('carlos@front.id');

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('hn');

        $this->assertTrue($this->accessManager->hasPermission($user, 'test permission', [$country]));
    }

    /**
     * Test that a user haven't 'test permission' permission if user is inactive.
     */
    public function testInactiveUserHavenTestPermissionWithoutContext(): void
    {
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('marco@front.id');

        $countryRepo = $this->entityManager->getRepository(Country::class);
        $country = $countryRepo->find('ni');

        $this->assertFalse($this->accessManager->hasPermission($user, 'test permission', [$country]));
    }

    /**
     * Test that we can get divisions tree how array.
     */
    public function testGetPlainDivisionsTree(): void
    {
        $divisionsRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $root = $divisionsRepository->find('01FENEDYFA6KNAA4J459P5S0RF');
        $divisions = $this->accessManager->getAdministrativeDivisionsPlain($root);
        $this->assertCount(8, $divisions);
    }

    /**
     * Test that an user has 'all administrative divisions' permission.
     */
    public function testUserHasAllDivisionsPermission(): void
    {
        // Load user.
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $admin */
        $admin = $users->findOneByEmail('carlos@front.id');

        // Load division to use how context.
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $root = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RF');

        $this->assertTrue(
            $this->accessManager->hasPermission(
                $admin,
                'all administrative divisions',
                [$admin->getCountry()],
                $root
            )
        );
    }

    /**
     * Test that an user have not 'all administrative divisions' permission.
     */
    public function testUserHaveNotAllDivisionsPermission(): void
    {
        // Load user.
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $admin */
        $admin = $users->findOneByEmail('marta.fernandez@front.id');

        // Load division to use how context.
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $root = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RF');

        $this->assertFalse(
            $this->accessManager->hasPermission(
                $admin,
                'all administrative divisions',
                [$admin->getCountry()],
                $root
            )
        );
    }

    /**
     * Tests access divisions by level 1.
     */
    public function testUserWithLevel1(): void
    {
        // Load user.
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('marta.fernandez@front.id');

        // Load division to use how context.
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $level1Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RF');
        $level2Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RG');
        $level3Division = $divisionRepository->find('01FFFBAWKRKV16EPSW3BDDZEX9');
        $level4Division = $divisionRepository->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');

        $user->addAdministrativeDivision($level1Division);

        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level1Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level2Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level3Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level4Division
            )
        );
    }

    /**
     * Tests access divisions by level 2.
     */
    public function testUserWithLevel2(): void
    {
        // Load user.
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('marta.fernandez@front.id');

        // Load division to use how context.
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $level1Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RF');
        $level2Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RG');
        $level3Division = $divisionRepository->find('01FFFBAWKRKV16EPSW3BDDZEX9');
        $level4Division = $divisionRepository->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');

        $user->addAdministrativeDivision($level2Division);

        $this->assertFalse(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level1Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level2Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level3Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level4Division
            )
        );
    }

    /**
     * Tests access divisions by level 3.
     */
    public function testUserWithLevel3(): void
    {
        // Load user.
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('marta.fernandez@front.id');

        // Load division to use how context.
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $level1Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RF');
        $level2Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RG');
        $level3Division = $divisionRepository->find('01FFFBAWKRKV16EPSW3BDDZEX9');
        $level4Division = $divisionRepository->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');

        $user->addAdministrativeDivision($level3Division);

        $this->assertFalse(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level1Division
            )
        );
        $this->assertFalse(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level2Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level3Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level4Division
            )
        );
    }

    /**
     * Tests access divisions by level 4.
     */
    public function testUserWithLevel4(): void
    {
        // Load user.
        $users = $this->entityManager->getRepository(User::class);
        /** @var User $user */
        $user = $users->findOneByEmail('marta.fernandez@front.id');

        // Load division to use how context.
        $divisionRepository = $this->entityManager->getRepository(AdministrativeDivision::class);
        $level1Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RF');
        $level2Division = $divisionRepository->find('01FENEDYFA6KNAA4J459P5S0RG');
        $level3Division = $divisionRepository->find('01FFFBAWKRKV16EPSW3BDDZEX9');
        $level4Division = $divisionRepository->find('01FFFC1K7EG1E2AZ4H2H5ZXQS3');

        $user->addAdministrativeDivision($level4Division);

        $this->assertFalse(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level1Division
            )
        );
        $this->assertFalse(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level2Division
            )
        );
        $this->assertFalse(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level3Division
            )
        );
        $this->assertTrue(
            $this->accessManager->hasPermission(
                $user,
                'test permission',
                [$user->getCountry()],
                $level4Division
            )
        );
    }
}
