<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\User;

use App\Entity\User;
use App\Repository\ConfigurationRepository;
use App\Service\SessionService;
use App\Tools\Json;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Init Permissions Trait.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
trait InitTestEnvironmentTrait
{
    protected static ?KernelBrowser $client = null;
    protected null|Connection $doctrine;

    /**
     * Init permissions.
     *
     * @param ConfigurationRepository $configuration
     * @param array                   $extras        Add extras configuration keys. ['[cfg_key]' => 'yml_path']
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function initConfigurations(ConfigurationRepository $configuration, array $extras = []): void
    {
        // Load configurations.
        $configurations = array_merge(
            [
                'system.permissions' => dirname(__FILE__).'/../../../assets/system.permissions.yml',
                'system.role.admin' => dirname(__FILE__).'/../../../assets/system.role.admin.yml',
                'system.role.admin-local' => dirname(__FILE__).'/../../../assets/system.role.admin-local.yml',
                'system.role.digitizer' => dirname(__FILE__).'/../../../assets/system.role.digitizer.yml',
                'system.role.reader' => dirname(__FILE__).'/../../../assets/system.role.reader.yml',
                'system.role.sectorial-validator' => dirname(__FILE__).'/../../../assets/system.role.sectorial-validator.yml',
                'system.role.sectorial-global' => dirname(__FILE__).'/../../../assets/system.role.sectorial-global.yml',
                'system.role.sectorial-local' => dirname(__FILE__).'/../../../assets/system.role.sectorial-local.yml',
                'system.role.supervisor' => dirname(__FILE__).'/../../../assets/system.role.supervisor.yml',
                'system.role.translator' => dirname(__FILE__).'/../../../assets/system.role.translator.yml',
                'system.role.user' => dirname(__FILE__).'/../../../assets/system.role.user.yml',
                'system.units.flow' => dirname(__FILE__).'/../../../assets/system.units.flow.yml',
                'system.units.length' => dirname(__FILE__).'/../../../assets/system.units.length.yml',
                'system.units.volume' => dirname(__FILE__).'/../../../assets/system.units.volume.yml',
                'system.units.concentration' => dirname(__FILE__).'/../../../assets/system.units.concentration.yml',
            ],
            $extras
        );
        // Clear form tables.
        $schemaManager = $this->doctrine->getSchemaManager();
        $tables = $schemaManager->listTables();
        $dbPlatform = $this->doctrine->getDatabasePlatform();
        foreach ($tables as $table) {
            if (str_starts_with($table->getName(), 'form_')) {
                $q = $dbPlatform->getTruncateTableSql($table->getName());
                $this->doctrine->executeStatement($q);
            }
        }
        // Clear configurations.
        $configuration->clear();
        // Install configurations.
        foreach ($configurations as $configKey => $confPath) {
            $configValue = Yaml::parseFile($confPath);
            $cfg = $configuration->findEditable($configKey);
            if (!$cfg) {
                $cfg = $configuration->create($configKey, $configValue);
            } else {
                $cfg->setValue($configValue);
            }
            $cfg->saveNow();
        }
    }

    /**
     * Get user session token.
     *
     * @param string $userName
     *
     * @return string
     */
    public function getUserToken(string $userName): string
    {
        $entityManager = self::$container->get('doctrine')->getManager();
        $tokenManager = self::$container->get('lexik_jwt_authentication.jwt_manager');

        $userRepository = $entityManager->getRepository(User::class);
        $user = $userRepository->findOneByUserName($userName);

        return $tokenManager->create($user);
    }

    /**
     * Start user session.
     *
     * @param string $username
     */
    protected function loginUser(string $username): void
    {
        $token = $this->getUserToken($username);
        $_ENV['TESTS_HEADER_HTTP_AUTHORIZATION'] = 'Bearer '.$token;
        // Update session status.
        //static::getContainer()->get('security.token_storage')->setToken(null);
        static::getContainerInstance()->get('session')->clear();
        static::getContainerInstance()->get('session')->invalidate();

        $entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $tokenManager = static::getContainerInstance()->get('lexik_jwt_authentication.jwt_manager');
        $session = static::getContainerInstance()->get('session');
        $this->sessionManager = new SessionService($entityManager, $tokenManager, $session);
        $this->sessionManager->resetSession();
    }

    /**
     * Login user by RestFul.
     *
     * Empty user to anonymous session.
     *
     * @param string $user
     *
     * @return KernelBrowser
     */
    protected function loginRest(string $user): KernelBrowser
    {
        unset($_ENV['TESTS_HEADER_HTTP_AUTHORIZATION']);

        // Login.
        $session = clone self::$client;
        if (!empty($user)) {
            $payload = [
                'username' => $user,
                'password' => '123456',
            ];

            $session->request('POST', '/api/v1/users/login', [], [], [], \json_encode($payload));

            $response = $session->getResponse();
            $responseData = $this->getResponseData($response);

            $session->setServerParameters(
                [
                    'HTTP_AUTHORIZATION' => 'Bearer '.$responseData['token'],
                    'CONTENT_TYPE' => 'application/json',
                    'HTTP_ACCEPT' => 'application/json',
                ]
            );
            // Server parameters aren't set while testing.
            $_ENV['TESTS_HEADER_HTTP_AUTHORIZATION'] = 'Bearer '.$responseData['token'];
        }

        return $session;
    }

    /**
     * Get usable response body.
     *
     * @param Response $response
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getResponseArray(Response $response): array
    {
        return (new Json())->decode($response->getContent());
    }

    /**
     * Parse response.
     *
     * @param Response $response Raw response.
     *
     * @return array
     */
    protected function getResponseData(Response $response): array
    {
        return \json_decode($response->getContent(), true);
    }
}
