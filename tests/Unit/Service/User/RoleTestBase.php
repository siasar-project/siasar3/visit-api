<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\User;

use App\Repository\ConfigurationRepository;
use App\Service\AccessManager;
use App\Service\SessionService;
use App\Tests\TestFixturesTrait;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Base to test's that require roles and permissions.
 */
class RoleTestBase extends KernelTestCase
{
    use TestFixturesTrait;
    use InitTestEnvironmentTrait;

    protected ?EntityManager $entityManager;
    protected ?ConfigurationRepository $configuration;
    protected ?Connection $doctrine;
    protected ?AccessManager $accessManager;
    protected ?JWTTokenManagerInterface $tokenManager;
    protected ?SessionService $sessionManager;
    protected ?Session $session;
    protected array $extraConfigurations = [];

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $_ENV['TESTS_LOADING_FIXTURES'] = true;
        $this->loadFixtures();

        $this->entityManager = static::getContainerInstance()->get('doctrine')->getManager();
        $this->entityManager->clear();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');
        $this->doctrine = static::getContainerInstance()->get('database_connection');
        $this->accessManager = static::getContainerInstance()->get('access_manager');
        $this->tokenManager = static::getContainerInstance()->get('lexik_jwt_authentication.jwt_manager');
        $this->sessionManager = static::getContainerInstance()->get('session_service');
        $this->session = static::getContainerInstance()->get('session');

        $this->initConfigurations($this->configuration, $this->extraConfigurations);

        $this->loginUser('Pedro');
        $_ENV['TESTS_LOADING_FIXTURES'] = false;
    }
}
