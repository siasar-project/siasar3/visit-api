<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Service\User;

use App\Entity\Country;
use App\Entity\User;
use App\Repository\CountryRepository;
use App\Repository\UserRepository;
use App\Service\Mailer\MailerService;
use App\Service\Password\EncoderService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Test user repository.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class UserRepositoryTest extends KernelTestCase
{
    protected Country $country;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();

        $countryRepository = self::$container->get(CountryRepository::class);

        if (!$country = $countryRepository->find('es')) {
            $country = new Country('es');
            $country->setName('España');
            $countryRepository->saveNow($country);
        }

        $this->country = $country;
    }

    /**
     * Test we can register users.
     *
     * @throws ORMException
     * @throws OptimisticLockException
     *
     * @return void
     */
    public function testUserRegister(): void
    {
        $name = 'username';
        $email = 'username@api.com';
        $password = '123456';

        $userRepository = self::$container->get(UserRepository::class);
        $user = $userRepository->create($name, $email, $password, $this->country);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($name, $user->getUsername());
        $this->assertEquals($email, $user->getEmail());
    }

    /**
     * Test that passwords are validated.
     *
     * @throws ORMException
     * @throws OptimisticLockException
     *
     * @return void
     */
    public function testUserRegisterForInvalidPassword(): void
    {
        $name = 'username';
        $email = 'username@api.com';
        $password = '123';

        $encoderService = $this->getMockBuilder(EncoderService::class)->disableOriginalConstructor()->getMock();
        $mockedUserRepository = new UserRepository(
            self::$container->get(ManagerRegistry::class),
            $encoderService,
            self::$container->get(MailerService::class)
        );

        $encoderService
            ->expects($this->exactly(1))
            ->method('generateEncodedPassword')
            ->with($this->isType('object'), $this->isType('string'))
            ->willThrowException(new BadRequestHttpException());

        $this->expectException(BadRequestHttpException::class);

        $mockedUserRepository->create($name, $email, $password, $this->country);
    }

    /**
     * Test that we can't register a user two times.
     *
     * @throws ORMException
     * @throws OptimisticLockException
     *
     * @return void
     */
    public function testUserRegisterForAlreadyExistingUser(): void
    {
        $name = 'Pedro';
        $email = 'pedro@front.id';
        $password = '123456';

        $userRepository = $this->getMockBuilder(UserRepository::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs(
                [
                    self::$container->get(ManagerRegistry::class),
                    self::$container->get(EncoderService::class),
                    self::$container->get(MailerService::class),
                ]
            )
            ->enableProxyingToOriginalMethods()
            ->onlyMethods(['saveNow'])
            ->getMock();
        $userRepository
            ->expects($this->exactly(1))
            ->method('saveNow')
            ->with($this->isType('object'))
            ->willThrowException(new UnprocessableEntityHttpException());

        $this->expectException(UnprocessableEntityHttpException::class);

        $userRepository->create($name, $email, $password, $this->country);
    }
}
