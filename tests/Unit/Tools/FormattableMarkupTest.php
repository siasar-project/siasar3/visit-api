<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Tools;

use App\Tools\FormattableMarkup;
use PHPUnit\Framework\TestCase;

/**
 * Tests the TranslatableMarkup class.
 *
 * @coversDefaultClass \App\Tools\FormattableMarkup
 *
 * @group utility
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FormattableMarkupTest extends TestCase
{

    /**
     * The error message of the last error in the error handler.
     *
     * @var string
     */
    protected string $lastErrorMessage;

    /**
     * The error number of the last error in the error handler.
     *
     * @var int
     */
    protected int $lastErrorNumber;

    /**
     * @covers ::__toString
     * @covers ::jsonSerialize
     */
    public function testToString()
    {
        $string = 'Can I please have a @replacement';
        $formattableString = new FormattableMarkup($string, ['@replacement' => 'kitten']);
        $text = (string) $formattableString;
        $this->assertEquals('Can I please have a kitten', $text);
        $text = $formattableString->jsonSerialize();
        $this->assertEquals('Can I please have a kitten', $text);
    }

    /**
     * @covers ::count
     */
    public function testCount()
    {
        $string = 'Can I please have a @replacement';
        $formattableString = new FormattableMarkup($string, ['@replacement' => 'kitten']);
        $this->assertEquals(strlen($string), $formattableString->count());
    }

    /**
     * Custom error handler that saves the last error.
     *
     * We need this custom error handler because we cannot rely on the error to
     * exception conversion as __toString is never allowed to leak any kind of
     * exception.
     *
     * @param int    $errorNumber  The error number.
     * @param string $errorMessage The error message.
     */
    public function errorHandler(int $errorNumber, string $errorMessage)
    {
        $this->lastErrorNumber = $errorNumber;
        $this->lastErrorMessage = $errorMessage;
    }

    /**
     * @covers ::__toString
     *
     * @param string $string       String.
     * @param array  $arguments    Arguments.
     * @param int    $errorNumber  Last error number.
     * @param string $errorMessage Last error message.
     *
     * @dataProvider providerTestUnexpectedPlaceholder
     */
    public function testUnexpectedPlaceholder(string $string, array $arguments, int $errorNumber, string $errorMessage)
    {
        // We set a custom error handler because of https://github.com/sebastianbergmann/phpunit/issues/487
        set_error_handler([$this, 'errorHandler']);
        // We want this to trigger an error.
        $markup = new FormattableMarkup($string, $arguments);
        // Cast it to a string which will generate the errors.
        $output = (string) $markup;
        restore_error_handler();
        // The string should not change.
        $this->assertEquals($string, $output);
        $this->assertEquals($errorNumber, $this->lastErrorNumber);
        $this->assertEquals($errorMessage, $this->lastErrorMessage);
    }

    /**
     * Data provider for FormattableMarkupTest::testUnexpectedPlaceholder().
     *
     * @return array
     */
    public function providerTestUnexpectedPlaceholder(): array
    {
        return [
            ['Non alpha starting character: ~placeholder', ['~placeholder' => 'replaced'], E_USER_ERROR, 'Invalid placeholder (~placeholder) in string: Non alpha starting character: ~placeholder'],
            ['Alpha starting character: placeholder', ['placeholder' => 'replaced'], E_USER_ERROR, 'Invalid placeholder (placeholder) in string: Alpha starting character: placeholder'],
            // Ensure that where the placeholder is located in the string is
            // irrelevant.
            ['placeholder', ['placeholder' => 'replaced'], E_USER_ERROR, 'Invalid placeholder (placeholder) in string: placeholder'],
        ];
    }
}
