<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Tools\Measurements;

use App\Repository\ConfigurationRepository;
use App\Tools\Measurements\FlowMeasurement;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Tests the FlowMeasurement class.
 *
 * @coversDefaultClass \App\Tools\Measurements\FlowMeasurement
 *
 * @group utility
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class FlowMeasurementTest extends KernelTestCase
{
    protected EntityManager|null $entityManager;
    protected ConfigurationRepository|null $configuration;
    protected array $units;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        self::bootKernel();
        $this->entityManager = self::$container->get('doctrine')->getManager();
        $this->configuration = $this->entityManager->getRepository('App\Entity\Configuration');

        $units = Yaml::parseFile(dirname(__FILE__).'/../../../assets/system.units.flow.yml');
        $cfg = $this->configuration->findEditable('system.units.flow');
        if (!$cfg) {
            $cfg = $this->configuration->create('system.units.flow', $units);
        } else {
            $cfg->setValue($units);
        }
        $cfg->saveNow();
        $this->units = $units;
    }

    /**
     * Test default constructor.
     */
    public function testInitClass(): void
    {
        $len = new FlowMeasurement();
        $this->assertEquals(0, $len->getValue());
        $this->assertEquals('liter/second', $len->getUnit());
    }

    /**
     * Test that we can change value witouth change units.
     */
    public function testChangeValue(): void
    {
        $len = new FlowMeasurement();
        $len->setValue(1);

        $this->assertEquals(1, $len->getValue());
        $this->assertEquals('liter/second', $len->getUnit());
    }

    /**
     * Test that we can change units without change value.
     */
    public function testChangeUnits(): void
    {
        $len = new FlowMeasurement(1000);
        $len->setUnit('cubic meter/second');

        $this->assertEquals(1, $len->getValue());
        $this->assertEquals('cubic meter/second', $len->getUnit());
    }

    /**
     * Test that we can init an not default unit and then change it.
     */
    public function testInitNotDefaultUnitsAndChangeUnits(): void
    {
        $len = new FlowMeasurement(1, 'cubic meter/second');
        $this->assertEquals(1, $len->getValue());
        $this->assertEquals('cubic meter/second', $len->getUnit());

        $len->setUnit('cubic meter/day');
        $this->assertEquals(86400, $len->getValue());
        $this->assertEquals('cubic meter/day', $len->getUnit());
    }
}
