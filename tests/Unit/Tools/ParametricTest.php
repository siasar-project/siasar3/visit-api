<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Tools;

use App\Entity\AdministrativeDivision;
use App\Entity\DisinfectingSubstance;
use App\Entity\GeographicalScope;
use App\Tools\FormattableMarkup;
use App\Tools\Parametric;
use App\Traits\GetContainerTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Tests the TranslatableMarkup class.
 *
 * @coversDefaultClass \App\Tools\FormattableMarkup
 *
 * @group utility
 *
 * @category SIASAR_3
 *
 * @author   Drupal <drupal.org>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class ParametricTest extends KernelTestCase
{
    use GetContainerTrait;

    protected Parametric|null $parametricTool;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->parametricTool = self::getContainerInstance()->get('parametric_tool');
    }

    /**
     * Test we can get parametric classes.
     */
    public function testGetParametricClasses()
    {
        $classes = $this->parametricTool->getParametricClasses();
        $this->assertCount(30, $classes);
    }

    /**
     * Test we can get label parametric property.
     */
    public function testGetParametricLabelProperty()
    {
        // By class name.
        $labelProperty = $this->parametricTool->getParametricLabelField(DisinfectingSubstance::class);
        $this->assertEquals('name', $labelProperty);

        // By object.
        $labelProperty = $this->parametricTool->getParametricLabelField(new DisinfectingSubstance());
        $this->assertEquals('name', $labelProperty);
    }

    /**
     * Test we can get label parametric property.
     */
    public function testGetParametricLabelPropertyWithTwoDefaults()
    {
        // By class name.
        $labelProperty = $this->parametricTool->getParametricLabelField(GeographicalScope::class);
        $this->assertEquals('name', $labelProperty);

        // By object.
        $labelProperty = $this->parametricTool->getParametricLabelField(new GeographicalScope());
        $this->assertEquals('name', $labelProperty);
    }

    /**
     * Test we can get label parametric property in an entity without Label annotation. It must return Id property.
     */
    public function testGetParametricWithoutLabelProperty()
    {
        // By class name.
        $labelProperty = $this->parametricTool->getParametricLabelField(AdministrativeDivision::class);
        $this->assertEquals('id', $labelProperty);

        // By object.
        $labelProperty = $this->parametricTool->getParametricLabelField(new AdministrativeDivision());
        $this->assertEquals('id', $labelProperty);
    }

    /**
     * Test that detect parametric class.
     */
    public function testIsParametricOk()
    {
        $this->assertTrue($this->parametricTool->isParametricClass(DisinfectingSubstance::class));
    }

    /**
     * Test that detect not parametric class.
     */
    public function testIsParametricFalse()
    {
        $this->assertFalse($this->parametricTool->isParametricClass(self::class));
    }

    /**
     * Test that can detect default parametric entity.
     */
    public function testDetectParametricEntity()
    {
        $entity = new DisinfectingSubstance();
        $entity->setStarting(true);
        $entity->setKeyIndex('99');
        $entity->setName('Other');

        $this->assertTrue($this->parametricTool->isDefaultParametric($entity));
    }

    /**
     * Test that can detect default parametric entity.
     */
    public function testDetectParametricEntityWithTwoDefaults()
    {
        $entity = new GeographicalScope();
        $entity->setStarting(true);
        $entity->setKeyIndex('99');
        $entity->setName('Other');

        $this->assertTrue($this->parametricTool->isDefaultParametric($entity));
    }

    /**
     * Test that can detect not default parametric entity.
     */
    public function testDetectNotDefaultParametricEntity()
    {
        $entity = new DisinfectingSubstance();
        $entity->setStarting(true);
        $entity->setKeyIndex('0');
        $entity->setName('Other');

        $this->assertFalse($this->parametricTool->isDefaultParametric($entity));
    }

    /**
     * Test that can detect not parametric entity.
     */
    public function testDetectNotParametricEntity()
    {
        $entity = new \stdClass();

        $this->assertFalse($this->parametricTool->isDefaultParametric($entity));
    }

    /**
     * Test that we can get a list of default entity sketch's.
     */
    public function testGetDefaultEntities()
    {
        $list = $this->parametricTool->getDefaultEntities(GeographicalScope::class);
        $mustHave = [
            1 => 'Country/State',
            99 => 'Other',
        ];

        foreach ($list as $item) {
            $this->assertArrayHasKey($item['keyIndex'], $mustHave);
            $this->assertEquals($mustHave[$item['keyIndex']], $item['name']);
        }
    }
}
