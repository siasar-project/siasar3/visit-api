<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Drupal <drupal.org>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Tools;

use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tests\TestFixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Tests the TranslatableMarkup class.
 *
 * @coversDefaultClass \App\Tools\TranslatableMarkup
 *
 * @group StringTranslation
 */
class StringTranslationTraitTest extends KernelTestCase
{
    use TestFixturesTrait;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();
    }

    /**
     * Test translation trait.
     *
     * @return void
     */
    public function testTranslateString(): void
    {
        $dummy = new TranslationTraitDummy();
        // Add new translation.
        $languageRepository = self::$container->get(LanguageRepository::class);
        $lang = $languageRepository->findOneById('SPAN');

        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        // Force add the new source.
        $aux = $dummy->getTranslatedText();
        // Not translated.
        $this->assertEquals($dummy->getText(), $aux);
        // And load it to compare later.
        $source = $sourceRepository->findOneByMessage($dummy->getText());
        $targetRepository = self::$container->get(LocaleTargetRepository::class);
        $targetRepository->create(
            $lang,
            $source,
            'mi traducción patatera'
        );
        // Translate la string.
        $aux = $dummy->getTranslatedText();
        $this->assertEquals('mi traducción patatera', $aux);
    }
}
