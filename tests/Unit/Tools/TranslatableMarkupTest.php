<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Drupal <drupal.org>
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Tools;

use App\Repository\LanguageRepository;
use App\Repository\LocaleSourceRepository;
use App\Repository\LocaleTargetRepository;
use App\Tests\TestFixturesTrait;
use App\Tools\FormattableMarkup;
use App\Tools\TranslatableMarkup;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Tests the TranslatableMarkup class.
 *
 * @coversDefaultClass \App\Tools\TranslatableMarkup
 *
 * @group StringTranslation
 */
class TranslatableMarkupTest extends KernelTestCase
{
    use TestFixturesTrait;

    /**
     * The error message of the last error in the error handler.
     *
     * @var string
     */
    protected string $lastErrorMessage;

    /**
     * The error number of the last error in the error handler.
     *
     * @var int
     */
    protected int $lastErrorNumber;

    /**
     * Setup tests environment.
     *
     * @see https://symfony.com/doc/current/testing.html#set-up-your-test-environment
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        // (1) boot the Symfony kernel
        self::bootKernel();
        $this->loadFixtures();
    }

    /**
     * Custom error handler that saves the last error.
     *
     * We need this custom error handler because we cannot rely on the error to
     * exception conversion as __toString is never allowed to leak any kind of
     * exception.
     *
     * @param int    $errorNumber
     *   The error number.
     * @param string $errorMessage
     *   The error message.
     */
    public function errorHandler(int $errorNumber, string $errorMessage)
    {
        $this->lastErrorNumber = $errorNumber;
        $this->lastErrorMessage = $errorMessage;
    }

    /**
     * @covers ::__construct
     */
    public function testIsStringAssertion()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('$string ("foo") must be a string.');
        new TranslatableMarkup(new TranslatableMarkup('foo', [], []));
    }

    /**
     * @covers ::__construct
     */
    public function testIsStringAssertionWithFormattableMarkup()
    {
        $formattableString = new FormattableMarkup('@bar', ['@bar' => 'foo']);
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('$string ("foo") must be a string.');
        new TranslatableMarkup($formattableString);
    }

    /**
     * Test translate a string.
     *
     * @return void
     */
    public function testTranslateString(): void
    {
        // Add new translation.
        $languageRepository = self::$container->get(LanguageRepository::class);
        $lang = $languageRepository->findOneById('SPAN');

        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $source = $sourceRepository->findOneByMessage('testing translations');

        $targetRepository = self::$container->get(LocaleTargetRepository::class);

        $targetRepository->create(
            $lang,
            $source,
            'probando traducciones'
        );

        // Translate la string.
        $text = new TranslatableMarkup('testing translations', [], ['langcode' => 'SPAN']);
        $this->assertEquals('probando traducciones', $text);
    }

    /**
     * Test create new source.
     *
     * @return void
     */
    public function testTranslateUnknownString(): void
    {
        // This must return the some string.
        $text = new TranslatableMarkup('Test create new source.', [], ['langcode' => 'es']);
        $this->assertEquals('Test create new source.', $text);

        // The source must be created.
        $sourceRepository = self::$container->get(LocaleSourceRepository::class);
        $source = $sourceRepository->findOneByMessage('Test create new source.');
        $this->assertNotNull($source);
    }
}
