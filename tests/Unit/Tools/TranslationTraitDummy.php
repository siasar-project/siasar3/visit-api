<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @package  API
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

namespace App\Tests\Unit\Tools;

use App\Traits\StringTranslationTrait;

/**
 * A dummy class to test the string translation trait.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */
class TranslationTraitDummy
{
    use StringTranslationTrait;

    /**
     * Get an english text.
     *
     * @return string
     */
    public function getText(): string
    {
        return 'Translation trait dummy';
    }

    /**
     * Get the some text but translated in spanish.
     *
     * @param string $langCode (optional) Language code to use. Default 'SPAN'.
     *
     * @return string
     */
    public function getTranslatedText(string $langCode = 'SPAN'): string
    {
        return $this->t($this->getText(), [], ['langcode' => $langCode]);
    }

    /**
     * Get a plural translated text.
     *
     * @param int    $count    Amount.
     * @param string $langCode (optional) Language code to use. Default 'FREN'.
     *
     * @return string
     */
    public function getPluralTranslatedText(int $count, string $langCode = 'FREN'): string
    {
        return $this->formatPlural($count, '1 hour', '@count hours', [], ['langcode' => $langCode]);
    }
}
