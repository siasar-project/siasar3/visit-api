<?php

/**
 * This file is part of the SIASAR package.
 *
 * PHP version 8.0
 *
 * SIASAR Global is a joint initiative launched by the governments of Honduras,
 * Nicaragua and Panama that soon expanded to other regions. The strategic
 * purpose of this initiative is to have a basic, updated and comparable
 * information tool on the rural water supply and sanitation services in place
 * in a given country.
 *
 * @category SIASAR_3
 *
 * @author   Pedro Pelaez <pedro@front.id>
 *
 * @license  https://www.gnu.org/licenses/gpl-3.0.html GPL v3
 *
 * @link     http://globalsiasar.org/es/contact
 */

// @codingStandardsIgnoreFile

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/bootstrap.php')) {
    // phpcs:ignore
    require dirname(__DIR__).'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}

echo "SIASAR Quality control tests".PHP_EOL.PHP_EOL;

$parsedUrl = parse_url($_ENV['DATABASE_URL'], PHP_URL_HOST);

echo printf('Using connection: %s', $parsedUrl).PHP_EOL.PHP_EOL;

$_ENV['TEST_ENVIRONMENT'] = true;

if (isset($_ENV['BOOTSTRAP_RESET_DATABASE']) && $_ENV['BOOTSTRAP_RESET_DATABASE'] == true) {
    echo "Refresh test database...\n";
    _runConsole('php "%s/../bin/console" doctrine:database:drop --env=test --force --no-interaction');
    _runConsole('php "%s/../bin/console" doctrine:database:create --env=test --no-interaction --if-not-exists');
    _runConsole('php "%s/../bin/console" doctrine:schema:update --env=test --force --no-interaction');
    echo "Done".PHP_EOL.PHP_EOL;
}

/**
 * Execute a host command.
 *
 * @param string $cmd       Console command to execute.
 * @param bool   $witOutput Allow send output to console.
 *
 * @return void
 */
function _runConsole(string $cmd, bool $witOutput = true): void
{
    if (!$witOutput) {
        ob_start();
    }
    passthru(sprintf($cmd, __DIR__));
    if (!$witOutput) {
        ob_end_clean();
    }
}
